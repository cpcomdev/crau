CRAU Upgrade
============

De CRAU 1.2 � CRAU 1.3
----------------------

### Gestion des cong�s

Affiche les cong�s via Figgo ainsi que le type de cong�s.
Affiche �galement les d�placements.

### Gestion des prestataire

Diff�rencie les prestataires pour une v�rification diff�rente du nombre d'heure car les cong�s ne sont pas import�.

### Compatibilit� IE 7-9

Modification du style pour une adaptation � IE.


De CRAU 1.0 � CRAU 1.2
----------------------

### Gestion des services

Affiche le total par service et permet de plier/d�plier par service.