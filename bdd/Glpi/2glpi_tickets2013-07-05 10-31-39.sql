insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (1,8,'Relais DHCP Inactif','2013-04-10 12:11:00','2013-04-22 09:08:22','2013-04-10 14:57:01','2013-04-22 09:08:22',6,'closed',6,6,0,'NetworkEquipment',4,'Relais DHCP du site inactif.
Ticket Orange n°1304260800',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-10 14:57:01',0,9706,324936,255,115,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (2,10,'Problème de réception poste de Mme GUILLERM','2013-04-12 11:40:00','2013-06-28 09:55:04','2013-06-17 08:23:01','2013-06-28 09:55:04',24,'closed',24,2,0,'',0,'Nadège,

Je rencontre des soucis avec mon poste de téléphone, en effet :

- je reçois des communications directement sur mon poste sans que Claire et Sabrina ne soient en ligne
- les communications arrivent automatiquement sur mon poste sans qu''il ne sonne ou que j''ai à décrocher.

Comme convenu je te laisse le soin de contacter qui de droit.

Par avance je t''en remercie.

Cordialement


Sylvie
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 08:23:01',0,1963644,406060,11737,223,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (3,10,'modification de TWP caller','2013-04-12 13:48:00','2013-04-23 08:43:33','2013-04-12 14:56:43','2013-04-23 08:43:33',24,'closed',24,2,0,'',0,'Patrice,

Merci 
•	d’installer et attribuer une licence TWP Caller + Alerter (3 personnes)
•	de retirer la licence TWP Caller + Alerter (2 personnes)
',3,3,3,1,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-12 14:56:43',0,0,284133,4123,119,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (4,11,'plus de téléphone poste de Mme LEBLOND','2013-04-15 09:18:00','2013-04-26 09:22:55','2013-04-15 17:27:00','2013-04-26 09:22:55',24,'closed',24,3,0,'',0,'Le téléphone de Mme LEBLOND ne fonctionne plus',5,3,4,0,1,0.0000,0.0000,0.0000,0,'&lt;p&gt;Ticket ouvert chez ORANGE N&#176; 1304307153&lt;/p&gt;','none',0,0,null,'2013-04-15 17:27:00',0,27917,361178,1423,154,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (5,10,'ACP non opérationnel','2013-04-15 15:23:00','2013-04-29 08:58:32','2013-04-16 16:52:23','2013-04-29 08:58:32',6,'closed',6,1,0,'Software',9,'Suite déplacement NetAPP :

Les utilisateurs ne peuvent plus faire de transferts.
Les statistiques ne remontent plus.

',5,5,6,0,1,0.0000,0.0000,0.0000,0,'&lt;p&gt;Lors d''un d&#233;placement de VM &#224; froid, l''adresse MAC peut changer.&lt;/p&gt;
&lt;p&gt;Attention aux logiciels dont la licence est li&#233;e &#224; l''adresse MAC :&lt;/p&gt;
&lt;p&gt;AASTRA ACP&lt;/p&gt;
&lt;p&gt;AASTRA M7450&lt;/p&gt;
&lt;p&gt;CogisNetwork : VTPro&lt;/p&gt;','none',0,0,null,'2013-04-16 16:52:23',0,41233,367699,7330,178,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (6,10,'M7450 Non opérationnel','2013-04-15 15:35:00','2013-04-29 08:58:32','2013-04-18 15:40:25','2013-04-29 08:58:32',24,'closed',6,1,0,'Software',8,'Suite déplacement baie NetAPP les licences M7450 ont été supprimées.

L''adresse MAC a été modifiée lors de la relance du serveur sur les nouveaux ESX.
Appel auprès d''Orange pour génération des clés de licence selon la nouvelle adresse MAC.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-18 15:40:25',0,0,408212,129925,220,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (7,0,'Déplacement Baie NetAPP sur Labège','2013-04-15 15:57:00',null,null,'2013-05-17 15:58:04',6,'assign',6,4,0,'',0,'Afin de finaliser la mise en service de la salle informatique.

Planification du déplacement de la baie NetAPP de production.',3,4,3,1,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,1005184,0,0,126,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (8,0,'Validation BES 10','2013-04-16 11:56:00',null,'2013-07-02 09:20:11','2013-07-02 09:20:11',6,'solved',6,1,0,'Software',2,'Christophe,

Merci de valider la version 10 de BES.

Jean-Hugues ',2,2,3,4,2,0.0000,0.0000,0.0000,0,null,'none',0,0,'2013-04-30 15:55:10','2013-07-02 09:20:11',0,0,0,2366651,2863,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (9,0,'Changement NAS Sauvegardes','2013-04-16 13:22:00','2013-05-14 08:51:36','2013-05-03 10:42:56','2013-05-14 08:51:36',150,'closed',6,1,0,'NetworkEquipment',41,'Merci de valider les sauvegardes sur le nouveau NAS.

Une fois cette opération réaliser, mettre à jour les procédures et transférer les informations au support.

Jean-Hugues',3,2,2,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-03 10:42:56',0,0,847776,552056,167,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (10,0,'changement de nom','2013-04-16 14:37:00','2013-04-29 08:58:32','2013-04-18 16:58:42','2013-04-29 08:58:32',24,'closed',24,2,0,'',0,'Bonjour,
 
Pouvez vous changer mon nom sur mon poste téléphonique 201185 (01 39 24 21 85 - ligne directe).
 
En effet, depuis fin d''année dernière, il apparait sous le nom de Sonia ZERROUGUI.
 
Par avance, merci
 
Cordialement,
 
Céline CHARRAUD
Service Rendez-vous 
Tél. direct : 01 39 24 21 85
',2,3,2,1,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-18 16:58:42',0,0,368492,94902,108,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (11,10,'problème licence VTPRO','2013-04-16 15:57:00','2013-04-29 08:58:32','2013-04-18 08:22:05','2013-04-29 08:58:32',24,'closed',24,2,0,'',0,'Patrice,

Je n’ai plus accès à VT PRO « Licence non valide »
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-18 08:22:05',0,0,363692,59105,95,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (12,0,'[KAREO] - Erreur de transfert SFTP du fichier NH du 16-04-2013','2013-04-16 16:46:00','2013-04-29 08:58:32','2013-04-18 10:43:54','2013-04-29 08:58:32',23,'closed',6,1,0,'',0,'Bonjour,

L’envoi du fichier NH Generali a rencontré une erreur. Il est nécessaire de renvoyer le fichier manuellement pour la date du 16-04-2013.

Cordialement ,

Le support CPCOM

16/04/2013  
------------------DEBUT--------------------------  
"Debut reception fichier resultat" 
"ERREUR de transfert" 
"erreur : " 1 
"Fin de traitement" 
-------------------FIN---------------------------
',3,3,3,5,1,0.0000,0.0000,0.0000,0,null,'none',0,0,'2013-04-18 19:54:55','2013-04-18 10:43:54',0,64495,296257,179,179,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (13,0,'Track-IT Problème de remonté multiple lors d''une recherche ','2013-03-19 09:29:00','2013-04-23 18:09:52','2013-04-23 18:09:52','2013-04-23 18:09:52',150,'closed',150,4,0,'',0,'Problème de remonté de plusieurs fiches lors d''une recherche sur un n° d''actif.

En cours
Ticket ouvert chez Numara
Après plusieurs analyses et tests le tickets est passé au support Numara US',3,3,3,5,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,1111252,1111252,908632,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (14,0,'Plantage du service de temps','2013-04-15 09:00:00','2013-04-30 08:45:30','2013-04-19 16:01:09','2013-04-30 08:45:30',154,'closed',6,1,0,'',0,'Le service ntpd s’arrête sur les serveurs de temps du groupe.',3,4,4,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-19 16:01:09',0,0,474330,198069,127556,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (15,0,'NETAPP Erreur de création de Snapshot','2013-04-18 08:26:00','2013-04-29 08:58:32','2013-04-18 15:04:47','2013-04-29 08:58:32',154,'closed',6,1,0,'NetworkEquipment',43,'Depuis hier 19h, des mails arrivent au projet pour une erreur de création de snapshot sur la baie netapp de NFrance.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-18 15:04:47',0,0,304352,23927,170,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (16,10,'2 postes std bloqué à VERSAILLES','2013-04-18 08:48:00','2013-04-29 08:58:32','2013-04-18 16:23:45','2013-04-29 08:58:32',24,'closed',24,2,0,'',0,'Il ya deux poste 5370ip N° 201102 et 201103 de bloqués',4,3,4,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-18 16:23:45',0,27020,276012,325,206,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (17,0,'[INSTALL] MaJ version logiciel pour le service achat','2013-04-18 11:35:00',null,null,'2013-04-22 13:49:40',6,'assign',23,1,0,'',0,'

De : CPCOM/CATALLO François-Xavier 
Envoyé : mardi 16 avril 2013 11:16
À : CPCOM/CHOLLET Ludovic
Cc : CPCOM/DELCOURT Jean-Hugues; CPCOM/FAURE Etienne; CPCOM/ROUGE Brice; CPCOM/SALINGARDES Damien; CPCOM/MARTI Mickael
Objet : RE: Installation de Thinprint 8.6 et des services Juniper United

Bonjour,

Ludo peux-tu revoir les anciennes procédure d’installation des Postes et mettre à jours les versions des logiciels à installer.

Voir également le Progi afin de s’assurer que les versions à jour sont présentes.

Nous utiliserons celle-ci jusqu’à ce qu’une solution soit trouvée pour remplacer le Master .

Merci 

Fx 



De : CPCOM/ROUGE Brice 
Envoyé : mardi 16 avril 2013 10:18
À : CPCOM/SALINGARDES Damien; CPCOM/MARTI Mickael
Cc : CPCOM/CATALLO François-Xavier; CPCOM/DELCOURT Jean-Hugues; CPCOM/FAURE Etienne
Objet : Installation de Thinprint 8.6 et des services Juniper United
Importance : Haute

Bonjour,

Nous venons de constater lors de la validation des postes qu’il n’était pas installé la bonne version du client Thinprint sur les pcs Eurexo. Je vous rappelle que vous devez installer la version 8.6.

Quand à la l’installation du client Vpn, il faut que vous installez également le service « Juniper United ».


Ces installations sont inclus dans le Master mais comme vous ne l’utilisez plus, attention de bien installer les bonnes version des logiciels ainsi que tous les éléments nécessaire au bon fonctionnement des applications.

Normalement, les procédures sont là pour vous aider.

Merci à vous.




Cordialement,

Brice Rougé
Service Informatique du Groupe Prunay
Responsable Support
Mail : brice.rouge@groupeprunay-si.fr
Tél : 05 61 39 96 33
Portable : 06 03 16 67 53

 

',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,196,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (18,0,'[QOS] Traffic réseau sur la bande D3 (Montrouge)','2013-04-18 11:38:00','2013-04-19 13:43:30','2013-04-19 13:43:30','2013-04-19 13:43:30',23,'closed',23,1,0,'',0,'Pour info.

Ludo jeter un oeil sur le lien de montrouge et regarder si il sature.

A+

Stéphane
 
De : claudine.delpy@orange.com [mailto:claudine.delpy@orange.com] 
Envoyé : Tuesday, April 16, 2013 05:08 PM
À : CPCOM/GIRAUDEAU Stephane 
Objet : RE: Ligne SDSL Eurexo Montrouge 
 
Bonjour M Giraudeau,

La QOS de votre routeur chez Ornis a pu être mise à niveau vendredi , il n’y a plus d’émissions en D3 .
Depuis 2 jours , on ne constate donc plus de réceptions en D3 sur Eurexo Montrouge .


Restant à votre disposition, 
 
Bien cordialement,
 
Claudine Delpy
Responsable Service Client
tel. 05 62 30 74 23  /  05 34 54 31 05 new
claudine.delpy@orange.com
Orange Business Services  SCE   USC Midi-Est
6 avenue Albert Durand 31706 BLAGNAC 
www.orange-business.com
 

De : DELPY Claudine SCE 
Envoyé : mercredi 10 avril 2013 16:03
À : ''CPCOM/GIRAUDEAU Stephane''
Objet : RE: Ligne SDSL Eurexo Montrouge

Re-Bonjour ,

Comme promis, nous avons analysé la QOS ..
A première vue c’est votre centre serveur Ornis  qui émet en D3 .D’où les réceptions constatées sur plusieurs sites.
 
S’agissant d’un routeur Bintec sur ce site , la mise à niveau automatisée de la QOS sur l’ensemble du réseau , a pu n’être que partielle.
Loick Carrière a tenté une mise à jour manuelle mais à priori non opérationnelle, pour l’instant.

Nous reprendrons la correction et vous tiendrons informé.


NB : la signalisation est codée en RT (voix) ou D1  ..
 
Bien cordialement,
 
Claudine Delpy
Responsable Service Client
tel. 05 62 30 74 23  /  05 34 54 31 05 new
claudine.delpy@orange.com
Orange Business Services  SCE   USC Midi-Est
6 avenue Albert Durand 31706 BLAGNAC 
www.orange-business.com
 

De : CPCOM/GIRAUDEAU Stephane [mailto:Stephane.GIRAUDEAU@groupeprunay-si.fr] 
Envoyé : lundi 8 avril 2013 16:48
À : DELPY Claudine SCE
Objet : Ligne SDSL Eurexo Montrouge

Bonjour, 


Sur le lien de l’agence Eurexo Montrouge, je constate du trafic sur la bande D3. Pourriez-vous me dire quel flux est redirigé dans cette bande par la QOS ?


En vous remerciant par avance,


Stéphane GIRAUDEAU
Service Projet CPCOM',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,50730,50730,292,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (19,0,'[HARDWARE] Arret du serveur w2003-741-01 dans la nuit','2013-04-18 11:42:00','2013-04-22 13:57:33','2013-04-22 13:57:33','2013-04-22 13:57:33',23,'closed',23,1,0,'',0,'Le serveur d''Annecy c''est coupé dans la nuit.

Erreur : 

ASR Detected by System ROM	4/18/2013 1:42AM
PCI Bus Error (Slot 0, Bus 0, Device 0, Function 0)	4/18/2013 1:32AM	

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,94533,94533,329,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (20,0,'[WSUS] Désactiver la mise à jour windows KB2823324','2013-04-18 11:49:00','2013-04-19 13:30:56','2013-04-19 13:30:56','2013-04-19 13:30:56',23,'closed',23,1,0,'',0,'
De : CPCOM/GAY Serge 
Envoyé : mercredi 17 avril 2013 16:14
À : CPCOM/ROUGE Brice
Objet : L''utilitaire chkdsk se lance suite au redémarrage du poste 
 
Bonjour,
 
Pour éviter  que chkdsk se lance au démarrage, il faut désinstaller la maj Windows qui pose un pb avec Kaspersky. 
 
http://support.kaspersky.com/fr/9750
Désinstallation manuelle de la mise à jour de sécurité
1.	Dans le Panneau de configuration, ouvrez Programmes, puis cliquez sur Afficher les mises à jour installées. 
2.	Sélectionnez la Mise à jour de sécurité pour Microsoft Windows (KB2823324), puis cliquez sur Désinstaller pour désinstaller la mise à jour de sécurité. 
 
@+
 
    Serge GAY
 
  05 61 39 96 44
 
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,49316,49316,114,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (21,0,'[VALIDATION] Logiciel LUNAR','2013-04-18 11:54:00','2013-04-22 13:55:37','2013-04-22 13:55:37','2013-04-22 13:55:37',23,'closed',23,1,0,'',0,'Valider le logiciel SuperNOVA (nouvelle version de LUNAR) destiné à Florence RALLE.',3,3,3,4,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,93697,93697,80,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (22,0,'[MESSAGERIE] Script de création de BAL','2013-04-18 14:01:00','2013-05-13 09:46:36','2013-05-02 17:22:58','2013-05-13 09:46:36',23,'closed',23,1,0,'',0,'J''ai déposé le script v1 sur le serveur d''admin. Etienne va le tester et me remonter les problèmes.',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-02 17:22:58',0,0,719136,444118,79,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (23,0,'[SAUVEGARDE] Autexia : erreur persistente avec BackupExec','2013-04-18 14:22:00','2013-04-22 10:45:49','2013-04-22 10:45:49','2013-04-22 10:45:49',23,'closed',23,1,0,'',0,'10.17.2.1',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,55,73374,73374,82,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (24,0,'[TELEPHONIE] Problème pour ouvrir TWP Caller','2013-04-18 14:24:00','2013-05-13 09:46:36','2013-05-02 15:36:03','2013-05-13 09:46:36',23,'closed',23,1,0,'',0,'Suite au remplacement de son poste, Josefa JEUX (Eurexo Nemours) n''a plus accès à TWP Caller.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-02 15:36:03',0,0,717756,436323,266,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (25,0,'[ORANGE] Autorisation des flux FTP passif vers telergos.com','2013-04-18 15:14:00','2013-04-19 13:30:16','2013-04-19 13:30:16','2013-04-19 13:30:16',23,'closed',23,1,0,'',0,'
De : Engimo/PERRON Pascal 
Envoyé : mardi 9 avril 2013 16:44
À : CPCOM/Support
Objet : Serveur FTP - Owliance

Bonjour,

Nous avons un service de frappe de documents à distance (Owliance). Nous leur envoyons des fichiers issus de dictataphone et ils nous renvoient ensuite le document sous Word.
Ils ont mis à notre disposition un serveur FTP pour nous permettre de leur envoyer des fichiers volumineux. Or le transfert ne fonctionne pas pour des « gros » fichiers.
Merci de vérifier si il y a un blocage de notre côté.
Le service infra de Owliance a fait des vérifications de son côté (voir mail ci-après).

Cordialement

PS
J’utilise filezilla pour faire le transfert.
Configuration du serveur FTP:
Hote = ftp.bar.telergos.com
Nom d’utilisateur = engimo6
Mot de passe = 8Y3f72B


Pascal Perron - Expert
Engimo INTERNATIONAL
Tél. : +33 1 30 78 18 46
Port : +33 6 22 08 29 03
Fax : +33 1 39 18 04 42
p.perron@engimo.fr
www.engimo.fr





De : Raphael KRIEF [mailto:rkrief@owliance.com] 
Envoyé : mardi 9 avril 2013 16:10
À : Engimo/PERRON Pascal
Objet : RE: test

Bonjour,
 
Nous avons récupéré les fichiers que vous avez posté sur free.
Concernant le ftp, pouvez-vous voir avec votre service infra si il n’y a pas de blocage lors du transfert ftp ?
 
 
Merci
Cordialement,
 
 
Raphael KRIEF
Ingénieur Systèmes et réseaux	
Tel  +33 1 82 70 16 31	 
26, rue Cambacérès
75008 PARIS
 
  Adoptez l’éco attitude : merci de n''imprimer ce mail qu''en cas de nécessité
 
De : Engimo/PERRON Pascal [mailto:p.perron@engimo.fr] 
Envoyé : vendredi 5 avril 2013 17:59
À : Raphael KRIEF
Objet : RE: test
 
 
 
De : Raphael KRIEF [mailto:rkrief@owliance.com] 
Envoyé : vendredi 5 avril 2013 17:58
À : Engimo/PERRON Pascal
Objet : test
 
TEST
 
Cordialement,
 
 
Raphael KRIEF
Ingénieur Systèmes et réseaux	
Tel  +33 1 82 70 16 31	 
26, rue Cambacérès
75008 PARIS
 
  Adoptez l’éco attitude : merci de n''imprimer ce mail qu''en cas de nécessité
 
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,36976,36976,85,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (26,0,'rnrnDe : Engimo/PERRON Pascal rnEnvoyé : mercredi 10 avril 2013 12:01r','2013-04-18 15:15:00',null,null,'2013-05-03 11:40:54',23,'assign',23,1,0,'',0,'

De : Engimo/PERRON Pascal 
Envoyé : mercredi 10 avril 2013 12:01
À : CPCOM/CHOLLET Ludovic
Objet : RE: Serveur FTP - Owliance

Bonjour,

Je viens de faire un essai avec un fichier de 16 Mo qui est passé sans souci.
Problème résolu.
Merci.


Pascal Perron - Expert
Engimo INTERNATIONAL
Tél. : +33 1 30 78 18 46
Port : +33 6 22 08 29 03
Fax : +33 1 39 18 04 42
p.perron@engimo.fr
www.engimo.fr




De : CPCOM/CHOLLET Ludovic 
Envoyé : mercredi 10 avril 2013 11:55
À : Engimo/PERRON Pascal
Cc : CPCOM/ROUGE Brice
Objet : RE: Serveur FTP - Owliance

Bonjour M. PERRON,

En effet, le service FTP fournit à votre disposition n’a pas un fonctionnement standard.
La protection mise en place chez Orange bloque donc certaine fonction car détecté hors cadre. 
Je viens d’autoriser ce site FTP en désactivant la sécurité avancée, vous est-il possible de faire un test de transfert afin de valider le bon fonctionnement ?

Restant à votre disposition,

Cordialement,

Ludovic CHOLLET
Service Informatique
GROUPE-PRUNAY
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,41,0,1);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (27,0,'[OWNCLOUD] Mission à fort enjeu','2013-04-18 15:39:00',null,null,'2013-05-03 10:50:31',23,'assign',23,1,0,'',0,'Florence CARDON
Clarice PERRET',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,152,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (28,0,'[SERVEUR] Mise à jour des icones bureau All USers','2013-04-18 15:42:00',null,null,'2013-04-22 13:49:40',6,'assign',23,1,0,'',0,'Supprimer les icones inutiles présent sur les bureau All Users des serveurs EURISK.',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,54,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (29,0,'[SERVEUR] Dépromouvoir DC w2003-061-01','2013-04-18 15:42:00',null,'2013-07-04 11:21:47','2013-07-04 11:21:47',23,'solved',23,1,0,'',0,'Dépromouvoir le DC
Sortir le serveur du domaine
Renommer le serveur
Remttre en domaine
Désactiver le DHCP
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-04 11:21:47',0,0,0,2360387,121,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (30,0,'[GPO] Validation de la stratégie domaine pour les postes SCAN (EURISK)','2013-04-18 15:45:00','2013-05-17 09:01:25','2013-05-06 17:35:11','2013-05-17 09:01:25',23,'closed',23,1,0,'',0,'Les scripts JSS sont bloquées par la GPO domaine.
J''ai sorti les postes de l''OU pour ne pas appliquer la GPO en attendant de la valider.',3,3,3,2,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-06 17:35:11',0,0,882985,525011,119,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (31,0,'[NAGIOS] Ajout des notifications NTP vers Pascal.PEREZ','2013-04-18 18:19:00','2013-04-18 18:21:27','2013-04-18 18:21:27','2013-04-18 18:21:27',23,'closed',23,1,0,'',0,'Bug, certain serveur n''envoi pas les notifications à Pascal.
J''ai corrigé un bug au niveau des templates de services.

Test : OK
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,147,147,111,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (32,0,'Fuite fuel groupe électrogène','2013-04-19 16:01:00','2013-05-10 09:03:56','2013-04-29 14:07:27','2013-05-10 09:03:56',154,'closed',154,4,0,'',0,'Suite à une forte odeur de fuel aux alentour du groupe électrogène, constat d''une nappe de fuel dans le groupe sur le réservoir.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-29 14:07:27',0,0,622976,252387,295,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (33,0,'Plus d''accès au partage support','2013-04-22 09:15:00','2013-05-03 08:36:49','2013-04-22 09:22:32','2013-05-03 08:36:49',6,'closed',6,1,0,'',0,'Depuis ce matin plus d''accès au partage Support sur le NAS.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-22 09:22:32',0,0,386509,452,379,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (34,0,'[NAGIOS] Migration nouvelle infra','2013-04-22 09:28:00','2013-05-27 09:28:09','2013-05-15 17:19:16','2013-05-27 09:28:09',23,'closed',23,1,0,'',0,'Migration de Nagios vers le réseau 10.31.20.0',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-15 17:19:16',0,0,1080009,762676,32,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (35,0,'Erreur service MySQL','2013-04-22 09:27:00','2013-05-03 08:36:49','2013-04-22 15:56:21','2013-05-03 08:36:49',154,'closed',154,4,0,'',0,'Crash du service MySQL lié à filesystem full.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-22 15:56:21',0,0,385789,23361,144,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (36,0,'[NAGIOS] Vérification du service DHCP groupe','2013-04-22 09:28:00','2013-05-27 09:28:09','2013-05-16 16:10:33','2013-05-27 09:28:09',23,'closed',23,1,0,'',0,'Ajout du check DHCP pour le cluster DHCP',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-16 16:10:33',0,0,1080009,801753,381,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (37,0,'[VALIDATION] CD-REEF en ligne','2013-04-22 09:43:00',null,null,'2013-04-22 13:49:40',6,'assign',23,1,0,'',0,'Tester l''application CD-REEF en ligne et voir s''il est possible de l''utiliser avec plusieurs postes.',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,50,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (38,0,'[NAGIOS] Change w2003-642-01 CP2000','2013-04-22 10:33:00','2013-04-22 10:42:14','2013-04-22 10:42:14','2013-04-22 10:42:14',23,'closed',23,1,0,'',0,'Vincent a supprimé une des deux bases CP2000. J''ai mis à jour le check du service CP2000.',3,3,3,6,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,554,554,140,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (39,0,'[NAGIOS] Change w2003-741-01 CP2000','2013-04-22 10:35:00','2013-04-22 10:42:00','2013-04-22 10:42:00','2013-04-22 10:42:00',23,'closed',23,1,0,'',0,'Vincent a déplacé la base CP2000 d''Amberieu vers le serveur d''Annecy.
- Supprimer le check CP2000 sur Ambérieu
- Ajouter le check de la nouvelle base sur Annecy',3,3,3,6,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,420,420,96,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (40,0,'[ARDI] Problème d''envoi de mail, erreur 401 unautorized','2013-04-22 11:35:00','2013-04-22 11:36:56','2013-04-22 11:36:56','2013-04-22 11:36:56',23,'closed',23,1,0,'',0,'
De : CPCOM/CHOLLET Ludovic 
Envoyé : lundi 22 avril 2013 11:36
À : CPCOM/FAURE Etienne
Objet : RE: Impossible d''envoyer des courriels

Etienne,

ARDI se sert de l’adresse montpellier@eurexo.fr pour envoyer les mails.
J’ai constaté que le mot de passe de ce compte a changé depuis le 19/04.

Je te conseille donc de remettre l’ancien mot de passe, à savoir : 4HEfc$!
Sinon il faut mettre à jour le nouveau mot de passe sur ARDI pour l’ensemble des sessions de Montpellier.

Je reste dispo si jamais,

Ludo

De : CPCOM/FAURE Etienne 
Envoyé : lundi 22 avril 2013 10:39
À : CPCOM/CHOLLET Ludovic
Objet : TR: Impossible d''envoyer des courriels



De : CPCOM/ROUGE Brice 
Envoyé : vendredi 19 avril 2013 15:43
À : CPCOM/Support
Objet : TR: Impossible d''envoyer des courriels



Cordialement,

Brice Rougé
Service Informatique du Groupe Prunay
Responsable Support
Mail : brice.rouge@groupeprunay-si.fr
Tél : 05 61 39 96 33
Portable : 06 03 16 67 53

 

De : Eurexo Castelnau/MUSTAPHA Roxanne 
Envoyé : vendredi 19 avril 2013 15:42
À : CPCOM/ROUGE Brice
Objet : Impossible d''envoyer des courriels

 

MUSTAPHA Roxanne
EUREXO MONTPELLIER
223 Avenue Clément Ader
Castelnau 2000
34170 CASTELNAU LE LEZ
 
Tél. : 04.99.52.28.60
Fax : 04.99.52.28.61
Ligne directe : 04.99.52.23.12
r.mustapha@eurexo.fr
 
 
 

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,116,116,98,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (41,0,'[DATAEXCHANGER] Mise à jour des sources ODBC','2013-04-22 11:39:00','2013-05-27 09:28:09','2013-05-14 13:42:11','2013-05-27 09:28:09',23,'closed',23,1,0,'',0,'
Remplacer les adresses IP par les alias DNS agce dans les sources ODBC.

---
De : CPCOM/BONNET Guillaume 
Envoyé : lundi 22 avril 2013 11:27
À : CPCOM/CHOLLET Ludovic
Objet : Suite à la suppression du serveur Amberieu...


Bonjour,

Suite à notre conversation voici les points à traiter :

Mise à jour des sources odbc de la machine DeX en utilisatant les alias DNS
  
Redirection de  agce-0010.groupe-prunay.fr (10.1.1.1) sur 10.74.1.1 (Annecy)


Merci 

Cordialement,
Guillaume Bonnet
Service Informatique
Groupe Prunay
05 61 39 70 39

',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-14 13:42:11',0,251540,820609,447051,54,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (42,0,'[DATAEXCHANGER] Change alias AGCE Ambérieu vers Annecy','2013-04-22 11:39:00','2013-04-22 13:52:38','2013-04-22 13:52:38','2013-04-22 13:52:38',23,'closed',23,1,0,'',0,'Suite au déplacement de la base CP2000 Ambérieu vers Annecy, rediriger l''alias AGCE d''ambérieu vers Annecy',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,8018,8018,133,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (43,1,'[EURISK Amberieu] : Demande démangement lignes téléphonique','2013-04-22 13:51:00','2013-06-28 09:55:04','2013-06-17 08:20:38','2013-06-28 09:55:04',24,'closed',6,2,0,'',0,'Dans le cadre de la fermeture de l''agence d''Amberieu, merci de demander le déménagement des lignes téléphoniques de l''agence à l''adresse suivante :

Isabelle BERTHE
7 clos Marquis
01150 LAGNIEU

Informations sur le point de départ :
Eurisk Amberieu
16, rue Alexandre Bérard
01500 Ambérieu-en-Bugey
Tel : 04 74 38 51 90

Jean-Hugues
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 08:20:38',0,1706757,395887,1421,301,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (44,0,'[DATAEXCHANGER] Supprimer la source ODBC 1640','2013-04-23 10:13:00','2013-04-23 10:14:44','2013-04-23 10:14:44','2013-04-23 10:14:44',23,'closed',23,1,0,'',0,'J''ai supprimé la source ODBC 1640 qui correspond a l''agence d''Agirdex.
(Vincent a supprimé la base de donnée CP2000 sur le serveur).
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,104,104,94,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (45,0,'La connexion RDP sur le serveur de Messagerie CPCOMFE04 n''est plus disponible','2013-04-19 10:31:00','2013-04-23 18:05:10','2013-04-23 18:05:10','2013-04-23 18:05:10',150,'closed',150,4,0,'',0,'Message :
Connexion RDP impossible, Erreur de protocole,code 3334

Un ticket est ouvert chez Orange 1304786569',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,113650,113650,86849,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (46,0,'[NAS] Problème pour parcourir un chemin réseau vers le nas-312-03','2013-04-23 10:15:00','2013-04-23 10:58:08','2013-04-23 10:58:08','2013-04-23 10:58:08',23,'closed',23,1,0,'',0,'Utiliser un autre compte d''authentification :Pas résolu
Sortir et remettre en domaine : Pas résolu
Droit administrateur / disable GPO : Pas résolu
Désactiver l''antivirus & parefeu : Pas résolu
Reconfigurer l''authentification en mode NTML : Pas résolu
Downgrader IE 9 vers IE 8 : Pas résolu
Vérification du cablage (cable & switch) : Pas résolu

Réinstallation des pilotes réseaux : OK',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,2588,2588,2572,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (47,0,'[MESSAGERIE] Création des E-mail address policies','2013-04-23 13:31:00','2013-04-23 16:56:33','2013-04-23 16:56:33','2013-04-23 16:56:33',23,'closed',23,1,0,'',0,'Afin de faciliter la création des messageries il faut créer des stratégies d''adresses pour l''ensemble des sociétés : 

Les stratégies permettront de créer automatiquement les adresses mails en fonction de la société.

- EURISK (déjà fait)
- CPCOM (déjà fait)
- EUREXO (déjà fait)
- AUTEXIA (OK)
- ADAPTHOME (OK)
- AMOERTEX (OK)
- AUTOPROGEST (OK)
- CIFEX (n''est plus utilisé)
- CYNDEXIA (n''est plus utilisé)
- EKWI (OK)
- ENGIMO (OK)
- HABISCAN (OK)
- PASQUALINI (n''est plus utilisé)
- PASSIMMO (OK)
- PRUNAYSERVICES (OK)
- PRUNEANCE (OK)
- QANTEX (OK)
- SERISK (OK)
- SIMCA-ASSURANCES (OK)
- W2R (OK)
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,12333,12333,460,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (48,11,'impossible de connecter le poste 210106 avec l''ACP','2013-04-23 14:33:00','2013-05-07 08:43:09','2013-04-26 10:26:42','2013-05-07 08:43:09',24,'closed',24,3,0,'',0,'Le poste 210106 n''arrive pas à ce connecter avec l''ACP',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-26 10:26:42',0,112074,298935,2748,107,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (49,0,'Vérifier le service de temps sur le DC w2008-3130-03','2013-04-23 17:33:00','2013-05-14 08:51:36','2013-05-03 10:46:43','2013-05-14 08:51:36',150,'closed',23,1,0,'',0,'Il semblerait que le service de temps soit mal configuré sur le serveur DC PRUNAY-EXT.
A vérifier !

Les commandes net time / w32tm :
http://wiki/doku.php?id=microsoft',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-03 10:46:43',0,0,616716,321223,104,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (50,0,'[KASPERSKY] Base corrompu sur le w2008-3120-114','2013-04-24 13:26:00','2013-05-24 09:26:27','2013-05-13 09:44:13','2013-05-24 09:26:27',23,'closed',23,1,0,'',0,'Le serveur remonte en erreur dans la console d''administration.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-13 09:44:13',0,0,936027,548293,117,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (51,1,'Poste 301121 ne fonctionne plus EURISK CRETEIL','2013-04-24 15:18:00','2013-05-06 08:31:12','2013-04-25 17:17:24','2013-05-06 08:31:12',24,'closed',24,3,0,'',0,'Obligé de brancher l''ITIUM en direct sans passer par le poste 6731i de Mme MOINAULT anne pour qu''il refonctionne .',4,3,4,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-25 17:17:24',0,49557,271635,807,350,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (52,0,'Wifi chateau pas d''accès à certain site','2013-04-24 17:11:00','2013-05-06 08:31:12','2013-04-24 17:17:19','2013-05-06 08:31:12',6,'closed',6,1,0,'',0,'"L’accès est (actuellement) limité au Web." visiblement c’est un peu compliqué -&gt; j’ai accès à certains sites (http://www.google.com) et pas d’autres (https://www.google.com/calendar?tab=wc). Il me semble que j’arrive depuis l’extérieur (donc accès internet) à synchroniser avec le serveur exchange : donc tout cela est bizarre (il doit y avoir un filtrage quelque part ?!)',3,3,3,7,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-24 17:17:19',0,0,314412,379,77,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (53,0,'[MIB] Problème d''encodage lors d''import','2013-04-25 11:09:00','2013-05-17 09:01:25','2013-05-06 17:36:45','2013-05-17 09:01:25',23,'closed',23,1,0,'',0,'

De : CPCOM/CHOLLET Ludovic 
Envoyé : jeudi 25 avril 2013 10:42
À : ''Fabrice Saint Blancat''
Objet : RE: MailInBlack

Bonjour, 

Merci pour cette réponse rapide.
Avez-vous une estimation sur le délai avant de migrer vers cette nouvelle version ?

Cordialement,

Ludovic CHOLLET

De : Fabrice Saint Blancat [mailto:FSaintBlancat@groupe-synox.com] 
Envoyé : jeudi 25 avril 2013 10:34
À : CPCOM/CHOLLET Ludovic
Objet : RE: MailInBlack

Bonjour,
 
Effectivement, nous avons déjà rencontré ce problème avec le Support MIB lors du dernier import fait avec Christophe.
 
Cela sera rectifié dans leur nouvelle version. Pour l’instant, il faut importer sans accent et modifier par la suite.
 
Cdt
 
  
 
 Synox s’engage pour l’ENVIRONNEMENT: n''imprimer que si nécessaire
 
 
De : CPCOM/CHOLLET Ludovic [mailto:Ludovic.CHOLLET@groupeprunay-si.fr] 
Envoyé : jeudi 25 avril 2013 10:31
À : Support Synox
Objet : MailInBlack
 
Bonjour, 
 
Nous rencontrons un problème d’encodage pour importer des utilisateurs dans MIB.
Dès qu’un accent ou caractère spécial est présent dans le fichier d’import les utilisateurs s’importent mal ou pas du tout.
 
Nous avons essayé avec différent type d’encodage (Unicode, UTF-8, ASCII, UTF32…) mais nous rencontrons toujours le même problème.
Impossible donc, d’utiliser la fonction d ‘import correctement.
 
Ci-joint un exemple de fichier en format UTF8.
Avez-vous une idée sur le problème ?
 
Société : CPCOM
 
Cordialement,
 
Ludovic CHOLLET
Service Informatique
GROUPE-PRUNAY
05 61 39 96 66
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-06 17:36:45',0,56977,626568,268688,40,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (54,8,'Modifier la temporisation de débordement','2013-04-25 11:59:00','2013-05-06 08:31:12','2013-04-25 15:09:04','2013-05-06 08:31:12',24,'closed',24,2,0,'',0,'
 
Bonjour Patrice,

Lorsque le téléphone d’un collaborateur sonne et que ce dernier ne répond pas, le renvoi est activé (généralement au standard) au bout de 6 sonneries (à la 7ème sonnerie).

Nous souhaitons que le renvoi soit dorénavant activé après 3 sonneries (soit lors de la 4ème).

Ce paramètre est un paramètre de l’application. Ne sais pas où cela se trouve (a priori pas dans l’ACP car c’est un paramètre général).

Pour information, au départ il avait été placé à 3, nous l’avions passé à 6. Nous revenons donc à la position de départ.

Cordialement
 
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-25 15:09:04',0,0,289932,11404,146,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (55,10,'Statistiques impression Eurexo','2013-04-25 16:41:00',null,null,'2013-05-29 11:04:15',23,'assign',6,2,0,'',0,'
Ludovic, Stéphane,

nous avons une demande de la part de Joel Le Rouzic pour la création d''un tableau de bord des impressions réalisées par site Eurexo depuis les serveurs de plateforme.

Merci d''étudier la possibilité de créer un tel tableau de bord.

Jean-Hugues',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,732,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (56,0,'[SAUVEGARDE] Engimo : Erreur régulière sur w2003-782-01','2013-04-25 18:24:00','2013-05-17 09:01:25','2013-05-06 09:39:06','2013-05-17 09:01:25',23,'closed',23,1,0,'',0,'Dépassement de la capacité (disque sur de 500Go et lecteur de sauvegarde 400Go).',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-06 09:39:06',0,0,657445,270906,73,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (57,0,'[NAGIOS] Change nas-312-34 a supprimer','2013-04-25 18:25:00','2013-05-13 09:46:36','2013-05-02 17:14:57','2013-05-13 09:46:36',23,'closed',23,1,0,'',0,'Il est remplacé par nas-312-35',3,3,3,6,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-02 17:14:57',0,0,487296,211797,78,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (58,0,'[SAUVEGARDE] Serisk ajout d''un nouveau disque dur en rotation de backup','2013-04-25 18:26:00',null,null,'2013-05-02 15:24:17',23,'waiting',23,1,0,'',0,'

De : CPCOM/Support 
Envoyé : jeudi 25 avril 2013 17:19
À : CPCOM/CATALLO François-Xavier; CPCOM/CHOLLET Ludovic
Cc : CPCOM/ROUGE Brice
Objet : TR: URGENT - RE: serisk - nouveau poste

FX,
Peux-tu répondre à la demande d’Eric Fauconnier ?
« Enfin il convient de faire un point complet sur les licences Office dispo sur le parc SERISK »


Ludo :
« formater un nouveau disque de sauvegarde sur le serveur (suite à l''intervention de la semaine dernière) »
Suite à Restauration d’un répertoire
Mise en sécurité du disque dur de sauvegarde.
Eric à ramené son disque de sauvegarde usb 500g de Montpellier pour remplacer le disque dur mis en sécurité
Il souhaite le formater pour l’inclure dans le roulement de sauvegarde du serveur

',3,3,3,1,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-02 15:24:17',0,0,0,0,137,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (59,0,'Mise en service Lien BVPN 20M Labege','2013-04-26 09:32:00',null,null,'2013-06-18 17:11:53',154,'waiting',6,2,0,'',0,'Pascal,

merci de planifier avec Orange la mise en service du lien 20M installé hier sur le site de Labège.
Les contacts Orange sont :

RSC : Claudine DELPY, 05 62 30 74 23, claudine.delpy@orange.com
                      06 88 06 35 36
BVPN : Michel Alain Martin, 05 62 30 75 82, mimartin.ext@orange.com

Jean-Hugues',3,4,4,3,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-18 17:11:53',0,0,0,0,975,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (60,0,'mise à jour de l''ACS','2013-04-26 10:27:00','2013-05-07 08:43:09','2013-04-26 10:34:57','2013-05-07 08:43:09',24,'closed',24,1,0,'',0,'Mise à jour cette nuit de l''ACS en R5.4 SP1 B805 pour amèliorer les blocages du serveur SIP.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-26 10:34:57',0,0,296169,477,157,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (61,26,'Wifi Public Siège','2013-04-26 14:23:00','2013-05-29 09:58:48','2013-05-17 10:36:51','2013-05-29 09:58:48',150,'closed',6,2,0,'',0,'Christophe,

merci de regarder le problème de Christophe Maroni sur le wifi public du siège.

Jean-Hugues

_________________________________________________________________________________

Bonjour,

Est-ce normal que lorsque je suis connecté au Wifi du Château je ne puisse pas passer en VPN ?
(essai NOK suite au test de bon fonctionnement du VPN sur le poste en liaison RJ45)

Cordialement,

C.M.

',3,2,2,7,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-17 10:36:51',0,0,977748,634431,136,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (62,0,'Demande de renouvellement de mot de passe support HermesD','2013-04-29 14:35:00','2013-05-10 09:03:56','2013-04-29 14:40:31','2013-05-10 09:03:56',154,'closed',154,1,0,'',0,'A la connexion en SSH en tant que support sur hermes dommage, le renouvellement du mot de passe est demandé.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-04-29 14:40:31',0,0,368936,331,115,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (63,8,'création du poste de Mr RAFFY','2013-04-29 15:04:00','2013-05-13 09:46:36','2013-05-02 16:28:50','2013-05-13 09:46:36',24,'closed',24,2,0,'',0,'•	P.Pruvost
o	Expédier un poste AASTRA 6731i
o	Paramétrer M.Raffy tel que ci-joint
o	Attribuer une licence TWP Caller & Alerter
o	Point d’attention : cette personne aura un n°SDA
',3,3,3,0,1,0.0000,0.0000,0.0000,0,'&lt;p class="MsoNormal" style="margin: 0cm 0cm 0pt;"&gt;&lt;span style="font-family: ''Arial'',''sans-serif''; font-size: 10pt; mso-fareast-font-family: ''Times New Roman'';"&gt;Monsieur Pruvost,&lt;/span&gt;&lt;/p&gt;
&lt;p class="MsoNormal" style="margin: 0cm 0cm 0pt;"&gt;&lt;span style="mso-fareast-font-family: ''Times New Roman'';"&gt;&lt;span style="font-size: medium;"&gt;&lt;span style="font-family: Times New Roman;"&gt;&#160;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;
&lt;p class="MsoNormal" style="margin: 0cm 0cm 0pt;"&gt;&lt;span style="font-family: ''Arial'',''sans-serif''; font-size: 10pt; mso-fareast-font-family: ''Times New Roman'';"&gt;Voici donc le N&#176; suppl&#233;mentaire &#224; impl&#233;menter dans votre PABX : 0130784195 au poste (299)218 ; ceci concerne le site Louveciennes.&lt;/span&gt;&lt;/p&gt;
&lt;p class="MsoNormal" style="margin: 0cm 0cm 0pt;"&gt;&lt;span style="mso-fareast-font-family: ''Times New Roman'';"&gt;&lt;span style="font-size: medium;"&gt;&lt;span style="font-family: Times New Roman;"&gt;&#160;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;
&lt;p class="MsoNormal" style="margin: 0cm 0cm 0pt;"&gt;&lt;span style="font-family: ''Arial'',''sans-serif''; font-size: 10pt; mso-fareast-font-family: ''Times New Roman'';"&gt;Le n&#233;cessaire est fait dans la plateforme BTIP.&lt;/span&gt;&lt;/p&gt;
&lt;p class="MsoNormal" style="margin: 0cm 0cm 0pt;"&gt;&lt;span style="mso-fareast-font-family: ''Times New Roman'';"&gt;&lt;span style="font-size: medium;"&gt;&lt;span style="font-family: Times New Roman;"&gt;&#160;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;
&lt;p class="MsoNormal" style="margin: 0cm 0cm 0pt;"&gt;&lt;span style="font-family: ''Arial'',''sans-serif''; font-size: 10pt; mso-fareast-font-family: ''Times New Roman'';"&gt;Bonne fin de journ&#233;e.&lt;/span&gt;&lt;/p&gt;
&lt;p class="MsoNormal" style="margin: 0cm 0cm 0pt;"&gt;&lt;span style="mso-fareast-font-family: ''Times New Roman'';"&gt;&lt;span style="font-size: medium;"&gt;&lt;span style="font-family: Times New Roman;"&gt;&#160;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;
&lt;p class="MsoNormal" style="margin: 0cm 0cm 0pt;"&gt;&lt;span style="font-family: ''Arial'',''sans-serif''; font-size: 10pt; mso-fareast-font-family: ''Times New Roman'';"&gt;Cordialement.&lt;/span&gt;&lt;/p&gt;','none',0,0,null,'2013-05-02 16:28:50',0,133110,279846,1580,98,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (64,0,'Demande d''accès en RW à la base Redmine.','2013-05-02 11:01:00','2013-06-04 08:35:57','2013-05-24 08:01:10','2013-06-04 08:35:57',154,'closed',154,1,0,'',0,'La demande est similaire à celle concernant l''accès à la base TRACKIT.
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'accepted',0,0,null,'2013-05-24 08:01:10',0,679772,305125,638,172,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (65,8,'statistiques téléphonique sur NEMOURS','2013-05-02 12:03:00','2013-05-13 09:46:36','2013-05-02 15:30:26','2013-05-13 09:46:36',24,'closed',24,1,0,'',0,'Bonjour,

Nous avons eu nos premiers résultats de Supervision sur les postes de l''Agence et il en ressort que notre taux de "Décroché" n''est pas bon.
En moyenne, 20 % des interlocuteurs raccrochent.

Après analyse détaillée de ces tableaux, on constate que deux assistantes (Gabrielle OSAER et Patricia LEPAGE) ont un "Décroché" à 0 depuis le 15/04/2013, ce qui me paraît anormal.

Gabrielle et Patricia me confirment que cette date coïncide avec le changement de leurs PC respectifs, et que effectivement :

- Elles ne reçoivent plus les appels en débordement sur la file d''attente,
- Seules les communications sur ligne directe leur parviennent,
- Elles n''ont plus de message leur indiquant que leur poste est supervisé.

Merci d''avance d''étudier le problème pour y remédier.

Cordialement, 
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-02 15:30:26',0,0,294216,12446,118,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (66,0,'[RESEAU] Lenteur sur le site de Clermont EURISK','2013-05-02 14:19:00','2013-06-14 08:47:29','2013-06-03 11:26:52','2013-06-14 08:47:29',23,'closed',23,1,0,'',0,'Le site est sur le routeur de secours.

J''ai ouvert un ticket Orange : 
Lors du déménagement le routeur Nominal n''a pas redémarré. Etienne a contacté Orange et ils doivent le remplacer dans la semaine.
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-03 11:26:52',0,898802,420507,41270,158,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (67,25,'[SAUVEGARDE] CpCom Restauration w2003-312-37','2013-05-02 14:21:00','2013-05-13 09:46:36','2013-05-02 14:26:47','2013-05-13 09:46:36',23,'closed',23,1,0,'',0,'

De : CPCOM/BOUREZ Vincent 
Envoyé : mercredi 1 mai 2013 23:33
À : CPCOM/CHOLLET Ludovic
Objet : sauvegarde dev
Importance : Haute

Ludovic,

Sur le serveur 10.31.2.37, j''aurais besoin de récupérer les deux fichiers suivants à la date du 28/04/2013 (ou avant) :

c:\\Progi\\bdd\\solid\\Sources2\\backup\\?????.log
c:\\Progi\\bdd\\solid\\Sources2\\backup\\solid.db

Merci

--
Vincent Bourez
Service Informatique
Groupe Prunay
05 61 39 08 31
06 75 67 54 60

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-02 14:26:47',0,0,285936,347,307,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (68,0,'[VMWARE] Module DevicePowerOn power on failed','2013-05-02 15:20:00','2013-05-13 09:46:36','2013-05-02 15:22:41','2013-05-13 09:46:36',23,'closed',23,1,0,'',0,'Problème pour démarrer des VM après une migration de Workstation vers un hotes ESX 5.0.
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-02 15:22:41',0,0,282396,161,125,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (69,0,'[GPO] Ajout du site acp.groupe-prunay.fr en site de confiance','2013-05-02 16:17:00',null,null,'2013-06-19 13:59:01',23,'assign',23,1,0,'',0,'Impossible d''ajouter le site directement dans la liste des sites de confiance : risque de perdre les sites ajouté manuellement par utilisateur.

Tester la modification de la GPO avec ajout du site en site de confiance en passant par les préférences IE8.
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,179,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (70,8,'problème de reprise d''un appel transféré','2013-05-03 11:43:00',null,null,'2013-05-03 13:30:37',24,'waiting',24,2,0,'',0,'Bonjour,

Jessie (214307) me signale l''incident suivant qui s''est produit plusieurs fois (hier et aujourd''hui notamment) :

Ce matin, elle a un interlocuteur en ligne, elle veut passer la communication à Nathalie (214772) qui a fait un transfert sur Gabrielle (214776) puisqu''elle a quitté momentanément son poste.
Gabrielle répond. Jessie veut reprendre l''interlocuteur pour prendre le message, mais impossible de récupérer ...
Elle ne peut que raccrocher.

Merci d''avance de votre intervention.

Très cordialement, 
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-03 13:30:37',0,0,0,0,105,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (71,10,'Mise à jour script STAT pour ajout d''un etat','2013-05-03 15:44:00','2013-05-14 08:51:36','2013-05-03 15:49:59','2013-05-14 08:51:36',154,'closed',154,1,0,'',0,'Suite à modification d''un script pour enrichissement d''une table d''états, demande d''ajout d''un état ''RUNNING'' en début de procédure.',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-03 15:49:59',0,0,277656,359,134,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (72,0,'Services Exchange indisponibles','2013-05-06 08:14:00','2013-05-24 09:26:27','2013-05-13 11:22:47','2013-05-24 09:26:27',150,'closed',154,1,0,'',0,'Pb de connexion IMAP
Pas de service OWA

Et erreur dans les logs.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-13 11:22:47',0,0,609147,227327,143,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (73,8,'Coupure inter-site + lenteur internet','2013-05-06 09:51:00','2013-05-21 09:31:50','2013-05-07 11:52:15','2013-05-21 09:31:50',24,'closed',24,3,0,'',0,'le site de NEMOURS , LOUCIENNES , LEVALLOIS ont des coupures inter-site téléphonique + des lenteurs internet',4,3,4,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-07 11:52:15',0,0,474050,50475,178,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (74,0,'Extend /home hermesD','2013-05-06 12:21:00','2013-05-17 09:01:25','2013-05-06 18:00:08','2013-05-17 09:01:25',154,'closed',154,1,0,'',0,'Ajout d''un disque 500g au /home de la VM Orange Business Service (Flexible Computing)
HERMES DOMMAGE
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-06 18:00:08',0,0,376825,20348,104,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (75,0,'[VMWARE] Migration du serveur Qantex/Adapthome','2013-05-06 15:50:00',null,null,'2013-05-06 15:52:59',23,'assign',23,1,0,'',0,'Migration sur la nouvelle infra. Changement d''adresse IP / Hostname. Reconfiguration du service Citrix.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,179,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (76,10,'Lenteur application WEB Jaspersoft','2013-05-13 10:09:00','2013-05-24 09:26:27','2013-05-13 10:40:20','2013-05-24 09:26:27',154,'closed',154,1,0,'',0,'L''application JasperSoft (application web) est actuellement victime de lenteurs inhabituelles (problème de process Java ?)',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-13 10:40:20',0,0,386247,1880,92,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (77,8,'problème sur l''ACP','2013-05-13 17:20:00',null,null,'2013-05-15 16:16:05',6,'waiting',24,1,0,'Software',9,'plus d''ACP sur les postes de tous les sites suite à une perte de licence du serveur ACP',5,3,4,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-14 11:20:06',0,4563,0,0,962,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (78,8,'plus d''affichage de nom sur les poste','2013-05-14 10:31:00','2013-05-27 09:28:09','2013-05-14 10:34:07','2013-05-27 09:28:09',24,'closed',24,3,0,'',0,'les noms ne s''affiche plus sur les postes',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-14 10:34:07',0,0,385029,187,89,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (79,8,'numéro de GSM des experts à injecter dans l''ACS','2013-05-15 11:21:00','2013-05-27 09:28:09','2013-05-15 12:13:44','2013-05-27 09:28:09',24,'closed',24,2,0,'',0,'Patrice,

Comme convenu, ci-joint la liste des portables des experts à injecter dans les annuaires.
•	311 numéros
•	N° de téléphone en colonne H
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-15 12:13:44',0,0,338829,3164,72,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (80,0,'[FIREWALL] Autoriser l''hote 10.31.20.200 sur le port 22 du firewall','2013-05-15 17:15:00','2013-05-27 09:28:09','2013-05-15 17:23:10','2013-05-27 09:28:09',154,'closed',23,1,0,'',0,'Depuis la migration NAGIOS vers la nouvelle infra, et donc un changement d''IP (10.31.2.175 vers 10.31.20.200) je n''arrive plus a me connecter en ssh sur la machine 10.31.20.11 à partir du serveur NAGIOS.

Peux tu autoriser le trafic sur ce port pour NAGIOS ?

Merci,

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-15 17:23:10',0,0,317589,490,154,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (81,25,'[NAGIOS] : Demande de supervision','2013-05-16 16:18:00','2013-06-14 08:47:29','2013-06-03 11:25:41','2013-06-14 08:47:29',23,'closed',6,4,0,'',0,'Ludovic,

Suite à l''installation des équipements Cisco sur le site Eurexo  de Limonest, merci d''intégrer à nagios les équipements suivants :

Les swtichs : SW-693-15, SW-693-16, SW-693-17
La borne wifi : AP-693-14
Les Serveurs : 10.69.3.81 et 10.69.3.82

Les mots de passe pour les serveurs sont dans KeePass.

Merci,
Jean-Hugues',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,'2013-05-17 17:00:00','2013-06-03 11:25:41',0,0,880169,500861,270,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (82,8,'Création ACP poste de Mme BOSSE','2013-05-17 08:35:00','2013-05-29 09:58:48','2013-05-17 08:56:09','2013-05-29 09:58:48',24,'closed',24,2,0,'',0,'Bonsoir,

Merci de finaliser le déploiement de l’ACP pour Mme C.BOSSE (qui a remplacé Mme Chretien).

Elle se connecte avec son login et sur sa session elle n’a probablement pas l’icône ACP. De plus, sa session n’est probablement pas autorisée à utiliser ACP.

Reste à votre écoute.

Cordialement
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-17 08:56:09',0,0,350628,1269,120,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (83,0,'[NAGIOS] Faux positif toutes les semaines (dimanche à midi)','2013-05-21 10:43:00',null,null,'2013-05-21 10:44:31',23,'assign',23,1,0,'',0,'Tous les serveurs LINUX du range 10.31.20.* et 10.31.2.* remonte en erreur (timeout) tous les dimanches à midi...',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,91,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (84,0,'Déclaration de photocopieur Limonest pour envoi de Mail','2013-05-21 11:02:00','2013-06-03 09:02:01','2013-05-22 11:45:43','2013-06-03 09:02:01',150,'closed',150,1,0,'',0,'Bonjour,

Merci de déclarer sur le serveur Exchange les adresse Ip suivante pour l''envoi de mail
10.69.3.23 et 10.69.3.26

Merci
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-22 11:45:43',0,0,381601,45823,246,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (85,0,'Demande de désactivation de comptes VPN','2013-05-21 11:46:00','2013-06-03 09:02:01','2013-05-21 11:51:58','2013-06-03 09:02:01',154,'closed',154,1,0,'',0,'Bonjour,

Pouvez-vous désactiver ces comptes vpn suite à l''arrêt de la mission Efficash (demande en pièce jointe):



Bernadette BUFFA

EFFICASH

plateforme.vpn084

YeopKuy2

Paul Favre-Verand

EFFICASH

plateforme.vpn085

PaotRao8





Merci,

Bonne journée



Cyril Trouvé

Support Informatique

------------------------------

Description : Description : cid:image001.jpg@01CC68AE.2171DC10

Tél : 05 61 39 96 44 - Fax : 05 61 00 59 56







			pièce jointe message de courriel (RE: Prestataire Efficash - Ouverture d''accès à nos systèmes ARDI)

	De: 	Groupe-Prunay/AZOULAY Jean-Jacques &lt;jj.azoulay@groupeprunay.com&gt;
À: 	CPCOM/TROUVE Cyril &lt;cyril.trouve@groupeprunay-si.fr&gt;
Cc: 	CPCOM/CHOLLET Ludovic &lt;Ludovic.CHOLLET@groupeprunay-si.fr&gt;, CPCOM/ROUGE Brice &lt;Brice.ROUGE@groupeprunay-si.fr&gt;, Eurexo Direction/SINQUIN Thierry &lt;T.SINQUIN@eurexo.fr&gt;, Eurexo Direction/LE ROUZIC Joël &lt;J.LEROUZIC@eurexo.fr&gt;
Sujet: 	RE: Prestataire Efficash - Ouverture d''accès à nos systèmes ARDI
Date: 	Wed, 15 May 2013 19:18:15 +0200
        
        
        Vous pouvez couper à partir de lundi prochain.
        
        J''informe Efficash que l''accès sera suspendu à partir du 20 mai.
        
        
        
        merci
        
        
        
        
        
        
        
        Cordialement,
        
        
        
        Jean-Jacques AZOULAY
        
        Directeur Administratif et Financier
        
        
        
        19, chemin de Prunay - BP24
        
        78431 Louveciennes cedex
        
        tél : 01 30 78 18 50
        
        mob : 06 23 39 41 98',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-21 11:51:58',0,0,378961,358,101,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (86,0,'[VPN] Créer un nouveau rôle pour les utilisateurs de téléphonie','2013-05-21 14:39:00','2013-06-03 09:02:01','2013-05-23 15:43:35','2013-06-03 09:02:01',154,'closed',23,1,0,'',0,'Suite a une demande d''accès depuis le VPN à la téléphonie j''ai :
-  ajouté dans la stratégie ACL : Pool Infra EUREXO les lignes suivantes :
            tcp://10.131.20.150:5060
            udp://10.131.20.150:5060
-  créé une stratégie de Split Tuneling appliqué sur le rôle EUREXO contenant :
            10.131.20.0/24

Afin de sécurisé un peu plus, peut on remplacer le rôle EUREXO par un nouveau rôle contenant les utilisateurs amené a faire de la téléphonie à travers le VPN ?!
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-23 15:43:35',0,0,368581,90275,329,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (87,0,'[HARDWARE] Surventilation w2003-741-01','2013-05-21 15:41:00','2013-06-07 09:06:26','2013-05-27 16:44:19','2013-06-07 09:06:26',23,'closed',23,1,0,'',0,'

De : CPCOM/CHOLLET Ludovic 
Envoyé : mardi 21 mai 2013 15:26
À : ''Ferchichi, Mohamed Saber''
Objet : RE: surventillation ML350 G5

Bonjour, 

Le serveur se trouve à Annecy soit 700 Km de nos locaux.
Nous avons contracté cette garantie justement pour ne pas avoir à se rendre sur site.

Habituellement l’envoi du technicien est systématique de votre part, même pour changer un disque dur. Ce refus est une première.
Il s’agit pourtant  d’ouvrir le serveur pour manipuler la carte mère, les secrétaires sur place ne peuvent pas le faire.

Pouvez-vous donc me confirmer que nous devons prendre à notre charge les frais de déplacement et d’intervention dans le but de réaliser les tests malgré notre contrat de garantie?

Cordialement,

Ludovic CHOLLET
Service Informatique
GROUPE-PRUNAY

De : Ferchichi, Mohamed Saber [mailto:mferchichi@hp.com] 
Envoyé : mardi 21 mai 2013 15:08
À : CPCOM/CHOLLET Ludovic
Objet : RE: surventillation ML350 G5

 
 
Bonjour Mr Chollet,
 
Merci pour votre retour,
Je craind que ça serait impossible d’envoyer un technicien pour faire les tests
Le technicien ne se deplacera que pour remplacer les pieces 
 
Au plaisir de pouvoir répondre à vos attentes,
Cordialement,
 
 
Mohamed Saber FERCHICHI
Support engineer
Industry Standard Server

saber@hp.com
Tel : +33 157323999
Hewlett-Packard Company
Technopôle Elgazala Lot 045 
2088 Ariana
Tunisia

 

Please print thoughtfully
 
From: CPCOM/CHOLLET Ludovic [mailto:Ludovic.CHOLLET@groupeprunay-si.fr] 
Sent: mardi 21 mai 2013 15:02
To: Ferchichi, Mohamed Saber; Ferchichi, Mohamed Saber
Subject: RE: surventillation ML350 G5
 
Bonjour, 
 
J’ai bien reçu la procédure, cependant il m’est impossible de m’en charger car le serveur n’est pas dans nos locaux.
Vous est-il possible d’envoyer un technicien pour effectuer ces manipulations ?
 
Il faudra planifier l’intervention avec moi pour anticiper l’interruption de service.
 
Bien cordialement,
 
Ludovic CHOLLET
Service Informatique
GROUPE-PRUNAY
05 61 39 96 66
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-27 16:44:19',0,0,537926,176599,63,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (88,0,'[SERVEUR] Configuration ILO w2003-741-01','2013-05-21 16:45:00','2013-06-03 09:02:01','2013-05-21 16:45:00','2013-06-03 09:02:01',23,'closed',23,1,0,'',0,'J''ai reconfiguré la carte ILO sur l''ip 10.74.1.250 du nouveau serveur.

Test : OK',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,361021,0,90,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (89,0,'[GESTISOFT] Archivage / Compactage','2013-05-21 16:46:00',null,null,'2013-06-03 15:27:52',23,'assign',23,1,0,'',0,'

De : CPCOM/DELCOURT Jean-Hugues 
Envoyé : mardi 7 mai 2013 09:05
À : CPCOM/CHOLLET Ludovic
Objet : RE: Problème d''espace disque sur le Partage ENGIMO

Ludovic,

Ok pour que tu t’en occupes avec le support Gestisoft.

Jean-Hugues

De : CPCOM/CHOLLET Ludovic 
Envoyé : lundi 6 mai 2013 17:29
À : CPCOM/DELCOURT Jean-Hugues
Objet : TR: Problème d''espace disque sur le Partage ENGIMO

Jean-Hugues, 

Suite à des erreurs de sauvegarde liées à la saturation du disque sur le serveur ENGIMO (DC) j’ai contacté Sylvain DADURE afin de trouver ou faire de la place. Nous avons fait un peu de ménage, cependant vu la consommation croissante annuel en espace disque il s’interroge sur les possibilités d’évolution.

Nous ne pouvons pas agrandir le volume de données car la sauvegarde est déjà au maximum de ses capacités.

Par contre leur logiciel métier (Gestisoft) utilise presque 300Go sur 400 . Je pense qu’il faudrait voir avec leur support s’il est possible d’archiver ou compresser les vieux dossiers, compacter la base etc…

Je peux m’en occuper si tu es OK.

Ludovic

De : Engimo/DADURE Sylvain 
Envoyé : vendredi 3 mai 2013 10:47
À : CPCOM/CHOLLET Ludovic
Cc : Engimo/BERNIER-RIOU Nicole
Objet : RE: Problème d''espace disque sur le Partage ENGIMO

Bonjour Ludovic

Comme suite à notre conversation, je te confirme que tu peux procéder dès à présent à la suppression pure et simple du répertoire personnel « Engimo11 » et de son contenu.
Les données concernées ont depuis longtemps fait l’objet de backup sur d’autres supports par Jérome Brazilier, qui est maintenant expert partenaire.

Bon WE
Cordialement

Sylvain Dadure
EngimoINTERNATIONAL
Tél. : +33 1 30 78 18 56
www.engimo.fr
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,226,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (90,0,'Déclaration de FAX/Photocopieur Eurexo Toulouse pour envoi de Mail','2013-05-23 11:03:00','2013-06-04 08:35:57','2013-05-24 14:22:19','2013-06-04 08:35:57',150,'closed',150,1,0,'',0,'
Bonjour,

Merci de déclarer sur le serveur Exchange les adresse Ip suivante pour l''envoi de mail
10.31.7.24

Merci',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-24 14:22:19',0,0,336777,55159,149,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (91,19,'[NAGIOS] Superviser la temperature sur le serveur et switch à Douzy','2013-05-23 13:48:00','2013-06-03 09:02:01','2013-05-23 13:51:38','2013-06-03 09:02:01',23,'closed',23,6,0,'',0,'De : CPCOM/DELCOURT Jean-Hugues 
Envoyé : mercredi 22 mai 2013 11:32
À : CPCOM/CHOLLET Ludovic
Cc : CPCOM/MARONI Christophe; CPCOM/CATALLO François-Xavier; Eurexo Douzy/RANVIAL Kenny
Objet : TR: Température salle serveur Douzi

Ludovic,

Merci de mettre cette surveillance de température en place sur Nagios, tu peux aussi regarder du côté des switchs Alcatel si besoin.

Cordialement,

Jean-Hugues DELCOURT
Service Informatique Groupe Prunay
Responsable Pôle Infrastructures

Tel : 05 61 39 70 37
Port : 06 15 02 20 80

De : CPCOM/MARONI Christophe 
Envoyé : mercredi 22 mai 2013 09:38
À : CPCOM/DELCOURT Jean-Hugues
Cc : Eurexo Douzy/RANVIAL Kenny; CPCOM/CATALLO François-Xavier
Objet : Température salle serveur Douzi

Jean-Hugues,

Est-ce que le serveur qui est dans la salle informatique de douzi dispose d’une sonde de température (ou peut-être l’onduleur téléphonie) ce qui nous permettrait de voir les variations (à défaut de connaitre réellement la température de la pièce ?)
En effet, une surveillance est nécessaire, même si le serveur héberge actuellement peu de services.

(NB : La solution provisoire temporaire dont j’ai parlé sur place serait de louer un climatiseur (type kiloutou) en ayant pris soin précédemment de percer la dalle afin de pouvoir mettre le condenseur en extérieur : Kenny doit regarder la faisabilité)

Christophe MARONI
GROUPE PRUNAY
Directeur des Systèmes d’Information Groupe
GSM 06 78 74 31 97
christophe.maroni@groupeprunay-si.fr
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-23 13:51:38',0,0,285241,218,164,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (92,0,'Problème de synchronisation Track-It avec AD','2013-05-23 13:47:00',null,'2013-07-03 14:46:27','2013-07-03 14:46:27',150,'solved',150,1,0,'',0,'Bonjour,

Nous rencontrons un problème de synchronisation entre Track-It et l''AD

Pouvez vous regardez ?

Merci',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-03 14:46:27',0,0,0,1256367,277,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (93,0,'[NAGIOS] Ajout de l''authentification par LDAP','2013-05-23 13:54:00','2013-06-03 09:02:01','2013-05-23 13:54:00','2013-06-03 09:02:01',23,'closed',23,1,0,'',0,'J''ai supprimé l''authentification local de NAGIOS.
Apache se connecte maintenant au LDAP 10.31.20.49 et autorise les utilisateurs présent dans l''OU CPCOM.

Les droits sur nagios sont gérés dans le fichier cgi.cgi
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,284881,0,561,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (94,25,'Pb de connexion SVN depuis Kastor','2013-05-23 14:20:00','2013-06-17 09:04:51','2013-06-04 08:37:28','2013-06-17 09:04:51',154,'closed',154,1,0,'',0,'svn: Signal reçu
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-04 08:37:28',0,0,715491,325048,64,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (95,0,'Remplacement Routeur CPCOM','2013-05-23 15:25:00','2013-06-24 08:52:45','2013-06-12 14:00:52','2013-06-24 08:52:45',154,'closed',154,1,0,'',0,'Ref: [CD023J39YA4 ]

RDV lundi 27 à partir de 13h30',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 14:00:52',0,0,926865,599752,106,1320,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (96,0,'Détail de renouvellement de licence et maintenance VPN','2013-05-23 15:31:00','2013-06-17 09:04:51','2013-06-04 08:18:26','2013-06-17 09:04:51',154,'closed',154,1,0,'',0,'Prendre le détail du devis fourni
Voir comme alternative un failover avec moins de users simultanés (actuellement 250)
ou bien CPCOM  (en qualité de distibueur) en négociations avec JUNIPER (--&gt; Achats FX)',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-04 08:18:26',0,0,711231,319646,143,1860,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (97,0,'[NAGIOS] Ajout d''une fonction déloguer les sessions HTTP','2013-05-23 16:07:00','2013-06-03 09:02:01','2013-05-23 18:16:27','2013-06-03 09:02:01',23,'closed',23,1,0,'',0,'Nagios ne possède pas de fonction pour se déloguer de l''interface web.
J''ai créé un fichier php : logout.php contenant :


&lt;?php 
header(''WWW-Authenticate: Basic realm="Private Access By Invitation Only"'');
header(''HTTP/1.0 401 Unauthorized''); 
echo ''Texte affiche en cas d\\''annulation''; 
exit();
?&gt;

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-23 18:16:27',0,0,276901,7767,121,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (98,0,'Problème mail/pieces jointes','2013-05-23 16:05:00','2013-06-10 08:51:13','2013-05-30 16:28:17','2013-06-10 08:51:13',150,'closed',150,1,0,'',0,'
Bonjour, 

L’agence de Rouen par la personne de Mme. LECOUSTEY (02.32.76.27.10) , nous a signalé un problème lors de la réception de certains mails. 

En effet, ils sont censés avoir dans certains mails des pièces jointes alors que ce n’est pas le cas.
J’ai donc contacté la personne de la société qui leur envoie ces mails ( Hélène Blois 01.41.11.46.16)
Et nous avons procédé à différents tests.

Les problèmes sont les mêmes pour moi en Outlook  2010 et pour Mme. LECOUSTEY en Outlook 2003 sur la plateforme eurexo

Lors de la réception des mails, il n’est indiqué nulle part dans la messagerie qu’il y a une pièce jointe et celle-ci n’est pas visible sur notre outlook.
Cependant, le même mail sur mon adresse gmail a bien une pièce jointe qui peut être ouverte sans soucis.
Je me suis transféré le mail depuis mon adresse gmail vers ma messagerie cpcom et à ce moment-là il m’affiche bien la pièce jointe.

Cependant, si je transfère le mail sans pièce jointe de mon adresse cpcom vers mon adresse gmail, je recevrai un mail avec le fichier pdf en pièce jointe.
De plus, dans outlook web access, je n’ai pas le trombone me signalant que j’ai la pièce jointe au mail, mais je l’ai quand même voir fichier image1.

Voilà.

Etienne
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-30 16:28:17',0,0,492373,217397,584,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (99,0,'Installation VMWare tools sur w2008-312-08','2013-05-23 17:16:00','2013-06-04 08:35:57','2013-05-24 09:31:52','2013-06-04 08:35:57',150,'closed',150,1,0,'',0,'Merci d''installation VMWare tools sur w2008-312.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-24 09:31:52',0,0,314397,15352,262,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (100,29,'[OFFICE] Problème d''ouverture de document distant','2013-05-23 16:09:00','2013-06-10 08:51:13','2013-05-29 10:12:25','2013-06-10 08:51:13',23,'closed',23,1,0,'',0,'

De : CPCOM/CHOLLET Ludovic 
Envoyé : jeudi 23 mai 2013 17:47
À : Eurexo IDF-Pôle/VERNAY Jana
Objet : RE: réinstallation d''Office

Je vais opter pour la tranche de 12 à 13h !
Merci de sauvegarder votre travail en cours demain avant d’aller manger car je risque de devoir redémarrer le poste.

Si vous souhaitez me joindre par téléphone, ma ligne directe : 05 61 39 96 66.

Cordialement,

Ludovic CHOLLET

De : Eurexo IDF-Pôle/VERNAY Jana 
Envoyé : jeudi 23 mai 2013 17:12
À : CPCOM/CHOLLET Ludovic
Objet : RE: réinstallation d''Office

Bonjour,

je vous remercie pour votre proposition.
Vous pouvez travailler sur mon poste chaque jour entre 8h - 9h ou entre 12h - 13h.

J''attends vos instructions.

Je vous remercie,
Jana VERNAY

________________________________________
De : CPCOM/CHOLLET Ludovic 
Envoyé : jeudi 23 mai 2013 16:58
À : Eurexo IDF-Pôle/VERNAY Jana
Objet : RE: réinstallation d''Office

Bonjour, 

Je viens vers vous concernant un problème d’ouverture de document Excel via un partage distant.

Le ticket d’incident a été escaladé. Je vais avoir besoin de me connecter sur votre poste pour essayer d’identifier la cause du problème.
Je vous propose de m’indiquer la date et l’heure à laquelle je peux intervenir sur votre poste (1H maximum).

Bien cordialement,

Ludovic CHOLLET
Service Inforamtique
GROUPE-PRUNAY

De : CPCOM/CHOLLET Ludovic 
Envoyé : jeudi 23 mai 2013 14:55
À : CPCOM/CHOPARD Sacha
Objet : RE: réinstallation d''Office

Sacha, 

Peux-tu me donner l’adresse IP du poste en question ?

Merci,

ludo

De : CPCOM/CHOPARD Sacha 
Envoyé : jeudi 23 mai 2013 08:49
À : Eurexo IDF-Pôle/VERNAY Jana
Cc : CPCOM/CHOLLET Ludovic
Objet : réinstallation d''Office

Bonjour Jana

J’ai désinstallé puis réinstallé Office 2010 sur ton PC, mais le pb d’ouverture des fichiers sur le dossier PIDF persiste.
Nous regardons ici une autre solution possible, en attendant continue à passer par la voie de contournement que tu avais trouvé.

Je reviendrais vers toi quand on aura une solution.

merci

Cordialement, 

Sacha Chopard
  Support Informatique
     05.61.39.96.44
 

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-29 10:12:25',0,0,492133,151405,6128,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (101,0,'[WEB] Problème d''affichage depuis mappy.fr','2013-05-23 17:51:00','2013-06-03 09:02:01','2013-05-23 17:54:23','2013-06-03 09:02:01',23,'closed',23,1,0,'',0,'Les postes de nantes n''arrivent plus a ouvrir Mappy.
Site introuvable.
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-23 17:54:23',0,0,270661,203,143,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (102,0,'[SERVEUR] Installation d''imprimante TCP/IP','2013-05-23 18:29:00','2013-06-10 08:51:13','2013-05-30 14:40:37','2013-06-10 08:51:13',23,'closed',23,1,0,'',0,'Ajout des photocopieurs sur les serveurs TSE de limonest.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-30 14:40:37',0,0,483733,202297,61,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (103,0,'[SERVEUR] Migration du serveur w2008-314-01 sur un serveur physique','2013-05-23 18:30:00',null,null,'2013-06-18 11:33:49',23,'assign',23,0,0,'',0,'V2P',3,3,3,1,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,165,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (104,0,'[SWITCH] Configuration Cisco','2013-05-23 18:32:00','2013-06-10 08:51:13','2013-05-29 10:10:24','2013-06-10 08:51:13',23,'closed',23,1,0,'',0,'

De : CPCOM/DELCOURT Jean-Hugues 
Envoyé : jeudi 23 mai 2013 16:57
À : CPCOM/CHOLLET Ludovic
Cc : CPCOM/CHOPARD Sacha
Objet : Configuration switchs

Ludovic,

Merci de configurer 2 switchs Cisco 2950 et de les donner à Sacha pour le 28 mai au plus tard.

Un switch pour St Etienne
Un switch pour Valence.

Jean-Hugues

',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-29 10:10:24',0,0,483553,142704,102,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (105,0,'Partages sur le NAS non accessible ','2013-05-24 09:03:00','2013-06-04 08:35:57','2013-05-24 09:28:26','2013-06-04 08:35:57',6,'closed',6,4,0,'',0,'Plus d''accès aux partage sur le NAS CPCOM',3,3,3,7,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-24 09:28:26',0,0,300777,1526,80,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (106,0,'Mib perte des attribus "Autorisation automatique en sortie" ','2013-05-24 14:25:00','2013-06-10 08:51:13','2013-05-28 15:03:25','2013-06-10 08:51:13',150,'closed',150,1,0,'',0,' l''ensemble des comptes dans MIB ont perdu l''attribut "Autorisation automatique en sortie" 

Tickets Ouvert chez Synox',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-28 15:03:25',0,0,455173,88705,299,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (107,25,'[SERVEUR] Redmine ne répond pas','2013-05-27 10:03:00','2013-06-07 09:06:26','2013-05-27 14:48:50','2013-06-07 09:06:26',23,'closed',23,1,0,'',0,'Le serveur est bloqué en RO.
Ctrl+D et reboot du système : OK',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-27 14:48:50',0,0,385406,17150,163,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (108,25,'[DATAEXCHANGER] Configurer l''accès vers Hermes Construction MYSQL','2013-05-27 10:06:00','2013-06-07 09:06:26','2013-05-27 15:08:02','2013-06-07 09:06:26',23,'closed',23,1,0,'',0,'Depuis le changement d''IP, le serveur n''a plus accès à la base MySQL Hermes EURISK.



De : CPCOM/BERTHALON David 
Envoyé : lundi 27 mai 2013 09:30
À : CPCOM/CHOLLET Ludovic; CPCOM/DELCOURT Jean-Hugues
Cc : CPCOM/BOUREZ Vincent
Objet : RE: Suite changement server Dex

Bonjour
Pascal n’étant pas là pouvez vous regarder cela, Pascal a du faire une doc sur le sujet, il faut juste autoriser la nouvelle machine Dex 10.31.20.114 à attaquer la base SQL d Hermes Construction

De : CPCOM/BERTHALON David 
Envoyé : lundi 27 mai 2013 09:26
À : CPCOM/PEREZ Pascal
Cc : CPCOM/BOUREZ Vincent
Objet : Suite changement server Dex
Importance : Haute

Pascal,
On a changé le serveur dex c est maintenant la machine 10.31.20.114 il faudrait que tu autorises ce serveur à attaquer la base mySQL sur HERMES CONSTRUCTION en prod
Tu peux t en occuper aujourd hui ? c est pour les stats qui sont envoyées à Paul Boyer





David Berthalon
Service Informatique
Groupe Prunay
Tel : 05 61 39 70 39
Tel CPCOM (223)

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-27 15:08:02',0,0,385226,18122,822,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (109,55,'[SERVEUR] Ajout d''une tâche planifié ','2013-05-27 11:11:00','2013-06-07 09:06:26','2013-05-27 11:13:03','2013-06-07 09:06:26',23,'closed',23,1,0,'',0,'

De : Eurisk Siege/LAGRANGE Patrick 
Envoyé : lundi 27 mai 2013 11:02
À : CPCOM/CHOLLET Ludovic
Cc : Eurisk Siege/DECASTRO Laetitia; Eurexo Siege/PIVAIN Corinne; Eurexo Siege/DOUKHAN Blandine; Eurexo Siege/BONNET Emilie; ''Virginie FRENEUIL''
Objet : RE: [NOTILUS] Gestion des virements

Bonjour Ludo,
 
Merci de bien vouloir mettre en place la tâche planifiée pour le lancement de ce programme tous les matins à 9h sauf w-e.
 
N:\\COMPTABILITE FOURNISSEURS\\IMPORT_QUADRA\\NDF_NOTILUS\\NOTILUS_RECUP_FTP.bat
 
Bien cordialement
LAGRANGE Patrick  patrick.lagrange@eurisk.fr
D.A.F.   EURISK SAS 
Société membre du Groupe PRUNAY
 01.30.78.18.16 / 06.12.57.55.81 /   01.30.78.18.37 
 Afin de contribuer au respect de l''environnement, merci de n''imprimer ce courriel que si nécessaire 
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-27 11:13:03',0,0,381326,123,93,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (110,25,'recherche de fichiers','2013-05-27 11:26:33','2013-06-07 09:06:26','2013-05-27 14:31:23','2013-06-07 09:06:26',23,'closed',159,1,0,'',0,'Est-ce que qq un peut nous trouver dans quels fichiers du clients QBE se trouve cette chaine de caractère ?



cd $HERMES_HOME/data/doc/DocumentChantier/docChantierUrl
find –name «028 Liste des intervenants.pdf »


Merci',4,3,4,2,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-27 14:31:23',0,0,380393,11123,11092,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (111,8,'impossible de ce connecter sur son poste 211122 avec l''ACP','2013-05-27 11:53:00','2013-06-07 09:06:26','2013-05-27 11:59:41','2013-06-07 09:06:26',24,'closed',24,3,0,'',0,'Mme LOPES de LEVALLOIS ne peut pas ce connecter sur l''ACP avec son N° de téléphone',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-27 11:59:41',0,0,378806,401,149,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (112,45,'Pluc de Wifi Siege ','2013-05-27 16:14:00','2013-06-10 08:51:13','2013-05-28 15:06:33','2013-06-10 08:51:13',150,'closed',150,4,0,'',0,'Suite Alerte Nagios, La connexion Wifi n''est plus détecté.  
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-28 15:06:33',0,0,405433,39153,211,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (113,0,'[SERVEUR] Lenteur sur le serveur 10.78.1.8','2013-05-29 10:12:00','2013-06-14 08:47:29','2013-06-03 09:43:35','2013-06-14 08:47:29',23,'closed',23,1,0,'',0,'Analyser l''origine des lenteurs pour préparer un compactage de la base.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-03 09:43:35',0,0,513329,127895,117,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (114,0,'[NAGIOS] Supprimer le supervision du matériel d''Ambérieu','2013-05-29 10:14:00','2013-06-10 08:51:13','2013-05-30 14:09:46','2013-06-10 08:51:13',23,'closed',23,1,0,'',0,'

De : CPCOM/CHOPARD Sacha 
Envoyé : mardi 28 mai 2013 14:08
À : CPCOM/CHOLLET Ludovic
Objet : serveur Ambérieu

Chollet

Je vais éteindre définitivement le serveur d’Ambérieu jeudi dans la journée.
Il faudrait l’enlever de la surveillance Naggios et de Yvette par la même occasion.

Merki

Chopard

Cordialement, 

Sacha Chopard
  Support Informatique
     05.61.39.96.44
 

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-30 14:09:46',0,0,340633,57346,71,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (115,0,'[SERVEUR] Planification d''un redémarrage w2003-271-01','2013-05-29 10:48:00','2013-06-10 08:51:13','2013-05-29 10:49:21','2013-06-10 08:51:13',23,'closed',23,1,0,'',0,'Concernant un problème avec Office, il faut redémarrer le serveur ce soir.
Planifié à 22h.',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-29 10:49:21',0,0,338593,81,68,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (116,0,'Ouverture de port','2013-05-29 11:34:00','2013-06-17 09:04:51','2013-06-04 15:21:35','2013-06-17 09:04:51',150,'closed',150,1,0,'',0,'Bonjour,

Pouvez vous ouvrir le port 3101 vers fr.srp.blackberry.com.pour la machine 10.31.4.253.

Merci

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-04 15:21:35',0,0,552651,186455,111,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (117,0,'[SERVEUR] Planification d''un redémarrage w2003-641-01','2013-05-29 14:43:00','2013-06-10 08:51:13','2013-05-29 14:45:00','2013-06-10 08:51:13',23,'closed',23,1,0,'',0,'De : CPCOM/REIX Sebastien 
Envoyé : mercredi 29 mai 2013 14:36
À : CPCOM/CHOLLET Ludovic
Objet : [42525] - Erreur de sauvegarde Eurisk PAU
Importance : Haute

Mr CHOLLET,

Pouvez-vous programmer le reboot du serveur de PAU (10.64.1.1) durant cette nuit ?

En effet la sauvegarde apparait en erreur et le test d’inventaire ne retrouve par le périphérique de sauvegarde…

Veuillez Monsieur me tenir informer de l’évolution de ma requête.

Merci par avance.

Cordialement,

Sébastien REIX
Service Informatique
Mail : sebastien.reix@groupeprunay-si.fr
 

',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-29 14:45:00',0,0,324493,120,57,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (118,0,'[EUREXO] Demande d''accès vers EURISK MONTPELLIER','2013-05-29 16:40:00','2013-06-17 09:04:51','2013-06-05 14:06:17','2013-06-17 09:04:51',23,'closed',23,1,0,'',0,'Michel CARON doit travailler pour EURISK avec ses assistantes, il faut donc mettre en place les accès FTP, DTU, ... pour lui et ses deux assistantes.

- Accès CD-DTU,
- Accès serveur de Montpellier
- vérifier liaison VPN
- besoin d''une imprimante ? utiliser le copieur
- besoin de numeriser ? poste scan ?
- besoin en transcripteur ?

Assistante : 
 - Anne RESCHE
 - Celine FAYET...',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-05 14:06:17',0,0,534291,206777,1951,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (119,28,'Problème de RV sur le STD','2013-05-30 08:52:00','2013-06-10 08:51:13','2013-05-30 09:57:23','2013-06-10 08:51:13',24,'closed',24,2,0,'',0,'Bonjour,

Les gestionnaires qui ne prennent pas la téléphonie mettent un renvoi d’appel sur le standard mais leurs lignes directes ne basculent pas sur le standard ou l’acp à la place nous avons un message qui précise que la ligne est non attribuée ou indisponible.

Pas terrible pour le client alors que la ligne existe. Peux-tu voir rapidement le problème et le solutionner.

Cdt.
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-30 09:57:23',0,0,302353,3923,104,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (120,0,'[SWITCH] Configuration Cisco SW-311-15','2013-05-30 11:31:00','2013-06-10 08:51:13','2013-05-30 11:32:40','2013-06-10 08:51:13',23,'closed',23,1,0,'',0,'Préparation du switch Cisco pour Toulouse EURISK.
Sebastien va faire le déplacement pour préparer la baie.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-30 11:32:40',0,0,292813,100,88,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (121,0,'[SERVEUR] Problème d''impression depuis adobe reader (w2003-131-01)','2013-05-30 14:33:00','2013-06-11 08:51:03','2013-05-31 10:57:49','2013-06-11 08:51:03',23,'closed',23,1,0,'',0,'Crash d''adobe lorsqu''on clique sur "Imprimer".
Problème localisé sur les imprimantes RICOH. 
Test de réinstallation du pilote : fail.
Réparation Adobe Reader : OK (reboot du serveur necessaire).
Planification d''un reboot pour ce soir 22h : OK

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-31 10:57:49',0,0,325083,30289,115,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (122,0,'[GESTISOFT] Demande d''accès au serveur Qantex pour mise à jour Gestisoft','2013-05-30 14:37:00','2013-06-10 08:51:13','2013-05-30 14:40:11','2013-06-10 08:51:13',23,'closed',23,1,0,'',0,'A la demande d''un utilisateur Qantex, Gestisoft doit mettre a jour une partie de leur application sur le serveur Qantex/Adapthome.

J''ai prévenu les utilisateurs connectés et j''ai donné l''accès à Gestisoft depuis mon poste au serveur.',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-30 14:40:11',0,0,281653,191,153,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (123,0,'[SERVEUR] Serveur ne démarre plus suite à l''installation d''une imprimante','2013-05-30 14:40:00','2013-06-14 08:47:29','2013-06-03 11:25:18','2013-06-14 08:47:29',23,'closed',23,1,0,'',0,'Le serveur TSE de Limonest ''10.69.3.82'' ne démarre plus (Bloque a l''application des paramétres).',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-03 11:25:18',0,0,454049,74718,411,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (124,0,'[SERVEUR] Lenteur sur le serveur w2003-312-50','2013-05-30 14:49:00','2013-06-13 11:17:25','2013-06-03 11:24:59','2013-06-13 11:17:25',160,'closed',23,1,0,'',0,'

De : CPCOM/GIRAUDEAU Stephane 
Envoyé : jeudi 30 mai 2013 13:42
À : CPCOM/PEREZ Pascal
Cc : CPCOM/CHOLLET Ludovic
Objet : Serveur VEEAM

Salut,


    Ton serveur w2003-312-50 ne semble pas répondre. Je pense qu’il manque de ressources. Il faut changer l’@ip des DNS :


DNSServerSearchOrder : {10.31.20.50, 10.31.2.1} 

A+


Stéphane
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-03 11:24:59',0,0,419305,74159,100,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (125,0,'[SERVEUR] Autoriser une adresse IP (copieur) a envoyer des mails','2013-05-30 15:24:00','2013-06-10 08:51:13','2013-05-30 15:36:15','2013-06-10 08:51:13',23,'closed',23,1,0,'',0,'Il s''agit du copieur 10.78.5.43 qu''Alexandre est en train d''installer a Versailles.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-30 15:36:15',0,0,278833,735,80,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (126,0,'Probleme de création de Mail contact avec avec compte Support','2013-05-30 16:14:00','2013-06-11 08:51:03','2013-05-31 10:10:45','2013-06-11 08:51:03',150,'closed',150,1,0,'',0,'Problème de création de Mail contact avec avec compte Support sur w2003-2120.110',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-31 10:10:45',0,0,319023,21405,156,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (127,31,'Problème de N° SDA Mme LAUTL Viviane','2013-05-30 16:41:00','2013-06-24 08:52:45','2013-06-12 15:05:41','2013-06-24 08:52:45',24,'closed',24,2,0,'',0,'Suite à entretien téléphonique de ce jour avec Philippe, qui me lit en copie, une erreur de ligne directe s''est glissée dans l''annuaire qui nous a été remis.

En effet, les coordonnées correctes de Viviane LAULT sont les suivantes :

N° interne :		213 158
N° ligne directe :	01.40.92.61.83 (et non 82)

Il s''avère donc que Viviane ne reçois pas de lignes extérieures qui basculent directement sur PIDF.

',4,3,4,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 15:05:41',0,232515,473790,150566,142,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (128,8,'Problème de diffusion de N° NDI sur le poste 200152','2013-05-30 17:05:00','2013-06-17 09:04:51','2013-06-04 16:38:48','2013-06-17 09:04:51',24,'closed',24,1,0,'',0,'Bonjour,

Nous venons de nous apercevoir lors d''un appel sortant du poste téléphonique dont le numéro interne est 200152 ( Nathalie JUSTINE), que le numéro qui apparait est le 05.61.62.98.83. Normalement devrait apparaitre le 03.10.36.00.90.

Nous avons fait les tests sur tous les postes du CRQM, il y a cette anomalie uniquement sur ce poste.

En regardant sur les pages jaunes, ce numéro correspond à une entreprise NFRANCE à RAMONVILLE SUR AGNE.

Merci de faire le nécessaire.

Cordialement

Emilie LETELLIER
EUREXO SERVICES
 
Tel 03.10.36.00.73
e.letellier@eurexo.fr

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-04 16:38:48',0,0,489591,128028,95,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (129,8,'Problème de diffusion de NDS poste Mr RAFFY 299218','2013-05-30 17:39:00','2013-06-17 09:04:51','2013-06-05 12:10:56','2013-06-17 09:04:51',24,'closed',24,3,0,'',0,'Problème de diffusion de NDS poste de Mr RAFFY il doit diffuser le 01 30 78 41 95 et non le 05 61 62 98 83',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-05 12:10:56',0,6101,481450,147015,94,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (130,8,'pas de supervision de LOUVECIENNES sur l''ACP','2013-05-31 10:40:00','2013-06-11 08:51:03','2013-05-31 12:25:07','2013-06-11 08:51:03',24,'closed',24,3,0,'',0,'pas de supervision de LOUVECIENNES sur l''ACP',2,3,2,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-31 12:25:07',0,0,295863,6307,76,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (131,0,'[SAUVEGARDE] Mise à jour Backupexec sur w2003-311-01','2013-05-31 14:02:00','2013-06-14 08:47:29','2013-06-03 11:45:23','2013-06-14 08:47:29',23,'closed',23,1,0,'',0,'Suite a plusieurs problème de sauvegarde, mettre à jour la version de Backupexec (v10 vers v12.5).',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-03 11:45:23',0,0,413129,35003,57,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (132,8,'Blocage ACP','2013-05-31 15:12:00','2013-06-11 08:51:03','2013-05-31 15:15:57','2013-06-11 08:51:03',24,'closed',24,3,0,'',0,'Blocage des postes ACP en pas prét ',5,3,4,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-31 15:15:57',0,0,279543,237,76,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (133,8,'poste 200112 ne reçoit pas d''appel de l''ACP','2013-05-31 16:07:00',null,null,'2013-06-18 11:16:15',24,'waiting',24,1,0,'',0,'Poste 200112 activé sur l''ACP ne reçoit pas d''appel.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-05-31 16:32:14',0,0,0,0,101,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (134,0,'Demande de modification DNS sur la plateforme VPN','2013-06-03 08:08:00','2013-06-17 16:47:43','2013-06-17 09:38:32','2013-06-17 16:47:43',160,'closed',154,1,0,'',0,'Modifier DNS1=10.31.20.50
par 10.31.20.49

Lié à la migration des rôles DC.',3,3,3,7,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 09:38:32',0,0,463183,437432,143,2100,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (135,25,'GRANT MySQL.','2013-06-03 08:14:00','2013-06-17 09:04:51','2013-06-04 09:08:41','2013-06-17 09:04:51',154,'closed',154,1,0,'',0,'Pascal,

On a changé le serveur dex c est maintenant la machine 10.31.20.114 il faudrait que tu autorises ce serveur à attaquer la base mySQL sur HERMES CONSTRUCTION en prod
Tu peux t en occuper aujourd hui ? c est pour les stats qui sont envoyées à Paul Boyer





David Berthalon
Service Informatique
Groupe Prunay
Tel : 05 61 39 70 39
Tel CPCOM (223)',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-04 09:08:41',0,0,435051,46481,371,900,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (136,0,'Script de maintenance backup HERMES','2013-06-03 08:21:00','2013-07-01 09:28:21','2013-06-18 16:49:15','2013-07-01 09:28:21',154,'closed',154,1,0,'',0,'Hi Mr Perez,
The backup will be ok this Week end.

What was the problem ? In my opinion , the cache file  was corrupted. 
That''s why he was very long
- The backup is very long due to the deduplication of Pdf fichier 
- However  every day , the maintenance starts and kills the jobs which are running. So the  blocks were  never had to the cache files.
- The First backup must be done and finished.


I cut the backup between 3 jobs
-system : All Except /home/hermes/data/doc/DocumentHermes/doh_url   &  /hermes/data/doc/DocumentHermes/nho_url/
-doh  :  /home/hermes/data/doc/DocumentHermes/doh_url
-nho  :  /home/hermes/data/doc/DocumentHermes/nho_url/

Is it possible to cron  a tar of  files f_cache.dat  and p_cache.dat once a week (No during the backup ) ? If Yes ,can you please give us the path where you will locate the file tar locally . I will have to exclude it of the backup job

Vikrant  , can you please have a look  on these 3 jobs on monday 27th  ?

Sorry for the delay

Regards,
Frederic Hemeury',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-18 16:49:15',0,45589,822452,460106,74,17400,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (137,8,'Remontée de dossier HermesD','2013-06-03 08:23:00',null,null,'2013-06-03 10:40:53',154,'new',154,1,0,'',0,'Bonjour,

Pouvez-vous programmer la remontée des 2 dossiers ci-dessous (avec documents) ?

Cordialement

                Philippe Perruisset
                Directeur des Systèmes d''Information
                Groupe Eurexo
                Tél Bureau : 01 30 78 42 14
                Tél Portable : 06 24 52 79 12',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,68,1200,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (138,10,'Demande d''accès MySQL','2013-06-03 08:58:00','2013-06-28 09:55:04','2013-06-17 15:56:19','2013-06-28 09:55:04',154,'closed',154,1,0,'',0,'De : Eurexo Direction/DOS SANTOS Nelson
Envoyé : lundi 27 mai 2013 11:08
À : CPCOM/Support
Objet : Connexion ODBC vers 10.31.20.157

Bonjour,

Pourriez-vous svp installer sur mon poste des drivers MySQL pour accéder à l''environnement Statistiques (serveur 10.31.20.157) par des connexions ODBC.

Merci d''avance.

Cordialement,

Nelson DOS SANTOS',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 15:56:19',0,0,824224,457099,56,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (139,0,'Modification DNS 10.31.20.50','2013-06-03 09:11:00','2013-06-03 10:10:38','2013-06-03 09:13:31','2013-06-03 10:10:38',160,'closed',154,1,0,'',0,'Salut,


    Ton serveur w2003-312-50 ne semble pas répondre. Je pense qu''il manque de ressources. Il faut changer l''@ip des DNS :


DNSServerSearchOrder : {10.31.20.50, 10.31.2.1}
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-03 09:13:31',0,0,3578,151,51,600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (140,0,'[MESSAGERIE] Problème avec le fichier joint du mail automatique','2013-06-03 09:45:00','2013-06-14 08:47:29','2013-06-03 10:56:14','2013-06-14 08:47:29',23,'closed',23,1,0,'',0,'Lorsqu''on créé une messagerie avec le script, la PJ du mail qui envoi le rapport ne contient pas les bonnes données.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-03 10:56:14',0,0,385349,4274,62,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (141,30,'plantage PTF5','2013-06-03 08:47:00','2013-06-10 10:25:45','2013-06-10 10:25:45','2013-06-10 10:25:45',160,'closed',160,1,0,'',0,'Le serveur ne répond plus.

Erreur dans les logs RSA:
A software NMI has occurred on system "SN# KD09P96"
A Uncorrectable Bus Error has occurred on system "SN# KD09P96"

led en façade:

PCI Orange On 

Le serveur reboot sans cesse depuis le 2/6 à 20:42. A 9:30 le serveur a redémarré sans intervention particulière de la part de CPCOM.

11 contact avec Ornis. Ouverture d''un ticket IBM erreur Processeur. copie des profils dans c:\\progi de PTF7.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,221925,221925,5358,10800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (142,0,'[MESSAGERIE] Script de création de mail : Ajout du rapport MIB pour EURISK','2013-06-03 10:56:00','2013-06-14 08:47:29','2013-06-03 11:24:38','2013-06-14 08:47:29',23,'closed',23,1,0,'',0,'Ajouté le rapport MIB pour les adresses @eurisk.fr',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-03 11:24:38',0,0,381089,1718,93,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (143,0,'[NAGIOS] Supprimer la supervision du matériel d''EUREXO FER','2013-06-03 10:58:00','2013-06-14 08:47:29','2013-06-03 17:44:39','2013-06-14 08:47:29',23,'closed',23,1,0,'',0,'Suite au rergoupement d''EUREXO FER et EUREXO GARONNE, il faut supprimer les équipements 10.31.3.0 de Nagios.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-03 17:44:39',0,0,380969,24399,65,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (144,10,'JASPERSERVER','2013-06-03 11:41:00',null,null,'2013-06-03 11:43:53',154,'assign',154,1,0,'',0,'Bonjour Pascal,

Suite à notre conversation téléphonique d''hier, pourrais-tu stp prévoir les modifications suivantes sur l''environnement « Statistiques » :

-          Mise à jour de JasperServer en version 5.0.1 (notamment en raison de la correction du bug 25986 - cf ci-joint)

-          Autoriser l''export des rapports au format Excel paginé

-          Supprimer l''export des rapports au format Excel 2007 (XLSX)

-          Afficher le menu « Créer » dans JasperServer uniquement pour les profils administrateur.

-          Masquer le bouton « Enregistrer » en visualisation de rapports pour tous les utilisateurs

-          Ajout d''un « custom component » sur l''ETL pour l''exécution et la diffusion de rapports Jasper depuis l''ETL

J''ai bien noté que ces modifications n''interviendront pas avant mi-juin.

Bonne journée (et bonnes vacances)

Cordialement,

Nelson',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,173,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (145,0,'Document architecture','2013-06-03 11:44:00',null,null,'2013-06-03 11:46:04',154,'assign',154,1,0,'',0,'Pour consolidation des déploiements et évolutions de l''infrastructure.',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,124,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (146,0,'Installation Redmine','2013-06-03 11:46:00',null,null,'2013-06-03 11:58:12',154,'assign',154,1,0,'',0,'Pour usage interne (reporting et qualité)',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,68,6000,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (147,0,'Migration Yvette.','2013-06-03 11:50:00',null,null,'2013-06-19 16:44:27',154,'assign',154,1,0,'',0,'Mamie rejoint l''infrastructure CPCOM.
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,104,10500,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (148,0,'[NAGIOS] Personnalisation des droits','2013-06-03 11:55:00','2013-06-14 08:47:29','2013-06-03 13:42:39','2013-06-14 08:47:29',23,'closed',23,1,0,'',0,'

De : CPCOM/DELCOURT Jean-Hugues 
Envoyé : vendredi 24 mai 2013 07:41
À : CPCOM/CHOLLET Ludovic
Objet : Re: Configuration des accès sur NAGIOS

Ludovic,

Merci de mettre les droits rw pour l''infra et ro pour les autres. 
Jean-hugues
De: CPCOM/CHOLLET Ludovic
Envoyées: jeudi 23 mai 2013 18:23 PM
À: CPCOM/DELCOURT Jean-Hugues
Objet: Configuration des accès sur NAGIOS

Jean-Hugues,
Suite à la mise en place de l’authentification sur LDAP, tous les comptes CPCOM peuvent se connecter.
La gestion des droits se fait à partir d’un fichier CGI avec les options détaillé dans le tableau ci-dessous. 
Peux-tu m’indiquer quel droit tu souhaites mettre en place (r/rw) ? Pour le moment, j’ai mis * partout donc tout le monde a tous les droits.
 
Nom	Valeurs	Description
authorized_for_system_information	*	Indiquent quels sont les utilisateurs pouvant voir l’état des services
authorized_for_configuration_information	*	Indiquent quels sont les utilisateurs pouvant voir la configuration de serveur Nagios
authorized_for_system_commands	*	Indiquent quels sont les utilisateurs pouvant exécuter des commandes systèmes au travers de l’interface de Nagios
authorized_for_all_services	*	Indiquent quels sont les utilisateurs pouvant voir l’état de tous les services (par défaut, on voit uniquement les services pour lesquels l’utilisateur est une personne de contact)
authorized_for_all_hosts	*	Indiquent quels sont les utilisateurs pouvant voir l’état de tous les hôtes (les machines)
authorized_for_all_service_commands	*	Indiquent quels sont les utilisateurs pouvant exécuter des commandes pour tous les services (par défaut, on peut exécuter des commandes uniquement sur les services pour lesquels l’utilisateur est une personne de contact)
authorized_for_all_host_commands	*	Indiquent quels sont les utilisateurs pouvant exécuter des commandes pour tous les hôtes (les machines)
 
J’ai mis à jour la procédure NAGIOS.
Merci !
Ludovic

',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-03 13:42:39',0,0,377549,6459,89,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (149,0,'Migration ALFRESCO','2013-06-03 11:58:00',null,'2013-06-26 16:56:28','2013-06-26 16:56:28',154,'solved',154,1,0,'',0,'From ORANGE Business Flexible Computing to CPCOM.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-26 16:56:28',0,0,0,752308,71,82800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (150,2,'Serveur VDOC','2013-06-03 11:59:00','2013-06-18 08:40:53','2013-06-07 14:44:56','2013-06-18 08:40:53',154,'closed',154,1,0,'',0,'Demande de production.
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-07 14:44:56',0,0,463313,182756,52,10800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (151,44,'Maquette OPENFIRE','2013-06-03 12:00:00',null,null,'2013-06-03 12:01:12',154,'assign',154,1,0,'',0,'Demande de réalisation d''une maquette OPENFIRE.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,72,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (152,0,'[NAGIOS] Mise à jour des équipements','2013-06-03 13:51:00','2013-06-14 08:47:29','2013-06-03 14:50:54','2013-06-14 08:47:29',23,'closed',23,1,0,'',0,'Plusieurs équipement en critical depuis longtemps sur NAGIOS :
- BOX-781-17
- RTR-191-12
- AP-927-14
- Serveur Hermes Dommage & Construction',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-03 14:50:54',0,0,370589,3594,128,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (153,0,'[SERVEUR] Problème de connexion SSH sur les serveurs Construction/Dommage','2013-06-03 14:19:00','2013-06-24 08:52:45','2013-06-12 15:22:33','2013-06-24 08:52:45',154,'closed',23,1,0,'',0,'La connexion SSH de l''utilisateur Nagios ne fonctionne plus sur les serveurs Hermes production:
Remote command execution failed: WARNING: Your password has expired.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 15:22:33',0,0,628425,306213,157,1200,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (154,0,'[VMWARE] Mise à jour VMWare Tools sur les serveurs Windows 10.31.2.0/24','2013-06-03 15:52:00','2013-06-17 09:04:51','2013-06-05 10:05:17','2013-06-17 09:04:51',23,'closed',23,1,0,'',0,'VMWare tools obsolete sur les machines Windows.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-05 10:05:17',0,0,407571,65597,69,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (155,0,'[VMWare] VMotion ne fonctionne pas sur notre cluster ESX 10.31.2.0/24','2013-06-03 15:53:00','2013-06-24 08:52:45','2013-06-11 22:29:29','2013-06-24 08:52:45',23,'closed',23,1,0,'',0,'*',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-11 22:29:29',0,0,622785,274020,85,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (156,0,'[SERVEUR] Reconfiguration de l''adresse IP du serveur EUREXO FER','2013-06-03 16:51:00','2013-06-14 08:47:29','2013-06-03 17:14:41','2013-06-14 08:47:29',23,'closed',23,1,0,'',0,'Suite au déménagement, il faut reconfigurer l''adresse IP sur le serveur Eurexo FER (10.31.3.1 en 10.31.7.2).

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-03 17:14:41',0,0,359789,1421,155,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (157,29,'supervision ACP sur VERSAILLES','2013-06-04 11:16:00','2013-06-17 09:04:51','2013-06-04 11:19:02','2013-06-17 09:04:51',24,'closed',24,1,0,'',0,'Plus de suppervision de l''ACP sur VERSAILLES',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-04 11:19:02',0,0,380931,182,116,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (158,31,'Pb Débordement accueil MONTROUGE sur fort enjeu MONTROUGE','2013-06-04 12:02:00','2013-06-18 08:40:53','2013-06-07 11:55:43','2013-06-18 08:40:53',24,'closed',24,1,0,'',0,'Les appels de l''accueil débordement sur l''accueil de fort enjeu .',4,3,4,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-07 11:55:43',0,94028,325905,35195,335,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (159,2,'Creation alias ','2013-06-04 13:15:45',null,null,'2013-06-12 09:41:45',159,'new',159,1,0,'',0,'Bonjour,

Pouvez-vous me créer un alias à rediriger sur ma messagerie :

testnoe@eurexo.fr

Merci

Vincent',3,3,3,1,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,0,0,1);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (160,31,'problème icone sur ACP poste 213112','2013-06-04 14:54:00',null,'2013-07-01 10:35:20','2013-07-01 10:35:20',24,'solved',24,1,0,'',0,'Pas icone clé pour l''ouverture de l''ACP du poste 216112 de Mme STORTZ',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-01 10:35:20',0,780442,0,24838,156,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (161,0,'[SERVEUR] Problème d''impression général','2013-06-04 15:03:00','2013-06-17 09:04:51','2013-06-05 13:37:19','2013-06-17 09:04:51',23,'closed',23,1,0,'',0,'Depuis la réinstallation des imprimantes IP, beaucoup de problème :

- Annie a perdu ses documents personnel
- Réinstallation du pilote : P2050 series PCL6 pour le poste de Sandra
- Réinstallation du pilote : HP Color LaserJet CP1510 Series PCL 6 pour le poste d''Annie
- Réinstallation du pilote : HP Color LaserJet CP1510 Series PCL 6 pour le poste d''Magalie

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-05 13:37:19',0,0,367311,38059,366,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (162,0,'GLPI Pb d''envoi de mail ','2013-06-04 15:49:00','2013-06-17 09:04:51','2013-06-04 15:59:38','2013-06-17 09:04:51',150,'closed',150,1,0,'',0,'Bonjour,

L application ne peut plus envoyé de mail ',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-04 15:59:38',0,0,364551,638,125,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (163,17,'Mdp session ACP','2013-06-04 16:54:00','2013-06-17 09:04:51','2013-06-04 16:58:00','2013-06-17 09:04:51',24,'closed',24,1,0,'',0,'Je n''ai pas les identifiant et Mdp Pour l''ACP pour le 216111 , 2165106 , 216110',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-04 16:58:00',0,0,360651,240,161,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (164,31,'Blocage TWP CALLER en cours de com','2013-06-04 17:37:00','2013-06-28 09:55:04','2013-06-17 08:52:56','2013-06-28 09:55:04',24,'closed',24,2,0,'',0,'depuis plus d''une heure, le TWP reste bloqué sur l''appel entrant de Roselyne ; j''ai beau sortir et essayer de raccrocher, rien à faire !

Peux-tu me dire ce qu''il faut faire ?

Merci,

Cécile LACROIX
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 08:52:56',0,342515,407369,14841,110,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (165,55,'[QUADRA] Automatisation de l''import ARDI -&gt; Quadra','2013-06-04 17:48:00',null,null,'2013-06-04 17:48:49',23,'assign',23,1,0,'',0,'

De : Eurisk Siege/LAGRANGE Patrick 
Envoyé : mardi 4 juin 2013 15:39
À : CPCOM/CHOLLET Ludovic
Cc : Eurexo Siege/DOUKHAN Blandine
Objet : TR: 

Bonjour Ludo,
 
Aurais-tu un petit moment à m''accorder car je travaille actuellement sur l''automatisation des interfaces Ardi/Quadra et j''ai besoin, d''avoir accès à la session Rdp Ardi, et que des liens soient mis en place entre le serveur w2003-922-03 et le w2003-781-07.groupe-prunay.fr\\partage\\6-service comptabilite qui pointe sur Apl1.
 
Ou penses-tu que le support peut prendre en charge.
 
Merci
 
LAGRANGE Patrick  patrick.lagrange@eurisk.fr
D.A.F.   EURISK SAS 
Société membre du Groupe PRUNAY
 01.30.78.18.16 / 06.12.57.55.81 /   01.30.78.18.37 
 Afin de contribuer au respect de l''environnement, merci de n''imprimer ce courriel que si nécessaire 
 

________________________________________
De : Eurexo Siege/DOUKHAN Blandine 
Envoyé : mardi 4 juin 2013 15:13
À : Eurisk Siege/LAGRANGE Patrick
Objet : 
PLF5
\\\\w2003-922-03\\ardi2k\\Eurexa\\wcpta\\Agen\\XIMPORT.TXT A CHANGER N/CPTA CLTS/ARDI

\\\\w2003-922-03\\ardi2k\\EurexA\\Wcpta
\\\\w2003-922-03\\ardi2k\\EurexO\\Wcpta
Cordialement


Blandine DOUKHAN
responsable comptable Groupe Prunay
19 chemin de Prunay
78430 LOUVECIENNES
tel 01 30 78 18 33
b.doukhan@eurexo.fr


',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,49,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (166,31,'ACP 213113 bloqué','2013-06-05 10:42:00','2013-06-17 09:04:51','2013-06-05 10:45:46','2013-06-17 09:04:51',24,'closed',24,2,0,'',0,'Bonjour Patrice,

Chorouk ne peut plus se connecter sur l''ACP, même en se déconnectant/reconnectant.

Peux-tu la rappeler STP au 213113 ?

Merci,

Cécile LACROIX
',4,3,4,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-05 10:45:46',0,0,339771,226,76,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (167,0,'[IMPRIMANTE] Problème de connexion vers le service maintenance de CANON','2013-06-05 11:10:00','2013-06-17 09:04:51','2013-06-05 11:13:00','2013-06-17 09:04:51',23,'closed',23,1,0,'',0,'Depuis le changement du réseau de Limonest, l''imprimante CANON 10.69.3.26 n''envoi plus ses rapport à CANON.

Il faut recontacter le technicien : 
0621945194
alain perret de chez canon ne parvient plus a joindre l''imprimante canon 10.69.3.26
iR C2880
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-05 11:13:00',0,0,338091,180,131,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (168,0,'[MESSAGERIE] Script de création de BAL : boite mail non nominative','2013-06-05 13:39:00',null,null,'2013-06-05 13:41:47',23,'assign',23,1,0,'',0,'Sacha doit créer une messagerie non nominative et ne peux donc pas utiliser le script.

Voir s''il est possible d''ajouter une option pour ce genre de création.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,167,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (169,0,'[WORD] Impossible d''ouvrir un document avec un double clic','2013-06-05 14:04:00','2013-06-17 09:04:51','2013-06-05 15:36:25','2013-06-17 09:04:51',23,'closed',23,1,0,'',0,'
De : CPCOM/GAY Serge 
Envoyé : mercredi 5 juin 2013 10:02
À : admin
Objet : URL : http://glpi/index.php?redirect=ticket_115

Bonjour,

Le redémarrage du serveur n’a pas résolu le pb.

Rappel :
Frédéric Barthe de Rouen, n’arrive pas à ouvrir les documents Word en cliquant dessus .

Son profil sur le serveur a été refait, une réparation de MS Word effectué puis redémarrage du serveur.

Pas de pb avec cp2000 ni si on ouvre MS Word en premier puis le document.

@+
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-05 15:36:25',0,0,327651,5545,68,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (170,0,'[MESSAGERIE] Script de création de BAL : problème avec l''adresse MIB','2013-06-05 15:36:00','2013-06-17 09:04:51','2013-06-05 15:42:48','2013-06-17 09:04:51',23,'closed',23,1,0,'',0,'De temps en temps l''adresse MIB n''est pas correcte. ',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-05 15:42:48',0,0,322131,408,76,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (171,17,'les postes 216106 et 216110 ne recoivent pas d''appel ACP','2013-06-05 15:29:00','2013-06-17 09:04:51','2013-06-05 16:40:47','2013-06-17 09:04:51',24,'closed',24,3,0,'',0,'problème de réception d''appel ACP poste 216106 et 216110',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-05 16:40:47',0,0,322551,4307,4246,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (172,31,'poste 213113 ne reçoit pas d''appel avec l''ACP','2013-06-05 16:41:00','2013-06-17 09:04:51','2013-06-05 16:44:22','2013-06-17 09:04:51',24,'closed',24,3,0,'',0,'le 213113 ne reçoit pas d''appel sr l''ACP',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-05 16:44:22',0,0,318231,202,149,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (173,0,'[NAGIOS] Problème avec le lien _blank vers w2003-312-50','2013-06-05 17:04:00','2013-06-17 09:04:51','2013-06-05 17:07:39','2013-06-17 09:04:51',23,'closed',23,1,0,'',0,'
De : CPCOM/FAURE Etienne 
Envoyé : mercredi 5 juin 2013 17:01
À : CPCOM/CHOLLET Ludovic
Objet : Envoyé à partir de l''outil Capture

Comment on fait pour les certificats ?

Etienne
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-05 17:07:39',0,0,316851,219,30,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (174,0,'creation du poste du stock CPCOM','2013-06-05 17:20:00',null,null,'2013-06-05 17:21:57',24,'new',24,1,0,'',0,'création d''un poste analogique pour le stock N° 225',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,0,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (175,0,'déplacement poste 206 CPCOM','2013-06-05 17:21:00','2013-06-17 09:04:51','2013-06-05 17:23:37','2013-06-17 09:04:51',24,'closed',24,1,0,'',0,'déplacement poste 206 dans salle de réunion',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-05 17:23:37',0,0,315831,157,139,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (176,0,'création poste au stock CPCOM','2013-06-05 17:23:00','2013-06-17 09:04:51','2013-06-05 17:25:51','2013-06-17 09:04:51',24,'closed',24,6,0,'',0,'Création du poste analogique N° 225 pour le stock CPCOM',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-05 17:25:51',0,0,315711,171,159,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (177,25,'Demande - Installation office 2012','2013-06-06 11:02:00',null,null,'2013-06-06 11:04:55',22,'new',22,1,0,'',0,'demande d''installationd ''Office 2010',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,0,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (178,0,'pas de missions generali','2013-06-05 12:52:00','2013-06-10 10:26:30','2013-06-10 10:26:30','2013-06-10 10:26:30',160,'closed',160,1,0,'',0,'Plus de reception generali',3,3,3,5,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,120870,120870,43457,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (179,0,'[SERVEUR] Planification d''un redémarrage w2003-521-01','2013-06-06 14:24:00','2013-06-21 13:01:32','2013-06-10 09:49:55','2013-06-21 13:01:32',23,'closed',23,1,0,'',0,'Suite à beaucoup de bruit provenant du serveur nous allons tenté un redémarrage cette nuit.',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-10 09:49:55',0,0,470252,69955,65,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (180,0,'Suppressio, de mail DEBUSSCHERE','2013-06-06 15:57:00','2013-07-03 14:47:10','2013-07-03 14:47:10','2013-07-03 14:47:10',150,'closed',150,1,0,'',0,'Suppressio, de mail DEBUSSCHERE',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,816610,816610,602,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (181,0,'Track-it - Etat de l''index peu satisfaisant','2013-06-06 16:07:00','2013-07-03 14:48:21','2013-07-03 14:48:21','2013-07-03 14:48:21',150,'closed',150,1,0,'',0,'Track-it - Etat de l''index peu satisfaisant',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,816081,816081,119,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (182,0,'Déclaration imprimante pour scan to mail','2013-06-06 16:09:00','2013-06-21 13:01:32','2013-06-10 17:12:43','2013-06-21 13:01:32',150,'closed',150,1,0,'',0,'Christophe,

L’@ip 10.31.7.28 de l’imprimante IMP317_28 est à déclarer pour pouvoir utiliser le scan to mail.
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-10 17:12:43',0,0,463952,90223,457,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (183,0,'MAJ Doc description de sauvegarde cpom','2013-06-06 16:16:00',null,'2013-07-03 14:45:52','2013-07-03 14:45:52',150,'solved',150,1,0,'',0,'MAJ Doc description de sauvegarde cpom',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-03 14:45:52',0,0,0,815392,236,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (184,0,'Cahier des charges Arborescence Exchange','2013-06-06 16:19:00',null,null,'2013-06-06 16:22:00',150,'new',150,1,0,'',0,'Cahier des charges Arborescence Exchange à réaliser',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,0,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (185,0,'[VPN] Création utilisateur : eurisk.vpn186','2013-06-06 16:25:00','2013-06-17 09:04:51','2013-06-06 16:27:03','2013-06-17 09:04:51',23,'closed',23,1,0,'',0,'
De : CPCOM/CHOLLET Ludovic 
Envoyé : jeudi 6 juin 2013 15:26
À : CPCOM/SALINGARDES Damien; admin; CPCOM/TROUVE Cyril
Objet : RE: vpn urgent

Bonjour, 

Stéphane KRANJEC	Taissy	eurisk.vpn186	eabTarm5

Ludovic

De : CPCOM/SALINGARDES Damien 
Envoyé : jeudi 6 juin 2013 15:20
À : admin
Cc : CPCOM/CHOLLET Ludovic
Objet : vpn urgent

Bonjour,

Besoin d’un compte VPN urgent !

Pour

Stéphane Kranjec
Eurisk Taissy

Merci,

- - - - - - - - - - - - - - - - - - - - - - - -
Damien Salingardes
Service Achats Informatique
05.61.39.96.00

 

',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-06 16:27:03',0,0,275991,123,109,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (186,0,'Réalisation doc install Track-It','2013-06-06 16:26:00',null,null,'2013-06-06 16:27:12',150,'new',150,1,0,'',0,'Réalisation doc install Track-It',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,0,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (187,0,'Pb de synchro Ardi/Outlook','2013-06-06 16:43:00','2013-06-21 13:01:32','2013-06-10 17:10:33','2013-06-21 13:01:32',150,'closed',150,1,0,'',0,'Pb de synchro Ardi/Outlook',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-10 17:10:33',0,0,461912,88053,91,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (188,0,'Suppressio, de mail M Marti','2013-06-06 16:51:00','2013-06-21 13:01:32','2013-06-10 17:11:34','2013-06-21 13:01:32',150,'closed',150,1,0,'',0,'Suppressio, de mail M Marti',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-10 17:11:34',0,0,461432,87634,47,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (189,0,'Prevoir arret serveur physique Track-It','2013-06-06 16:51:00',null,'2013-07-04 16:11:37','2013-07-04 16:11:37',150,'solved',150,1,0,'',0,'Prevoir arret serveur physique Track-It',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-04 16:11:37',0,0,0,861637,861281,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (190,0,'[MESSAGERIE] Script de création de BAL : problème avec l''adresse MIB (PrunayService)','2013-06-07 09:26:00','2013-06-18 08:40:53','2013-06-07 09:48:33','2013-06-18 08:40:53',23,'closed',23,1,0,'',0,'Suite à la création d''un compte "PrunayService" nous rencontrons le même problème qu''avec EURISK : Le fichier MIB n''est pas au bon format.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-07 09:48:33',0,0,299693,1353,98,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (191,0,'Analyse serveur HermesC','2013-06-07 09:52:00','2013-06-24 08:52:45','2013-06-12 16:01:36','2013-06-24 08:52:45',154,'closed',154,1,0,'',0,'Suite à un problème de lenteur et d''erreur d''envoi de mail lors de l''import.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 16:01:36',0,0,471645,151776,83,5400,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (192,29,'RV immédiat exterieur uniquement','2013-06-07 11:04:00','2013-06-18 08:40:53','2013-06-07 11:06:55','2013-06-18 08:40:53',24,'closed',24,3,0,'',0,'Peut on faire un RV immédiat des appels extérieur uniquement ?',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-07 11:06:55',0,0,293813,175,124,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (193,0,'test de la ligne ADSL de Mme ULDARIC','2013-06-07 11:07:00',null,null,'2013-06-07 11:38:31',24,'waiting',24,1,0,'',0,'Problème pour recevoir la télévision',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-07 11:38:31',0,0,0,0,180,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (194,8,'création d''un SDA pour le 211131','2013-06-07 11:24:00','2013-06-24 08:52:45','2013-06-11 11:39:04','2013-06-24 08:52:45',24,'closed',24,1,0,'',0,'Bonjour Patrice,

Merci de bien vouloir créer une ligne directe pour Philippe Rueche tel que décrit dans le fichier ci-joint et m’informer de ce nouveau N°

Reste en attente 

Cordialement
 
                Philippe Perruisset
                Directeur des Systèmes d''Information 
                Groupe Eurexo
                Tél Bureau : 01 30 78 42 14
                Tél Portable : 06 24 52 79 12
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-11 11:39:04',0,86572,379553,732,88,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (195,0,'Espace disque HermesC','2013-06-10 08:55:00','2013-06-21 13:01:32','2013-06-10 08:59:02','2013-06-21 13:01:32',154,'closed',154,1,0,'',0,'/dev/mapper/rootvg-lv_usr
                       15G   13G  1.4G  91% /usr
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-10 08:59:02',0,0,403592,242,61,900,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (196,0,'Alerte serveur Ornis','2013-06-10 10:27:00','2013-06-10 10:33:58','2013-06-10 10:33:58','2013-06-10 10:33:58',160,'closed',160,6,0,'',0,'Bonjour,

Le systeme d''exploitation n''etait pas disponible. Apres le redemarrage le serveur revient a la normale




Cordialement,

--
Le Centre de Service Clients Navaho
Email : support@navaho.fr
Web : https://support.navaho.fr
Tel : 0825.000.412
Fax : 01.55.02.24.70


06/09/2013 12:19 - Navaho -  Centre de Services Clients wrote:
Bonjour,

Notre Centre de Supervision nous a remonté une alarme pour votre serveur ayant l''adresse IP :
10.92.2.5

Le message d''erreur est le suivant :

eurexo-sgbd--22780    Network Ping -&gt; icmp    CRITICAL - 10.92.2.5: rta nan, lost 100%

eurexo-sgbd--22780    check_mk_active    CRIT - Cannot get data from TCP port 10.92.2.5:6556: timed out, execution time 20.0 sec



Cordialement,

--
Le Centre de Service Clients Navaho
Email : support@navaho.fr
Web : https://support.navaho.fr
Tel : 0825.000.412
Fax : 01.55.02.24.70
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,418,418,148,2700,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (197,29,'GSM sur ACP','2013-06-10 11:01:00','2013-06-25 10:33:30','2013-06-14 13:43:29','2013-06-25 10:33:30',24,'closed',24,2,0,'',0,'Bonjour, 

Nous  vous  informons  que  sur  les  annuaires  portables  des  experts avec l''''ACP ne fonctionne pas -

Il faut enlever  33  et  mettre  006...............

Merci 

Cordialement, 

VIVIANE GALINET 

01 39 24 88 09
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-14 13:43:29',0,0,473550,182549,70,14400,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (198,29,'Pb d''acheminement des appels ACP IDF','2013-06-10 11:02:00','2013-06-28 09:55:04','2013-06-17 09:04:50','2013-06-28 09:55:04',24,'closed',24,2,0,'',0,'Bonjour à vous deux,

Nous rencontrons un disfonctionnement et nous n’arrivons pas à résoudre cette anomalie.

L’équipe Accueil Téléphonique est disponible, et nous avons 5 appels en attente qui n’arrivent pas sur le pôle (copie écran de la standardiste disponible).

Comment pouvons nous corriger ce fonctionnement afin d’avoir la possibilité de répondre aux 5 interlocuteurs en attente ?

Cordialement
  
Roselyne Legrand
Tel 01.39.24.22.28
Port 07.87.53.64.94 
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 09:04:50',0,204474,396310,4496,121,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (199,31,'Pb de transfert sur l''ACP de Mme SALLE MONTROUGE','2013-06-10 11:04:00','2013-06-28 09:55:04','2013-06-17 09:09:00','2013-06-28 09:55:04',24,'closed',24,2,0,'',0,'Bonjour Patrice,

peux-tu rappeler Alexandra SALLE qui vient de rencontrer un problème de transfert et vient de perdre une ligne.

Merci,

Cécile LACROIX
Responsable Administrative
 
Agence de MONTROUGE
Tél : 01 40 92 70 43
Fax : 01 40 92 70 49
e.mail : cecile.lacroix@eurexo.fr

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 09:09:00',0,208314,392350,786,93,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (200,48,'[MESSAGERIE] Configuration d''un compte POP/SMTP vers jedeclare.com','2013-06-10 11:37:00','2013-06-24 08:52:45','2013-06-13 14:06:44','2013-06-24 08:52:45',23,'closed',23,1,0,'',0,'Blandine n''arrive pas a envoyer/recevoir de mail avec son compte je décalre.com :
pop : 995
smtp : 487
SSL/TLS

login : prunay_services@jedeclare.com
pass: ohy3ad4n
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-13 14:06:44',0,0,422145,138584,346,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (201,0,'ajout de sauvegarde base SQL Douzy','2013-06-10 13:23:00','2013-06-10 13:25:35','2013-06-10 13:25:35','2013-06-10 13:25:35',160,'closed',160,1,0,'',0,'La base se nomme meteo
 
Cordialement
Morgan Van Der Perre.
 
________________________________________
De : CPCOM/GIRAUDEAU Stephane 
Envoyé : lundi 10 juin 2013 11:31
À : Eurexo Douzy/VAN DER PERRE Morgan
Objet : RE: nouvelle base de donnée : meteo

Bonjour,


Effectivement, je ne l’ai pas eu. Par contre, il me faut le nom de la base.


Cordialement,

Stéphane GIRAUDEAU
Chef de projet service infra CPCOM



De : Eurexo Douzy/VAN DER PERRE Morgan 
Envoyé : lundi 10 juin 2013 11:29
À : CPCOM/GIRAUDEAU Stephane
Objet : nouvelle base de donnée : meteo

Bonjour,

Je me permets de vous contacter, je ne sais plus si je vous avais envoyé le mail ou pas.

Lors d'' un appel je vous avais signalé que je n''avais plus accès à phpmyadmin, et je vous avais signalé par la même occasion, qu'' une nouvelle base allait être mise en place.

La base de donnée est la base meteo, et le dossier avec les fichier php se trouve dans le dossier meteo de www.



 
Cordialement
Morgan Van Der Perre.
 
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,155,155,104,900,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (202,25,'Test Affection Ticket Groupe','2013-06-10 18:31:00',null,null,'2013-06-10 18:44:08',6,'assign',6,1,0,'',0,'Test affectation d''un ticket à un groupe',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',1,0,'2013-06-11 10:31:00',null,0,0,0,0,94,3240,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (203,8,'Modif de parametrage divers téléphonie','2013-06-11 08:44:00','2013-06-24 08:52:45','2013-06-11 09:53:41','2013-06-24 08:52:45',24,'closed',24,2,0,'',0,'Patrice,

Tu trouveras ci-joint une liste de 41 interventions ASAP.

En priorité :  AGNES – Montrouge

Reste à ton écoute.

Cordialement
 
                Philippe Perruisset
                Directeur des Systèmes d''Information 
                Groupe Eurexo
                Tél Bureau : 01 30 78 42 14
                Tél Portable : 06 24 52 79 12

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-11 09:53:41',0,0,389325,4181,113,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (204,0,'PROBLEME RECEPTION EDI','2013-06-11 10:19:00','2013-06-11 12:17:33','2013-06-11 12:17:33','2013-06-11 12:17:33',160,'closed',160,1,0,'',0,'Bonjour,

Nous ne réceptionnons pas les ''EDI depuis 9h30.

Merci de faire le nécessaire.

Cordialement.

Emilie LETELLIER
EUREXO SERVICES
 
Tel 03.10.36.00.73
e.letellier@eurexo.fr
',4,3,4,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,7113,7113,233,2100,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (205,10,'Modification ACL - Split Tunneling VPN pour Jasperserver','2013-06-11 11:03:00','2013-06-24 08:52:45','2013-06-11 17:09:48','2013-06-24 08:52:45',154,'closed',154,1,0,'',0,'De : Eurexo Direction/DOS SANTOS Nelson
Envoyé : mardi 11 juin 2013 09:19
À : CPCOM/Support
Objet : Accès à Jasper depuis le client VPN

Bonjour,

Pourriez-vous svp faire en sorte que l''application Jasper (http://jaspersoft.groupe-prunay.fr:8080/jasperserver-pro/login.html) soit accessible par tous les utilisateurs via le client VPN.

Les utilisateurs ne peuvent pas s''y connecter depuis leur domicile sans intervention de votre part, est-il possible de donner accès à l''application par défaut ?

Exemple : Joël LE ROUZIC ne peut pas s''y connecter actuellement depuis le VPN.

Merci d''avance.

Cordialement,

Nelson',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-11 17:09:48',0,0,380985,22008,142,2700,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (206,31,'pas de supervision de l''ACP Mme STORTZ','2013-06-11 11:39:00','2013-07-01 09:28:21','2013-06-19 15:29:47','2013-07-01 09:28:21',24,'closed',24,3,0,'',0,'Problème sur son PC pour utiliser l''ACP Anywhere',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-19 15:29:47',0,255293,341668,17754,185,7200,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (207,0,'[NAGIOS] Création d''un plugin pour controler la présence d''un fichier LOCK sur un serveur Windows','2013-06-11 10:48:00','2013-06-24 17:12:39','2013-06-24 17:12:39','2013-06-24 17:12:39',160,'closed',23,1,0,'',0,'Tout est dans l''objet.',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,411879,411879,6209,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (208,25,'Jenkins DOWN','2013-06-11 16:39:00','2013-06-24 08:52:45','2013-06-11 16:43:46','2013-06-24 08:52:45',154,'closed',154,1,0,'',0,'Il faudra veiller sur notre ami JENKINS en coma artificiel ...


[cid:image001.jpg@01CE66B6.A293CF80]
- - - - - - - - - - - - - - -
LABY Sabrina
Chef de Projet',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-11 16:43:46',0,0,360825,286,52,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (209,0,'Update règles firewall pour relay KAV en DMZ','2013-06-11 17:18:00','2013-06-24 17:12:12','2013-06-24 17:12:12','2013-06-24 17:12:12',160,'closed',154,1,0,'',0,'Pour déploiement...',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,388452,388452,105,1500,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (210,0,'[FIREWALL] Autoriser le poste de Blandine en pop3s sur le serveur pop.jedeclare.com','2013-06-11 18:33:00',null,'2013-06-25 16:50:22','2013-06-25 16:50:22',23,'solved',23,1,0,'',0,'Salut Ludovic,

Pas de problème pour l''ouverture mais spécifie dans la règle : ip source, ip d''est et port pop3s

Jean-Hugues

Le 10 juin 2013 à 15:41, "CPCOM/CHOLLET Ludovic" &lt;Ludovic.CHOLLET@groupeprunay-si.fr&gt; a écrit :
Salut Jean-Hugues,
 
Je viens d’avoir Blandine DOUKHAN (Louveciennes) qui n’arrive pas à réceptionner des mails en POP TLS (sur le port 995).
Elle en a besoin pour la télé déclaration depuis le site « jedeclare.com ».
 
J’ai fait un nmap le port est bien filtré. Puis-je autoriser les flux sur notre firewall UD ?
 
Bonne journée,
 
Ludovic
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-25 16:50:22',0,0,0,425842,150,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (211,0,'[FIREWALL] Autoriser les flux du VPN Maisonning','2013-06-11 18:35:00','2013-06-24 08:52:45','2013-06-13 10:02:46','2013-06-24 08:52:45',23,'closed',23,1,0,'',0,'De : CPCOM/DELCOURT Jean-Hugues 
Envoyé : mardi 11 juin 2013 17:48
À : CPCOM/CHOLLET Ludovic
Objet : RE: [FIREWALL] demande d''ouverture de port

Ludvic,

Pas de problème, par contre ne restreint pas au seul site de Strasbourg mais à l’ensemble du réseau.

Jean-Hugues.

De : CPCOM/CHOLLET Ludovic 
Envoyé : mardi 11 juin 2013 17:44
À : CPCOM/DELCOURT Jean-Hugues
Objet : [FIREWALL] demande d''ouverture de port

Salut Jean-Hugues,

J’ai reçu une demande hier pour ouvrir les flux VPN Cisco (UDP port 500, UDP port 1000) vers le serveur VPN de Maisoning (195.7.99.231) depuis le site d’Eurexo Strasbourg.
Tu dois t’en douter : le cabinet Eurexo Strasbourg va travailler pour Maisoning.

J’attends ton accords. 
Bonne soirée,

Ludovic
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-13 10:02:46',0,0,353865,55666,148,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (212,0,'[ACP] Problème d''accès au menu "ACP Anywhere" depuis le poste de Severine STORZ','2013-06-11 18:37:00','2013-07-01 09:28:21','2013-06-19 15:12:42','2013-07-01 09:28:21',23,'closed',23,1,0,'',0,'J''ai regardé avec Patrice un problème de connexion vers l''"ACP Anywhere" sur le poste de Severine.

Ancien poste pas a jour, beaucoup de problème.
Mise à jour IE.
Mise à jour JAVA.
Mise à jour FlashPlayer.
Mise à jour DOTNET 3.5
Réinitialisation IE.
Désactivation GPO locale.

Problème toujours là !!!



',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-19 15:12:42',0,0,571881,246942,1281,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (213,42,'[PC] Problème de résolution depuis le changement du poste','2013-06-11 18:58:00','2013-06-24 08:52:45','2013-06-12 11:19:55','2013-06-24 08:52:45',23,'closed',23,1,0,'',0,'
De : Eurisk Haute Savoie/BOVET Nathalie 
Envoyé : mardi 11 juin 2013 08:50
À : CPCOM/CHOLLET Ludovic
Objet : Police PC

Bonjour Ludovic
J’ai installé mon nouveau poste (PC) MAIS peux-tu trouver un moyen de changer la police (ou la résolution) au niveau des logiciels.
J’ai eu ***** qui m’a dit que ce n’était pas possible………… oups ! 
¾ d’heure de boulot, j’ai déjà mal à la tête….
Je te remercie de ton intervention…
Nathalie
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 11:19:55',0,0,352485,15715,369,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (214,0,'[ScoreCard] Migrer l''application un serveur','2013-06-11 22:03:00',null,null,'2013-06-14 15:34:13',23,'assign',23,1,0,'',0,'
De : CPCOM/MARONI Christophe 
Envoyé : mardi 11 juin 2013 09:42
À : Eurexo Direction/PERRUISSET Philippe
Cc : CPCOM/DELCOURT Jean-Hugues; CPCOM/CHOLLET Ludovic; Eurexo Direction/LE ROUZIC Joël; Eurisk Siege/BOYER Paul
Objet : Logiciel Java score Card

Philippe,

Suite au rappel de Thierry et comme aucune installation du logiciel java score card n’avait été effectué, j’ai demandé à Ludovic (équipe infrasctructure CPCOM) de faire en urgence le nécessaire pour que le logiciel : 
1/ soit disponible sur un serveur ou vous pourrez l’utiliser
2/ qu’il vérifie la capacité du serveur à recompiler les sources afin que le test puisse aussi être effectué lors des prochaines livraisons

Merci de ton aide si tu es sollicité,
Cordialement,

Christophe MARONI
GROUPE PRUNAY
Directeur des Systèmes d’Information Groupe
GSM 06 78 74 31 97
christophe.maroni@groupeprunay-si.fr

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,113553,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (215,2,'[MESSAGERIE] Création d''un alias','2013-06-12 09:25:00','2013-06-24 08:52:45','2013-06-12 09:34:14','2013-06-24 08:52:45',23,'closed',23,1,0,'',0,'________________________________________
De : CPCOM/BOUREZ Vincent
Date d''envoi : mardi 11 juin 2013 17:18
À : CPCOM/GIANFREDA Christophe
Objet : TR: Esker - Test de soumission des document Eurexo

Christophe,

Tu pourrais voir pour me créer l''alias testnoe@eurexo.fr à rediriger vers ma messagerie.
C''est urgent à partir demain, je suis censé recevoir des mails...

Merci

Vincent

PS : c''est en relation avec le ticket GLPI 159.


De : Rodriguez-Martin, Sophie [mailto:sophie.rodriguez-martin@esker.fr]
Envoyé : mardi 11 juin 2013 16:38
À : CPCOM/BOUREZ Vincent
Objet : RE: Esker - Test de soumission des document Eurexo

Les règles décrites ci-dessous sont maintenant actives en environnement de pré-production.

Bien cordialement,


Sophie Rodriguez-Martin
Consultant PS / Professional Services Consultant Esker SA De : Rodriguez-Martin, Sophie Envoyé : mardi 11 juin 2013 15:57 À : ''CPCOM/BOUREZ Vincent''
Objet : RE: Esker - Test de soumission des document Eurexo

C’est bien noté, je vous remercie. J’applique la modification demandée dès maintenant et vous préviens par email dès qu’elle est active en environnement de pré-production.

Bien cordialement,


De : CPCOM/BOUREZ Vincent [mailto:Vincent.BOUREZ@groupeprunay-si.fr]
Envoyé : mardi 11 juin 2013 15:55
À : Rodriguez-Martin, Sophie
Objet : RE: Esker - Test de soumission des document Eurexo

Bonjour,

Pour 1. et 2. c''est ok pour moi.

Dans les messages de notification, il est indiqué « Veuillez contacter le support Eurisk si nécessaire (support@eurisk.fr&lt;mailto:support@eurisk.fr&gt; - 05 61 39 96 44). ». Existe-t-il un support Eurexo ?

Le support eurexo est joignable au même numéro mais l''adresse mail est support@eurexo.fr&lt;mailto:support@eurexo.fr&gt; . donc cela donnera la petite phrase suivante :
« Veuillez contacter le support Eurexo si nécessaire (support@eurexo.fr&lt;mailto:support@eurexo.fr&gt; - 05 61 39 96 44). »

Cordialement,

--
Vincent Bourez
Service Informatique
Groupe Prunay
05 61 39 08 31
06 75 67 54 60



De : Rodriguez-Martin, Sophie [mailto:sophie.rodriguez-martin@esker.fr]
Envoyé : mardi 11 juin 2013 15:44
À : CPCOM/BOUREZ Vincent
Objet : RE: Esker - Test de soumission des document Eurexo

Bonjour,

J’ai maintenant conditionné le nom de domaine de l’adresse de destination des notifications emails sur celui de l’utilisateur de soumission des documents. Voici donc les nouvelles règles de choix de l’adresse de notification, actives en environnement de pré-production :

1.       Soumission des documents par un utilisateur dont l’identifiant se termine par « @eurisk.fr » :

a.       Les notifications en cas de hors SLA sont envoyées à l’adresse « notifesker_&lt;agence&gt;@eurisk.fr » et en copie à « ctrlepost@eurisk.fr&lt;mailto:ctrlepost@eurisk.fr&gt; »

b.      Les notifications en cas de problème sur le courrier (problème d’adresse ou NPAI) sont envoyées à l’adresse « notifesker_&lt;#office&gt;@eurisk.fr » et en copie à « ctrlepost@eurisk.fr&lt;mailto:ctrlepost@eurisk.fr&gt; »

c.       Les notifications en cas de doublon sont envoyées à l’adresse « support@eurisk.fr&lt;mailto:support@eurisk.fr&gt; »

2.       Soumission des documents par un utilisateur dont l’identifiant se termine par « @eurexo.fr » :

a.       Les notifications en cas de hors SLA sont envoyées à l’adresse « notifesker_&lt;agence&gt;@eurexo.fr » et en copie à « ctrlepost@ eurexo.fr »

b.      Les notifications en cas de problème sur le courrier (problème d’adresse ou NPAI) sont envoyées à l’adresse « notifesker_&lt;#office&gt;@ eurexo.fr » et en copie à « ctrlepost@eurexo.fr&lt;mailto:ctrlepost@eurexo.fr&gt; »

c.       Les notifications en cas de doublon sont envoyées à l’adresse « support@eurexo.fr&lt;mailto:support@eurexo.fr&gt; »

Il me reste une question en plus de celle sur l’extraction du cabinet : dans les messages de notification, il est indiqué « Veuillez contacter le support Eurisk si nécessaire (support@eurisk.fr&lt;mailto:support@eurisk.fr&gt; - 05 61 39 96 44). ». Existe-t-il un support Eurexo ?

Bien cordialement,


Sophie Rodriguez-Martin
Consultant PS / Professional Services Consultant Esker SA De : CPCOM/BOUREZ Vincent [mailto:Vincent.BOUREZ@groupeprunay-si.fr]
Envoyé : vendredi 7 juin 2013 10:29
À : Rodriguez-Martin, Sophie
Objet : RE: Esker - Test de soumission des document Eurexo

Bonjour

Merci de l''info. Je n''ai pas encore regardé ce qui a été testé.

Aussi je reviendrais sur la problématique de cabinet rapidement (j''espère milieu de semaine prochaine)


Vincent

De : Rodriguez-Martin, Sophie [mailto:sophie.rodriguez-martin@esker.fr]
Envoyé : vendredi 7 juin 2013 10:24
À : CPCOM/BOUREZ Vincent
Objet : Esker - Test de soumission des document Eurexo

Bonjour,

Je vois que vous avez effectué 4 tests de soumission pour Eurexo, qui sont tous en erreur : peut-être le savez-vous déjà, mais la raison est que vous avez spécifié « Test » en tant que valeur de « Rules configuration », au lieu de « CUSTOMER_Eurisk ».

Bien cordialement,


Sophie Rodriguez-Martin
Consultant PS / Professional Services Consultant Esker SA De : Rodriguez-Martin, Sophie Envoyé : mercredi 5 juin 2013 11:56 À : ''CPCOM/BOUREZ Vincent''
Cc : Chupin-Peresson, Nathalie
Objet : RE: Demande d''informations

Bonjour,

L’utilisateur de test spécifique à EUREXO est maintenant créé. Vous trouverez-ci-dessous les informations de connexion à indiquer pour la soumission des documents EUREXO en environnement de test, via l’outil EskerLoader :
Login : testnoe@eurexo.fr&lt;mailto:testnoe@eurexo.fr&gt;
Mot de passe : Mdp3ur3x02013

Je n’ai pas encore mis à jour la règle traitant les notifications en cas de hors SLA pour les documents Eurexo et compte le faire en fin de semaine.

Concernant les tris effectués dans les factures qu’Esker vous envoie, il m’a été indiqué que vous souhaitiez ajouter un classement par « cabinet » pour Eurexo : me confirmez-vous cela? Si oui, pouvez-vous m’indiquer où je peux extraire cette information ? 

Je reste à votre disposition pour toute information complémentaire.

Bien cordialement,


De : CPCOM/BOUREZ Vincent [mailto:Vincent.BOUREZ@groupeprunay-si.fr]
Envoyé : mercredi 29 mai 2013 11:26
À : Rodriguez-Martin, Sophie
Objet : TR: Demande d''informations

Bonjour,

Voici, en PJ, les documents de référence demandés, ce sont les mêmes que j''avais fourni à Nathalie Chupin Peresson, il y a un peu plus de 6 mois.



Ensuite, pour info, notre projet fonctionnera avec l''EskerLoader présenté sur cette page http://doc.esker.com/Tools/cv/en/EskLoader/Content/InstallConfig/InstallJava.html


Bonne journée

--
Vincent Bourez
Responsable Etudes & Développements
Groupe Prunay
05 61 39 08 31
06 75 67 54 60




De : CPCOM/BOUREZ Vincent
Envoyé : mercredi 3 octobre 2012 17:35
À : ''Chupin-Peresson, Nathalie''
Objet : RE: Demande d''informations

Bonjour,

Ci-joint quelques exemples en sortie de l''application métier actuelle.
Il va y avoir des nouveaux modèles de documents avec la réalisation de notre nouveau processus de génération de document mais ceux-ci ne seront disponible qu''à partir de décembre.
Ceci dit, il ne devraient pas être différents dans l''absolu.


Vincent Bourez



De : Chupin-Peresson, Nathalie [mailto:nathalie.chupin-peresson@esker.fr]
Envoyé : mercredi 3 octobre 2012 16:09
À : CPCOM/BOUREZ Vincent
Objet : Demande d''informations

Bonjour
En vue de notre réunion de la semaine prochaine, pourriez- vous nous transmettre quelques documents de référence afin de pouvoir les tester et vous faire une démonstration avec vos documents ?
Je vous remercie par avance.
Cordialement.
Nathalie Chupin-Peresson
Responsable des ventes/Sales Manager
Esker SA
Tél : +33(0)4 26 84 72 59
Fax : +33 (0) 4 72 83 46 40
Mobile : +33 (0) 6 27 49 18 13
nathalie.chupin-peresson@esker.fr&lt;mailto:nathalie.chupin-peresson@esker.fr&gt;
www.esker.fr&lt;http://www.esker.fr/&gt; ■ www.flydoc.fr&lt;http://www.flydoc.fr/&gt;


CONFIDENTIALITE : Ce message et les éventuelles pièces jointes sont confidentiels. Si vous n''êtes pas dans la liste des destinataires, veuillez informer l''expéditeur immédiatement et ne pas divulguer le contenu à une tierce personne. Les idées et opinions présentées dans ce message sont celles de son auteur, et ne représentent pas nécessairement celles de la société. Par ailleurs et malgré toutes les précautions prises pour éviter la présence de virus dans nos envois, nous vous recommandons de prendre, de votre côté, les mesures permettant d''assurer la non-introduction de virus dans votre système informatique. La société ne saurait être tenue pour responsable de tout dommage causé par la présence d''un virus dans ce message.
__________
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 09:34:14',0,0,343665,554,56,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (216,0,'[VEEAM] Analyzing object "w2008-312-08" Object w2008-312-08 not found','2013-06-12 09:36:00','2013-06-25 10:33:30','2013-06-14 11:09:46','2013-06-25 10:33:30',23,'closed',23,1,0,'',0,'
------------
De : CPCOM/TROUVE Cyril 
Envoyé : mercredi 12 juin 2013 08:55
À : admin
Cc : CPCOM/GIANFREDA Christophe
Objet : VEEAM tâche BCK_w2008-312-08

Bonjour,
La tâche de sauvegarde ‘BCK_w2008-312-08’ est en échec :


BCK_w2008-312-08
Analyzing object "w2008-312-08" Object w2008-312-08 not found. There are no objects to backup

Time	Type	Information
11/06/2013 20:31	Success	Job started.
11/06/2013 20:31	Error	Analyzing object "w2008-312-08" Object w2008-312-08 not found. There are no objects to backup
11/06/2013 20:31	Error	Job finished.

Cordialement

Cyril Trouvé
Support Informatique
------------------------------
 
Tél : 05 61 39 96 44 - Fax : 05 61 00 59 56

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-14 11:09:46',0,0,392250,92026,213,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (217,0,'[MESSAGERIE] Change the IP Adress for send mail with Redmine','2013-06-12 10:53:00','2013-06-12 13:59:41','2013-06-12 10:56:27','2013-06-12 13:59:41',154,'closed',23,1,0,'',0,'LUDO,

Redmine n''envoi plus de mail.
Il faut déclarer le nouveau serveur 10.31.20.159 dans Exchange
Plus d''infos auprès de notre admin réseau !',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 10:56:27',0,0,11201,207,172,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (218,0,'[RESEAU] Impossible d''atteindre le serveur de Montrouge depuis un poste à Compiegne','2013-06-12 11:02:00','2013-06-24 08:52:45','2013-06-12 11:05:54','2013-06-24 08:52:45',23,'closed',23,1,0,'',0,'Le réseau fonctionne, adresse IP en 10.60.3.0, elle ping le routeur 10.75.10.10 mais pas les serveurs en 10.75.10.5 et 10.75.10.4.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 11:05:54',0,0,337845,234,115,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (219,25,'[RESEAU] Brasser la prise 68 pour le bureau d''Yves','2013-06-12 11:20:00','2013-06-24 08:52:45','2013-06-12 11:22:56','2013-06-24 08:52:45',23,'closed',23,1,0,'',0,'Yves a changé son bureau de place, j''ai brassé sa prise (68).',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 11:22:56',0,0,336765,176,167,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (220,8,'création poste 213162 ','2013-06-12 11:52:00','2013-06-24 08:52:45','2013-06-12 11:54:52','2013-06-24 08:52:45',24,'closed',24,2,0,'',0,'création du N° 213162 de Mme JOE-HANA Agnés',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 11:54:52',0,0,334845,172,116,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (221,8,'changement de nom poste 200103','2013-06-12 12:26:00','2013-06-24 08:52:45','2013-06-12 12:30:30','2013-06-24 08:52:45',24,'closed',24,3,0,'',0,'changer le nom du poste 200103 mettre Mr BENEDIC Xavier',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 12:30:30',0,0,332805,270,194,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (222,8,'problème ACP poste 200109','2013-06-12 12:30:00','2013-06-24 08:52:45','2013-06-12 13:57:42','2013-06-24 08:52:45',24,'closed',24,3,0,'',0,'le poste 200109 se connecte sur l''ACP il ne reste pas en prét , il se déconnecte tout seul',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 13:57:42',0,0,332565,5262,140,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (223,25,'[RESEAU] Problème avec la prise 81 baie CPCOM','2013-06-12 14:20:00','2013-06-24 08:52:45','2013-06-12 14:24:16','2013-06-24 08:52:45',23,'closed',23,1,0,'',0,'L''imprimante du DEV a basculé dans le bureau d''Irina/Olivier.

Elle est branché sur la prise 81 mais n''a pas de réseau.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 14:24:16',0,0,325965,256,153,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (224,0,'problème de déconnection de l''ACP poste 215101','2013-06-12 14:48:00','2013-06-24 08:52:45','2013-06-12 14:57:24','2013-06-24 08:52:45',24,'closed',24,1,0,'',0,'le poste 215101 est bloqué impossible de l''appeler , pb de déconnection de l''ACP',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 14:57:24',0,0,324285,564,182,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (225,25,'svn update','2013-06-12 15:24:00','2013-06-28 09:55:04','2013-06-17 10:43:46','2013-06-28 09:55:04',154,'closed',154,1,0,'',0,'Suite à migration de serveur Redmine.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 10:43:46',0,26915,471749,85871,351,5400,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (226,10,'Pb de connexions RDP','2013-06-12 15:39:00','2013-06-24 08:52:45','2013-06-12 15:48:00','2013-06-24 08:52:45',154,'closed',154,1,0,'',0,'sur le serveur de stats w2008-3120-116 :
pb de connexions en RDP',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 15:48:00',0,0,321225,540,402,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (227,0,'Déploiement template 2008','2013-06-12 15:48:00','2013-06-13 11:15:47','2013-06-12 15:50:57','2013-06-13 11:15:47',160,'closed',154,1,0,'',0,'Pour KAV en DMZ',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 15:50:57',0,0,26867,177,58,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (228,0,'git','2013-06-12 15:52:00','2013-06-24 08:52:45','2013-06-12 16:00:47','2013-06-24 08:52:45',154,'closed',154,1,0,'',0,'Mise en place d''un GIT local pour maintenance des scripts de prod.',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-12 16:00:47',0,0,320445,527,40,2700,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (229,31,'poste 213100 décroche tout seul','2013-06-13 09:21:00','2013-06-24 08:52:45','2013-06-13 09:25:24','2013-06-24 08:52:45',24,'closed',24,3,0,'',0,'Lors d''un appel sur le 213100 le poste décroche tout seul',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-13 09:25:24',0,0,300705,264,145,900,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (230,0,'[SAUVEGARDE] Services ne démarre pas sur w2008-171-01','2013-06-13 09:24:00','2013-06-24 08:52:45','2013-06-13 09:28:42','2013-06-24 08:52:45',23,'closed',23,1,0,'',0,'Les services BackupExec sont bloqués.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-13 09:28:42',0,0,300525,282,165,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (231,8,'Pb ACP à RAMONVILLE Mme GUERINEAU','2013-06-13 10:39:00','2013-06-24 08:52:45','2013-06-13 10:43:47','2013-06-24 08:52:45',24,'closed',24,1,0,'',0,'Blocage ACP sur le poste 216105',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-13 10:43:47',0,0,296025,287,192,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (232,0,'[SERVEUR] Création de base de données','2013-06-13 10:45:00','2013-06-28 09:55:04','2013-06-17 09:13:53','2013-06-28 09:55:04',154,'closed',23,1,0,'',0,'
De : CPCOM/CHOLLET Ludovic 
Envoyé : mercredi 12 juin 2013 17:32
À : CPCOM/PEREZ Pascal
Objet : Création de base de donnée

Rascal,

Peux-tu mettre en place les bases de données suivantes sur le serveur de stat Mysql :
Les fichiers .dump sont disponible dans le répertoire Projet\_EN COURS\LC_ScoreC@rd\DUMP\

Nom de la base : datbas001a
Fichier dump : datbas001a.dump

Nom de la base : datbas002a
Fichier dump : datbas002a.dump

Nom de la base : datcom001a
Fichier dump : datcom001a.dump

Nom de la base : datres001a
Fichier dump : datres001a.dump

Nom de la base : datres002a
Fichier dump : datres002a.dump

Nom de la base : datpar001a
Fichier dump : datpar001a.dump

Nom de la base : dathermes001a
Fichier dump :  pas encore (il s’agit du dump de la base envoyé dimanche dernier)!

Merci,

Dulo
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 09:13:53',0,10168,462036,70765,193,6900,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (233,0,'Probleme DNS','2013-06-13 11:14:00','2013-06-13 17:29:53','2013-06-13 17:27:32','2013-06-13 17:29:53',160,'closed',160,1,0,'',0,'Stéphane,

Dans la série mystère, j’ai pioché la disparition d’une entrée DNS.
En effet, l’enregistrement DNS du NAS : nas-312-03.groupe-prunay.fr a disparu hier dans le DNS 10.31.20.49.

Je l’ai recréé mais c’est la deuxième fois qu’il disparait…

As-tu une idée sur le problème ?

Merci,

Ludo
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-13 17:27:32',0,0,22553,22412,67,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (234,0,'Pb DHCP tel. MENDE','2013-06-13 11:14:00','2013-06-24 08:52:45','2013-06-13 11:38:24','2013-06-24 08:52:45',154,'closed',154,1,0,'',0,'Le téléphone récupère une IP Data.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-13 11:38:24',0,0,293925,1464,1253,900,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (235,0,'[NAS] Problème pour monter un lecteur réseau sur le nas','2013-06-13 11:37:00','2013-06-24 08:52:45','2013-06-13 11:41:04','2013-06-24 08:52:45',23,'closed',23,1,0,'',0,'Le NAS ne répond pas depuis son nom FQDN : nas-312-03.groupe-prunay.fr alors qu''il répond en NETBIOS nas-312-03.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-13 11:41:04',0,0,292545,244,148,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (236,0,'[MESSAGERIE] Modification d''une adresse de messagerie','2013-06-13 15:58:00','2013-06-24 08:52:45','2013-06-13 16:23:42','2013-06-24 08:52:45',23,'closed',23,1,0,'',0,'L''adresse de messagerie de JOE HANA AGNES n''est pas correcte.

Son nom est : AGNES 
Son prénom est : Joe Hana

Nouvelle adresse de messagerie : j.agnes@eurexo.fr',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-13 16:23:42',0,0,276885,1542,148,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (237,8,'Faire le RV du FAX Eurexo Ramonville','2013-06-13 16:19:00','2013-06-28 09:55:04','2013-06-17 09:19:33','2013-06-28 09:55:04',24,'closed',24,3,0,'',0,'Faire un RV du FAX 05 61 34 95 81 sur le FAX 05 61 61 22 68',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 09:19:33',0,60785,391379,448,138,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (238,10,'demande de mot de passe de messagerie','2013-06-13 17:27:00','2013-06-13 17:30:11','2013-06-13 17:29:36','2013-06-13 17:30:11',160,'closed',160,1,0,'',0,'Salut Phafane,
URGENT je ne retrouve plus le mot de passe pour la messagerie formation.
Tu peux me le renvoyer pour la ….. fois !!
Merci par avance,
Cordialement, 
Régine HERARD 
 
Chargée de Formation
Direction des Systèmes d''Information et Administration
P : 06.25.19.46.58
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-13 17:29:36',0,0,191,156,103,1500,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (239,55,'[SERVEUR] Configuration apache https://files-engimo.groupe-prunay.fr','2013-06-14 11:12:00','2013-07-01 09:28:21','2013-06-18 10:23:48','2013-07-01 09:28:21',23,'closed',23,1,0,'',0,'le lien https://files-engimo.groupe-prunay.fr pointe sur la default page apache.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-18 10:23:48',0,0,468981,83508,106,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (240,16,'Répertoire TWP Caller','2013-06-14 11:41:00','2013-06-25 10:33:30','2013-06-14 11:48:23','2013-06-25 10:33:30',24,'closed',24,3,0,'',0,'Comment peux t''on créer l''annuaire de contact dans TWP Caller ?',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-14 11:48:23',0,0,298350,443,325,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (241,8,'Modif annuaire LEVALLOIS','2013-06-14 11:49:00','2013-06-25 10:33:30','2013-06-14 11:53:22','2013-06-25 10:33:30',24,'closed',24,2,0,'',0,'Modifier suivant liste les noms sur le site de LEVALLOIS',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-14 11:53:22',0,0,297870,262,163,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (242,8,'demande de 41 modifications divers','2013-06-14 13:22:00','2013-06-25 10:33:30','2013-06-14 13:34:00','2013-06-25 10:33:30',24,'closed',24,2,0,'',0,'Patrice,

Tu trouveras ci-joint une liste de 41 interventions ASAP.

En priorité :  AGNES – Montrouge

Reste à ton écoute.

Cordialement
 
                Philippe Perruisset
                Directeur des Systèmes d''Information 
                Groupe Eurexo
                Tél Bureau : 01 30 78 42 14
                Tél Portable : 06 24 52 79 12
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-14 13:34:00',0,510,291780,210,117,10800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (243,31,'Pb de réception d''appel sur les fax','2013-06-14 13:25:00','2013-07-01 09:28:21','2013-06-18 10:50:45','2013-07-01 09:28:21',24,'closed',24,2,0,'',0,'Bonjour Patrice,

depuis hier, nos fax sonnent "occupé" et nous ne recevons aucun document. Peux-tu voir et me tenir informée ?

Merci,

Cécile LACROIX
Responsable Administrative
 
Agence de MONTROUGE
Tél : 01 40 92 70 43
Fax : 01 40 92 70 49
e.mail : cecile.lacroix@eurexo.fr

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-18 10:50:45',0,76943,384058,202,125,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (244,0,'erreur réception de missions','2013-06-14 14:43:00','2013-06-14 15:21:07','2013-06-14 15:21:07','2013-06-14 15:21:07',160,'closed',160,1,0,'',0,'Bonjour, 

Nous n''avons pas reçus d''EDI à 14H ni à 14h30.

Merci de vérifier.

Cordialement

Emilie LETELLIER
EUREXO SERVICES
 
Tel 03.10.36.00.73
e.letellier@eurexo.fr
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,2287,2287,59,1200,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (245,31,'Eurexo NIMES ne peux plus téléphoner','2013-06-14 15:22:00','2013-06-25 10:33:30','2013-06-14 15:26:28','2013-06-25 10:33:30',24,'closed',24,3,0,'',0,'Eurexo NIMES ne peux plus téléphoner , mais ils reçoivent les appels',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-14 15:26:28',0,0,285090,268,154,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (246,31,'Modifier le calendrier ACP de l''Accueil de MONTROUGE','2013-06-14 15:44:00','2013-06-25 10:33:30','2013-06-14 15:47:55','2013-06-25 10:33:30',24,'closed',24,2,0,'',0,'Patrice,

depuis la semaine dernière, j''ai demandé à Diane/Florence d''assurer une permanence téléphonique le midi. Or, entre 12h30 et 13h30, lorsqu''on appelle l''agence, on tombe sur le répondeur.

Peux-tu voir stp et me tenir informée.

Merci

Cécile LACROIX
Responsable Administrative
 
Agence de MONTROUGE
Tél : 01 40 92 70 43
Fax : 01 40 92 70 49
e.mail : cecile.lacroix@eurexo.fr

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-14 15:47:55',0,0,283770,235,108,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (247,29,'Pb réception d''appel ACP poste 215104','2013-06-17 10:47:00',null,null,'2013-06-17 11:02:04',24,'waiting',24,3,0,'',0,'Quand elle est en prét elle ne recoit pas souvent des appels du groupement 215790',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 11:02:04',0,0,0,0,150,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (248,28,'affectation N° 201999 à Mr RUECHE','2013-06-17 11:02:00','2013-07-01 09:28:21','2013-06-18 10:59:20','2013-07-01 09:28:21',24,'closed',24,2,0,'',0,'Bonjour,

Je vous remercie de bien vouloir changer le numéro de poste interne de Mr RUECHE actuellement il est sur numéro 211131 or il faudrait le changer en 201999 (poste anciennement de Mr Patier par contre il faut bien garder la ligne directe tel 01.41.27.34.10.

Cdt. 

NGoncalves.
Responsable Administrative
 
Agence de LEVALLOIS
Tél : 01 41 27 34 35 
Fax :  01 41 27 03 22
e.mail : n.goncalves@eurexo.fr

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-18 10:59:20',0,0,426381,43040,136,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (249,0,'Volumétrie HermesC','2013-06-17 11:13:00','2013-06-28 09:55:04','2013-06-17 11:23:44','2013-06-28 09:55:04',154,'closed',154,1,0,'',0,'Bonjour,

+ de 90 % d''utilisation sur :
/dev/mapper/rootvg-lv_usr
                       15G   13G  1.1G  93% /usr

Cordialement.

Etienne',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 11:23:44',0,0,384124,644,63,900,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (250,0,'Ajou vNIC KAV','2013-06-17 11:41:00','2013-06-17 16:47:27','2013-06-17 11:59:11','2013-06-17 16:47:27',160,'closed',154,1,0,'',0,'Ajout d''une vNIC dans le VLAN NFrance_Pub pour la VM proxy KAV (w2008-3150-50)',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 11:59:11',0,0,18387,1091,922,1500,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (251,0,'copie iso sur datastore local','2013-06-17 13:28:00','2013-06-17 16:46:57','2013-06-17 13:32:27','2013-06-17 16:46:57',160,'closed',154,1,0,'',0,'De informatique/sources/... vers 10.31.20.67/vmfs/volumes/esx-3120-67/ISO/',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 13:32:27',0,0,11937,267,134,1200,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (252,0,'[SERVEUR] Kaspersky ne démarre pas : w2003-3120-50','2013-06-17 13:33:00','2013-06-28 09:55:04','2013-06-17 14:37:15','2013-06-28 09:55:04',23,'closed',23,1,0,'',0,'Le serveur va bientot disparaitre mais voir si on peut quand meme régler le problème.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 14:37:15',0,0,375724,3855,85,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (253,8,'création du poste de Mr CHEVALLIOT ','2013-06-17 13:57:00',null,'2013-07-01 11:02:49','2013-07-01 11:02:49',24,'solved',24,2,0,'',0,'Créé le 200157 à Mr cHEVALLIOT Aurélien',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-01 11:02:49',0,419864,0,1685,1581,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (254,28,'3 postes 6731 HS suite à orage','2013-06-17 14:39:00','2013-06-28 09:55:04','2013-06-17 16:52:51','2013-06-28 09:55:04',24,'closed',24,2,0,'',0,'Bonjour Patrice, 

Il y a 3 postes téléphoniques HS à Eurexo Levallois ( suite à un Orage ).
Justine BROUSSOLLE
Gwendoline DANEL
Carine ALEXA-LAMBERT



Voici les contacts de Nathalie.
01 41 27 34 35
Elle est en copie du mail
Merci de te rapprocher d’elle.
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 16:52:51',0,0,371764,8031,228,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (255,0,'[SERVEUR] Déclaration d''une imprimante sur le serveur QUADRA','2013-06-17 15:29:00','2013-06-28 09:55:04','2013-06-17 15:37:36','2013-06-28 09:55:04',23,'closed',23,1,0,'',0,'Colette (Eurexo ROUEN) souhaite utiliser son imprimante USB sur le serveur QUADRA.
Le pilote HP LaserJet 1200 Series PCL 5
',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 15:37:36',0,0,368764,516,131,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (256,0,'N° de serie VPN','2013-06-17 15:46:00','2013-06-28 09:55:04','2013-06-17 15:49:50','2013-06-28 09:55:04',154,'closed',154,1,0,'',0,'Pour la gateway active.',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-17 15:49:50',0,0,367744,230,58,600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (257,0,'reprise d''envoie H du 18/04','2013-06-17 16:48:00','2013-06-18 15:45:33','2013-06-18 15:45:33','2013-06-18 15:45:33',160,'closed',160,1,0,'',0,'o	La NH n’a pas été transmise par EDI. Le traitement d’envoi EDI du 19/04 (qui envoie les NH du 18/04) n’a pas été exécutée
	P.Giraudeau + P.Trocme : 
•	est-il possible de lancer le traitement en date du 19/04/2013 ?
•	Si ce n’est pas possible, est-il possible de forcer l’envoi EDI de la NH du dossier 0037388503/ Réf Eurexo = 328831
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,39453,39453,143,1200,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (258,0,'Alfresco','2013-06-18 07:59:00','2013-06-18 16:38:17','2013-06-18 16:38:17','2013-06-18 16:38:17',154,'closed',154,1,0,'',0,'Migrer Alfresco de chez ORANGE Flexible Computing vers l''infra. CPCOM.',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,31097,31097,29,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (259,8,'Pb ACP pas de téléphone','2013-06-18 08:40:00','2013-07-01 09:28:21','2013-06-18 08:47:50','2013-07-01 09:28:21',24,'closed',24,3,0,'',0,'Pas de téléphone activé à l''ouverture de l''ACP',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-18 08:47:50',0,0,391701,470,169,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (260,8,'Pb de supervision sur ACP + transfert sur 200157','2013-06-18 09:07:00','2013-07-01 09:28:21','2013-06-18 09:46:11','2013-07-01 09:28:21',24,'closed',24,2,0,'',0,'Bonjour,

Je me permets de revenir vers vous, j''ai branché le poste de Aurélien, il est bien remonté et sa ligne directe fonctionne.

Cependant dans l''ACP on ne peut pas lui transférer de ligne, le double clic ne fonctionne pas et de plus on ne voit pas le combine qui indique qu''il est en ligne ou non.

Au sujet de l''absence du combiné Madame Husson (200154) et Monsieur Bru (200105) ont le même soucis. 

 
Cordialement
Morgan Van Der Perre.
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-18 09:46:11',0,0,390081,2351,171,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (261,8,'Pb ACP supervision ','2013-06-18 09:26:00',null,null,'2013-06-18 09:31:14',24,'assign',24,2,0,'',0,'Bonjour Patrice,

Peux-tu vérifier si les personnes suivantes ont l’ACP installé sur leur poste en version « superviseur » ?

Si tel n’est pas le cas, merci de faire en sorte que …

Reste à ton écoute.

Cordialement
 
                Philippe Perruisset
                Directeur des Systèmes d''Information 
                Groupe Eurexo
                Tél Bureau : 01 30 78 42 14
                Tél Portable : 06 24 52 79 12

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,193,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (262,8,'blocage base Eurexo','2013-06-18 09:48:00','2013-06-18 09:51:15','2013-06-18 09:51:15','2013-06-18 09:51:15',160,'closed',160,1,0,'',0,'Incident Base eurexo inaccessible à 17:30.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,195,195,58,2400,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (263,0,'[SERVEUR] Warning espace disque messagerie','2013-06-18 09:58:00','2013-07-01 09:28:21','2013-06-19 14:46:16','2013-07-01 09:28:21',23,'closed',23,1,0,'',0,'Les deux serveurs de messageries arrive a saturation du disque C.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-19 14:46:16',0,0,387021,60496,105,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (264,0,'Demande de ligne analogique FAX dans le 64500','2013-06-18 10:45:00',null,null,'2013-06-18 10:49:12',24,'waiting',24,2,0,'',0,'Patrice,

Peux tu voir pour cette demande de ligne fax pour un expert délégué à St Jean de Luz qui travaille pour Eurisk Pays Basque Béarn dont le responsable d’agence est Michel SANJAUME.



Cordialement,

Brice Rougé
Service Informatique du Groupe Prunay
Responsable Support
Mail : brice.rouge@groupeprunay-si.fr
Tél : 05 61 39 96 33
Portable : 06 03 16 67 53
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-18 10:49:12',0,0,0,0,114,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (265,0,'[ROUTEUR] Nominal ne répond plus 10.27.1.0','2013-06-18 11:22:00','2013-07-01 09:28:21','2013-06-18 11:23:51','2013-07-01 09:28:21',23,'closed',23,1,0,'',0,'Appel orange. Ouverture incident.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-18 11:23:51',0,0,381981,111,82,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (266,0,'[IE] la GPO domaine bloque l''accès aux options d''IE ','2013-06-18 13:41:00','2013-07-01 09:28:21','2013-06-19 13:57:40','2013-07-01 09:28:21',23,'closed',23,1,0,'',0,'Sur le poste de Josefa JEUX (EUREXO NEMOURS)',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-19 13:57:40',0,0,373641,44200,81,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (267,0,'[SERVEUR] Deploiement de l''outil compatiblité pour les docx (format 2007)','2013-06-18 13:54:00',null,null,'2013-06-18 13:56:46',23,'assign',23,1,0,'',0,'Installé sur 10.25.1.1 / 10.13.1.1 / 10.34.1.1
Tester et déployer sur les serveurs EURISK.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,166,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (268,0,'Plus de téléphone EURISK ROUEN','2013-06-18 14:50:00','2013-07-01 09:28:21','2013-06-20 17:29:47','2013-07-01 09:28:21',24,'closed',24,1,0,'',0,'Plus de téléphone , on ne peut plus les joindre',4,3,4,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-20 17:29:47',0,95249,274252,738,81,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (269,0,'Installation routeur CP COM ORANGE','2013-06-19 11:26:00','2013-07-01 09:28:21','2013-06-19 17:12:54','2013-07-01 09:28:21',24,'closed',24,6,0,'',0,'Réception du technicien ORANGE pour l''installation du nouveau routeur 8 Mega pour le futur basculement de la téléphonie IP',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-19 17:12:54',0,18097,320444,2717,197,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (270,31,'Pb de reprise avec l''ACP','2013-06-19 12:22:00',null,null,'2013-06-19 17:09:49',24,'waiting',24,2,0,'',0,'Bonjour Patrice,

peux-tu rappeler Elise GUILLOTEAU pour faire le point : elle semble avoir des problèmes pour prendre et transférer les lignes.

Merci

Cécile LACROIX
Responsable Administrative
 
Agence de MONTROUGE
Tél : 01 40 92 70 43
Fax : 01 40 92 70 49
e.mail : cecile.lacroix@eurexo.fr

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-19 15:24:48',0,0,0,0,104,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (271,31,'modif Fort enjeu à MONTROUGE','2013-06-19 15:18:00','2013-07-01 09:28:21','2013-06-19 15:20:54','2013-07-01 09:28:21',24,'closed',24,2,0,'',0,'Bonjour Patrice,

Je me rapproche de toi pour soulever trois points :

- Sur la cellule Moyens à Forts enjeux, le standard tenu par Véronique (ligne directe 01 40 92 7045 - lingne interne 213002) ne peut pas être joint après 17h00. Un message vocal stipule que nos bureaux ferment à 17h00.

Joe-Hana prend la permanence téléphonique de 17h00 à 18h00. Peux-tu faire le nécessaire pour que le message vocal ne s''applique qu''à partir de 18h00 sur la cellule Moyens à Forts enjeux ?

- Par ailleurs, Benjamin DUSSART a intégré la cellule de Montrouge hier. Son téléphone ne fonctionne pas (il a amené son poste téléphonique de Levallois, comme convenu avec Philippe la semaine dernière).

Est-il possible de procéder à sa mise en fonctionnement à distance ?

- Où en est-on de l''installation de l''ACP sur mon poste ?

Je reste à ta disposition.

Cordialement

Séverine STORTZ
Responsable Administratif
Cellule Moyens à Forts Enjeux

Ligne directe : 01.40.92.70.44
Télécopie : 01.40.92.61.19
E-mail : s.stortz@eurexo.fr

 
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-19 15:20:54',0,0,324621,174,89,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (272,28,'installation ACP sur 3 postes','2013-06-19 16:53:00','2013-07-01 09:28:21','2013-06-19 16:57:48','2013-07-01 09:28:21',24,'closed',24,2,0,'',0,'Bonjour,

Pour EVELYNE BIANCONI + JUSTINE BROUSSOLLE + CELINE LESSAGE.

Merci pour vos interventions urgence pour Evelyne demandé depuis + de 15 jours 

Cdt.

NGoncalves.
Responsable Administrative
 
Agence de LEVALLOIS
Tél : 01 41 27 34 35 
Fax :  01 41 27 03 22
e.mail : n.goncalves@eurexo.fr
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-19 16:57:48',0,0,318921,288,181,5400,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (273,0,'[SERVEUR] Problème de remontée d''imprimante USB','2013-06-19 16:56:00','2013-07-05 09:18:28','2013-06-24 11:32:40','2013-07-05 09:18:28',23,'closed',23,1,0,'',0,'

De : CPCOM/FAURE Etienne 
Envoyé : mercredi 19 juin 2013 16:44
À : CPCOM/CHOLLET Ludovic
Objet : Installation imprimante sur le serveur Quadra

L’adresse ip de l’imprimante est le 10.76.1.200

Merci de me faire un retour que je prévienne Mme. Lecoustey

Merci.

Etienne
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-24 11:32:40',0,0,490948,110200,60,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (274,0,'Pb ligne EUREXO TOULON','2013-06-20 09:25:00','2013-07-01 09:28:21','2013-06-20 09:53:40','2013-07-01 09:28:21',24,'closed',24,1,0,'',0,'Bonjour,

Ce problème de ligne n’est toujours pas solutionné et commence à être préjudiciable, les agents,  assurés et confrères cherchant à nous contacter tombant sur des messages aléatoires (« numéro non attribué », « conseillers en ligne », « ligne inaccessible ») ou se retrouvent coupés sans tonalité dès lors qu’une seule personne du bureau est déjà en ligne.

Cela pose également problème pour la prise de rendez-vous ou l’on doit attendre que quelqu’un raccroche avant de pouvoir passer soi même un appel, de plus pendant ce temps nous ne pouvons recevoir aucun appel entrant.

Monsieur PERRUISSET a pu le constater lorsqu’il a tenté de nous joindre il y a quelques semaines.

Ce problème a également été signalé à CP COM lors de leur venue en nos locaux mardi dernier et ces derniers ont trouvé la ligne qui a été coupée lors de l’intervention d’Orange pour la mise en place de la ligne IPN.

A ce jour, nous n’avons pas été informés des solutions techniques envisagées pour palier à ce problème.




Vous souhaitant bonne réception du présent envoi,

Sincères Salutations,


ZOLFERINO Coralie
Assistante de Monsieur CHAUVET, expert

 

ZI TOULON EST - BP 265           Tel.   :  04.94.57.00.00
83078 TOULON CEDEX 9           Fax   :  04.94.57.06.43
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-20 09:53:40',0,0,302601,1720,107,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (275,8,'Pb de tranfert poste 200112','2013-06-20 09:54:00',null,'2013-07-01 11:52:50','2013-07-01 11:52:50',24,'solved',24,2,0,'',0,'Bonjour,

Je me permets de vous contacter Mademoiselle Davenne vient d''avoir un souci pour transférer un appel, je viens de redémarrer son portail ACP, elle a pu transférer son appel.

 
Cordialement
Morgan Van Der Perre.
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-01 11:52:50',0,0,0,309530,143,1500,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (276,0,'installation AXS CPCOM','2013-06-20 11:07:00','2013-07-01 09:28:21','2013-06-20 11:10:26','2013-07-01 09:28:21',24,'closed',24,1,0,'',0,'Installation de la méga Gateway AXS CP COM avec Nicolas VARLET ORANGE',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-20 11:10:26',0,0,296481,206,128,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (277,0,'inventaire poste stock CP COM','2013-06-20 11:11:00','2013-07-02 08:49:41','2013-06-21 14:36:22','2013-07-02 08:49:41',6,'closed',24,1,0,'',0,'inventaire poste stock CP COM',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-21 14:36:22',0,0,337121,55522,86,4500,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (278,25,'[SWITCH] : Préparation matériel','2013-06-21 10:20:00',null,'2013-07-03 11:39:56','2013-07-03 11:39:56',768,'solved',6,1,0,'',0,'Dans le cadre de la mise en service de la téléphonie sur IP, merci de préparer les switchs pour les sites de :

Brest
Lanester : IP 10.56.2.15 GW 10.56.2.10
La Farlède (Toulon) : IP 10.83.4.15 GW 10.83.4.10
Marseille : IP 10.13.4.15 GW 10.13.4.10 A VERIFIER
Puget sur Argens : IP 10.83.2.15 GW 10.83.2.10

Jean-Hugues',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-03 11:39:56',0,0,0,350396,155,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (279,25,'[CP2000] : Envoi de mail depuis les PCs Scan de Creteil','2013-06-21 12:04:00',null,null,'2013-06-24 10:43:13',23,'assign',6,1,0,'',0,'Ludovic,

Afin de pouvoir envoyer des mails depuis CP2000 sur les PCs Scan de Créteil, merci :
- D''autoriser dans Exchange les @IP de ces PCs à envoyer des mails.
- De déployer blat, pdfp et pdftk sur les PCs.
- De configurer Kaspersky pour exclure le scan sur ces outils.

Jean-Hugues',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,1042,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (280,8,'plus d''affichage de nom à DOUZY','2013-06-21 14:31:00','2013-07-02 08:49:41','2013-06-21 14:40:28','2013-07-02 08:49:41',6,'closed',24,1,0,'',0,'plus d''affichage de nom à DOUZY',3,3,3,0,1,0.0000,0.0000,0.0000,0,'&lt;p&gt;Initialisation de l''annuaire de l''AXL de Douzy&lt;br /&gt;Controle et essais effectu&#233;s&lt;/p&gt;','none',0,0,null,'2013-06-21 14:33:49',0,0,281921,568,85,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (281,0,'Demande création ISO vm cliente eC@rd','2013-06-24 09:18:00',null,null,'2013-06-24 09:19:55',154,'assign',154,1,0,'',0,'Bonjour pascal
A ce jour j’ai fini les test de virtualisation et tout est fonctionnel sous serveur centos virtualizer (virtuak box) et avec un client seven virtualiser aussi
J’attends ton iso pour continuer les test
Bon week end

Jérôme QUAGLIA
Sté CARD
400 Ave Pasteur
13330 Pélissanne
tel: 04 90 55 34 20
Fax: 04 90 55 20 13',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,115,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (282,0,'Qualification appel','2013-06-24 10:17:00',null,null,'2013-06-24 10:19:12',6,'assign',6,1,0,'',0,'test liaison ticket.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,63,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (283,0,'Test duplication','2013-06-24 10:18:00',null,null,'2013-06-24 10:19:12',6,'new',6,1,0,'',0,'test duplication',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,72,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (284,0,'[MATERIEL] Redaction procédure : Garantie matériel HP','2013-06-24 10:43:00',null,null,'2013-06-24 10:45:31',23,'assign',23,1,0,'',0,'Créer une procédure pour faire intervenir un technicien HP sur nos serveurs en panne.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,151,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (285,0,'[NAGIOS] Erreur de sauvegarde','2013-06-24 14:50:00',null,null,'2013-06-25 13:54:50',23,'assign',23,1,0,'',0,'Plusieurs serveurs ne remonte pas sur Nagios : 
 - 10.6.1.1
 - 10.21.1.1
 - 10.31.1.1
 - 10.31.7.1
 - 10.51.2.1
 - 10.64.1.1
 - 10.92.7.6
 - 10.97.1.1
 - 10.17.1.1
 - 10.62.2.1
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,141,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (286,0,'Souscription de contrat de maintenace Infra CPCOM','2013-06-24 15:55:00',null,null,'2013-06-24 15:57:01',154,'assign',154,1,0,'',0,'Afin de finaliser les déploiements d''infrastructure, il manque la partie maintenance matérielle.',4,4,4,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,121,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (287,44,'erreur dns','2013-06-24 17:19:00','2013-06-25 09:46:07','2013-06-25 09:46:07','2013-06-25 09:46:07',160,'closed',160,1,0,'',0,'Salut,

Pour info c’est plutôt inquiétant car nous avons des entrées DNS qui disparaissent.

Cet après-midi au redémarrage du serveur TWP w2008-3120-100, il a disparu des DNS et tous les raccourcis web sur les postes client ne fonctionnait plus.

Je l’ai recréé manuellement pour dépanner…

Ludovic
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,62,15965,15965,74,4200,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (288,8,'Pb TWP','2013-06-24 17:21:00','2013-07-05 09:18:28','2013-06-24 17:25:41','2013-07-05 09:18:28',24,'closed',24,3,0,'',0,'On ne peux plus ouvrie les application TWP ',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-24 17:25:41',0,0,359848,281,92,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (289,0,'[VALIDATION] Mise à jour XRT 3.1','2013-06-25 11:22:00',null,null,'2013-06-25 11:22:59',23,'assign',23,1,0,'',0,'Prérequis W2008.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,59,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (290,31,'poste 213152 renvoyé impossible de l''annuler','2013-06-25 13:16:00',null,'2013-06-25 13:20:20','2013-06-25 13:20:20',24,'solved',24,2,0,'',0,'Bonjour Patrice,

Nous rencontrons un souci avec le poste téléphonique de Sandra ALSUAR, poste interne 213 152 (cellule Forts Enjeux).

Son poste est en renvoi, que l''on ne peut pas désactiver.

Lorsque l''on désactive avec # 21 COMPOSE, le poste nous informe que la demande est acceptée, mais le renvoi demeure !!!

Merci de revenir vers moi au plus vite STP.

Cdt

Séverine STORTZ
Responsable Administratif
Cellule Moyens à Forts Enjeux

Ligne directe : 01.40.92.70.44
Télécopie : 01.40.92.61.19
E-mail : s.stortz@eurexo.fr
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-25 13:20:20',0,0,0,260,186,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (291,0,'[SERVEUR] Renommer le serveur de Cagne','2013-06-25 13:30:00',null,'2013-07-04 10:14:19','2013-07-04 10:14:19',23,'solved',23,1,0,'',0,'Il faut supprimer le role DC et le renommer car il y a une erreur dans le hostname.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-04 10:14:19',0,0,0,290659,65,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (292,8,'modif N° de Mr DUSSART','2013-06-25 13:43:00',null,'2013-07-01 10:33:16','2013-07-01 10:33:16',24,'solved',24,1,0,'',0,'Changement du N° de Mr DUSSART en 213103',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-01 10:33:16',0,159183,0,2233,105,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (293,0,'[NAGIOS]  91 services UNKNOW','2013-06-25 13:57:00',null,null,'2013-06-25 16:50:47',23,'assign',23,1,0,'',0,'La plupart du temps il s''agit d''une reconfiguration (mot de passe expiré, ...)
A controler sur tous les services unknow.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,107,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (294,0,'[GPO] Problème avec le fond d''écran fujitsu','2013-06-25 15:04:00',null,'2013-06-25 15:37:40','2013-06-25 15:37:40',23,'solved',23,1,0,'',0,'De : CPCOM/REIX Sebastien 
Envoyé : mardi 25 juin 2013 15:02
À : CPCOM/CHOLLET Ludovic
Objet : [43794] - Suppression fond d''écran

Tentative suppression fond écran Fujitsu rouge =&gt; NOK
Cela ne relève d''une stratégie locale mais d''une stratégie réseau.

Hostname =&gt; S-EUREXO-006878

@ ip : 10.78.3.63

session: dleboucher

Cordialement,

Sébastien REIX
Service Informatique
Mail : sebastien.reix@groupeprunay-si.fr
 

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-25 15:37:40',0,0,0,2020,69,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (295,8,'Blandine,rnrnCi-après les points évoqués :rnrn•	NH GENERALI émises le ','2013-06-25 15:06:00','2013-06-25 17:45:48','2013-06-25 17:45:48','2013-06-25 17:45:48',160,'closed',160,1,0,'',0,'Blandine,

Ci-après les points évoqués :

•	NH GENERALI émises le 18/04 et le 19/04
o	Comme indiqué, j’avais déjà détecté il ya 15 jours un problème d’envoi EDI pour ces 2 journées et avait demandé à Stéphane une réémission EDI
	Ces NH ont été envoyées par EDI le 18 et 19/06
	Le flux a été émis. Le flux retour a été reçu. Elles ont été indiquées « payées » (J’ai vérifié quelques NH - voir si virement OK avec relevé de compte)
•	 NH GENERALI émises le 28/05
o	Un flux EDI a été émis le 29/05 pour un montant de 37926,19 €. Il n’y a pas eu de flux retour de traiter

Blandine : le flux EDI émis le 29/05 semble correspondre au virement GENERALI que tu n’arrives pas à lettrer. Confirmation ?

Stéphane : Peux-tu retrouver 
•	le flux retour correspondant au flux aller du 29/05  chez nous ou le redemander à GENERALI ? 
•	Puis l’injecter dans le système

Reste à votre écoute.

Cordialement
 
                Philippe Perruisset
                Directeur des Systèmes d''Information 
                Groupe Eurexo
                Tél Bureau : 01 30 78 42 14
                Tél Portable : 06 24 52 79 12
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,9588,9588,65,2400,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (296,55,'[SERVEUR] Vérifier la compatibilité pour le module d''office','2013-06-25 15:40:00',null,null,'2013-06-25 15:41:46',23,'assign',23,1,0,'',0,'valider et installer le module de conversion XLS',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,106,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (297,0,'[SERVEUR] IE8 : impossible d''acceder a notilus','2013-06-25 17:55:00','2013-07-04 09:23:26','2013-07-04 09:23:26','2013-07-04 09:23:26',160,'closed',156,1,0,'',0,'Erreur de certificat en voulant ouvrir les congés :

https://eurisk.mesconges.net

le problème se produit uniquement sur PTF4.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,271706,271706,100,2400,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (298,0,'[PROCEDURE] Mise à jour deploiement CISCO','2013-06-26 10:17:00',null,'2013-07-04 10:13:51','2013-07-04 10:13:51',23,'solved',23,1,0,'',0,'(ajouter le hostname a la dimo sur les switch & borne avant l''envoi)',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-04 10:13:51',0,0,0,259011,92,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (299,55,'[QUADRA] Problème d''execution sur un script batch','2013-06-26 13:24:00',null,'2013-06-26 13:27:26','2013-06-26 13:27:26',23,'solved',23,1,0,'',0,'N:\\COMPTABILITE CLIENTS\\IMPORT_QUADRA\\
N: = (APL1 sur w2003-781-07).

Il s''agit d''un script pour renommer des fichiers en masse.
Erreur : chemin non résolu',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-26 13:27:26',0,0,0,206,178,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (300,0,'NetApp Log # 2004387337','2013-06-27 08:58:00',null,null,'2013-06-27 17:20:08',154,'assign',154,1,0,'',0,'NetApp Support [SUPPORT@netapp.com]
Sent: 	Thursday, June 27, 2013 3:43 AM
To: 	
CPCOM/PEREZ Pascal
Dear NetApp Customer  perez  pascal:
 
[This is an AutoSupport-generated email.  Please do not reply.]
 
NetApp has received an AutoSupport from the following system:
 
SYSTEM DETAILS:
================
SITE NAME:
	
CPCOM
 
HOSTNAME:
	
NETAPP3120-181
 
SERIAL NUMBER:
	
200000506505
 
MODEL:
	
FAS2040-R5
 
SW VERSION:
	
8.0.2P5 7-MODE
Case number 2004387337 has been assigned to this AutoSupport.  You can review or update this case anytime at:
http://support.netapp.com/portal?_nfpb=true&_pageLabel=caseDetailsPage&initialPage=true&caseNumber=2004387337
For more detailed information on this error, please refer to the KnowledgeBase article
https://kb.netapp.com/support/index?page=content&id=2011413',4,4,4,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,259,9300,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (301,0,'[SERVEUR] Installation / Préparation serveur ESX + VM 2008','2013-06-27 11:23:00',null,null,'2013-07-03 11:13:00',23,'assign',23,1,0,'',0,'Suite à la validation de la mise à jour XRT 3.2, il faut préparer un environnement pour l’accueillir.

Installation d''un serveur ESXi 5.1 + deux VM 2008.

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,110,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (302,0,'[ORANGE] Ouverture de port Kaspersky ','2013-06-27 15:20:00','2013-07-04 09:23:43','2013-06-28 17:17:47','2013-07-04 09:23:43',160,'closed',23,1,0,'',0,'port: 8060
IP : 80.247.233.228',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-28 17:17:47',0,0,194623,50267,277,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (303,8,'renvoi de fax','2013-06-28 08:32:00',null,'2013-06-28 10:46:43','2013-06-28 10:46:43',24,'solved',24,2,0,'',0,'Bonsoir Patrice,

En urgence, peux-tu paramétrer ou faire réaliser le paramétrage suivant :
•	Renvoi automatique immédiat des appels Fax reçus sur le n° 03 10 36 00 64 vers le n° 03 66 41 46 45

Les Fax reçus émis sur le 03 10 36 00 64 seront alors automatiquement transférés vers la société RTE – Faxbox au 03 66 41 46 45 et convertit en *.pdf et transmis à l’@mail swisslife@eurexo.fr

Reste en attente de la prise en compte de cette demande et du top de mise en place.

En te remerciant d’avance.

Cordialement
 
                Philippe Perruisset
                Directeur des Systèmes d''Information 
                Groupe Eurexo
                Tél Bureau : 01 30 78 42 14
                Tél Portable : 06 24 52 79 12
',4,3,4,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-28 10:46:43',0,0,0,8083,94,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (304,0,'Problème Kaspersky','2013-06-28 09:55:00','2013-06-28 09:57:21','2013-06-28 09:57:21','2013-06-28 09:57:21',160,'closed',160,1,0,'',0,'Problème de paramétrage KAV.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,141,141,88,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (305,8,'blocage base EurexA','2013-06-28 10:27:00','2013-06-28 10:29:25','2013-06-28 10:29:25','2013-06-28 10:29:25',160,'closed',160,1,0,'',0,'Bonsoir Stéphane,

Ton avis ?

Cordialement
 
                Philippe Perruisset
                Directeur des Systèmes d''Information 
                Groupe Eurexo
                Tél Bureau : 01 30 78 42 14
                Tél Portable : 06 24 52 79 12

De : Eurexo IDF-Montrouge/LACROIX Cécile 
Envoyé : jeudi 27 juin 2013 17:35
À : Eurexo Direction/PERRUISSET Philippe
Cc : CPCOM/Support; Eurexo IDF-Pôle/LEGRAND Roselyne
Objet : Blocage ARDI suite

Bonsoir,

nous sommes de nouveau complètement bloqués, cette fois sur EUREXA (écran bleu).

Une raison à ce dysfonctionnement ?

Merci de ton retour,

Cdt,

Cécile LACROIX
Responsable Administrative
 
Agence de MONTROUGE
Tél : 01 40 92 70 43
Fax : 01 40 92 70 49
e.mail : cecile.lacroix@eurexo.fr
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,145,145,71,600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (306,0,'ralentissement ligne SDSL','2013-06-28 10:45:00','2013-06-28 10:54:29','2013-06-28 10:54:29','2013-06-28 10:54:29',160,'closed',160,1,0,'',0,'problème de lenteur',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,569,569,515,1200,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (307,0,'[SERVEUR] w2003-271-01 ne démarre plus','2013-06-28 13:25:00',null,'2013-07-01 17:00:57','2013-07-01 17:00:57',23,'solved',23,1,0,'',0,'Problème de boot : Erreur disk 
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-01 17:00:57',0,0,0,56157,45,11100,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (308,0,'[WIFI]  borne de Levallois ne répond plus','2013-06-28 13:36:00',null,'2013-07-05 09:57:39','2013-07-05 09:57:39',768,'solved',23,1,0,'',0,'
De : CPCOM/CHOLLET Ludovic 
Envoyé : mardi 4 juin 2013 15:18
À : CPCOM/ROUGE Brice; CPCOM/AVERLANT Alexandre
Objet : RE: Borne WIFI Levallois

Brice,

Alex m’a rappelé hier mais nous n’avons pas réussi à refaire fonctionné la borne.
J’ai mis le ticket en attente par manque de temps.

Ludo

De : CPCOM/ROUGE Brice 
Envoyé : mardi 4 juin 2013 14:57
À : CPCOM/AVERLANT Alexandre
Cc : CPCOM/CHOLLET Ludovic
Objet : TR: Borne WIFI Levallois

Alexandre,

As-tu pris en compte la demande de Ludovic ?

            

Cordialement,

Brice Rougé
Service Informatique du Groupe Prunay
Responsable Support
Mail : brice.rouge@groupeprunay-si.fr
Tél : 05 61 39 96 33
Portable : 06 03 16 67 53

 

De : CPCOM/CHOLLET Ludovic 
Envoyé : lundi 3 juin 2013 14:13
À : CPCOM/AVERLANT Alexandre
Cc : CPCOM/ROUGE Brice
Objet : Borne WIFI Levallois

Salut Alex,

Nous avons un problème avec la borne WIFI que tu as installé à Levallois (AP-927-14). Il semble qu’elle ne réponde plus au PING.
Pourrais-tu (quand tu auras le temps) les appeler ou y passer pour voir s’ils ne l’ont pas changé de place.

Je ne sais pas trop comment vous procédez d’habitude, veux-tu un ticket Trackit ?

Merci,

Ludo
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-05 09:57:39',0,0,0,202899,126,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (309,0,'Volumetrie Exchange','2013-06-28 13:51:00',null,'2013-07-03 14:43:17','2013-07-03 14:43:17',150,'solved',150,1,0,'',0,'Christophe,

Pourrais-tu me faire un tableau récapitulatif de la volumétrie des serveurs Exchange ?

Par serveur / partions et détail de la volumétrie des bases Exchanges pour les 4 et 5.
Et récap. des partitions pour le Edge (9).

Par avance merci.
--
Pascal.
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-03 14:43:17',0,0,0,132737,102,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (310,0,'Analyse de flux firewall public NFrance ','2013-06-28 15:43:00','2013-07-04 09:23:57','2013-07-02 16:02:33','2013-07-04 09:23:57',160,'closed',154,1,0,'',0,'Pour accès public au proxy KAV en DMZ.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-02 16:02:33',0,0,150057,87573,143,15900,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (311,8,'suppression de postes aux achats','2013-06-28 16:30:00',null,'2013-06-28 16:32:41','2013-06-28 16:32:41',24,'solved',24,1,0,'',0,'Patrice,

Merci de supprimer les 2 postes ci-joint.

Reste en attente de la prise en compte.

Cordialement
 
                Philippe Perruisset
                Directeur des Systèmes d''Information 
                Groupe Eurexo
                Tél Bureau : 01 30 78 42 14
                Tél Portable : 06 24 52 79 12
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-28 16:32:41',0,0,0,161,111,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (312,0,'Pb débordement d''appel P COM','2013-06-28 16:37:00',null,'2013-06-28 16:39:42','2013-06-28 16:39:42',24,'solved',24,2,0,'',0,'Patrice,

J’ai essayé de joindre ludovic qui ne répondait pas je suis alors tombé sur une annonce « Bienvenu chez Eurexo, … si votre appel concerne un dossier … ».
Peux-t remédier à cela dès que le fonctionnement du support est nominal en terme de monitoring des appels entrant par le technitien en « réception d’appel »

Merci,

Christophe MARONI
GROUPE PRUNAY
Directeur des Systèmes d’Information Groupe
GSM 06 78 74 31 97
christophe.maroni@groupeprunay-si.fr
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-06-28 16:39:42',0,0,0,162,109,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (313,2,'Mise à jour licence hermesC','2013-07-01 07:15:00',null,'2013-07-01 07:18:47','2013-07-01 07:18:47',154,'solved',154,1,0,'',0,'blalblabla',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-01 07:18:47',0,0,0,0,0,1200,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (314,0,'Activer la déduplication sur les shares CIFS NetApp + resize des LUN 91/93 du cluster 2008','2013-07-01 07:30:00',null,null,'2013-07-04 17:32:24',154,'assign',154,1,0,'',0,'Pour rationnalisation de l''espace disque avant refonte plateforme.
Utilise pour monitoring des ratios de dédup.

LUN à 18g trop grandes pour le services.
Valider conf. dynamique (autogrow)',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,98647,2700,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (315,8,'Pb encombre Ramonville','2013-07-01 08:11:00',null,'2013-07-03 10:17:03','2013-07-03 10:17:03',24,'solved',24,1,0,'',0,'Bonsoir,

Je viens d’avoir Martin Lucas au téléphone (Responsable du bureau d’Eurexo Ramonville).

Il m’a fait part que de nombreux clients, comme lui-même ne parvenaient pas à joindre l’agence (standard ou collaborateur). Leur appel se terminant par « encombrement » ou « Toutes les lignes de votre correspondant sont occupées. Veuillez rappeler ultérieurement » .  Aussi, des raccrochés immédiats après message d’accueil (ACP).

Pour mémoire, lors du regroupement du cabinet FER avec Eurexo Ramonville (LUCAS), pour des questions de timing, nous n’avons pas augmenté le nombre de canaux voie.

Il est peut-être urgent de revoir cette position à la hausse.
',4,3,4,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-03 10:17:03',0,93384,0,579,515,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (316,8,'modif horaire ACP','2013-07-01 09:54:00',null,'2013-07-01 10:23:15','2013-07-01 10:23:15',24,'solved',24,1,0,'',0,'Bonjour Patrice,

Peux-tu modifier les horaires d’ouverture ACP de toutes les agences IDF ?
•	Horaire d''ouverture : 8h30 à 18h30 du lundi au vendredi  (Attention à tous les DNIS : cf fichier ACP.xls)
•	Agences concernées
o	Compiègne
o	Levallois
o	Melun
o	Montrouge
o	Nemours
o	Versailles

Tu trouveras ci-joint les fichiers de collecte modifiés.
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-01 10:23:15',0,0,0,1755,117,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (317,0,'[DATAEXCHANGER] Problème accès site web','2013-07-01 09:58:00',null,'2013-07-01 10:00:20','2013-07-01 10:00:20',23,'solved',23,1,0,'',0,'Le site ne répond pas. J''ai du redémarrer le services IntraDEX.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-01 10:00:20',0,0,0,140,110,600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (318,8,'RV MENDE sur NIMES','2013-07-01 11:04:00',null,'2013-07-01 11:36:12','2013-07-01 11:36:12',24,'solved',24,1,0,'',0,'Faire un RV de MENDE vers NIMES',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-01 11:36:12',0,0,0,1932,132,2100,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (319,0,'[MESSAGERIE] Autorisé une adresse IP','2013-07-01 11:33:00',null,'2013-07-01 11:33:54','2013-07-01 11:33:54',23,'solved',23,1,0,'',0,'10.60.3.27 : OK',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-01 11:33:54',0,0,0,54,47,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (320,8,'RV sur non réponse postes de DOUZY','2013-07-01 13:15:00',null,'2013-07-01 14:35:42','2013-07-01 14:35:42',24,'solved',24,2,0,'',0,'•	TIP-083 : Téléphonie / Douzy / Télé Experts : renvoi sur non réponse vers standard au bout de 5 sonneries
o	Cette demande est restreinte aux télé experts (cf fichier de collecte ci-joint)
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-01 14:35:42',0,0,0,4842,80,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (321,0,'NA EURISK sur l''ACS','2013-07-01 14:36:00',null,'2013-07-01 16:03:06','2013-07-01 16:03:06',24,'solved',24,1,0,'',0,'création des numèro abrégés EURISK sur ACS',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-01 16:03:06',0,0,0,5226,93,4500,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (322,0,'plus de nom sur les postes','2013-07-02 09:30:00',null,'2013-07-02 09:32:59','2013-07-02 09:32:59',24,'solved',24,1,0,'',0,'plus de nom sur les postes et TWP ',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-02 09:32:59',0,0,0,179,105,900,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (323,0,'modifier le message d''acceil CP COM','2013-07-02 12:13:00',null,'2013-07-03 11:49:51','2013-07-03 11:49:51',24,'solved',24,2,0,'',0,'Patrice,

Le message MES_ACC est de 17’’ avec à peine 6’’ utiles : peux-tu recouper le début et la fin pour descendre à 8’’ maxi ?

Merci,
C.M.
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-03 11:49:51',0,0,0,41811,54,5400,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (324,0,'Internalisation service XFERT','2013-07-02 14:40:00',null,null,'2013-07-02 14:41:39',154,'assign',154,1,0,'',0,'A migrer en DMZ',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,99,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (325,0,'[OFFICE] Valider et déployer l''addon ODF pour Office','2013-07-02 16:50:00',null,null,'2013-07-02 16:51:44',23,'assign',23,1,0,'',0,'.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,104,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (326,8,'modif renvoi sur non réponse des télé experts DOUZY','2013-07-03 09:12:00',null,'2013-07-03 09:17:07','2013-07-03 09:17:07',24,'solved',24,2,0,'',0,'•	TIP-083 : Téléphonie / Douzy / Télé Experts : renvoi sur non réponse vers standard au bout de 5 sonneries
o	Cette demande est restreinte aux télé experts (cf fichier de collecte ci-joint)
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-03 09:17:07',0,0,0,307,184,1800,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (327,29,'message inaudible EUREXO','2013-07-03 10:10:00',null,'2013-07-03 10:15:41','2013-07-03 10:15:41',24,'solved',24,1,0,'',0,'Patrice,

Pour info, le message après correction était inaudible.
As-tu une explication sur cette nouvelle anomalie. 
D’avance merci,
Bien cordialement

Roselyne Legrand
Tel 01.39.24.22.28
Port 07.87.53.64.94
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-03 10:15:41',0,0,0,341,153,900,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (328,8,'modif supervision ACP ','2013-07-03 11:52:00',null,'2013-07-03 14:55:44','2013-07-03 14:55:44',24,'solved',24,1,0,'',0,'Bonjour à tous les deux,

Merci de bien vouloir déployer l’outil ACP – Supervision aux 6 personnes ci-jointes
•	Installation du raccourci ACP sur les PCs
•	Paramétrage site central

Je reste en attente de la prise en compte de la demande et de la confirmation du déploiement quand cela aura été effectué.

',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-03 14:55:44',0,0,0,11024,51,7200,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (329,0,'Probleme de conection lecteur réseaux Hhasnaoui','2013-07-03 14:43:00',null,null,'2013-07-03 14:45:20',150,'assign',150,1,0,'',0,'Problème de conection lecteur réseaux Hhasnaoui sur nas-312-03',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,140,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (330,0,'Suppression de mail','2013-07-03 14:48:00',null,null,'2013-07-03 15:06:55',150,'new',150,1,0,'',0,'Laetitia PLAISSAIS 
Antoine LABASTE
Damien GILLIG
d’Elisabeth LIENHARDT',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,0,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (331,10,'inventaire téléphonique EUREXO CHARTRES','2013-07-03 15:33:00',null,null,'2013-07-03 16:11:57',24,'waiting',24,2,0,'',0,'Bonsoir 

L’agence de Chartres va être rattachée à l’organisation de la Région Parisienne.

Il y aurait lieu de faire un inventaire détaillé des postes informatiques et bureautiques avec une urgence qui se situe au niveau du standard qui montre des faiblesses récurrentes alors que cette agence a un surcroit d’activité important à la suite d’orages de Grêle.

Pouvez-vous regarder en urgence ce dernier point et programmer l’étude du reste pour les semaines qui suivent ?
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-03 16:11:57',0,0,0,0,146,3600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (332,0,'erreur compression JPG Eurexo','2013-07-04 09:10:00','2013-07-04 09:13:35','2013-07-04 09:13:35','2013-07-04 09:13:35',160,'closed',160,1,0,'',0,'Peux-tu voir le soucis ou documenter ou donner accès aux documents afin que nous puissions traiter l’incident ?
Je n’ai absolument pas le temps de faire des fouilles.

Merci,

ludo

De : CPCOM/REIX Sebastien 
Envoyé : mercredi 3 juillet 2013 11:12
À : admin
Objet : Erreur détectée sur la compression des photos jpeg

Bonjour,

Une erreur a été détectée concernant la compression des photos. (10.92.2.3)

Merci  de votre retour.

Cordialement,

Sébastien REIX
Service Informatique
Mail : sebastien.reix@groupeprunay-si.fr
 
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,215,215,74,900,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (333,0,'erreur aechivage','2013-07-04 09:24:00',null,'2013-07-04 09:52:23','2013-07-04 09:52:23',160,'solved',160,1,0,'',0,'Stéphane,

J’ai un soucis pour faire l’archivage, j’ai eu ce message pour ouvrir ardi :

 

 

Sais-tu d’où peux venir le soucis ?

Ludo
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-04 09:52:23',0,0,0,1703,88,1200,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (334,0,'[Switch] : Configuration switch','2013-07-04 09:24:00',null,'2013-07-05 09:57:03','2013-07-05 09:57:03',768,'solved',6,1,0,'',0,'Merci de préparer les switchs et borne WiFi pour les sites suivants :

Agen : 1 switch + 1 borne
Nice : 2 switchs
Chartes (Eurexo) : 1 switch.

Jean-Hugues',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-05 09:57:03',0,0,0,45183,145,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (335,55,'[RESTAURATION] Récupérer un document sur le backup du siege','2013-07-04 10:46:00',null,'2013-07-04 10:50:38','2013-07-04 10:50:38',23,'solved',23,1,0,'',0,'bla',3,3,3,0,2,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-04 10:50:38',0,0,0,278,73,600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (336,8,'erreur kareo NH','2013-07-04 11:08:00',null,null,'2013-07-05 10:22:01',160,'assign',160,1,0,'',0,'Bonsoir 

Merci de regarder

Cordialement
 
                Philippe Perruisset
                Directeur des Systèmes d''Information 
                Groupe Eurexo
                Tél Bureau : 01 30 78 42 14
                Tél Portable : 06 24 52 79 12

De : Eurexo Direction/HERARD Régine 
Envoyé : mardi 2 juillet 2013 13:59
À : Eurexo Direction/PERRUISSET Philippe
Cc : Eurexo Direction/LE ROUZIC Joël; Eurexo Direction/ROYER Hélène; CPCOM/GIRAUDEAU Stephane
Objet : TR: flux kareo


Philippe,
Je fais suite au mail de Karen ci-dessous et t’informe que les NH émises via EDI les 28 et 29 juin derniers n’ont pas eu de retour (la ligne Envoi NH apparait bien dans l’historique mais ni paiement ni rejet).
Je te laisse la main sur ce problème.
Dans l’attente de ton retour,
Cordialement, 
Régine HERARD 
 
Chargée de Formation
Direction des Systèmes d''Information et Administration
P : 06.25.19.46.58

De : Eurexo Services/HERVAUX Karen 
Envoyé : mardi 2 juillet 2013 09:35
À : Eurexo Direction/HERARD Régine
Objet : flux kareo

Coucou,

comment vas tu?

car la dernière fois que je t''ai appelé, tu n avais pas l''air en forme!!!

sinon un petit mail pour te poser une question: j''ai passé des hono 8 en hono 13 le 27/06/2013,le 28/06/2013 . quand tu vas vérifier dans le dossier si la nh a été réglée tu n'' as que la ligne "envoi" en histo mais tu ne sais pas si c''est "rejetée" ou "payée ". est ce normal??
exemple: 252460

bisous

Cordialement
 
Karen HERVAUX
EUREXO SERVICES
Chargée de Recouvrement
03.24.26.92.58
k.hervaux@eurexo.fr
 
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,92,600,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (337,8,'validation maj Kareo','2013-07-04 11:12:00',null,null,'2013-07-04 11:15:07',160,'assign',160,1,0,'',0,'Stéphane,

Merci de faire un retour à Philippe et Card sur la validation et la mise en production.

Jean-Hugues

De : Eurexo Direction/PERRUISSET Philippe 
Envoyé : mercredi 3 juillet 2013 18:51
À : sfb@card.fr; CPCOM/DELCOURT Jean-Hugues
Cc : Eurexo Balan/PONCELET Annick; FL; PT
Objet : RE: DEM-261 - Nouvelle demande Urgente - Envoi auto SMS - Cie = MACIF GATINAIS CHAMPAGNE - Evnt Rdv (idem Generali) + Signature rap.

Transmis à JH.Delcourt pour test sur env. de formation.

Restons à l’écoute du top rapide de JH Delcourt pour mise en prod sur ARDI.

Cordialement
 
                Philippe Perruisset
                Directeur des Systèmes d''Information 
                Groupe Eurexo
                Tél Bureau : 01 30 78 42 14
                Tél Portable : 06 24 52 79 12

De : sfb@card.fr [mailto:sfb@card.fr] 
Envoyé : lundi 1 juillet 2013 11:47
À : Eurexo Direction/PERRUISSET Philippe
Cc : Eurexo Balan/PONCELET Annick; FL; PT
Objet : Re: DEM-261 - Nouvelle demande Urgente - Envoi auto SMS - Cie = MACIF GATINAIS CHAMPAGNE - Evnt Rdv (idem Generali) + Signature rap.

  
Bonjour Philippe,
 
Je viens d''avoir Stéphane GIRAUDEAU au téléphone pour la mise en place de l''application demandée.
 
Il m''indique qu''il n''y a pas de mise en place d''application sur le serveur 2 (serveur Eurexa) mais que tout se fait à partir du Serveur 1. Pas de soucis on va faire le nécessaire en ce sens.
 
Par contre il m''indique qu''il n''y a pas de mise en place sans leur accord.
CPCOM ne semble pas au courant de cette demande.
 
Comme convenu à l''instant avec Stéphane, je ne fais rien sans un retour de leur part.
 
Restant dans l''attente.
 
Cordialement.
 
 
Sébastien FAURE-BRAC
400 Avenue Pasteur – 13330 Pélissanne
Tel: 04 90 55 34 20 - Fax : 04 90 55 20 13
http://www.Card.Fr 
----- Original Message ----- 
From: Eurexo Direction/PERRUISSET Philippe 
To: sfb@card.fr 
Cc: Eurexo Balan/PONCELET Annick 
Sent: Wednesday, June 26, 2013 2:41 PM
Subject: RE: DEM-261 - Nouvelle demande Urgente - Envoi auto SMS - Cie = MACIF GATINAIS CHAMPAGNE - Evnt Rdv (idem Generali) + Signature rap.

Sébastien,

Je confirme : « l''élément déclencheur sera la date de signature » et je prends note que « les dates qui seraient saisies à postériori ne seront pas prises en compte »

Merci de m’indiquer quand cela sera livré en prod. de façon à ce qu’Annick puisse vérifier le fonctionnement.

Je rappelle que la demande porte sur une seule compagnie MACIF GATINAIS CHAMPAGNE sur le SI ARDI EUREXA…

Cordialement
 
                Philippe Perruisset
                Directeur des Systèmes d''Information 
                Groupe Eurexo
                Tél Bureau : 01 30 78 42 14
                Tél Portable : 06 24 52 79 12

De : sfb@card.fr [mailto:sfb@card.fr] 
Envoyé : mercredi 26 juin 2013 11:59
À : Eurexo Direction/PERRUISSET Philippe
Objet : Re: DEM-261 - Nouvelle demande Urgente - Envoi auto SMS - Cie = MACIF GATINAIS CHAMPAGNE - Evnt Rdv (idem Generali) + Signature rap.

 
Sébastien FAURE-BRAC
400 Avenue Pasteur – 13330 Pélissanne
Tel: 04 90 55 34 20 - Fax : 04 90 55 20 13
http://www.Card.Fr 
Bonjour Philippe,
Je viens vers toi pour cette demande.
Pour l''envoi du SMS concernant le rapport, l''élément déclencheur sera la date de signature du rapport comme tu nous l''as indiqué. Il est bon de savoir que dans ce cas, les dates qui seraient saisies à postériori ne seront pas prises en compte.
Confirmes-tu la mise en place immédiate ?
Restant à ta disposition.
 
 
----- Original Message ----- 
From: Eurexo Direction/PERRUISSET Philippe 
To: pt@card.fr ; fl@card.fr ; jpb@card.fr 
Cc: CPCOM/MARONI Christophe ; Eurexo Balan/PONCELET Annick 
Sent: Friday, June 07, 2013 4:44 PM
Subject: DEM-261 - Nouvelle demande Urgente - Envoi auto SMS - Cie = MACIF GATINAIS CHAMPAGNE - Evnt Rdv (idem Generali) + Signature rap.

Bonjour à tous,

Veuillez trouver ci-joint une nouvelle demande 

DEM-261	07/06/13	Poncelet.A	ARDI - Envoi auto SMS - Cie = MACIF GATINAIS CHAMPAGNE - Evnt Rdv (idem Generali) + Signature rap.	01-ARDI	ARDI - Envoi auto SMS - Cie = MACIF GATINAIS CHAMPAGNE - Evnt Rdv (idem Generali) + Envoi rapport.
¤ Envoi automatique d''un SMS – Portable assuré
     = Rdv : idem Generali 
                + à la prise de rdv, 
                + à J-x du Rdv
     = Envoi de rapport : déclencheur à définir (date signature ?) + 
        texte du Sms ?

Sébastien peut-il réaliser rapidement cette demande ?

Cordialement
 
                Philippe Perruisset
                Directeur des Systèmes d''Information 
                Groupe Eurexo
                Tél Bureau : 01 30 78 42 14
                Tél Portable : 06 24 52 79 12



-----Message d''origine-----
De : Eurexo Balan/PONCELET Annick 
Envoyé : jeudi 6 juin 2013 12:10
À : Eurexo Direction/PERRUISSET Philippe
Objet : SMS 

Bonjour Philippe,

Je suis actuellement a la Macif de Nemours nous confirmant la mise en place de SMS pour les Rdv et dépôt rapport. 

Merci de me contacter cela devient très très urgent. 

Cordialement,

Annick Poncelet
',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,143,900,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (338,0,'[Borne WiFi] : Configuration Borne Levallois','2013-07-04 16:26:00',null,'2013-07-05 09:56:40','2013-07-05 09:56:40',768,'solved',6,1,0,'',0,'
Merci de configurer une borne wifi pour levallois en remplacement de celle actuellement en place :

AP-927-14 : 10.92.7.14.

La faire envoyer sur site par les achats pour instalaltion par Alexandre et inclure le bon de retour pour celle defectueuse.

Jean-Hugues',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-05 09:56:40',0,0,0,19840,131,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (339,8,'3ème vague','2013-07-04 17:28:00',null,null,'2013-07-04 17:29:39',24,'assign',24,1,0,'',0,'inventaire 3ème vague',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,69,2700,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (340,16,'Pb RV MENDE vers RODEZ','2013-07-05 09:00:00',null,'2013-07-05 09:07:03','2013-07-05 09:07:03',24,'solved',24,1,0,'',0,'elle n''arrive pas à faire le RV de MENDE à RODEZ',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,'2013-07-05 09:07:03',0,0,0,423,191,900,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (341,0,'Problème de connexion Projets SGireudeau','2013-07-05 10:24:00',null,null,'2013-07-05 10:24:50',150,'new',150,1,0,'',0,'Problème de connexion Projets SGireudeau',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,0,0,0);
insert into `glpi_tickets`(`id`,`entities_id`,`name`,`date`,`closedate`,`solvedate`,`date_mod`,`users_id_lastupdater`,`status`,`users_id_recipient`,`requesttypes_id`,`suppliers_id_assign`,`itemtype`,`items_id`,`content`,`urgency`,`impact`,`priority`,`itilcategories_id`,`type`,`cost_time`,`cost_fixed`,`cost_material`,`solutiontypes_id`,`solution`,`global_validation`,`slas_id`,`slalevels_id`,`due_date`,`begin_waiting_date`,`sla_waiting_duration`,`waiting_duration`,`close_delay_stat`,`solve_delay_stat`,`takeintoaccount_delay_stat`,`actiontime`,`is_deleted`) values (342,0,'Problème archivage Ardi','2013-07-05 10:22:00',null,null,'2013-07-05 10:31:35',160,'assign',160,1,0,'',0,'L''archivage était en cours et une personne la arrêtée brutalement.

Vu avec Pascal Trocme, il regarde comment on peut faire une reprise des documents.',3,3,3,0,1,0.0000,0.0000,0.0000,0,null,'none',0,0,null,null,0,0,0,0,575,0,0);
