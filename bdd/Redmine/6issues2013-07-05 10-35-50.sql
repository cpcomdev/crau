insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1,2,4,'Flux XML des dossiers','Ajout de données dans le flux XML de la remontée journalière des dossiers :

* <DOS_ADRNUM> = champs indiquant le n° du bureau de gestion du dossier
* <DOC_PROV> = provenance du document
* <EXP_PRENOM> = prénom de l''expert
* <EXP_NOM2> = Nom de l''expert (seul)
* <EXP_MOB> = téléphone portable de l''expert

DOS_ADRNUM sera utilisé pour faire le lien avec le bureau de l''agence (ADR_AGENCE), il faut afficher l''email du bureau de gestion dans les infos du dossier en lieu et place de l''email de l''agence.
Exemple si DOS_ADRNUM = 20 alors il faut afficher l''email plateforme@eurisk.fr

EXP_PRENOM et EXP_NOM2 sont les valeurs  prénom et nom de l''expert présent dans le champs déjà existant EXP_NOM. Il doivent être utilisés pour pouvoir accéder à la liste des experts par ordre alphabétique des noms lors de la recherche par "l''identité" et non par les prénoms.

EXP_MOB sera utilisé pour la fonction SMS à venir. Il ne doit pas être affiché dans le dossier.

DOC_PROV sera remonté dans l''import. Aucun affichage dans Hermès. Sera utilisé par le support informatique pour des procédures de contrôles sur l''existence des documents.','2012-04-30',1,5,null,4,25,4,18,'2011-02-17 11:53:06','2012-05-04 13:50:22','2011-09-05',100,null,null,1,1,2,0,'2012-04-23 16:00:14');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (2,2,4,'Accès transparent','L’accès transparent contient actuellement 3 paramètres (login, motdepasse, numero de sinistre). Il faut rajouter l’email à utiliser pour remplacer hermes@eurisk.fr sur les utilisateurs non connus chez Hermès.

Le nouveau paramètre est facultatif, il aura pour étiquette la valeur « m »
La valeur du paramètre email sera encodé au format hexadecimal comme c’est le cas pour les autres paramètres login et mot de passe.

Exemple : 
https://hermes.eurisk.fr/hermes/dossier.jsp?login=%44%69%72%65%63%74%44%6f%73&pwd=%33%67%71%38%21&idclient=C1070040326&m=%70%6C%61%74%65%66%6F%72%6D%65%40%65%75%72%69%73%6B%2E%66%72

Les anciennes urls sans le nouveau paramètre doivent continuer de fonctionner.
',null,2,1,null,4,12,4,5,'2011-02-17 11:53:48','2011-07-05 16:36:45','2011-09-05',0,null,null,2,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (3,2,4,'Envoi de SMS','Il faut pouvoir contacter un expert par SMS.

h2. Accès de la fonction 

Ne pas afficher le numéro de portable de l''expert.
A coté du nom de l''expert, rajouter une icone SMS.
Sur clic de l''icone, ouvrir un écran de saisie de SMS.

S''inspirer de la pièce jointe pour la création de l''écran de saisie des SMS.

!exemple_ecran_sms.png!


h2. Envoi SMS

Nous utilisons le protocole SMTP pour envoyer un SMS. Il faut donc créer un mail pour transmettre les SMS aux experts.

Les SMS doivent être préfixé automatiquement de la norme suivante :
[HERMES CODEAGENCE/NUMERODOSSIER]
Exemple : [HERMES 0830/310960]

Voici les paramètres à utiliser :
* Expéditeur du mail= hermes@eurisk.fr
* Destinataire = numerosms@rtesms.com
* Corps du mail = Préfixage du message + caractères saisis par l''utilisateur. Max 159 caractères.
* Objet = [HERMES] SMS CODEAGENCE/NUMERODOSSIER

h2. Stockage SMS

Les SMS seront stockés dans le dossier, voir pour créer un nouvel onglet à l''instar des mails avec les infos suivantes :
* Date / Heure
* Nom destinataire
* Téléphone destinataire
* Message',null,3,1,null,4,12,4,9,'2011-02-17 12:07:26','2011-07-05 16:36:59','2011-09-05',0,null,null,3,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (4,1,2,'Arret CP2000 suite à envoi mail + remise à blanc (JMOggiano)','L’utilisateur envoi des mails… il sort du dossier en faisant « remise a blanc » pour effacer tout le porte onglet. Il cherche un nouveau dossier, continue à travailler dessus, notamment à envoyer d’autres mails… et l’erreur arrive. Il faut chercher aussi avec la manipulation de l’onglet document/mail.

Cf Etienne : Ticket 19771 dans Track-it
',null,6,5,5,5,5,4,7,'2011-02-17 14:14:27','2011-02-21 18:29:41','2011-02-17',100,null,null,4,1,2,0,'2011-02-21 16:37:51');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (5,1,2,'suppression envoi epost (maileva) impossible à la martinique','Marie-aline - Martinique

Impossible de supprimer un envoi Epost… 9720/359387 envoi n°97200000007318
',null,7,4,null,3,null,4,13,'2011-02-17 14:16:13','2011-09-01 13:50:36',null,10,16,null,5,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (6,2,2,'Fusion de documents','Dominique Santoni, le 29-11-2010.

Laisser la possibilité aux secrétaires de rassembler plusieurs documents en 1 seul.
','2012-03-30',5,5,5,5,42,4,44,'2011-02-17 15:42:04','2012-06-17 11:34:25','2012-03-05',100,24,null,6,1,2,0,'2012-06-01 14:33:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (7,2,3,'choix mail interne/externe','Dans la macro d''archivage des mails présente dans Outlook, l''utilisateur doit pouvoir changer la notion "interne/externe"',null,null,1,null,4,3,4,1,'2011-02-17 16:16:29','2011-02-17 17:26:18','2011-03-14',0,null,null,7,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (8,2,4,'Lien annuaire Eurisk','M. COLLET Xavier ACS souhaiterais avoir un lien sur HERMES pour accéder directement à l’annuaire qui est en ligne sur le site EURISK.',null,null,1,null,4,23,4,5,'2011-02-17 16:25:01','2011-07-05 16:48:00','2011-10-31',0,null,null,8,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (9,2,2,'Import OM Hermès','Importer les OM en provenance d''Hermes',null,4,5,5,5,null,4,19,'2011-02-17 17:21:39','2012-10-23 12:00:45',null,60,80,null,9,1,4,0,'2011-11-18 11:28:07');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (10,2,2,'Séparation de documents','Dominique Santoni, le 29-11-2010.

Laisser la possibilité aux secrétaires de séparer 1 document externe en plusieurs documents.
','2011-04-15',5,5,5,5,7,4,17,'2011-02-17 17:29:59','2011-04-16 19:30:43','2011-02-21',100,32,null,10,1,2,0,'2011-04-15 11:10:28');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (11,2,2,'Accès dossier technique (Chemise 6) pour tous les dossiers d''une opération','Le dossier G est en fait constitué de tous les documents de la chemise N° 6 « Dossier Technique » des diverses missions attachées à ce dossier G. A partir de l’une quelconque des missions on a accès à la liste de tous les documents techniques enregistrés dans une mission ou l’autre. 

h2. A faire 

* Ecran opération (CPXG009)
** Afficher seulement 1 occurence des POL au lieu de 2
** Ajouter un espace pour les documents (lister 5 occurences)
*** Afficher les documents ayant la nature ''EX5'' appartenant à tous les dossiers de l''opération
*** Afficher les champs codeagence, dosnum, doclib, docomp,
*** Sur double clic, permettre d''ouvrir le document
docdtcre
* Ecran envoi (CPXL010B)
*** Afficher en plus dans les documents externes, les documents de nature ''EX5'' contenu dans les autres dossiers de l''opération
','2011-11-18',5,5,5,6,26,4,18,'2011-02-17 17:36:41','2011-11-17 10:06:05','2011-11-01',100,80,null,11,1,2,0,'2011-11-17 10:06:05');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (12,1,2,'perte de l''intervenant sur changement d''onglet','Dans un dossier RCD, on ajoute et selectionne un intervenant.
Si on change d''onglet, on a le message "Champs obligatoire" car le rôle n''est pas renseigné, mais au réaffichage de l''écran, la selection de l''intervenant est perdue, effacée...','2011-04-15',8,5,5,4,7,4,10,'2011-02-18 11:03:30','2011-04-16 19:29:58','2011-02-18',100,16,null,12,1,2,0,'2011-04-06 00:04:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (13,1,2,'Navigation dans les dommages HS','Dans un dossier avec plusieurs dommage, impossible de naviguer correctement de dommage en dommage.
On reste coincé sur le dernier dommage','2011-02-21',9,5,5,6,4,4,4,'2011-02-21 11:42:23','2011-02-21 13:04:56','2011-02-21',100,1,null,13,1,2,0,'2011-02-21 11:53:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (14,2,2,'Intégrer des documents par lot','Marseille demande si c''est possible d''intégrer dans CP2000 des lots de documents en provenance d''un CD (par exemple).

',null,5,1,null,4,null,4,2,'2011-02-21 14:22:09','2012-08-28 15:22:08','2011-02-21',0,null,null,14,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (15,2,2,'Enrichissement info DT pour la plateforme','Dans le DT838 Minute Enq. Tel.DO/PUC, il faut afficher les infos saisies par la plateforme dans le module PFT.','2011-03-08',5,5,5,6,6,4,12,'2011-02-21 14:41:49','2011-03-10 13:20:38','2011-02-23',100,8,null,15,1,2,0,'2011-02-25 15:49:18');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (18,1,2,'Impossible d''intégrer un document sur le PC Scan.','Arros, Caroline Vaquero :

Pb PC Scan : Elle peut numériser à partir de CP2000, mais elle ne peut pas intégrer un document','2011-02-23',10,5,4,6,5,4,2,'2011-02-21 16:55:28','2011-02-21 18:26:50','2011-02-21',100,16,null,18,1,2,0,'2011-02-21 18:26:50');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (19,2,2,'Cocher public pour un mail déjà envoyé','Dans l''écran document/mail interne, les cases à cocher "hermes" et "public" doivent pouvoir etre modifiable','2011-04-15',5,5,5,4,7,6,14,'2011-02-22 12:05:02','2011-04-16 19:31:05','2011-03-08',100,8,null,19,1,2,0,'2011-04-06 00:05:06');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (20,1,2,'Extension document externe','Certains documents externes ont une mauvaise extension de fichier.
Cela bloque la remontée Hermes.','2011-03-08',5,5,4,4,6,4,9,'2011-02-22 14:11:42','2011-03-10 13:17:58','2011-02-22',100,8,null,20,1,2,0,'2011-03-09 18:13:21');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (21,2,2,'Filtrage de certaines pièces jointes à l''intégration des mails','Voir pour rendre filtrable certaines pièces jointes à l''intégration des mails
(exemple filtrage sur le nom de la pièce jointe)

Utile pour filtrer les logo des signatures, des mails automatiques comme les AR de fax...
',null,6,5,4,3,null,4,1,'2011-02-22 15:34:49','2011-05-31 18:20:32','2011-02-24',100,24,null,21,1,2,0,'2011-05-31 18:20:32');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (22,1,2,'envoi de fax même quand il n''y a pas de numero','CAS que je n''ai pas pensé à tester et qui s''est produit à l''agence centrale qui a FAXE sans mettre de n° de fax : il n''y a pas eu de message signalant qu''il manque le n° et elle n''a pas de message d''erreur dans sa bte mail

J''ai fait idem sur le site test : le fax a été envoyé et je le retrouve dans ma liste des envois!!! il est parti chez qui??? dans la liste des envois,je le retrouve avec comme destinaire 8300000210145 (voir dossier 309950)

D''autre part de l''Agence centrale  j''ai fait un envoi sur TOULON : ça ne marche pas
','2011-03-08',11,5,4,5,6,6,6,'2011-02-22 16:05:48','2011-03-10 13:18:18','2011-02-22',100,null,null,22,1,2,0,'2011-03-09 18:12:32');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (23,2,1,'Homepage','Voir pour rajouter une portlet, éditable ... liens pointant vers les sites externes du groupe prunay.',null,null,3,3,4,16,3,6,'2011-02-22 17:09:52','2011-07-12 16:20:49','2011-02-22',100,4,null,23,1,2,0,'2011-07-12 16:20:49');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (24,1,1,'Proxy AJP','activé proxy ajp dans tomcat/conf/server.xml','2011-02-23',null,5,3,4,null,3,3,'2011-02-22 17:11:35','2011-02-25 16:16:10','2011-02-22',100,1,null,24,1,2,0,'2011-02-23 09:40:39');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (25,1,1,'CIFS & NTLM','Incompatibilié entre CIFS et NTLM ?
','2011-02-28',null,1,3,5,null,3,2,'2011-02-22 17:12:28','2011-02-23 11:22:48','2011-02-22',40,8,null,25,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (26,1,1,'Mettre à jour la maquette','Mettre à jour la maquette => redirection apache / proxy ajp','2011-02-28',null,5,3,4,null,3,2,'2011-02-22 17:13:31','2011-02-25 16:16:19','2011-02-22',100,4,null,26,1,2,0,'2011-02-23 15:28:34');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (28,2,3,'Possibilité de supprimer le popup','Pour les messageries qui n''ont pas possibilité d''accéder à l''extérieur, il ne faut plus que le formulaire d''archivage leur pose la question.
',null,null,10,4,4,3,4,7,'2011-02-23 09:46:54','2011-07-04 11:57:27','2011-03-14',90,16,null,28,1,2,0,'2011-03-30 09:54:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (29,2,5,'Gestion des accès','L''application doit être restreint selon accès login / mot de passe.

h2. Gestion des droits

Chaque fonction ou notion de l''application sera soumis à un droit.
Nous pouvons distinguer 4 nuances sur les droits (Cf CRUD)
# Créer
# Lire
# Modifier
# Supprimer

h2. Gestion des profils

Un profil est un regroupement de droits.
Liste des profils à prendre en compte :

* Administrateur
* Support
* Expert
* Secrétaire
* Gestionnaire
* Chef d''agence
* Plateforme
* Siège (compta, réclamations, juridique)
* Applicatif (pour accès aux batchs, ou autre accès directs)
* Sansprofil (pour permettre de rendre certaines infos de l''appli disponible à quiconque)

On doit pouvoir définir un profil par défaut à affecter aux nouveaux utilisateurs.
 
h2. Gestion des utilisateurs

L''utilisateur se connecte en utilisant son identifiant et mot de passe messagerie.

h3. Si l''utilisateur est inconnu de LDAP

Retour erreur sur la page de login.

h3. Si l''utilisateur est connu de LDAP mais inconnu de l''application

Les infos de LDAP sont récupérées (Nom, prénom, email), l''utilisateur est créé dans l''application et affecté au profil par défaut.
L''utilisateur est redirigé sur la page d''accueil avec les droits disponibles de son profil.

h3. Si l''utilisateur est connu de LDAP et présent dans l''application

L''utilisateur est redirigé sur la page d''accueil avec les droits disponibles de son profil.

h2. Donc à faire :

* Création de la table Profil
* Création de la table des utilisateurs (Id, Nom, prénom, email, Role dans l''application)
* Formulaire login/mot de passe
* Module de connexion à LDAP
* Page d''échec de connexion
* Si connecté, page d''accueil qui liste les infos utilisateurs : Id, Nom, prénom, email, Role dans l''application
* Si connecté, lien "déconnexion" pour sortir de l''application.
* Tests unitaires

Deux solutions:
- gérer un messages d''erreurs
- configurer les pages d''erreurs directement dans symfony',null,14,5,8,4,34,4,32,'2011-02-23 17:06:37','2013-04-17 15:01:28','2013-03-15',50,80,null,29,1,8,0,'2012-03-12 09:29:15');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (30,1,2,'Bloquer l''accès paramétrage Darva en agence','Dans le menu administration > consultation des sites administratifs, le bouton paramétrage DARVA est en libre consultation/modification. Les agences ne doivent pas avoir accès à ce paramétrage uniquement disponible au siège !!!

Composant CPXG103','2011-03-08',15,5,4,5,6,4,5,'2011-02-24 10:17:30','2011-03-10 13:18:36','2011-02-24',100,1,null,30,1,2,0,'2011-02-24 17:54:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (31,3,2,'Revoir procédure controle de date de remise en poste Flydoc','Cf Cédric et Geneviève.

La procédure actuelle semble ne pas coller à la réalité.
Refaire le point sur la démarche à adopter en cas d''alerte de courrier non remis en poste.','2011-02-24',7,5,4,6,null,4,3,'2011-02-24 13:22:57','2011-02-25 14:29:32','2011-02-24',100,1,null,31,1,4,0,'2011-02-25 14:29:32');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (32,3,2,'Rattrapage des dates en agences','Nous n’arrivons pas à récupérer certaines date de remise en poste dans notre SI sur des courriers traité en début de semaine, voici la liste :

<pre>
        672867 0490                4900000004592 2011-02-18 12:45:00 
        295593 0740                7400000027074 2011-01-25 15:58:18 
        295593 0740                7400000027077 2011-01-25 16:02:55 
        691694 0250                2500000006232 2011-02-21 15:57:04 
           115 0190                1900000001990 2011-02-21 16:58:05 
        618715 0060                 600000014782 2011-02-21 13:44:09 
        177874 0730                7300000013651 2011-02-21 12:18:54 
           409 0280                2800000003277 2011-02-18 13:27:19 
        188632 0030                 300000005069 2011-02-22 09:13:26 
        622019 0400                4000000006761 2011-02-22 09:29:58 
          2240 0210                2100000015586 2011-02-21 13:28:13 
        644552 0780                7800000028462 2011-02-16 09:04:58 
        249466 0750                7500000025823 2011-02-21 12:09:54 
        494130 0910                9100000007498 2011-02-22 09:02:41 
        336292 0690                6900000032577 2011-02-10 09:23:29 
        335726 0690                6900000033068 2011-02-18 12:37:26 
        133850 0130                1300000023259 2011-02-18 12:05:50 
        232320 0570                5700000020212 2011-02-21 12:35:37 
        517497 0440                4400000020243 2011-02-21 12:27:24 
        628544 0450                4500000014347 2011-02-21 12:04:18 
        168985 0140                1400000009149 2011-02-21 13:27:49 
        236174 0790                7900000015641 2011-02-21 12:06:54 
        310129 0830                8300000024245 2011-02-18 14:01:10 
         91690 0310                3100000018402 2011-02-18 11:19:11 
        681489 0260                2600000004122 2011-02-21 14:59:45 
        564596 0590                5900000011427 2011-02-18 13:46:53
</pre> 


Il y a eu un problème dans la récupération du fichier journalier, voici un exemple de ce que j’ai trouvé dans les logs :

<pre>
19:5:1 --> Login with Username = 83@eurisk.fr
19:5:1 --> executing query with filter=(&(RecipientType=MOD)(State>=100)(Identifier=NEW*)). Retrieving attributes: CompletionDateTime,ToUser1,ToUser2,ToUser3,State,Color,StampType,AskReceipt,NPages,NSheets,EnvelopUsed
19:5:37 --> getting attributes of item with MSN=750683929
19:5:37 --> getting attributes of item with MSN=750683929
19:5:37 --> Updating item with MSN=750683929 -- attribute=Identifier -- value=DONE
19:7:17 --> Le délai d''attente de l''opération est dépassé.
</pre>

','2011-02-24',7,5,4,6,null,4,4,'2011-02-24 14:09:47','2011-02-24 16:47:04','2011-02-24',100,1,31,31,2,3,0,'2011-02-24 16:43:57');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (33,2,5,'Support Darva','h2. écran

Créer un écran ou l''on saisi le n° de dossier.
Ecran disponible en lecture, modification pour le profil administrateur et support.

Charge les informations relatives au dossier et à Darva :
* consultation de la date de création du dossier ainsi que le nom de l''affaire.
* consultation/modification de l''indicateur Dossier Darva
* consultation/modification du numero de sinistre (il y en a 2)
* consultation/modification du numéro OM DARVA
* consultation du code GTA ou code ACAM + code/nom du client qui missionne
* consultation de l''utilisateur Darva (avec son code agence/bureau)
* Recréer le bouton Darva

Permettre la sauvegarde des éléments modifiables du dossier.

h2. Bouton Darva

il y a 2 type de liens Darva, selon les clients 
* SMABTP = construweb
* le reste = irdweb 

h3. Construweb

voir http://10.31.2.165/dokuwiki/doku.php?id=new_0158#exemple_de_lien_a_construire_pour_construweb

exemple : https://pro.darva.com/controller/loginTransparent?userid=33626.saisie&password=6y86qsdcq&Service=S012&Sinistre=001SDO10021990&CodeAcam=05450405

h3. Irdweb

voir http://10.31.2.165/dokuwiki/doku.php?id=new_0040

exemple url :

https://pro.darva.com/controller/loginTransparent?userid=%%LOGIN%%%&password=%%MOTDEPASSE%%%&Sinistre=%%NUMEROSINISTRE%%%&Mission=%%OMDARVA%%%&Gta=%%CODEGTA%%%&Service=S022',null,12,5,8,4,40,4,23,'2011-02-24 18:35:12','2013-04-17 14:59:22',null,30,null,null,33,1,2,0,'2013-04-17 14:59:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (34,3,2,'rattrapage des noms de documents','Des fichiers malnommés sont présents dans l''arborescence CP2000.
Il faut corriger les noms de fichier dans CPXDOC et dans le dossier CP2000 de l''agence',null,5,1,4,4,null,4,5,'2011-02-25 16:11:24','2011-07-12 15:39:47',null,0,24,null,34,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (35,1,2,'Affichage Post-it manquant','Problème sur changement d''état mission terminé vers ouverte.

c''est dès la réouverture d''un dossier que je n''ai pas accès au POST-IT (meme après "Enregistre sans quitter"). Il faut Enregistrer et quitter puis revenir dans le dossier pour que le bouton s''affiche.','2011-04-15',16,5,5,4,7,4,5,'2011-03-07 12:24:11','2011-04-16 19:30:25','2011-04-04',100,2,null,35,1,2,0,'2011-04-06 00:04:38');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (36,3,4,'Tests Selenium','Tests à rejouer sur la nouvelle version d''Hermès qui sera livré en préprod par Cité-SI le 11 mars 2011.','2011-06-24',null,5,3,4,10,4,9,'2011-03-08 14:48:50','2011-06-21 15:29:56','2011-03-14',100,null,null,36,1,2,0,'2011-06-21 15:29:56');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (37,3,4,'Test mail automatique','Ecrire un programme qui soit capable de tester automatiquement les différents cas d''envoi de mail présent dans le fichier excel en pièce jointe.

Prendre la base d''un fichier xml et modifier les emails des gestionnaires selon les cas listés dans les tableaux du fichier excel. Simuler un import (Cela doit ressembler à /home/hermes/util/import.sh ). et générer un rapport avec le resultat de tous les cas. Les envois doivent correspondre à ce qui est attendu à droite du tableau dans le fichier excel.

Note : Voir pour utiliser des faux emails au lieu des vrais adresses de nos clients.
prendre des exemples du genre emailperso@groupeprunay-si.fr, emailsvc@groupeprunay-si.fr, emailcopie@groupeprunay-si.fr etc...',null,3,5,3,4,10,4,18,'2011-03-08 18:22:48','2011-07-25 14:57:45','2011-03-31',80,null,null,37,1,2,0,'2011-07-25 14:57:45');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (38,2,2,'Remplacer Pdfp par gsPrint','Pdfp n''est pas compatible avec Adobe Acrobat Reader 8 et +.
Voir pour utiliser GsPrint : http://pages.cs.wisc.edu/~ghost/gsview/get49.htm',null,17,1,null,4,null,4,3,'2011-03-09 14:23:46','2011-04-19 14:35:55',null,0,null,null,38,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (39,3,2,'Flydoc - mettre en place le nouvel EskerLoader','Voir pour mettre en place le nouveau Esker Loader.
Tout est sur le 10.31.2.39
Voir dans le dossier C:\Progi\Flydoc\0830\Webservices_client
il faut :
* comparer par rapport à une agence en production, pour isoler les éléments à livrer
* adapter le script nettoyage.cmd

','2011-04-15',7,5,4,4,null,4,5,'2011-03-14 14:44:30','2011-06-01 11:54:54','2011-04-11',100,8,null,39,1,2,0,'2011-04-18 16:39:28');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (40,2,2,'Mise en forme alerte controle des dates','Pour Flydoc, Dans le Ctrl_DateRemisePoste.bat, on génère la phrase suivante :
600000014509-0060/619789-Courrier du 2011-02-09 non remis en poste 

voir pour formater l''identifiant comme ceci : 
000001/0060/600000014509
et si possible la date au format français...

',null,7,1,null,4,null,4,2,'2011-03-15 14:31:16','2011-07-12 15:39:15',null,0,8,null,40,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (41,1,2,'Perte d''infos dans la mise à jour des documents','Sur le dossier 0780/643908, dans le document DT226 du 14/03/2011, la ville ne se mets pas a jour dans le document word alors que c''est saisi dans CP2000.

La ville est "PARIS LA DEFENSE CEDEX". A la mise à jour du document, le champs de la ville disparait ... pourquoi?

Ci-joint le rendu pdf du document ensuite transmis à Esker','2011-04-15',5,5,5,4,null,4,3,'2011-03-15 14:39:12','2011-05-10 17:45:50','2011-04-11',60,8,null,41,1,2,0,'2011-05-10 17:45:50');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (42,1,2,'CPXL005 - recherche intervenant sur repertoire --> -1123','corriger l''appel qui tombe en erreur -1123 (mauvais nombre de paramètres)

!erreurcpxl005.png!','2011-04-15',8,5,5,4,null,4,4,'2011-03-29 10:04:02','2011-03-30 10:39:59','2011-04-04',100,1,null,42,1,2,0,'2011-03-30 10:09:50');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (43,2,5,'Créer un document convocation','Objectif : refaire dans Symfony, l''équivalent de la création d''une convocation. S''inspirer de CP2000 (CPXT005/CPXG014).

h2. Ecran 1 (futur onglet document)

Choisir un document type parmi les DT disponibles (on ne prendra pas en compte ici l''histoire des procédures, sous procédures, on commence simple).
Action à coder : rédaction et validation définitive.

h2. Ecran 2 (équivalent CPXG014)

Créer le formulaire pour saisir tout ce qui peut l''être dans une convocation, séparer en 3 bloc distincts:
* Info héritée du dossier (donc pas de saisie) : Références, Lieu, Expert en charge.
* saisie commune à tous les destinataires :  Date, Heure,  Liste des dommages, Liste des destinataires
* saisie particulière à chaque destinataire : Adresse, Mode d''envoi (attention nouvelle notion!), clausier

h2. Génération du PDF 

Sur validation du document, générer le PDF (ne pas s''occuper de l''entete et du pied de page qui feront l''objet d''un sujet à part) et le stocker dans une arborescence du serveur (utiliser l''existante... ou recréer équivalent, a voir selon le plus adéquate)

',null,18,5,null,4,null,4,5,'2011-03-29 10:26:32','2013-04-17 14:59:42',null,0,null,145,145,4,5,0,'2013-04-17 14:59:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (44,2,5,'Analyse des documents type existant','Créer un fichier excel qui indiquera les propriétés de chaque document.

Pour chaque document voir s''il contient :
Entête (oui/non)
Bloc de référence sinistre (oui/non) (à détailler???)
Pavé adresse (oui/non)
Corps du document
Pied de page
...
à compléter selon cas des documents à rencontrer.

',null,18,5,4,4,null,4,8,'2011-03-29 17:43:57','2013-04-17 15:00:06',null,0,null,145,145,6,7,0,'2013-04-17 15:00:06');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (45,3,2,'Recherche dossiers dans un même numéro de G','Pour les polices de type RC - RCD - MGAE
Extraire tous les dossiers dans un même numéro de G','2011-04-01',19,5,4,4,null,4,4,'2011-03-30 09:35:24','2011-04-01 14:10:18','2011-03-30',100,null,null,45,1,2,0,'2011-04-01 14:10:18');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (46,2,1,'Migrer les archives Yvette vers serveur Pygmalion','Il nous faut déplacer les archives présentes sur Yvette :
* Archives CDDOC ( /media/sdb1/datas/ (32go) + NFS sur 10.31.2.99 (11Go) SOIT 43Go )
* Archives CPExpert ( /media/sdb1/export (39Go) )
* Archives AR_Epost ( /media/sdb1/AR_epost (283Mo) à mettre à jour !!! )

Attention, il y a un mini script de recherche en PHP, voir pour faire l''équivalent sur Pygmalion
Et ne pas oublier la page d''accueil et le doc d''aide (peut-être à mettre à jour?) 

Rappel de l''objectif des archives : on doit pouvoir naviguer dans les archives, consulter le contenu d''un fichier xml dans une présentation correcte pour le visiteur, accéder aux fichiers joints aux dossiers à télécharger.

Voir Pascal pour le transfert des fichiers.
','2011-04-29',null,1,3,4,null,4,0,'2011-03-30 11:56:14','2011-03-30 11:56:14','2011-04-14',0,null,null,46,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (47,3,2,'Mise à jour Acquire - créer un batch update pour le support','Le support va installer la nouvelle version de l''application acquire.

Il faut créer un batch de commande pour mettre à jour la clé de licence dans la base de donnée qui nous intéresse pour chaque install d''acquire par le support.

mettre la base de donnée en parametre : IP + port

valeur actuelle de APKEY dans CPXPGA : /app=EuriskScan /key=1676925469
valeur après l''install à mettre à jour : /app="EURISK FRANCR" /key=-1009525153','2011-04-01',10,5,4,4,null,4,9,'2011-03-30 15:11:48','2011-06-29 11:51:55','2011-03-30',100,4,null,47,1,2,0,'2011-06-15 13:53:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (48,3,2,'Stats utilisation Faxbox','Extraire les données de Faxbox, voir pour faire des stats.','2011-04-08',11,5,4,4,null,4,2,'2011-03-30 15:34:24','2011-07-21 17:43:48','2011-04-04',100,4,null,48,1,2,0,'2011-04-06 17:06:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (49,2,2,'Paramétrage licence Scan','valeur actuelle de APKEY dans CPXPGA : /app=EuriskScan /key=1676925469

La nouvelle valeur : /app="EURISK FRANCR" /key=-1009525153 ne rentre pas dans le champs de 32 caractère de la base de donnée.
Donc il faut séparer les valeurs dans CPXPGA :
* Supprimer le paramétrage APKEY
* Créer un paramétrage SCAPP, stocker les valeurs sans les "
* Créer un paramétrage SCKEY
* Adapter l''appel dans le CPXL250 : [[source:/trunk/frm/CPXL250.xml@86#L337]]
','2011-04-08',10,5,5,5,7,4,5,'2011-04-01 11:20:21','2011-04-16 19:31:29','2011-04-01',100,8,null,49,1,2,0,'2011-04-06 00:03:15');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (50,2,2,'Création d''un dossier à partir d''un OM Hermès','A partir de la liste des OM présentes dans l''écran de consultation des OM, donner la possibilité à l''utilisateur de créer un dossier CP2000

Remplir les champs suivants à partir de l''enregistrement sélectionné dans la table CPXOMS
!creation_om.png!

h1. Sur validation de la création du dossier (DOSNUM créée) :

h2. Sauver l''occurrence de l''OM comme "a été utilisée" 

Par l''intermédiaire d''une opération dans le nouveau service qui concerne CPXOMS.

h2. Importer les pièces jointes dans le dossier,

h3. l''OM, fichier XML

Garder dociprov = ''O'' et affecter le dossier (DOSNUM).
Rangement et Affichage à définir –> comme email ou devis Cape ou réclamation cad ecran qui affiche les infos de l''OM. Ce fichier n''est pas à montrer à l''utilisateur dans les onglets documents de CP2000

h3. Autre pièce jointe 

dociprov = ''E'' affecter le dossier (DOSNUM).
',null,4,2,5,5,null,4,14,'2011-04-04 16:52:53','2012-10-23 12:00:45',null,60,80,9,9,2,3,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (51,2,2,'Import OM Darva','Prendre en exemple le message xml en pièce jointe, il faut sauvegarder cet OM dans CPXOMS.

Dans le fichier excel, voir comment enregistrer les données dans CPXOMS. Attention à la recherche de l''expert et du code agence (qui doit découler du code postal du sinistre)

Déplacer les champs dates existant de CPXOMS pour la nouvelle table CPXOMSDT qui regroupera toutes les dates d''un OM.

Attention, donc sur les lignes orange dans le fichier excel...
* Création de table pour stocker les dates CPXOMSDT
* OMSVILLE (ville du sinistre)

!ordre_de_mission.png!

Autre nouveauté (Pour Darva) : on va stocker des "segments" du flux XML qui seront réutilisé pour plus tard comme les AR missions... Donc nouvelle table CPXOMSSEG + stockage des segments XML en fichier physique sur le serveur + paramétrage de liste des segments à stocker dans CPXLIS/CPXVAL','2011-04-15',4,5,null,6,null,4,10,'2011-04-04 21:56:33','2011-11-18 11:29:35','2011-04-04',0,40,null,51,1,2,0,'2011-11-18 11:29:28');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (52,2,2,'Import OM Gsicass','Importer un OM du syteme Gsicass

En orange, nouveau champs date (date mission) à stocker dans CPXOMSDT

!http://10.31.2.42/attachments/download/24!','2011-04-15',4,5,null,6,null,4,8,'2011-04-04 22:31:12','2011-11-18 11:30:05','2011-04-11',0,40,null,52,1,2,0,'2011-11-18 11:30:05');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (53,2,4,'Cryptage des login/password dans les accès direct','De : Eurisk Siege/BOYER Paul 
Envoyé : jeudi 31 mars 2011 10:11
À : CPCOM/BOUREZ Vincent
Objet : TR: Hermes - Lien transparent 

Bonjour Vincent,

Dans le cadre du projet CAREO (EUREXO) le lien transparent utilisé a été crypté pour le sécuriser à la demande du client : 
on ne voit plus en clair l’ID + Pwd permettant d’accéder à un dossier particulier sur Hermès. 
Cette technologie devrait être mise en place également pour la construction car pour le moment le contrôle d’accès est très superficiel voire inexistant !!!

Ceci est applicable aux liens 
* Depuis GSICASS, 
* Dans le mail automatique
* Depuis CP2000
* Depuis Sésame chez nous

Cela permettrait 
# de créer une nouvelle fonction CP2000 : envoi par mail d’une URL « dossier » pour un accès à un dossier unique.
# d’ouvrir un dossier Hermès sur demande particulière : une entreprise, un avocat… sans risque de débordement dans d’autres dossiers

Ceci impose la mise en place d’un module de cryptage de l’URL dans l’application de départ et le décryptage sur Hermès.
Je propose qu’on demande à Nathalie de nous faire une proposition technique pour voir ce qui serait faisable et dans quel délais
Vu que ça fonctionne déjà coté dommage, ça ne devrait pas poser de problème particulier.
',null,2,1,null,4,23,4,7,'2011-04-06 10:19:03','2011-07-05 16:47:12','2011-10-31',0,null,null,53,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (54,2,4,'Partenariat Police-Sinistre QBE-Marsh','h1. Contexte général

L’objectif est de permettre à notre partenaire QBE de gérer ses polices d’assurance dans l’extranet Hermès et de lier, le cas échéant, les polices aux sinistres les concernant.

h2. Organisation

!schema_QBE.png!

h1. Projet

h2. Besoins Ecrans Hermès

L’accès dans Hermès est en consultation uniquement.

Aucune saisie de la part des gestionnaires assureurs (ou courtiers) sauf sur l’enrichissement de pièce jointe. Prévoir une possibilité d’accéder directement sur l’onglet des pièces jointes en appelant une url spécifique.

Les utilisateurs doivent pouvoir exporter les résultats.

*3 écrans principaux sont attendus :*
* Ecran de filtrage (recherche sur divers champs… à définir)
* Ecran liste des résultats (correspondant à la recherche)
** Accès au détail d’un enregistrement
** Export des enregistrements (vers excel)
* Détail d’un enregistrement : Afficher les données de l’opération, les polices de l’opération, les intervenants, les pièces jointes et suivi des OM/Dossier sinistres (Système d’onglets ?)

h2. Les données échangées s’articulent autour de 4 grands thèmes :

* Le chantier (équivalent opération chez Eurisk)
* Les polices appartenant à un chantier (maximum 2)
* Les intervenants (point restant à définir)
* Les documents à joindre (Norme à définir aussi)

Voici la structure du fichier xml qui sera mis en place. Il faut attendre la validation définitive par QBE de l’ensemble des données avant de définir complètement ce flux xml :

<pre><code class="xml">
<?xml version="1.0" encoding="ISO-8859-1" ?> 
<simplicite>
 <object>
  <name>Chantier</name> <!-- nom de notre objet -->
  <action>upsert</action> <!-- Mode upsert = insertion si non existant ou update -->
  <data>
   <chnum>123456789</chnum> 
   <chdate>2010-04-08</chdate> 
<!-- ici toute la liste des champs concernant les chantiers selon définition ds le fichier excel QBE -->
  </data>
 </object>
 <object>
  <name>Police</name> <!-- Au minimum 1 police, au maximum 2 polices -->
  <action>upsert</action> 
  <data>
   <polchamps1>2</polchamps1> 
   <polchamps2>123456789</polchamps2> 
   <polchamps3>2010-04-08</polchamps3> 
<!-- ici toute la liste des champs concernant les polices selon définition ds le fichier excel QBE -->
  </data>
 </object>
</simplicite>

</code></pre>

h2. Liaison Police – OM – Dossier

Lors d’une déclaration de sinistre, à la création d’un ordre de mission, il doit être possible d’appeler les infos de la police et remplir les infos de la création d’un OM à partir de ce qui est déjà connu (à définir). Dans ce cas, la liste des intervenants + les pièces jointes doit être transmise à l’agence Eurisk (impact sur l’export d’OM).

h1. Planning

QBE demande une utilisation de cette évolution dans Hermès pour la 1ere semaine de Juillet 2011. Des maquettes d’écrans doivent être disponible pour le 12 mai 2011. Voici le planning concernant ce projet :

!planning_QBE.png!
','2011-07-15',20,5,4,4,11,4,12,'2011-04-06 10:50:26','2011-10-26 15:37:07','2011-04-18',100,null,null,54,1,2,0,'2011-10-26 15:36:59');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (55,2,2,'Fiche Crac Sycodes dès le 1er euro','Dans le dossier/dommage, il faut créer une fiche Crac Sycodes dès le 1er euro uniquement pour le client SMABTP (c''est en lien avec le systeme Darva).

Attention, pour les autres clients et l''export vers l''AQC, il faut conserver les règles actuelles.

Il faut donc créer un flag pour repérer les fiches à destination de Darva qui ne doivent pas être exportées vers l''AQC.

h2. Base de donnée (A tester)

(OK) Nouveau champs dans CPXSCY :

| Nom | Type | Valeur |
|SCYFLGDRV|Char(1)|''T'' ou ''F''|

h2. Règle de gestions

h3. Gestion des dommages (CPXG015)

-Dans le CPXS262, operation TypeFiche-

Tester le type fiche en sortie de l''appel de CPXS262 pour émettre une exception pour le client SMABTP et ainsi avoir accès à la gestion CracSycodes.

Si non éligible à la fiche Crac Sycodes actuelle et Client = SMABTP alors
SCYFLGDRV = ''T''

h3. Gestion Crac Sycodes (A tester)

(OK) Afficher le champs SCYFLGDRV en lecture seule dans l''écran CPXG262. Libellé "Stats Darva"

h3. Export AQC (A tester)

Pour +ne pas+ charger les fiches SCYFLGDRV = ''T'' : Adapter l''opération CountCpxScy (OK)',null,21,4,5,4,null,4,28,'2011-04-12 16:07:34','2012-10-23 12:00:30',null,60,64,null,55,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (56,2,5,'Accueil - Tableau de bord','Voir spécification détaillée sur le serveur de développement : 

C:\\Progi\\Projets\\CPWeb\\Détaillé\\Agence\\Accueil\\Accueil-001-Liste des dossiers.doc


En résumé :

Il faut créer un tableau de bord ou l''on va lister les dossiers selon certains critères.
Le tableau de bord de la page d’accueil est divisé en 3 parties :
* Filtrage personnalisé : champs de formulaire pour préciser des critères de filtrage communs
* Requêtes prédéfinie : liste lien de recherches spécifiques
* Tableau d''affichage des données
','2012-01-16',22,5,8,3,40,4,15,'2011-04-14 16:24:39','2012-09-10 09:31:16','2012-01-10',100,null,null,56,1,2,0,'2012-09-10 09:31:16');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (57,3,2,'alerter sur les convocations non envoyées','Il faut pouvoir alerter l''agence sur une convocation validée mais qui n''a pas été envoyée.','2011-04-22',5,5,4,5,null,4,3,'2011-04-15 13:44:29','2011-04-18 15:18:30','2011-04-15',100,null,null,57,1,2,0,'2011-04-18 15:18:30');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (58,3,2,'Mise à plat de la base de données de Eurisk Louveciennes.','',null,23,5,7,4,null,7,3,'2011-04-19 16:02:01','2011-04-21 09:39:58','2011-04-19',100,null,null,58,1,2,0,'2011-04-21 09:39:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (59,2,5,'Document Architecture','Concevoir le document architecture global à l''application CPWEB.
Le localiser dans les docs du projet CPWEB, je propose ici :
10.31.2.37\\c$\\Progi\\Projets\\CPWeb\\Détaillé\\archi-globale.doc

Liste rapide des choses connues :
* Entre 1 et 4 bases solid sur serveur agence utilisé par CP2000
* C''est l''utilisateur qui sait quelle base il doit utiliser
** Soit la base est sélectionnable dans l''appli
** Soit il faudra définir 1 url par appli
* il y a un LDAP général mais aussi une réplication avec 24h de décalage dans une AD locale (faudra se faire expliquer par Cédric comment on devra fonctionner)
* les tests sont à faire sur un serveur Windows serveur 2003 avec IIS...
* on peut développer en local sur son PC ou sur le serveur 10.31.2.37
* on devrait pouvoir tester sur le 10.31.2.39
** les bases solid sont installées sur le 10.31.2.39
*** port 1320 = agence
*** port 1321 = siège production
*** port 1322 = référence ( = paramétrage du siège)
*** port 1323 = agence pour test avant livraison paramétrage production
*** port 1325 = 2e agence
*** port 1326 = 3e agence
*** (je peux en installer indéfiniment en test si besoin)
* La recette est effectuée sur 10.31.2.40 (qui devra être installé)
** base solid installé en local aussi
*** port 1320 = agence
*** port 1321 = siège production
*** port 1322 = référence ( = paramétrage du siège)
*** port 1323 = agence pour test avant livraison paramétrage production




',null,null,5,3,4,null,4,3,'2011-04-20 16:53:43','2013-04-17 14:58:22','2011-04-20',0,null,null,59,1,2,0,'2013-04-17 14:58:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (60,2,5,'Document norme de développement CPWEB','Localiser avec le doc d''architecture.

Définir (non exhaustif) :
* Ou coder le métier
(pas de métier dans le twig ** pleur **)
** par rapport à objet base de donnée (CPXDOS, CPXDOS, CPXCLI ...)
** par rapport au objet métier (un objet Darva, un objet "document envoyé"...)
* normaliser les routes
si j''ai un écran pour le support, qui doit créer un enregistrement sur un objet document cela doit ressembler à /support/document/creer si je supprime c''est /support/document/supprimer
si je travail sur darva, c''est support/darva/... etc...

* Si je traite des formulaires, voir pour passer par les variables $POST et bien définir mes routes (on doit avoir des routes qui accepte uniquement des requetes "POST" dans la console symfony route:debug)

* Voir comment écrire un commentaire intéressant.
** est-ce qu''on utilise PHPDoc ?

Voir ce qui est bon à prendre dans les slides sensio comme 
http://www.symfony-live.com/pdf/sflive09fr/Les-bonnes-pratiques-du-developpement-symfony-en-30-point-cles.pdf

Pour éviter ce que l''on peut voir dans le SupportDarvaController.php de la [[r9]]
alors que j''ai lu que 
"Le code PHP d’une méthode du contrôleur, c’est 10 lignes maximum!"

etc ...

',null,null,1,10,4,null,4,10,'2011-04-20 17:30:00','2012-09-10 08:40:02','2012-07-04',100,null,null,60,1,4,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (61,1,2,'Correctif création police/opération pour le support','h2. probleme recherche ville sur dossier RCD

Il faut prendre en compte du renseignement du CP/Ville pour les opération RCD

h2. Création opération sur le support

si le type de la police = RCD ou MGAE, on doit pouvoir créer une opération
sinon pour les autres type de police, il faut une police par aliment

Type de la police = TPOCOD (cpxpas)
','2011-04-22',24,5,5,4,null,4,4,'2011-04-22 09:59:44','2011-04-22 10:53:40','2011-04-22',100,null,null,61,1,2,0,'2011-04-22 10:53:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (62,2,2,'Agrandissement du champ DTYREF','Demande d''agrandissement du champ DTYREF du fait du passage du code document type sur 4 caractères','2011-05-02',5,5,5,4,13,5,3,'2011-05-03 10:44:46','2011-05-04 18:22:48','2011-05-02',100,1,null,62,1,2,0,'2011-05-04 18:22:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (63,1,2,'Problèmes de liste d''enjeux sur Devis CAPE','Impossibilité de créer un devis car la liste des enjeux est vide.
Cas isolé sur le site de Colombe.','2011-05-02',24,5,5,4,null,5,3,'2011-05-03 10:46:31','2011-05-04 17:22:09','2011-05-02',100,1,null,63,1,2,0,'2011-05-03 10:57:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (64,1,2,'Retour à la ligne dans les commentaires des Post-it','Modifié le fonctionnement du champ de saisie du POST IT pour un retour à la ligne forcé une fois arrivé au bord du cadre.

Ecran CPXG057. Prendre exemple sur le champ de saisie des mails dans CPXC078','2011-05-03',25,5,5,4,13,5,2,'2011-05-03 10:57:02','2011-05-04 18:18:53','2011-05-03',100,1,null,64,1,2,0,'2011-05-04 18:18:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (65,2,6,'DeX: Document d''installation de Dataxchanger','A faire !

* voir l''aide de DataExchanger
** procédure "mise en route" / installation
* voir doc avec screenshot joint
* voir ce qui est présent dans le C:\\progi\\install
* sauvegarde = dump mysql
* sauvegarder aussi les fichiers ddc de DataExchanger/serveurweb/config
* Licence temporaire de 30 jours propre à la VM

Le serveur DATA EXCHANGER est accessible à l’adresse 10.31.30.12

Identifiant CDB Technologie :
Login : CDBT
Mot de passe : cvf;67tr
  ','2012-03-23',31,5,11,4,98,4,18,'2011-05-04 18:36:44','2013-02-13 11:42:34','2012-03-08',100,16,null,65,1,2,0,'2012-03-22 18:08:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (66,2,2,'Insertion données Région','Rajouter dans la table CPXRGN le délégué Régional (RGNDELREG).
Rajouter dans la table CPXDEP le code région (DEPRGN) ainsi que le nom du département (DEPNOM)','2011-05-04',15,5,5,4,null,5,2,'2011-05-05 08:40:05','2011-05-17 16:23:42','2011-05-04',100,8,null,66,1,2,0,'2011-05-17 16:23:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (67,3,2,'Paramétrage Darva Rennes IRDWEB','Il y a un nouveau client, la MACSF, qui utilise *Darva IRDWEB*
CLICOD = MASC092-01 --> Client majeur = "MACSF"

L''agence de Rennes (0350) travaille avec ce client et a donc hérité d''un nouveau code d''accès Darva pour IRDWeb : 34991.acces/34991.acces
(cf z:\\Societes\\Eurisk\\Logiciels\\Darva\\Contrats\\CodesAcces.xls)

A faire :
* Script SQL pour insérer dans CPXLGN les accès Darva pour l''agence de Rennes
** attention au compteur dans CPXCPT (actuellement en prod = 160)','2011-05-05',15,5,5,4,null,4,5,'2011-05-05 09:22:11','2011-05-05 17:21:04','2011-05-05',100,1,null,67,1,2,0,'2011-05-05 17:14:15');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (68,1,2,'Problème sur création Opération seule','Voir le problème sur l''écran CPXG076',null,24,5,5,4,null,5,2,'2011-05-05 10:16:03','2011-05-10 17:38:09','2011-05-05',100,8,null,68,1,2,0,'2011-05-05 15:20:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (69,1,2,'double numérotation entre les avoirs et les factures','Le numero de note d''honoraire doit être unique pour une année donnée. Or ce n''est pas le cas.
Aujourd''hui, dans la base de donnée, il y a 65 doublons

Cela semble systématique avec la création d''avoir depuis fin mars

Contact éventuel pour les avoirs : Dominique Gouveia - 01.30.78.18.17


Analyser le problème dans CPXNHO (nhonum), trouver l''origine du bug, voir pour proposer quelques chose pour corriger.

Attention... sujet hautement sensible.',null,26,5,5,5,14,4,12,'2011-05-05 11:38:22','2011-05-19 18:34:07','2011-05-05',100,null,92,92,4,5,0,'2011-05-17 15:03:34');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (70,1,2,'plantage sur envoi document - afficher les mails','* Se placer sur l’onglet document
* Cliquer sur Option > Envoi de documents
* Cocher le mode d’envoi Email
* Cliquer sur Afficher les mails
* Cocher un mail et recliquer sur Afficher les mails
* Cocher un ou des documents et cliquer sur sélectionner
* CP2000 plante
',null,27,5,5,4,20,4,4,'2011-05-05 14:25:31','2011-06-07 15:39:59','2011-05-05',100,null,null,70,1,2,0,'2011-05-31 14:28:09');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (71,2,6,'Contrôler l''import des mails dans CP2000','Pour chaque agence :
* Dans le dossier C:\\Agce\\Cp2000\\log du serveur de l''agence
* analyser les fichiers contenant "CPXS050_ImportMail"
* voir pour proposer
** des corrections automatiques des xml qui plantent (ex : code agence)
* faire un rapport des anomalies à destination de l''agence
** sur ce qui est détecté et corrigé automatiquement
** sur ce qui est détecté et non corrigé
',null,28,1,4,4,52,4,3,'2011-05-05 16:20:34','2013-04-26 10:33:26','2011-05-05',0,null,null,71,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (72,1,2,'Code postal et ville de l''opération','*Le code postal et la ville d''une opération doivent dorénavant toujours être obligatoire.* 

* Dans le cas ou l''opération est de type construction (tpoiconst != ''F'') c''est à l''utilisateur de faire la saisie dans l''écran "opération" accessible depuis le dossier
* Dans les autres cas, le code postal et la ville sont à renseigner automatiquement par la création de dossier.

_Appliquer les mêmes règles pour l''écran de création des opérations du support informatique._

','2011-05-13',29,5,5,5,14,4,12,'2011-05-06 10:57:37','2011-05-19 18:33:44','2011-05-09',100,null,92,92,2,3,0,'2011-05-19 14:18:37');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (73,2,2,'Conversion de fichier','Objectif : Convertir des fichiers quelconque en PDF.

Traiter un fichier .TIF
Traiter un fichier .DWG (autocad)

Voir :
* http://www.caderix.com/journal/spip.php?article167
* mail support : vendredi 06/05/2011 09:42',null,null,1,null,4,null,4,1,'2011-05-06 16:55:40','2011-07-25 10:20:06','2011-05-06',0,null,null,73,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (74,3,4,'Lien OM Hermès - Dossier CP2000','Surveiller qu''un OM créé sur Hermès soit bien traité par CP2000 + retour de l''import dans Hermès correctement relié à l''OM existant.

A faire :
* Karin : Créer des OM sur l''agence 0830 (au moins les 3 types de gestion)
* Nicolas : Déclencher et surveiller l''import dans CP2000
* Dominique : Ouvrir les dossiers correspondants
* Vincent : Paramétrer l''import CP2000-Hermès préprod sur le serveur de test
* David : Déclencher et surveiller l''import dans Hermès','2011-05-27',1,2,4,4,null,4,3,'2011-05-08 17:15:56','2011-06-15 10:44:36','2011-05-17',10,40,null,74,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (75,2,4,'retrouver un dossier par le nom du « Bénéficiaire »','prérequis :
* champs du dossier à remonter

A faire : 
* champs de recherche à ajouter…',null,30,1,null,4,12,4,6,'2011-05-09 13:36:12','2011-08-19 11:08:11','2011-09-05',0,null,null,75,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (76,2,4,'Pouvoir filtrer les dossiers réalisés et terminés en enquête téléphonique','plusieurs pistes :
* soit cela se trouve dans l''OM Hermès
* soit il faut enrichir la remontée Hermès pour récupérer les champs PFT de CP2000.

Voir Karin.',null,30,1,null,4,12,4,3,'2011-05-09 13:38:03','2011-07-05 16:37:49','2011-09-05',0,null,null,76,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (77,2,1,'Présentation / maquette du site','Livrer le site conforme au souhait de présentation de P. Boyer

','2012-12-03',null,3,null,4,16,4,12,'2011-05-09 13:52:51','2012-10-23 10:12:08','2011-06-21',80,null,null,77,1,12,0,'2011-07-12 16:00:04');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (78,2,1,'Définir les droits d’accès','Créer le doc qui définira les droits d''accès.
Le faire valider par Paul Boyer',null,null,1,3,4,16,4,1,'2011-05-09 13:55:16','2011-08-31 10:17:00','2011-06-06',0,null,null,78,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (79,2,1,'Transférer les docs et répertoires existants','Importer les documents du Pygmalion actuel (10.78.1.7)','2011-05-09',null,5,3,4,16,4,2,'2011-05-09 14:02:11','2011-05-09 14:33:26','2011-05-09',100,null,null,79,1,2,0,'2011-05-09 14:02:11');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (80,2,1,'Accès direct pour certains contributeurs','Permettre l''import d''un grand nombre de fichier.

--> Accès FTP

','2011-05-09',null,9,3,4,16,4,6,'2011-05-09 14:03:54','2011-08-16 08:07:06','2011-05-09',100,null,null,80,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (81,2,2,'Mettre à jour l''accès Pygmalion','Dans l''entete des dossiers (CPXG021), rendre paramétrable l''url Pygmalion

Nouvelle url du site: https://pygmalion.groupe-prunay.fr

Nouvelle url du dossier (pour exemple, elle n''est pas définitive)
ftp://10.31.2.222/Alfresco/Sites/eurisk/documentLibrary/Syst%C3%83%C2%A8me%20Qualit%C3%83%C2%A9/Le%20R%C3%83%C2%A9f%C3%83%C2%A9rentiel%20EURISK/

Préparer ce qu''il faut pour que CP2000 soit à jour lors de la livraison de Pygmalion : script SQL ou autre ','2011-06-30',16,4,5,3,16,4,7,'2011-05-09 14:26:16','2012-08-28 14:35:55','2011-06-01',0,null,null,81,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (82,2,1,'Rédiger le manuel utilisateur','Notice utilisateur à rédiger pour diffusion le jour J.','2011-07-29',null,5,3,4,16,4,3,'2011-05-09 14:29:30','2011-08-30 10:06:33','2011-05-09',100,null,null,82,1,2,0,'2011-05-23 11:02:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (83,3,2,'Intégrer les cout esker dans les bases de données des agences','Au siège il faut intégrer les fichiers pour les mois de
* janvier
* février
* mars
* avril

Prévenir Irina quand c''est fini.
','2011-05-20',7,5,4,4,null,4,4,'2011-05-11 16:42:49','2011-07-25 15:27:07','2011-05-11',100,8,null,83,1,2,0,'2011-05-19 12:24:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (84,3,2,'lien entre CP2000 LES ULYS et DARVA/LOUVECIENNES ','Mettre à jour le paramétrage CP2000 CPXLGN :
Récupérer l''accès Darva du 78 et le donner au 91.

Script SQL à faire !','2011-05-17',15,5,4,6,null,4,3,'2011-05-17 15:26:21','2011-05-17 16:22:47','2011-05-17',100,1,null,84,1,2,0,'2011-05-17 15:40:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (85,1,2,'Plantage sur envoi mail','Jessica Decologne (Besançon) a envoyé un mail par CP2000. Le traitement ne se termine pas comme attendu : l''application se ferme subitement.

Le mail est quand même envoyé (car des accusés de reception ont été reçu) mais il n''est pas archivé dans le dossier CP2000.

Dossier sur lequel elle travaillait : 0250/691984
Remarque : dans le mail il y avait de nombreux destinataires en copie.

h2. A faire

* Reproduire le crash (s''aider des fichiers de logs joints pour comprendre)
* Corriger l''envoi mail en conséquence.

',null,6,5,null,4,18,4,2,'2011-05-18 11:40:21','2011-05-27 10:47:54','2011-05-18',0,24,null,85,1,2,0,'2011-05-27 10:47:13');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (86,3,2,'Support - Aide hotline','Consigner ici toute aide apportée au Support sur les sujets CP2000.

1 aide = 1 commentaire

* Ne pas hésiter à mentionner le ticket Track-it
* Si cela donne lieu à une autre demande (exemple correction d''anomalie), noter la référence

Voir aide aussi ici : [[support]]',null,24,4,null,4,null,4,86,'2011-05-18 15:40:35','2013-04-30 16:55:46',null,0,null,null,86,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (87,2,2,'Intégrer les couts des envois Maileva aux dossiers DOMTOM','Demande d''Irina :

Avoir la même chose qu''Esker aujourd''hui. Il faut ventiler sur chaque dossier les couts des envois maileva contenu dans le fichier récapitulatif mensuel reçu par mail de la part de maileva.

',null,7,1,null,4,null,4,0,'2011-05-19 10:35:39','2011-05-19 10:35:39','2011-05-19',0,40,null,87,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (88,3,2,'Préparation site formation','Mettre à jour le site de formation pour Dominique
Formation à Bordeaux à partir de Lundi 23 mai.','2011-05-20',15,5,4,4,null,4,2,'2011-05-19 10:38:17','2011-05-21 23:21:58','2011-05-19',100,4,null,88,1,2,0,'2011-05-21 23:21:58');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (89,1,2,'Rattrapage cout NPAI','Faire un script SQL pour que dans CPXCHRONO, tous les champs CRNMTNPAI différent de null aient une valeur de 0.24

A passer sur toutes les agences.',null,7,1,4,6,null,4,1,'2011-05-19 12:05:14','2011-07-12 15:41:42',null,0,2,null,89,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (90,1,2,'Corriger l''intégration du flux mensuel Flydoc','L''intégration dans CPXCHRONO du montant de traitement des courriers NPAI est faux.
On attend la valeur 0,24 mais on reçoit le nombre de feuille à la place.

voir siège production : CPXG013 intégration Flydoc.

A corriger !','2011-08-17',7,5,5,4,2,4,8,'2011-05-19 12:08:00','2011-08-31 14:01:55','2011-06-08',100,null,null,90,1,2,0,'2011-08-31 14:01:55');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (91,1,2,'Renseignement automatique du statut de l''intervenant non voulu','De : Eurisk Siege/SANTONI Dominique 
Envoyé : jeudi 19 mai 2011 10:58
Objet : TEST 309984

REPERTOIRE : à la création d''un assuré/intervenant le statut Tiers se complète automatiquement!!sans aucune intervention de ma part

quand je créé un premier intervannts : insertion auto du statut Assuré
quand j''en crée un second : insertion auto du statut Tiers

A faire :
* Ne pas renseigner automatiquement le champs du statut de l''intervenant, C''est à l''utilisateur de le saisir.','2011-05-31',8,5,5,4,20,4,4,'2011-05-19 12:23:37','2011-06-15 12:16:47','2011-05-19',100,16,null,91,1,2,0,'2011-06-15 12:16:47');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (92,1,2,'Livraison 1.8.6','Préparation de la mise à jour CP2000 en agence.
Livraison prévue Jeudi soir.','2011-05-13',15,5,4,5,14,4,7,'2011-05-19 14:16:41','2011-05-19 18:34:07','2011-05-05',100,null,null,92,1,6,0,'2011-05-19 18:33:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (93,1,2,'Export Import de document interne/externe','Test d''export / import pour un dossier Sur Marseille :

Vous serait-il possible de rapatrier svp l''ensemble des documents externes du dossier 0130/133860/ RCDJ dans ce nouveau dossier 0130/ 134521/RCDJ ?','2011-05-20',5,5,4,4,null,4,2,'2011-05-19 15:38:04','2011-05-19 18:10:02','2011-05-19',100,1,null,93,1,2,0,'2011-05-19 18:10:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (94,2,6,'Script pour descendre un dump journalier de la base MySQL','Script pour descendre un dump journalier de la base MySQL de la base de production DataExchanger','2011-05-25',31,5,4,4,null,4,5,'2011-05-19 17:07:04','2011-06-21 11:51:22','2011-05-23',100,16,null,94,1,2,0,'2011-05-30 09:46:08');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (95,3,2,'Drivers ODBC Solid','L''import des OM Hermès a besoin de drivers ODBC Solid pour chacune des bases de donnée en agence.

Préparer les drivers qu''il faudra installer sur les serveurs agences',null,15,5,4,4,null,4,4,'2011-05-20 12:03:00','2012-03-23 16:19:24','2011-05-20',100,null,null,95,1,2,0,'2012-03-23 16:19:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (96,2,2,'Ajouter une signature en bas de chaque mail','h2. Paul Boyer

Avec la mise en ligne des nouveaux sites du groupe, nous souhaiterions rajouter un lien en signature...

Visitez notre nouveau site : http://www.eurisk.fr

A faire :
* Rendre ceci paramétrable dans CPXPGA.
* C''est l''option -ps <file> dans la commande blat','2011-05-21',6,5,4,6,17,4,4,'2011-05-20 17:03:42','2011-05-26 11:16:15','2011-05-20',100,4,null,96,1,2,0,'2011-05-23 10:32:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (97,3,2,'AQC - Pathologie des installations photovoltaïques en outremer','* Rapatrier tous les rapports sur 1 serveur
* Indexer les documents
* Sortir des listes selon des mots clés

h2. A faire

# Faire un script
## qui récupère les chemins des documents dans CPXDOC pour la guadeloupe, guyane, réunion, martinique
## + commande pour faire la copie
# Définir le serveur qui va être utilisé (test avec 10.31.2.39)
# Indexer les documents (tester google desktop)','2011-06-05',5,5,4,4,null,4,4,'2011-05-23 10:18:01','2011-05-24 21:55:34','2011-06-01',100,8,null,97,1,2,0,'2011-05-24 21:37:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (98,3,5,'Autre - Divers','Tâche dédiée à recevoir le temps passé sur le projet qui n''est pas affecté à une tache précise.

Cela peut prendre la forme de :

* Réunions
* Aide diverse

',null,null,4,null,3,null,4,2,'2011-05-23 12:10:03','2012-02-09 17:27:51','2011-05-16',0,null,null,98,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (99,3,6,'Autre - Divers','Tâche dédiée à recevoir le temps passé sur le projet qui n''est pas affecté à une tache précise.

Cela peut prendre la forme de :

* Réunions
* Aide diverse

',null,null,4,null,3,null,4,4,'2011-05-23 12:11:04','2012-06-08 10:53:50','2011-05-16',0,null,null,99,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (100,3,4,'Autre - Divers','Tâche dédiée à recevoir le temps passé sur le projet qui n''est pas affecté à une tache précise.

Cela peut prendre la forme de :

* Réunions
* Aide diverse

',null,null,4,null,3,null,4,7,'2011-05-23 12:11:45','2012-08-07 16:58:05','2011-05-16',0,null,null,100,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (101,3,2,'Autre - Divers','Tâche dédiée à recevoir le temps passé sur le projet qui n''est pas affecté à une tache précise.

Cela peut prendre la forme de :

* Réunions
* Aide diverse

',null,null,4,null,3,null,4,3,'2011-05-23 12:12:31','2012-10-01 14:04:03','2011-05-16',0,null,null,101,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (102,3,1,'Autre - Divers','Tâche dédiée à recevoir le temps passé sur le projet qui n''est pas affecté à une tache précise.

Cela peut prendre la forme de :

* Réunions
* Aide diverse

',null,null,4,null,3,null,4,0,'2011-05-23 12:13:11','2011-05-23 12:13:11','2011-05-16',0,null,null,102,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (103,2,1,'AQC - Pathologie des installations photovoltaïques en outremer','h2. Objectif 

# Indexer des fichiers à 1 endroit
# Permettre une recherche dans le contenu de ses fichiers

h2. A faire

A partir du fichier joint attachment:rapport.txt qui contient pour chaque ligne un emplacement de fichier source + nom de fichier de destination

* Récupérer les documents sur les serveur agence Eurisk correspondant
** 9710 + 9730 = 10.97.1.1
** 9720 = 10.97.2.1
** 9740 = 10.97.4.1

Faire au mieux pour intégrer cela dans la Ged Alfresco séparé de la Ged Eurisk.
Données à héberger sur la production Pygmalion.

Les données seront temporaires, la démarche pourra se répéter à l''avenir...


* Merci de bien vouloir m''indiquer une estimation de la date à laquelle les documents seront disponible (attention, la liste contient 13000 rapports)
* Et que faut-il comme info pour octroyer les droits aux personnes qui doivent y travailler (une liste d''emails ?).

','2011-05-31',null,5,3,6,null,4,2,'2011-05-24 21:54:53','2011-06-15 12:19:45','2011-05-24',100,16,null,103,1,2,0,'2011-06-15 12:19:45');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (104,2,6,'Darva - RecupérerDocument','Pour chaque abonné chez Eurisk

* Envoyer une requete RecupererDocument
* Mapper les champs dans un fichier CSV',null,32,5,11,5,null,4,8,'2011-05-25 17:56:40','2012-06-15 14:53:25',null,100,null,null,104,1,2,0,'2012-06-15 14:53:05');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (105,2,5,'Migration Symphony Beta1 --> Beta2','Migrer vers l''application Symphony beta2

http://symfony.com/download
','2011-06-03',null,5,8,4,null,4,2,'2011-05-26 09:34:29','2011-05-30 09:37:31','2011-05-26',100,8,null,105,1,2,0,'2011-05-30 09:37:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (106,1,2,'Mail - plantage si envoi en destinataire en copie','CP2000 se ferme violemment si on mets des destinataires en copie sur un envoi mail dans CP2000','2011-05-26',6,5,4,4,18,4,2,'2011-05-26 11:17:35','2011-05-26 12:26:42','2011-05-26',100,4,null,106,1,2,0,'2011-05-26 12:18:17');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (107,2,6,'Import OM Hermès','Importer les OM Hermès par l''intermédiaire de DataExchanger','2011-05-27',28,5,4,4,null,4,8,'2011-05-26 19:14:32','2011-11-18 11:28:07','2011-05-26',40,16,null,107,1,2,0,'2011-11-18 11:28:07');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (108,2,2,'Ajouter le numero de mission dans le lien Darva Construweb','Pour éviter des conflits sur des dossiers avec plusieurs OM (anomalie site Darva), il faut ajouter le paramètre mission avec le champs DOSDRVOM


Avant :

<pre>
https://pro.darva.com/controller/loginTransparent?userid=33628.saisie&password=Rt37d4cx&Sinistre=002SDO11003225&CodeAcam=05450405&Service=S012
</pre>

Après :
<pre>
https://pro.darva.com/controller/loginTransparent?userid=33628.saisie&password=Rt37d4cx&Sinistre=002SDO11003225&Mission=2&CodeAcam=05450405&Service=S012&
</pre>','2011-05-27',33,5,4,4,18,4,1,'2011-05-27 16:13:48','2011-05-27 16:24:32','2011-05-27',100,2,null,108,1,2,0,'2011-05-27 16:24:32');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (109,2,5,'Migration Symphony Beta2 --> Beta3','http://symfony.com/blog/symfony2-beta3-available',null,null,5,8,4,null,4,3,'2011-05-30 09:36:35','2011-06-08 12:14:25','2011-05-30',100,8,null,109,1,2,0,'2011-06-08 12:14:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (110,2,5,'Migration SolidDB --> Mysql','Préparer la migration d''une base de donnée CP2000/SolidDB vers CPWEB/MySQL','2012-01-09',null,5,8,6,null,4,21,'2011-05-31 11:30:55','2013-04-17 14:58:03','2011-06-28',100,128,null,110,1,10,0,'2013-04-17 14:58:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (111,2,5,'Migration de la structure','Créer les tables suivantes selon le modèle de donnée CP2000 :

CPXODC.sql
CPXOMS.sql
CPXOMSDT.sql
CPXMAJ.sql
CPXSMS.sql
CPXFSE_V.sql
CPXMLIS.sql
CPXAAG.sql
CPXACD.sql
CPXADR.sql
CPXAGE.sql
CPXALE.sql
CPXASO.sql
CPXASP.sql
CPXASS.sql
CPXBAC.sql
CPXBAN.sql
CPXBCL.sql
CPXBLN.sql
CPXCAT.sql
CPXCCE.sql
CPXCFO.sql
CPXCHP.sql
CPXCHRONO.sql
CPXCHRONODOC.sql
CPXCLI.sql
CPXCLO.sql
CPXCPH.sql
CPXCPT.sql
CPXCTC.sql
CPXDAT.sql
CPXDDO.sql
CPXDEP.sql
CPXDES.sql
CPXDEV.sql
CPXDOC.sql
CPXDOM.sql
CPXDOS.sql
CPXDOS_V.sql
CPXDPL.sql
CPXDRT.sql
CPXDTR.sql
CPXDTY.sql
CPXDVS.sql
CPXDVT.sql
CPXENH.sql
CPXENT.sql
CPXERD.sql
CPXERF.sql
CPXERN.sql
CPXERV.sql
CPXES1.sql
CPXES2.sql
CPXETD.sql
CPXFAM.sql
CPXFEF.sql
CPXFEX_V.sql
CPXFIG.sql
CPXFOA.sql
CPXFOR.sql
CPXGAR.sql
CPXGES.sql
CPXGRP.sql
CPXHDO_V.sql
CPXIBT.sql
CPXIDO.sql
CPXINRC.sql
CPXINT.sql
CPXIOP.sql
CPXJUD.sql
CPXLGN.sql
CPXLIC.sql
CPXLIGCA.sql
CPXLIGRE.sql
CPXLIS.sql
CPXLNK.sql
CPXLOG.sql
CPXLOP.sql
CPXLOT.sql
CPXMIGAGE.sql
CPXMIGCAT.sql
CPXMIGCLI.sql
CPXMIGEXE.sql
CPXMIGPRT.sql
CPXMIGREJET.sql
CPXMIGREP.sql
CPXMIGTPO.sql
CPXMIGTRANSI.sql
CPXMIGTVA.sql
CPXMIS.sql
CPXMOT.sql
CPXMSE.sql
CPXNAD.sql
CPXNHO.sql
CPXNHO_V.sql
CPXOPE.sql
CPXOPE_V.sql
CPXOPE_V1.sql
CPXOPE_V2.sql
CPXPAS.sql
CPXPAS_V.sql
CPXPAS_V1.sql
CPXPCL.sql
CPXPDM.sql
CPXPDO.sql
CPXPEF.sql
CPXPGA.sql
CPXPHR.sql
CPXPIT.sql
CPXPOD_V.sql
CPXPOL.sql
CPXPRE.sql
CPXPRI.sql
CPXPRO.sql
CPXPRS.sql
CPXPRT.sql
CPXPST.sql
CPXPTA.sql
CPXPUT.sql
CPXPVREC.sql
CPXRAD_V.sql
CPXRCM.sql
CPXREF.sql
CPXREG.sql
CPXREL.sql
CPXREMDOC.sql
CPXREMDOM.sql
CPXREMERD.sql
CPXREMERF.sql
CPXREMERN.sql
CPXREMES1.sql
CPXREMES2.sql
CPXREMIDO.sql
CPXREMPHR.sql
CPXREMRESP.sql
CPXREMSCY.sql
CPXREMTAAG.sql
CPXREMTAGE.sql
CPXREMTBAN.sql
CPXREMTCLI.sql
CPXREMTCLO.sql
CPXREMTCPH.sql
CPXREMTDAT.sql
CPXREMTDEV.sql
CPXREMTDTY.sql
CPXREMTERV.sql
CPXREMTETD.sql
CPXREMTFOR.sql
CPXREMTMIS.sql
CPXREMTMOT.sql
CPXREMTNAD.sql
CPXREMTPRO.sql
CPXREMTPRS.sql
CPXREMTPRT.sql
CPXREMTRGI.sql
CPXREMTSPG.sql
CPXREMTTAL.sql
CPXREMTTAR.sql
CPXREMTTPO.sql
CPXREMTTTI.sql
CPXREMVAT.sql
CPXREP.sql
CPXRESP.sql
CPXREX_V.sql
CPXRGI.sql
CPXRGN.sql
CPXRSE_V.sql
CPXRUB.sql
CPXSCA.sql
CPXSCY.sql
CPXSEF.sql
CPXSER.sql
CPXSEUILS.sql
CPXSMA.sql
CPXSPG.sql
CPXSRE.sql
CPXSTA_V.sql
CPXSTI.sql
CPXSYCODE.sql
CPXTAL.sql
CPXTAR.sql
CPXTAS.sql
CPXTEF.sql
CPXTFO.sql
CPXTNH.sql
CPXTPA.sql
CPXTPO.sql
CPXTRD.sql
CPXTRT.sql
CPXTTI.sql
CPXTVA.sql
CPXVAL.sql
CPXVAT.sql
CPXVIL.sql',null,null,5,8,4,null,4,6,'2011-05-31 11:34:24','2011-07-05 18:23:01',null,0,null,110,110,2,3,0,'2011-07-05 18:23:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (112,2,5,'Migration des données','Préparer un programme qui permet de migrer les données d''une base solid vers une base MySQL

Le programme doit convenir pour toutes les bases solid existantes sur toutes les serveurs :
Rendre paramétrable la source et la destination (IP + port)

*Le programme doit fonctionner sans intervention manuelle autre que le déclenchement.*

# Décrire le process envisagé
# Réaliser le programme
# Préparer la documentation : Processus de migration Solid -> Mysql

','2012-01-09',null,5,8,6,33,4,15,'2011-05-31 11:37:29','2013-04-17 14:57:38','2011-12-12',40,120,110,110,4,5,0,'2013-04-17 14:57:38');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (113,2,5,'Migration Symphony Beta3 --> Beta4','http://symfony.com/blog/symfony2-beta4-available',null,null,5,8,4,null,4,2,'2011-06-06 14:09:39','2011-06-08 12:15:16','2011-06-06',100,null,null,113,1,2,0,'2011-06-08 12:15:16');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (114,2,6,'QBE - Echange Marsh-Qbe -> Hermès','* récupérer les fichiers de Marsh/QBE sur un serveur unix (Pascal précisera les accès)
* les envoyer sur Hermès pour traitement

	

',null,34,5,11,4,99,4,19,'2011-06-06 21:12:17','2013-02-13 11:44:43','2011-07-07',100,16,null,114,1,2,0,'2011-08-26 15:49:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (115,2,2,'Assureur RCD - par défaut = actif','Dans l''écran CPXL076, sur une sélection d''assureur RCD, peux-t-on avoir le choix par défaut des assureurs = Actif au lieu de "Tous"','2011-06-10',35,5,4,4,20,4,2,'2011-06-07 14:51:44','2011-06-07 15:24:47','2011-06-07',100,2,null,115,1,2,0,'2011-06-07 15:06:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (116,2,2,'Affichage de la référence DT dans le détail des documents','Dans l''onglet des documents internes, il faudrait pouvoir voir la référence DT d''un document ajouté au dossier.

En profiter pour retirer l''indice des DT dans la liste des documents types disponibles','2011-06-10',5,5,4,4,20,4,3,'2011-06-07 14:54:07','2011-06-07 15:19:57','2011-06-07',100,4,null,116,1,2,0,'2011-06-07 15:19:57');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (117,3,2,'Livraison 1.8.9','Livraison à préparer...','2011-06-24',null,5,4,4,20,4,4,'2011-06-07 15:23:06','2011-06-21 18:29:03','2011-06-20',100,2,null,117,1,2,0,'2011-06-21 18:29:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (118,3,2,'Préparation site formation','Mettre à jour le site de formation pour Dominique

Vérifier:

* s''il reste des fiches sycodes
* si les dossiers existants gardent en mémoire la validation d''une fiche C/S','2011-06-13',15,5,4,4,null,4,1,'2011-06-07 16:38:54','2011-06-14 23:35:50','2011-06-07',100,4,null,118,1,2,0,'2011-06-14 23:35:50');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (119,3,2,'Différence Assureur OM Hermès vs Assureur Dossier CP2000','Problème :

Dans Hermès, à la création d''un OM, l''utilisateur choisi un client assureur parmi la liste des "clients majeurs"
<pre><code class="sql">select CLIMAJ, MAJLIB from cpxmaj where majai=''A'';</code></pre>

Dans CP2000, le nom de l''assureur de la police provient d''une autre liste (présente dans le paramétrage global de l''application).
<pre><code class="sql">select VALLIBC, VALLIBL from cpxval where liscod = ''LSTAS'';</code></pre>

Voir fichier joint : attachment:difference_assureur_hermes_cp2000.xls

Question :
* Dois-t-on garder 2 listes ?
* Si non, Quelle liste souhaitez-vous garder ?
* Si oui, comment fait-on la jonction entre l''assureur de l''OM et l''assureur du dossier
',null,null,5,4,4,null,4,2,'2011-06-08 14:18:07','2011-06-08 16:42:42','2011-06-08',100,null,null,119,1,2,0,'2011-06-08 16:42:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (120,2,4,'Assureur des OM - Changer la liste, restreindre la saisie','Améliorer la saisie de l''assureur pour éviter des conflits à l''import de l''OM dans CP2000 :

* Le champs assureur dans la création d''OM ne doit plus être saisissable.

* La liste des valeurs possibles doit être mise à jour avec la liste LSTAS de CPXVAL (CP2000)
<pre><code class="sql">select VALLIBC, VALLIBL from cpxval where liscod = ''LSTAS'';</pre></code>

* Prévoir une migration pour l''existant des OM

<pre><code class="sql">select distinct om_compagnie from mission;</pre></code>

Début de correspondance non exhaustive :

|_.HERMES Code Client Majeur|_.Code CP2000 CPXVAL[LSTAS]|
|AGEMI|QBE|
|ALBINGIA|ALB|
|ALLIANZ|ALLI|
|AUXILIAIRE|AUXI|
|AVIVA|AVIVA|
|AVIVA PJ|AVIVA PJ|
|AXA-CORPO|AXACS|
|AXA-FR|AXA|
|AXA-PESSAC|? (AXAP????)|
|GAN-ASS|GANAS|
|GAN-EURO|GANEU|
|GENERALI|GENER|
|GENERALI/EQUITE|EQUIT|
|GROUPAMA|GROUP|

Voir avec Karin Boudet.',null,36,1,null,5,12,4,4,'2011-06-08 16:41:22','2011-07-05 16:37:59','2011-09-05',0,null,null,120,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (121,3,2,'Changement du compteur de numéro de dossier pour Rouen','l’expert Julien Budes de Rouen me demande si ce serait possible de mieux séparer les plages de dossiers entre les bases :
* 0760 – Rouen
* 0140 – Pont l’évèque

Actuellement il y a chevauchement de numéros de dossiers car la première agence tourne dans les 161000-166000 et la 2e dans les 165000-168000 ce qui porte régulièrement à confusion dans les dialogues secrétaire/expert etc… sur les numéro commun présent dans les 2 bases malgré le code agence discriminatoire','2011-06-10',null,5,4,4,null,4,1,'2011-06-08 17:56:03','2011-06-10 11:06:52','2011-06-08',100,1,null,121,1,2,0,'2011-06-10 11:06:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (122,3,2,'Estellon - Ajout Agence CP2000 Avignon (1840)','Il faut ajouter une nouvelle agence CP2000.
C''est pour l''agence ESTELLON EUREXO.

Le code agence est 1840.

* Serveur ?
* Prévoir le paramétrage siège
** Adresse agence ?
** Paramétrage D. Gouveia ?
* Procédure Hermès
* Flydoc (prévoir mail notifesker_1840@eurisk.fr)
** Paramétrage chez Esker
** Installation sur le serveur Eurisk
* Mail ???
** Macro outlook ???
* Fax Rte ???
* Documents par Candice ou Dominique


Possibilité d''un démarrage opérationnel Mi août 2011
','2011-08-26',24,5,4,4,null,4,40,'2011-06-14 12:07:19','2011-09-09 15:22:37','2011-06-27',100,15,null,122,1,18,0,'2011-09-09 15:22:37');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (123,2,2,'Gestion déléguée inaccessible si date d''expertise non passée','L''accès à la case à cocher de la gestion déléguée doit être conditionnée :
* Si la date de prochaine réunion >= date du jour, alors case inaccessible

Attention accès à l''info dans 2 écrans
* CPXT001 - gestion du dossier
* CPXT051 - gestion des documents','2011-06-20',16,5,5,6,null,4,8,'2011-06-14 17:38:33','2011-08-22 11:21:44','2011-06-20',100,4,null,123,1,2,0,'2011-08-22 11:21:17');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (124,2,2,'Allianz - Mail AR Mission automatique','Pour le client ALLIANZ, il faut sur validation d''un document AR Mission (DT ?)

# Envoyer un mail automatiquement à partir de CP2000
# Archiver le mail au dossier
# 

h2. le mail

Il est envoyé au gestionnaire qui missionne + gestionnaire qui paye (si différent du gestionnaire qui missionne)
Le sujet est :
<pre>
Num Sinistre + Nom gestionnaire + Num Police + réf. EURISK + AR
</pre>

-Le DT validé doit être ajouté en pièce jointe avec le nom ARMission.pdf-

Il faut rajouter le lien transparent vers Hermes avec :

* Landing page : https://hermes.eurisk.fr/hermes/dossier.jsp
* idclient = [NUM SINISTRE du dossier]
* login = MailAgf
* pwd = auetng

Le login / pwd est à stocker dans CPXLGN pour l''application "HERMES" et le client Majeur d''Allianz... (fait dans #125)

Exemple :
<pre>
Vous pouvez suivre l''évolution de votre dossier sur le portail Hermès :
https://hermes.eurisk.fr/hermes/dossier.jsp?idclient=B1000610325&login=MailAgf&pwd=auetng
</pre>

h2. Stocker le mail

* Tracer l''envoi dans CPXCHRONO --> consultable dans l''écran liste des envois
* Archiver la réponse dans CPXDOC --> consultable dans l''onglet document/mail

h2. Infos -manquantes-

* Code Client Majeur : ALLIANZ + LAUN-AGF + ALLIANZDOM
* Numéro DT pour les AR Mission : DT206 - DT207 - DT253 - DT630 -  DT785 - DT786 - DT787 - DT891 - CO206 - CO253 - CO207','2011-08-26',6,5,5,6,2,4,26,'2011-06-14 18:26:30','2011-08-29 15:51:25','2011-06-20',100,32,null,124,1,2,0,'2011-08-29 15:51:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (125,2,2,'Allianz - Mail AR Gestion Déléguée','même critère que #124

Sur changement d''état gestion classique --> gestion déléguée :
* Poser la question pour confirmer : Souhaitez-vous envoyer un mail "AR mode de gestion" au gestionnaire Allianz ?
* Si oui,
## Envoyer un mail à Allianz
*** avec le lien Hermes dans le corps du mail (voir #124).
*** différence 1 : Remplacer "AR" par le mot clef "DELEX"
*** différence 2 : aucun document à joindre
## Tracer l''envoi (CPXCHRONO)
## Archiver le mail (CPXDOC)','2011-08-26',6,5,5,6,2,4,32,'2011-06-15 13:50:57','2011-08-29 15:51:44','2011-07-04',100,24,null,125,1,2,0,'2011-08-29 15:51:44');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (126,3,4,'Documentation utilisateur','Document utilisateur à mettre à jour

https://hermes.eurisk.fr/hermes/doc/GuideUtilisateur.pdf',null,null,5,4,4,11,4,4,'2011-06-15 13:57:03','2011-07-25 14:53:23','2011-06-20',100,24,null,126,1,2,0,'2011-07-05 16:33:50');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (127,1,2,'CPXG076 - Support : créer un opération seule','2 problèmes :

# Impossible de créer une opération sur la bonne police dans le cas ou plusieurs police ont le même numéro de contrat assureur : A cause de la scroll bar, les controles sur l''action de créer l''opération se fait sur la mauvaise police (voir image jointe)
# Impossible de créer une opération sur une police seule fraichement créée
** Il faut rajouter une condition seulement pour l''écran support :
*** si ma police est par alimentation --> OK
*** si ma police est de type construction --> -OK- --> KO
*** -*si ma police à 0 opération* --> OK- 
# Remplacer le message d''erreur pour la condition sur le type construction :
> Le type %%TPOCOD.CPXPAS%%% ne permet pas la création d''une nouvelle opération','2011-07-19',29,5,5,4,2,4,7,'2011-06-15 15:39:22','2011-07-07 10:10:45','2011-07-18',100,16,null,127,1,2,0,'2011-07-07 10:10:45');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (128,1,2,'Nommage fichier pièce jointe : compteur doublon à parfaire','h2. Problème

Dominique 18.11.2010 :

<code>
j''ai fait 1er envoi que j''ai annulé les pj se sont numérotés 2e et 3e.
OH!! zut j''ai oublié un destinataire je refais un envoi avec même pj
ça se numérotent 4e2e et 5e3e et zut je suis vraiment pas réveillé je me suis trompé de destinaire
donc je recommence et maintenant 3 Numéros
</code>

h2. A faire

Améliorer le système de nommage des pièces jointes concernant la règle de gestion anti-doublon.

actuellement :
* 1er envoi, si doublon, nommage = 2enom.pdf, 3enom.pdf
* 2e envoi suite à annulation, nommage = 4e2enom.pdf, 5e3enom.pdf
* 3e envoi suite à encore annulation, nommage = 6e4e2enom.pdf, 7e5e3enom.pdf

voir pour arriver au résultat :

* 1er envoi, si doublon, nommage = 2enom.pdf, 3enom.pdf
* 2e envoi suite à annulation, nommage = 2enom.pdf, 3enom.pdf
* 3e envoi suite à encore annulation, nommage = 2enom.pdf, 3enom.pdf

le préfixe compteur+"e" peut éventuellement être changé si besoin.

h2. Composant

* CPXL010B',null,6,1,null,4,null,4,1,'2011-06-15 16:22:47','2011-06-15 16:23:24',null,0,24,null,128,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (129,1,2,'Lock sur dossier disparu : 2 utilisateurs en modification = plantage','h2. Problème

Un utilisateur travail sur le dossier. Le dossier n''est pas considéré comme étant bloqué et en cours d''utilisation... Un autre utilisateur ouvre le dossier. Le premier utilisateur reçoit des alertes comme quoi il est impossible d''enregistrer son travail.

h2. A Faire

Voir pourquoi le lock saute pour le 1er utilisateur. Vérifier après la validation définitive des documents et la valorisation d''une NH. Si le lock disparait, modifier pour relocker de suite le dossier !',null,16,1,null,4,null,4,0,'2011-06-15 16:24:52','2011-06-15 16:24:52',null,0,24,null,129,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (130,2,2,'Faxbox - Tester la différence entre un envoi normal et un envoi fin','> Le forfait inclut en effet 4 400 Pages en émission/mois, Les télécopies expédiées en mode fin sont comptabilisées un équivalent de 1,5 pages (selon nos conditions générales de vente) ce qui justifie cette différence, le mode fin étant défini par défaut. Cette configuration pouvant être modifiée à votre demande vers le mode normal

# Comparer un envoi normal et un envoi fin
# Décider si on change le mode fin vers le mode normal','2011-07-01',11,5,4,4,null,4,5,'2011-06-15 17:53:43','2011-06-29 13:50:15','2011-06-21',100,null,null,130,1,2,0,'2011-06-28 10:55:09');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (131,3,2,'Dossier à cloturer sur la base IDFO','De : Eurisk IDF Est/BATISTA Maria 
Envoyé : lundi 30 mai 2011 15:05
À : CPCOM/Support
Objet : DOSSIERS A CLOTURER SUR LA BASE IDFO
Importance : Haute

Bonjour, 
suite au transfert de dossiers de la base 78 à la base 91 merci de fermer les dossiers suivants (qui n''avaient pas encore de rapports) et que nous ne pouvons pas fermer :
il s''agit des dossiers suivants : 

- 0780/645122
- 0780/645121
- 0780/644931
- 0780/644925
- 0780/644914
- 0780/644686
- 0780/644300
- 0780/644291
- 0780/643821
','2011-06-28',null,5,4,4,null,4,2,'2011-06-15 17:59:13','2011-06-28 11:42:02','2011-06-28',0,2,null,131,1,2,0,'2011-06-28 11:42:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (132,2,5,'Migration Symphony Beta4 --> Beta5','http://symfony.com/blog/symfony2-beta5-available','2011-07-04',null,5,8,4,null,4,2,'2011-06-17 09:43:52','2011-06-28 15:35:36','2011-07-04',100,8,null,132,1,2,0,'2011-06-28 15:35:36');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (133,1,2,'Test sur copie dos de fichier','Le test sur la copie de fichier du CPXS101 est faux pour un OS windows 7.
C''est le même problème que #18 : Reporter la solution r86
','2011-06-17',37,5,5,6,20,4,6,'2011-06-17 10:33:18','2011-06-21 14:53:10','2011-06-17',100,4,null,133,1,2,0,'2011-06-21 14:53:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (134,2,4,'Création d''un OM à partir d''une police Assureur','Pour Qbe/Marsh, il faut rajouter un bouton "créer un OM" sur la police.

Le formulaire de création d''OM allégé par rapport à l''existant pour la plateforme.
Les champs du formulaire sont à définir.
Le process lié à la création d''OM est à définir

(Avec Marsh/QBE)',null,36,1,null,4,null,4,7,'2011-06-17 16:52:15','2012-03-06 09:42:30',null,0,null,null,134,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (135,3,7,'Tests Selenium','','2011-08-31',null,5,3,4,11,4,14,'2011-06-21 15:29:39','2012-01-05 09:40:52','2011-07-11',0,null,null,135,1,2,0,'2012-01-05 09:40:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (136,2,1,'fix dashlet site internet explorer','js','2011-06-21',null,5,3,4,16,3,3,'2011-06-21 16:19:05','2012-10-23 09:47:42','2011-06-21',100,null,77,77,2,3,0,'2011-06-22 17:11:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (137,1,1,'fix dashlet flux rss ie','js',null,null,6,3,4,16,3,1,'2011-06-21 16:19:31','2011-06-23 11:25:16','2011-06-21',100,null,77,77,4,5,0,'2011-06-23 11:25:16');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (138,1,1,'fix dashlet news ie','js',null,null,5,3,4,16,3,2,'2011-06-21 16:19:55','2012-10-23 09:48:01','2011-06-21',100,null,77,77,6,7,0,'2011-06-23 11:26:04');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (139,1,1,'fix dashlet dernieres activités ie','js',null,null,5,3,4,16,3,3,'2011-06-21 16:21:41','2012-10-23 09:48:08','2011-06-21',100,null,77,77,8,9,0,'2011-07-08 09:39:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (140,1,1,'packaging 1.1.2','re-builder les paquêts rpm',null,null,2,3,4,16,3,1,'2011-06-21 16:25:54','2011-07-12 16:04:02','2011-06-21',0,null,null,140,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (141,1,1,'rsync + import','rsync données du siège
import dans nouveau pygmalion',null,null,5,3,4,null,3,2,'2011-06-21 16:26:38','2011-08-31 15:01:13',null,0,null,null,141,1,2,0,'2011-08-31 15:00:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (142,1,1,'reset profils pour tous les utilisateurs','afin de leur proposer la page d''accueil par défaut',null,null,3,3,4,16,3,3,'2011-06-21 16:30:57','2011-07-07 16:52:58','2011-06-21',100,null,null,142,1,2,0,'2011-07-07 16:52:58');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (143,1,1,'auth dashlet actualités','','2011-06-22',null,3,3,4,16,3,2,'2011-06-21 16:53:56','2011-06-22 17:11:42','2011-06-22',100,null,null,143,1,2,0,'2011-06-22 17:11:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (144,2,2,'Livraison 1.9.0','Préparer la livraison.
Sujets pour les mails Allianz...','2011-08-21',null,5,4,4,2,4,8,'2011-06-21 19:04:08','2011-09-03 10:30:42','2011-08-21',100,4,null,144,1,2,0,'2011-08-31 13:58:28');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (145,2,5,'Gestion des documents','Concerne la liste des tâches pour gérer les documents du dossier dans CPWEB',null,18,5,null,4,null,4,8,'2011-06-22 15:25:04','2013-04-17 15:00:06','2011-06-22',100,null,null,145,1,8,0,'2012-02-03 13:33:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (146,2,5,'Vérifier la faisabilité','Il faut s''assurer que gérer les documents sans Microsoft Word est possible.

--> voir pour utiliser l''expertise Sensio ?',null,18,5,4,4,null,4,10,'2011-06-22 15:29:20','2012-09-10 09:30:29','2011-06-22',30,null,145,145,2,3,0,'2012-02-03 13:34:30');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (147,2,5,'Mise en page','Tutoriel pour aider à la réalisation de la mise en page :
http://j-place.developpez.com/tutoriels/php/ameliorez-vos-applications-developpees-avec-symfony2/

Le but sera d''obtenir la structure suivante :
!structure.png!',null,null,5,8,4,34,4,9,'2011-06-23 10:31:00','2012-03-12 09:46:20','2012-01-26',30,null,null,147,1,2,0,'2012-03-12 09:46:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (148,3,2,'Purger les tables de transferts sur siege référence','Supprimer les enregistrements 2010','2011-06-23',null,5,4,4,null,4,1,'2011-06-23 11:06:54','2011-06-23 11:15:10','2011-06-23',100,1,null,148,1,2,0,'2011-06-23 11:15:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (149,2,5,'Migration Symphony Beta5 --> RC1','http://symfony.com/blog/symfony2-2-0-rc1-released','2011-07-04',null,5,8,4,null,4,3,'2011-06-24 16:01:19','2011-06-28 15:36:01','2011-07-04',100,null,null,149,1,2,0,'2011-06-28 15:36:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (150,2,2,'Estellon - Paramétrage du Serveur','Installer CP2000 
* serveur 10.78.1.11, 
* port 1321
* code agence = 1840
* Nom = Estellon Avignon','2011-07-13',null,5,4,4,null,4,6,'2011-06-27 10:23:25','2011-07-12 17:52:42','2011-07-11',100,4,122,122,2,3,0,'2011-07-12 16:59:56');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (151,2,2,'Estellon - Paramétrage du Siège','Enrichir le paramétrage du siège avec
* La nouvelle agence 1840 (voir #150)','2011-07-13',null,5,null,4,null,4,4,'2011-06-27 11:14:18','2011-07-12 17:07:00','2011-07-11',100,1,122,122,4,5,0,'2011-07-12 17:06:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (152,3,2,'Estellon - Identifier l''adresse Mail','h2. Contacts

Discuter avec 
* Paul Boyer
* Dominique Santoni
* Carole GROSJEAN

Informer 
* F. Amant
* C. Miquel
* D. Gouveia

h2. Etat des lieux

Pour définir quelle adresse mail "agence" sera utilisée. L''adresse mail est utilisé à 3 endroits différents dans CP2000 :
 
h3. Service Flydoc - Dématérialisation du courrier

N''importe quel mail.

Le mail sert à identifier l''utilisateur et à réceptionner les retour du prestataire : erreurs de traitements, NPAI etc...

h3. Service Faxbox - Dématérialisation du fax

NB : impossible de faire rentrer dans le contrat Eurisk actuel.
Faut il prévoir un nouveau contrat ?
Si oui, qui s''en occuper ?
Si non, doit-on faire un avenant sur notre contrat Eurisk ?

Dans tous les cas, le paramétrage actuel de CP2000 prévoit de prendre le même mail que celui des entêtes courriers (donc le mail du bureau principal de l''agence)

h3. Macro d''archivage de la boite agence 

Cette fonctionnalité est lié à l''utilisation de la messagerie "Agence" sur le serveur.
Il faut disposer d''une messagerie agence à la norme Eurisk pour que cela fonctionne.
Dans l''état actuel des choses, la fonction ne sera pas disponible.','2011-07-08',null,5,4,4,null,4,9,'2011-06-27 12:01:01','2011-07-07 10:27:01','2011-06-27',100,2,122,122,6,7,0,'2011-07-07 10:27:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (153,2,2,'Estellon - Création compte Flydoc Manager','Créer un compte sur www.flydoc.fr

Prérequis : une adresse mail','2011-07-11',null,5,null,4,null,4,5,'2011-06-27 12:01:21','2011-07-12 13:34:03','2011-07-11',100,1,122,122,8,9,0,'2011-07-12 13:34:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (154,2,2,'Estellon - Procédure Hermès','Installer ou adapter la procédure d''export vers Hermès','2011-07-22',null,5,4,4,null,4,4,'2011-06-27 14:32:08','2011-07-26 09:50:34','2011-07-18',100,4,122,122,10,11,0,'2011-07-26 09:50:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (155,3,2,'Estellon - Installation Flydoc','Installer Flydoc pour l''agence 1840 dans C:\\Progi\\Flydoc','2011-07-14',null,5,null,4,null,4,9,'2011-06-27 14:33:49','2011-07-12 17:07:16','2011-07-12',100,2,122,122,12,13,0,'2011-07-12 17:07:16');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (156,3,2,'Estellon - Flydoc - alias notifesker','Créer l''alias notifesker_1840@eurisk.fr
Rediriger vers ? (voir #152)','2011-07-11',null,5,null,4,null,4,6,'2011-06-27 15:39:11','2011-07-12 13:31:19','2011-07-11',100,1,122,122,14,15,0,'2011-07-12 13:31:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (157,3,2,'Test PC Scan avec Adobe Acrobat 7','Tester :

- l''ouverture d''un document PDF
- l''envoi par mail d''un document
- l''envoi par mail de plusieurs documents dont document d''origine word (interne) / document pdf (externe)
- l''envoi vers l''imprimante (_sensible_)
- le scan ?
- la séparation de document','2011-07-29',null,5,null,4,null,4,7,'2011-06-27 18:56:09','2012-04-25 09:57:42','2011-06-29',100,2,null,157,1,2,0,'2012-04-25 09:57:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (158,2,5,'Exemple migration','Faire un exemple de migration de CPXTPO/CPXPAS avec Php/Symfony2

Pour récupérer les tables solid, sur le serveur de dev, utiliser les commandes :
(mais il doit déjà y avoir un C:\Partage\cp2000.zip avec les fichiers sur cpxtpo et cpxpas)

<pre>C:\Progi\UtilSolid>solexp "tcp 10.31.2.39 1320" admincpx admincpx CPXTPO
C:\Progi\UtilSolid>solexp "tcp 10.31.2.39 1320" admincpx admincpx CPXPAS</pre>

Les entity Symfony2 sont déjà créées voir TypePolice et PoliceAssurance :
source:trunk/src/CPCOM/CPWebCommonBundle/Entity
','2011-06-28',null,5,3,6,null,4,2,'2011-06-27 19:57:06','2011-07-05 18:15:38','2011-06-28',100,8,110,110,6,7,0,'2011-07-05 18:15:38');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (159,2,2,'Suivi des dossiers - critère supplémentaire','De : Eurisk Dauphiné Savoie/LOUVAT Barbara 
Envoyé : lundi 27 juin 2011 13:39
À : CPCOM/Support
Objet : Concerne CP2000 : CPXC005 suivi des dossiers et du stock

Bonjour à toute l''équipe

Peut-on afficher un critère de recherche supplémentaire "Responsable administratif " dans :

CPXC005 : Suivi des dossiers et du stock

Cela nous permettrait de mieux nous rendre compte de la répartition des dossiers.

Merci de me répondre

Barbara LOUVAT 
','2011-07-20',16,5,4,3,2,4,5,'2011-06-29 13:53:40','2011-07-08 11:30:33','2011-07-20',100,8,null,159,1,2,0,'2011-07-08 11:28:39');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (160,2,5,'Gestion des clients majeurs','A faire :

* créer un client majeur
* visualiser un client majeur
* modifier un client majeur
* supprimer un client majeur

Spécification complète à suivre...','2011-07-08',38,5,8,4,32,4,9,'2011-06-29 16:50:30','2012-09-10 09:30:14','2011-06-29',100,40,null,160,1,2,0,'2012-09-10 09:30:14');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (161,1,4,'Confidentialité - Hermes affiche tous les dossiers','Anomalie dans Hermès Construction en production actuellement.
L’utilisateur  concerné était le suivant :
login=LaPaix
pwd=sd84f9

Il était rattaché à un client mineur PAIX075-01 inexistant

Si le code client n’existe pas, l’utilisateur a accès à l’intégralité de la base.
Le comportement attendu est que l’on accède à 0 dossier.

Tracking Cité SI : n° 228.
','2011-07-22',2,2,null,4,null,4,5,'2011-06-30 10:45:09','2011-08-16 16:56:45','2011-07-18',20,null,null,161,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (162,3,2,'rapports "Carrelages" pour la SMABTP','De : Eurisk Siege/BOYER Paul 
Envoyé : vendredi 1 juillet 2011 17:10
À : CPCOM/BOUREZ Vincent; Eurisk Siege/PEYROU Irina
Objet : Les rapports "Carrelages" pour la SMABTP


Richard LOURS demande si il est possible de compter combien de tels rapports (DT1029) on été émis à l’attention de marie-pierre_peycelon@socabat.fr depuis leur mise en service le 3 mai ?
Si oui : lui donner le Nb et la période correspondante.

Merci
PB
','2011-07-08',null,5,null,4,null,4,3,'2011-07-04 09:23:05','2011-07-05 16:08:13','2011-07-04',100,4,null,162,1,2,0,'2011-07-05 16:08:13');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (163,3,2,'Analyse pathologique des sinistres : pompes à chaleur','_____________________________________________
De : Eurisk Siege/BOYER Paul 
Envoyé : vendredi 1 juillet 2011 16:33
À : CPCOM/DEHEE Thomas; CPCOM/BOUREZ Vincent
Cc : CPCOM/AMANT Francois
Objet : Serveur d''analyse Pathologique des sinistres (indexation des rapports)


Les premiers tests sont concluants, même si l’outil de recherche est un peu primaire !

Il se trouve que nous avons maintenant une étude à faire sur les sinistres relatifs à des pompes à chaleur...
Pour cela, le périmètre de l’étude est national. Avec cette étude en grandeur réelle, on y verra nettement plus clair sur ce qu’il y aurait lieu de faire par la suite pour optimiser le système.

Donc pour qu’on puisse travailler et réaliser cette première étude, il faut transférer l’ensemble des rapports sur le serveur en appliquant la procédure qui a été utilisée pour les DOM.
Objectif : il faudrait pouvoir accéder à cette base des rapports au mois d’aout.

Comme le serveur est installé et que la macro commande de transfert a déjà tournée, j’ai tendance à penser que ce doit âtre faisable ?
Reste la question du volume ?

Pour les DOM : 13 320 rapports occupent 2,77 Go
On va passer à 300 000 rapports soit 62 Go voire 80 => pas de quoi effaroucher une vierge…

Reste donc à faire lancer le boulot agence par agence : qui s’en charge ?

Merci

Paul BOYER
','2011-08-19',null,5,4,4,null,4,4,'2011-07-04 09:25:14','2011-07-06 17:22:03','2011-08-16',100,16,null,163,1,2,0,'2011-07-06 17:22:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (164,2,2,'Dossiers à fort Enjeux','h2. Existant :

quand l''enjeu et/ou l''indemnité du dossier sont renseignés > 76 000 €, le message suivant s''affiche : 
"Votre dossier est classé ''Fort enjeu'' chez certains clients, consultez la SM076, merci."

Aucune obligation pour la saisie de l''enjeu ou de l''indemnité 

h2. Souhait :

Rendre la saisie obligatoire de l''enjeux dans le détail du dommage avant la -rédaction- validation du rapport.

h2. Mise en place : 

h3. Paramétrage initial

Enrichir la table CPXMIS avec l''enjeu initial à priori (MISMTENJINI). Voici les valeurs à paramétrer par défaut :

|Type de Mission|Valeur de l''Enjeu Initial Renseignée à priori à l’ouverture de la mission|
|Dommages Ouvrage Amiable|1 900 €|  
|Dommages Ouvrage Judiciaire|1 900 €|   
|Eval. de Risques en vue de Sousc|0 €| 
|Globale Chantier Amiable|0 € |
|Mission tech. sachant|0 €|    
|Police Unique de Chantier Amiable|1 200 €|  
|Protection Juridique Amiable|0 €|
|Protection Juridique Judiciaire|0 €|
|Responsabilité Civile Amiable|900 €|   
|Responsabilité Civile Déce Judic|1 300 €|   
|Responsabilité Civile Déce. Amia|1 400 €|   
|Responsabilité Civile Judiciaire|0 €|
|Sècheresse|0 €|
|Tous Risques Chantier Amiable|0 €| 
|Tous Risques Chantier Judiciaire|0 €| 
|Autres Missions|0 €|

A la création d''une mission, le dommage de la mission doit hériter par défaut du montant paramétré dans MISMTENJINI de CPXMIS.

Rajouter le champs de l''enjeu HT dans les écrans de création de mission (CPXG001, CPXG002)


h3. Contrôle validation du rapport

Bloquer la -sélection- validation de document présents dans la liste des DT à bloquer si contrôle positif.

h5. Paramétrage (créer 1 entrée CPXLIS/CPXVAL) :

DT204
DT050
DT099
DT222
DT440
DT226
DT005
DT156
DT494
DT496
DT542
DT546
DT548
DT596
DT590
DT573
DT610
DT621
DT549
DT513
DT061
DT604
DT059
DT058
DT669
DT664
DT723
DT724
CO058
CO059
CO061
CO204
CO222
CO226
CO339
CO544
CO548
CO604
DT740
DT813
DT825
DT820
DR821
DT821
CO549
DT832
DT837
CO837
AJ204
AJ226
AJ813
AJ679
AJ222
AJ837
AJ621
AJ549
AJ058
AJ604
AJ548
AJ061
AJ059
AJ339
YC903
DT905
YC904
CO813
CO905
DT905_
DT941
DT943
DT944
DT945
DT1013
DT992
DT1001
DT980
DT395
DT535
DT789
DT790 
DT791
DT792
DT793
DT851
DT852
DT894

h5. Contrôle 1

Pour tous les dommages, il faut un enjeux ou une indemnité différente de 0 quelque soit la position (garanti ou non)

Donc vérifier sur chaque dommage :

<pre>
Si enjeu HT est vide et TVA HT est vide
ou enjeu HT + TVA HT = 0
</pre>

Afficher le message : 

> "Avant toute validation de rapport, veuillez renseigner l''enjeu de chaque dommage" 

h5. Contrôle 2

Si l''enjeu HT = l''enjeu par défaut du type de mission, alors il faut demander confirmation que le montant est bien celui souhaité :
"L’Expert confirme-t-il la valeur actuelle de l’Enjeu ?" -> Oui/Non
Si oui, on valide le document.
Si non, on retourne en arrière pour correction.

h3. Mail automatique à la DT + DJ 

h5. Ajouter 3 paramètre dans CPXPGA

Montant fort enjeux (76 000)
Mail Direction Juridique (?)
Mail Direction Technique (?)

h5. déclenchement

Lorsque l''enjeu d''un dossier > 76000, envoyer un mail à la DT + DJ + copie agence

Flaguer le dossier "Fort enjeu" 
Donc rajouter un champs dans CPXDOS : 
* DOSFORTENJEUX (Char1)- les valeurs possibles seront F=Non/T=Oui/M=Oui+mail envoyé

Prévoir un script SQL pour valoriser le champs sur les dossiers existants (calculer l''enjeux, selon le resultat, initialiser à NON ou à OUI

Effectuer le contrôle
* sur enregistrement de l''écran des dommages (CPXG015)
* sur validation des rapports

h5. mail

Objet : "Fort Enjeu - [CodeAgence/NumeroDossier/TypeMission] - [NumSinistre]"
Corps du message :
Bonjour,

Le dossier [CodeAgence/NumeroDossier/TypeMission] est classé "Fort Enjeu" avec un montant total de [Somme des enjeux] euros.

Liste des dommages :
[Numéro Dommage 1 (DOMRG)] - [dommage déclaré (DOMDEC)] - [Enjeu]
[Numéro Dommage 2 (DOMRG)] - [dommage déclaré (DOMDEC)] - [Enjeu]
[Numéro Dommage 3 (DOMRG)] - [dommage déclaré (DOMDEC)] - [Enjeu]

',null,9,5,5,6,24,4,23,'2011-07-04 09:31:20','2011-10-14 10:14:05',null,100,120,null,164,1,2,0,'2011-10-14 10:14:05');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (165,2,2,'Envoi de convocation et rapport non sécurisé','De : Eurisk Siege/BOYER Paul 
Envoyé : vendredi 1 juillet 2011 10:12
À : Eurisk Siege/SANTONI Dominique
Cc : CPCOM/BOUREZ Vincent
Objet : RE: envoi de convocation et rapport non sécurisé

Il semblerait que l’agence de Marseille ignore cette subtilité, 3 oublis : ça fait beaucoup !

La question serait de mettre une alerte sur la nécessité de poster les documents validés dans certains cas
Sinon le travail n’est pas terminé !

Cela vaut pour les convocations (dans tous les cas) 
Mais surtout pour les Rapports qui peuvent être oubliés en pensant qu’on est dans du zéro papier…
L’alerte n’apparaîtrait que pour les codes clients où il n’y a pas de convention « zéro papier » et ou aucun postage n’a été fait.
Et là, si on oublie de cocher la case (ou de e-poster), l’alerte reste active.
Cela n’empêchera pas d’enlever l’alerte en cochant la case, même si le courrier n’est pas parti…

PB
De : Eurisk Siege/SANTONI Dominique 
Envoyé : vendredi 1 juillet 2011 08:28
À : Eurisk Siege/BOYER Paul; CPCOM/BOUREZ Vincent
Objet : RE: envoi de convocation et rapport non sécurisé

Bonjour 
Dans le système actuel, il est possible d''avoir la traçabilité d''un envoi posté manuellement par l''agence : menu Imprimante et une case à cocher Manuel. 
Nous avons donc  une traçabilité de tous nos envois.
Bonne journée
cordialement
Mme Dominique SANTONI 
EURISK 
Tél : 04.98.00.38.82 
Port :  06.22.08.29.25 
courriel : dominique.santoni@eurisk.fr 
Afin de contribuer au respect de l''environnement, merci de n''imprimer ce courriel que si nécessaire ! 

De : Eurisk Siege/BOYER Paul 
Envoyé : jeudi 30 juin 2011 19:33
À : CPCOM/BOUREZ Vincent
Cc : Eurisk Siege/SANTONI Dominique
Objet : TR: envoi de convocation et rapport non sécurisé
Importance : Haute

On a déjà évoqué le problème du courrier validé, mais qu’on oublie de poster…
Il faudrait noter quelque part de mettre en place un système d’alerte si il n’y a pas d’e-poste pour les Convocs et les Rapports…

Par contre cela peut avoir été posté manuellement, mais là comment garder la traçabilité ?
	Solution : Scanner l’enveloppe timbrée (avec la date) dans le dossier avant remise en poste ?

Voila qui limiterait l’usage de ce système ancien appelé à disparaître ?
On en parler un de ces jours ?

PB
De : Eurisk Provence/OGGIANO Jean-marc 
Envoyé : jeudi 30 juin 2011 18:57
À : Eurisk Siege/BOYER Paul
Cc : Eurisk Languedoc Roussillon/CASONI Jean-philippe; Eurisk Provence/OGGIANO Jean-marc
Objet : envoi de convocation et rapport non sécurisé
Importance : Haute

Bonsoir,

Nous rencontrons sur l''agence, quelques cas récents  ( 3 identifiés à ce jour) de documents essentiels (convoc  ou rapport DO) validés mais non envoyés…

…avec l''incidence que l''on imagine sur la RC Eurisk.

Ces anomalies, d''un genre nouveau,  sont sans doute le pendant de la "dématérialisation".

La répétition s''soulève la question d''une sécurisation de l''action de validation.

En première analyse, je propose que l''action de validation du document soit assujettie à une expédition dans les 15mn par eposte, ou d''une formalisation de l''engagement nominatif de la secrétaire à assurer l''expédition "papier".

A défaut la validation du document est alors  annulée et un message d''alerte transmis à la boite agence et la boite de la secrétaire concernée.

Bonne réflexion,

Jean Marc OGGIANO
',null,null,1,null,4,null,4,1,'2011-07-04 09:49:14','2011-07-04 09:51:12',null,0,40,null,165,1,4,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (166,2,2,'Signalisation des documents validés non envoyés','h2. Sur l''écran documents

* Pour les clients majeurs non zéro papier
* Pour les documents validés
* Pour les documents de type "convocation", "rapport"

On affiche une petite icône
* verte si envoi OK (tous les destinataires)
* orange si envoi partiel (manque des destinataires)
* rouge si envoi KO (aucun envoi)',null,5,1,null,4,null,4,2,'2011-07-04 09:51:12','2012-10-23 12:00:55',null,0,40,165,165,2,3,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (167,2,6,'Alerte sur les documents validés non envoyés','Tous les jours, il faut vérifier que les documents validés soient bien envoyés.
A défaut, un mail doit être envoyé à l''agence

NB : s''aider du cas de Brive #57',null,28,1,null,4,52,4,1,'2011-07-04 09:54:55','2012-05-16 12:18:55',null,0,null,null,167,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (168,3,3,'Install Macro v2 ','h3. Problème

Impossible d''installer proprement la macro v2 sur une session car la GPO écrase systématiquement le paramétrage avec l''install de la macro v1.

h3. Solution temporaire

A l''ouverture de la session, j''install un patch pour écraser la v1 par la v2','2011-07-04',null,5,4,4,null,4,1,'2011-07-04 11:38:33','2011-07-04 11:56:16','2011-07-04',100,1,null,168,1,2,0,'2011-07-04 11:56:16');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (169,1,4,'Onglet document technique','Demandé à Nathalie :

* L’onglet technique et l’onglet antériorité doivent avoir le même filtre sur les numéro de dossier
* Le filtre souhaité actuellement pour trouver les dossiers est uniquement le numéro de police. (Je me le suis fait confirmé par P.Boyer vendredi dernier)
* Pour éviter de faire une recherche sur « 6.xx » comme écrit dans le document, tu peux filtrer uniquement sur la famille « EX5 » (code nature, voir doh_codenat). La famille EX5 est le classement qui doit contenir tous les documents liés à l’opération. Inutile de filtrer sur 6.xx.
','2011-07-29',39,5,null,4,11,4,3,'2011-07-04 15:03:41','2011-08-30 17:20:44','2011-07-18',100,4,null,169,1,2,0,'2011-08-26 15:46:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (170,1,6,'[E74B10] : Problème accent sur l''exploitation d''un webservice','','2011-07-04',31,5,4,4,null,4,2,'2011-07-04 15:11:44','2011-07-05 12:12:25','2011-05-05',100,null,null,170,1,2,0,'2011-07-05 12:12:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (171,3,4,'Test montée de version','Sujet traité par Pascal.

# Reproduction d''une prod
# Montée de version selon doc d''install de Cité-SI','2011-07-15',null,5,null,4,10,4,3,'2011-07-04 16:08:00','2011-07-25 15:05:36','2011-06-28',100,null,null,171,1,2,0,'2011-07-25 15:05:36');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (172,3,2,'Accès Darva Martinique/Guadeloupe','D. GOUVEIA a créé 2ème Site sur la GUADELOUPE sur lequel tu peux mettre l''accès à DARVA/MARTINIQUE.
Merci de m''informer dès que c''est fait pour que j''explique aux assistantes comment ça fonctionne.
','2011-07-05',33,5,4,4,null,4,2,'2011-07-04 17:55:43','2011-07-05 11:38:19','2011-07-04',100,2,null,172,1,2,0,'2011-07-05 11:38:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (173,2,5,'Migration Symphony RC3 --> RC4','http://symfony.com/blog/symfony2-2-0-rc4-released','2011-07-08',null,5,8,4,null,4,2,'2011-07-05 10:40:04','2011-07-05 18:14:01','2011-07-05',100,4,null,173,1,2,0,'2011-07-05 18:14:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (174,1,6,'Webservice Gsicass - Liste des OM','Récuperer un OM Gsicass

L''intégrer dans la bonne base CP2000/Solid

Attention : problème de gestion avec existant Plateforme/Hermes, étudier le sujet avec Karin Boudet',null,40,5,null,4,null,4,1,'2011-07-05 12:15:22','2012-04-24 10:22:27','2011-07-05',100,null,null,174,1,2,0,'2012-04-24 10:22:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (175,3,7,'Test montée de version','Montée de version V4 --> V5.0.? assurée par Pascal.','2011-09-02',null,5,null,4,11,4,8,'2011-07-05 16:19:12','2012-01-05 09:41:17','2011-08-29',0,16,null,175,1,2,0,'2012-01-05 09:41:17');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (176,2,4,'Analyse full text des rapports','Recherche :

# je dois pouvoir filtrer sur des documents de nature rapport
# préciser d''autres critères (type de mission, fourchette date DROC)
# rechercher une expression clef

Résultat sous forme de "page tableau" classique Hermès avec possibilité d''export
+ ouverture du document possible',null,39,1,null,4,23,4,2,'2011-07-05 16:43:54','2011-09-22 15:45:24','2011-10-31',0,null,null,176,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (177,2,4,'Onglet réclamation','Importer les données réclamations de CP2000',null,41,1,null,4,23,4,1,'2011-07-05 16:44:53','2011-07-05 16:47:37','2011-10-31',0,null,null,177,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (178,2,5,'Contraintes sur les relations des entités','Voir pour implémenter les restrictions et les suppression en cascade dans le modèle de donnée CPWEB',null,null,6,null,4,null,4,2,'2011-07-05 18:21:26','2012-02-03 13:43:52',null,0,null,110,110,8,9,0,'2012-02-03 13:43:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (179,3,2,'correction script nettoyage.cmd','* prendre compte de l''heure pour la recherche de l''erreur
* virer mon adresse mail, remplacer par support','2011-07-13',24,5,4,4,null,4,5,'2011-07-11 17:30:26','2011-07-18 16:40:42','2011-07-11',100,2,null,179,1,2,0,'2011-07-11 17:40:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (180,1,2,'Rédaction document - début/fin de liste - message d''erreur HS','Le test sur le retour de la fonction "CPXS011".MAJDOCVAR est HS suite à l''evol 132 - NG - 04/05/2010

# déplacer le code de l''évol 132 en dessous du test d''erreur
# nettoyer les commentaires','2011-07-13',5,5,5,5,2,4,3,'2011-07-12 15:19:32','2011-07-19 11:47:03','2011-07-12',100,1,null,180,1,2,0,'2011-07-19 11:47:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (181,1,2,'Référence : Transfert site - champs inconnu','CP2000 Référence : Dans CPXG044, je veux faire un transfert de la table des sites et j''ai l''erreur :
> 0111 - le champ AGERGN.CPXAGE.CPX est inconnu',null,42,5,5,4,2,4,6,'2011-07-12 16:47:13','2011-07-19 14:27:33',null,100,2,null,181,1,2,0,'2011-07-19 14:27:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (182,3,2,'Suivi item pour la nouvelle agence Estellon','* Mise en place des Experts/Secrétaire par D. Gouveia
* Contrat Fax par Estellon
* PC Scan par Cédric
* Documents type par Candice et Dominique
','2011-08-26',null,5,4,4,null,4,2,'2011-07-12 17:08:38','2011-09-09 15:05:21','2011-08-15',0,null,122,122,16,17,0,'2011-09-09 15:05:21');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (183,2,5,'Gestion des types de police','A faire, gestion CPXTPO :

* créer un type de police
* visualiser un type de police
* modifier un type de police
* supprimer un type de police

Spécification complète sur le serveur développement :

c:\Progi\Projets\CPWeb\Détaillé\Siège\Paramétrage\Paramétrage-001-Gestion-type-police.doc',null,38,5,null,4,null,4,8,'2011-07-18 16:46:17','2013-04-17 14:56:57',null,0,40,null,183,1,2,0,'2013-04-17 14:56:57');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (184,1,2,'relation agence/département','utiliser AGEANA n''est pas correct. il faut réellement utiliser la relation AGEDEP.CPXAGE = DEPCOD.CPXDEP. Sinon la relation est HS sur l''agence centrale (0781) et l''agence de PAU (0641) 

Donc à faire :
# valoriser le champs AGEDEP.CPXAGE (SQL)
# remplacer les utilisations de ageana par agedep','2011-07-22',null,5,5,4,2,4,5,'2011-07-19 11:56:28','2011-07-29 09:37:16','2011-07-20',100,16,null,184,1,2,0,'2011-07-25 12:48:54');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (185,1,2,'Transfert paramétrage - cpxval - date de derniere modification HS','Dans le transfert de la table CPXVAL vers les agences, le champs date de derniere modification de la base référence est systématiquement mis à jour (à tord) avec la date du transfert.

En parallèle, chercher s''il n''y a pas un problème sur la table des scanners (CPXSCA) qui semble anormalement longue à être transférée.

','2011-07-21',42,5,4,4,null,4,2,'2011-07-21 10:57:40','2011-07-21 13:47:54','2011-07-21',100,4,null,185,1,2,0,'2011-07-21 13:47:54');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (186,3,2,'Transfert agence 1840 - problème sur le forfait','Erreur quotidienne :

|_.Date du contrôle|_.Tech|_.Code Site|_.Type de transfert|_.Réussi ?|_.Date de début|_.Date de fin|_.Erreur|
|22/07/2011|EF|1840|Batch param|N|22/07/11 00:46:14|22/07/11 00:46:25|Erreur entité regroupement forfiat table=CPXFOA identifiant=FEKWI00001|

Trouver pourquoi','2011-07-29',42,5,4,4,null,4,2,'2011-07-22 10:04:47','2011-07-26 09:37:18','2011-07-25',100,4,null,186,1,2,0,'2011-07-26 09:37:18');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (187,3,7,'Tests mails automatiques','Attention à penser à rediriger les mails sortants vers une adresse interne (exemple cp2000@groupeprunay-si.fr)

* Tester les 88 cas d''envoi du mail (voir #37)
* Tester l''import d''une journée agence',null,null,5,3,4,11,4,2,'2011-07-25 14:57:07','2012-01-05 09:41:05',null,0,8,null,187,1,2,0,'2012-01-05 09:41:05');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (188,2,1,'Ajouter le site par defaut à la création d''un utilisateur','Lors de la création de l''utilisateur, il faut affecter un site par défaut en relation avec le nom de domaine de la messagerie.

Exemple :
* toto@eurisk.fr --> rejoint le site "Eurisk"
* titi@eurexo.fr --> rejoint le site "Eurexo"
* tata@groupeprunay-si.fr --> rejoint le site "CPCOM"

Définir la liste des noms de domaines "éligible par defaut" pour chaque site créé dans Alfresco',null,null,1,3,4,16,4,1,'2011-07-27 11:23:50','2011-07-29 11:38:11','2011-07-27',10,40,null,188,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (189,2,2,'Support - Changement Type de Police','h2. Objectif

* Avoir une vision claire de la structure Police/Opération/Dossier
** Afficher les informations permettant de repérer une mauvaise structure
*** Type de la police en relation avec les type missions de dossier
*** Nom des opérations en relation avec les nom des affaire des dossiers
* Permettre de corriger si besoin

h2. Critère de recherche

* numéro de police (PASNUM) à saisir

h2. Fonctions 

# permettre le changement de l''information TPOCOD (dropdownlist)
# afficher l''arborescence de la police / opérations / dossiers

h2. Règles de gestion

* sur saisie du numéro de police --> lancer automatiquement la recherche
* gérer le cas ou le numéro de police n''est pas unique : il faut avoir un compteur d''occurence courante / occurences totales répondant à la recherche (voir 1/2 sur le mockup + scroll barre sur les polices)
* Par défaut le controle de cohérence est activé
* Si controle de cohérence, liste des types police possible = tous ceux qui ont le même TPOICONST que la valeur TPOCOD actuelle de la police.
* Si pas de contrôle de cohérence, afficher la liste complète TPOCOD de la table CPXTPO

h2. Maquette ecran


!support-typepolice.png!','2011-08-12',24,5,5,4,2,4,6,'2011-07-27 16:16:33','2011-08-23 11:28:36','2011-07-28',100,24,null,189,1,2,0,'2011-08-23 11:28:36');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (190,2,2,'Support - aide documents du dossier','h2. Objectif 

Permettre au support d''avoir une vue rapide du coté "technique" des documents pour dépanner efficacement l''utilisateur

h2. Souhait

Pour un dossier donné, faire la liste

# séparée
# listes des documents tronc communs
# listes des documents destinataires (pour les documents internes)
# savoir s''ils sont réellement présents physiquement sur le serveur
# Donner la possibilité d''ouvrir directement le document
# créer le lien vers Hermès (voir CPXG021 pour l''exemple)

h2. Maquette écran

Ecran en lecture seule pour cette première version.


!support-documents.png!','2011-08-12',24,5,5,4,2,4,6,'2011-07-27 16:52:13','2011-08-30 14:07:46','2011-07-28',100,24,null,190,1,2,0,'2011-08-30 14:07:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (191,2,5,'Migration Symphony RC4 --> Final 2.0','Symfony est passé en version final 2.0

http://symfony.com/blog/symfony-2-0','2011-08-16',null,5,8,4,null,4,3,'2011-07-28 12:39:20','2011-08-16 10:56:37','2011-08-16',100,4,null,191,1,2,0,'2011-08-16 10:47:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (192,2,5,'Support - Changement Type de Police','h2. Objectif

* Avoir une vision claire de la structure Police/Opération/Dossier
** Afficher les informations permettant de repérer une mauvaise structure
*** Type de la police en relation avec les type missions de dossier
*** Nom des opérations en relation avec les nom des affaire des dossiers
* Permettre de corriger si besoin

h2. Critère de recherche

* numéro de police (PASNUM) à saisir

h2. Fonctions 

# permettre le changement de l''information TPOCOD (dropdownlist)
# afficher l''arborescence de la police / opérations / dossiers

h2. Règles de gestion

* sur saisie du numéro de police --> lancer automatiquement la recherche
* gérer le cas ou le numéro de police n''est pas unique : il faut avoir un compteur d''occurence courante / occurences totales répondant à la recherche (voir 1/2 sur le mockup + scroll barre sur les polices)
* Par défaut le controle de cohérence est activé
* Si controle de cohérence, liste des types police possible = tous ceux qui ont le même TPOICONST que la valeur TPOCOD actuelle de la police.
* Si pas de contrôle de cohérence, afficher la liste complète TPOCOD de la table CPXTPO

h2. Maquette ecran


!support-typepolice.png!','2011-08-12',12,5,5,3,null,4,8,'2011-07-29 10:18:14','2012-09-10 09:29:50','2011-07-28',20,24,null,192,1,2,0,'2012-09-10 09:29:50');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (193,1,2,'Envoi document : tri document interne par date de validation ET date de création','Lorsque nous avons plusieurs documents validés le même jour, ils sont rangés dans le mauvais sens car le plus vieux document est en haut...

h2. a faire 

Ajouter un deuxième critère de tri (docid) après le tri sur la date de validation','2011-08-23',27,5,4,4,2,4,2,'2011-08-23 14:19:46','2011-08-30 14:08:12','2011-08-23',100,2,null,193,1,2,0,'2011-08-30 14:08:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (194,3,2,'Etude statistique de la facturation pour Generali','Bonjour,

Ci-dessous les paramètres de l’extraction à faire sur les NH :

h2. Périmètre

|Code Client|Generali, tous les codes missions|
|Date de mission|depuis 01/01/2008 jusqu’au 30/06/2011|
|Types de missions|Toutes missions/ agences/experts|
|Type de NH|annulées ou non, dont le (ou les) code facturation appartient à l’un des TR suivants|
||TR016|
||TR092|
||TR127|

On peut certainement utiliser les entités de regroupement ?

h2. Créer une table

* N° Dossier
* Type de mission
* date de la mission
* Expert signataire
* date de la NH
* code facturation appliqué
* montant, état règlement
* date de règlement
* état dossier

Après, avec ça nous ferons notre cuisine ici.
Ca va ?

Si question, m’en parler.
PB
','2011-08-25',null,5,4,4,null,4,7,'2011-08-24 11:37:52','2011-08-24 17:50:53','2011-08-24',100,4,null,194,1,2,0,'2011-08-24 17:50:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (195,2,6,'Hermès - contrôle de la présence des documents','Suite à la remontée nocture CP2000 -> Hermès, il faut contrôler que :
* Pour tous les dossiers du jour
* Pour chaque document, récupérer le chemin
* Tester s''il est présent physiquement sur le serveur
* Si non présent 
** sauver la référence agence/dossier + id/libellé du document dans une liste temporaire

Envoyer un mail au support avec la liste de tous les documents manquants','2011-11-14',34,2,4,4,52,4,6,'2011-08-25 16:04:50','2013-01-16 14:59:35','2011-11-07',0,36,null,195,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (196,2,1,'Authentification "silencieuse" ','h2. Demande

Pouvoir accéder à une ressource dans Pygmalion directement sans passer par l''étape login.

h2. Pistes

Voici quelques pistes par ordre de préférence

* côté Alfresco : Enrichir l''étape de login avec une option "Se souvenir de moi" (cookies?)
* côté Navigateur : Stocker le login/mdp dans le navigateur --> à tester / procédure à expliquer (doit fonctionner avec IE8)
* côté SSO : Voir prérequis-compatibilité Alfresco/SSO. L''installation du SSO est un projet à part entière à étudier...

Si rien n''est réalisable expliquer un temps soit peu pourquoi sur chaque point.

',null,null,1,3,3,null,4,2,'2011-08-25 16:23:39','2011-08-31 14:55:26',null,0,null,null,196,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (197,3,2,'Préparation site formation','Mettre à jour le site de formation pour Dominique

Vérifier:

* s''il reste des fiches sycodes
* si les dossiers existants gardent en mémoire la validation d''une fiche C/S','2011-08-29',15,5,4,4,null,4,1,'2011-08-25 16:40:34','2011-08-29 12:10:07','2011-08-29',100,3,null,197,1,2,0,'2011-08-29 12:10:07');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (198,2,2,'Filtrage procédure/document gestion délégué','En gestion déléguée ne pas afficher la Procédure EXPERTISE GESTION CLASSIQUE ',null,43,5,4,4,24,6,10,'2011-08-26 15:20:49','2011-10-14 10:42:19','2011-09-12',100,24,null,198,1,2,0,'2011-10-05 16:24:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (199,1,2,'Condition du cout de construction absent pour accès fiche Crac/Sycodes','il est impossible de vérifier l''éligibilité à la gestion Crac/Sycodes si aucun cout de construction n''est saisi pour dans l''opération.

h2. Problème 

l''utilisateur n''est pas averti du manque d''info.
Le test Crac/Sycodes est faux.

h2. A faire 

Si cout de constuction est vide alors message d''erreur à l''utilisateur.',null,21,2,5,4,null,4,6,'2011-08-26 16:44:54','2012-10-23 12:00:17',null,30,24,null,199,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (200,1,1,'Résultat de recherche: ne plus afficher les documents dans Alfresco','Télécharger le doc au lieu de l''afficher dans le visualiseur Alfresco (et au passage, perdre tout l''interêt d''Alfresco !!)',null,null,1,3,3,null,3,3,'2011-08-30 09:22:38','2011-08-31 14:56:02',null,0,8,null,200,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (201,1,1,'Afficher plus d''éléments dans les listes de fichiers','Afficher 25 éléments dans la liste des fichiers. Attention aux documents qui contiennent une description et au vertical-terrorism',null,null,6,3,4,16,3,3,'2011-08-30 09:23:50','2011-09-08 09:19:23',null,100,16,null,201,1,2,0,'2011-09-08 09:19:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (202,1,1,'Ordre de tri des documents','Voir comment Alfresco tri les documents (par date de création, titre du document, nom du fichier ... ?)
et arranger le tri par ... ce qui est affiché à l''écran (soit le titre du document soit le nom du fichier si pas de titre)',null,null,3,3,4,16,3,5,'2011-08-30 09:24:43','2011-09-19 10:38:41',null,100,null,null,202,1,2,0,'2011-09-19 10:38:41');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (203,1,1,'Auto inscription au site par défaut','En fonction du domaine de messagerie',null,null,5,null,4,null,3,3,'2011-08-30 09:25:16','2011-08-31 14:55:04',null,0,null,null,203,1,2,0,'2011-08-31 14:53:05');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (204,1,1,'dashlet rss: supprimer les liens (qui sont propre aux rss !!)','Un petit coup de display:none sur les liens présents dans les flux rss (Envoyé à ...)',null,null,3,3,4,16,3,3,'2011-08-30 09:25:54','2011-09-05 10:30:04',null,100,1,null,204,1,2,0,'2011-09-05 10:30:04');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (205,1,1,'Droits','plop',null,null,6,3,4,null,3,3,'2011-08-30 09:26:06','2011-08-30 10:16:54',null,0,null,null,205,1,2,0,'2011-08-30 10:16:21');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (206,1,1,'Réimport des données','rsync + import + ....',null,null,1,null,4,16,3,2,'2011-08-30 09:27:04','2013-01-22 11:26:51',null,0,null,null,206,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (207,1,1,'Rédaction divers docs','maj des docs exploits/admin/utilisateurs',null,null,8,3,4,16,3,3,'2011-08-30 09:27:36','2011-08-30 10:07:44',null,0,null,null,207,1,2,0,'2011-08-30 10:06:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (208,3,6,'Gsicass - récupérer l''OM du Gan + répondre','Bonjour,

Dans le cadre du projet " Portail des échanges avec les experts", la société GAN ASSURANCES a créé en environnement de Validation une mission qui vous est destinée. Afin d''effectuer des contrôles fonctionnels et  de vérifier la cohérence des informations transmises, pouvez vous récupérer cette mission et en retour transmettre un rapport en relation avec celle-ci.
',null,40,5,11,4,null,4,6,'2011-08-30 10:22:35','2012-04-24 10:18:22','2012-03-01',100,null,null,208,1,2,0,'2012-04-24 10:18:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (209,2,6,'FLAG - Dispositif alerte (FLAG)','Vous trouverez ci-joint en PJ une liste de codes SYCODES & CRAC remise ce jour par FLAG et destinée à définir le contour du Processus Alerte .
Pour tous les dossiers où un Rapport et une Fiche on été faite, il faut si l’un des codes figure dans la liste, envoyer une copie à FLAG.

Le processus imaginé est simple :
Tous les deux mois, on collecte les rapports correspondant à la définition ci-dessus sur un CD qu’on envoie à Flag.
Estimation +/- 1000 rapports à chaque envoi.

Reste à définir comment le mettre en œuvre ?
Je pense que le plus simple serait d’utiliser un paramètre dans les diverses tables pour indiquer si le code doit faire l’objet d’un envoi (ou non).
A la suite de quoi une simple requete externe permet de collecter les documents …
','2012-06-05',61,5,11,4,95,4,29,'2011-08-30 10:59:54','2013-02-13 11:45:01','2012-02-13',100,null,null,209,1,2,0,'2012-06-04 09:47:55');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (210,2,2,'Alerte rappel envoi de courrier','Pour les clients "non zero papier" , il faut rappeler à l''utilisateur de poster son courrier.

Commencer par surveiller 
* les convocations
* les rapports

L’alerte n’apparaîtrait que pour les codes clients où il n’y a pas de convention « zéro papier » et ou aucun envoi n’a été fait (fax, mail, epost, impression ...)',null,27,1,null,4,null,4,3,'2011-08-30 11:05:01','2012-10-23 12:01:03',null,0,null,null,210,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (211,2,2,'Attacher un document à une operation ou une police','On devrait pouvoir

# attacher un document externe à une opération, ceux-ci seraient disponibles dans tous les dossiers de l''opération.
** consultation dans l''écran opération
** consultation dans l''écran document externe
# attacher un document à une police, ceux-ci seraient disponibles dans tous les dossiers de la police.
** consultation dans l''écran police, 
** consultation à la page "Assurance RCD" des entreprises)
** consultation dans l''écran document externe

Origine demande B.Giraud (Toulouse)
',null,5,1,null,5,null,4,9,'2011-08-30 11:11:34','2012-10-03 09:43:02',null,0,120,null,211,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (212,1,2,'Mail en provenance d''Allianz impossible à intégrer','Les agences rencontrent régulièrement un problème sur l''import de mail dans CP2000 en provenance d''Allianz

# Trouver
## si c''est la macro outlook qui exporte mal
## si c''est CP2000 qui importe mal
# Proposer une correction (CP2000 de préférence)

Voir jeux de tests (dans ta messagerie).
A utiliser à partir de la messagerie recette1-cp2000@groupepruna-si.fr (mdp Rep6kop)
','2011-09-05',6,5,5,4,2,4,8,'2011-08-30 17:34:14','2011-09-05 18:16:00','2011-09-01',100,24,null,212,1,2,0,'2011-09-05 18:16:00');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (213,1,2,'Intégration d''un document : message d''erreur erroné','CPXL250 - LP_SELECT_SOURCE
<pre>
......message/error $text(E_DOCAGE)
</pre>

C''est faux, ca veux rien dire... ici c''est un manque de paramétrage dans CPXPGA --> Trouver un message dans la librairie CPX qui ressemble à "Paramétrage manquant"',null,10,5,5,4,24,4,5,'2011-09-02 09:26:27','2011-09-12 17:00:46','2011-09-05',100,null,null,213,1,2,0,'2011-09-12 17:00:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (214,3,2,'Nettoyage anciennes données (NH) au siège','Des notes d''honoraires antérieur à CP2000 (2004) sur des références dossiers CPXpert sont présentes au siège.
Ces références sont en conflit sur certains dossiers CP2000.
Ainsi une note d''honoraire créée en 2000 peut se retrouver sur un nouveau dossier datant de 2010... C''est faux.

Il faut nettoyer les NH qui ne sont pas dans leur dossiers réels : comparer les NH douteuses au siège avec le contenu des agences...

Voir s''il faut nettoyer autre chose que les NH (intervenants? documents?)',null,null,5,4,4,null,4,5,'2011-09-06 14:07:55','2011-10-12 10:48:43',null,100,null,null,214,1,2,0,'2011-09-12 15:46:11');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (215,3,2,'Analyse pathologie des sinistres à partir des Fiches Sycodes','Il apparaît comme un prolongement naturel de l’outil qu’il faudrait pouvoir faire le même type de recherche sur les fiches Sycodès qui existent dans les dossiers et permettent de qualifier les dommages de façon organisée On pourrait utiliser exactement la même méthode :
# Stocker sur un répertoire dédié l’ensemble des Fiches Sycodes
# Les indexer
# Ressortir des listes de dossiers avec divers critères…
attention : on cherchera à trouver des fiches contenant tel ou tel code particulier…
','2011-09-23',null,1,4,4,null,4,2,'2011-09-07 11:56:59','2011-09-07 11:58:43','2011-09-23',10,8,null,215,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (216,2,2,'Onglet Avenant 1','Il faut ajouter un onglet dans la gestion du dossier dans le même genre que l''onglet judiciaire : Sur activation de la case à cocher l''onglet "Avenant n° 1" doit être disponible.

!avenant1.png!

h2. Contenu de l''onglet

_Rediscuter avec Dominique Santoni du contenu de l''onglet avenant 1_

Nom de l’intervenant responsable --> lien direct avec le répertoire où on a coché l’entreprise responsable ( ?)

* Nom de son Assureur -> lien avec l’assureur associé au chantier (répertoire dossier ??)
* Gestionnaire : saisie manuelle par la secrétaire – Tél :   
* Réf. Assureur -> lien avec la saisie Réf. Assureur dans le répertoire ( ??? )
* Cabine d’expertise -> R création en passant par répertoire site
* Nom de l’Expert -> saisie manuelle				Réf. Expert : saisie manuelle

Possibilité d’avoir plusieurs entreprises responsables (même beaucoup…) 

h2. Document Type à générer

Tous ces renseignements seront paramétrés dans un DT.
Le DT sera valorisé automatique par CP2000.

Exemple du document : attachment:6000000046269.rtf',null,null,1,null,5,null,4,2,'2011-09-08 11:28:51','2011-10-13 10:25:02',null,0,null,null,216,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (217,2,2,'Envoyer un mail en différé','Certains traitements dans CP2000 déclenchent des envois de mail. 
(exemple un AR ou un passage en gestion délégué chez Allianz voir #124 et #125).
Les mails ne seraient pas toujours à envoyer immédiatement.
Il faut pouvoir envoyer un mail en différé.

h2. A faire

* descendre les infos et le contenu du mail dans une arborescence sur le serveur
* créer un batch qui va traiter les mails en attentes de l''arborescence
 
','2011-12-08',6,5,5,6,29,4,19,'2011-09-08 11:49:27','2012-01-16 18:17:53','2011-12-01',80,40,null,217,1,2,0,'2012-01-16 18:17:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (218,2,2,'signature mails CP2000','demande fréquente des agences :

insertion d''une signature à la rédaction du mail adressé via CP2000 par exemple : Celle de l''expéditeur (Expert ou assistante) + EURISK/nom de l''agence + n°tél 

h2. A faire

h3. gestion des signatures

Prévoir un écran de gestion de signature par Expert/Secrétaire (lecture création modification suppression) accessible depuis le menu administration d''une agence.

Une personne peut ne pas avoir de signature
Une personne peut avoir plusieurs signatures. 
Un utilisateur doit avoir une signature par défaut.

h3. utilisation de la signature

Dans l''écran d''envoi mail, sur sélection de l''utilisateur qui envoi le message, rapatrier la signature par défaut dans le corps du mail.

Laisser le choix par une liste déroulante de sélectionner une autre signature

Sur changement de signature, remplacer l''ancienne signature dans le corps du texte par la nouvelle choisie

',null,6,1,null,5,null,4,1,'2011-09-08 11:58:41','2011-10-13 10:24:31',null,0,null,null,218,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (219,2,2,'FR099 - liste des assureurs en recours','<pre>
> De : Eurisk Siege/SANTONI Dominique 
> Envoyé : mardi 23 novembre 2010 09:55
> À : Eurisk Siège/CHARTIER Sylvie; Eurisk Siege/BOYER Paul; CPCOM/BOUREZ Vincent; Eurisk Siege/COLOMBEL Candice
> Cc : Eurisk Siege/DUMONT Sylvie
> Objet : RE: FR099 : Liste des assureurs en recours
> 
> Oui en effet, ça éviterait la mise à jour du répertoire et surtout le risque qu''elle ne soit pas faite par l''agence. 
> Mais je vous rassure, c''est un fichier commun qui s''associe au répertoire des dossiers, donc  une seule mise à jour est nécessaire (et non une saisie à chaque dossier).
> A voir avec Vincent
> 

>> De : Eurisk Siège/CHARTIER Sylvie 
>> Envoyé : mardi 23 novembre 2010 09:32
>> À : Eurisk Siege/BOYER Paul; Eurisk Siege/SANTONI Dominique; CPCOM/BOUREZ Vincent; Eurisk Siege/COLOMBEL Candice
>> Cc : Eurisk Siege/DUMONT Sylvie
>> Objet : RE: FR099 : Liste des assureurs en recours
>> 
>> Ce serait super
>> Reste à vérifier la faisabilité…
>> 

>>> De : Eurisk Siege/BOYER Paul 
>>> Envoyé : mardi 23 novembre 2010 09:25
>>> À : Eurisk Siege/SANTONI Dominique; CPCOM/BOUREZ Vincent; Eurisk Siège/CHARTIER Sylvie; Eurisk Siege/COLOMBEL Candice
>>> Cc : Eurisk Siege/DUMONT Sylvie
>>> Objet : FR099 : Liste des assureurs en recours
>>> 
>>> Bonjour,
>>> 
>>> Je viens de valider la révision 6 de ce FR099 qui présente une liste longue comme le bras (150 lignes) de tous les sites des assureurs lorsqu’ils sont mis en cause pour les recours…
>>> 
>>> Vu comme ça, il me semblerait logique que cette liste soit un des paramétrage de CP2000 plutôt que sur un document FR.
>>> Cela éviterait sans doute des saisies à faire chaque fois qu’on doit envoyer un courrier à l’un de ces assureurs ?
>>> 
>>> Comme la liste existe sous forme de table , il est facile de l’intégrer dans CP2000 (Vincent) et d’avoir un écran administrateur pour y faire les mises à jour (Candice).
>>> Reste ensuite à définir à quel endroit utiliser cette liste ? Dominique a surement des idées ?
>>> Et rédiger ou ajuster une instruction ou SM pour finaliser le tout : Sylvie.
>>> 
>>> Si cette idée ne vous paraît pas trop saugrenue, ça ferait encore un papier en moins à gérer et la certitude que tout le monde travaille avec des données à jour.
>>> 
>>> On en parle à l’occasion ?
>>> 
>>> PB
</pre>

h2. Analyse

Le souhait est de mettre à disposition à partir du siège un ensemble d''adresses communes dans le répertoire des agences

Cf CPXL076 (a revoir car le filtre actif/inactif/tous ne fonctionne pas)',null,8,5,5,6,46,4,80,'2011-09-08 12:02:36','2012-09-09 09:57:05',null,100,40,null,219,1,2,0,'2012-09-07 13:55:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (220,2,2,'Surveillance des dossiers judiciaires','Voir demande initiale dans le document : attachment:Surveillance_des_dossiers_judiciaires.pdf

h1. A faire

h2. Date accédits

* En base, ajouter une table "Reunion" (CPXREU)
** Prévoir les champs REUID (autoincrement), AGECOD, DOSNUM, REUCOD (Char 5), REUDT (DateTime), REUCOM (Char 50)
** Dans CPXLIS/CPXVAL prévoir la liste "CDREU" ayant la valeur "ACCED=Accédit"
* Dans l''écran, ajouter un champs Date + un champs heure
** stocker le résultat dans la table réunion avec concatenation de la date heure dans REUDT et "ACCED" dans REUCOD

!surveillance_dossiers_judiciaires1.png!

h2. Confirmer la présence du cabinet d''avocat

Ajouter un radio bouton, libellé : "Saisine d''un Avocat par le Client"

!surveillance_dossiers_judiciaires2.png!

Si :
* oui : 2 options (radio bouton) "Pour l''assureur" / "Pour l''assuré" + affichage actuel du choix du cabinet + référence
* non : aucun affichage

!surveillance_dossiers_judiciaires2bis.png!


h2. avocat de l''assuré

Notion à supprimer de l''écran 
(vu avec Sylvie et Anne-Marie le 03/10/2011)

!surveillance_dossiers_judiciaires3.png!

h2. Contrôle documents

!surveillance_dossiers_judiciaires4.png!

Bloquer la sortie du dossier (Quitter, enregistrer et quitter, remise à blanc...) si aucun DT suivant non validé
* DT255 - CO255 - ES255
* DT232 - CO232 (pas de ES)

OU

Client majeur = Generali et DT854 non envoyé par mail

h2. A suivre 

!surveillance_dossiers_judiciaires5.png!


','2012-01-06',44,2,5,5,null,4,13,'2011-09-08 15:27:25','2012-01-16 15:33:27','2011-12-19',0,null,null,220,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (221,2,2,'Copier une enveloppe','Dominique, le 05-01-2011

Il faudrait avoir la possibilité de recopier le contenu d''une enveloppe et le copier sur une nouvelle enveloppe.
',null,27,1,null,5,null,4,1,'2011-09-09 14:22:06','2011-10-13 10:20:02',null,0,null,null,221,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (222,2,2,'Filtrer l''intégration des PDF protégés par mot de passe','Les agences intègrent des documents en provenance de tiers.
Certains sont protégés par mot de passe et ils sont inutilisables pour les envois Epost/Mail/Fax Darva etc...

h2. A faire 

Détecter et interdire un PDF à l''intégration qui est protégé par mot de passe.
Attention 2 types d''intégrations
- intégration manuelle (CPXL250)
- archivage automatique des mails (CPXS050)','2011-11-04',5,5,5,5,24,4,9,'2011-09-09 14:55:14','2011-10-06 16:32:47','2011-11-01',100,32,null,222,1,2,0,'2011-10-06 16:32:47');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (223,1,2,'Problème de mise à jour du numéro de police','Sur l''écran CPXG021/CPXT001, on clique sur le bouton "Police".

Une fois dans l''écran CPXG008, on modifie le numéro de police en haut à gauche puis on clique sur "Enregistrer et quitter".

De retour sur CPXG021/CPXT001, dans le bandeau en haut, le numéro de police ne s''est pas mis à jour alors que l''on vient de modifier le numéro de police.

Il faut recharger l''écran pour voir apparaitre le nouveau numéro de police.',null,19,5,5,4,24,5,3,'2011-09-13 09:34:49','2011-09-14 10:54:09','2011-09-13',100,null,null,223,1,2,0,'2011-09-14 10:54:09');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (224,1,2,'Recherche par tiers très lente','Dans le menu consultation > rechercher un dossier par tiers (CPXC001), la recherche par tiers est très longue (15 minutes sur le 74 ou 83)

h2. A faire 

* Etudier le retrieve
* Trouver une correction pour des temps d''accès plus courts (index?)','2011-09-16',45,5,4,4,24,4,6,'2011-09-13 14:11:45','2011-09-13 17:04:33','2011-09-13',100,8,null,224,1,2,0,'2011-09-13 17:04:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (225,3,2,'Livraison 1.9.1','Toutes les infos concernant la livraison 1.9.1','2011-10-23',null,5,4,4,24,4,10,'2011-09-13 17:02:42','2011-10-24 14:15:11','2011-10-17',100,null,null,225,1,2,0,'2011-10-21 13:39:08');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (226,2,2,'Gestion des Habilitations des experts','h2. Demande

Gérer les habilitations au siège.
Consulter les habilitations en agence

h2. A faire

reproduire le comportement de la feuille excel présente dans Pygmalion',null,46,1,null,5,null,4,3,'2011-09-15 14:34:47','2012-10-03 10:11:50',null,0,120,null,226,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (227,1,2,'Lenteur sur validation document CPXG014','Problème = 20 secondes pour valider un AR !',null,5,5,4,4,null,4,21,'2011-09-15 16:44:23','2011-09-26 18:14:51','2011-09-15',100,null,null,227,1,2,0,'2011-09-26 17:30:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (228,1,1,'tester accès depuis cp2000','',null,null,1,null,4,null,3,1,'2011-09-19 10:39:37','2011-09-19 11:24:41','2011-09-19',50,null,null,228,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (230,1,2,'Support Darva - Construction du lien transparent','La construction du lien n''est pas la même dans l''écran du support (CPXG902) que dans l''écran de nos agences (CPXG021)

L''écran du support est à mettre à niveau (CPXG902)','2011-09-23',24,5,5,4,24,4,2,'2011-09-20 14:48:54','2011-09-30 14:09:27','2011-09-20',100,4,null,230,1,2,0,'2011-09-30 14:09:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (231,2,4,'Informations documents externes complets','Dans CP2000, l’indexation des documents externes comporte plusieurs champs descriptifs : origine, expéditeur... Il me semble que ce qu’on voit sur Hermès n’est que partiel : libellé tronqué et champ complément manquant...

A l’époque, on nous a expliqué qu’il y avait une impossibilité technique... j’ai oublié laquelle (le socle ?).

Par contre ce serait tout de même sympathique d’arriver à présenter ces infos au complet !
',null,39,1,null,4,23,4,1,'2011-09-20 16:28:23','2011-09-20 16:30:08',null,0,null,null,231,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (232,2,2,'Ajouter le choix du mail du contact du client qui paye pour la rédaction de mail','Lorsqu''on écrit un mail, si le client qui paye est différent du client qui missionne, son email n''est pas disponible dans l''écran CPXL012.

Il faut ajouter l''affichage du mail pour le code client 2.

(Service CPXS042)',null,6,5,4,4,24,4,2,'2011-09-22 16:50:13','2011-10-03 09:39:44','2011-09-26',100,4,null,232,1,2,0,'2011-09-26 15:23:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (233,2,2,'Liste des assureurs : ne plus afficher les Inactifs au retour de la selection','Ecran CPXL076 : ne plus afficher les Inactifs qui apparaissent après la consultation de l''adresse d''un assureur sur l''écran CPXG074 

demande Villescrenes/Marie OLAK','2011-09-30',19,5,4,4,24,6,6,'2011-09-28 10:28:06','2011-10-05 16:25:42','2011-09-28',100,2,null,233,1,2,0,'2011-10-05 16:25:35');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (234,2,2,'Ecran CPXG008 : Ajouter Effectif déclaré ','Ecran CPXG008 : Ajouter Effectif déclaré au niveau de la case "Souscription" + paramétrage dans les rapports - Supprimer Libellé Indice et valeur d''origine si besoin (demande Experts/Rennes 14/09/2011)',null,19,1,null,4,null,6,4,'2011-09-28 11:50:35','2011-10-11 15:54:33','2011-09-28',0,16,null,234,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (235,2,2,'Si pas de date de résiliation, afficher "En cours" dans les rapports','Dans la police, écran CPXG008 :

Si pas de date de résiliation, insertion automatique du libellé dans le rapport : "En cours"

Demande Experts Rennes 13/09/2011 ','2011-10-21',5,5,5,4,26,6,12,'2011-09-28 11:54:39','2011-11-10 09:22:01','2011-10-19',100,8,null,235,1,2,0,'2011-11-10 09:22:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (236,2,2,'Classement des dates réception ','Classement des dates réception par ordre numérique dans l''écran CPXG009

(demande Exp. Rennes 13/09/2011)',null,47,1,null,4,null,6,1,'2011-09-28 11:55:40','2011-09-28 14:09:43',null,0,8,null,236,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (237,2,2,'Si PV non  vu, afficher "Non connues" dans les réserves','si PV vu non coché s''affiche dans le rapport Réserve : Non .

Comme il est impossible de supprimer dans le rapport le mot Réserve sans une intervention manuelle de l''assistante, il faudrait mettre "sans objet" en insertion auto lorsque la case PV vu n''est pas cochée','2011-10-21',5,5,5,4,26,6,11,'2011-09-28 11:58:24','2011-11-22 13:52:20','2011-10-19',100,8,null,237,1,2,0,'2011-11-09 14:43:04');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (238,2,2,'Agrandissement de la zone commentaire de la date de récpetion','Zone commentaire: autoriser la saisie de 60 caractères au lieu de 40 actuellement


(demande Rennes 13/09/2011)','2011-09-30',47,5,4,4,24,6,5,'2011-09-28 12:00:51','2011-10-05 16:27:44','2011-09-28',100,8,null,238,1,2,0,'2011-10-05 16:27:44');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (239,2,2,'Masquer un intervenant non concerné','On ne peut pas supprimer de cette liste un intervenant qui a été associé à un dossier et convoqué. il n''est pas intervenu dans cette opération de construction, il ne doit plus apparaître dans cette liste et donc dans la préparation convocation. Il faudrait pouvoir le le masquer case à cocher (comme Expiré dans les Pos-it) ou autre moyen. (demande Rennes 13/09/2011)
',null,35,1,null,4,null,6,2,'2011-09-28 12:04:13','2011-09-28 14:19:16',null,0,24,null,239,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (240,1,2,'Gestion répertoire intervenants de l''opération (Ecran CPXG005) : masquer un intervenant non concerné','On ne peut pas supprimer de cette liste un intervenant qui a été associé à un dossier et convoqué. il n''est pas intervenu dans cette opération de construction, il ne doit plus apparaître dans cette liste et donc dans la préparation convocation. Il faudrait pouvoir le le masquer case à cocher (comme Expiré dans les Pos-it) ou autre moyen. (demande Rennes 13/09/2011)
Déifférents écran où il apparaît à ajouter',null,null,5,null,4,null,6,1,'2011-09-28 12:19:56','2011-09-28 14:20:57','2011-09-28',0,null,null,240,1,2,0,'2011-09-28 14:20:57');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (241,2,3,'message erreur si numero agence erroné','il faut qu''un message d''erreur s''affiche lorsque le n° de l''agence n''est pas correctement renseignée.

',null,null,1,null,4,3,6,1,'2011-09-29 14:26:04','2011-09-29 14:27:01',null,0,null,null,241,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (242,3,2,'Acces Darva Rennes/Mayenne','Il faut dupliquer les accès Darva de Rennes pour le site de la Mayenne (0530)

--> Script SQL CPXLGN + update compteur CPXCPT

Prendre exemple sur #172','2011-09-30',33,5,5,6,null,4,4,'2011-09-29 17:56:41','2011-09-30 09:56:58','2011-09-30',100,2,null,242,1,2,0,'2011-09-30 08:10:36');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (243,2,2,'Ajout de la fonction de responsable d''agence','<pre>
Je reviens vers toi pour mon problème de code expert sur P2000 dont nous avions parlé au téléphone.
Je t''en rappelle les grrandes lignes.
Depuis le 01/10/11 je suis enfin déchargé de la responsabilité de l''agence de Cagnes et peux donc me consacrer à l''expertise.
Je reste responsable des agences de Marseille et Montpellier.
Ainsi toutes les nouvelles missions  de ces agences sont enregistrées sous mon code expert 1034, soit par l''agence soit par la PFT.
Ensuite je réattribue les missions aux experts selon la procédure qualité.
Le code expert étant unique cela pose des problème de gestion.
Pour être sur que j''ai bien réattribué les dossiers (un oubli est toujours possible pour les secrétaires ou moi-même) je dois examiner toutes les missions avec le code 1034 sachant que ce code fonctionnel  correspond également à mon code expert opérationnel.
Pour éviter de graves erreurs (comme le non traitement d''un dossier), j''ai demandé à Dominique GOUVEIA de créer un code JP CASONI fonctionnel, différent de mon code opérationnel.
Elle a refusé de le faire en raison des problèmes que cela engendrerait au niveau comptable.
</pre>

h2. A faire

# Dans CPXVAL pour le LISCOD = METIE, ajouter le Responsable agence (code = RAG) --> update SQL
# Sur l''écran de choix d''un expert (CPXL004) prévoir le type de recherche EXP+RAG (P_TYPE = 3) 
# Sur la création de mission, *pour un expert redacteur* autoriser le choix des profils EXP+RAG, accéder à la recherche P_TYPE = 3
# Sur onglet dossier, *pour un expert redacteur*, autoriser le choix des profils EXP+RAG, accéder à la recherche P_TYPE = 3
en plus de EXP (1) ou SEC (2) 
# Transformer le code 1638 de JP Casoni existant sur Marseille ou Montpellier vers un profil RAG (prévoir update SQL)','2011-10-07',48,5,5,4,24,4,10,'2011-10-03 18:01:41','2011-10-13 16:36:53','2011-10-03',100,24,null,243,1,2,0,'2011-10-13 16:36:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (244,2,2,'Siège Ref - CPXL023 - Liste des procédures de gestion','Ecran CPXL023 : il faudrait que la Liste des procédures de gestion s''affiche automatiquement par ordre alphabétique (actuellement on doit cliquer dans Procédure à chaque consultation pour que ça soit dans l''ordre)
','2011-10-05',43,5,4,4,24,6,2,'2011-10-05 15:07:06','2011-10-05 15:22:13','2011-10-05',100,1,null,244,1,2,0,'2011-10-05 15:22:13');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (245,2,2,'Eviter d''envoyer la signature des mails dans les SMS','Les SMS sont actuellement envoyés avec la signature mail ajouté cet été "visitez notre site Eurisk" !!! --> pas bon ','2011-10-12',6,5,4,4,24,4,1,'2011-10-12 16:21:05','2011-10-12 16:23:19','2011-10-12',100,2,null,245,1,2,0,'2011-10-12 16:23:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (246,1,2,'ARCHIVAGE BTE OUTLOOK','La macro doit donner la Possiblité de classer le mail dans CP2000 soit en Interne soit en Externe + afficher un message lors d''une erreur sur le N° de l''agence',null,null,6,null,6,null,6,1,'2011-10-13 09:34:54','2011-10-13 09:37:14','2011-10-13',0,null,null,246,1,2,0,'2011-10-13 09:37:14');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (247,2,2,'Indexation automatique du mail archivé','h2. Demande

le mail archivé doit s''indexer automatiquement (indexation à créer 2.11 Mails) et non plus s''afficher dans la liste des mails entrants avec l''indexation Mail à classer

h2. A faire

CPXS050 - Uniquement pour les mails, les classer directement dans la chemise 2.11 Mails. Ne pas toucher au traitement des PJ qui reste identique.','2011-10-18',6,5,5,6,26,6,9,'2011-10-13 09:41:45','2011-11-10 16:03:05','2011-10-17',100,null,null,247,1,2,0,'2011-11-10 16:03:05');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (248,2,2,'Accès au répertoire Site à partir du répertoire des Pols','Dans la liste des Pols (écran CPXG063) pouvoir faire une recherche dans le répertoire site comme dans l''onglet répertoire d''un dossier Hors Construction avec le bouton "R"',null,8,2,5,5,111,6,11,'2011-10-13 09:46:45','2013-03-05 18:35:04',null,30,24,null,248,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (249,2,2,'Gestionnaire/client ou expert Inactif','Afficher un message dès l''ouverture de la mission pour signaler qu''un doc ne pourra être validé (actuellement le message s''affiche lors de la validation d''un document) ',null,16,5,5,4,59,6,18,'2011-10-13 09:50:18','2012-12-16 08:36:43','2011-10-13',60,null,null,249,1,2,0,'2012-11-23 16:47:21');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (250,2,2,'Mise à jour du Document Modèle de données CP2000','Traçabilité de l''évolution du document',null,null,5,5,4,null,5,2,'2011-10-14 11:32:14','2012-10-03 10:46:41','2011-10-14',0,null,null,250,1,2,0,'2012-10-03 10:46:41');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (251,2,2,'SMART','h2. Demande

Afficher et gérer des fiches d''aides pour secrétaires (GM/SM/TR) 

h2. Fichiers

* attachment:Smart-agence.pdf 
* attachment:Smart-siege.pdf ',null,49,1,null,6,null,4,13,'2011-10-14 16:14:14','2013-07-04 22:27:45',null,13,120,null,251,1,8,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (252,3,2,'Export Fiches Crac/Sycodes','Export à faire + téléchargement sur le site de l''AQC

aide : http://10.31.2.165/dokuwiki/doku.php?id=crac_sycodes','2011-11-10',21,5,4,4,null,4,4,'2011-10-19 15:59:54','2012-07-11 12:13:56','2011-11-09',100,16,null,252,1,2,0,'2012-07-11 12:13:56');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (253,2,2,'Date de réception du mail','h2. Demande

Insérer dans le dossier la date de réception réelle du mail au lieu de la date d''export messagerie/CP2000.

h2. A faire

Dans le service CPXS050 : remplacer l''extraction de <docdtcre> par <docdtmail>','2011-10-28',6,5,5,6,28,4,5,'2011-10-19 16:11:23','2011-10-24 14:04:02','2011-10-28',100,4,null,253,1,2,0,'2011-10-24 14:04:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (254,3,2,'Récupération des dates de réceptions','Rejouer la procédure stockée pour réaffecter les dates de réception aux dommages

','2012-03-21',47,5,5,6,null,4,6,'2011-10-19 17:01:14','2012-10-19 09:22:47','2012-03-19',100,24,null,254,1,2,0,'2012-10-19 09:22:47');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (255,1,2,'message erreur sur date de reception','Le message d''erreur "I_NODATEREC - La date de réception n''est pas renseignée." apparait alors que nous sommes en RCJ, C''est faux.

h2. A faire 

CPXG015 - Vérifier tous les tests du genre
<code> if(MISCOD.CPXDOS != "RCA")</code>
et remplacer par
<code> if(MISINDCST.CPXMIS = "T")</code>

(il doit y en avoir 3 ou 4)
','2011-10-28',50,5,5,4,30,4,3,'2011-10-20 14:16:28','2011-10-25 17:14:10','2011-10-28',100,2,null,255,1,2,0,'2011-10-25 15:07:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (256,2,4,'Ajout champs Chantiers','Ajouter les champs suivants dans les chantiers :

* Les 2 lignes d’adresses du chantiers : 
** adresseRisque
** adresseCompRisque
* La description courte du chantier :
** desc_operation
* Les montants HT et TTC prévisionnels des travaux :
** MontantPrevHT
** MontantPrevTTC
* La date de début des travaux :
** d_deb_travaux

Champs à rajouter en base + à afficher dans l''écran de détail du chantier',null,51,5,null,4,45,4,5,'2011-10-20 14:26:23','2012-10-08 11:33:34',null,100,null,null,256,1,2,0,'2012-10-08 11:33:34');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (257,1,2,'correctif scan + integration pdf + fiche CS','3 problèmes

* Scan fonctionne plus
* Fiche CS impossible à valider
* integration pdf','2011-10-24',null,5,5,4,28,4,3,'2011-10-24 11:40:51','2011-10-24 13:32:33','2011-10-24',0,null,null,257,1,2,0,'2011-10-24 13:32:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (258,2,2,'Historique des dates de RDV','Irina.

Il faut garder l''historique des dates de RDV.
(Et surtout ne pas perdre la date de 1er RDV)

Une date de 1er RDV se trouve de la façon suivante :
* Onglet Document - Date de 1ere convocation
* Onglet Date - Date prochain RDV
* Onglet Date - Date dernier RDV

Utiliser la table CPXREU créée dans #220 - (surveillance des dossiers judiciaires)',null,52,1,5,5,null,4,9,'2011-10-25 09:25:28','2013-07-04 22:27:46',null,0,80,null,258,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (259,2,2,'Filtrage document externe','Si un libellé change dans la nomenclature de classement des documents externes, impossible de retrouver le document avec l''ancien libellé par l''intermédiaire du filtrage dans les documents externes.

h2. A faire

CPXT009 - bouton filtrer : Ne plus filtrer selon le libellé mais selon le DTYREF.','2011-10-28',5,5,5,6,26,6,2,'2011-10-25 11:19:13','2011-11-17 10:09:24','2011-10-28',60,4,null,259,1,2,0,'2011-11-17 10:09:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (260,3,4,'Procédure changement image du menu','Créer la procédure qui permettra à un membre du service info (support ou projet) de modifier le bloc "Version" en bas du menu

Scénario actuel envisagé :
* Préparer une image à copier en production
(définir des règles de nommages, ex: no space !)
* Le responsable de la production (aujourd''hui Pascal) déploie en prod. Préparer une demande de déploiement ''type''.
* Mode d''emploi pour modifier dans le paramétrage Hermes
(Screenshots, info diverses comme nombre de caractère maximum etc...)

','2013-02-01',53,1,25,4,null,4,3,'2011-10-26 14:48:19','2013-02-11 10:57:42','2013-01-31',0,null,null,260,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (261,3,2,'Sondage utilisation envoi de mail','Demande Paul Boyer :

Décompter par mois, par agence, le nombre de mails envoyés directement par CP2000 vs le nombre de mail envoyé par Outlook + intégration CP2000.
(ne pas prendre en compte les mails reçus)

Sortir un tableau Excel
+ graphe évolution mensuelle utilisation toutes agences confondues.

','2011-11-07',null,1,null,4,null,4,1,'2011-11-03 15:55:16','2011-11-10 14:07:40','2011-11-07',0,null,null,261,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (262,2,2,'Ajouter dans un mail le lien vers le dossier Hermès','Demande Paul Boyer, le 03/11 :

Pour Axa, par defaut, tout mail écrit à partir de CP2000 doit contenir le lien vers l''accès direct au dossier Hermès (donc avec login/mdp du client)

h2. A faire

* Voir pour importer les accès utilisateurs Hermès du client AXA et les intégrer dans la table CPXLGN.
* CPXG059 - écrire un mail : Rajouter une case à cocher : "Lien Hermès"
** Si le client du dossier n''a pas d''accès disponible dans CPXLGN, la case à cochée est cachée + décochée
** Si le client du dossier a un accès dans CPXLGN, la case à cocher est disponible + cochée (true par defaut)
* CPXS050 - si "lien hermès" est coché, alors ajouter une phrase "Votre dossier est consultable en ligne sur Hermès : http..." dans le corps du mail.',null,6,1,null,4,null,4,0,'2011-11-03 17:14:32','2011-11-03 17:14:32',null,0,40,null,262,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (263,2,2,'Mail fort enjeu à envoyer au gestionnaire de l''agence','Concernant les alertes pour dossiers à fort enjeu, sera t-il possible que le mail automatique soit également envoyé aux gestionnaires de région?
Merci

Magdaléna LANGE
Gestionnaire EURISK Nord-Ouest

h2. A faire 

Les gestionnaires existent dans la table CPXPRT

<code class="sql">
select prtcod,agecod, prtnom, prtemail from cpxprt where prtmet = ''GEST'' and prtai = ''A'';
</code>

# Il faut enrichir CP2000 de la relation Agence/Gestionnaire :
** ajouter un champs AGEGEST dans CPXAGE.
** adapter l''écran de creation/modification de l''agence de ce nouveau champs.
** ajouter le nouveau champs au transfert du paramétrage
# Dans la création du mail alerte fort enjeux, 
** rajouter dans les destinataires en copie le gestionnaire de l''agence concernée.
# Retirer la Direction Juridique de la liste des destinataires
','2012-05-04',6,5,5,5,43,4,18,'2011-11-04 10:55:38','2012-06-24 17:25:53','2012-05-02',100,24,null,263,1,2,0,'2012-06-22 16:19:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (264,2,2,'Signalement de dossier sensible ou à fort enjeu par l''utilisateur','Créer une case à cocher manuellement pour signaler un dossier sensible et qui se coche automatiquement pour un dossier à fort enjeu.

h2. A faire 

Ajouter un nouveau champs dans CPXDOS :
* DOSSENSIBLE Char(1)

Préparer un update:
* Tous les enregistrements CPXDOS existants étant à fort enjeux (DOSFORENJ à ''T'' ou ''M'') sont sensibles.

Sur l''onglet dossier, à gauche de "judiciaire", rajouter la case à cochée "sensible |_|".

h3. Règle de gestion

* Si dossier à fort enjeux, alors dossier sensible = coché + non modifiable (comportement dossier à fort enjeux à modifier, cf #164)
* Si dossier sensible = coché, alors afficher le libellé sur fond rouge

','2011-11-30',16,5,5,6,29,6,32,'2011-11-09 14:40:50','2012-01-13 11:55:52','2011-11-28',100,16,null,264,1,2,0,'2012-01-13 11:55:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (265,2,2,'Gsicass - Enrichissement des infos sur les OM','h2. Table CPXOMS

Rajouter les champs :
* OMSACAM (Char 15) - Code ACAM du client
* OMSPFT (Char 1) - OM à traiter par la plateforme, bloqué agence

Modifier le champs
* OMSPROV (Integer) --> Char 1
* OMSNUM (Double 15) --> Char 20

Traiter SQL + Entité uniface','2011-11-10',4,5,5,6,26,4,6,'2011-11-09 14:42:23','2011-11-14 10:27:00','2011-11-09',100,4,null,265,1,2,0,'2011-11-14 10:27:00');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (266,3,2,'Livraison 1.9.2','Préparation de la livraison de la version:1.9.2','2011-11-27',null,5,4,4,26,4,4,'2011-11-09 16:03:59','2011-11-28 10:23:31','2011-11-21',100,null,null,266,1,2,0,'2011-11-28 10:23:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (267,2,2,'Création d''un dossier à partir d''un OM Gsicass','Sur l''écran de la liste des ordres de missions :
* Créer un bouton vers nouvel écran "Accès PFT"
* Si accès PFT = OK, tous les ordres de missions sont sélectionnables pour créer un dossier
* Si accès PFT = KO, seuls les ordres de missions OMSPFT != "T" sont utilisables pour créer un dossier

Créer un écran popup "accès PFT" pour saisir le mot de passe de la PFT.

Sur sélection d''un OM, 
* Effectuer la recherche d''antériorité
* Prérenseigner l''écran de la création d''un dossier avec les champs disponible dans l''OM
* Quand c''est un enregistrement Gsicass, donner accès à un écran qui liste les infos du fichier xml en annexe (voir attachment:Gsicass_OM1_exemple.xml)

!detail_ordre_mission.png!

','2012-02-28',4,5,5,6,31,4,19,'2011-11-10 10:57:52','2012-03-17 07:04:05','2011-11-21',100,80,null,267,1,2,0,'2012-03-09 11:22:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (268,1,4,'Vérification accès Allianz','Le contenu d''un dossier est différent pour le login
* azfbat (?)
* mailagf

--> Vérifier les droits

Exemple : sinistre C1130065314','2012-04-30',30,5,4,4,25,4,10,'2011-11-10 15:13:10','2012-05-04 13:38:58','2011-11-10',100,2,null,268,1,2,0,'2012-04-18 11:21:17');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (269,2,2,'POLICE : accès dossier technique chemise 6.02','Afficher le document externe chemise 6.02 dans l''écran POLICE CPXG008','2011-11-15',5,5,5,6,26,6,16,'2011-11-10 17:20:21','2011-11-17 10:05:52','2011-11-14',100,24,null,269,1,2,0,'2011-11-17 10:05:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (270,2,2,'Support Info : Aide Client Mineur / Client Majeur','Pour le support informatique :

Créer un nouvel écran disponible à partir du menu du support qui doit pouvoir :

# Trouver un client majeur à partir d''un code client mineur
** saisie du clicod -> tab = recherche -> affichage du climaj
# Lister et filtrer les clients majeur
** critère de filtrage : code, nom, flag mail, code ACAM, actif/inactif
** Affichage : code, nom, flag mail, code ACAM, actif/inactif

Note : Lister un max d''occurences, on doit pouvoir voir tous les clients majeurs avec flag mail = ''T'' sans scrollbar','2011-12-02',24,5,5,4,29,4,10,'2011-11-16 10:11:15','2011-12-16 11:49:27','2011-12-01',100,16,null,270,1,2,0,'2011-12-16 11:49:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (271,1,4,'Onglet antériorité vide','Les dossiers attendus sont absents de l’onglet.

V4 = filtre sur numéro de police + code postal
V5 = filtre sur numéro de police uniquement

Donc dans mon exemple ci-dessous, ce serait normal que je ne vois rien dans mon onglet antériorité en V4… 
Mais en V5 mon onglet antériorité reste vide alors que je m’attends à 5 enregistrements d’après ma recherche sur le numéro de police 54479165
',null,39,1,4,4,null,4,1,'2011-11-16 14:38:03','2012-03-06 09:42:17',null,0,null,null,271,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (272,2,2,'Gsicass - Envoi de Rapport','h2. Objectif

Déclencher un signal pour que DataExchanger envoi le rapport à Gsicass


h2. Accès écran spécial Gsicass

Dans le porte onglet CPXG021, rendre variable le libellé en fonction de l''origine de l''OM :
* Si origine = Telechargement Darva, garder le comportement actuel
* Si origine = Webservice Gsicass, donner accès à un nouvel écran

h2. Ecran Gsicass

h3. Infos générales

* Rappeler le numero de dossier, le numéro d''OM et le code/nom du client en entetes
* Lister les derniers envois effectués
* Créer 3 boutons pour le rapport préliminaire, rapport complémentaire, rapport définitif
* Créer dans CPXPGA une variable "Dossier racine Gsicass" avec pour valeur "N:\\Gsicass\\"
* Pour choisir les documents (voir schema sur cahier)
* CPXL266 -> revoir radiobutton, à tranformer en dropdownlist
** Voir pour créer une liste CPXLIS/CPXVAL



h3. Envoi d''un rapport

* Création d''un fichier texte
** Nom du fichier : [CodeAgence]_[TypeEnvoi]_[NumeroOM]_[ChronoID].txt
Exemple : 0830_MDO08_2012012410170900865_8300000039049.txt
*** Type d''envoi = MDO07, MDO08, MDO09
** Faire appel CPXL010B pour construction PJ + transformation en PDF (même fonctionnement que les mails : CPXG059)
** Contenu du fichier = chemin complet vers les pièces jointes de la forme :
*** \\\\[AGENET]\\agce\\Gsicass\\docid_datetime.pdf
* Stockage dans CPXCHRONO
** Type d''envoi : EDI Gsicass = 6 (Prévoir EDI Darva = 7)
** Libellé dans nompdf exemple = EDI GSICASS - MDO08 Rapport Complémentaire
','2012-02-17',27,5,5,5,31,4,16,'2011-11-24 11:33:06','2012-03-17 07:04:26','2012-01-02',100,40,null,272,1,2,0,'2012-03-05 16:26:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (273,2,2,'Gestion des scanner en reseau','h2. Objectif

Permettre de scanner à partir d''un scanner reseau
# Permettrai de s''affranchir du PC Scan
# Permettrai de scanner à distance (une agence A scanne, a partir d''une application agence B)

note : Prévoir le choix du scanner
Par défaut, scanner de l''agence, mais laisser libre choix de spécifier un scanner d''une autre agence.',null,10,1,null,4,null,4,0,'2011-11-25 14:39:06','2011-11-25 14:39:06',null,0,null,null,273,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (274,1,2,'CPXG103 - rattachement département HS','Dans l''écran CPXG103, il y a un champs non database sur la région alors que l''on s''attends à un rattachement au département.

A corriger','2011-12-02',15,5,5,4,29,4,5,'2011-11-28 10:23:05','2012-01-13 11:34:50','2011-12-02',60,2,null,274,1,2,0,'2012-01-13 11:34:50');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (275,1,3,'Piece jointe manquante à l''export','Dans le cas d''un document joint dans le corps du mail, lors de l''export par la macro, il n''est pas mentionné dans le fichier xml.

Exemple :

!pb_ajout_document.png!

donne le xml suivant :
<pre><code class="xml">
<exportmail>
<agecod>0830</agecod>
<dosnum>563011</dosnum>
<docdtcre>28/11/2011 15:53:11</docdtcre>
<docdtmail>21/11/2011 11:28:53</docdtmail>
<provenance>S</provenance>
<expediteur>/O=PRUNAY/OU=EXCHANGE ADMINISTRATIVE GROUP (FYDIBOHF23SPDLT)/CN=RECIPIENTS/CN=MAGDALENA.LANGE</expediteur>
<destinataire>/O=PRUNAY/OU=EXCHANGE ADMINISTRATIVE GROUP (FYDIBOHF23SPDLT)/CN=RECIPIENTS/CN=59</destinataire>
<destcopie>/O=PRUNAY/OU=EXCHANGE ADMINISTRATIVE GROUP (FYDIBOHF23SPDLT)/CN=RECIPIENTS/CN=Martial.FLOCH</destcopie>
<objet>TR: 565011 DOA en GD - J45 = 17/11 - relu par MF - Merci. Steph </objet>
<message>20111128-03977.txt</message>
</exportmail>
</code></pre>
',null,null,1,null,4,3,4,0,'2011-11-28 15:55:46','2011-11-28 15:55:46',null,0,null,null,275,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (276,2,4,'Meme acces mail automatique + accès standart','Nous avons un client qui nous demande de ne pas limiter l’accès lorsque l’on envoi un lien dans les alertes mail.
Il souhaite avoir 1 seul identifiant pour
-	Se connecter lui même à ses dossiers
-	Se connecter a partir des mails automatiques envoyés par Hermès

Le mode de fonctionnement d’accès limité actuel ne doit pas changer pour les autres clients
',null,2,1,null,4,null,4,1,'2011-11-28 16:23:14','2012-03-06 09:42:52',null,0,null,null,276,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (277,1,2,'Les documents classé 2.11 mails ne remontent pas sur Hermès','Lors du classement automatique du mail dans le dossier, le paramétrage du document type correspondant n''est pas hérité.

Le document ''mail'' ne remonte pas sur Hermès comme paramétré dans les documents type

h2. A faire 

Prendre en compte le paramétrage du DT ''2.11 mail'' (id 2665), notamment en recopiant les champs DTYIHE, DTYPRU, DTYREF, DTYIND dans CPXDOC

Préparer un rattrapage SQL, update sur CPXDOC, pour corriger tous les enregistrements incorrects. 
',null,6,5,5,6,35,4,3,'2011-11-30 11:02:42','2011-12-06 13:54:45','2011-11-30',100,2,null,277,1,2,0,'2011-12-06 13:54:45');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (278,3,5,'Installation sur Serveur Windows 2003','Installer l''application CPWEB sur Windows/IIS. IP = 10.31.2.39

Cela doit fonctionner en appelant l''adresse de n''importe quel PC : http://10.31.2.39/','2011-12-09',null,5,8,4,33,4,8,'2011-12-01 15:13:17','2012-02-03 13:20:16','2011-12-05',90,40,null,278,1,2,0,'2012-02-03 13:20:00');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (279,2,5,'Bunddle Accueil','Suite sujet #56 : reste à faire =
* Filtrage par expert
* Tests Unitaires',null,22,5,8,4,40,4,11,'2011-12-01 15:41:46','2012-09-10 09:29:34','2012-01-25',100,80,null,279,1,2,0,'2012-09-10 09:29:34');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (280,3,2,'Livraison 1.9.3','Tout ce qui concerne la livraison de la version version:1.9.3','2012-01-06',null,5,4,4,29,4,3,'2011-12-09 10:46:29','2012-01-16 11:49:38','2011-12-12',10,4,null,280,1,2,0,'2012-01-16 11:49:38');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (281,1,2,'Acces Darva HS pour la Corse','Le lien vers le dossier Darva est inaccessible pour la Corse


Client majeur : LAUN-SMABT
Voir dossier 0200/004006/DOA
','2011-12-09',33,5,4,4,29,4,7,'2011-12-09 13:54:27','2012-01-16 11:46:39','2011-12-09',100,2,null,281,1,2,0,'2011-12-09 14:01:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (282,3,2,'Identification et récupération des rapports pour l''AQC / DT','Suite au sujet #163','2012-01-13',21,1,4,4,null,4,0,'2011-12-09 16:59:44','2011-12-09 16:59:44','2012-01-10',0,24,null,282,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (283,2,2,'Pouvoir retourner un document PDF','h2. Contexte

Les Fax arrivant directement par PDF dans la messagerie, ils sont intégrés semi-automatiquement par la macro d''archivage. Cependant, dans quelques cas, ils sont faxés à l''envers et la secrétaire est obligée d''imprimer et de rescanner le document pour le mettre à l''endroit.

h2. Demande 

Prévoir une option pour "retourner" un PDF présent dans les documents externes

h2. Technique

http://www.pdflabs.com/docs/pdftk-cli-examples/
<pre>
Rotate an Entire PDF Document’s Pages to 180 Degrees

    pdftk in.pdf cat 1-endS output out.pdf
</pre>

h2. Exemple de test

attachment:pdf_a_l_envers.pdf',null,5,5,null,4,null,4,2,'2011-12-13 15:05:44','2012-10-03 10:45:55',null,0,null,null,283,1,2,0,'2012-10-03 10:45:55');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (284,1,2,'Lenteur sur écran opération','Améliorer les temps de réponses sur l''écran des opérations

(impact depuis livraison du sujet #11)','2012-01-03',29,5,5,6,29,4,4,'2011-12-13 15:30:25','2012-01-13 11:54:48','2012-01-02',100,16,null,284,1,2,0,'2011-12-21 14:58:58');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (285,2,2,'Date de règlement sur les NH','Ajouter dans le document note d''honoraire une phrase + date de règlement
La date de règlement doit remonter dans la comptabilité (ajouter ds export des règlements)

h2. Calcul de la date

Date de facturation + 30 jours -> récupérer le dernier jour du mois

Exemple 

| facture le 15/12 | 15+30->dernier jour du mois = 31 janvier |
| facture le 31/01 | 30+30->dernier jour du mois = 31 mars |

 ',null,26,1,null,4,null,4,0,'2011-12-15 10:33:37','2011-12-15 10:33:37',null,0,24,null,285,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (286,2,2,'Liste des etats d''un OM','Créer une liste CPXLIS,  Liscod = "OMETA" pour les états d''un OM
Valeurs :
* RECU = "Reçu"
* UTIL = "Utilisé"
* ANNU = "Annulé"

','2011-12-16',4,5,5,6,31,4,4,'2011-12-16 14:53:24','2012-02-03 14:01:29','2011-12-16',100,1,null,286,1,2,0,'2012-02-03 14:01:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (287,2,2,'Liste des systeme à l''origine d''un OM','Créer une liste CPXLIS, Liscod = "OMORI" pour l''origine d''un OM
Valeurs :
* T = "Darva (Téléchargé)"
* D = "Darva"
* G = "Gsicass"
* H = "Hermès"
','2011-12-16',4,5,5,6,31,4,5,'2011-12-16 14:59:19','2012-02-03 14:01:56','2011-12-16',100,1,null,287,1,2,0,'2012-02-03 14:01:56');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (288,3,2,'Livraison OM Gsicass','Attention livrer les scripts SQL des sujets #286 et #287','2012-03-17',null,5,4,4,31,4,11,'2011-12-19 10:48:47','2012-04-26 12:12:28','2012-03-12',100,null,null,288,1,2,0,'2012-03-17 07:06:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (289,2,2,'Commentaire pour la fiche du client mineur','Créer une zone Commentaire dans les codes clients (comme pour les gestionnaires)',null,60,5,5,4,43,6,8,'2011-12-19 15:22:33','2012-06-24 17:25:04',null,100,8,null,289,1,2,0,'2012-06-22 16:18:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (290,3,2,'Ajout Agence Agirdex Eurisk','Serveur : Même IP que CP2000 Biarritz = 10.64.2.1
Code agence : 1640
Nom : Agirdex Eurisk
Email : agirdex@agirdex.com
Adresse : 2 chemin de l''aviation, 64200 Bassussarry
Tel : 05 59 15 21 50
Fax : 05 59 15 21 59
','2012-01-06',null,5,4,4,null,4,7,'2011-12-20 14:33:34','2012-02-03 14:47:34','2011-12-21',100,1,null,290,1,4,0,'2011-12-21 14:43:26');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (291,1,2,'Crash CP2000 sur accès écran opération','Dossier 0830/309778 sur le serveur de l''agence : 
Dès que l''on clique sur le bouton opération de l''onglet dossier --> CP2000 se ferme.

Même problème sur 0570/233043
voir Ticket trackit "28613":http://10.31.2.8/TIWEB/scripts/TIWebPortal/viewWorkOrder.asp?WOID=28613

Edit 21/12 : Même problème sur un dossier de la plateforme à PAU 0640/205323

Ce sont des opérations avec un gros volume de dossier (100 pour le 83, 144 pour le 54, 90 pour le 64)

Cela peut avoir un lien avec le sujet #11 livré dans la version CP2000 version:1.9.2',null,29,5,5,6,29,4,18,'2011-12-20 15:09:53','2012-01-12 16:56:10',null,0,null,null,291,1,2,0,'2012-01-12 16:56:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (292,1,2,'Vérifier les nouveaux messages depuis le PC Scan','Voir pour débloquer la fonction d''import des mails à partir d''un PC Scan
(A priori pas de contre indication technique sauf erreur de ma part... c''est juste bizarre qu''ils travaillent sur le PC Scan ...)


De : Eurisk Pays Basque Béarn/SANJAUME Leila 
Envoyé : lundi 19 décembre 2011 13:22
À : CPCOM/Support
Objet : TR: FONCTION MAIL SUR PC SCAN

Bonjour

Pourriez-vous installer au poste scan la fonction "VERIFIER LES NOUVEAUX MESSAGES" dans l''onglet DOCUMENTS/MAIL .
Etant donné que ces mails ne nous sont pas directement adressés en agence mais retransférés par BASSUSSARRY nous devons coché HERMES ou PUBLIC pour les mettre à disposition sur HERMES.

Leïla SANJAUME
EURISK ARROS DE NAY
05.59.13.99.97
',null,6,1,5,5,null,4,1,'2011-12-20 15:27:57','2012-03-07 11:53:58',null,0,16,null,292,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (293,3,2,'Paramétrage Darva Agirdex Eurisk (1640)','Créer le script CPXLGN pour l''agence 1640, client majeur SMABTP dès reception des infos de Darva

> De : CPCOM/BOUREZ Vincent 
> Envoyé : mercredi 21 décembre 2011 14:12
> À : ''assistance''
> Objet : Client 13752 - Accès transparent Agirdex Eurisk
> 
> Bonjour,
> 
> Pouvez-vous me fournir l’accès transparent du compte n° 13752 – « AGIRDEX EURISK » qui doit commencer à travailler avec SMABTP sur le portail Darva Construction à partir du 1er janvier 2012.
> 
> Merci
> ','2012-01-06',33,5,null,4,null,4,3,'2011-12-21 14:41:42','2012-02-03 14:47:34','2011-12-21',100,1,290,290,2,3,0,'2012-02-03 14:47:34');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (294,1,4,'Lien vers l''aide cassé','Le lien qui doit donner l’accès au document pdf à partir de l’aide (en haut à droite) ne fonctionne pas.
 
Hermès génère un lien dans l’aide comme ceci :
https://10.31.2.131/hermes/jsp/ALL_docblob.jsp?path=../doc/GuideUtilisateur.pdf
La popup sort en erreur et se ferme.
 
Le document est bien accessible ici :
https://10.31.2.131/hermes/doc/GuideUtilisateur.pdf
','2011-11-04',54,5,null,4,11,4,0,'2012-01-04 14:45:48','2012-01-04 14:45:48','2011-11-04',0,null,null,294,1,2,0,'2012-01-04 14:45:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (295,1,4,'Envoi email rapport','Hermès envoi trop d''email concernant les rapports sur l''intégration des dossiers d''une agence.
','2012-01-03',null,5,null,4,36,4,0,'2012-01-04 17:48:03','2012-01-04 17:48:03','2012-01-03',0,null,null,295,1,2,0,'2012-01-04 17:48:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (296,3,2,'Paramétrage des environnements de Tests','Remettre à plat les serveurs 10.31.2.39 et 10.31.2.40 avec 
# siege Prod / siege test/ siege Ref
# 5 agences
## 0830
## 0010
## 0690
## 0350
## 0740

Faire procédure.','2012-01-05',null,5,5,4,null,4,2,'2012-01-05 11:06:27','2012-04-25 10:02:32','2012-01-04',100,20,null,296,1,2,0,'2012-04-25 10:02:32');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (297,1,4,'expéditeur mail auto nouveau rapport','L’expéditeur du mail n’est plus le même
V4 : email de l’agence
V5 : email dans les paramètres Hermes (admin puis hermes@eurisk.fr )

!hermes_emailexpediteur_ko.png!','2012-01-05',null,5,null,4,null,4,2,'2012-01-05 11:08:03','2012-01-20 16:20:40','2012-01-05',0,null,null,297,1,2,0,'2012-01-20 16:20:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (298,1,4,'OM - Lien vers la mission HS dans le mail d''affectation','Nous avons un problème sur la génération du lien vers l’OM dans le mail envoyé à l’agence lors d’une affectation d’OM.

Dans le template mail :
<code>
<p>Vous pouvez consulter cet OM <a href="[EXPR:obj.calcURL()]">en cliquant i&ccedil;i.</a></p>
</code>

le numéro d’OM en paramètre dans le lien n’est pas le bon.
Dans l’exemple ci-dessous, j’ai 29076 au lieu de 29084


<pre>
Hermès a le plaisir de vous confirmer l’affectation de l’ordre de Mission N° OM-29084. 
Instructions pour l’agence :
A OUVRIR ET A TRAITER PAR VOS SOINS
Vous pouvez consulter cet OM en cliquant içi.
https://hermes.eurisk.fr/hermes/jsp/index.jsp?direct=ALL_form.jsp?object=Mission&action=update&nav=add&row_id=29076&inst=the_Mission&login=Eurisk014&pwd=0ls5r4
</pre>
</pre>',null,36,5,null,4,37,4,4,'2012-01-05 11:17:27','2012-02-22 22:25:28','2012-01-04',100,null,null,298,1,2,0,'2012-02-22 09:35:41');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (299,1,4,'Envoi Mail - mauvaise référence dossier dans le mail','Sylvie Chartier :

GENERALI qui signale que les mails adressés à partir d’HERMES , arrivent avec des ref erronées.

Ce matin j’ai eu le cas (cf le mail si dessous que j’ai reçu d’HERMES ) Je pensai à une fausse manœuvre.
Mais en consultant HERMES les ref (au niveau du « sujet ») ne correspondent pas  au dossier 0060/620616 mais au dossier 0340/679548/DOA pour lequel Mme Perret a adressé un mail juste avant, à partir d’HERMES. 
',null,3,5,null,4,37,4,7,'2012-01-05 11:20:22','2012-02-22 22:25:47','2012-01-05',100,null,null,299,1,2,0,'2012-02-22 09:35:57');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (300,1,4,'Dossier - etat dossier MTA absent','L''état du dossier 140984 dans CP2000 de la base 0350 est en Miss Term Auto.
Cet état ne remonte pas sur HERMES 

!hermes_etatdossier_ko.png!','2012-01-05',30,5,4,4,null,4,2,'2012-01-05 12:42:31','2012-01-05 12:45:58','2012-01-05',100,1,null,300,1,2,0,'2012-01-05 12:45:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (301,1,4,'Export - impossible d''exporter avec internet explorer','L''export CSV est impossible avec internet explorer

!hermes_exportIE_ko.png!

Fonctionne avec Firefox',null,55,5,null,4,37,4,4,'2012-01-05 12:50:52','2012-02-22 22:26:06','2012-01-05',100,null,null,301,1,2,0,'2012-02-22 09:36:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (302,1,4,'OM - création mission : mauvais mail agence à l''enregistrement','Nos missions sont enregistrées avec un mail agence ne correspondant pas à ce qui est choisi

Ce qui est choisi (avec la loupe)
!Hermes_creationOM_1.png!

Ce qui est affiché après clic sur le bouton enregistré
!Hermes_creationOM_2.png!',null,36,5,null,4,37,4,5,'2012-01-05 14:40:24','2012-02-22 22:27:24','2012-01-05',100,null,null,302,1,2,0,'2012-02-22 09:36:56');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (303,1,4,'OM - Filtre sur les dates','Les filtres sur les dates des écrans des OM ne fonctionne pas comme attendu 

Il rapporte trop d''enregistrements (voir piece jointe)',null,36,5,null,4,37,4,4,'2012-01-05 15:01:31','2012-02-22 22:27:48','2012-01-05',100,null,null,303,1,2,0,'2012-02-22 09:37:07');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (304,2,4,'OM - champs Date réception mission expert obligatoire','Dans la création d''OM, mettre le champs intitulé "Date réception mission expert" obligatoire.',null,36,5,null,4,37,4,6,'2012-01-05 15:03:35','2012-02-22 22:29:39','2012-01-05',100,null,null,304,1,2,0,'2012-02-22 09:39:00');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (305,2,4,'OM - Annulation d''une mission affectée','Un OM affecté doit pouvoir être annulé

Rajouter l''etat annulé + le bouton annulé à disposition de l''utilisateur

!hermes_om_annuler_om_affecte.png!',null,36,5,null,4,37,4,7,'2012-01-05 15:12:24','2012-02-22 22:30:01','2012-01-05',100,null,null,305,1,2,0,'2012-02-22 09:39:14');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (306,1,4,'Manque le type de mission dans la recherche dossier','Impossible de faire une recherche dans les dossiers par type de mission

Si je fais une requête en filtrant sur le N° de dossier EURISK par exemple avec %RCD%  -> ça ne fonctionne pas

!hermes_filtrage_dossier.png!',null,30,5,null,4,37,4,6,'2012-01-05 15:15:52','2012-02-22 22:28:11','2012-01-05',100,null,null,306,1,2,0,'2012-02-22 09:37:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (307,1,4,'Import - manque accès aux logs dans l''interface','De : CPCOM/AMANT Francois 
Envoyé : jeudi 5 janvier 2012 10:38
À : Nathalie SEITZ (nseitz@cite-si.net)
Objet : Problème d''import.

Bonjour,

Pourriez-vous regarder dans les logs joints car nous n’avons plus accès depuis l’interface aux logs concernant les erreurs de traitement de l’import ?

Vous trouverez ci-joint une copie d’écran de la liste des dossiers en erreur ou vous pourrez constater que pour le traitement du 03 nous avons bien accès aux fichiers Source et Log par contre depuis le 04 nous n’y avons plus accès.

J’ai regardé dans les logs et voici la liste des dossiers concernés :

04/01/2012 10 : 35 : 59 => AXASCPAR01_2012105_0780645588.xml (dossier 0780/645588)
04/01/2012 10 : 36 : 00 => AXASSFON02_20110311_068088.xml (dossier 0680/88)

05/01/2012 06 : 58 : 03 => AXASSANG02_20110601_0380488039.xml (dossier 0380/488039)
05/01/2012 06 : 58 : 04 => AXASSANG02_20111207_0380488394.xml (dossier 0380/488394)
05/01/2012 06 : 58 : 04 => AXASSFON02_20091217_0380486759.xml (dossier 0380/486759)
05/01/2012 06 : 58 : 04 => AXASSFON02_20100706_0380487301.xml (dossier 0380/487301)


!hermes_ListeDossiersEnErreur.jpg!',null,55,5,null,4,null,4,3,'2012-01-05 15:20:35','2012-02-23 15:59:24','2012-01-05',100,null,null,307,1,2,0,'2012-02-23 15:59:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (308,1,4,'Dossier - manque la référence Eurisk dans le tableau resultat','La référence Eurisk est manquante dans le tableau de la liste des résultats après une recherche de dossier.

Demande :
Afficher le numéro de eurisk du type CODEAGENCE/DOSSIER/MISSION en 2e position après le numéro de sinistre (exemple 0830/309000/DOA)',null,30,5,null,4,37,4,5,'2012-01-05 15:28:45','2012-02-22 22:28:29','2012-01-05',100,null,null,308,1,2,0,'2012-02-22 09:37:44');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (309,1,4,'Dossier - Etat NH manquant','Sylvie Chartier :

Dans ce dossier 0330/64443/DOA, la NH apparaît "réglée" dans l’onglet  NH. Or sur l’écran principal à "état NH" : rien n’apparaît',null,30,5,null,4,37,4,5,'2012-01-05 15:32:28','2012-02-22 22:28:46','2012-01-05',100,null,null,309,1,2,0,'2012-02-22 09:38:15');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (310,1,4,'Documents - harmoniser les colonnes','* dans l’onglet "documents"», les titres de colonnes sont : action, voir, titre du document, taille, type, date de validation, destinataire
* dans l’onglet  "autres pièces", les titres des colonnes sont : action, voir, taille, type, N°, date, titre du document, émetteur expéditeur :

le titre du doc devrait être mis après la colonne voir comme dans "documents"
',null,39,5,null,4,37,4,5,'2012-01-05 15:34:50','2012-02-22 22:29:01','2012-01-05',100,null,null,310,1,2,0,'2012-02-22 09:38:34');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (311,1,4,'Documents - ouverture problématique dans IE (Sylvie)','Sylvie Chartier :

Qd on clique devant un doc externe  (autres pièces), le message est le suivant : pour protéger votre sécurité, internet a bloqué le téléchargement…..il faut télécharger le fichier (pas ce problème dans la version IV) pour enfin accéder à l’ouverture ou l’enregistrement du dossier. 

Le temps d’ouverture d’un doc est très long…
',null,39,2,null,4,null,4,4,'2012-01-05 15:35:53','2012-02-24 10:21:29','2012-01-05',0,null,null,311,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (312,1,4,'OM - écran om sans dossier : contient des OM avec dossier','Dans l''écran "OM sans dossier", aujourd''hui sont affichés des OM qui contiennent des dossiers

!hermes_om_sans_dossier.png!',null,36,5,null,4,37,4,4,'2012-01-06 10:50:38','2012-02-22 22:29:19','2012-01-06',100,null,null,312,1,2,0,'2012-02-22 09:38:45');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (313,2,4,'Mail - Agrandir le champs copie "cc"','De : Eurisk Plateforme/MANGIN Sandrine 
Envoyé : mardi 10 janvier 2012 10:50
À : CPCOM/BOUREZ Vincent
Cc : Eurisk Siege/BOYER Paul; Eurisk Plateforme/BOUDET Karin
Objet : PROBEME HERMES NOUVELLE VERSION

Bonjour,

je viens d''avoir M. ERNOUX de GENERALI qui m''informe rencontrer un problème avec la nouvelle version d''Hermes pour l''envoi d''un mail.
Si il n''y a qu''un destinataire en copie, cela fonctionne. Par contre, en voulant mettre deux destinataires en copie (malgré le point virgule entre les deux) cela ne fonctionne pas.

Je lui ai dit que je faisait remonter l''information auprès du service informatique.

A voir... 

Cordialement.

Sandrine MANGIN
Plate-forme Téléphonique
Tél. : 01.30.82.11.03


h2. Infos complémentaires

Le champs est effectivement limité à 50 caractères actuellement ce qui est trop petit.
Nous allons le passer en illimité comme le champs "A" juste au dessus.

Ticket 248 chez Cité SI',null,3,5,null,4,37,4,4,'2012-01-10 11:00:39','2012-02-22 22:30:37','2012-01-10',100,null,null,313,1,2,0,'2012-02-22 09:39:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (314,1,4,'Export - dates à l''envers dans Excel 2003','Excel 2003 affiche les dates des fichiers exportés à l''envers, exemple :
le 1er décembre 2011 = 12/01/2011 au lieu de 01/12/2011
voir fichier exporté + affichage en pièce jointe

Ticket 249 chez CitéSI',null,55,6,null,4,null,4,1,'2012-01-10 11:16:32','2012-01-20 09:53:44','2012-01-10',0,null,null,314,1,2,0,'2012-01-20 09:53:44');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (315,1,4,'Document - icone PDF manquante','Dossier 0641/552 
Le document est bien présent dans le dossier, mais l''icone et la taille du fichier sont manquantes à l''affichage

Ticket 252 CitéSI',null,39,5,null,4,null,4,1,'2012-01-10 16:21:03','2012-01-20 09:52:48','2012-01-10',0,null,null,315,1,2,0,'2012-01-20 09:52:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (316,1,4,'Import dossier trop long depuis la V5 (François)','François Amant :
Depuis la V5, le temps d''import moyen d''une journée a plus que doublé.
Il est passé à environ 4H.
Avant, en V4, le temps de traitement était environ d''1h30

Ticket 251 CitéSI',null,56,5,null,6,39,4,9,'2012-01-10 16:22:33','2012-04-03 13:35:12','2012-01-10',100,null,null,316,1,2,0,'2012-02-27 17:45:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (317,1,2,'lenteur sur les écrans CPXG063 CPXG010 et CPXG005 ','Pb de lenteur sur les écrans CPXG063 CPXG010 et CPXG005 sur les opérations existantes (peut-être bp de dossiers?? sur toulon). cette lenteur n''existe pas lors de la création d''une nouvelle police et mission (Ecran vierge!!)
exemple dossier 311254 ou 311220 pour rapatrier un POLS dans le répertoire du dossier - Action : onglet REPERTOIRE clic POLS DE L''OPERATION cocher case POLS [enregistrer et quitter] c''est très lent pour revenir sur l''onglet Répertoire
ou pour rapatrier un intervenant de l''écran CPXG005 même lenteur
Accès à l''écran CPXG010 pour le bouton OPERATION puis POLS/DOMMAGES','2012-03-16',8,5,5,6,31,6,4,'2012-01-10 17:29:42','2012-03-17 06:59:29','2012-03-12',100,40,null,317,1,2,0,'2012-03-17 06:59:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (318,1,2,'DT064 Ind06 ','Dans le dosier 311254 sur la base de TEST, c''est ainsi que s''affiche la date "Le, 10 Janvier 2012" avec une virgule alors que dans le même document (DT064 Ind06)sur BaseRéf et en Agence il n''y a pas la virgule voir dossier 311254',null,5,4,5,4,null,6,4,'2012-01-10 17:34:26','2012-12-05 09:32:34','2012-01-10',0,null,null,318,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (319,1,2,'Fort enjeux - mail non envoyé','* Tester l''envoi de mail à fort enjeux
* Regarder pourquoi on a pas envoyé de mail "fort enjeux" sur le cas 0830/309833

voir #164','2012-01-11',6,5,5,4,29,4,13,'2012-01-10 17:51:55','2012-01-12 16:55:52','2012-01-10',0,null,null,319,1,2,0,'2012-01-12 16:55:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (320,2,2,'Document de l''opération - ajout filtrage et export csv','Ajouter Filtre de recherche (comme sur onglet Documents ext.) et Export (pour exporter la liste sur fichier Excell)- Possible ou non ?? Merci  
  ','2012-03-30',5,5,5,4,42,6,16,'2012-01-11 11:10:36','2012-06-17 11:34:47','2012-03-26',100,40,null,320,1,2,0,'2012-06-01 14:34:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (321,2,2,'Mail - tester l''existence de l''outil ''blat''','h2. problème

Si blat.exe disparait du serveur, les mails ne peuvent plus partir et CP2000 ne previent pas l''utilisateur 

h2. A faire

Tester l''existence de l''outil blat. Le chemin est renseigné dans CPXLOG (LOGCOD = "BLAT").
Donc à partir du CPXS050 -> appeler CPXS013 (de tete) avec LOGCOD qui renvoi un booleen "présent" oui/non. 
','2012-03-09',6,5,5,4,31,4,7,'2012-01-11 17:18:25','2012-03-17 07:04:45','2012-03-05',100,16,null,321,1,2,0,'2012-03-09 11:16:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (322,1,2,'Ajout Pol - probleme récupération adresse','On ne récupère pas l''adresse du dossier en cours dans l''écran CPXG063 à partir du bouton "..."

note technique :
Passage de paramètre HS dans le bouton "..." : les paramètres opeid, dosnum et agecod sont vides
activate "CPXG064"."exec"( $P_CONSULT$ , V_CREREPIREP , V_CREREPID , OPEID.CPXDOS , DOSNUM.CPXDOS , AGECOD.CPXDOS )',null,35,5,5,4,29,4,3,'2012-01-11 17:44:16','2012-01-12 16:54:43',null,0,null,null,322,1,2,0,'2012-01-12 16:54:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (323,1,2,'URGENT Transfert de Siègetest à Site test ne fonctionne pas','Le pb de mes tests sur la création de mes alertes vient du fait que le transfert de Siègetest à Sitetest ne fonctionne pas corectement. ',null,null,6,null,6,null,6,3,'2012-01-13 11:40:04','2012-04-18 11:30:37','2012-01-13',0,null,null,323,1,2,0,'2012-04-18 11:30:37');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (324,2,6,'Envoi de mail différé','h2. Objectif

Envoyer pendant la nuit les mails générés par CP2000 le jour.

* Les éléments sont préparés par CP2000, voir #217
* il faut controler les logs d''envoi pour détecter les erreurs éventuelles
* Trigger date : envoi à 1h du matin.

',null,28,5,4,4,null,4,1,'2012-01-16 18:16:26','2012-01-16 18:16:55','2012-01-10',100,24,null,324,1,2,0,'2012-01-16 18:16:55');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (325,1,2,'pb affichage partiel des enjeux sur hermès','Le TTC doit être valorisé comme le HT (actuellement quand le TTC n''est pas renseigné dans CP2000, l''enjeu initial ne remonte pas sur HERMES).
Si le montant de l''indemnité n''est pas renseigné, il ne faut pas afficher "0"',null,16,5,6,6,31,6,7,'2012-01-17 14:26:23','2012-03-17 07:01:10','2012-01-17',100,8,null,325,1,2,0,'2012-03-09 11:23:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (326,1,2,'Reclamations - Perte d''infos à l''enregistrement','De : Eurisk Siege/DUMONT Sylvie 
Envoyé : mardi 17 janvier 2012 16:44
À : CPCOM/BOUREZ Vincent
Objet : Problème dans "gestion des réclamations" CP 2000

Bonjour Vincent,

Je rencontre un problème dans CP2000 pour la gestion des réclamations sur l’agence de Colmar. J’ai enregistré une réclamation et il y a des champs que j’avais bien saisis (intervenants et dates de traitement) qui s’enlèvent à l’enregistrement,  donc par conséquent manquants sur le document FR009 associé.

A ta disposition pour en parler

Sylvie Dumont
Service Clients
Tél : 01.30.78.18.53
email : sylvie.dumont@eurisk.fr
',null,57,5,5,6,null,4,4,'2012-01-17 17:27:08','2012-06-27 16:21:12','2012-01-17',60,null,null,326,1,2,0,'2012-06-27 16:21:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (327,3,2,'Agirdex - paramétrage signature expert + darva','* Faire Script SQL pour copier les signatures des experts

M. CASTILLON 1643 (signature sur code 1431)
M. VACHERAND 1644 (signature sur code
M. PEYROUX 1645 (signature sur code 1430)
M. DUSSAUSSOIS 1646 (signature sur code 1433)
M. SOLER 1647 (signature sur code 1447)

* Faire script SQL pour CPXLGN',null,null,5,4,4,null,4,3,'2012-01-18 15:06:27','2012-01-27 14:39:42','2012-01-18',100,null,null,327,1,2,0,'2012-01-27 14:39:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (328,2,2,'Envoi FAX','Lorsqu''on envoie un Fax à partir de CP2000, sur le document reçu par le destinataire le n° de FAx d''EURISK ne s''affiche pas ',null,null,1,null,4,null,6,1,'2012-01-19 10:53:33','2012-02-03 14:43:40','2012-01-19',0,null,null,328,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (329,1,2,'Erreur dans l''affichage du nom de l''expert dans l''écran des Mails externes à classer','Erreur dans l''affichage du nom de l''expert dans l''écran des Mails externes à classer
Le même nom d''expert se retrouve dans toutes les PJ entrantes à classer alors qu''il n''apparait pas forcément dans le dossier concerné.',null,6,5,5,5,null,5,3,'2012-01-20 13:39:07','2012-01-20 14:34:58','2012-01-20',60,4,null,329,1,2,0,'2012-01-20 14:34:58');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (330,1,2,'N° sinistre dossier DARVA','Le n° de sinistre pour un dossier DARVA est modifiable alors qu''il doit être bloqué et grisé (voir dossier 0910/494946)',null,4,5,5,4,31,6,10,'2012-01-26 18:03:01','2012-03-17 07:01:29','2012-01-26',100,null,null,330,1,2,0,'2012-02-03 14:04:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (331,1,2,'N° sinistre dossier DARVA','Le n° de sinistre pour un dossier DARVA est modifiable alors qu''il doit être bloqué et grisé (voir dossier 0910/494946)',null,null,5,null,4,null,6,1,'2012-01-26 18:04:08','2012-01-26 18:21:49','2012-01-26',0,null,null,331,1,2,0,'2012-01-26 18:21:49');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (332,3,2,'Liste des codes date pour intégration DARVA/GSICASS','Liste des codes date DARVA :

!http://10.31.2.42/attachments/download/28!

Liste des codes date GSICASS :',null,4,5,5,4,31,5,2,'2012-01-27 15:43:08','2012-03-02 11:03:02','2012-01-27',0,null,null,332,1,2,0,'2012-03-02 11:03:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (333,3,2,'Remise à plat base Uniface','',null,15,5,5,4,null,5,1,'2012-01-31 10:03:09','2012-02-03 14:42:24','2012-01-31',100,null,null,333,1,2,0,'2012-02-03 14:42:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (334,2,5,'Migration vers Symfony','S''assurer de la migration de Symfony dans sa dernière version afin d''etre compatible avec Wamp 2.2c',null,null,5,8,4,null,10,6,'2012-01-31 13:51:08','2012-02-16 08:47:08','2012-01-31',100,null,null,334,1,2,0,'2012-02-03 13:18:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (335,1,5,'Configuration SVN','Configurer le SVN afin d y intégrer la mise a jour automatique de Symfony',null,null,5,8,4,null,10,2,'2012-01-31 13:54:25','2012-02-03 13:19:13','2012-01-31',100,null,null,335,1,2,0,'2012-02-03 13:19:13');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (336,1,2,'Darva accessible avec code client MMA-072-01','DARVA pour MMA ou COVEA ne doit être accessible qu''avec les codes clients : DARVAMMA01 ou DARVACOV01.
avec le code MMA-072-01 on a le lien avec DARVA (vu dans dossier 0950/755162)? ce n''est pas normal ça tjs était bloqué puisque ce code client est utilisé pour mail auto HERMES ',null,null,1,null,4,null,6,1,'2012-01-31 16:44:37','2012-01-31 17:51:43','2012-01-31',0,null,null,336,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (337,1,2,'PB sur Ecran CPXG131 - création d''une note d''honoraires en régie','Je sélectionne une prestation, le code Expert s''affiche automatiquement. Mais lorsque je modifie cette Prestation le Code Expert disparaît.',null,null,1,null,4,null,6,0,'2012-01-31 17:56:02','2012-01-31 17:56:02','2012-01-31',0,null,null,337,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (340,3,5,'Documentation KASTOR','Se baser sur les règles de nommages mise en place.
Inclure 
Inclure:
- les phases de déploiement de chaque module.
- les processus de tests unitaire et de tests intégré.
- inclure les processus de mise à jour de Symfony avec GIT
- inclure une procedure de deploiement complet de l''application sous :
   * Windows
   * Linux

',null,18,1,10,4,null,10,1,'2012-02-01 09:18:16','2013-05-03 15:12:13','2012-02-01',0,16,null,340,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (341,3,5,'Décrire le processus de développement modulaire','Décrire pour chaque module le cycle de développement attendu',null,null,5,10,4,null,10,1,'2012-02-01 09:21:12','2012-09-10 09:29:00','2012-02-01',0,null,null,341,1,2,0,'2012-09-10 09:29:00');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (342,2,5,'Charte Graphique','Proposer une charte graphique pour chaque objet de l''application et la documenter','2012-03-30',18,5,10,4,40,10,36,'2012-02-01 09:27:34','2012-09-10 09:28:43','2012-02-01',100,98,null,342,1,24,0,'2012-09-10 09:28:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (343,2,5,' Layout principal des pages','','2012-02-03',null,5,10,4,40,10,10,'2012-02-01 09:28:15','2012-09-10 09:28:24','2012-02-01',100,8,342,342,2,3,0,'2012-02-09 12:34:04');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (344,2,5,'Tableaux','','2012-02-08',null,5,8,3,40,10,9,'2012-02-01 09:29:30','2012-09-10 09:28:12','2012-02-01',100,16,342,342,4,5,0,'2012-09-10 09:28:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (345,2,5,'Formulaire','Créer la charte graphique des formulaires','2012-03-12',null,5,10,4,40,10,8,'2012-02-01 09:30:34','2012-09-10 09:28:00','2012-02-01',40,32,342,342,6,7,0,'2012-09-10 09:28:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (346,2,5,'Messages','Créer la chartes graphique des messages:
_ information
_ validation
_ erreur','2012-02-03',null,5,10,4,40,10,11,'2012-02-01 09:31:09','2012-09-10 09:27:48','2012-02-01',100,2,342,342,8,9,0,'2012-09-10 09:27:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (347,2,5,'Button','Créer la charte graphique des objet de type bouton','2012-03-30',null,5,null,4,40,10,7,'2012-02-01 09:31:35','2012-09-10 09:27:37','2012-02-01',10,8,342,342,10,11,0,'2012-09-10 09:27:37');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (348,2,5,'Images','Sélectionner les différentes images du site
','2012-03-30',null,5,null,4,40,10,6,'2012-02-01 09:32:07','2012-09-10 09:27:26','2012-02-01',10,32,342,342,12,13,0,'2012-09-10 09:27:26');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (349,2,5,'Etude de faisabilité','Etudier la faisabilité d''un concept et valider l''orientation a prendre','2012-03-02',null,5,null,4,null,10,13,'2012-02-01 09:33:30','2012-09-10 09:27:12','2012-02-08',100,48,null,349,1,12,0,'2012-09-10 09:27:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (350,2,5,'Internationalisation','Tester l''internationalisatio nsur la page de connexion et valider la faisabilité','2012-03-02',null,5,10,4,40,10,21,'2012-02-01 09:34:45','2013-04-17 15:00:38','2012-02-08',100,48,349,349,2,11,0,'2013-04-17 15:00:38');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (351,1,6,'Envoi mail différés - tester les fichiers en local','Afin de fiabiliser les controles, il faut améliorer le scénarios pour tester les fichiers en local (dans le temp) 

suite du sujet #324',null,28,5,4,4,null,4,3,'2012-02-01 13:54:48','2012-06-18 17:44:10','2012-02-01',100,4,null,351,1,2,0,'2012-06-18 17:44:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (352,2,6,'Dump SQL DataExchanger','Il faut enrichir le script avec toutes les bases MySQL présentes sur le serveur DEX',null,31,5,4,4,null,4,5,'2012-02-01 14:03:09','2012-02-02 12:26:25','2012-02-01',80,4,null,352,1,2,0,'2012-02-01 16:50:54');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (353,1,2,'Erreur sur document Crac/Sycodes','Le contenu de l''écran CP2000 est différent de ce qui est écrit dans le document word correspondant???

Voir la partie "ACTEUR" --> dans l''écran = FMG / dans le document = GEOXIA

!cs_ecrancp2000.png!

!cs_document.png!','2012-03-23',21,6,5,4,null,6,5,'2012-02-01 15:04:24','2012-03-28 12:00:09','2012-03-22',100,16,null,353,1,2,0,'2012-03-28 12:00:09');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (354,2,5,'Description technique modulaire de CPWEB','Ce document devra contenir pour chaque module:
- les pages qu''il contient
- la navigation
- les objects spécifiques contenus
_ les types d''object des taillés
- les contraintes d''entrée
- les contraintes de sortie
- les messages d''erreurs globaux
_ les messages d''erreurs sur les champs
...',null,18,1,10,4,null,10,0,'2012-02-02 08:55:26','2012-02-02 08:55:26','2012-02-02',0,null,null,354,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (355,1,2,'ECRAN CPXL010B - Mauvais affichage Date des documents externes','Ecran CPXL010B Envoi de document -Liste des documents externes du dossier : ils s''affichent en fonction de leur indexation (1.01-2.11)L''affichage doit se faire en fonction de la date de création (du document le + récent au document le - récent)pour faciliter nos envois. on doit t cliquer 2 fois sur Date pour qu''elles s''affichent dans l''ordre chronologique','2012-02-17',27,5,6,4,31,6,5,'2012-02-02 18:48:54','2012-03-06 14:40:31','2012-02-02',100,null,null,355,1,2,0,'2012-03-06 14:40:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (356,2,5,'Menu','',null,null,5,8,4,40,10,11,'2012-02-03 13:15:16','2012-09-10 09:26:23','2012-07-20',100,null,342,342,14,17,0,'2012-09-10 09:26:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (358,2,5,'Tester PHPDoc et statuer sur son utilisation','Voir les possibilités offertes par phpdocs',null,null,5,10,4,32,10,7,'2012-02-03 13:28:46','2013-04-17 14:55:42','2012-02-03',100,32,null,358,1,2,0,'2013-04-17 14:55:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (359,2,5,'Intégration du PHPUNIT','Tester l''intégration de PHPUNIT dans les modules déployés','2012-03-02',null,5,10,5,33,10,6,'2012-02-03 14:04:31','2012-02-16 11:14:02','2012-02-13',100,12,null,359,1,2,0,'2012-02-16 11:14:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (360,2,4,'Purge à rendre accessible dans l''exploitation (François)','Demande d''ajout de l''accès à la purge des logs dans l''interface d''exploitation.

En vu d''améliorer les temps de traitements, voir sujet #316

Ticket 253 chez Cité-SI. Nécessitera un patch xml',null,56,5,null,4,39,4,4,'2012-02-06 10:47:40','2012-04-03 13:55:55','2012-02-06',100,null,null,360,1,2,0,'2012-04-03 13:55:55');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (361,2,2,'Ajout zone de saisie Code immeuble sur Ecran CPXG009','Créer une nouvelle zone de saisie sur l''écran Enrichissement Opération CPXG009 pour renseigner Codes accès d''un immeuble avec paramétrage dans la fiche Type(Suite demande Luc Mareel) ','2012-03-23',29,5,5,4,38,6,13,'2012-02-07 14:45:33','2012-04-01 16:01:47','2012-03-19',100,40,null,361,1,2,0,'2012-04-01 16:01:47');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (362,2,5,'S''assurer de l''internationalisation du nom des colomnes de tableau','CF REPERTOIRE DATA',null,null,5,8,3,40,10,5,'2012-02-08 09:30:14','2012-09-10 09:25:46','2012-02-08',100,8,350,349,3,4,0,'2012-09-10 09:25:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (363,1,2,'Manque accès portail DARVA du 78 à partir de l''agence de LORIENT','L''identifiant et mot de passe du 78 n''est pas accessible sur le portail; par contre on a bien accès à DARVA à partir du dossier CP2000.',null,null,5,null,4,null,6,1,'2012-02-08 10:18:41','2012-02-08 12:48:55','2012-02-08',0,null,null,363,1,2,0,'2012-02-08 12:48:55');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (365,2,5,'Bunddle Admin','Administratrion de l application et gestion des driots',null,null,5,null,6,33,10,72,'2012-02-09 09:26:56','2013-05-07 11:34:10','2012-02-10',100,212,null,365,1,46,0,'2013-04-17 14:55:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (366,2,5,'Gestion des droits','Proposer une interface de gestion des droits par modules et par role',null,null,5,8,6,33,10,9,'2012-02-09 09:28:46','2013-04-17 14:54:55','2012-04-11',100,null,365,365,2,5,0,'2013-04-17 14:54:55');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (367,2,5,'Page d''erreur de Symfony','Configurer les pages d''erreur de Symfony','2012-02-10',null,5,10,4,40,10,6,'2012-02-09 09:48:43','2013-04-17 14:54:35','2012-02-09',100,8,null,367,1,2,0,'2013-04-17 14:54:35');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (368,2,5,'Revue de code','Revoir le code déjà développé sur les modules 
- IMAG
- ACCUEIL
- ADMIN

S''assurer que les commentaires sont inscrits
S''assurer que la charte définie est intégrée
S''assurer que le code est conforme à la version de Symfony
S''assurer que les paramètres sont bien dans le parameter.ini (exemple mail et mot de passe de l''administrateur)
S''assurer que l''internationalisation est correcte
','2013-12-31',null,5,10,5,40,10,13,'2012-02-09 11:58:09','2013-04-17 14:53:40','2012-04-11',95,100,null,368,1,8,0,'2013-04-17 14:53:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (369,2,5,'Revue de code DAVID','CF descriptif précédent',null,null,5,8,5,40,10,8,'2012-02-09 11:58:40','2013-04-17 14:53:24','2012-04-11',100,null,368,368,2,5,0,'2013-04-17 14:53:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (370,2,5,'Module de log','Tracer toute les requetes faites à MYSQL dans des fichiers de LOG
L''activation de ces log sera dépendante d''un bouléen dans les paramètres
Structure du fichiers de log
*Si la requête de génère pas d''erreur*
*Nom du fichier: requeteok_user_timestamp.xml //Si l''utilisateur n''est pas encore connecté mettre la valeur de user à ananymous
Balise = 
# <requete>requete_sql</requete> 
# <resultat><champ>champ1</champ><valeur>valeur1</valeur><champ>champ2</champ><valeur>valeur2</valeur></resultat> 
*Si la requete génère des erreurs*
*Nom du fichier: erreur_user_timestamp.xml //Si l''utilisateur n''est pas encore connecté mettre la valeur de user à ananymous
Balise = 
# <requete>requete_sql</requete> 
# <resultat><erreur>Message d''erreur MySQL</erreur></resultat> ','2012-03-02',12,5,10,4,40,10,8,'2012-02-09 12:06:34','2012-09-10 09:23:52','2012-02-13',80,32,null,370,1,2,0,'2012-09-10 09:23:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (371,2,5,'Gestion des utilisateurs','Proposer une interface dans la partie administration de gestion des utilisateurs.
# Valider les tables nécessaire à la gestion des profils
# Permettre l’interaction avec ces tables : ajout, modification, suppression (attention, il s''agit d''une suppression logique, on ne supprime pas de la base)
*  la suppression d''un utilisateur entraine un rôle non défini lors de la connexion ??? A valider avec Vincent !
# Associé un rôle à chaque utilisateurs (nous verrons la pertinence de gérer plusieurs rôles dans un second temps)

',null,null,5,8,6,34,10,46,'2012-02-09 12:18:03','2013-05-07 11:34:10','2012-02-10',100,48,365,365,6,33,0,'2012-09-10 09:23:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (372,1,2,'Manque mise à jour du dossier sur intégration d''un mail','h2. Tests

Sur 10.31.2.40, je prend le dossier 311150. 

<pre><code class="SQL">
select dosdtmaj from cpxdos where dosnum = 311150;
-- date de mise à jour = 2012-01-18 15:02:11
select count(*) from cpxdoc where dosnum = 311150;
-- Nombre de document dans le dossier = 4
</code></pre>

J''archive un mail

<pre><code class="SQL">
select dosdtmaj from cpxdos where dosnum = 311150;
-- date de mise à jour = 2012-01-18 15:02:11
select count(*) from cpxdoc where dosnum = 311150;
-- Nombre de document dans le dossier = 5
</code></pre>

La date de mise à jour n''est pas rafraîchie dans le dossier !

h2. A faire

CPXS021 - créer un opération de mise à jour du dossier
CPXS050 - a la fin de la procédure d''intégration d''un document dans un dossier, appeler l''opération pour mettre CPXDOS à jour sur la date de modif.','2012-02-10',6,4,5,3,null,4,5,'2012-02-09 12:25:33','2012-03-19 11:26:15','2012-02-10',100,4,null,372,1,2,0,'2012-03-06 14:39:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (373,2,5,'S''assurer de l''internationalisation de l''existant','','2012-03-02',null,5,10,4,40,10,3,'2012-02-09 12:50:42','2012-09-10 09:26:59','2012-02-09',100,32,350,349,5,6,0,'2012-09-10 09:26:59');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (374,2,5,'Revoir la structure de la BD','Intégrer les contraintes suivantes:
- un utilisateur peut avoir accès a plusieurs agence avec un droit spécifique par agence
- un expert est un utilisateur avec des données supplémentaires (ex: signature)
- un utilisateur a des préférences utilisateur (agence préfére)

',null,null,5,10,6,33,10,31,'2012-02-10 11:53:25','2013-05-07 11:34:10','2012-02-29',100,null,371,365,7,22,0,'2012-09-10 09:24:35');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (375,2,5,'Synchronisation des données LDAP - MySQL','A chaque fois que l''on s''authentifie avec LDAP, il faudra mettre à jour les données locale de MySql avec les information retournée par LDAP
A ce jour *Nom* et *Prénom*',null,null,5,null,4,40,10,4,'2012-02-10 12:12:30','2012-09-10 09:22:46','2012-02-10',80,null,371,365,23,24,0,'2012-09-10 09:22:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (376,2,5,'Adapter le code existant avec le nouvel objet user','Ne pas oublier:
- chartes et traduction',null,null,5,null,4,40,10,5,'2012-02-10 12:14:26','2012-09-10 09:22:20','2012-02-10',100,null,371,365,25,26,0,'2012-09-10 09:22:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (377,1,4,'Dans le sujet de l''envoi mail, il faut la date J+60 (Sylvie)','De : Eurisk Siège/CHARTIER Sylvie 
Envoyé : vendredi 10 février 2012 15:51
À : CPCOM/AMANT Francois; CPCOM/Support
Cc : VEMCLEFS Corinne; sarrouche@generali.fr
Objet : TR: 53908596 - ARROUCHE SARAH - 0910/494609 - DOA - AL014608 - 27/09/2011
Importance : Haute

Bonjour,

Dossier DO gestion classique dans le mail automatique, le jour J apparait au lieu du J 60. Merci de voir et nous tenir informés

Cordialement

P/O Sylvie Chartier – Sylvie DUMONT
 
sylvie.chartier@eurisk.fr 
   01 30 78 18 07 
  01 30 78 18 23 


A corriger ici : 
source:/trunk/instances/hermesC/src/com/simplicite/objects/Hermes/HermesC/DocumentHermes.java#L346','2012-04-30',3,5,null,4,25,4,6,'2012-02-10 16:27:38','2012-05-04 13:40:05','2012-02-10',100,null,null,377,1,2,0,'2012-04-23 16:05:14');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (378,2,5,'Paramétrer différents environnements','L''objectif est de tester la possibilité de paramétrage de divers environnements:
- dev: pour les développers
- test: pour les tests intégrés
- param: pour les tests de paramétrage au siège
- prod: pour l''environnement de production

Les fichiers à créer:
- app_%env%.php
- param_%env%.php
- routing_%env%.php
- config_%env%.php

Exemple de paramétrage du fichier .HTACCESS
<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteCond %{HTTP_HOST} ^(prod.cpcom\\.com)(:80)? [NC] 
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^(.*)$ app.php [QSA,L]
    RewriteCond %{HTTP_HOST} ^(dev.cpcom\\.com)(:80)? [NC] 
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^(.*)$ app_dev.php [QSA,L]
    RewriteCond %{HTTP_HOST} ^(param.cpcom\\.com)(:80)? [NC] 
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^(.*)$ app_param.php [QSA,L]
</IfModule>

','2012-02-17',null,5,null,3,40,10,6,'2012-02-15 10:57:42','2012-09-10 09:22:06','2012-02-14',100,18,null,378,1,2,0,'2012-09-10 09:22:06');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (379,2,5,'PHPUnit','S''assurer que le module PHP unit teste au moins 70% des cas dans le *Bundle CPCOM*
- Admin
- Accueil
- CpWebCommon
- UserBundle','2012-03-12',null,5,8,4,40,10,6,'2012-02-15 17:30:47','2013-04-17 14:53:08','2012-02-15',100,null,null,379,1,2,0,'2013-04-17 14:53:08');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (380,2,5,'S''assurer que les messages d''erreur de Symfony sortent en Francais','',null,null,5,null,4,40,10,5,'2012-02-16 11:16:28','2012-09-10 09:21:26','2012-02-16',100,8,350,349,7,8,0,'2012-09-10 09:21:26');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (381,1,2,'N° de Sinistre éditable','Dans le cas des dossiers DARVA ou GSICASS, les champs N° de sinistre restent éditables dans CPXT001','2012-02-16',33,5,5,4,31,5,4,'2012-02-16 15:51:07','2012-03-17 07:02:51','2012-02-16',100,0.5,null,381,1,2,0,'2012-03-09 11:25:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (382,1,4,'Suppression des documents HS (François)','> De : CPCOM/AMANT Francois 
> Envoyé : vendredi 17 février 2012 18:21
> À : Nathalie Seitz
> Cc : CPCOM/BOUREZ Vincent; CPCOM/PEREZ Pascal
> Objet : Problème suppression documents.
> 
> Bonjour,
> 
> Ce que je craignais vient d’être vérifié.
> Nous n’avons plus la suppression des documents lors de la remontée des dossiers.
> Rappel : Si un document d’un dossier est présent sur Hermès mais pas dans la liste des documents du flux Xml du dossier, alors c’est qu’il a été supprimé en agence et doit l’être sur Hermès.
> 
> Là ce n’est plus le cas.
> Il va falloir corriger au plus vite car je vais être obligé de remonter l’intégralité des dossiers depuis la mise en service de la V5.
> 
> Si vous tenez compte du fait que le temps d’import s’allonge (#318) de manière exponentielle (lancement de la tâche à 03 h 30 du matin / fin à 18 h 00) vous comprendrez que je me retrouve avec une bombe qui ne va pas tarder à m’exploser au nez.
> 
> Donc il faut voir le plus rapidement possible ce que vous pouvez faire pour réduire définitivement les temps de traitements et corriger la suppression des documents.
> 
> Je reste à votre disposition pour en discuter.
> 
> Vous remerciant par avance.
> 
> Cordialement,
> 
> François Amant
> ',null,39,5,null,4,39,4,5,'2012-02-20 12:05:01','2012-03-26 14:03:22','2012-02-20',100,null,null,382,1,2,0,'2012-03-23 14:22:06');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (383,2,2,'Ajout DT pour AR Mission Allianz','Ajouter les document types DT1060 et DT1061 à la  liste de DT éligibles aux AR mission Allianz

Procédure LP_MAIL du CPXS050 : source:trunk/svc/CPXS050.xml#L978',null,6,5,6,4,31,4,5,'2012-02-21 12:17:07','2012-03-05 12:18:14','2012-02-21',100,1,null,383,1,2,0,'2012-03-05 10:57:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (384,2,5,'Geolocalisation','S''assurer de mettre en place une base de géolocalisation efficace.
Modèle INSEE',null,null,5,10,4,33,10,8,'2012-02-21 18:42:19','2012-09-10 09:21:07','2012-02-29',100,null,374,365,8,11,0,'2012-09-10 09:21:07');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (386,2,4,'Recherche Dossier - ajouter la recherche sur le CODE client mineur (Candice)','De : Eurisk Siege/COLOMBEL Candice 
Envoyé : jeudi 23 février 2012 08:50
À : CPCOM/BOUREZ Vincent
Objet : RE: Correctifs Hermès

Bonjour Vincent,

Serait-il possible également de modifier la recherche au niveau du client mineur, à l’heure actuelle nous sommes obligés de rechercher par le nom du client mineur, ce qui pose problème étant donné que le nom d’un client mineur (par exemple AXA France) peut servir à plusieurs codes clients mineurs.

Merci 

Candice Colombel 
EURISK 
Tél : 01 30 78 18 06 
Fax : 01 30 78 18 23 
Courriel : candice.colombel@eurisk.fr
','2012-04-30',30,5,null,4,25,4,4,'2012-02-23 16:17:42','2012-05-04 13:45:43','2012-02-23',100,null,null,386,1,2,0,'2012-04-18 11:26:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (387,2,4,'OM - Ajouter le code agence dans l''écran de sélection d''une agence (Karin)','Lors de la création d''un OM, dans le bloc "choix agence", dans l''écran de la sélection d''une agence, il faut rajouter le code agence

!hermes_om_codeagence.png!','2012-04-30',36,5,null,4,25,4,5,'2012-02-24 10:12:29','2012-05-04 13:45:53','2012-02-24',100,null,null,387,1,2,0,'2012-04-18 17:28:58');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (388,2,4,'OM - Ne pas déclencher un mail d''affectation -systématiquement- (Karin)','Sur un OM affecté.
Après modification, si je clique sur le bouton "Affecter" alors un mail est envoyé
Après modification, si je clique sur "enregister" ou "enregister et fermer" alors pas de mail à envoyer.

','2012-04-30',36,5,null,4,25,4,6,'2012-02-24 10:19:56','2012-05-04 13:46:09','2012-02-24',100,null,null,388,1,2,0,'2012-04-23 16:01:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (389,2,5,'Serveur LAMP','Choisir, commander et configurer un serveur LAMP',null,null,5,10,4,null,10,3,'2012-02-24 16:03:31','2012-07-04 10:55:00','2012-02-24',100,null,null,389,1,4,0,'2012-07-04 10:55:00');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (390,2,5,'Choirir une configuration LAMP','*Environnement de développement*
- 1 seul seul pour 
-- Apache
-- Php
-- MySql
-- Centos??
',null,null,5,10,4,null,10,2,'2012-02-24 16:04:50','2012-07-04 10:54:39','2012-02-24',100,null,389,389,2,3,0,'2012-07-04 10:54:39');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (391,2,5,'Gestion des dossiers','Décrire le processus de gestion des dossiers',null,null,2,null,4,null,10,17,'2012-02-27 17:45:02','2013-04-26 09:47:59','2012-02-27',89,28,null,391,1,20,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (392,2,5,'Revoir la structure de données','- gestionnaires
- clients majeurs
- client
- dossier
',null,null,5,null,4,null,10,1,'2012-02-27 17:46:12','2013-04-17 14:52:38','2012-02-27',0,null,391,391,2,3,0,'2013-04-17 14:52:38');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (393,3,8,'Autre divers','',null,null,1,null,3,null,4,1,'2012-02-27 18:56:55','2012-04-23 17:57:53','2012-02-27',0,null,null,393,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (394,2,6,'Gsicass - OM - réception des OM vers CP2000','Récupération des Ordres de missions Gsicass par webservice pour dispatch vers agence CP2000

Voir Doc (sur serveur dev) Presta CDB technology

',null,40,5,11,4,90,4,20,'2012-02-28 11:46:01','2013-02-13 14:36:51',null,100,null,null,394,1,2,0,'2012-07-18 10:48:55');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (395,2,6,'Gsicass - envoi de rapport','A partir du signal CP2000, envoyer un message de type rapport au webservice Gsicass


Voir Doc (sur serveur dev) Presta CDB technology

lié à la demande CP2000 #272','2012-04-24',40,5,null,4,90,4,10,'2012-02-28 11:50:37','2013-02-13 14:37:58','2011-11-21',100,null,null,395,1,2,0,'2012-07-18 10:48:32');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (396,2,5,'Suppression de l''attribut DEPMETROPOLE','L''attribut depmetropole n''ayant plus son utilité, il faudra supprimer la mise à jour de ce champs dans la routine d''import',null,null,5,8,4,null,10,2,'2012-02-29 15:11:23','2012-09-10 09:20:40','2012-02-29',100,null,384,365,9,10,0,'2012-09-10 09:20:41');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (397,2,5,'Insertion de la table agencetype','',null,null,5,8,4,null,10,2,'2012-02-29 18:22:02','2012-09-10 09:20:27','2012-02-29',100,null,374,365,12,13,0,'2012-09-10 09:20:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (398,1,4,'délai d''ouverture des documents très long','délai d''ouverture des documents très long, en moyenne le délai est de 13 s comparé au délai moyen relevé de 2.5 s soit près de 5 fois plus de temps',null,39,5,null,4,25,4,4,'2012-03-01 09:54:30','2012-04-04 10:53:29','2012-03-01',100,null,null,398,1,2,0,'2012-03-23 14:29:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (399,2,5,'Migration des données','Migration des tables CPXAGE et CPXADR
- la routine générale est effectuée => un nouveau Bundle a été créé pour plus de clarté

Nom du bundle : ConvertBundle

Reste à faire
- Initialiser les objets migration
- Formater les données : champs à NULL et Champs de numéro de téléphone
- Créer une classe statique pour le département par défaut et le pays par défaut (cf telTypeBureau, telTypeFax ...)
- Accéder au bon code de département si l''on identifie la ville existante 
- Externaliser la liaison de l''agence type
- Mettre en place l''adresse type
- Proposer une routine de tests',null,null,2,10,4,null,10,10,'2012-03-01 18:37:49','2013-05-07 11:34:10',null,100,null,374,365,14,17,0,'2012-09-10 09:24:49');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (400,2,5,'Faire un Logueur de Migration','CF Monolog ...',null,null,5,10,4,null,10,2,'2012-03-01 18:41:36','2012-07-04 10:52:55','2012-03-01',100,16,null,400,1,2,0,'2012-07-04 10:52:55');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (401,1,2,'Case SENSIBLE ','Dossier 0830/308605 : impossible de décocher la case SENSIBLE (ereur lors de la saisie de l''enjeu).',null,null,5,5,4,46,6,8,'2012-03-02 09:02:39','2012-09-09 09:55:22','2012-03-02',100,null,null,401,1,2,0,'2012-08-01 16:32:14');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (402,2,5,'Mettre à jour le noms des champs normés dans code','CF CPWEB_champsbasededonnees_20120302_v10.docx (Z:\Projets\CPWeb\Général)
CF CPWebGeneral_v1.mwb (Z:\Projets\CPWeb\Général\SchemaBDD)',null,null,5,8,4,33,10,4,'2012-03-02 09:55:54','2012-09-10 09:25:00','2012-03-02',100,null,374,365,18,19,0,'2012-09-10 09:25:00');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (403,2,5,'Mettre à jour le document de modèle de données','Mettre à jour le document CPWEB_modelededonnées_20120221_v5.docx en fonction du modele de donnée .mwb',null,null,5,10,6,33,10,3,'2012-03-02 09:57:02','2012-09-10 09:25:19','2012-03-02',40,null,374,365,20,21,0,'2012-09-10 09:25:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (404,2,9,'Développent d''un utilitaire Print2PDF pour la conversion RTF ->  PDF','Utilitaire permettant de contourner les problèmes liés à l''utilisation de PDFCreator en ligne de commande.','2012-03-05',null,2,11,4,16,11,6,'2012-03-02 13:48:28','2012-07-19 09:33:28','2012-03-02',0,null,null,404,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (405,1,6,'Gsicass - OM - pb caractère illisible','Le problème a été rencontré sur l''OM 2012010617093500911','2012-03-07',40,5,11,4,null,4,3,'2012-03-02 14:51:12','2012-04-24 10:15:10','2012-03-05',100,5,null,405,1,2,0,'2012-04-24 10:15:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (406,2,6,'Gsicass - OM - PFT - blocage plateforme','Si type de mission amiable, CPXOMS.OMSPFT = ''T''','2012-03-21',40,5,11,4,null,4,7,'2012-03-02 14:57:33','2012-04-24 10:14:50','2012-03-21',100,4,null,406,1,2,0,'2012-04-24 10:14:50');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (407,2,6,'Gsicass - OM - PFT - mail d''alerte','Pour les ordres de missions "bloqué plateforme", un mail d’alerte sera généré à destination de la plateforme.

En objet : [NumeroOM] Assureur - typecontrat - contrat - sinistre - JourJ
Dans le corps : Assureur - Typecontrat - contrat - sinistre - JourJ - JourJ60 - Code Postal - Ville - Nom des bénéficiaires, description désordre.
','2012-03-26',40,5,11,4,null,4,10,'2012-03-02 14:59:50','2012-04-24 10:14:31','2012-03-26',100,8,null,407,1,2,0,'2012-04-24 10:14:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (408,2,6,'Gsicass - OM - PFT - Gérer les réponses mails ','La plateforme pourra débloquer un ordre de mission en répondant au mail d’alerte reçu. L’ordre de mission sera alors disponible pour l’agence

','2012-04-03',40,5,11,4,null,4,5,'2012-03-02 15:04:13','2012-04-24 10:13:12','2012-03-27',100,40,null,408,1,2,0,'2012-04-24 10:13:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (409,2,6,'Gsicass - OM - PFT - Etat hebdomadaire des OM PFT en attente','Un récapitulatif des ordres de missions plateforme en attente de traitement (déblocage ou création dossier) sera envoyé par mail chaque semaine à la plateforme.','2012-05-21',40,5,17,4,90,4,7,'2012-03-02 15:05:52','2013-02-13 11:45:29','2012-04-04',90,null,null,409,1,2,0,'2012-06-04 09:52:44');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (410,2,6,'Gsicass - OM - Reprise sur erreur','Avoir un fichier "run" par  OM

Voir y:\Progi\Projets\Gsicass\CR_TEST_290312_Script_DeX.doc','2012-05-23',40,5,11,4,90,4,15,'2012-03-02 15:10:57','2013-02-13 14:38:31','2012-04-11',100,40,null,410,1,2,0,'2012-07-18 10:47:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (411,3,6,'FLAG - Contacter FLAG','* Proposer et expliquer la disponibilité par FTP (au lieu de gravure CD)
* Donner les accès...
* Savoir depuis quand il veut les rapports','2012-06-10',28,5,11,4,95,4,8,'2012-03-02 15:16:57','2013-02-13 14:23:35','2012-06-06',100,null,null,411,1,2,0,'2012-05-16 12:13:14');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (412,1,2,'Separation des documents - Docdtenr absent = pb remontée Hermès','Lors de la création d''un document externe par l''intermédiaire de la fonction de séparation des documents (CPXL269), quand on renseigne CPXDOC, il faut valoriser DOCDTENR (Champs indispensable pour traitement de la remontée Hermès)

','2012-03-06',5,5,5,6,31,4,5,'2012-03-06 11:07:50','2012-03-17 07:03:31','2012-03-06',100,null,null,412,1,2,0,'2012-03-09 11:15:28');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (413,2,6,'Esker - Suivi SLA','Chaque début de mois ou à la demande, faire un etat (feuille excel) qui détail jour par jour, agence par agence les données suivantes :
* Le nombre d''envoi chez Flydoc
* le détail en Eco/Urgent/Recommandé
* le nb d''envoi traité à jour J
* le nb en attente de traitement
* le nombre de hors SLA officiel (mail esker)

+ Totaux :décompte global pour le mois et pour toutes les agences

Rapport à envoyer aux responsables des services : Achat/Support/Développement
',null,28,1,11,4,52,4,2,'2012-03-12 16:57:31','2013-04-26 10:32:34',null,0,null,null,413,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (414,1,6,'Dump SQL DataExchanger','Le dump n''est pas correct :
# il manque l''instruction sur la création de la database
# DataExchanger ajoute inutilement la ligne de commande en entete','2012-03-13',31,5,4,4,null,4,1,'2012-03-13 13:47:38','2012-03-13 13:51:14','2012-03-13',100,2,null,414,1,2,0,'2012-03-13 13:51:14');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (415,3,2,'Test CP2000 config PC Scan sur Windows 64b','Paramétrer le PC Scan pour utilisation sur le serveur 10.31.2.39

A tester :

* Création de dossier
* L''ouverture d''un document PDF
* L''envoi par mail d''un document
* Récupérer une adresse dans l''annuaire groupe dans l''envoi mail (Accès LDAP)
* L''envoi par mail de plusieurs documents dont document d''origine word (interne) / document pdf (externe)
* L''envoi vers l''imprimante (sensible)
* Le scan (simple, multiple, recto/verso)
* La séparation de document
* Création d''une NH
* Tester un accès par lien vers Hermes et Darva

Attention :

* vérifier les nouveaux mails ne fonctionne pas, voir #292

Créer un rapport de test. (Modele sur \\\\10.31.2.37\\Progi\\Projets\\CPCOM-v1.1.0.docx).
Il doit comporter :

* L''environnement système ( Windows 64bit , messagerie ?) 
* Les logiciels Pack Office ????, Adobe Acrobat Reader ?, CP2000 V1.9.4)
* Récap des tests + résultats (Garder les références dossiers)','2012-03-15',null,5,5,4,null,4,1,'2012-03-13 15:28:05','2012-04-25 09:57:58','2012-03-13',100,8,null,415,1,2,0,'2012-04-25 09:57:58');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (416,1,4,'Probleme accès dossier Allianz (Corse absente) - Paul Boyer','# Sur Hermès l’administrateur (eurisk) trouve 216 dossiers en Corse (code postal =20%) depuis 01/01/2011 pour Allianz. (code client = LAUN-AGF).
https://hermes.eurisk.fr/hermes/jsp/index.jsp?login=eursup&pwd=d6z9i3
Là tout va Bien.

# Le client ALLIANZ, F BOYER n’en voit  aucun… ????
https://hermes.eurisk.fr/hermes/jsp/index.jsp?login=agf&pwd=qi!46d1

Il devrait voir les mêmes 216 dossiers que ci-dessus…

J’aurai tendance à en déduire que le regroupement de clients n’a pas l’air de fonctionner… sur la V5.
',null,2,5,4,5,null,4,4,'2012-03-14 12:11:09','2012-03-19 11:45:19','2012-03-14',100,null,null,416,1,2,0,'2012-03-19 11:45:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (417,2,2,'Ajout expert - permettre de saisir une date d''entrée dans le futur','CPXG104 

Permettre de saisir une date d''entrée dans le futur lors de la création d''une fiche expert.

','2012-03-19',48,5,5,4,38,4,2,'2012-03-15 16:40:46','2012-04-01 16:03:36','2012-03-19',100,2,null,417,1,2,0,'2012-04-01 16:03:36');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (418,2,5,'TRI DU CODE','Actuellement, il y a énormément de code qui n''est pas utilisé, notemment dans les entités et les modèles
Il faudrait mettre temporairement les fichiers de cotés.
Effectivement, on ne sait plus ce qui est a jour (cadré sur la nouvelle structure) et ce qui est obsolète (cadré sur l''ancienne structure)
Afin de ne pas perdre uneinfomation pertinente pour la suite des développemnts, préserver ces fichiers dans un répertoire de sauvegarde qui sera également mis sous SVN',null,null,6,8,4,33,10,1,'2012-03-19 15:06:11','2012-03-19 15:29:27','2012-03-19',0,null,null,418,1,2,0,'2012-03-19 15:29:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (422,2,6,'Gsicass - OM - Gestion du bloc pièce jointe dans les messages MDO01, MDO07, MDO08, MDO09','Les messages MDO01, MDO07, MDO08, MDO09 contiennent des champs (facultatifs) :

Libelle de la pièce jointe
Date de constitution de la pièce jointe

Les messages MDO07, MDO08, MDO09 sont émis par nous vers APRIA.
Prendre en compte cette évolution d''intégration des pièces jointes.
Voir script gsicassenvoirapport.vsl','2012-03-28',null,5,null,4,null,11,5,'2012-03-21 11:23:09','2012-04-24 10:12:29','2012-03-21',100,null,null,422,1,2,0,'2012-04-24 10:12:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (423,2,2,'GSICASS - tracer le detail du contenu des pièces jointes','Demande de Candice :

Garder une trace de la sélection des documents comme pour les envois Flydoc.
(--> valoriser CPXCHRONODOC)

note : dans l''écran des envois, il manque le radio bouton "EDI GSICASS" + les envois GSICASS sont présents sans libéllé





',null,27,5,5,6,38,4,3,'2012-03-22 15:34:48','2012-04-01 16:04:12','2012-03-22',100,null,null,423,1,2,0,'2012-03-28 12:01:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (424,2,2,'Liste des documents d''une police','Il faut afficher dans l''écran CPXL100 les documents indexé en 6.02 communs à la police.
Aujourd''hui c''est limité au numero de G, donc on ne voit pas tous les documents attendus.

Même problème sur l''écran d''envoi des documents CPXL010B

exemple sur le 45 : police numero AD589597','2013-01-28',5,5,5,6,72,6,15,'2012-03-22 17:12:33','2013-02-10 09:34:07','2013-01-25',60,16,null,424,1,2,0,'2013-01-30 14:03:55');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (425,2,2,'NH HT - suspension de la TVA pour les factures à l''exportation','h2. Objectif

Permettre de facturer HT uniquement.
Les montants de la note d''honoraire doivent avoir un HT et TTC équivalent.

h2. A faire :

h3. Gestion des clients

Ajouter une information sur la fiche client CPXCLI : Suspension de la TVA autorisée (case à cocher).
Prendre en compte la modification dans le transfert du paramétrage.

Le numéro de TVA intracommunautaire existe déjà dans le paramétrage client. Il se trouve pour l''instant dans la fiche du client dans la "référence client" 

Le numéro de TVA intracommunautaire est obligatoire si la case "suspension TVA autorisée" est cochée.

h3. Gestion du dossier

Ajouter un flag au dossier (DOSFACTHT.CPXDOS)
Une case à cocher afin d''initialiser cette information doit se trouver à la création de dossier et sur l''onglet d''enrichissement du dossier (pour permettre une modification). Seulement si le client sélectionné fait parti de la liste des clients identifiés pour cette fonction (cf paragraphe précedent sur modif CPXCLI).
Prendre en compte la modification dans le transfert du dossier.

h3. Ecran Note d''honoraire

Lors de la valorisation de la note d''honoraire, vérifier le flag du dossier :
si False alors, NH normale, si true, alors NH spéciale : calcul des montants sans tenir compte de la TVA, HT = TTC.
Dans le cas d''une NH spéciale, il n''y a que 2 lignes à créer au lieu de 3 minimum sur les exports comptable du chiffre d''affaire (CPXLIGCA)

h3. Document NH

Rajouter au document word des champs de fusion :
En haut de la NH : TVA INTRACOMM : <Numero>
En dessous du total général : "FACTURE A L''EXPORTATION, HORS DE LA ZONE INTRACOMMUNAUTAIRE, ETABLIE EN SUSPENSION DE LA TVA"


',null,26,1,null,5,27,4,11,'2012-03-23 11:01:22','2013-07-04 22:27:46',null,0,40,null,425,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (426,2,2,'Les intervenants : Ajouter Case Marché vu/soldé+ dans le répertoire le type de Contrat ','Dans le répertoire des intervenants dossier, pour une entreprise on retrouve les données mises dans l''écran CPXG005 càd ses coordonnées, son rôle dans l''opération et son Assurance. Seul le type du Contrat : Locateur d''ouvrage ou sous-traitant (etc..) n''est pas rapatrié.

Il faut donc ajouter cette notion dans le répertoire et nous permettre de faire un paramétrage dans les rapports. (actuellement l''assistante est obligée de saisir manuellement cette donnée dans son rapport, si l''expert ne le dicte pas, elle est obligée de quitter son document Word, pour consulter l''écran CPXG005!!)

Dans le même esprit, on pourrait enrichir l''écran CPXG005 de la case à cocher Marché soldé OUI avec transfert dans répertoire et paramétrage dans le rapport',null,35,1,null,6,null,6,6,'2012-03-23 14:02:57','2012-12-17 13:52:10',null,0,null,null,426,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (427,2,6,'CP2000 - Controle des transferts','h2. Contrôle Traitements CP2000 - Base Référence

Le traitement se lance du lundi au vendredi à 20h30. Contrôler à partir de 23h.

Se connecter au 10.78.1.3 1317.

<pre><code class="sql">select agecod, max(convert_date(tefdtdeb)) as dtjour, teferr from cpxtef where teferr is not null and teftyp = ''PARB'' and convert_date(tefdtdeb) = ''2012-03-26'' group by agecod, tefdtdeb, teferr order by tefdtdeb desc;</code></pre>

Pour le Jour J, 
* Si dernier enregistrement, agecod = 9999 alors, c''est bon, sinon lever une alerte mail
* Si autre enregistrement que agecod = 9999 alors, lever une alerte mail avec code erreur.


h2. Contrôle Traitements CP2000 - Base Production

Le traitement se lance du lundi au vendredi à 22h45. Il se termine vers 2-3h du matin. à Contrôler à partir de 5h du matin.

Se connecter au 10.78.1.8 1313

<pre><code class="sql">select agecod, max(convert_date(tefdtdeb)) as dtjour, teferr from cpxtef where teferr is not null and teftyp = ''DOSS'' and tefdtdeb >= ''2012-03-21 12:00:00'' and tefdtdeb <= ''2012-03-22 12:00:00'' group by agecod, tefdtdeb, teferr order by tefdtdeb desc;</code></pre>

Pour la période inspectée (de 12h la veille à 12h le jour même)

* Si dernier enregistrement, agecod = 9999 alors, c''est bon, sinon lever une alerte mail
* Si autre enregistrement que agecod = 9999 alors, lever une alerte mail avec code erreur.

h2. Alerte mail 

Si alerte mail levée :

Objet = "[CP2000] Controle transfert <<Date>>"

mentionner dans le corps :
* Le type de transfert (production, référence)
* La ou les agence(s) concernée(s)
* La description des erreurs

Destinataires : 
* Responsable Dev
* Contrôle de gestion',null,28,5,17,4,100,4,9,'2012-03-26 12:53:46','2013-02-13 11:46:35',null,100,24,null,427,1,2,0,'2012-11-07 14:44:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (428,1,4,'Autres pièces - tri par date décroissant','Les documents présents dans l''onglet autres pièces doivent être classés du plus récent au plus ancien
(date de validation)

CiteSI - ticket 262',null,39,6,null,4,null,4,3,'2012-03-26 14:25:34','2012-05-24 10:44:48',null,0,null,null,428,1,2,0,'2012-05-24 10:44:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (429,1,2,'GSICASS - Création d''un dossier à partir d''un OM PFT','sur enregistrement de la mission (Mission PFT) , le numero de G est absent, l''etat de dossier est absent. Voir test 0350/141633.',null,4,6,5,6,null,4,5,'2012-03-27 18:46:24','2012-06-22 10:17:07',null,0,null,null,429,1,2,0,'2012-06-22 10:17:07');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (430,2,2,'GSICASS - Création d''un dossier  : Adresse sinistre','Sur récupération de l''adresse du sinistre à partir de l''OM, voici comment utiliser les données :

ligne 1 de l''OM --> DOSADR2
ligne 2-3-4 de l''OM --> DOSADR3 (Limiter à 32 caractères)','2012-03-29',4,5,5,6,38,4,6,'2012-03-27 18:51:51','2012-04-01 16:04:53','2012-03-28',100,1,null,430,1,2,0,'2012-04-01 16:04:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (431,1,2,'Gsicass - Envoi rapport - Donner la possibilité de créer 1 seule pièce jointe','CPXL010B - Dans le cas d''un envoi EDI GSICASS, limiter la création de pièce jointe à 1 seule possible

--> Supprimer les boutons "Nouvelle piece jointe" et "supprimer piece jointes"

CPXG123 - Bloc pièce jointe : sur double clic, donner la possibilité d''ouvrir et consulter le document','2012-03-28',27,5,5,4,38,4,6,'2012-03-27 19:03:07','2012-04-01 16:02:59','2012-03-28',100,2,null,431,1,2,0,'2012-03-28 17:52:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (432,2,2,'GSICASS - interdire l''envoi de rapport si email gestionnaire manquant','CPXG123 - A l''envoi de rapport GSICASS, controler si le gestionnaire qui missionne a au moins 1 email perso ou 1 email service de renseigné

Note : créer une local proc séparée pour les contrôles parce qu''à l''avenir il y aura surement d''autres règles à prendre en compte',null,27,5,5,6,38,4,5,'2012-03-28 11:31:06','2012-04-01 16:05:16','2012-03-28',100,2,null,432,1,2,0,'2012-04-01 16:05:16');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (433,2,2,'Allianz - envoi rapport automatique par email ','h2. A faire

Sur validation d''un rapport, si le dossier répond au critères suivants :

|_. Code Client| ALLIANZ et LAUN-AGF |
|_. Contexte de gestion | gestion classique + gestion déléguée |
|_. Type document | Liste plus bas |
|_. Destinataire | Prendre le 1er destinataire validé, si aucun document avec destinataire, prendre le tronc commun |
|_. ??? | ??? |


Créer un mail pour envoi immédiat + archivage dans le dossier (CPXDOC) et trace dans la liste des envois (CPXCHRONO/CPXCHRONODOC)

|_. Expéditeur | email site de gestion |
|_. Destinataire | RAPPDIC@ALLIANZ.FR |
|_. Objet | NS:"NUMSINISTRE"|
|_. Corps du message | Si gestion classique = "Rapport + lettre d''accompagnement" |
|_.  | Si gestion déléguée = "Rapport"  |
|_. Pièce jointe | gestion classique = Rapport + lettre d''accompagnement en PDF|
|_.  | gestion déléguée = Rapport uniquement (en PDF) |



h2. Source de la demande

> De : ITHURBIDE, Virginie (Allianz FR) 
> Envoyé : lundi 5 mars 2012 09:37
> À : Eurisk Siege/BOYER Paul; Eurisk Siège/CHARTIER Sylvie
> Objet : TR: Sécurisation des données par l''Expert et par ALLIANZ
> Importance : Haute
> Bonjour, 
>  
> Comme nous vous l''avions précisé lors de la mise en place de la dématérialisation des flux entre nos Sociétés, nous souhaitons sécuriser les données échangées. 
>  
> D''autre part, une évolution vous sera demandée de façon à ce que les rapports soient archivés et sécurisés par ALLIANZ. 
>  
> Il vous appartiendra alors d''adresser des mails avec en pièce jointe vos rapports au format PDF à notre plate-forme d''archivage automatique. 
> La normalisation de l''objet de mail sera la suivante NS: suivi du n° de sinistre (exemple NS:C1240004567). Attention, il n''y a pas d''espace entre "NS" et ":" et le n° de sinistre.
> Tous vos rapports déposés seront ainsi sauvegardés puisque jusqu''à présent nous archivons uniquement des liens d''accès à votre portail. 
> Cela ne remet pas en cause les push mail que vous nous adressez chaque jour. Les nouveaux envois qui vous sont demandés seront à transmettre en parallèle des push mail habituels. 
>  
> Merci de m''indiquer à quelle date vous serez en mesure de répondre à cette dernière requête sachant que nous envisageons, de notre côté, une mise en place pour avril-mai. 
>  
> Restant dans l''attente de vous lire, 
>  
> Bien cordialement. 
>  
>
> Virginie ITHURBIDE
> Responsable du Service Expertise et Support
> Direction Indemnisation Construction
> Case Courrier : 7C6
> Adresse : Immeuble Acacia 13-27 Esplanade du Général de Gaulle 92076 Paris la Défense Cedex
> E.mail : virginie.ithurbide@allianz.fr
> Tél. : 0978 978 072 Code d''accès direct : 3931 ; Fax : 01.30.68.70.72

=======================================================================================================

> De : Eurisk Siege/TINCQ Anne-Marie [mailto:Anne-Marie.TINCQ@eurisk.fr]
> Envoyé : mardi 27 mars 2012 16:07
> À : ITHURBIDE, Virginie (Allianz en France) Cc : Eurisk Siege/BOYER Paul; Eurisk Siège/CHARTIER Sylvie; CPCOM/AMANT Francois; Eurisk 
> Siege/SANTONI Dominique Objet : TR: Sécurisation des données par l''Expert et par ALLIANZ
> 
> Bonjour,
> 
> Suite à votre mail,  Nous avons quelques questions :
> - Rapport : que comprend le rapport ? la lettre d’accompagnement et le rapport ?
> => effectivement, en gestion classique, il convient de nous transmettre le rapport + la lettre d''accompagnement.
> 
> - Les envois de rapports concernent aussi bien la gestion classique que la gestion déléguée ?
> => oui 
> 
> - Si  gestion déléguée, que comprend l’envoi ?
> => Le rapport + lettre de position ?  uniquement le rapport
> 
> - Adresse mail du service Archivage ?
> => RAPPDIC@ALLIANZ.FR<mailto:RAPPDIC@ALLIANZ.FR>
> 
> - Déclenchement du mail immédiatement ou en différé ?
> => immédiat
> 
> En vous remerciant   pour votre réponse
> 
> Bien cordialement
> 
> [cid:581110915@06042012-0A08]
> Anne-Marie Tincq
> tél : 01 30 78 18 21
> anne-marie.tincq@eurisk.fr

========================================================================================================

2 Client majeur : ALLIANZ et LAUN-AGF

Les documents : 

|_. EURISK |_. ESTELLON |_. PASQUALINI |
|_. DO/PUC Gestion Classique |
| DT003 + DT003_ | ES003 + ES003_ | CO003 + CO003_ |
| DT604 + DT604_ | ES604 + ES604_ | CO604 + CO604_ |
| DT058 + DT058_ | ES058 + ES058_ | CO058 + CO058_ |
| DT905_ | ES905_ | CO905_ |
| DT059 + DT059_ | ES059 + ES059_ | CO059 + CO059_ |
| DT061 + DT061_ | ES061 + ES061_ | CO061 + CO061_ |
| DT548 + DT548_ | ES548 + ES548_ | CO548 + CO548_ |
|_. DO/PUC Gestion déléguée |
| DT776 | ES776 | CO776 |
| DT789 | ES789 | CO789 |
| DT792 | ES792 | CO792 |
| DT793 | ES793 | CO793 |
|_. GLOBALE CHANTIER - gestion classique |
| DT832 + DT832_ | Néant | Néant |
| DT542 + DT542_ | Néant | Néant |
| DT546 + DT546_ | Néant | Néant |
|_. RCA Gestion Classique |
| DT837 + DT837_ | ES837 + ES837_ | CO837 + CO837_ |
| DT222 + DT222_ | ES222 + ES222_ | CO222 + CO222_ |
|_. RCA Gestion Déléguée |
| DT894 | ES894 | CO894 |
| DT852 | ES852 | CO852 |
|_. RCDA Gestion Classique |
| DT204 + DT204_ | ES204 + ES204_ | CO204 + CO204_ |
| DT729 | ES729 | CO729 |
|_. RCDA Gestion Déléguée |
| DT875 | ES875 | CO875 |
| DT851 | ES851 | CO851 |
|_. TRCA |
| DT544 + DT544_ | Néant | CO544 + CO544_ |
|_. JUDICIAIRE DO/PUC/RCD/RC/TRC |
| DT226 + DT226_ | ES226 + ES226_ | CO226 + CO226_ |
| DT813 + DT813_ | ES813 + ES813_ | CO813 + CO813_ |
| DT905 |	
|_. ENQUETE PFT - Gestion déléguée |
| DT395 | Néant | Néant |','2012-04-15',6,5,5,5,42,4,86,'2012-03-28 11:59:04','2012-06-17 11:35:54','2012-04-09',100,40,null,433,1,2,0,'2012-06-15 11:22:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (434,2,2,'Stocker le mail perso dans la fiche Prestataire','* Ajouter un nouveau champs dans CPXPRT : PRTMAILPERSO (Email perso)
* Rendre accessible la donnée dans CPXG104
* Renommer le libellé actuel "Email" --> "Email Agence"
* Prendre en compte le changement dans le transfert du paramétrage.
* Préparer un script SQL pour valoriser les données existantes


PS : utile pour la migration CPWEB',null,15,5,5,4,43,4,12,'2012-03-28 13:57:32','2012-06-24 17:23:53',null,100,null,null,434,1,2,0,'2012-06-22 16:18:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (435,1,2,'Problème avec l''envoi de mail auto "Fort Enjeu" sur d''ancien dossier en consultation','Le mail automatique "Fort Enjeu" se déclenche systématiquement sans la possibilité de mettre le Flag du dossier à jour comme quoi le mail a été envoyé, ce qui fait qu''à chaque visite sur le dommage en "consultation", un mail part.',null,9,5,5,4,38,5,2,'2012-03-28 15:44:19','2012-04-01 16:02:29','2012-03-28',100,1,null,435,1,2,0,'2012-04-01 16:02:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (436,2,6,'GSICASS - OM  Ajouter les pièces jointes dans le mail de blocage plateforme','Ajouter les pièces jointes dans le mail de blocage plateforme','2012-04-03',40,5,null,4,null,11,8,'2012-03-30 12:07:24','2012-09-17 09:45:57','2012-04-02',100,16,null,436,1,2,0,'2012-04-24 10:10:47');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (437,2,2,'Liste des documents techniques (écran CPXL100) dans rapport','Serait-il possible d''insérer la liste des documents techniques dans un rapport ??',null,null,1,null,4,null,6,1,'2012-04-02 09:03:40','2012-06-27 16:20:18','2012-04-02',0,null,null,437,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (438,1,2,'Creation operation+mission sur police par Aliment HS','> De : Eurisk Siege/COLOMBEL Candice 
> Envoyé : lundi 2 avril 2012 14:29
> À : CPCOM/BOUREZ Vincent; CPCOM/GONZALES Nicolas
> Cc : Eurisk Siege/SANTONI Dominique
> Objet : Problème d''enregistrement de mission
> Importance : Haute
> 
> 
> Bonjour,
> 
> Sur les agences 0280 et 0450, lorsque l’on créé une nouvelle opération et mission sur une police par aliment et que l’on clique sur « Enregistrer sans quitter » pour avoir le numéro de dossier rien ne se passe.
> 
> En gros aucune génération de numéro, elles sont obligées de quitter et enregistrer et de rechercher le nouveau dossier par le numéro de sinistre pour faire l’enrichissement.
> 
> Si c’est pas clair, me contacter.
> 
> Bonne journée
> 
> Candice Colombel 
> EURISK 
> Tél : 01 30 78 18 06 
> Fax : 01 30 78 18 23 
> Courriel : candice.colombel@eurisk.fr','2012-04-02',16,5,5,4,44,4,2,'2012-04-02 14:45:32','2012-04-02 15:09:08','2012-04-02',100,1,null,438,1,2,0,'2012-04-02 15:09:08');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (439,2,8,'Modifier la procédure pour n''envoyer que les xml sans les fichiers','Afin de mettre a jour les données sans nécessairement refaire transiter les fichiers pdf joints déjà existants','2012-04-03',null,5,7,4,null,4,1,'2012-04-03 10:35:49','2012-04-03 12:26:11','2012-04-03',100,null,null,439,1,2,0,'2012-04-03 12:26:11');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (440,1,4,'Etat NH par expert rédacteur','Angélique - Dijon

Le montant des NH par expert rédacteur est faux sur hermès.

!hermes_etatnhparexpertredacteur.png!',null,58,5,null,4,45,4,7,'2012-04-03 15:16:55','2012-08-06 14:24:34','2012-04-03',0,null,null,440,1,2,0,'2012-08-06 14:24:34');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (441,2,2,'Compression des photos','h2. Objectif

Réduire systématiquement tout document photo entrant dans CP2000 afin que les photos soit directement exploitable dans un rapport comme dans l''exemple en PJ attachment:rapport_avec_photo.pdf

h2. A faire

h3. Module de réduction de photo

* Appel en ligne de commande d''un script qui va réduire une photo
* Tester, essayer, trouver, documenter la bonne taille des photos à paramétrer

h3. CP2000

* Sur intégration document (CPXL250)
* Sur intégration des PJ d''un mail (CPXS250)

Si le document = jpg ou jpeg, alors appel à procédure de réduction de la photo

',null,5,1,null,6,null,4,10,'2012-04-04 09:37:38','2013-07-04 22:27:46',null,0,40,null,441,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (442,2,6,'CP2000 - Alerte mail ajout de document pour expert rédacteur','h2. Objectif

Tous les jours, envoyer un état par mail aux experts rédacteurs avec la liste des documents ajouté à leurs dossiers CP2000.

h2. SQL

<pre><code class="SQL">
select cpxdos.agecod, cpxdos.dosnum, opeg, prtnom, docorigine, doclib, convert_date(docdtcre) as DateDoc, docdtenr as DateScan, ''\\\\'' + agenet + ''\\agce\\cp2000\\'' + docfic as Path
from cpxdoc, cpxope, cpxprt, cpxdos, cpxage
where dociprov = ''E'' and docdtenr = ''2012-03-26''
and cpxdos.dosnum = cpxdoc.dosnum and cpxdos.agecod = cpxdoc.agecod
and cpxdos.opeid = cpxope.opeid
and cpxdos.prtcod1 = cpxprt.prtcod
and cpxdos.agecod = cpxage.agecod
</pre></code>

note : manque le mail de l''expert rédacteur (qui sera ajouté dans CPXPRT, voir #434).',null,28,5,null,5,85,4,7,'2012-04-04 10:49:42','2013-04-02 15:42:36',null,100,24,null,442,1,2,0,'2013-04-02 15:42:36');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (443,3,6,'NOE - Echange Ardi - NOE','Echange des données entre Ardi et Noe','2012-08-20',59,2,null,4,93,11,21,'2012-04-06 13:30:19','2013-04-30 11:41:11','2012-04-05',50,null,null,443,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (444,1,2,'GSICASS - Correction création mission','Problème lors de l''enregistrement à la création d''une nouvelle mission à partir d''un OM GSICASS, les PJ de l''OM ne sont pas rattachées au dossier.

Ensuite, donner la possibilité au profil "PFT" de pouvoir décocher la case PFT quand on crée une nouvelle mission à partir d''un OM GSICASS en se tagant "PFT" dans l''écran "Ordre de mission"

Ecrans à modifier :
 - CPXG001
 - CPXG002',null,4,5,5,4,42,5,8,'2012-04-06 15:33:07','2012-06-17 11:32:34','2012-04-06',100,null,null,444,1,2,0,'2012-06-01 14:36:16');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (445,2,6,'GSICASS - Le fichier XML OM ne doit pas contenir les pièces jontes','Le fichier XML OM ne doit pas contenir les pièces jontes
Effacer : <Fichier>....</Fichier>',null,40,5,null,4,null,11,3,'2012-04-06 16:17:49','2012-09-17 09:46:14','2012-04-06',100,null,null,445,1,2,0,'2012-04-24 10:10:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (446,2,5,'Supprimer tous les champs de ID des objet quand il s''agit d''une liaison','*Exemple:*
class UserRoleAgence 
    private $user_usrid;
    private $role_roleid;  
    private $agence_ageid;
Ces attributs sont inutiles ... il faut les supprimer.
Supprimer également les Getter et les Setter de l''objet et adapter le code.

J''attends également une évaluation du temps sur cette partie...',null,null,5,8,5,null,10,3,'2012-04-11 08:43:15','2012-09-10 09:20:02','2012-04-11',100,null,369,368,3,4,0,'2012-09-10 09:20:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (447,2,5,'Permettre à un adminsitrateur de gérer les utilisateurs','1 - Faire un document de spécification e amont du code :
    * Liste des pages utiles à la gestions des utilisateurs
    * Maquette d''écran
    * Descriptif des actions possibles et actions sur la base
2 - Réunion de validation
3 - Code 
    3.1 Coder la partie fonctionelle sans design ni effet
    3.2 Intégrer les feuilles de syle de formulaire
    3.3 Tester
Attention: pas de javascript à ce stade du développement

Je ssouhaiterais un estimé du temps prévu pour chaque tache',null,null,5,8,5,null,10,8,'2012-04-11 08:46:50','2012-09-10 09:19:47','2012-04-11',100,48,371,365,27,28,0,'2012-09-10 09:19:47');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (448,1,6,'GSICASS - OM  - La copie des pièces jointes n''est pas réalisée ','Prb La copie des pièces jointes n''est pas réalisée ',null,40,5,null,4,null,11,4,'2012-04-11 09:30:11','2012-09-17 09:47:06','2012-04-10',100,null,null,448,1,2,0,'2012-04-24 10:10:05');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (449,2,5,'Banniere de droits','Gerer le droit maximum (hiérarchie) par Agence.
Lorsque l''on change d''agence, il faut mettre à jour la liste des roles losque l''on change d''agence dans la baniere. ',null,22,5,8,6,null,10,2,'2012-04-11 09:32:14','2012-09-10 08:46:53','2012-04-11',100,null,366,365,3,4,0,'2012-09-10 08:46:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (450,1,6,'GSICASS  - OM - Prb pièce jointe trop volumineuse pour envoyer mail de blocage plateforme','La limite acceptable est 10Mo (cf Christophe) or le fichier joint envoyé par ACS est de 12Mo.
Action:
-Clarifier avec Apria si des limites sont gérées
Ou
Ne pas mettre la pièce jointe dans le mail mais un lien qui pointera sur le document qui sera placé sur un serveur.- 

La solution envisagée (non implémentée) est de donner la liste des pièces jointes dans le mail de blocage si ces dernières sont trop volumineuse. L''opératrice de la plateforme se connectera au serveur agence pour visualiser les pièces.',null,40,5,null,4,90,11,9,'2012-04-11 10:26:36','2013-04-02 16:32:00','2012-04-11',90,null,null,450,1,2,0,'2013-04-02 16:32:00');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (451,2,2,'Nouveau paramétrage Nom et n° Tél Assistante','h2. Objectif

Pouvoir insérer dans un document le nom et n° de tel d''une assistante. 
Dans un premier temps seulement dans les convocations.  

h2. Source

Ci-après la demande de M. QUENIN :

> Dans le cadre de l''organisation des 2 agences ,les assistantes sont demandeuse de faire figurer leur nom et leur numéro de ligne directe sur les documents envoyés et ceci afin de faciliter la gestion des appels ,gestion qui est de plus en plus lourde .
> Cela peut t''il être envisager au moins a titre d''essais sur Annecy et Amberieu avant éventuellement de généraliser .
>
> Bonne réception 
> P.QUENIN

h2. A faire

Script SQL CPXCHP pour ajouter le téléphone','2012-04-17',15,5,4,4,42,6,7,'2012-04-16 15:55:57','2012-04-19 12:13:24','2012-04-16',60,4,null,451,1,2,0,'2012-04-19 12:13:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (452,1,6,'Correction sur mise à jour CPXDOC + Correction si pas de PJ','L''attribut dtyref doit être mis à EX039, de plus un OM sans PJ génére une erreur sur script VB (calculer taille) et sur envoyer avec PJ',null,40,5,11,4,null,11,3,'2012-04-17 15:47:50','2012-04-24 10:09:11','2012-04-17',100,5,null,452,1,2,0,'2012-04-24 10:09:11');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (453,1,6,'GSICASS: Prb sur insertion CPXDOC certaines valeurs doivent être mieux renseignées','1. On doit mieux renseigner les documents joints (ne concerne pas le document XML de l''OM) :
                A partir du code DTYREF = ''EX039'', toutes les PJ ajoutées dans CPXDOC doivent hériter des infos de CPXDTY suivantes : 
                select DTYREF, DTYLIB,NADCOD,DTYIND,DTYIHE,DTYPRU,DTYPRU, DTYQLTE from cpxdty where dtyref = ''EX039'';
                
2. DOCORIGINE On attends le nom de l''émetteur


3. Afin de ne pas rendre visible la PJ sur le message XML de l''OM dans le docs du dossier CP2000, il faut, uniquement pour le fichier XML, valoriser DOCIPROV = ''O''
',null,40,5,null,4,null,11,4,'2012-04-18 14:44:11','2012-09-17 09:49:15','2012-04-18',100,5,null,453,1,2,0,'2012-04-24 10:08:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (454,1,2,'Caractère problématique dans l''import d''un mail','h2. Description du problème 

Sur certains imports de mails, un caractère illisible est inséré dans la base de donnée, dans le champs des destinataires/expéditeurs du document.

!pb_hieroglyphe.png!

Cela cause un plantage dans l''import d''Hermes.

h2. Todo

Trouver une méthode pour éviter d''insérer les "carrés" dans la base Solid de l''agence CP2000','2012-04-20',6,5,4,6,42,4,4,'2012-04-18 14:50:28','2012-04-18 19:17:13','2012-04-18',100,8,null,454,1,2,0,'2012-04-18 18:57:15');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (455,3,10,'Entretiens d''embauche','',null,null,4,null,4,null,4,1,'2012-04-19 11:26:33','2012-05-02 10:55:01','2012-04-19',0,null,null,455,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (456,2,2,'GSICASS - modification libellé écran CPXG123','Voir pièce jointe
 
 ','2012-04-20',27,5,5,4,42,6,5,'2012-04-19 11:35:13','2012-06-17 11:36:16','2012-04-19',100,1,null,456,1,2,0,'2012-04-25 14:46:06');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (457,2,2,'Plus possible de sélectionner un assureur inactif dans les destinataires','h2. Ergonomie de l''écran

Avant :
!cpxl077.png!

Après :

!cpxl077_apres.png!

h2. Règle de gestion

A l''affichage si l''enregistrement est inactif, ne pas afficher la case à cocher','2012-05-16',35,5,5,4,46,4,18,'2012-04-19 15:44:41','2012-09-09 09:57:23','2012-05-16',100,8,null,457,1,2,0,'2012-08-09 11:54:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (458,3,10,'Divers','',null,null,4,null,4,null,4,1,'2012-04-20 16:26:51','2012-04-25 12:21:55',null,0,null,null,458,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (459,2,8,'Transférer les nouveaux c hamps Hermès','La procédure doit être mise à jour suite aux souhaits d''enrichissement d''Hermès, cf #1
',null,null,5,7,4,null,4,1,'2012-04-23 10:30:01','2012-05-04 13:50:56',null,100,null,null,459,1,2,0,'2012-05-04 13:50:56');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (460,1,9,'Prb a l''exécution de print2pdf via le service DX','Des exceptions sont levées par Print2PDF lorsque ce dernier est exécuté via le service d''ordonancement de DeX',null,null,5,null,4,106,11,3,'2012-04-23 16:31:33','2013-02-13 14:47:08','2012-04-23',100,16,null,460,1,2,0,'2013-02-13 14:47:08');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (461,1,2,'Ne pas afficher l''OM xml dans les documents internes','Toulon - l''OM XML de gsicass apparait dans les documents internes (ex: 311593)

Il ne faut pas l''afficher','2012-04-23',5,5,4,4,42,4,3,'2012-04-23 16:38:19','2012-06-17 11:32:51','2012-04-23',100,1,null,461,1,2,0,'2012-06-01 14:35:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (462,1,2,'PB affichage Ecran CPXG015 lorsqu''un dossier RCD ou RC est fermé "Miss.terminée"','voir les copies d''écran dansle document en pj - Merci ',null,null,1,null,4,null,6,0,'2012-04-23 17:28:14','2012-04-23 17:28:14','2012-04-23',0,null,null,462,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (463,1,2,'Pb AFFICHAGE SYNTHESE DES DOMMAGES des missions RC','Lorsque l''indemnité d''une dommage = 0 ce montant ne s''affiche pas dans la Synthèse dommages Indemnité dossier (onglet DATES) Voir les copies d''écran en pièce jointe - Merci ',null,null,1,null,4,null,6,0,'2012-04-23 17:53:31','2012-04-23 17:53:31','2012-04-23',0,null,null,463,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (464,1,2,'dossier 0730/178591 - montant indemnité ','le montant de l''indemnité est bien enregistré dans l''écran CPXG015 mais il ne s''est pas enregistré dans la Synthèse du dommage Montant de l''indemnité (voir copie d''écran) et de ce fait n''est pas remonté sur HERMES

!P1.PNG!',null,null,5,5,4,46,6,8,'2012-04-23 18:18:49','2012-09-09 09:55:36','2012-04-23',100,null,null,464,1,2,0,'2012-08-09 12:14:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (465,2,6,'DeX - Convergence des scripts DeX','Une stratégie commune aux différents scripts doit être trouvées et documentées (sous forme d''un diagrame):
','2012-05-28',31,5,null,4,98,11,7,'2012-04-24 17:53:38','2013-02-13 11:46:54','2012-04-24',100,32,null,465,1,2,0,'2012-11-07 14:34:47');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (466,2,6,'Hermès - OM - réception des OM vers CP2000','h2. Objectif 

Alimenter CP2000 avec les ordres de missions XML générés par Hermès.',null,34,1,null,4,52,4,1,'2012-04-25 10:10:39','2012-05-16 12:05:47',null,0,null,null,466,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (467,3,10,'Etudes préalables','Intervention sur des sujets avant démarrage d''un projet...',null,null,4,null,4,null,4,0,'2012-04-25 12:22:08','2012-04-25 12:22:08',null,0,null,null,467,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (468,1,2,'Validation rapport à bloquer si enjeu vide ou égale 0','Actuellement si l''enjeu initial est supprimé ou égal à 0 on peut valider le rapport. 
Il faut donc bloquer la validation d''un rapport si la case enjeu du 1er dommage est vide ou égale à zéro


','2012-07-04',9,5,5,4,46,6,7,'2012-04-25 12:39:46','2012-09-09 09:55:51','2012-07-04',100,8,null,468,1,2,0,'2012-08-17 17:10:59');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (469,2,2,'améliorer le contrôle des montants des enjeux et indemnités','Il faut filtrer encore plus le système pour bloquer la validation : 
* si dommages garantis et que l''enjeu n''est pas renseigné sur chacun des dommages
* si dommages non garantis et que le 1er dommage non garanti n''est pas complété
* -si rapports unique ou d''expertise et dommages garantis et enjeu et indemnité non complétés- --> traité dans #897
',null,9,2,5,4,111,4,14,'2012-04-25 13:16:21','2013-07-04 22:27:47',null,40,24,null,469,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (470,3,11,'Autres - divers','Tâche dédiée à recevoir le temps passé sur le projet qui n''est pas affecté à une tache précise.

Cela peut prendre la forme de :

* Réunions
* Aide diverse

',null,null,4,null,3,null,4,4,'2012-04-25 14:18:02','2013-05-03 17:35:16',null,0,null,null,470,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (471,1,2,'dossier 0830/311593/DOA -absence indemnité ','Dans l''onglet Dates : Synthèse dommages manque Indemnité dossier alors que l''indemnité dans le dommage est bien renseigné',null,null,1,null,4,null,6,0,'2012-04-26 12:03:25','2012-04-26 12:03:25','2012-04-26',0,null,null,471,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (472,1,2,'pb affichage Enjeu/Indemnité','dossier 134238 : voir ci-joint les copies d''écran avec anomalie',null,null,1,null,4,null,6,1,'2012-04-26 14:49:10','2012-04-26 14:49:56','2012-04-26',0,null,null,472,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (473,1,2,'dossier 169419 - enjeu/indemnité','enjeu et indemnité n''ont pas été renseignés, synthèse devrait être vide or il apparaît enjeu et indemnité 0.00 voir copie d''écran',null,null,1,null,4,null,6,0,'2012-04-26 14:56:20','2012-04-26 14:56:20','2012-04-26',0,null,null,473,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (474,3,6,'Support - hotline','Consigner ici toute aide apportée au Support sur les sujets Dex.

1 aide = 1 commentaire

* Ne pas hésiter à mentionner le ticket Track-it
* Si cela donne lieu à une autre demande (exemple correction d''anomalie), noter la référence
',null,null,4,null,3,null,4,32,'2012-04-26 18:30:07','2013-05-23 10:39:45','2012-04-26',0,null,null,474,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (475,2,2,'Mail fort enjeu : enrichir le texte du message','h2. objectif

Sur réception d''un mail à fort enjeux, voir immédiatement de quoi il s’agit sans devoir aller obligatoirement sur CP2000

h2. infos souhaitées :

Dans le corps du mail :

| Enjeu | DOSETTCMNTNG |
| Client | ES1COD (cpxcli)|
| N° Dossier |DOSNUM|
| Type |MISCOD|
| Date de la mission |DOSDTAC|
| Expert | PRTCOD + PRTNOM|
| Position EURISK |DOSPOSE|
| Gestionnaire Compagnie |GESCOD + GESNOM|
| Assuré | REPNOM (si IDOASS like ''ASS%'')|
|liste des dommages |DOMTITRE (n fois)|
','2012-05-23',16,5,5,4,42,4,8,'2012-04-27 13:53:34','2012-06-17 11:37:03','2012-05-21',100,24,null,475,1,2,0,'2012-06-15 11:21:21');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (476,1,2,'Problème de création de mission à partir d''un OM','Aucune génération de dossier en base alors que tout le traitement envoie un status à 0 à la création à partir de CPXG001 ou CPXG002.

De plus, problème de gestion du tag "PFT", du moment que le mot de passe était saisi, tous les OMs étaient considérés PFT.

Écrans concernés :
CPXG001
CPXG002
CPXG014','2012-05-02',4,5,5,6,42,5,3,'2012-05-02 17:31:16','2012-06-17 11:33:43','2012-05-02',100,4,null,476,1,2,0,'2012-06-01 14:36:06');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (477,1,6,'Gsicass - Prb sur l''intégration de la piéce jointe (balise fichier)','Après étude l''encodage du fichier xml génére une erreur d''interprétation de la chaîne base 64 entre les balises "fichier"',null,40,5,11,4,90,11,5,'2012-05-03 09:13:21','2013-02-13 10:56:43','2012-05-02',90,16,null,477,1,2,0,'2012-05-16 12:00:50');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (478,2,6,'FLAG - Un sommaire doit être inclus dans le fichier ZIP','Un sommaire (fichier txt ou csv à voir) doit être inclus dans le fichier ZIP avec : 
Le nom du document (fichier pdf) 
Numéro dossier 
Numéro OM
Cause desordre
dysfonctionnemen
agce code
le code qui a conduit à la sélection du rapport
...

','2012-06-05',61,5,17,4,95,11,10,'2012-05-03 09:58:09','2013-02-13 11:47:23','2012-05-03',90,8,null,478,1,2,0,'2012-06-04 09:48:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (479,2,2,'Position OFF sur le flag fort enjeu (P Boyer / F Cardon)','h2. Demande

Un flag spécifique inaccessible à l’agence sera ajouté au dossier : 
Il sera automatiquement positionné sur ON avec l’envoi du mail d’alerte sur enjeu important
Il pourra être positionné sur OFF par Florence uniquement lorsqu’elle jugera que le dossier n’ a pas grand intérêt ou lorsque le dossier est terminé ou lorsque qu’il n’y a plus de risques…
Pour cela elle se connectera avec des droits particuliers (comme ceux de la plateforme par exemple…).
Ce flag permettra de faire les Reporting indépendamment de la variation des valeurs d’Enjeu.

h2. A faire

* Modifier le flag fort enjeu. Aujourd''hui, il prend en compte 3 états Vrai/Faux/Mail Envoyé. Rajouter l''état A comme "Annulé".
* Créer un écran saisie avec mot de passe (comme pour PFT --> paramétrage CPXPGA)
** Si mot de passe OK, le flag peut être positionné à annulé (suite à un état "mail envoyé")

NB : Prévenir le contrôle de gestion quand le flag après la mise en production',null,16,1,5,4,null,4,2,'2012-05-03 09:58:16','2012-10-23 12:01:15',null,0,32,null,479,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (480,3,10,'Point Projets','Point avec les équipes projets :
- réunion internes
- communications vers l''exterieur',null,null,4,null,4,null,4,0,'2012-05-03 15:27:22','2012-05-03 15:27:22',null,0,null,null,480,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (481,3,10,'Accueil nouvel arrivant','',null,null,4,null,4,null,4,1,'2012-05-03 18:17:25','2012-05-03 18:17:35',null,0,null,null,481,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (482,2,2,'GSICASS - PJ : ajout libellé+ date + PJ séparée','h2. Dans l''écran des envois (CPXL010B) 

Changer le comportement de la construction de la PJ.
1 document = 1 PJ (non fusionné avec d''autres)

h2. Envoi Rapport Gsicass (CPXG123)

Dans le bloc d''info PJ, permettre l''affichage de plusieurs lignes de PJ (afficher les 3 premières + scrollbar)
Donc le fichier déclencheur, renseigner 1 ligne par PJ.
En plus du chemin vers le fichier, il faut rajouter le libellé Hermès du document (CPXDTY.DTYLIBH & CPXDOS.DOCOMP) + la date de validation séparée d''un point virgule.
Exemple :
<pre>\\\\10.31.2.40\\agce\\Gsicass\\PJ_2012032611384400.pdf;Rapport d''expertise;10/05/2012
\\\\10.31.2.40\\agce\\Gsicass\\PJ_2012032611384567.pdf;Devis entreprise trucmuche;05/05/2012</pre>

',null,27,5,5,5,46,4,16,'2012-05-10 09:58:18','2012-09-09 09:57:50',null,100,40,null,482,1,2,0,'2012-08-09 11:57:00');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (483,2,6,'DeX - Création script d''initialisation cpcom.vsl','Un script permettant une gestion convivial du context d''exécution et d''aide à la gestion des erreurs doit remplacer l''ancien script cpcom-gen.',null,31,5,null,4,98,11,4,'2012-05-11 13:33:24','2013-02-13 11:48:15','2012-05-11',0,16,null,483,1,2,0,'2012-06-04 10:05:49');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (484,2,2,'A la création du dossier, générer une clé d''accès publique (Hermes/Allianz)','h2. Objectif

Générer une clé pour accéder au dossier Hermès de la manière suivante :
https://10.31.2.221/hermes/PUB_dossier.jsp?code=ABHUYTZEYUIHJOPQFGLM
La clé est attendue sur 20 caractères.

h2. Todo

* Concaténer 3 champs dossier pour avoir systématiquement + de 20 chiffres : Date/Heure de création du dossier + code agence + num2ro de dossier
* Trouver un algorythme de transformation chiffre --> lettre (Voir idée de base ci-dessous)
* Générer une clé de 20 caractère
* Stocker dans CPXDOS (Nouveau champs à créer. exemple DOSPUBKEY)
* Question : Faut-il prévoir une possibilité de changement de la clé dans le dossier ?

h2. Algorythme

Sur la base d''une correspondance des 10 chiffres 0123456789 vers une plage de ABCDEFGHIJKLMNOPQRSTUVWXYZ
|Pas|Source|Destination|
|1|0123456789|ABCDEFGHYJ|
|2|0123456789|BDFHJLNPRT|
|3|0123456789|CFJNRVZDHL|
|4|0123456789|DHLPTXBFJN|
|5|0123456789|EJOTYDYNSX|
|6|0123456789|FLRXDJPVBH|
|7|0123456789|GNUBYPWDKR|
|8|0123456789|HPXFNVDLTB|
|9|0123456789|IRAJSBKTCL|
|0|0123456789|JTDNXGQZJT|

-Choisir le pas en fonction de la milliseconde du traitement ($clock)-
Choisir le pas en fonction du code agence. Faire une somme sur les 4 chiffres du code agence jusqu''à tomber sur un chiffre de longueur 1. Exemple pour Toulon : 0830 = 0+8+3+0 = 11 = 1+1 = 2

||Pas|CodeAgence|Dossier|DateHeure|Resultat|
|Longueur|1|4|1-6|14|20 (tronque)|
|Exemple1|2|0830|310900|20120511161240||
|||BRHB|HDBTBB|FBDFBLDDDNDFJB|BRHBHDBTBBFBDFBLDDDN(BRHBHDBTBBFBDFBLDDDNDFJB)|
','2012-11-30',16,5,4,5,59,4,17,'2012-05-11 16:37:26','2012-12-16 08:36:44','2012-11-26',90,24,null,484,1,2,0,'2012-12-11 15:55:47');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (485,2,4,'Ajout champs dossier : clé accès publique + origine création dossier','Informations à ajouter dans le dossier

* doh_pubkey
* doh_origine

doh_pubkey = clé d''accès publique sur 20 caractères
doh_origine = Système informatique d''origine

doh_origine peut prendre les valeurs suivantes : T = Téléchargé Darva ; H = Hermès ; G = EDI Gsicass ; D = EDI Darva ; F ou NULL = aucun',null,30,5,null,4,45,4,5,'2012-05-14 14:54:23','2012-10-08 11:33:53',null,100,null,null,485,1,2,0,'2012-09-24 14:38:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (486,2,4,'Sur import dossier, ne pas envoyer de mail pour les dossier en provenance de GSICASS','GSICASS - Xavier Collet (ACS) - demande 46 

> Les rapports qui sont transmis à GSICASS par webservice ne doivent plus faire l''objet d''une alerte mail par Hermès.

Rajouter la règle : envoyer un mail sauf si rapport sur un dossier doh_origine = ''G'', 
',null,56,1,null,4,12,4,1,'2012-05-14 15:23:05','2012-05-24 14:40:20',null,0,null,null,486,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (487,2,5,'Configurer un serveur "type" sous Linux','Mettre en place un serveur de développement Linux avec les outils et la configuration nécessaires au bon fonctionnement de l''application WEB',null,null,4,10,4,9,10,2,'2012-05-15 09:34:02','2012-09-10 08:46:40','2012-05-15',90,null,null,487,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (488,1,2,'Anomalie écran CPXG010 et CPXG015 suite TEST MAIL ARCHIVAGE ALLIANZ','Cf ci-joint, copies d''écran.
Il me semble que la première anomalie avait déjà été repérée et réparée à l''occasion d''autres tests',null,null,1,null,4,null,6,0,'2012-05-15 14:08:06','2012-05-15 14:08:06','2012-05-15',0,null,null,488,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (490,2,6,'Gsicass - envoyer les notes d''honoraires','voir opération EnvoyerNoteDHonoraires
','2012-07-06',40,5,11,4,90,4,14,'2012-05-16 10:33:04','2013-02-13 11:49:26','2012-05-21',90,null,null,490,1,2,0,'2012-09-27 17:11:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (491,2,6,'NOE - script export Ardi->Noe','Envoi des dossiers en provenance d''Ardi vers le serveur Noe
',null,59,5,null,4,93,4,10,'2012-05-16 10:58:10','2013-04-02 16:17:01',null,100,null,null,491,1,2,0,'2013-04-02 16:17:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (492,2,6,'NOE - script import Noe->Ardi','Retour des dossiers traités par la tablette pour intégration dans ARDI',null,59,5,17,4,93,4,8,'2012-05-16 10:58:56','2013-04-02 16:17:09',null,100,null,null,492,1,2,0,'2013-04-02 16:17:09');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (493,2,6,'NOE - Controle + alerte dossiers non récupérés sur les tablettes','Les dossiers qui ne sont pas uploadés par leur expert à 8h le jour J feront l''objet d''une alerte spécifique sur ARDI',null,59,5,null,4,93,4,4,'2012-05-16 11:02:52','2013-02-13 11:50:09',null,0,null,null,493,1,2,0,'2012-10-11 15:48:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (494,2,6,'NOE - Gestion des mises à jour du référentiel des polices','',null,59,5,null,4,93,4,4,'2012-05-16 11:03:42','2013-02-13 11:50:34',null,100,null,null,494,1,2,0,'2012-10-12 11:46:45');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (495,2,2,'Prévisualisation des notes d''honoraires','h2. Objectif 

Avant la valorisation définitive de la NH, donner la possibilité à l''utilisateur d''avoir un récapitulatif chiffré de la note d''honoraire
Attention : afficher dans un écran CP2000, pas de document PDF généré avant valorisation définitive.

',null,26,1,5,4,null,4,3,'2012-05-16 11:35:55','2013-07-04 22:27:47',null,0,40,null,495,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (496,2,2,'Gestion des avoirs en agences','h2. Objectif 

Transférer la création des avoirs en agence

h2. Besoin

Sur sélection d''une NH *valorisée*, l''annulation en agence doit créer l''avoir correspondant
Copier le process existant au siège actuellement. Uniquement la partie "avoir total", l''avoir partiel reste interdit.


notes : 
* adapter le transfert des dossiers agence/siège en conséquence.
* analyser les impacts sur la rémontée Hermes

2 notions : 
* Erreur = NH inférieur ou égale à 3 jours
* Réclamation = NH + de jours

Une NH de + de 3 jours, doit automatiquement créer une réclamation dans le dossier

Alerte mail pour
* Compta (Dominique Gouveia) si erreur 
* Compta + Réclamation (Dominique Gouveia + Sylvie Dumont) si réclamation
',null,26,6,null,4,null,4,4,'2012-05-16 11:45:14','2012-10-22 16:41:51',null,0,120,null,496,1,2,0,'2012-10-22 16:41:51');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (497,2,6,'Darva - Suivi Dossier','Requête webservice Darva "suivi dossier"
Dans un premier temps seul les messages :

o « Accusé de réception »
o « Refus de la mission »
o « Note confidentielle»
o « Prise de position »

Seront pris en compte','2012-07-11',32,5,11,4,87,11,10,'2012-05-16 14:42:02','2013-04-02 16:14:20','2012-06-25',30,90,null,497,1,2,0,'2012-10-16 10:23:39');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (498,2,6,'Darva - Recupérer document ','La requête récupérer document permet de traiter:

o « Traitement de mission »,
o « Annulation de la mission »,
o « Commentaires»
o « Relance de l’expert »','2012-07-11',32,5,11,4,87,11,22,'2012-05-16 14:48:33','2013-04-02 16:14:20','2012-05-16',90,null,null,498,1,2,0,'2013-04-02 16:14:21');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (499,2,6,'Darva - Note Honoraires','Gestion de la requête NH (« Note d’honoraires ») pour Darva','2012-07-11',32,5,11,4,87,11,10,'2012-05-16 15:41:27','2013-04-02 16:14:21','2012-06-25',90,60,null,499,1,2,0,'2013-04-02 16:14:21');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (500,2,2,'Modification écran CPXL010B-Envoi de documents','Est-il pas possible de présenter la Liste des documents externes du dossier comme sur l''écran CPXL268 (fusion de documents):
Libellé - Complément libellé - Origine/Expéditeur - Date

La lecture du Complément faliciterait les envois pour identifier des pièces différentes mais indexées sous le même libellé (actuellement on est obligé de les ouvrir). ',null,5,1,null,5,null,6,3,'2012-05-16 16:32:02','2012-08-28 15:18:33','2012-05-16',0,null,null,500,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (501,2,6,'Darva','Projet Darva : Gestion des échanges avec la plateforme Darva','2012-12-07',32,5,11,4,87,11,14,'2012-05-18 09:38:43','2013-04-02 16:14:21','2012-11-05',100,40,null,501,1,4,0,'2013-04-02 16:14:21');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (502,2,6,'Darva - Gestion message LR','Gestion du message LR (Rapport et lettre d’accompagnement)Dans un premier temps les messages LR :

o Préliminaire
o Définitif (qui à pour nom aussi Rapport Expertise)
o Unique

Seront prises en compte.','2012-07-12',32,5,17,4,87,11,14,'2012-05-18 09:54:36','2013-04-02 16:14:22','2012-05-18',90,150,null,502,1,2,0,'2013-04-02 16:14:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (503,2,6,'Controle des boites au lettres RA+DR','Script Dex de notification suite à la réception d''un mail:
Le script doit scruter une boîte mail puis via un référentiel pour trouver le destinataire envoyer une notification
',null,62,5,17,4,101,11,5,'2012-05-18 16:31:22','2013-02-13 11:53:55','2012-05-18',100,null,null,503,1,2,0,'2012-06-04 12:08:16');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (504,2,12,'ExchangeCommander: Création des commandes pour la configuration et pour le projet UG','Nous devons pouvoir :
Spécifier les configurations nécessaire via la ligne de commande ou en spécifant le chemin d''un fichier de configuration
Les commandes pour accèder aux emails d''une boîte spécifié
Récupérer les informations d''un email (To, From etc...) 
Faire la distinction entre les emails lu ou non lu
Faire des recherches sur le carnet d''adresses
','2012-05-31',null,9,17,4,57,11,4,'2012-05-23 11:39:29','2012-05-24 17:27:17','2012-05-23',90,null,null,504,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (505,1,6,'FLAG - Spécification sur le sommaire','Suite à un contact avec flag et en se basant sur le sommaire envoyé, ce dernier nous a précisé les informations qu''il souhaite avoir dans son sommaire.
Les informations sont:
Le numéro du dossier du rapport, le numéro présent actuellement dans le sommaire n''est pas correct
Le code SICODES: 2 codifications sont possible (Disfonctionnement, Cause désordre)
Le code CRAC

Il n''est pas nécessaire de préciser le code agence dans le désordre   

Le fichier doit être au format CSV','2012-06-05',61,5,17,4,95,11,5,'2012-05-23 15:24:58','2013-02-13 10:57:11','2012-05-23',100,null,null,505,1,2,0,'2012-06-04 09:49:14');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (506,1,4,'Nombre notes d''honoraire en attente de règlement','Sur le lien vers la recherche des NH, il y a la phrase "Veuillez noter que 284598 notes d''honoraires sont en attente de règlement pour un montant total de 5770106.0€ TTC"

Le nombre de NH (ici 284598) est faux. On s''attend à un chiffre d''environ 10000 NH max.',null,63,5,null,4,45,4,5,'2012-05-24 10:10:32','2012-10-08 11:31:52','2012-05-24',100,null,null,506,1,2,0,'2012-09-24 15:37:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (507,2,4,'Vue publique d''un dossier','Origine du sujet : Allianz

Il s’agirait de pouvoir créer des accès pour des tiers qui sont partie au dossier mais qui ne sont pas Gestionnaire 1 ou 2, sans devoir les enregistrer spécifiquement comme utilisateur hermès (avocat, courtier...)
Nous allons mettre en place un systeme d''affichage à base de clé cryptée

Voir PJ pour un exemple de ce qui va être présenté :

!vue_publique.png!

Ticket CitéSI 266','2013-05-22',2,2,25,4,62,4,15,'2012-05-25 11:03:43','2013-07-04 22:27:47','2013-05-22',60,20,null,507,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (508,2,6,'Compta - permettre des extractions différées','Celine Cassagne / Delphine Audhoui - 01 30 78 18 28

Est-il possible de faire des extractions automatique de QuadraCompta?
A la demande, on souhaite pourvoir faire une extraction du grandlivre ou de la balance selon certains critères :
* date de début / date de fin
* classe de compte 

',null,64,6,null,4,52,4,5,'2012-05-25 11:26:58','2013-04-26 10:23:56',null,100,null,null,508,1,2,0,'2013-04-26 10:23:56');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (509,3,10,'Plaquette CPCOM Dev','Faire une plaquette pour communiquer sur les activités du développement

cible : futures recrues',null,null,4,null,4,null,4,1,'2012-05-25 11:41:01','2012-05-25 11:59:33',null,0,null,null,509,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (510,2,6,'Envoi mail différés ','Le processus de l''envoi du mail différé a changé. Désormais CP2000 génére un fichier xml (voir document joint). A 1h00 du matin tous les jours le script dex scrute les répertoires partagés des différentes agences et génére un mail avec les informations issues du fichier xml (destinataire,expéditeur,body,signature) ainsi que les documents en pièce jointe spécifiées (par les balises pjs, pj) par le fichier xml.',null,28,5,17,4,96,11,8,'2012-05-29 16:42:25','2013-02-13 11:54:26','2012-05-29',100,null,null,510,1,2,0,'2012-08-01 10:17:35');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (511,1,4,'Impossible de créer un nouveau profil','Lors de la création d''un nouveau profil, l''enregistrement n''est pas affiché dans le tableau.

Une intervention en bdd est nécessaire pour corriger l''enregistrement :
<pre><code class="sql">
update m_group set niveau_profil = 20 where row_id = 48;
</code></pre>

Ticket CitéSI 267',null,65,5,null,4,45,4,3,'2012-05-30 10:20:41','2012-10-08 11:32:13',null,100,null,null,511,1,2,0,'2012-10-08 11:32:13');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (512,2,2,'ECRAN CPXL100','Possibilité de supprimer un document à partir de cet écran (actuellement, on doit retourner dans le dossier où il a été numérisé pour faire cette action) ',null,null,1,null,4,null,6,0,'2012-05-30 15:28:40','2012-05-30 15:28:40','2012-05-30',0,null,null,512,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (513,3,5,'Prise en main Symfony et Kastor','L''objectif est de prendre en main le framework Symfony et de ',null,22,5,18,4,null,10,6,'2012-05-31 15:27:53','2012-09-10 08:46:11','2012-05-31',100,24,null,513,1,8,0,'2012-09-10 08:46:11');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (514,3,5,'Apréhender  Symfony2','-	Prise en main simple du framework Symfony2 :  http://j-place.developpez.com/tutoriels/php/creer-premiere-application-web-avec-symfony2/
-	Aller plus loin avec Symfony2 :  http://www.siteduzero.com/tutoriel-3-517569-symfony2-un-tutoriel-pour-debuter-avec-le-framework-symfony2.html
',null,18,5,18,4,null,10,4,'2012-05-31 15:42:13','2012-06-12 15:53:44','2012-05-31',100,16,513,513,2,3,0,'2012-06-12 15:53:44');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (515,3,5,'Installation du projet','-	Installer un environnement LAMP
-	Installer Turtoise SVN
-	Installer GIT

-	Faire un checkout sur le dépôt SVN : http://10.31.2.42/svn/cpweb/trunk/
-	Consulter le fichier « readme.txt » à la racine du rojet pour effectuer les installations
',null,18,5,18,4,null,10,6,'2012-05-31 15:45:42','2012-06-12 16:01:09','2012-05-31',100,8,513,513,4,7,0,'2012-06-12 16:01:09');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (516,3,5,'Documenter l''installation du projet étape par étape','',null,null,5,18,4,null,10,3,'2012-05-31 15:50:29','2012-06-12 16:01:48','2012-05-31',100,8,515,513,5,6,0,'2012-06-12 16:01:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (517,2,2,'Normaliser les envois de mail différés','Uniformiser l''envoi des mails différés déjà existants comme pour celui du sujet #433',null,6,5,5,4,42,5,6,'2012-06-01 15:47:46','2012-06-17 11:38:03','2012-06-01',100,null,null,517,1,2,0,'2012-06-15 13:27:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (518,2,6,'QBE-HERMES: Gérer l''archivage et la suppression des fichiers sur le serveur sFTP xfer dans le répertoire QBE après le transfert','Les fichiers xml et pdf dans le répertoire qbe sur le serveur xfer doivent être archivés (dans un répertoire qui à pour nom date_heure du transfert sous "gRepArchive..." sur DeX)  et supprimés après le transfert.',null,34,5,17,4,99,11,5,'2012-06-01 16:41:34','2013-02-13 11:54:45','2012-06-01',100,null,null,518,1,2,0,'2012-11-14 16:47:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (519,1,6,'Gsicass : Le script Gsicasstraitementmission ne doit pas envoyer le mail de blocage plateforme si la mission existe en base ','Dans "ACT2B: recuperer infos mission - Integration BDD", le paragraphe "Gosub : Appel ACT8- Gestion du blocage plateforme" ne doit pas s''exécuter si la mission existe déjà en base (voir fin de "ACT2C: Creation de repertoires pour sauvegarde + ConnexionODBC + IP serveur Agence")',null,40,5,null,4,90,11,4,'2012-06-04 17:36:27','2013-02-13 14:36:26','2012-06-04',100,null,null,519,1,2,0,'2012-07-18 10:46:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (520,2,6,'Gsicass: Un référentiel devra être mis en place  (Siret ou departement) ','Un référentiel devra être mis en place  (Siret ou departement) pour gérer la sélection de l''agence destinataire ',null,40,5,null,4,90,11,5,'2012-06-04 17:39:11','2013-02-13 14:39:02','2012-06-04',100,null,null,520,1,2,0,'2012-07-18 10:45:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (521,2,6,'GSICASS - Redirection OM en fonction du SIRET','h2. Objectif

Rediriger les OM Gsicass vers la bonne agence de destination en fonction du numéro de SIRET +*ET*+ du département du code postal du sinistre indiqué dans l''OM.

h2. Choix de l''agence

Si code agence du SIRET = code agence du code postal du sinistre, alors le code agence trouvé est l''agence de destination.

Si divergence ou incompatibilité entre le résultat du code agence déduit du SIRET et le code agence déduit du code postal du sinistre, alors email d''alerte vers Dominique Santoni. Le contenu peut être le même que le mail vers la plateforme.
L''OM en transit restera en attente de traitement chez DataExchanger.

h2. Arbitrage

Permettre à Dominique Santoni de débloquer l''OM en attente par le choix d''un code agence de destination.
(Mail de déblocage ?)


 
 ',null,40,5,null,4,90,4,4,'2012-06-05 09:31:48','2013-02-13 14:39:25',null,100,null,null,521,1,2,0,'2012-07-18 10:45:05');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (522,2,6,'CP2000 - Duplication d''un dossier','h2. Objectif 

Pouvoir dupliquer un dossier d''une agence CP2000 vers une autre.
Le besoin est aujourd''hui concentré sur la région parisienne (IDFO/IDFP/IDFEst) mais il pourra être étendu à l''avenir.

h2. Process existant

h3. Bases

Pour la base 0780 ou 0750 ou 0950, export-import effectué "à la main" par François Amant vers 0910 ou 0940
Paramètres = OPEG pour la source / DOSNUM de destination

h3. Contrôles

* Si n° de G (OPEG) déjà traité -> Ne rien faire, envoyer un mail à la secrétaire ayant fait la demande pour signaler "le doublon"
* Si n° de dossier (DOSNUM) déjà traité -> Ne rien faire, envoyer un mail à la secrétaire ayant fait la demande pour signaler "le doublon"

A voir : verrouiller la demande selon habilitation de la secrétaire.

h3. Déplacement

Recherche OPEID selon OPEG (info de la secrétaire)

CPXDOC:

* Si Dossier Judiciaire : Prendre uniquement les documents externes (Dociprov = ''E'',''X''). Export CPXDOC + copie document physique
* Si non judiciaire : Prendre les documents Nadcod = ''EX5''. Export CPXDOC + copie document physique

Prendre aussi les tables CPXIOP, CPXASP, CPXASO, CPXASS, CPXREP

Attention a la mise à jour des compteurs selon la norme CP2000
* Aspid (Cpxasp, Cpxass) 
* Assid (Cpxass, Cpxaso)
* Repid (Cpxrep, Cpxrep, Cpxiop, Cpxass)
* Docid 

Garder un historique des n° de G traité, numéro de dossier, demandeur, date de traitement

h3. Exemple SQL

<pre><code class="sql">
select * from cpxaso where opeid = XXX
select * from cpxasp where aspid in (select aspid from cpxass where repid in (select repid from cpxiop where opeid = XXX))
select * from cpxass where repid in (select repid from cpxiop where opeid = XXX)
-- Documents externes
select * from cpxdoc where dosnum in (select dosnum from cpxdos where opeid = XXX) and dociprov = ''E''
-- Documents judiciaires
select * from cpxdoc where dosnum in (select dosnum from cpxdos where opeid = XXX) and nadcod = ''EX5''
select * from cpxiop where opeid = XXX
select * from cpxrep rep2 where rep2.repid in (select repids from cpxrep where repid in (select repid from cpxiop where opeid = XXX
select * from cpxrep where repid in (select repid from cpxiop where opeid = XXX
</code></pre>',null,28,5,17,4,86,4,9,'2012-06-05 16:12:40','2013-02-13 14:40:04',null,100,null,null,522,1,2,0,'2013-02-13 14:40:04');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (523,2,6,'Hermes - Statistiques Hebdomadaire','h2. Objectif 

Demandeur : François Amant

Extraire un journal des connexions et téléchargements de l''application Hermès. 
A partir du journal, Sortir 2 états par semaine indiquant :
* Par Groupe > utilisateur Hermès, le nombre de connexion par jour
* Par Groupe > utilisateur Hermès, le nombre de téléchargement par jour

Valable pour Hermès Construction + Hermès Dommage
Prévoir de pouvoir faire un récapitulatif annuel : Chaque semaine cumul de toute les semaines de l''année en cours

Périodicité : Hebdomadaire, le lundi matin.
Destinataire : ???


h2. Procédure Actuelle

voir C:\\Progi\\Hermes\\HermesStats\\ sur serveur de développement (10.31.2.37)

<pre><code>
rem ==============================================================
rem ================== Partie Construction =======================
rem ==============================================================

del C:\\Progi\\Hermes\\HermesStats\\*.txt
C:\\Progi\\Hermes\\HermesStats\\pscp -pw hermes2004 hermes@10.31.30.6:/home/hermes/script/Fa/Lstutil.txt C:\\Progi\\Hermes\\HermesStats < C:\\Progi\\Hermes\\HermesStats\\Y.proc
C:\\Progi\\Hermes\\HermesStats\\pscp -pw hermes2004 hermes@10.31.30.6:/home/hermes/script/Fa/LogsDu.tar.gz C:\\Progi\\Hermes\\HermesStats < C:\\Progi\\Hermes\\HermesStats\\Y.proc
pause
copy C:\\Progi\\Hermes\\HermesStats\\LogsDu*.txt C:\\Progi\\Hermes\\HermesStats\\Logs.txt 
findstr "login" C:\\Progi\\Hermes\\HermesStats\\Logs.txt > C:\\Progi\\Hermes\\HermesStats\\Connexions.txt
findstr "doh_url" C:\\Progi\\Hermes\\HermesStats\\Logs.txt > C:\\Progi\\Hermes\\HermesStats\\Telechargements.txt
move C:\\Progi\\Hermes\\HermesStats\\LogsDu2*.txt C:\\Progi\\Hermes\\HermesStats\\C\\Archives
cscript //nologo C:\\Progi\\Hermes\\HermesStats\\Logs.vbs "C:\\Progi\\Hermes\\HermesStats\\Logs.xls" "Logs.xls!LogsHermes" "Connexions" "C:\\Progi\\Hermes\\HermesStats" "C"
cscript //nologo C:\\Progi\\Hermes\\HermesStats\\Logs.vbs "C:\\Progi\\Hermes\\HermesStats\\Logs.xls" "Logs.xls!LogsHermes" "Telechargements" "C:\\Progi\\Hermes\\HermesStats" "C"



rem ==============================================================
rem ================== Partie Dommages ===========================
rem ==============================================================

del C:\\Progi\\Hermes\\HermesStats\\*.txt
C:\\Progi\\Hermes\\HermesStats\\pscp -pw hermes2007 hermes@10.31.30.7:/home/hermes/scriptFa/Lstutil.txt C:\\Progi\\Hermes\\HermesStats < C:\\Progi\\Hermes\\HermesStats\\Y.proc
C:\\Progi\\Hermes\\HermesStats\\pscp -pw hermes2007 hermes@10.31.30.7:/home/hermes/scriptFa/LogsDu.tar.gz C:\\Progi\\Hermes\\HermesStats < C:\\Progi\\Hermes\\HermesStats\\Y.proc
pause
copy C:\\Progi\\Hermes\\HermesStats\\LogsDu*.txt C:\\Progi\\Hermes\\HermesStats\\Logs.txt 
findstr "login" C:\\Progi\\Hermes\\HermesStats\\Logs.txt > C:\\Progi\\Hermes\\HermesStats\\Connexions.txt
findstr "doh_url" C:\\Progi\\Hermes\\HermesStats\\Logs.txt > C:\\Progi\\Hermes\\HermesStats\\Telechargements.txt
move C:\\Progi\\Hermes\\HermesStats\\LogsDu2*.txt C:\\Progi\\Hermes\\HermesStats\\D\\Archives
cscript //nologo C:\\Progi\\Hermes\\HermesStats\\Logs.vbs "C:\\Progi\\Hermes\\HermesStats\\Logs.xls" "Logs.xls!LogsHermes" "Connexions" "C:\\Progi\\Hermes\\HermesStats" "D"
cscript //nologo C:\\Progi\\Hermes\\HermesStats\\Logs.vbs "C:\\Progi\\Hermes\\HermesStats\\Logs.xls" "Logs.xls!LogsHermes" "Telechargements" "C:\\Progi\\Hermes\\HermesStats" "D"
</pre>
</code>',null,34,5,17,4,92,4,17,'2012-06-06 14:11:35','2013-04-02 16:24:51','2013-02-21',100,null,null,523,1,4,0,'2013-04-02 16:24:50');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (524,1,6,'Gsicass : Dans le script GsicassTraitementMission Les fichiers archivés .xml.run ne sont pas renommés en .xml si le script fonctionne en mode paresseux','Dans le script GsicassTraitementMission Les fichiers archivés .xml.run ne sont pas renommés en .xml si le script fonctionne en mode paresseux',null,40,5,null,4,90,11,5,'2012-06-06 17:02:55','2013-04-02 16:32:00','2012-06-06',90,null,null,524,1,2,0,'2013-04-02 16:32:00');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (525,2,6,'Gsicass : Evolution script GsicassTraitementMissions suite à la V6 des normes assureur','Le script GsicassTraitementMissions doit pouvoir récupérer les informations suivantes (cf fichier norme assureur V6)

- Le code délocalisation
- Le type de la mission
- L''indicateur de centre de gestion déléguée

',null,40,5,17,4,90,11,8,'2012-06-08 11:57:10','2013-02-13 11:55:09','2012-06-08',70,null,null,525,1,2,0,'2012-07-18 10:44:16');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (526,2,6,'R&D Les agents DeX : Création d''un poc sur l''utilisation des agents DeX','Test de déploiement et d''utilisation des agents DeX',null,66,9,17,4,94,11,4,'2012-06-08 12:00:14','2013-02-13 10:56:01','2012-06-08',90,null,null,526,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (527,2,2,'Ajouter des colonnes dans la table des OMs pour les informations issues de GSICASS','Les colonnes suivantes doivent être ajoutées dans la table des OMs:

- Le code délocalisation (VARCHAR)
- Le type de la mission  (VARCHAR)
- L''indicateur de centre de gestion déléguée (O ou N)

',null,4,5,5,4,43,11,10,'2012-06-08 15:04:49','2012-06-24 17:24:16','2012-06-08',100,null,null,527,1,2,0,'2012-06-22 16:18:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (528,2,5,'Gestion des clients majeurs','Permettre:
-l ajout 
- la modification
- suppression (passer en inactif) des clients majeurs',null,38,5,18,4,null,10,6,'2012-06-11 09:11:16','2012-09-10 08:45:56','2012-06-11',100,24,365,365,34,35,0,'2012-09-10 08:45:56');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (529,2,5,'Axe de réflexion et questions en cours','Formulaires des entités:
- Affiche-on les dates de création et de modification dans un champs DISABLED du formulaire - Décision actuelle = *OUI*
- Affiche-on les ID (compteur AUTO incrément dans les formulaires) - Décision actuelle = *NON*
- Comment gère-t-on le paramétrage du siège ? Axe de réflexion : Deux base de données dont une de paramétrage -> Mise à jour de la base de données réelle durant une routine quotidienne
',null,null,5,null,4,null,10,2,'2012-06-11 15:01:11','2013-04-17 14:51:06','2012-06-11',0,null,null,529,1,2,0,'2013-04-17 14:51:06');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (530,1,2,'Différence entre la liste des documents EX et ce qui s''affiche en agence','Dans notre Référence dans la liste des documents externes EX une seule indexation pour 2.11 Mail (EX099 ) alors qu''en agence dans le menu déroulant il y a 2 indexations : 2.11 Mail et 2.11 Compte Rendu signé (voir ci-joint copie d''écran)',null,null,5,5,4,null,6,3,'2012-06-12 10:18:47','2012-10-26 10:39:38','2012-06-12',0,null,null,530,1,2,0,'2012-10-26 10:39:38');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (531,2,5,'Géolocalisation','Dans l''ordre, gérer l''ajour de toutes les données de géolocatisation:
1 - PAYS : LISTE - AJOUT - MODIFICATION - SUPPRESSION ... Aucun lien vers des entités autres
2 - DECOUPAGEREGIONNAL: LISTE - AJOUT - MODIFICATION - SUPPRESSION ... Lien vers le USER
3 - REGION :  LISTE - AJOUT - MODIFICATION - SUPPRESSION ...Lien vers le PAYS
4 - DEPARTEMENT:  LISTE - AJOUT - MODIFICATION - SUPPRESSION ...Lien vers le DECOUPAGE REGIONAL ET VERS REGION
5 - VILLE:  LISTE - AJOUT - MODIFICATION - SUPPRESSION ... Lien vers le département
6-  CODEPOSTAUX:  LISTE - AJOUT - MODIFICATION - SUPPRESSION ... Sur le même écran que Ville',null,null,5,18,4,33,10,23,'2012-06-13 16:57:07','2012-09-10 08:45:14','2012-06-13',100,120,365,365,36,37,0,'2012-09-10 08:45:14');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (532,2,5,'Brainstorming','------------
Proposer une interface basée sur le modèle de l’objet USER avec les éléments suivants :
•	Lister les users
•	Ajouter un user
•	Modifier un user
•	Supprimer un user
Résolution : 1024 * 768
',null,null,5,18,4,null,10,5,'2012-06-14 10:33:51','2012-09-10 09:18:59','2012-06-27',100,null,342,342,18,21,0,'2012-09-10 09:18:59');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (533,1,6,'CP2000MailDiffere: Le processus d''archivage n''est pas tjr exécuté','Dans le script CP2000MailDiffere, l''archivage du fichier xml généré par cp2000 étant exécuté après l''envoi du mail, ce dernier n''est malheureusement pas exécuté si une erreur arrive. L''archivage doit se faire au plus tôt pour permettre la reprise sur erreur.  ',null,28,5,null,4,96,11,6,'2012-06-14 12:26:58','2013-02-13 10:58:24','2012-06-14',100,null,null,533,1,2,0,'2012-08-01 10:18:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (534,2,6,'Darva : Gestion des annulations de mission','Le message d''annulation de mission du Web service Darva (SD 25) doit pouvoir être géré par un script DeX',null,32,5,17,4,52,11,1,'2012-06-14 17:55:29','2012-06-15 14:55:50','2012-06-14',0,null,null,534,1,2,0,'2012-06-15 14:55:50');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (535,2,6,'Darva : Création d''une librairie permettant l''ensemble des appels aux WS Darva','Après analyse des scripts existants pour la gestion de la plateforme Darva et pour pouvoir répondre à la demande de future exigence. Une librairie (lib-ws-darva) est nécessaire. Cette dernière intégrera les différentes actions permettant la communication avec le Web Service Darva. Chaque action recevra en paramètre le chemin d''un fichier spécifiant les paramètres nécessaires à l''exécution de l''action et un chemin de fichier pour le résultat de l''action.
Les actions seront documentées pour donner les pré-conditions et les post-conditions nécessaires.
Les informations qui sont nécessaires à l''exécution de l''action mais qui ne sont pas trouvées dans CP2000 seront listé pour analyse par l''équipe CP2000.
Pour tester la librairie un script "test unitaire" sera créé.','2012-07-13',32,5,null,4,87,11,6,'2012-06-15 11:00:08','2013-04-02 16:14:22','2012-06-15',90,null,null,535,1,2,0,'2013-04-02 16:14:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (536,3,2,'Livraison 1.9.6','h2. Livraison CP2000 en cours de préparation

* Anomalie #444: GSICASS - Correction création mission
* Anomalie #454: Caractère problématique dans l''import d''un mail
* Anomalie #461: Ne pas afficher l''OM xml dans les documents internes
* Anomalie #476: Problème de création de mission à partir d''un OM
* Evolution #6: Fusion de documents
* Evolution #320: Document de l''opération - ajout filtrage et export csv
* Evolution #433: Allianz - envoi rapport automatique par email
* Evolution #451: Nouveau paramétrage Nom et n° Tél Assistante
* Evolution #456: GSICASS - modification libellé écran CPXG123
* Evolution #475: Mail fort enjeu : enrichir le texte du message
* Evolution #517: Normaliser les envois de mail différés

+ Structure SQL uniquement du sujet #263 Mail fort enjeu à envoyer au gestionnaire de l''agence
(référentiel gestionnaire non paramétré car sujet non terminé)

h2. Particularité de la livraison

JS :
* #6 - fusiondoc.js 
* #433 - fusionPJ.js  

SQL : 
* #6 - INSERT CPXDTY
* #263 - ALTER CPXAGE ( + INSERT à NULL temporaire )
* #433 - INSERT CPXLIS + INSERT CPXPGA (Pour l''instant Mail = NULL) + INSERT CPXVAL + CREATE VIEW + UPDATE CPXLNK + ALTER CPXLNK

DEX :
* mise en prod script mail différé (voir #510)','2012-06-17',null,5,4,4,42,4,2,'2012-06-15 13:48:31','2012-06-17 11:32:01','2012-06-15',100,null,null,536,1,2,0,'2012-06-17 11:32:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (537,1,6,'CP2000MailDiffere: Un petit prb lié à la mise en forme du message quand les balises href sont utilisées','Le prb est dans l''exemple ci-dessous:

<pre>
<p>Nous vous informons que le dossier est traité en gestion déléguée.
Vous pouvez suivre l''évolution de votre dossier sur le portail <href=""https://hermes.eurisk.fr/hermes/dossier.jsp?idclient=C1406201208&login=MailAgf&pwd=auetng"">Hermès</href></p>


--
Visitez notre site www.eurisk.fr

</pre>




',null,28,5,null,4,96,11,4,'2012-06-15 14:14:21','2013-02-13 10:58:57','2012-06-15',100,null,null,537,1,2,0,'2012-06-29 16:28:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (538,2,6,'Darva : Message LR Préliminaire','Gestion du message LR Préliminaire','2012-06-29',32,5,17,4,87,11,10,'2012-06-18 16:11:29','2013-04-02 16:14:22','2012-06-18',90,80,null,538,1,2,0,'2013-04-02 16:14:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (539,2,6,'Darva : Message LR Définitif','Gestion du message LR Définitif ','2012-07-11',32,5,17,4,87,11,6,'2012-06-18 16:15:18','2013-04-02 16:14:23','2012-06-18',90,40,null,539,1,2,0,'2013-04-02 16:14:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (540,2,6,'Darva : Message LR  Unique','Message LR Unique','2012-07-11',32,5,17,4,87,11,9,'2012-06-18 16:17:00','2013-04-02 16:14:23','2012-06-18',90,40,null,540,1,2,0,'2013-04-02 16:14:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (541,2,2,'Mail CP2000 - fonction "Répondre à Tous"','h2. Demande 

Christophe LIN, IDF Est,  le 18/06/2012

> Pensez-vous que nous pourrions avoir la possibilité de "repondre à tous" aux mails intégrés et cerise sur le gâteau pouvoir transférer un message avec ses pièces jointes?
> Cette tâche est malheureusement impossible via CP2000 et encore moins depuis nos messageries internes.

h2. Analyse 

Pour les mails écrits par CP2000, cela devrait être possible.
Pour les mails en provenance d''outlook, avec l''archivage de la macro, c''est compromis.

h2. A faire

Dans CPXG059, procédure LP_RECUP :
* Scanner les lignes De: A: et CC: afin de récupérer tous les emails d''un message.
* Supprimer l''email de l''agence expéditrice en cours d''usage (pour ne pas s''écrire à soi-même)
* Sur la création du message, placer les emails,
** A: et De: dans A:
** CC: dans CC:


h2. Exemples

Contenu d''un mail CP2000
<pre>
De:recette2-CP2000@eurisk.fr
Envoyé:19/06/2012 14:57:07
À:vincent.bourez@eurisk.fr
Cc:vincent.bourez@groupeprunay-si.fr,cp2000@groupeprunay-si.fr
Objet:C12345678 - NosRef : 0830/311285/DOA
PJ:

test envoi plusieurs destinataire
</pre>



Contenu d''un mail Outlook, archivé par la macro

<pre>De:	Charles Pierre 2000 [test.cp2000@gmail.com]
Envoyé:	jeudi 31 mai 2012 12:19
À:	recette2 cp2000
Objet:	Fwd: 11202151 - NosRef : 0830/31061/DOA
Pièces jointes:	Convocation.pdf
</pre>

Cela va être plus dur de récupérer les emails dans ce cas là : 
Tous les emails connu de l''annuaire du groupe prunay n''est pas réellement présent, donc nous avons un problème pour tous les internes.
Tous les emails de l’extérieur devraient être récupérable, mais plus difficilement.',null,6,1,null,4,null,4,0,'2012-06-19 15:44:22','2012-06-19 15:44:22',null,0,24,null,541,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (542,2,2,'Mail CP2000 - Transférer un message avec ses pièces jointes','h2. Demande 

Christophe LIN, IDF Est,  le 18/06/2012

> Pensez-vous que nous pourrions avoir la possibilité de "repondre à tous" aux mails intégrés et cerise sur le gâteau pouvoir transférer un message avec ses pièces jointes?
> Cette tâche est malheureusement impossible via CP2000 et encore moins depuis nos messageries internes.

h2. Analyse

On a le message, on a les pièces jointes... cela doit donc être possible
Le seul problème sera de trouver comment placer le bouton "transférer" dans les écrans !
',null,6,1,null,4,null,4,0,'2012-06-19 15:48:43','2012-06-19 15:48:43',null,0,24,null,542,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (543,2,6,'GSICASS: Le script GsicassEnvoiRapport doit être compatible avec la norme assureur V6','Le script doit transmettre les informations :

Type de mission
Indicateur centre de gestion déléguée
Code délocalisation
Code délocalisation de l''assureur RCD (Tjr à 001)
','2012-06-25',40,5,11,4,90,11,8,'2012-06-20 10:33:09','2013-02-13 11:55:29','2012-06-20',70,24,null,543,1,2,0,'2012-07-18 10:43:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (544,1,2,'Enjeu initial non affiché sur HERMES','Dossier 0380/488882/RCA : dans CP2000 l''enjeu initial est affiché dans HT mais pas en TTC.L''enjeu n''est pas remonté sur HERMES',null,16,5,5,6,46,6,5,'2012-06-21 09:00:07','2012-09-09 09:56:10','2012-06-21',100,null,null,544,1,2,0,'2012-08-09 11:57:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (545,2,2,'Modification d''un libellé dans l''écran CPXG001 Création d''une mission','A la demande des Assistantes "débutantes" SVP modifier 
Adresse principale du  --> Adresse du sinistre','2012-06-22',16,5,4,6,43,6,3,'2012-06-22 12:03:07','2012-06-24 17:24:33','2012-06-22',100,null,null,545,1,2,0,'2012-06-22 16:18:47');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (546,2,2,'Modification Ecran CPXG005 : afficher les entreprises par ordre alphabétiques','sur cet écran, nous enregistrons la liste des intervenants qui s''affiche dans l''ordre de leur création.
Serait-il possible qu''ils se rangent après création dans l''ordre alphabétique (pour faciliter la recherche d''une entreprise que l''on doit cocher pour l''associer au répertoire du dossier)','2012-06-22',35,5,4,6,43,6,4,'2012-06-22 12:07:08','2012-06-24 17:24:47','2012-06-22',100,null,null,546,1,2,0,'2012-06-22 16:18:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (547,2,6,'Gsicass: Intégrer la notion des agences jumelles dans la réception des OMs','Un référentiel permettant de connaître les agences jumelles (agences traitant les OMs d''un même département) doit être créé. De plus, lors de la réception d''un OM et si ce dernier doit être intégré dans une base qui a une ou des agences jumelles, le n°de contrat (N° de police) de l''OM doit être cherché en priorité dans les bases agences jumelles et si le contrat est trouvé dans l''une des bases l''intégration doit être faite dans cette base.    ','2012-09-03',40,1,null,4,122,11,3,'2012-06-22 15:19:46','2013-04-02 16:35:30','2012-06-22',0,null,null,547,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (548,1,6,'GSICASS: Gestion de l''indicateur judiciaire dans la norme V6','L''indicateur judiciare n''existe plus dans la V6 de la norme (la disparition a été faite entre la V3 et la V4), ceci dit actuellement seul les misssion amiable sont gérée.
Le champs CPXOMS doit être donc A (A pour amiable, J pour judiciaire)',null,40,5,null,4,90,11,5,'2012-06-22 16:27:47','2013-04-02 16:32:00','2012-06-22',80,null,null,548,1,2,0,'2013-04-02 16:32:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (549,1,2,'Ecran CPXC078 Affichage du message : Expert M. BERTEAUX','Le nom de M. BERTEAUX apparaît en tant qu''expert dans la Pièce Jointe 
dossier 0450/629025 --> consultation d''un mail externe
dossier 0310/092795 --> consultation d''un mail interne
voir les copies d''écran ',null,6,5,4,6,58,6,1,'2012-06-25 10:17:19','2012-06-25 17:28:25','2012-06-25',100,null,null,549,1,2,0,'2012-06-25 17:28:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (550,2,2,'Ordre alphabétique des intervenants : Répertoire dossier + DT208','Suite classement des intervenants par ordre alphabétique dans Ecran CPXG005 --> faire idem dans Répertoire du dossier et dans le DT208 (il y a des paramétrages) ',null,null,1,null,6,null,6,1,'2012-06-25 10:21:00','2012-12-17 13:52:10','2012-06-25',0,null,null,550,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (551,1,6,'Gsicass : Anomalie de GSICASS traitement mission en mode paresseux','Le script GSICASS est tjr affiché comme si il était exécuté en mode paresseux (E02=-99)
',null,40,5,11,4,90,11,4,'2012-06-25 10:29:53','2013-02-13 10:59:12','2012-06-25',100,1,null,551,1,2,0,'2012-07-09 17:07:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (552,1,2,'lien hermes HS','il manque le "a" dans la balise <a href> !',null,6,5,4,4,58,4,1,'2012-06-25 12:39:59','2012-06-25 13:05:35','2012-06-25',100,null,null,552,1,2,0,'2012-06-25 13:05:35');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (553,2,6,'DeX : L''action CPCOM-INIT-UNIQUE-ID  dans le script CPCOM doit être ajouter pour initialiser  pUNIQUE_ID (géneration d''un identifiant unique)','L''action CPCOM-INIT-UNIQUE-ID  dans le script CPCOM doit être ajouter pour initialiser  pUNIQUE_ID (géneration d''un identifiant unique)',null,31,5,11,4,98,11,2,'2012-06-26 16:59:33','2013-02-13 11:57:10','2012-06-26',100,null,null,553,1,2,0,'2012-07-26 14:34:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (554,3,4,'Test paramétrage accès direct avec regroupement client majeur','h2. Demande

Voir si un regroupement de client pour le GAN/LAUN/GROUPAMA serait la solution au problème d''accès direct des login %sicass rencontré par Gan/Groupama
!sicass.png!

h2. Tests

Jeu de test avant Paramétrage :

|Client mineur|Client majeur|Sinistre| Test GanSicass | Test LauGanSicass |
|CORSEGAN02|LAUN-GANAS|11162212S| "KO":https://hermes.eurisk.fr/hermes/dossier.jsp?login=GanSicass&pwd=6qh8u1&idclient=11162212S | "OK":https://hermes.eurisk.fr/hermes/dossier.jsp?login=LauGanSicass&pwd=hb43e1&idclient=11162212S |
|CORSEGAN02|LAUN-GANAS|11162588S| "KO":https://hermes.eurisk.fr/hermes/dossier.jsp?login=GanSicass&pwd=6qh8u1&idclient=11162588S | "OK":https://hermes.eurisk.fr/hermes/dossier.jsp?login=LauGanSicass&pwd=hb43e1&idclient=11162588S | 
|GAN-SA-001|GAN-ASS|0275984| "OK":https://hermes.eurisk.fr/hermes/dossier.jsp?login=GanSicass&pwd=6qh8u1&idclient=0275984 | "KO":https://hermes.eurisk.fr/hermes/dossier.jsp?login=LauGanSicass&pwd=hb43e1&idclient=0275984 |
|GAN-SA-001|GAN-ASS|01162060S| "OK":https://hermes.eurisk.fr/hermes/dossier.jsp?login=GanSicass&pwd=6qh8u1&idclient=01162060S | "KO":https://hermes.eurisk.fr/hermes/dossier.jsp?login=LauGanSicass&pwd=hb43e1&idclient=01162060S |
',null,2,5,4,4,null,4,3,'2012-06-26 17:49:02','2012-06-27 13:57:28','2012-06-26',100,null,null,554,1,2,0,'2012-06-27 13:57:28');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (555,2,5,'Proposer un logo pour l''application','',null,null,5,null,4,null,10,1,'2012-06-27 10:16:52','2012-09-10 08:44:23','2012-06-27',0,null,532,342,19,20,0,'2012-09-10 08:44:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (556,2,2,'GSICASS - Envoi note d''honoraire','h2. Demande 

En plus des 3 messages pour les rapports, il faut pouvoir envoyer la note d''honoraire vers Gsicass

h2. Question

Dominique, 
# que préfères-tu pour le déclenchement ?
** Un bouton "Envoi NH" dont le déclenchement sera à l''initiative de l''utilisateur ?
** Automatiquement sur la validation d''une note d''honoraire, CP2000 déclenche un message vers Gsicass ?
# Est-ce que la NH eurisk doit être envoyée en PJ ?


h2. Fichier déclencheur

Prendre exemple sur le sujet des envois de rapport (chemin, nommage)
Ici, le fichier doit contenir l''identifiant nhonumage, voir avec Guillaume pour le contenu et le format exact',null,27,5,5,4,46,4,14,'2012-06-28 13:45:43','2012-09-09 09:58:00',null,100,null,null,556,1,2,0,'2012-08-09 11:58:37');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (557,2,5,'Gestion des agences','Objet Agence (ajouter, modifier, supprimer)
Champs à gérer:
- AGECOD : Saisie libre sur 5 chiffres -- Vérifier l''unicité
- AGENOM
- AGELIB
- AGERAG - Responsable agence - Lien vers le user
- AGECAD - Contact administratif - Lien vers le user',null,null,5,18,4,null,10,11,'2012-06-29 10:05:37','2012-09-10 08:43:57','2012-07-03',100,16,365,365,38,41,0,'2012-09-10 08:43:57');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (559,1,6,'GSICASS: Un gros prb sur webservice erreur WSE910','Suite au changement de certificat, le webservice en validation GSICASS retourne l''erreur 
Erreur WSE910: An error happened during the processing of a response message, and you can find the error in the inner exception.  You can also find the response message in the Response property.

Sous DeX.

L''erreur est reproductible sous le designer au premier lancement du script ci-joint
','2012-07-02',40,2,11,5,122,11,8,'2012-06-29 16:41:56','2013-04-02 16:35:01','2012-06-29',60,null,null,559,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (560,2,5,'Gestion des clients','Permettre l''ajout, la modification, l''activation d''un client
Établir les règles de gestions
Documenter la solution',null,null,5,8,4,null,10,9,'2012-07-02 16:46:12','2013-04-22 10:35:12','2013-04-15',100,4,365,365,42,45,0,'2012-09-10 08:43:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (561,2,2,'Ajout nom de l''assuré dans le mail auto Fort Enjeu / Forte indemnité','Ajouter dans le libellé du mail : le nom del''assuré, pour tous types de police RCP/RCD/MGAE/MOE/Fabricant, en fait il ne le faut pas pour DO et PUC.
Voir copie d''écran',null,6,5,4,6,46,6,8,'2012-07-02 17:24:33','2012-09-09 09:58:14','2012-07-02',100,null,null,561,1,2,0,'2012-08-09 11:59:36');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (562,2,5,'Lien agence','•	Auteur: Sabrina Laby
•	Statut: Nouveau
•	Priorité: Normal
•	Assigné à: Florent Poiteau
•	Catégorie: 
•	Version cible: 
Agence_mail
- ajouter, modifier, supprimer 
Agence_adresse
- ajouter, modifier, supprimer 
Agence_téléphone
Se baser sur le développement des user : Attention à l''unicité
',null,null,5,18,4,null,10,5,'2012-07-03 09:46:02','2012-09-10 08:43:19','2012-07-03',100,16,557,365,39,40,0,'2012-09-10 08:43:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (563,2,5,'Message d''erreur','S''assurer de l''internationalisation des messages d''erreur
S''assurer que les messages d''erreur ne passent plus par l''URL',null,null,5,8,4,null,10,2,'2012-07-04 09:48:15','2012-09-10 08:42:50','2012-07-04',100,null,350,349,9,10,0,'2012-09-10 08:42:51');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (564,2,5,'Choisir DTD en accord avec Symfony (cf Label Size)','S assurer que la DTD choisie valide les formulaires générés par Symfony',null,null,5,10,4,null,10,1,'2012-07-04 10:51:32','2012-09-10 08:40:02','2012-07-04',0,null,60,60,2,3,0,'2012-09-10 08:40:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (565,1,6,'Darva : Erreur Web Service via brique WS DeX "Failed to deserialize contexte, expecting" sur message LR','Une erreur "Failed to deserialize contexte, expecting" lors de l''execution de la brique WS sur dex or via soap ui la requête LR fonctionne.
',null,32,5,17,4,87,11,7,'2012-07-05 11:12:56','2013-04-02 16:14:23','2012-07-05',90,null,null,565,1,2,0,'2013-04-02 16:14:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (566,1,2,'OM GSICASS Dossier 0940/473907 - Document 2 manquant','Dans cet OM, il y 2 pièces jointes : 1ère = déclaration de sinistre et 2ème = dossier technique (CP/Att/marchés).

Seule la 2ème s''est indexée automatiquement dans les Documents Ext. en 1.01

',null,5,5,5,6,46,6,9,'2012-07-05 16:57:42','2012-09-09 09:56:23','2012-07-05',100,null,null,566,1,2,0,'2012-08-01 16:34:04');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (567,2,2,'Darva -  Ajouter le type maitre d''ouvrage','CPXG009 - Ajouter une liste déroulante "Type Maitre d''ouvrage" sur l''opération en dessous du contrôleur technique.

Les données de la liste déroulante sont présents dans la nomenclature ''B'' du module Crac/Sycodes
(Voir exemple dans CPXG262, appel vers "CPXS251".Charge pour valoriser la liste)
<pre><code class="sql">select * from cpxsycode where syc_nomenclature = ''B'';</code></pre>

Champs Obligatoire dans le cadre d''un dossier DARVA (T ou D)

Attention ! Nécessite modification structure de la table CPXOPE.
Ne pas oublier le ''grant'' SQL pour Irina',null,29,5,5,4,46,4,21,'2012-07-06 10:03:05','2012-09-09 09:58:35','2012-07-06',100,null,null,567,1,2,0,'2012-09-07 11:42:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (568,2,2,'Darva - Ajouter le  role de l''intervenant','CPXG005 - repertoire de l''opération

Il faut ajouter une notion dans CPXIOP : role de l''intervenant.
Cela peut prendre les valeurs : conception, exécution, matériau ou procédé.
Ce sont les même valeurs que la nomenclature ''1'' du module Crac/Sycodes
(Voir exemple dans CPXG262, appel vers "CPXS251".Charge pour valoriser la liste)
<pre><code class="sql">select * from cpxsycode where syc_nomenclature = ''1'';</code></pre>

Le champs est obligatoire pour un dossier Darva (T ou D)

Attention, vérifier s''il y a un impact sur l''écran CPXG269 (appelé pour le CT depuis l''écran des opérations CPXG009)
Nicolas : Voir si CPXG269 et CPXG005 ne peuvent pas être "fusionné"


',null,35,5,5,4,46,4,21,'2012-07-06 10:46:22','2012-09-09 09:58:48','2012-07-06',100,null,null,568,1,2,0,'2012-09-07 11:42:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (569,1,6,'GSICASS:  Dans GsicassTraitementMission OMSFLGCRAC doit être mis à T (true) ou F (False), un mapping est donc nécessaire entre gCodeTypeMission et OMSFLGCRAC ','Dans le script GsicassTraitementMission OMSFLGCRAC doit être mis à T (true) ou F (False), un mapping est donc nécessaire entre gCodeTypeMission et OMSFLGCRAC.',null,40,1,11,4,122,11,2,'2012-07-06 12:21:32','2013-04-02 16:35:30','2012-07-06',0,null,null,569,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (570,1,6,'GSICASS : Script RecapOrdreMission prb lié à la requête de recherche OMSPFT=''F'' il doit être à T','ript RecapOrdreMission prb lié à la requête de recherche OMSPFT=''F'' il doit être à T',null,40,5,11,4,90,11,3,'2012-07-06 16:01:31','2013-02-13 10:59:37','2012-07-06',100,null,null,570,1,2,0,'2012-07-18 10:41:09');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (572,1,6,'Darva: Erreur  sur la réponse donné par Darva suite à une requête LR','Erreur sur la réponse donné par Darva suite à une requête LR',null,32,5,null,4,87,11,6,'2012-07-06 16:21:34','2013-04-02 16:14:24','2012-07-06',90,null,null,572,1,2,0,'2013-04-02 16:14:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (574,2,2,'Darva - envoi rapport','h2. Déclenchement

L''utilisateur choisira le type de rapport à envoyer.
Il a 3 choix :
* 01 - Rapport Unique
* 02 - Rapport Préliminaire
* 03 - Rapport Définitif

Toute trace d''un envoi doit être enregistrée dans CPXCHRONO (mode d''envoi darva)

Attention contrainte Darva, on ne doit pas pouvoir envoyer de rapport unique dans le cas où un rapport préliminaire a déjà été envoyé pour une même référence sinistre et un même numéro mission


h2. Fichier déclenchement

il faut que CP2000 génère un fichier déclencheur de type texte pour les envois de rapport vers Darva
le nom du fichier doit contenir les informations suivantes :
* Code Agence
* Type rapport (01 = Rapport Unique, 02 = Rapport Préliminaire, 03 = Rapport définitif)
* Référence Sinistre
* Numéro Mission
* Numéro envoi
* Site de gestion
* exemple de nom de fichier : 

0830_01_TESTCONSTRUVABF2_1_8300000039117_222.txt (cf attachement pour contenu)

à l''intérieur de ce fichier doit se trouver le chemin vers les pièces jointes

h2. PS : 

1 déclencheur pour 1 PJ, sachant que la PJ est une fusion de tous les documents sélectionnés.','2012-11-30',33,5,17,4,59,17,126,'2012-07-13 14:20:59','2012-12-16 08:36:48',null,90,40,769,769,10,11,0,'2012-12-14 16:20:30');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (575,1,6,'GSICASS: Dans le scipt GSICASSTraitementMission Le mail de blocage plateforme est envoyé des documents qui ne correpondent pas à la mission  ','Dans le scipt GSICASSTraitementMission, le mail de blocage plateforme est envoyé des documents qui ne correpondent pas à la mission. Le répertoire incluant les pièces jointes à envoyer doit être "RAZ" après le "send email".',null,40,5,11,4,90,11,4,'2012-07-13 16:33:24','2013-02-13 11:01:05','2012-07-13',70,null,null,575,1,2,0,'2012-09-27 17:12:38');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (576,1,6,'GSICASS: Dans le script TraitementMission le renomage du fichier archivé Mission ...run en Mission...XML n''est pas fait correctement lors d''un traitement de mission multiple ','Dans le script TraitementMission le renomage du fichier archivé Mission ...run en Mission...XML n''est pas fait correctement lors d''un traitement de mission multiple (réception de plusieurs OM sur consulter mission)',null,40,2,11,4,122,11,2,'2012-07-13 16:36:59','2013-04-02 16:35:31','2012-07-13',0,null,null,576,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (577,2,6,'Une solution permettant la gestion des logs dans les script DeX doit être mise en oeuvre','Une solution incorporée au script CPCOM devra être mise en place en s''inspirant des API tel que log4J. Les logs seront insérés en base et pourront être affichés ou pas sur la console des logs de DeX (utilisation de couleur).  ',null,31,5,17,4,98,11,5,'2012-07-13 16:43:05','2013-02-13 11:57:27','2012-07-13',100,null,null,577,1,2,0,'2012-11-07 14:34:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (578,2,6,'NOE: Formaliser les échanges ARDI DEX suite à la réunion avec CARD','* Spécifier les noms de fichier utilisés pour les échanges
* Spécifier la structure du zip
* Résumé des périmetres CARD / CPCOM
...','2012-07-18',59,5,11,4,93,11,3,'2012-07-13 17:10:07','2013-02-13 11:57:52','2012-07-13',100,null,null,578,1,2,0,'2012-10-09 17:04:26');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (579,2,6,'GSICASS:  Une gestion du routage semi automatique doit être géré','Un email permettant le  routage des OM en erreur de routage doit être envoyé à la plateforme proposant différentes options pour router l''OM',null,40,5,11,4,90,11,5,'2012-07-16 09:45:58','2013-02-13 11:58:16','2012-07-09',100,null,null,579,1,2,0,'2012-11-30 16:04:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (580,2,2,'Augmenter la taille d''un export à 5Mo','Darva a relevé la limite de la taille des pièces jointes à 5 Mo.
Il faut donc revoir le test sur la restriction de taille des documents actuellement limité dans CP2000 à 1 Mo

Note : Pour les exports PAPS, la limite est à 20Mo
',null,27,5,4,4,46,4,3,'2012-07-17 11:00:25','2012-09-09 09:59:02','2012-07-17',100,null,null,580,1,2,0,'2012-07-25 17:43:32');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (581,2,6,'Optimiser le transfert Ardi - Hermes ','Optimiser le transfert entre les serveurs Eurexo Eurexa Eurexp et hermes:

La création d''un zip et l''optimisation de la conversion en pdf (pour éviter les blocages IHM des processus automatiques) peuvent être appliquées. De plus, un référentiel pour DeX permettant de gérer les informations (adresse ip, nom) des serveurs Eurex* devra être mis en place.  
','2013-05-22',34,9,11,4,91,11,34,'2012-07-17 11:05:11','2013-05-16 15:06:06','2013-04-02',90,null,null,581,1,4,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (582,2,9,'Print2PDF doit pouvoir gérer les formats ms-excel et ms-word','Print2PDF doit pouvoir gérer les formats ms-excel et ms-word en fichier source (détection de l''extension, ajout option pour forcer le type du fichier source)',null,null,5,17,4,106,11,6,'2012-07-17 11:27:40','2013-02-13 14:53:06','2012-07-17',100,null,null,582,1,2,0,'2013-02-13 14:42:50');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (583,2,5,'Optimiser le merveilleux objet pagination','Faire en sorte de rentre modulable l''objet pagination',null,null,5,8,4,null,10,2,'2012-07-18 11:48:59','2013-04-17 14:50:37','2012-07-18',0,null,null,583,1,2,0,'2013-04-17 14:50:37');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (584,1,2,'Non envoie de mail auto AR même quand le DT est concerné','Dans certains cas où le client qui missionne (ALLIANZ, ALLIANZDOM ou LAUN-AGF) est différent de celui qui paye, malgré le fait que le DT soit concerné par un envoi de mail Auto AR, celui-ci n''est pas déclenché.

Bug suite à modif de code dans la révision r356.

cf Track IT 34182 - Etienne Faure - Sandrine Pontasse - 0670/268130 ','2012-07-18',6,5,5,6,46,5,11,'2012-07-18 15:56:16','2012-09-09 09:56:39','2012-07-18',100,4,null,584,1,2,0,'2012-08-09 12:21:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (585,3,2,'Documentation sur les nouvelles règles de gestion des Forts Enjeux/Fortes Indemnités','Réaliser la documentation sur les nouvelles règles de gestion des Forts Enjeux/Fortes Indemnités.','2012-07-19',5,5,5,4,null,5,1,'2012-07-18 16:02:22','2012-07-19 15:46:07','2012-07-18',100,null,null,585,1,2,0,'2012-07-19 15:46:07');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (586,2,6,'Transfert QBE - vérifier la cohérence des XML / PJ','h2. Problème

Aujourd''hui, on transfère des fichiers xml avec une liaison vers des pièces jointes (PDF).
Hermès trace une erreur si un fichier PDF mentionné dans le XML est absent.
L''erreur reste chez Hermès et personne n''est prévenu.

h2. Idée

Il serait bien de contrôler la cohérence au moment du transfert du FTP vers hermès.
Ajouter un test : vérifier si tous les fichiers spécifiés dans le XML sont bien présents sur le FTP
* Si oui, OK, rien à signaler
* Si non, prévenir par mail Marsh - Olivier Kauf. Mentionner l''objet concerné avec sa référence + le nom du fichier manquant.

Le contrôle n''est pas bloquant, juste informatif.

Faire apparaitre le processus dans une couleur/statut "warning" dans le suivi de la console DataExchanger
',null,34,1,null,4,52,4,0,'2012-07-19 10:57:12','2012-07-19 10:57:12','2012-07-19',0,null,null,586,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (587,2,5,'Envisager la création d''un menu dynamique','1 - Etudier les solutions offertes par le couple (XML/XSLT) ou MySQL ou autre
2 - Etudier la structure d''un menu à N niveaux : 1 niveau contenant N fils
3 - Etudier les paramètres de chaque noeud - (optionnel, requis, ordre, acl(droit principaux) ...)
4 - Faire un prototype rapide sans aucun style mais combiné aux droits sur les objets 
',null,null,5,8,4,null,10,2,'2012-07-20 15:54:10','2012-09-10 08:43:04','2012-07-20',0,null,356,342,15,16,0,'2012-09-10 08:43:04');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (588,2,5,'Gestion des gestionnaires','Ajout, Modification, Liste, Suppression
Cf maquette balsamiq',null,null,5,18,4,null,10,3,'2012-07-20 15:54:50','2012-09-10 08:42:33','2012-07-20',100,32,null,588,1,2,0,'2012-09-10 08:42:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (589,2,5,'Gestion des répertoires','Gérer l''ajout, la suppression, la modification et la liste
Attention lien fort entre Répertoire et Contact principal (droits)
Cf maqueette Balsamiq',null,null,5,18,4,null,10,7,'2012-07-20 15:55:55','2013-04-17 14:50:24','2012-07-20',100,32,null,589,1,2,0,'2013-04-17 14:50:08');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (590,2,5,'Gestion des contacts','Permettre l''ajout, la suppression et la modification des Contacts
Les formulaires doivent etre réutilisable dans plusieurs obj (Client, répertoire)
Cf. maquette Balsamiq',null,null,5,8,4,null,10,3,'2012-07-20 15:57:18','2012-09-10 08:39:15','2012-07-20',90,16,null,590,1,2,0,'2012-09-10 08:39:15');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (591,2,5,'Gestion des objets paramétrage','- Etudier une solution de gestion des listes hérité du paramétrage -> résultat de l''étude sous forme de doc succinte
- Créer les formulaires de chaque listes héritées (ajout, modification, désactivation, liste)
-> MINIMISER LE CODE
',null,null,5,18,4,null,10,3,'2012-07-20 15:59:19','2012-09-10 08:39:02','2012-07-20',100,null,null,591,1,2,0,'2012-09-10 08:39:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (592,2,5,'Maquette Balsamiq','',null,null,1,10,4,null,10,0,'2012-07-20 16:07:01','2012-07-20 16:07:01','2012-07-20',0,null,null,592,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (593,2,2,'Rotation des documents PDF','h2. Demande

> De : CPCOM/CHOLLET Ludovic 
> Envoyé : mardi 24 juillet 2012 11:12
> À : CPCOM/BOUREZ Vincent
> Objet : [CP2000] Demande d''ajout de fonctionnalité
> 
> Vincent,
> 
> Suite à notre conversation je te confirme le besoin de l’agence de Marseille, à savoir, manipuler les fichiers PDF depuis CP2000 (en rotation).
> En effet, ils réceptionnent de plus en plus de document PDF qui ont été numérisés à l’envers.
> A l’heure actuelle, ils impriment le PDF pour le re-numériser.
> 
> Nous disposons sur les serveurs de l’utilitaire « pdftk » qui peut être candidat pour cette opération.
> 
> Effectuer une rotation de 180 degrés du document entier :
> pdftk in.pdf cat 1-endS output out.pdf
> 
> Peux-tu voir avec ton équipe s’il est possible de l’intégrer directement dans CP2000, ou si je dois créer un script batch.
> Cette demande ne concerne pour le moment que l’agence de Marseille, et n’est pas urgente mais je pense cette nouvelle fonction pourrait intéresser tout le monde.

h2. Modification souhaitée

Dans l''écran des documents externes (là ou se trouvent les PDF), à l''instar de ce qui est disponible pour la fusion et la séparation de document, proposer dans le menu "option" :
* la rotation horaire 90°
* la rotation antihoraire 90°

Pas besoin de nouvel écran : lancer directement la commande DOS/PDFTK sur le document sélectionné.

h3. Controles

# Un document est sélectionné
# Le document est un document PDF


h2. Ecran

!CP2000-rotation-pdf.png!',null,5,5,5,4,59,4,6,'2012-07-24 15:09:24','2012-12-16 08:36:44',null,90,8,null,593,1,2,0,'2012-09-28 17:45:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (594,1,6,'GSICASS: La mise à jour sur CPXCHRONOS dans le script GSICASSEnvoyerRapport dysfonctionne','La mise à jour sur CPXCHRONOS dans le script GSICASSEnvoyerRapport dysfonctionne.',null,40,5,11,4,90,11,4,'2012-07-24 17:12:30','2013-02-13 11:09:06','2012-07-24',70,null,null,594,1,2,0,'2012-08-16 15:52:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (595,2,6,'R&D: Outils permettant la reprise sur echec de copie de fichier volumineux ','Une recherche et des tests doivent être réalisés pour trouver une solution de copie de fichier avec reprise sur echec.',null,66,9,null,4,94,11,4,'2012-07-24 17:18:02','2013-02-13 10:55:38','2012-07-24',90,null,null,595,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (596,2,6,'Mise à zero des variables globales à l''initialisation et la fin des script','Mise à zero des variables globales à l''initialisation et la fin des scripts',null,31,5,11,4,98,11,2,'2012-07-26 14:37:50','2013-02-13 11:58:39','2012-07-26',100,null,null,596,1,2,0,'2012-11-07 14:33:34');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (597,2,13,'Création de copybatchcommander : outil de copie de fichier en mode Batch avec gestion des logs','Création de copybatchcommander : outil de copie de fichier en mode Batch avec gestion des logs',null,null,2,17,4,105,11,6,'2012-07-26 17:34:49','2013-02-13 14:54:04','2012-07-26',90,null,null,597,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (598,2,14,'crééer fichier .Bat pour lancement des process Print2PDF, CopyBatchCommander, 7Zip et RSync','crééer les batch pour lancer les process :
Print2PDF, CopyBatchCommander, 7Zip, RSync et Rd',null,null,1,17,4,104,17,1,'2012-07-27 14:38:15','2013-02-13 14:44:06','2012-07-27',70,null,null,598,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (599,1,4,'Import Hermès - Pas de libellé dans la supervision pour un import Chantier QBE','Pas de libellé dans la supervision pour un import Chantier QBE
Le champs origine est vide, c''est génant pour les recherches',null,56,1,null,3,null,4,1,'2012-07-30 13:49:06','2012-07-30 13:53:48','2012-07-30',0,null,null,599,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (600,1,4,'Tache panifiée QBE','ne fonctionne pas',null,51,5,null,4,null,4,1,'2012-07-30 13:49:28','2012-11-06 14:27:52','2012-07-30',100,null,null,600,1,2,0,'2012-11-06 14:27:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (601,1,4,'Import fichier chantier QBE','Les fichiers sont écrasés à l''import. Le chantier ne garde qu''un seul document dans sa liste d''enregistrement.',null,51,5,null,4,45,4,3,'2012-07-30 13:50:19','2012-10-08 11:32:40','2012-07-30',100,null,null,601,1,2,0,'2012-10-08 11:32:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (602,1,4,'Suppression des documents physiques','les fichiers sur le disques ne sont pas supprimés lorsque il ne sont plus présent dans un dossier.
(incohérence entre document et m_document)

',null,56,5,null,4,60,4,9,'2012-07-30 13:53:26','2012-08-31 14:24:12','2012-07-30',0,null,null,602,1,2,0,'2012-07-31 11:15:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (603,2,6,'Darva Etude des informations nécessaires pour AXA','Etude des différences de données pour les appels WS Darva pour AXA ',null,32,5,17,4,87,17,5,'2012-08-01 09:45:52','2013-04-02 16:14:24','2012-07-30',90,null,null,603,1,2,0,'2013-04-02 16:14:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (604,2,5,'Intégration continue Jenkins','Mettre en place un serveur d''intégration continue',null,null,2,10,4,null,10,2,'2012-08-01 11:53:50','2013-04-17 15:05:57','2012-08-01',0,null,null,604,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (605,3,2,'Livraison 1.9.8','h2. Livraison CP2000 en cours de préparation

* Anomalie #401: Case SENSIBLE
* Anomalie #464: dossier 0730/178591 - montant indemnité
* Anomalie #468: Validation rapport à bloquer si enjeu vide ou égale 0
* Anomalie #544: Enjeu initial non affiché sur HERMES
* Anomalie #566: OM GSICASS Dossier 0940/473907 - Document 2 manquant
* Anomalie #568: Darva - Ajouter le role de l''intervenant
* Anomalie #584: Non envoie de mail auto AR même quand le DT est concerné
* Evolution #219: FR099 - liste des assureurs en recours
* Evolution #457: Plus possible de sélectionner un assureur inactif dans les destinataires
* Evolution #482: GSICASS - PJ : ajout libellé+ date + PJ séparée
* Evolution #556: GSICASS - Envoi note d''honoraire
* Evolution #561: Ajout nom de l''assuré dans le mail auto Fort Enjeu / Forte indemnité
* Evolution #567: Darva - Ajouter le type maitre d''ouvrage
* Evolution #580: Augmenter la taille d''un export à 5Mo

h2. Particularités

URR + DOL + tous les composants uniface

h3. JS

* #580 : rtf2pdfdarva.js

h3. SQL 

@Sabrina, prendre en compte lors de la livraison effective

* #567 ALTER CPXOPE
* #568 ALTER CPXIOP
* #219 CPXASP.BAT, UPDATE CPXAS,P INSERT CPXPTA

+ 

* #484 ALTER CPXDOS

h3. DEX

@Guillaume, surveiller les impacts sur les scripts DEX
* #490 Gsicass - envoyer les notes d''honoraires 
* #422 Gsicass - OM - Gestion du bloc pièce jointe',null,null,5,4,4,46,4,5,'2012-08-02 11:11:27','2012-09-10 11:10:07',null,100,16,null,605,1,2,0,'2012-09-10 11:10:07');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (606,2,6,'Hermes: Le script XFERTQBE_HERMES doit pouvoir gérer le mode paresseux','Le script XFERTQBE_HERMES doit pouvoir gérer le mode paresseux',null,34,5,17,4,99,11,3,'2012-08-02 14:12:56','2013-02-13 11:59:14','2012-08-02',100,null,null,606,1,2,0,'2012-11-14 16:46:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (607,1,4,'OM - Le bouton affecter a disparu','Suite à la dernière mise à jour, la plateforme nous informe d''une régression sur ce qui a été fait dans le sujet #388',null,36,5,null,4,61,4,3,'2012-08-02 17:07:30','2012-08-03 09:54:39','2012-08-02',100,null,null,607,1,2,0,'2012-08-02 17:34:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (608,2,6,'Darva envoyer l''accusé de reception pour chaque OM','il faut generer le message Accuse de Reception d''OM le message darva concerné est le SD 16
ce message sera envoyé à partir d''un fichier déclencheur ',null,32,5,17,4,87,17,5,'2012-08-03 11:08:35','2013-04-02 16:14:25','2012-08-03',90,null,null,608,1,2,0,'2013-04-02 16:14:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (609,2,2,'Bouton Gsicass en couleur','Serait-il possible de mettre une couleur au bouton Gsicass afin d''interpeler l''assistante en effet, Villescrene a zappé l''envoi d''un rapport via Gsicass','2012-08-03',16,5,5,4,46,6,4,'2012-08-03 11:45:59','2012-09-09 09:59:17','2012-08-03',100,null,null,609,1,2,0,'2012-08-09 12:21:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (610,2,6,'Darva : Crééer table dans référentiel pour les numéros abonnés assuré','crééer une table contenant les codes acam des sociétés d''assurance et leur code abonné Darva',null,32,5,null,4,87,17,6,'2012-08-03 11:46:41','2013-04-02 16:14:25','2012-08-03',90,null,null,610,1,2,0,'2013-04-02 16:14:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (611,2,2,'Darva - accusé de reception d''un OM','h2. Declenchement

L''AR retour vers Darva doit être déclenché automatiquement lorsque l''utilisateur crée un dossier à partir d''un OM Darva.
Il doit y avoir une trace dans les envois (CPXCHRONO - mode d''envoi Darva)


h2. Fichier déclencheur

Il faut que CP2000 génère un fichier déclencheur de type texte pour les envois d''accusé de reception d''OM vers Darva
le nom du fichier doit contenir les informations suivantes :
* Code Agence
* la Chaine "SD"
* le code "16"
* Référence Sinistre
* Numéro Mission
* Numéro envoi
* Site de gestion

Exemple de nom de fichier : 
<pre>0830_SD_16_TESTCONSTRUVABF2_1_8300000039117_222.txt </pre>
','2012-11-04',33,5,17,4,59,17,30,'2012-08-03 16:48:16','2012-12-16 08:36:47',null,90,24,769,769,8,9,0,'2012-12-14 16:19:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (612,2,6,'Darva : reception/annulation OM spécificité AXA','permettre au script développé pour SMABTP de gérer les OMs provenant d''AXA',null,32,5,17,4,87,17,5,'2012-08-06 10:20:37','2013-04-02 16:14:25','2012-08-06',90,null,null,612,1,2,0,'2013-04-02 16:14:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (613,3,10,'Préparations','* de réunions',null,null,4,null,4,null,4,0,'2012-08-06 10:55:20','2012-08-06 10:55:20',null,0,null,null,613,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (614,2,6,'Darva : rapports (preliminaire, définitif, unique) Spécificités AXA','mettre à jour le script darvaenvoilettrerapport pour qu''il puisse gérer les rapports pour AXA',null,32,5,17,4,87,17,6,'2012-08-06 11:29:28','2013-04-02 16:14:26','2012-08-06',90,null,null,614,1,2,0,'2013-04-02 16:14:26');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (615,2,2,'Darva - Détail d''un OM','Lors de la réception d''un ordre de mission Darva, les données reçues sont stockées au format XML dans un document.

Il faut pouvoir les lire dans un écran. Faire semblable au CPXG097 existant pour Gsicass.


Travailler à partir de l''exemple en piece jointe.
Afficher le plus de champs possible intéressant pour l''utilisateur.
Voir doc : \\\\10.31.2.37\\Progi\\Projets\\DataExchanger\\webservices_doc_pour_presta\\Lot2-Darva\\doc_darva\\Norme_Construction\\CST_GTM_OM_080101_20100421.pdf',null,33,5,5,4,59,4,45,'2012-08-06 13:44:16','2012-12-16 08:36:47',null,90,80,769,769,6,7,0,'2012-12-12 17:21:13');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (616,2,2,'Choix client - ne pas autoriser de travailler avec un client inactif','Interdire la validation d''un document ou d''une NH si un client mineur est inactif.',null,60,5,5,4,59,4,16,'2012-08-06 14:01:45','2012-12-16 08:36:44',null,60,8,null,616,1,2,0,'2012-11-23 16:46:45');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (617,2,2,'Darva - Envoi note d''honoraire','h2. Demande 

En plus des 3 messages pour les rapports, il faut pouvoir envoyer la note d''honoraire vers Darva
On ne peut pas joindre de document à une message NH Darva (Pas de PJ NH possible)

h2. Déclenchement

L''utilisateur choisira le type NH.

Toute trace d''un envoi doit être enregistrée dans CPXCHRONO (mode d''envoi darva)

h2. Fichier déclenchement

il faut que CP2000 génère un fichier déclencheur de type texte pour les envois de rapport vers Darva
le nom du fichier doit contenir les informations suivantes :
* Code Agence
* Type = NH
* Numéro NH en agence
* Référence Sinistre
* Numéro Mission
* Numéro envoi
* Site de gestion
* exemple de nom de fichier : 

0830_NH_TESTCONSTRUVABF2_1_8300000039117_222.txt

à l''intérieur de ce fichier doit être vide.','2012-11-04',33,5,11,4,59,4,29,'2012-08-06 14:07:59','2012-12-16 08:36:47',null,90,16,769,769,4,5,0,'2012-12-14 16:21:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (618,2,2,'Darva - Reception des messages en attente','h2. Demande

Permettre à l''utilisateur de pouvoir visualiser les messages en provenances du systeme DARVA.

La réception des message se fait dans un onglet prévu à cet effet

!CPXT-OM.png!

!CPXT-MA.png!

Le message est automatiquement archivé dans les documents du dossier (à classer dans correspondance de la compagnie)
Le détail du message est consultable par un double clic sur la ligne qui nous intéresse, voir ci dessous.

!CPXT-MA-details.png!

Sur clic sur le bouton "traité", le message disparait de la liste de l''écran précédent.
Il faut donc gérer 2 états : message reçu, message lu
','2012-11-04',33,5,5,4,59,4,10,'2012-08-06 14:10:30','2012-12-16 08:36:47',null,90,80,769,769,2,3,0,'2012-12-14 16:21:38');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (619,1,2,'Champ N°OM qui réapparait sans raison','Ecran CPXG001 création d''une mission hors DARVA [enregistrer sans quitter] --> OK puis clic dans Enrichissement pour ouverture écran CPXG021 PUIS je reviens sur Ecran CPXG001 --> ANOMALIE : affichage de Ordre de mission (voir copie d''ecran)

!ANO1.PNG!',null,16,5,5,4,46,5,4,'2012-08-06 15:02:04','2012-09-09 09:56:54','2012-08-06',100,null,null,619,1,2,0,'2012-08-09 12:22:47');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (620,2,2,'Prise en compte du format Alpha Numérique des N° OM DARVA dans la création de mission DARVA à partir d''un OM DARVA','Le n° d''OM (DOSDRVOM) affiche 0 quand on crée une mission à partir d''un OM DARVA dans les écrans CPXG001 et CPXG002.
Il faut modifier l''intégration des données de l''OM à la création d''une mission à partir d''un OM DARVA pour prendre en compte que DARVA envoi des n°OM Alpha Numérique.

Le cas ne se présente pas pour GSICASS car ils n''envoient que des OM avec des n° d''OM en numérique.',null,33,5,5,4,46,5,4,'2012-08-06 15:45:21','2012-09-09 09:59:31','2012-08-06',100,null,null,620,1,2,0,'2012-08-09 12:19:04');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (621,2,2,'Paramétrage Assureurs Ecran CPXG088 base Siegetest',' --> si possible modifier l''écran suivant pj',null,null,6,null,4,null,6,1,'2012-08-06 17:03:25','2012-08-09 16:09:12','2012-08-06',0,null,null,621,1,2,0,'2012-08-09 16:09:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (622,2,2,'Paramétrage Assureurs  base Siegetest','Possibilité d''afficher la liste des Assureurs créés dans l''écran CPXG088 ',null,null,6,null,4,null,6,1,'2012-08-06 17:05:07','2012-08-09 16:09:44','2012-08-06',0,null,null,622,1,2,0,'2012-08-09 16:09:44');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (623,1,6,'Script cp2000-mailsdifferes.vsl  une tabulation est insérée dans l''objet du mail','Une tabulation est insérée dans l''objet du mail',null,28,5,11,4,96,11,12,'2012-08-07 16:56:21','2013-02-13 11:09:33','2012-08-07',100,null,null,623,1,2,0,'2012-11-07 14:46:00');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (624,3,6,'DeX: DataExchanger Anomalies','Regroupe l''ensemble des anomalies remontées chez DeX',null,31,2,null,4,null,11,12,'2012-08-07 17:04:07','2013-04-26 15:06:34','2012-08-07',0,null,null,624,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (625,2,6,'Darva : Insertion OM, Annulation OM, Receptio Commentaire Reprise sur erreur','ajout de la fonctionnalité de reprise sur erreur en gardant l''ordre de reception des actions à effectuer
script concerné : darvatraitementmissions ',null,32,5,17,4,87,17,4,'2012-08-08 10:45:07','2013-04-02 16:14:26','2012-08-08',90,null,null,625,1,2,0,'2013-04-02 16:14:26');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (626,2,5,'Revoir la chartes afin de pouvoir moduler les CSS selon les profils','1 - Créer une css structurer
2 - Envisager de paramétrer les css par profil : structure BD + Thème CSS',null,null,5,18,4,null,10,3,'2012-08-09 09:30:34','2012-09-13 16:47:33','2012-08-09',100,null,342,342,22,23,0,'2012-09-10 08:38:47');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (627,2,6,'Darva : Pour les rapports alimenter le type maitre d''ouvrage et les infos complémentaires','le type de maitre d''ouvrage ainsi que le role de l''intervenant chantier doivent être renseigné dans le message ',null,32,5,17,4,87,17,5,'2012-08-09 11:29:29','2013-04-02 16:14:26','2012-08-09',90,null,null,627,1,2,0,'2013-04-02 16:14:26');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (628,1,2,'Gestion CRAC/Sycodès','Cette anomalie a été relévée pendant les tests, mais elle existe actuellement sur les bases en Agence : Lorsque l''indemnité est > 1500 le message signalant qu''une fiche CRAC/Sycodès doit être réalisée ne s''affiche pas. 
Il faut enregistrer sans quitter pour avoir accès au bouton Gestion Crac/Sycodès (il reste grisé)
Si on n''ouvre pas l''écran CPXG262 (Gestion des fiches CRAC/sycodes)--> aucun message ne nous informe qu''une fiche est à établir et à valider --> le dossier peut être mis en Etat "Mission terminée" (voir dossier 311823)',null,null,1,null,4,null,6,0,'2012-08-10 16:31:55','2012-08-10 16:31:55','2012-08-10',0,null,null,628,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (629,1,2,'Suppression d''un mail envoyé Ecran CPXL266','A partir de l''écran CPXL266-Liste des envois : on peut supprimer un mail envoyé. ce qui ne devrait pas être possible d''autant plus qu''à partir de l''onglet Documents/Mails on ne peut pas le supprimer --> I_NOSUPCHRONO - Ce document a été envoyé par courrier et/ou par messagerie. Suppression impossible- 
Voir dossier 311774 fait par Karine qui signale que cette anomalie existe dans les agences 
Au sujet du message qui s''affiche corriger la faute de conjugaison +à+ été envoyé',null,null,5,5,4,59,6,6,'2012-08-10 16:38:49','2012-12-16 08:36:45','2012-08-10',90,null,null,629,1,2,0,'2012-12-13 12:25:50');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (630,1,6,'GSICASS: Lors de la gestion du routage semi automatique le fichier ListeMissions dans le répertoire archive  n''est pas supprimé','Lors de la gestion du routage semi automatique le fichier ListeMissions n''est pas supprimé, or le split de fichier est correctement exécuté',null,40,1,null,4,122,11,3,'2012-08-16 16:08:23','2013-04-02 16:35:31','2012-08-16',0,null,null,630,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (631,2,6,'GSICASS: Le mail de demande de routage doit inclure les pièces jointes de l''OM','Le mail de demande de routage doit inclure les pièces jointes de l''OM',null,40,5,null,4,122,11,3,'2012-08-16 16:09:44','2013-04-26 15:31:45','2012-08-16',100,null,null,631,1,2,0,'2013-04-26 15:31:45');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (632,1,6,'GSICASS: Dans le traitement des OMs de GSICASS l''adresse 3 est concaténée avec l''adresse 4 de l''OM or cette approche entraine un dépassement de capacité du champ adresse3 de CP2000','Dans le traitement des OMs de GSICASS l''adresse 3 (chantier) est concaténée avec l''adresse 4 de l''OM or cette approche entraine un dépassement de capacité du champ adresse3 de CP2000.

Solution envisageable:

Ajouté un champ adresse 4 dans cp2000 (Voir avec vincent)
',null,40,6,11,4,122,11,5,'2012-08-16 16:14:04','2013-05-13 11:57:27','2012-08-16',0,null,null,632,1,2,0,'2013-05-13 11:57:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (633,2,2,'Bloquer la  validation des documents suivants si enjeu non complété','ES058
ES059
ES061
ES204
ES222
ES226
ES548
ES604
ES789
ES790
ES791
ES792
ES793
ES813
ES837
ES851
ES852
ES894
ES905_
',null,5,5,5,6,46,6,10,'2012-08-17 11:16:29','2012-09-09 09:59:44','2012-08-17',100,null,null,633,1,2,0,'2012-08-27 08:55:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (634,2,6,'DeX: Documentation pour developpeur','Documentation pour developpeur deX',null,31,5,17,4,98,11,4,'2012-08-17 15:21:30','2013-02-13 11:59:32','2012-08-17',90,null,null,634,1,2,0,'2012-11-30 15:58:08');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (635,2,6,'DeX: Documentation représentation graphique script DeX','Documentation représentation graphique script DeX. Une représentation graphique exprimant les dépendances entre acteurs et flux doit être élaborée',null,31,2,17,4,139,11,6,'2012-08-17 15:25:54','2013-05-13 11:37:57','2012-08-17',90,null,null,635,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (636,2,2,' Ajout règles de gestion pour le blocage de la validation du rapport','actuellement, blocages : 
- 1er enjeu initial non renseigné ou 0
- Enjeux non renseignés en totalité
- enjeu HT est égal à l''enjeu initial

peut-on rajouter les blocages suivants : 
- position Garanti ou Non garanti ou Voir client si non renseignés 
- cf #469 -l''enjeu doit être renseigné sur tous les dommages garantis (un dommage non renseigné --> on bloque)-
- cf #469 -l''enjeu total non garanti doit être renseigné sur le 1er dommage non garanti- 

avec affichage du message suivant:  
Avant toute validation de rapport, renseigner l''enjeu total non garanti dans le 1er dommage non garanti, et l''enjeu (et l''indemnité si RU) dans chaque dommage garanti.



 ',null,5,2,5,4,111,6,13,'2012-08-17 17:26:52','2013-03-05 18:34:43',null,0,8,null,636,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (637,2,6,'DeX: Mise en oeuvre d''une librairie pour la gestion des statistiques','Une librairie pour la gestion des statistique',null,31,5,17,4,98,11,5,'2012-08-21 15:51:18','2013-02-13 12:00:05','2012-08-21',90,null,null,637,1,2,0,'2012-11-30 15:56:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (638,1,2,'OM Gsicass création nouvelle police et mission enregistrée sur une police existante Base de lyon dossier 0690/338486','Ecran CPXG001 Création d''une police et mission suite nouvel OM : l''opération et la police n''existent pas.
Après [enregistrer sans quitter] n° dossier est créé mais pas celui du n°G. je vais enrichissement dossier pour consulter l''opération quand j''ouvre l''écran CPXG009 un message s''affiche demandant un N° de G. Je quitte et reviens sur l''écran CPXG001 --> enregistrement de la mission sous une police existante (donc un n° différent) et avec n°G existant.',null,4,4,5,6,null,6,4,'2012-08-21 17:22:45','2013-05-16 11:01:11','2012-08-21',0,null,null,638,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (639,2,6,'DARVA : Ajout du commentaire emis par l''assureur en base ','lorsqu un commentaire est émis de la part de l''assureur il faut l''insérer dans la base CP2000',null,32,5,17,4,87,17,6,'2012-08-22 10:33:48','2013-04-02 16:14:26','2012-08-22',90,null,null,639,1,2,0,'2013-04-02 16:14:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (640,1,2,'Anomalie entre base SiègeTest et Base de référence et production des documents type EX (à rapprocher de l''anomalie 530)','voir PJ ',null,null,5,5,6,null,6,7,'2012-08-22 12:13:05','2012-08-28 09:48:28','2012-08-22',0,null,null,640,1,2,0,'2012-08-28 09:48:28');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (641,2,5,'Creer une configuration par utilisateur','Chaque utilisateur se verra attribuer une et une seule configuration
Dans l''objet configuration, nous n''auront aujourd hui qu un lien vers un theme
La table des themes sera saisie dans la base de données par l''administrateur - elel contiendra : le nom du theme, le dossier du theme et si le theme est un theme par défault, ainsi que les dates de dispo du theme (date de début et date de fin) ',null,null,5,18,4,null,10,2,'2012-08-22 12:21:17','2012-09-10 08:38:30','2012-08-22',100,null,371,365,29,30,0,'2012-09-10 08:38:30');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (642,1,2,'HERMES - Anomalie Indexation documents Ext. 6.21 (à rapprocher certainement des anomalies 640 et 530)','anomalie Indexation 6.21 du libellé HERMES et de la remontée sur HERMES voir copies d''écran faite sur dossier 0830/311896',null,null,2,4,6,null,6,5,'2012-08-22 12:39:24','2013-05-16 11:01:11','2012-08-22',0,null,null,642,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (643,1,4,'PB Remontées sur HERMES des Documents techniques Police DO ou CMI par aliment et police RC/RCD','pour ces polices, il y a des opérations de construction différentes donc des documents techniques différents par mission,  or sur HERMES sur chaque dossier dans Documents Techniques ont retrouve tous les documents de toutes les missions
Exemple : 
0130/135931/DO --> CP2000 = pas de document technique en 6 --> HERMES = 16 documents qui ne concernent pas du tout l''opération 
0330/65441/RCA --> CP2000 = 1 dt en 6.14 -- HERMES --> 2 documents 6.14 La facture du 02 08 11 concerne dossier 0330/64693/RCDA ',null,null,1,4,6,null,6,5,'2012-08-22 15:33:21','2012-08-28 14:20:27','2012-08-22',0,null,null,643,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (644,1,2,'HERMES - Problème de consultation de dossiers dans Hermes à partir de CP2000','Est-il normal qu''à partir du dossier CP2000 0130/135931 quand je consulte HERMES ça n''ouvre pas directement le dossier concerné --> Je trouve une liste de 48 dossiers traités par diverses agences EURISK?? ',null,null,5,4,4,null,6,4,'2012-08-22 15:37:39','2012-08-27 10:23:43','2012-08-22',0,null,null,644,1,2,0,'2012-08-27 10:23:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (645,2,6,'GSICASS: Ajouter les stats sur insertion OM','Ajouter les stats sur insertion OM',null,40,5,11,4,90,11,2,'2012-08-22 17:04:20','2013-02-13 12:00:23','2012-08-22',100,null,null,645,1,2,0,'2012-12-12 10:02:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (646,2,2,'Ajout filtrage Origine/Expéditeur à partir de l''onglet Documents Ext. ','Actuellement aucun filtrage n''est possible par libellé Origine ou Expéditeur.
Voir copies d''écrans suite à la demande de P. VERQUIERE',null,5,5,5,5,59,6,5,'2012-08-27 10:00:36','2012-12-16 08:36:45','2012-08-27',90,null,null,646,1,2,0,'2012-09-28 17:44:05');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (647,1,2,'pb consultation Documents Police (type RC/RCD/MGAE) Ecran CPXL100','Le document indexé 6.02 ne s''affiche pas s''il n''a pas été numérisé dans le dossier que l''on consulte. 
Exemple : consultation de la police 20516013406587 à partir de l''écran CPXG008
--> curseur sur le dossier 305548 = pas de document 6.02 dans l''écran CPXL100
--> curseur sur le dossier 307565 = affichage document 6.02 numérisé dans 307565
--> curseur sur le dossier 308389 = affichage document 6.02 numérisé dans 308389

Ces attestations devraient s''afficher à la consultation de l''écran CPXL100 à partir de tous les dossiers',null,5,5,5,4,null,6,5,'2012-08-27 11:36:33','2013-02-11 12:26:07','2012-08-27',100,null,null,647,1,2,0,'2013-01-21 14:28:39');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (648,2,5,'Page de recherche d''antériorité','Permettre la recherche d''antériorité d ''un dossier!!',null,null,5,18,4,null,10,3,'2012-08-28 11:49:10','2012-09-13 16:29:50','2012-08-28',100,null,391,391,4,5,0,'2012-09-10 08:38:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (650,1,6,'CP20000: cp2000-mailsdifferes ajout d''une gestion erreur EC  "sans fin" pour brique S3/P19','cp2000-mailsdifferes ajout d''une gestion erreur EC  "sans fin" pour brique S3/P19',null,28,5,17,4,96,11,3,'2012-08-29 11:53:29','2013-02-13 11:09:56','2012-08-29',100,null,null,650,1,2,0,'2012-11-07 14:45:13');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (651,1,2,'Ecran CPXL267 - détail d''un envoi : erreur date des documents Ext. ','dans l''écran CPXL267 -Détail d''un envoi pour les documents Ext. c''est la date de validation du document qui s''affiche (càd la date de la numérisation ou de l''intégration du document) alros que ça devrait être la date de création.

!CPXL267.PNG!

!508342.PNG!',null,null,1,null,4,null,6,1,'2012-08-29 12:18:13','2012-08-29 12:53:31','2012-08-29',0,null,null,651,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (652,2,2,'Separation des documents - info sur l''ordre de séparation','Lors d''une séparation des documents, sur le retour dans l''écran des documents externes, on ne sait plus vraiment quel est l''ordre des divers nouveau documents générés.
Il faut garder une trace de l''ordre de séparation en utilisant le champs CPXDOC prévu à cet effet : DOCSUITE.

!CPXL269_docsuite.png!',null,5,5,5,4,59,4,4,'2012-08-30 14:42:32','2012-12-16 08:36:45','2012-08-30',90,null,null,652,1,2,0,'2012-12-14 16:16:15');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (653,1,6,'GSICASS: Le mail de routage met fin au script or ce dernier doit continuer son traitement pour les autres OM ','Le mail de routage met fin au script or ce dernier doit continuer son traitement pour les autres OM ',null,40,5,11,4,90,11,3,'2012-09-03 09:56:35','2013-02-13 11:10:43','2012-09-03',100,null,null,653,1,2,0,'2013-01-17 12:00:34');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (654,1,4,'pb regroupement client mineur','Lorsque l’on crée une fiche utilisateur Hermès de niveau client mineur sur Hermès Construction, le code client majeur et le code client mineur sont obligatoires.
Ceci ne devrait pas (régression) car nous pouvons créer des regroupements de clients mineurs.

Ticket CiteSI 268',null,67,5,null,4,45,4,2,'2012-09-03 11:20:44','2012-10-08 11:33:20','2012-09-03',100,null,null,654,1,2,0,'2012-10-08 11:33:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (655,1,4,'Client mineur - filtrage recherche HS','Lorsque l’on veut sélectionner un code client mineur dans l’écran de création d’un utilisateur de niveau client mineur, la zone de recherche ne fait strictement rien

!selectionclientmineur.png!',null,67,1,null,4,null,4,0,'2012-09-03 11:27:52','2012-09-03 11:27:52','2012-09-03',0,null,null,655,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (656,2,12,'Gerer l''envoi de mail','Gerer l''envoi de mail en donnant le destinataire, le sujet, le corps du message et les pieces jointes',null,null,5,17,4,57,17,2,'2012-09-03 17:05:32','2013-02-13 14:25:07','2012-08-31',100,null,null,656,1,2,0,'2013-02-13 14:25:07');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (657,2,5,'Créer la vue sur un dossier','',null,null,2,8,4,null,10,5,'2012-09-04 08:39:06','2013-03-25 11:27:27','2012-09-04',20,null,391,391,6,7,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (658,2,5,'Création d''un dossier','',null,null,1,null,4,null,10,4,'2012-09-04 08:39:35','2013-04-17 14:41:37','2012-11-09',100,null,391,391,8,11,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (659,2,6,'Script EnvoiMailNotif mettre à jour avec l envoi de mail par exchange','Mettre à jour le script EnvoiMailNotif pour qu''il envoi les mails avec ExchangeCommander',null,31,5,17,4,103,17,4,'2012-09-04 09:28:55','2013-02-13 14:41:15','2012-09-03',100,null,null,659,1,2,0,'2013-02-13 14:41:15');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (660,2,6,'Script CP2000AlerteMailDocumentExpert envoi de mail avec ExchangerCommander','Pour que les mails ne soient par redirigés vers une autre boite que celle de l expert
utiliser le WS Exchange pour l''envoi de mail
Ajouter aussi en copie du mail l''email de l''agence concerné',null,31,5,17,4,85,17,4,'2012-09-04 14:59:37','2013-02-13 12:01:28','2012-09-03',100,null,null,660,1,2,0,'2012-11-07 14:32:41');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (661,1,15,'Page de Login et MySql connection','-	Page de LOGIN, les messages d’erreurs ne fonctionnent plus – nous avons regardé depuis quand les erreurs sont apparu, mais sans succès #375
	Lorsque je coupe le service MySql et que je tente de me connecter, j’ai cette erreur  - C’est OK dans le cas d’une mauvaise saisie et d’un accès a LDAP coupé
	Fatal error: Class ''IMAG\\LdapBundle\\Provider\\AuthenticationException'' not found in C:\\wamp\\www\\cpweb\\src\\IMAG\\LdapBundle\\Provider\\LdapUserProvider.php on line 85
','2012-09-07',null,5,8,4,40,10,3,'2012-09-06 15:40:11','2012-09-06 16:27:25','2012-09-06',100,null,null,661,1,2,0,'2012-09-06 16:27:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (662,1,15,'Client Majeur et Detail du client','-	Rajouter le lien clientmajeur dans le client 
	LE client majeur n’apparait pas dans le détail du client
','2012-09-07',null,5,8,4,40,10,3,'2012-09-06 15:41:11','2012-09-06 16:26:37','2012-09-06',100,null,null,662,1,2,0,'2012-09-06 16:26:37');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (663,1,15,'Corriger les erreurs d''accessibilité','Voir le document d''accessibilité et me le retourner remplit',null,null,5,18,4,40,10,2,'2012-09-06 15:42:07','2012-09-06 16:38:11','2012-09-06',100,null,null,663,1,2,0,'2012-09-06 16:27:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (664,1,15,'Liste des users','Retirer le user migration de la liste des user',null,null,5,8,4,40,10,2,'2012-09-06 15:43:08','2012-09-07 08:36:11','2012-09-06',100,null,null,664,1,2,0,'2012-09-07 08:36:11');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (665,1,15,'User Role Agence','Meme si elle est non défini, il faut ajouter la date de fin dans les détails du user role agence',null,null,5,8,4,40,10,2,'2012-09-06 15:45:25','2012-09-06 16:25:41','2012-09-06',100,null,null,665,1,2,0,'2012-09-06 16:25:41');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (666,1,15,'Utilisateur - Nom et Civilité','Lors de l''ajout et la modification d''un utilisateur, mettre la civilité et le nom sur deux lignes distinctes avec deux labels distincts',null,null,5,8,4,40,10,2,'2012-09-06 15:47:02','2012-09-07 08:37:33','2012-09-06',100,null,null,666,1,2,0,'2012-09-07 08:37:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (667,2,16,'Voir les spécifications','1 - Lire le document joint
2 - Voir le commentaire associé de Vincent (MAIL)
3 - Regarder la structure de la BD
4 - Proposer une réunion si nécessaire avec les questions à poser - ci-besoin

','2012-09-10',null,4,8,5,null,10,4,'2012-09-07 09:35:54','2012-09-10 15:39:33','2012-09-07',0,8,null,667,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (668,2,16,'Structure de projet','1 - Voir avec Vincent pour la création d''un dépot SVN
2 - Le projet sera déployer sous le Framework Symfony combiné avec Le framework Javascript JQueryUI (Date Picker)
3 - Des tests unitaires seront requis avec PHPUNIT


Étapes du projet
1 - Déployer un structure de Symfony2 vierge
2 - Lier cette structure au dépôt SVN
3 - Créer les entités et modèles liés au modèle MySQL - Nous nous adaptons à un modèle existant et non l''inverse
4 - Proposer une interface de saisie des critères basique  (SANS CSS, SANS JS)
5 - Créer la requête associée multicritère 
6 - Proposer la sortie tabulaire uniquement
7 - Organiser une validation des résultats avec l''équipe de Guillaume
8 - Proposer une feuille de style pour le visuel de l''application WEB
9 - Faire une étude (documentation) sur les outils de reporting graphique du coté des librairies de PHP 
10 - Organiser une réunion dev. pour valider la sélection de la librairie - licence, option et rendu
11 - Intégrer cette librairie pour proposer la sortie graphique
12 - Proposer la sortie CSV en téléchargement de fichier
13 - Intégrer JQueryUI
14 - Intégrer le DatePicker de Jquery
15 - Développer un CSS d''impression - Voir les besoins de Guillaume et Vincent lors de cette étape - proposition de maquette et validation de Guillaume
15 - S''assurer que toute l''application est valide W3C et conforme au spécification d''accessibilité
','2012-09-28',null,1,8,4,null,10,40,'2012-09-07 09:50:24','2012-09-18 18:25:53','2012-09-07',100,150,null,668,1,36,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (669,3,16,'Création du répertoire SVN','Donner les droits d''accès à David et Sabrina',null,null,5,4,4,null,10,1,'2012-09-07 09:50:59','2012-09-07 10:12:04','2012-09-07',0,null,668,668,2,3,0,'2012-09-07 10:12:04');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (670,2,16,'Déployer une structure Symfony 2 vierge','','2012-09-11',null,5,8,4,null,10,6,'2012-09-07 09:52:03','2012-09-17 16:01:40','2012-09-07',100,4,668,668,4,5,0,'2012-09-17 16:01:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (671,2,16,'Lier cette structure au dépôt SVN','','2012-09-11',null,5,8,4,null,10,5,'2012-09-07 09:52:21','2012-09-17 16:01:25','2012-09-07',100,4,668,668,6,7,0,'2012-09-17 16:01:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (672,2,16,'Créer les entités et modèles liés au modèle MySQL - Nous nous adaptons à un modèle existant et non l''inverse','','2012-09-13',null,10,8,4,null,10,4,'2012-09-07 09:52:37','2012-09-17 16:02:02','2012-09-10',100,16,668,668,8,9,0,'2012-09-17 16:02:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (673,2,16,'Proposer une interface de saisie des critères basique (SANS CSS, SANS JS)','','2012-09-14',null,2,8,4,null,10,9,'2012-09-07 09:52:58','2012-09-18 13:55:38','2012-09-12',100,8,668,668,10,11,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (674,2,16,'Créer la requête associée multicritère ','','2012-09-14',null,9,8,4,null,10,9,'2012-09-07 09:56:53','2012-09-17 17:18:13','2012-09-13',100,8,668,668,12,13,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (675,2,16,' Organiser une validation des résultats avec l''équipe de Guillaume','','2012-09-19',null,5,8,4,null,10,4,'2012-09-07 09:57:19','2012-09-17 16:24:42','2012-09-13',100,4,668,668,14,15,0,'2012-09-17 16:24:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (676,2,16,' Proposer une feuille de style pour le visuel de l''application WEB','','2012-09-19',null,10,8,4,null,10,5,'2012-09-07 09:58:19','2012-09-17 16:26:57','2012-09-14',100,8,668,668,16,17,0,'2012-09-17 16:26:57');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (677,2,16,'Faire une étude (documentation) sur les outils de reporting graphique du coté des librairies de PHP ','','2012-09-21',null,5,8,4,null,10,6,'2012-09-07 09:58:37','2012-09-17 16:27:41','2012-09-17',100,24,668,668,18,19,0,'2012-09-17 16:27:41');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (678,2,16,' Organiser une réunion dev. pour valider la sélection de la librairie - licence, option et rendu','','2012-09-21',null,5,8,4,null,10,3,'2012-09-07 09:59:02','2012-09-17 16:28:10','2012-09-18',0,4,668,668,20,21,0,'2012-09-17 16:28:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (679,2,16,' Intégrer une librairie PHP pour proposer la sortie graphique','','2012-09-25',null,10,8,4,null,10,4,'2012-09-07 10:00:05','2012-09-17 16:29:54','2012-09-20',100,24,668,668,22,23,0,'2012-09-17 16:29:54');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (680,2,16,'Proposer la sortie CSV en téléchargement de fichier','','2012-09-26',null,10,8,4,null,10,4,'2012-09-07 10:00:31','2012-09-17 16:31:28','2012-09-24',100,8,668,668,24,25,0,'2012-09-17 16:31:28');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (681,2,16,' Intégrer JQueryUI','','2012-09-26',null,10,8,4,null,10,4,'2012-09-07 10:00:56','2012-09-18 13:58:17','2012-09-24',100,2,668,668,26,27,0,'2012-09-18 13:58:17');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (682,2,16,'Intégrer le DatePicker de Jquery','','2012-09-26',null,10,8,4,null,10,5,'2012-09-07 10:01:25','2012-09-18 13:58:39','2012-09-24',100,4,668,668,28,29,0,'2012-09-18 13:58:39');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (683,2,16,'Développer un CSS d''impression - Voir les besoins de Guillaume et Vincent lors de cette étape - proposition de maquette et validation de Guillaume','','2012-09-28',null,9,8,4,null,10,3,'2012-09-07 10:01:43','2012-09-18 18:25:53','2012-09-26',100,8,668,668,30,31,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (684,2,16,'S''assurer que toute l''application est valide W3C et conforme au spécification d''accessibilité','','2012-09-28',null,10,8,4,null,10,6,'2012-09-07 10:02:02','2012-09-17 16:35:59','2012-09-10',100,16,668,668,32,33,0,'2012-09-17 16:34:13');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (685,2,16,'Proposer la sortie tabulaire uniquement','','2012-09-17',null,10,null,4,null,10,3,'2012-09-07 10:04:32','2012-09-17 16:34:31','2012-09-13',100,8,668,668,34,35,0,'2012-09-17 16:34:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (686,3,16,'Autre - Gestion, réunion, échange question','',null,null,1,10,4,null,10,0,'2012-09-07 16:16:57','2012-09-07 16:16:57','2012-09-07',0,null,null,686,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (687,2,6,'Stream Reporting','Spécification sur projet IHM WEB de reporting ',null,68,5,null,4,102,11,2,'2012-09-10 10:08:27','2013-02-13 12:02:00','2012-09-10',100,null,null,687,1,2,0,'2012-11-30 16:10:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (688,1,2,'Dossier 0060/621199/doa  Impossible de sélectionner Assureur Recours','Ecran CPXL077 --> coche Assureur AM TRUST INTERNATIONAL "Actif" + E_STONOK - une anomlaie a été détectée pendant l''enregistrement --> Ejectée de CP2000.
Pour info, test = OK avec autre assureur CRAC ou HORS CRAC. 
le pb ne vient-il pas du fait que c''est un assureur que l''on a modifié de Minuscule à Majuscule? ',null,5,5,4,6,63,6,2,'2012-09-10 10:40:45','2012-09-10 15:39:29','2012-09-10',100,null,null,688,1,2,0,'2012-09-10 15:39:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (689,1,2,'controle saisie darv a trop sévère','Ne contrôler que les "futurs" dossiers Darva "D"',null,5,5,4,4,63,4,1,'2012-09-10 15:28:18','2012-09-10 15:39:23','2012-09-10',100,null,null,689,1,2,0,'2012-09-10 15:39:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (690,1,2,'Saisie non sauvegardé du maitre ouvrage dans les operations','Villecresnes - JC Trischler

Impossible de sauvegarder le type maitre d''ouvrage.
Menu enrichissement de l''opération --> saisie G = 5698
Je renseigne la liste.
Je sauve et je ferme.
Je reviens, liste non renseignée','2012-09-11',29,5,4,4,64,4,2,'2012-09-11 11:53:52','2012-09-11 13:46:53','2012-09-11',100,2,null,690,1,2,0,'2012-09-11 13:46:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (691,1,2,'correction archivage rapport allianz','* manque les 2 points dans l''objet entre NS et numerosinistre 
* Ne rien écrire dans le corps du texte
* analyser le probleme sur PJ manquante dans certains cas compliqués
** cf test karine sur dossier 311772 et 311778
','2012-09-14',6,5,4,4,65,4,4,'2012-09-12 12:04:18','2012-09-16 22:20:46','2012-09-12',100,8,null,691,1,2,0,'2012-09-16 22:20:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (692,1,2,'choix CT HS','On choisi un controleur technique.
CP2000 affiche le dernier intervenant de la liste sans prendre en compte le choix de l''utilisateur

Vu sur 0690/338390 et 0030/189192',null,35,5,4,4,66,4,2,'2012-09-12 16:23:54','2012-09-12 17:09:48','2012-09-12',100,null,null,692,1,2,0,'2012-09-12 17:09:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (693,1,2,'Ecran CPXG123 Envoi de document GSICASS','impossible de consulter les pièces jointes 
un message s''affiche --> 0120 Le champ contient trop de caractères BIDON BT_AJOUTNH',null,5,5,5,6,68,6,9,'2012-09-12 18:37:01','2012-09-18 11:53:43','2012-09-12',100,null,null,693,1,2,0,'2012-09-18 11:53:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (694,1,2,'NH s''affichent dans l''écran CPXL010B accès via le bouton AJOUT PJ','Dans évolution #482, j''avais demandé --> question : lorsque l''on prépare les PJ pour envoi rapport serait-il possible de ne pas aficher la note d''honoraire dans les documents à sélectionner (écran CPXL010B)? puis test OK

',null,null,5,null,6,null,6,1,'2012-09-12 18:54:52','2012-12-17 13:40:33','2012-09-12',0,null,null,694,1,2,0,'2012-12-17 13:40:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (695,2,12,'ExchangeCommander expediteur du mail','gerer l expediteur quand il est précisé dans le fichier paramètre',null,null,5,17,4,57,17,4,'2012-09-13 10:41:47','2013-02-13 14:26:02','2012-09-12',100,null,null,695,1,2,0,'2012-11-07 14:32:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (696,1,2,'ANOMALIE ECRAN CPXL010B Envoi de document (en base de test comme en agence)','Quelque soit le type d''envoi choisi (email, epost, fax, EDI Gsicass....) après la sélection des documents, l''affichage de la liste des documents externes se modifie.
Voir copie d''écran (je ne me rappelle plus faire pour afficher ma pièce faudra que je le note!!)',null,27,5,5,4,59,6,5,'2012-09-13 11:14:03','2012-12-16 08:36:45','2012-09-13',90,null,null,696,1,2,0,'2012-12-14 16:15:44');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (697,2,12,'ExchangeCommander gérer l envoi de mail en SMTP','pour pouvoir spécifier le champs from du message sans qu il soit soumis aux règles de vérification exchange, gérer l''envoi de mail avec le protocole smtp (ne peut être fait avec la brique DEX car celle ci insère une tabulation dans le sujet du message au bout d un certain nombre de caractère',null,null,5,17,4,57,17,4,'2012-09-14 14:28:40','2013-02-13 14:25:33','2012-09-14',100,null,null,697,1,2,0,'2012-11-07 14:31:39');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (698,1,2,'Impossible de valider un rapport, controle sur 1er enjeu non garanti HS','Dossier 0670/268224

> Bonjour,
> L''enjeu est bien renseigné et le message suivant s''affiche.
> Merci de m''indiquer où est l''erreur à corriger.
> Cordialement
> Katia


!controle_enjeux_NG.png!','2012-09-17',9,5,4,4,67,4,2,'2012-09-17 14:04:01','2012-09-17 14:26:43','2012-09-17',80,2,null,698,1,2,0,'2012-09-17 14:26:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (699,2,5,'Gestion de la TVA','Gérer la TVA PAR région',null,null,5,18,4,null,10,4,'2012-09-17 15:21:54','2013-04-17 14:49:21','2012-09-14',100,null,null,699,1,2,0,'2013-04-17 14:49:21');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (700,2,2,'CPXG123 - afficher la date de validation','vu sur dossier 474073 dans l''écran CPXG123 c''est la date de la création du document qui s''affiche laors que ça devrait être celle de sa validation (document créé le 13/09 et validé le 17/09/2012)
!ANOMALIE2CPXG123!
','2012-09-19',27,5,5,4,69,6,6,'2012-09-18 12:36:09','2012-09-19 15:35:52','2012-09-18',100,null,null,700,1,2,0,'2012-09-19 14:05:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (701,2,16,'Documentation','documenter le projet',null,null,9,8,4,null,10,1,'2012-09-18 14:23:04','2012-09-19 11:05:49','2012-09-18',100,4,null,701,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (702,2,16,'Installation sous IISS','Installer et configurer le projet sous iiss - enrichir le "readme.txt" pour cette partie si besoin',null,null,9,8,4,null,10,2,'2012-09-18 14:24:20','2012-10-02 18:20:09','2012-09-20',100,16,null,702,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (703,2,11,'CARD - Export Ardi','',null,null,5,null,4,null,4,1,'2012-09-18 21:43:51','2012-09-19 09:26:26',null,0,null,null,703,1,2,0,'2012-09-19 09:26:26');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (704,1,11,'Dex - scénario d''export','','2012-10-12',null,5,null,4,null,4,1,'2012-09-18 21:46:24','2012-09-19 09:26:26',null,80,null,null,704,1,2,0,'2012-09-19 09:26:26');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (705,2,5,'TICTAC','1 - Proposer uns structure de données détaillées pouvant représenter le processus TICTAC
2 - Une fois la structure finalisée, détailler les insertion / modification / suppression effectuée aux différentes étapes du projet
3 - Envisager un "statut" TIC TAC par défaut pour les objets migrés - que peut-on proposer de cohérent

Le travail fournit résultera d''une concertation en équipe afin de proposer un solution optimale se rapprochant de la demande initiale.',null,null,1,8,4,null,10,7,'2012-09-19 14:24:47','2013-04-17 15:05:29','2012-09-26',100,null,null,705,1,4,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (706,2,6,'Script CP2000AlerteMailDocumentExpert gerer l heure de l envoi du mail selon le fuseau horaire de l agence','selon le parametre indiqué par le processus
METROPOLE
ANTILLES
OCEANINDIEN
faire en sorte de traiter uniquement les agences concernées',null,31,5,17,4,85,17,3,'2012-09-20 11:52:00','2013-02-13 12:04:06','2012-09-20',100,null,null,706,1,2,0,'2012-11-07 14:31:17');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (707,1,2,'OM Gsicass : anomalie création mission sur police existante','Mission créée à partir d''un OM --> manque le bouton qui permet l''insertion automatique de l''adresse de l''opération voir 1ère copie écran 
!ANOMALIECPXG001.PNG!

2ème copie d''écran : création d''une mission à partir de l''écran CPXL040 accès au bouton Adresse
!ANOMALIECPXG0012.PNG!',null,16,5,5,6,70,6,6,'2012-09-20 14:55:57','2012-09-24 09:47:10','2012-09-20',60,null,null,707,1,2,0,'2012-09-24 09:47:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (708,2,2,'Suivi des relances','h2. Objectif 

Permettre de garder une trace du suivi des relances effectué par les agences concernant les notes d''honoraires impayées.

h2. Etats des lieux

Le contrôle de gestion envoi régulièrement, à chaque agence, un etat listant les NH impayées.
Charge à l''agence de traiter cet état, de relancer le client.
Le problème est que l''on ne garde aucune trace du travail effectué.

h2. Idée 

A partir de la Note d''honoraire, permettre à l''utilisateur de noter l''historique de ses relances.
Il faut donc sauvegarder la date, l''utilisateur et l''action effectuée.

h3. ecran

Accès depuis l''onglet des NH, choisir la NH et ensuite appeler l''écran (bouton)

!CP2000-suivirelances.png!

h4. Appel Téléphonique

on peut ajouter un commentaire manuel pour préciser les termes de l''entretien

h4. Email

tout automatiser avec 
* envoi de la NH en pièce jointe 
* sans envoi du rapport (à définir)

ou semi-automatiser (à définir)

h4. FAX

Idem Mail

h4. COURRIER

Validation d''un DT spécifique qui générera une mise à jour automatique de l''historique

h3. Base de donnée

Créer une nouvelle table.
Lier avec la table des NH. ',null,26,1,null,5,null,4,5,'2012-09-21 14:36:52','2013-07-04 22:27:48',null,0,60,null,708,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (709,3,4,'Supprimer le chantier 632236/001','> De : Olivier Kauf [mailto:olivier@betk.com] 
> Envoyé : jeudi 20 septembre 2012 08:52
> À : CPCOM/BOUREZ Vincent
> Cc : Olivier Rambaud; Lenclud, Nathalie; ''Romanet, Jean-Claude''
> Objet : Problèmes et attentes sur Hermes pour QBE
> 
> Bonjour Vincent,
> 
> J’ai noté trois attentes sur Hermes.
> 
> La première attente est de notre fait, il y a un chantier qui a été supprimé et recréé sous un autre code. Le chantier avec son nouveau code est bien présent sur Hermes, mais l’ancien également.
> Pourriez le retirer de Hermes, le chantier dont le code est 632236/001 (SCI LE DOUBLON COURBEVOIE). 
> [...]',null,51,5,4,4,null,4,1,'2012-09-21 15:06:59','2012-09-21 15:17:19','2012-09-21',100,2,null,709,1,2,0,'2012-09-21 15:17:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (710,1,2,'Ecran CPXL076 : affichage de l''adresse est incomplet','dans l''écran CPXL076 détail de l''adresse : manque l''affichage de la 1ère ligne 
!ECRANCPXL076.PNG!
il faut que le détail de l''adresse s''affiche avec le Service comme sur l''écran CPXG088 
!ECRANCPXG088.PNG!',null,null,1,null,4,null,6,0,'2012-09-24 12:33:20','2012-09-24 12:33:20','2012-09-24',0,null,null,710,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (711,2,2,'Civilité dans les destinataires pour les assureurs','h2. Demande

Rajouter automatiquement le terme "messieurs" dans les convocations pour un destinataire de type "assureurs".

h2. A faire

Sur choix d''un assureur, au moment ou l''on copie l''information des assureurs (CPXASP) vers le destinataire (CPXDES), il faut compléter la civilité dans l''enregistrement CPXDES avec la valeur correspondant à "." soit DESINTIT = "MMD"

h2. Test

Test ok si on voit apparaitre "messieurs" dans le contenu de la convocation.',null,5,5,5,4,59,4,21,'2012-09-25 15:40:11','2012-12-16 08:36:46',null,90,4,null,711,1,2,0,'2012-11-23 10:26:49');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (712,1,2,'OM Gsicass : anomalie création mission sur police existante toulon 0830/311991','Suite OM Gsicass création d''une nouvelle police de type DO n°374076 et mission --> le dossier s''est enregistré sur un G existant n°7696.
Voir mes copies d''écran.
1ère copie  écran classeur dossier manque Cie d''assurance- N° de police, n° G et nom de l''opération
!ANOMALIE311911.PNG!
2ème copie écran CPXG008 Enrichissement police : police n°330764 de type RCD  
!anomalie2311991.PNG!
3ème copie écran CPXG009 Enrichissement opération : pas de n°G
!ANOMALIE3311911.PNG!
Je fais quoi sur la base de TOULON ??? ',null,4,4,5,6,null,6,7,'2012-09-26 09:27:48','2013-05-16 11:01:11',null,0,null,null,712,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (713,2,5,'Etudier VDOC','-Voir ce que propose le produit vdoc (fonctionnalité, limites, licence ...)
-Tester le produit (API)
-Voir si il peut répondre à nos attentes : envisager de déployer une preuve de concept : attendre pour cela les retour du projet NOE et se coordonner avec Guillaume',null,null,5,18,4,null,10,1,'2012-09-26 10:08:46','2013-04-17 14:48:36','2012-09-26',0,null,705,705,2,3,0,'2013-04-17 14:48:36');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (714,2,11,'Flydoc','Connecter Noe et Flydoc ensemble',null,null,1,null,4,null,4,0,'2012-09-26 15:07:46','2012-09-26 15:07:46',null,0,null,null,714,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (715,1,2,'manque libellé de la NH voir copie d''écran','Il manque le libellé et la date de la NH dans l''envoi NH à Gsicass :
attachment:"Copie de 0940_MDO09_2012060612290000176_9400000074745.txt"',null,27,5,5,6,73,6,9,'2012-09-26 16:02:42','2012-10-26 10:36:23',null,100,null,null,715,1,2,0,'2012-10-19 11:22:11');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (716,1,2,'Dossier 0830/311991 enregistré suite OM Gsicass - anomalie Ecran des envois','Anomalie sur l''écran CPXL010B - Envoi de documents dans la liste des documents internes s''affiche la pièce externe 1.01 quand je clique pour consultation --> la page XML ne peut être affichée. A rapprocher de l''anomalie 461?? mais dans celle-ci la pièce s''affichait également dans les Documents internes, ce qui n''est pas le cas aujourd''hui 
Même anomalie dans dossier 0570/233857 (mais en consultation s''affichagent les codifications de l''OM)
voir copies d''écran 
!ECRANCPXL010B.PNG!
!ANOMALIEECRANCPXL010B.PNG!
Karine consulte tous les dossier pour faire le point sur le pb des J60 et je lui ai demandé de vérifier également cette anomalie ',null,null,1,null,6,null,6,1,'2012-09-27 10:03:40','2013-05-16 11:01:11','2012-09-27',0,null,null,716,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (717,2,5,'Processus de gestion des ordre de mission','Décrire le processus de gestion des ordre de mission',null,null,5,4,4,null,10,1,'2012-09-27 13:52:42','2013-04-17 14:48:20','2012-09-27',0,null,391,391,12,13,0,'2013-04-17 14:48:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (718,1,2,'Correction déclencheur EDI','Corrigé la génération des déclencheur pour l''EDI ainsi que revoir le moment où ce fichier est généré pour ne pas être consommé par l''EDI avant que la ou les PJs ne soient créées.','2012-09-27',27,5,5,4,71,5,3,'2012-09-27 15:28:57','2012-09-27 15:33:43','2012-09-27',100,null,null,718,1,2,0,'2012-09-27 15:28:57');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (719,1,6,'GSICASS : Pour GSICASS Envoyer rapport si une PJ est absente le script ne se termine pas!','Le script devrait se terminer si une pj est absente sur erreur et ne pas consommer les pjs non traitées ',null,40,1,null,4,122,11,3,'2012-09-27 16:55:40','2013-04-02 16:35:32','2012-09-27',0,null,null,719,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (720,2,5,'MapBundle','Créer un Bundle de cartographie',null,null,5,10,4,null,10,3,'2012-09-27 17:23:33','2013-04-17 14:47:51','2012-09-27',100,50,null,720,1,2,0,'2013-04-17 14:47:51');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (721,2,6,'NOE initialisation du referentiel','creer un script pour initialiser le referentiel NOE avec les fichiers fournis par Ardi',null,59,5,17,4,93,17,5,'2012-09-28 10:40:34','2013-04-02 16:18:07','2012-09-26',100,null,null,721,1,2,0,'2013-04-02 16:18:07');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (722,1,6,'Script CP2000AlerteMailDocumentExpert mise a jour referentiel','Le referentiel DataExchanger cpagenceref doit être mis à jour pour prendre en compte toute les agences',null,31,5,17,4,85,17,4,'2012-09-28 15:04:49','2013-02-13 14:22:20','2012-09-27',100,null,null,722,1,2,0,'2012-11-07 14:30:32');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (723,2,6,'NOE Initialisation du referentiel des utilisateurs','faire un script qui prend en compte le csv généré par Ardi pour envoyer la liste des utilisateurs dans le référentiel NOE',null,59,5,17,4,93,17,4,'2012-09-28 15:09:01','2013-02-13 12:04:41','2012-09-28',100,null,null,723,1,2,0,'2012-11-30 16:09:34');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (724,1,6,'GSICASS: Envoyer Rapport Prb sur "Convoque" et "Concerne"','Prb sur les valeurs dev mapping "Convoque" et "Concerne"',null,40,5,11,4,90,11,2,'2012-09-28 15:32:16','2013-02-13 11:13:13','2012-09-28',100,null,null,724,1,2,0,'2012-11-14 17:07:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (725,3,2,'Mise à jour du site formation','Site de formation à mettre à jour selon procédure : [[Mise à jour de la base de donnée formation]]

note : Attention, je suis pas sur que l''étape sur CPXASP soit à faire... vu qu''on a tout changé dernièrement (cf etape 3 et 4)','2012-10-01',null,5,5,6,null,4,1,'2012-09-28 16:17:14','2012-10-02 09:23:24','2012-10-01',100,4,null,725,1,2,0,'2012-10-02 09:23:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (726,1,6,'GSICASS: Prb de calcul sur opération NH','Prb de calcul sur opération NH - Gestion des Double en VBScript',null,40,5,11,4,90,11,2,'2012-10-01 10:00:35','2013-02-13 11:14:05','2012-10-01',0,null,null,726,1,2,0,'2012-10-01 10:00:35');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (727,2,6,'GSICASS: Le mail de routage OM doit inclure les PJ','Le mail de routage OM doit inclure les PJ',null,40,5,null,4,90,11,2,'2012-10-01 10:03:32','2013-02-13 12:05:03','2012-10-01',0,null,null,727,1,2,0,'2012-11-30 16:03:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (728,2,5,'Enrichissement Javascript','',null,null,6,8,4,null,10,2,'2012-10-01 14:42:39','2013-04-17 14:46:32','2012-10-01',100,null,null,728,1,4,0,'2013-04-17 14:46:32');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (729,2,5,'Intégrer le DatePicker de Jquery','Intégrer le Date Picker dans le User_Role_Agence',null,null,5,8,4,null,10,2,'2012-10-01 14:43:23','2013-04-17 14:44:08','2012-10-01',100,null,728,728,2,3,0,'2013-04-17 14:44:08');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (730,2,2,'SMART - paramétrage : ajouter un PDF','Au lieu de proposer d''éditer un document dans CKeditor, il faut proposer l''indexation d''une pièce jointe PDF.
Cela implique :
* suppression de la fonction de création
* la lecture ouvre le PDF (au lieu du html généré par CKeditor)
* la modification d''une fiche engendre une réindexation d''un PDF
 ',null,49,5,4,6,59,4,6,'2012-10-03 09:49:36','2012-12-16 08:36:43',null,90,16,251,251,2,3,0,'2012-12-11 17:27:00');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (731,2,2,'SMART - paramétrage : diffusion des documents en agence','Sur le même principe que les documents types, prévoir le processus de diffusion sur les serveurs agences des PDF concernant le sujet SMART.

Attention transfert données en base + documents PDF liés',null,49,1,null,4,null,4,0,'2012-10-03 09:52:39','2012-10-03 09:52:39',null,0,24,251,251,4,5,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (732,2,2,'SMART - accès agence','h2. Création mission

Le bouton Aide Mémoire présente la fiche PC de ce client et devra également se trouver dans l’écran d’ouverture de mission (CPXG001) une fois que la compagnie, le type de police et la mission ont été saisis.

h2. enrichissement des dossiers

Sur l’écran CPXG021 du dossier CP2000, on dispose d’une série de boutons qui sont paramétrés pour présenter les Fiches au format PDF.

Ces boutons ne sont actifs qu’une fois que la mission a été enregistrée dans CP2000 (numéro de dossier généré) à partir de l’écran CPXG021 (Classeur Dossier).ils sont tous visibles à partir de tous les onglets 

Les séries de boutons GM, SM, PC, TR sont de couleurs différentes.

!exemple_bouton_couleur.png!
Le fond d’écran affiche le N° des Doc du Référentiel SM, GM, PC, ou TR qui s’appliquent au dossier en cours, par exemple pour la GM052 et la SM058 :

Si pour un dossier, il n’y a pas de Fiche associée à un bouton, ce bouton est absent ou grisé. Ceci permettra de faire arriver les documents progressivement.

h2. Onglet document

Ce bouton n’apparait que sur l’onglet "Documents"

Il est paramétré pour présenter la Fiche du cas de Gestion sélectionné dans le cadre de la SM dès qu’elle a été sélectionnée.

!casdegestion.png!

Les N° de cas de Gestion devront être visibles (à définir).

h2. Onglet NH

Ces boutons n’apparaissent que sur l’onglet des NH :
 Le N° de la TR applicable s’affiche sur le fond.

!TR.png!

h2. Règle à éclaircir

liste des souhaits demandant explications :
* Les boutons SM n’apparaissent que lorsque la gestion déléguée a été sélectionnée.
* Les Fiches PC "Procédure Client" sont actuellement consignées dans le doc DT391.
',null,49,1,null,4,null,4,1,'2012-10-03 10:09:58','2012-10-03 10:10:15',null,0,80,251,251,6,7,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (734,1,2,'Gsicass- Envoi rapport  - document pdf externe manquant','Lors de la selection des PJ, il manque la copie des fichiers pdf.

exemple :
<pre>\\\\10.67.1.1\\agce\\Gsicass\\6700000189667.PDF;Lettre d''accompagnement du Rapport d''Expertise;03/10/2012
\\\\10.67.1.1\\agce\\Gsicass\\6700000189665.PDF;Rapport d''expertise;03/10/2012
\\\\10.67.1.1\\agce\\Gsicass\\6700000189789.PDF;Pièces Jointes au Rapport;03/10/2012
\\\\10.67.1.1\\agce\\Gsicass\\6700000189779.PDF;Lettre de transmission du dossier commun d''instruction;03/10/2012
</pre>

Ici, le fichier 6700000189789.PDF n''est pas présent pour DEX dans le dossier Gsicass.
',null,27,5,5,6,null,4,1,'2012-10-03 11:15:11','2012-10-19 12:09:52',null,0,null,null,734,1,2,0,'2012-10-19 12:09:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (735,2,2,'Allianz - Gestion des intermédiaires','A l''image des gestionnaires, il faut gerer les intermédiaires d''Allianz en vu de le permettre un accès au dossier Hermès

# gestion des intermédiaires au siège
# transfert du référentiel en agence
# dans le dossier, sélection de l''intermédiaire (facultatif) pour un dossier Allianz
# export Hermès à faire évoluer en conséquence.
','2013-01-31',null,9,5,4,null,4,29,'2012-10-03 11:34:51','2013-07-04 22:27:48','2012-12-12',73,120,null,735,1,8,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (736,1,6,'GSICASS : GsicassEnvoiRapport erreur pour trouver les PJ','GsicassEnvoiRapport erreur pour trouver les PJ',null,40,5,11,4,90,11,3,'2012-10-03 15:01:02','2013-02-13 11:15:49','2012-10-03',100,null,null,736,1,2,0,'2012-11-07 14:37:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (737,2,5,'ImageBundle','Créer un module permettant la gestion des images de l''application',null,null,5,8,4,null,10,9,'2012-10-05 14:36:17','2013-04-17 14:47:05','2012-10-05',90,null,null,737,1,2,0,'2013-04-17 14:47:05');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (738,2,5,'Document Bundle','Déployer un Bundle de gestion des modèles de documents',null,null,5,18,4,null,10,8,'2012-10-05 14:37:05','2013-04-17 14:46:49','2012-10-05',100,null,null,738,1,8,0,'2013-04-17 14:46:49');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (739,2,5,'Etudier et adapter le code de Sensio à notre application','',null,null,5,18,4,null,10,2,'2012-10-05 14:37:39','2013-04-17 14:46:12','2012-10-05',100,null,738,738,2,3,0,'2013-04-17 14:46:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (740,2,5,'Proposer une maquette de gestion des templates','',null,null,5,18,4,null,10,4,'2012-10-05 14:38:10','2013-04-17 14:43:35','2012-10-05',100,null,738,738,4,5,0,'2013-04-17 14:43:35');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (741,3,6,'GSICASS: Réunion Apria','Réunion Apria',null,40,5,11,4,90,11,2,'2012-10-08 10:55:50','2013-02-13 14:23:05','2012-10-08',100,null,null,741,1,2,0,'2012-11-07 14:36:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (742,2,8,'Prise en compte des documents sans fichiers','Amélioration de la gestion des erreurs avec la prise en compte des documents indexés au dossier sans ajouter le fichier.
Document de type ''X''

','2012-10-09',null,5,7,4,null,4,2,'2012-10-09 09:34:01','2012-10-09 10:24:47',null,100,null,null,742,1,2,0,'2012-10-09 09:43:44');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (743,2,2,'Améliorer le contrôle des envois de convocations','h2. problème 

Les convocations envoyés par mail ne sont pas repérable car le lien entre le détail d''un envoi et les documents/destinataires n''est pas effectué

h2. A faire

* Identifier comment corriger en enrichissant le dossier (table CPXCHRONODOC à renseigner)
* Faire la correction dans CP2000 au moment de l''envoi

h2. Exemple 

> -----Message d''origine-----
> De : support@groupeprunay-si.fr [mailto:support@groupeprunay-si.fr]
> Envoyé : mardi 9 octobre 2012 09:30
> À : Eurisk Quercy
> Objet : 0190 - Convocations validees non envoyees
> 
> Liste des convocations qui n''ont pas été envoyées
>
> =================================================
> 0190/496 - 2012-07-10 - PONCHARAL CLAUDE
> 0190/535 - 2012-10-01 - SECB
> 0190/535 - 2012-10-01 - WANNITUBE
> 0190/535 - 2012-10-01 - SACDROP ASSURANCES
> 0190/535 - 2012-10-01 - CH ESQUIROL 
',null,27,2,4,4,null,4,1,'2012-10-09 11:37:27','2012-10-09 11:40:56',null,0,32,null,743,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (744,1,2,'Date du dossier à mettre à jour suite à modification document externe','h2. Problème

Certains documents ne sont pas pris en compte lors de la remontée Hermès.
Ce sont des documents externes, PJ indexées au départ "A classer" par la macro d''archivage.
La secrétaire modifie le classement a posteriori (quelques jours plus tard). Le nouveau classement indique qu''il doit désormais remonter sur Hermès
Et le document reste manquant sur Hermès.

On pense que la date de mise à jour (DOSDTMAJ) du dossier n''est pas réactualisée si on modifie le document.
',null,5,5,5,4,73,4,7,'2012-10-09 14:21:51','2012-10-26 10:37:12','2012-10-09',100,4,null,744,1,2,0,'2012-10-11 12:08:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (745,2,6,'NOE: Script NOEDossierPrisEnCharge','Gestion du message acquittement dossier sur tablette',null,59,5,11,4,93,11,3,'2012-10-09 17:18:16','2013-04-02 16:18:26','2012-10-09',100,null,null,745,1,2,0,'2013-04-02 16:18:26');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (746,2,17,'Mise en place de PIWIK','http://devmonitor.groupe-prunay.fr/piwik',null,null,1,10,4,null,10,0,'2012-10-10 12:10:34','2012-10-10 12:10:34','2012-10-10',0,null,null,746,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (747,2,6,'NOE: Script NOEDossierNonPrisEnCharge','Script pour la gestion du message getDossierNonPrisEnCharge',null,59,5,11,4,93,11,4,'2012-10-10 15:16:21','2013-04-02 16:18:40','2012-10-10',100,null,null,747,1,2,0,'2013-04-02 16:18:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (748,2,6,'NOE: NoeGestionMsgAcquittementSurImport','Script de gestion du message acquittement de ARDI sur import et acquittement des dossier via WS NOE',null,59,5,11,4,93,11,3,'2012-10-11 15:50:32','2013-04-02 16:18:56','2012-10-11',100,null,null,748,1,2,0,'2013-04-02 16:18:56');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (749,1,2,'OM - controle sur l''existence d''un dossier HS','Dans le cas ou nous avons plusieurs OM dans une liste :
!CPXG094-listeOM.png!

Le controle de la présence d''un dossier se fait sur le dernier OM de la liste
!CPXG094debug.png!

Corriger la gestion du pointeur.',null,4,5,5,4,59,4,4,'2012-10-11 16:09:31','2012-12-16 08:36:46','2012-10-11',90,null,null,749,1,2,0,'2012-12-12 17:15:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (750,3,4,'Changement GAN-EURO --> ALL-EURO','Problème dans le paramétrage des profils, des users GAN-EURO, des accès dossiers.

regarder tout ce qui "traine" en Gan-euro et remplacer par ALL-EURO','2012-10-12',2,5,4,4,null,4,2,'2012-10-12 10:25:07','2012-10-23 10:17:34','2012-10-12',0,4,null,750,1,2,0,'2012-10-12 10:55:49');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (751,1,4,'Problème sur la création d''un user','Problème sur la création d''un user, le nouvel utilisateur n''apparait pas dans la liste !
Ici c''est le row_id = 639
<pre>
+--------+---------------+---------------------+------------+---------------------+------------+-------------+--------------+----------+----------------------+--------------+--------------+--------------+----------------------+----------------------+-------------+-------------+-------------+--------------+---------------+---------------+--------------+-----------------+--------------+--------------+--------------+--------------+-------------+----------+-----------+-------------+----------+--------------------+-----------------------+-----------------------+-----------+-------------+------------+---------+---------+-------------+--------------+------------------------+---------------------+---------+
| row_id | row_module_id | created_dt          | created_by | updated_dt          | updated_by | usr_login   | usr_password | usr_lang | usr_email            | usr_work_num | usr_home_num | usr_cell_num | usr_last_name        | usr_first_name       | usr_home_id | usr_minrows | usr_maxrows | usr_cli_code | niveau_profil | usr_gest_code | usr_exp_code | usr_climin_code | usr_timezone | usr_image_id | usr_address1 | usr_address2 | usr_zipcode | usr_city | usr_state | usr_country | usr_menu | usr_menu_defhidden | usr_menu_defcollapsed | usr_menu_defdomain_id | usr_extra | usr_subhead | usr_active | usr_civ | usr_fax | usr_service | usr_fonction | usr_profil             | usr_date_creat      | usr_pwd |
+--------+---------------+---------------------+------------+---------------------+------------+-------------+--------------+----------+----------------------+--------------+--------------+--------------+----------------------+----------------------+-------------+-------------+-------------+--------------+---------------+---------------+--------------+-----------------+--------------+--------------+--------------+--------------+-------------+----------+-----------+-------------+----------+--------------------+-----------------------+-----------------------+-----------+-------------+------------+---------+---------+-------------+--------------+------------------------+---------------------+---------+
|    188 |            16 | 2006-12-12 18:03:32 | eurisk     | 2012-10-12 09:20:38 | eurisk     | GanEuro     | 6js1w4       | FRA      | hermes@eurisk.fr     | NULL         | NULL         | NULL         | GANEURO              | NULL                 |           2 |          50 |         100 | ALL-EURO     | 40            | NULL          |         NULL | NULL            | NULL         |         NULL | NULL         | NULL         | NULL        | NULL     | NULL      | NULL        | 1        | 0                  | 0                     |                  NULL | 1         | 1           | 1          | NULL    | NULL    | NULL        | NULL         | Administrateurs Client | 2006-12-12 18:03:32 | 6js1w4  |
|    355 |            16 | 2009-05-01 03:55:12 | Eurisk     | 2011-07-04 14:16:01 | eurisk     | MailAgf     | auetng       | FRA      | ctrlhermes@eurisk.fr | NULL         | NULL         | NULL         | Mail automatique AGF | Mail automatique AGF |           2 |         100 |         200 | ALLIANZ      | 40            | NULL          |         NULL | NULL            | NULL         |         NULL | NULL         | NULL         | NULL        | NULL     | NULL      | NULL        | 1        | 0                  | 0                     |                  NULL | 1         | 1           | 1          | NULL    | NULL    | NULL        | NULL         | Administrateurs Client | 2009-05-01 00:00:00 | auetng  |
|    639 |            16 | 2012-10-12 09:38:29 | eurisk     | 2012-10-12 09:38:29 | eurisk     | MailAllEuro | 3nz7a6       | FRA      | NULL                 | NULL         | NULL         | NULL         | ALLIANZ EUROCOURTAGE | Mail Automatique     |           2 |          10 |          25 | ALL-EURO     | NULL          | NULL          |         NULL | NULL            | NULL         |         NULL | NULL         | NULL         | NULL        | NULL     | NULL      | NULL        | 1        | 0                  | 0                     |                  NULL | 1         | 1           | 0          | NULL    | NULL    | NULL        | NULL         | NULL                   | NULL                | 3nz7a6  |
+--------+---------------+---------------------+------------+---------------------+------------+-------------+--------------+----------+----------------------+--------------+--------------+--------------+----------------------+----------------------+-------------+-------------+-------------+--------------+---------------+---------------+--------------+-----------------+--------------+--------------+--------------+--------------+-------------+----------+-----------+-------------+----------+--------------------+-----------------------+-----------------------+-----------+-------------+------------+---------+---------+-------------+--------------+------------------------+---------------------+---------+
</pre>

Modif SQL :
<pre><code class="SQL">
update m_user set niveau_profil=''40'', usr_active=''1'', usr_profil=''Administrateurs Client'' where row_id=639;
</code></pre>
<pre>
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0
</pre>

',null,null,1,null,4,null,4,0,'2012-10-12 10:52:44','2012-10-12 10:52:44','2012-10-12',0,null,null,751,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (752,1,6,'NOE: Phase 1 - recette Anomalies Script','* Dans les scipts NOE mettre la gestion des erreurs dans Exec net use...
* Dans le script sendReferential compléter les logs
* Dans le script export supprimer le package après copie
* Dans le script export l''emission des PJ n''est pas bloquante sur erreur (ajouter NOEND sur erreur WS sendPJ)
',null,59,5,17,4,93,11,4,'2012-10-12 16:11:15','2013-02-13 11:16:06','2012-10-12',100,null,null,752,1,2,0,'2012-11-30 16:08:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (753,1,6,'NOE: Sur impor ARDI Erreur parsing','C:\\>java -jar c:\\noe\\noe.jar -IMP
IMPORT
DÚbut du traitement : IMPORT
Recherche des messages d''import ...
   Analyse du message : msg_import_20121012153554_135004895633585000AC9060F12101
215355635B.xml
      - Contr¶le de la validitÚ du message ...
      - Analyse du fichier ZIP ...
         - Traitement du fichier : meta_143303_exo.xml
         - Extraction XML ''meta_143303_exo.xml'' vers : d:/ftp/partage/noe/workin
g/in/meta_143303_exo.xml
            - Parseur -> Mission
            - Parseur -> Sinistre
            - Parseur -> AssurÚ
            - Parseur -> Tiers
Exception : Nombre de Tiers manquant dans le fichier ''meta_143303_exo.xml''
null
            - Parseur -> Historiques
Erreur Import XML : Parseur : d:\\ftp\\partage\\noe\\msg\\in\\msg_import_2012101215355
4_135004895633585000AC9060F12101215355635B.xml
null
java.lang.NullPointerException
        at noe.fonctions.NoeImport.ImportDoss(NoeImport.java:318)
        at noe.Noe.main(Noe.java:92)
',null,59,5,17,4,93,11,3,'2012-10-12 16:30:11','2013-02-13 10:43:54','2012-10-12',100,null,null,753,1,2,0,'2013-02-13 10:43:54');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (754,2,6,'Améliorer Liste Alerte Mail CP2000 Récapitulatif des documents ajoutés','Remontées suite formation Experts et Assistantes : mettre la Réf complète du dossier avec type de la mission (ex : 0830/333333/DOA) et à la place du n°G mettre le libellé de l''affaire',null,28,5,17,4,85,6,5,'2012-10-15 09:23:43','2013-04-02 15:42:57','2012-10-15',100,null,null,754,1,2,0,'2013-04-02 15:42:57');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (755,1,6,'GSICASS: Calcul erroné dans GsicassNoteHonoraire','Calcul erroné dans GsicassNoteHonoraire, il faut intégrer la TVA dans les différents montants pour avoir la somme en TTC  ',null,40,5,11,4,90,11,3,'2012-10-15 16:53:21','2013-02-13 11:17:07','2012-10-15',100,null,null,755,1,2,0,'2012-11-07 14:38:36');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (756,2,6,'CPCOM: Compactage de UNIQUE ID','La génération de l''id unique dans cpcom.vsl doit donner un id plus court',null,31,5,11,4,98,11,4,'2012-10-17 09:37:03','2013-02-13 12:05:36','2012-10-17',100,null,null,756,1,2,0,'2012-11-30 15:57:11');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (757,2,6,'GSICASS: Dans Mail de blocage plateforme script GSICASSTraitementMission donner la raison si les pj ne sont pas envoyés','Dans Mail de blocage plateforme script GSICASSTraitementMission donner la raison si les pj ne sont pas envoyées:
PJ trop volumineuse avec nom des PJ ou Pas de PJ',null,40,5,null,4,90,11,2,'2012-10-17 10:51:07','2013-02-13 12:07:20','2012-10-17',100,null,null,757,1,2,0,'2012-12-10 11:24:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (758,2,2,'Intermédiaire Allianz - paramétrage siège','La gestion des intermédiaires (ajout/modification/suppression) doit avoir un fonctionnement similaire à la gestion des gestionnaires.
L''accès est disponible dans le menu client

!menu_client_intermediaire.png!

Créer une nouvelle table SQL pour enregistrer les informations de cet écran 

!GestionIntermédiaire.png!


Le code intermédiaire sera la clé de l''enregistrement.
Le bouton "R" permet de rechercher un enregistrement existant pour modification
Le bouton "+" permet de créer un nouvel identifiant automatiquement (Prendre exemple sur CPXG104)
Le bouton "R" sur le client majeur permet de choisir le client majeur concerné par l''ajout de l''intermédiaire.
Les champs en beige sont obligatoires.

','2012-12-14',69,5,5,4,72,4,20,'2012-10-17 15:25:22','2013-02-10 09:34:08','2012-12-12',90,16,735,735,2,3,0,'2013-01-23 15:25:35');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (759,2,2,'Intermédiaire Allianz - Transfert CP2000','La table contenant les informations sur les intermédiaires doit être ajoutée au transfert siège > agence
* Ajouter le paramétrage
* Enrichir le processus de transfert CP2000 

(cf document#6)
','2012-12-19',69,5,5,4,72,4,8,'2012-10-17 15:31:36','2013-02-10 09:34:08','2012-12-17',90,24,735,735,4,5,0,'2013-01-23 14:50:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (760,2,2,'Intermédiaire Allianz - Affectation en agence','Les intermédiaires sont liés au client qui missionne ( à confirmer... sinon il faut doubler le système pour le client qui paye)
La sélection d''un intermédiaire est facultatif. On ne peut choisir qu''un intermédiaire maximum.

Modifier la table CPXDOS pour ajouter le code intermédiaire

Le bouton "Intermédiaire" est gris quand aucun intermédiaire n''est sélectionné.
Le bouton "Intermédiaire" est vert quand un intermédiaire est sélectionné

!boutonintermediaire.png!

L''utilisateur choisi son enregistrement dans la liste des intermédiaires disponibles et clique sur "enregistrer et quitter" pour valider son choix.

!ListeIntermédaire.png!','2013-01-31',69,4,5,4,null,4,33,'2012-10-17 15:37:00','2013-01-29 14:24:48','2012-12-20',60,80,735,735,6,7,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (761,2,8,'Intermédiaire Allianz - Transfert Hermès','Ajouter l''export xml des intermédiaires dans la procédure Hermès existante 

!hermesprocsiege.png!


* Ajouter la référence du document type dans les attributs du document (CPXDOC.DTYREF)
* Ajouter la table des intermédiaires (se calquer sur les gestionnaires TT_hermes_gest.xls, au siège)','2013-02-07',null,2,5,4,null,4,16,'2012-10-17 15:41:08','2013-02-07 22:24:24','2013-02-01',0,32,null,761,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (762,2,6,'Script de suppression d OM Darva dans CP2000','Supprimer les informations relatives à l''OM donnée en paramètre dans la base CP2000',null,28,5,17,4,87,17,5,'2012-10-18 09:52:05','2013-04-02 16:14:27','2012-10-18',90,null,null,762,1,2,0,'2013-04-02 16:14:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (763,1,15,'Formulaires','Vérifier que le getDefaultOptions est présent dans l''ensemble des formulaire',null,null,1,18,4,null,10,0,'2012-10-18 10:02:09','2012-10-18 10:02:09','2012-10-18',0,null,null,763,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (764,1,2,'Envoi de rapport avec PJ vers GSICASS','Il s''avère que si on envoie une pièce jointe qui est déjà de format PDF, elle aura au finale le même nom en fin de traitement que le document d''origine et donc est supprimée lors de la purge dans le dossier GSICASS des fichiers de travail.

Une clause a été rajoutée dans le code pour ne pas PDFiser et supprimer une PJ qui serait déjà en PDF afin d''éviter la suppression de la PJ qui est censée partir.',null,27,5,5,4,73,5,4,'2012-10-18 14:32:33','2012-10-26 10:37:39','2012-10-18',100,null,null,764,1,2,0,'2012-10-26 10:37:39');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (765,2,5,'S''assurer de la conformité checkstyle','','2013-12-31',null,2,10,5,null,10,2,'2012-10-19 15:12:30','2013-04-17 14:43:19','2012-10-19',90,100,368,368,6,7,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (766,1,6,'GSICASS: GSICASSEnvoyerNH Erreur sur copie PJ','GSICASSEnvoyerNH Erreur sur copie PJ',null,40,5,11,4,90,11,4,'2012-10-19 15:35:02','2013-02-13 11:17:34','2012-10-19',0,null,null,766,1,2,0,'2013-01-17 11:59:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (767,2,6,'NOE: Acquittement sur import dossier','Script gérant les messages 21,22,23 du diagrame de séquence dans le NOE STI',null,59,5,11,4,93,11,2,'2012-10-22 14:52:38','2013-04-02 16:19:33','2012-10-22',100,null,null,767,1,2,0,'2013-04-02 16:19:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (768,2,6,'Gsicass - Bloquer les OM qui n''ont pas de pièces jointes','Sur réception d''un OM Gsicass, dans le cas ou je n''ai pas de pièces jointes, il faut refuser l''OM.

h2. A faire

Mail prévenant le gestionnaire de l''assureur que l''OM ne sera pas pris en charge. Inviter à retransmettre un OM avec une pièce jointe.
Mettre en copie la plateforme pour suivi.
Ne pas insérer l''OM en agence.

h2. Question 

* Qui en expéditeur de du mail ?

','2012-10-30',40,5,11,6,90,4,11,'2012-10-22 14:59:05','2013-02-13 12:07:49','2012-10-29',100,16,null,768,1,2,0,'2012-11-30 16:00:58');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (769,2,2,'Darva','','2012-11-30',33,5,5,4,59,4,34,'2012-10-22 18:06:27','2012-12-16 08:36:48','2012-11-06',100,264,null,769,1,18,0,'2012-12-14 16:21:55');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (770,2,6,'Certification Darva','Obtenir la Certification Darva','2012-12-07',32,5,17,4,87,4,8,'2012-10-22 18:33:17','2013-04-02 16:14:21','2012-11-05',90,40,501,501,2,3,0,'2013-04-02 16:14:21');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (771,2,1,'Présentation / Page d''accueil','Une nouvelle page d''accueil est en cours de développement par ICUS.
','2012-12-03',null,1,23,4,null,4,0,'2012-10-23 10:12:08','2012-10-23 10:12:08',null,0,null,77,77,10,11,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (772,2,4,'Allianz - Accès intermédiaire','Ajouter un champs sur les documents : doh_dtyref

Créer le profil spécial pour l''intermédaire
Il doit avoir le droit de voir les dossiers le concernant.
Il ne peut voir que les documents dont le dtyref est présent parmi une liste de référence (qui reste à définir)

Ticket CitéSI 270','2013-05-24',67,9,25,4,62,4,12,'2012-10-23 10:31:01','2013-07-04 22:27:49','2013-05-23',60,64,null,772,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (773,2,6,'DARVA:  Les flux XML provenant de Darva pour CP2000 doivent être formatés pour éliminer les attributs xml','Les flux XML provenant de Darva pour CP2000 doivent être formatés pour éliminer les attributs xml
ex:

<xxxx attrib="a"/>

doit devenir

<xxxx>a</xxxx>
',null,32,5,17,6,87,11,4,'2012-10-24 16:31:32','2013-02-12 16:29:56','2012-10-24',100,null,null,773,1,2,0,'2013-02-12 16:29:56');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (774,1,6,'Script Liste Alerte Mail CP2000 Récapitulatif - controle des dates du document','contrôler les dates du documents qui peuvent etre incohérentes ou mal formattés pour eviter que le script parte en erreur',null,31,5,17,4,85,17,3,'2012-10-25 11:34:11','2013-02-13 11:19:14','2012-10-24',100,null,null,774,1,2,0,'2012-11-07 14:29:49');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (775,1,2,'Mauvais nombre de paramètres à l''appel de CPXG003 dans CPXG065','Origine : Sacha Chopard - 25/10/2012

Dans la local proc LP_SELECTPARAMCREREP, CPXG003 est appelé avec le mauvais nombre de paramètres.
Mettre à jour l''appel avec les bons paramètres.
',null,16,5,5,6,73,5,2,'2012-10-25 15:05:41','2012-10-26 10:39:05','2012-10-25',100,null,null,775,1,2,0,'2012-10-26 10:39:05');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (776,2,17,'Mise en place de Jenkins','',null,null,1,10,4,null,10,0,'2012-10-26 14:26:11','2012-10-26 14:26:11','2012-10-26',0,null,null,776,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (777,2,6,'Formation: Transfert de compétence pour le support ','Transfert de compétence pour le support ','2013-06-03',70,2,null,4,140,11,7,'2012-10-26 16:30:10','2013-05-13 11:40:30','2012-10-26',50,null,null,777,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (778,1,15,'Probleme du chargement de la carte sous IE','',null,null,5,10,4,null,10,2,'2012-10-29 15:25:48','2012-11-05 10:15:08','2012-10-29',0,null,null,778,1,2,0,'2012-11-05 10:15:08');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (779,1,15,'Page d''accueil Filtre des USER','Effectuer la requete des utilisateir par rapport à leur date de présence',null,null,5,10,4,null,10,2,'2012-10-29 15:26:45','2013-01-07 11:45:57','2012-10-29',0,null,null,779,1,2,0,'2013-01-07 11:45:57');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (780,1,15,'Erreur de suppression','Lorsque l''on tente de supprimer un dépatement ou une résion qui est déja liée, il y a un plantage au lieu d''une levée d''erreyr',null,null,5,18,4,null,10,2,'2012-10-29 15:27:39','2013-02-14 16:42:30','2012-10-29',100,null,null,780,1,2,0,'2013-02-14 16:42:30');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (781,1,15,'Droits sur les fromulaire de localisation','Attention a la gestion des droits dans les formulaires.
Il y a des erreurs dans le controller lorsque l''on a pas tous les droits sur la localisation.
Faire un jeu de test pour valiser que tout est OK',null,null,9,18,4,null,10,1,'2012-10-29 15:28:49','2012-11-05 10:24:26','2012-10-29',100,null,null,781,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (782,1,6,'NOE: Script NOEExport et NOEImport Pour le WS Les balises creationdate et modificationdate doivent être modifié en datedoc et datemaj','Les balises creationdate et modificationdate doivent être modifié en datedoc et datemaj',null,59,5,17,4,93,11,3,'2012-10-30 10:00:52','2013-04-02 16:16:19','2012-10-30',100,null,null,782,1,2,0,'2013-04-02 16:16:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (783,1,6,'NOE: Retester le script NOEAcquittementDossier Prb lors du test sur recheche VDOCID','Retester le script NOEAcquittementDossier Prb lors du test sur recheche VDOCID',null,59,5,17,4,93,11,3,'2012-10-30 10:03:39','2013-02-13 11:19:35','2012-10-30',100,null,null,783,1,2,0,'2012-11-07 16:19:56');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (784,1,6,'NOE enlever digicode et etage','dans l export et l import ne plus prendre en compte le digicode et l etage mais l adresse 3',null,59,5,17,4,93,17,3,'2012-10-30 10:26:45','2013-02-13 11:19:50','2012-10-30',100,null,null,784,1,2,0,'2012-11-30 16:07:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (785,1,6,'GSICASS: Anomalie sur update cpxcpt en erreur','Anomalie sur update cpxcpt en erreur',null,40,5,11,4,90,11,3,'2012-10-31 15:47:47','2013-02-13 11:20:19','2012-10-31',80,null,null,785,1,2,0,'2012-11-30 16:01:34');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (786,3,2,'Mise à jour du site formation pour le 6/11/2012','J''ai repris le texte de Vincent lors de la précédente mise à jour (Assistance 725)
Site de formation à mettre à jour selon procédure : Mise à jour de la base de donnée formation
note : Attention, je suis pas sur que l''étape sur CPXASP soit à faire... vu qu''on a tout changé dernièrement (cf etape 3 et 4)
',null,null,5,null,6,null,6,1,'2012-10-31 16:36:39','2012-11-13 18:32:17','2012-10-31',0,null,null,786,1,2,0,'2012-11-13 18:32:17');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (787,2,2,'Modification de l''arborescence DARVA/GSICASS pour Data Exchanger dans la gestion d''envoi de documents','Rajouter dans les dossiers déjà existants "Darva" et "Gsicass", les dossiers "NH" et "Rapport" afin de dissocier les 2 process et ainsi éviter les écrasements de PJ.',null,33,5,5,4,59,5,5,'2012-11-05 10:18:57','2012-12-16 08:36:49','2012-11-05',60,null,null,787,1,2,0,'2012-11-23 10:25:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (788,2,6,'NOE : Phase 2 Anomalie Test ','Suite aux tests effectués avec Xavier Quincieux les points suivants ont pu être relevés :

•	Problème concernant les champs « Etage » et « Digicode » (pour « assuré» et  «  tiers »). Actuellement, suite aux ajustements, ARDI et le Webservice NOE  gèrent trois lignes pour l’adresse. 
Les champs   « Etage » et « Digicode » ne sont pas présent sur Ardi et la 3ème  ligne du champ adresse n’est pas présente sur la tablette. 
Ceci pouvant être la source de divers problèmes à l’export et à l’import. 
La solution (vu avec Quentin) serait que DeX ne transmette que les champs adresses.  Les champs  « Etage » et « Digicode » seraient à la discrétion de NOE. Par contre, un troisième champ adresse serait nécessaire sur l’IHM de la tablette.
•	Ajout des champs dateDoc et dateMaj (cette mise à jour a été faite ce matin)
•	Problème sur historique référençant la même pièce jointe (correction réalisée ce matin)
•	Problème heure du rendez-vous : il a été remarqué que le rendez-vous sur la tablette avait un décalage d’une heure (peut être un problème passage à l’heure d’hiver) à tester
•	Problème lors de l’acquittement du dossier qui à pour origine DeX et Ardi (en cour de correction) et erreur sur le WS  getDossierAck (corrigé)
',null,59,5,null,4,93,11,3,'2012-11-05 10:40:03','2013-02-13 12:08:07','2012-11-05',100,null,null,788,1,2,0,'2012-11-07 14:43:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (789,1,6,'GSICASS: Script GSICASSTRAITEMENTMISSION Prb sur maj le CPXCPT','Prb de la mise à jour cpxcpt qui doit être effectué hors transaction',null,40,5,11,4,90,11,2,'2012-11-05 10:53:05','2013-02-13 11:20:36','2012-11-05',0,null,null,789,1,2,0,'2012-11-30 16:00:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (790,2,5,'Existant CP2000','Consigner l''historique fonctionnel et visuel de CP2000 afin de faire une proposition dans l''interface Kastor',null,null,1,5,4,null,10,3,'2012-11-05 16:25:15','2013-04-17 14:41:58','2012-11-05',100,null,null,790,1,4,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (791,2,5,'Convocation','Screen-shot de création d''une convocation à partir du dossier:
 - ajout de destinataire
 - recherche de destinataire de type autre
 - ajout clausier',null,null,5,5,4,null,10,1,'2012-11-05 16:34:17','2013-04-17 14:41:58','2012-11-05',0,null,790,790,2,3,0,'2013-04-17 14:41:58');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (792,2,2,'Darva - envoi SD commentaire','Préparer un écran, accessible depuis l''ecran de gestion des envois DARVA pour permettre à l''utilisateur d''envoyer un commentaire
(champs textarea, pas de limite).

Fichier déclencheur, voir avec David

Un SD 99 envoyé, doit être consultable dans l''onglet "Mail" en interne.
Destinataire / Expéditeur : le client à qui on l''envoi
Sujet : SD Commentaire
Date création : DOCDTCRE
Expéditeur : rien',null,33,5,5,4,59,4,35,'2012-11-06 10:50:19','2012-12-16 08:36:48','2012-11-06',90,16,769,769,12,13,0,'2012-11-23 10:43:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (793,2,6,'Darva faire script pour l''envoi de commentaire','Faire un script qui gère l''envoi de commentaire vers Darva, message SD99',null,32,5,17,4,87,17,6,'2012-11-06 11:58:38','2013-04-02 16:12:43','2012-11-06',100,null,null,793,1,2,0,'2013-04-02 16:12:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (794,2,2,'Contrôle d''infos obligatoires pour l envoi de rapport DARVA','Rajouter un contrôle supplémentaire avant tout envoi de rapport vers DARVA',null,33,5,5,4,59,5,10,'2012-11-06 14:30:18','2012-12-16 08:36:49','2012-11-06',60,null,null,794,1,2,0,'2012-12-12 17:14:54');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (795,1,2,'Bug dans la gestion des doublons à la création de mission à partir d''un OM DARVA','Élément à modifier : CPXT094.',null,33,5,5,4,59,5,4,'2012-11-06 15:28:53','2012-12-16 08:36:49','2012-11-06',60,null,null,795,1,2,0,'2012-11-23 10:17:58');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (796,2,6,'Gsicass - Changement redirection agence','Bonjour, 

Dans l''annuaire de Novembre 2012, les départements suivants ont changé d''Agence : 

* Le Département 05 traité par la base 0380
* Le Département 47 traité par la base 0470
* Le Département 87 traité par le 0190 
','2012-11-07',40,5,11,4,90,4,5,'2012-11-07 11:13:48','2013-02-13 12:08:30','2012-11-07',90,2,null,796,1,2,0,'2012-11-30 15:59:59');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (797,2,6,'Flydoc - CP2000 - intégration mensuel des couts d''un envoi en agence','h2. Objectif

Chaque mois, Eurisk reçoit un fichier récapitulatif des couts des envois Esker.
Ce fichier est reçu sur un FTP.
Il faut intégrer les données dans la bonne base CP2000 et prévenir le controle de gestion que le fichier est disponible.

h2. Accès

Source : FTP 
* FTP Server : ftp.groupe-prunay.Fr
* FTP Port : 21
* FTP User : esker
* FTP Password : eavTycs9
* FTP Folder : la racine 

Destination : Partage Eurisk Siege 
* \\\\W2003-781-07.groupe-prunay.fr\\partage\\8-PUBLIC\\Irina Peyrou\\ESKER

h2. A faire

# Pour chaque envoi réintégrer les couts dans la base bonne base CP2000, table CPXCHRONO. S''aider du code uniface joint pour établir le mapping.
# Archiver le fichier sur le serveur Dex
# Prévenir, par mail, le contrôle de gestion que le fichier est disponible sur le partage

h2. Périodicité

A chaque fois qu''un nouveau fichier est déposé sur le FTP.',null,28,1,null,4,52,4,0,'2012-11-07 12:25:34','2012-11-07 12:25:34','2012-11-15',0,40,null,797,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (798,2,5,'Reunion + Convocation','- Lors de la vue d un dossier, s''assurer de voir les reunion dans l''onglet date
- Lors ce que l''utilisateur a les droit de modification de dossier et de création de réunion (objet père), lui permettre de d''ajouter une reunion de type Organiser ou Participer',null,null,5,18,4,null,10,4,'2012-11-09 08:54:29','2013-04-17 14:41:37','2012-11-09',80,null,658,391,9,10,0,'2013-04-17 14:41:37');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (799,2,4,'Empecher le blocage IE sur double clic','Reproductible dans IE8, sur double clic "lent" pour l''ouverture d''un document.
OK dans IE9 et autre navigateur.

Le problème vient de l''ouverture du popup',null,39,5,null,4,null,4,1,'2012-11-09 13:46:05','2012-11-14 15:31:49','2012-11-09',0,null,null,799,1,2,0,'2012-11-14 15:31:49');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (800,3,19,'Autre - divers','- reunion
- temps divers passé sur Hermès dommage',null,null,4,null,3,null,4,1,'2012-11-09 16:24:33','2013-05-14 13:38:19',null,0,null,null,800,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (801,2,4,'Erreur internet explorer sur double clic','Sur l''ouverture d''un document, avec internet explorer 8, un avertissement apparait lorsque l''utilisateur effectue un double clic "lent".
OK dans IE9 et autre navigateur.
Le problème vient de l''ouverture du popup

Voir pour annuler ce problème : Est-il possible a la suite du 1er clic de ne pas prendre en compte le 2e clic ?

',null,null,1,null,4,74,4,5,'2012-11-12 09:59:50','2013-07-04 22:27:49',null,0,null,null,801,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (802,1,6,'CP2000: Le script CP2000AlerteMailDocumentExpert lance une erreur EC mais  met  la ligne en jaune (mode paresseux)','Le script CP2000AlerteMailDocumentExpert lance une erreur EC mais  met  la ligne en jaune (mode paresseux) 
Ex le 2012-11-09 17h01

Erreur:

2012-11-09 17:00:05 Message du LOG : Agence Code=9740
2012-11-09 17:00:05 ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2012-11-09 17:00:26 E9001: Erreur de thread dans ExecuteConnect
2012-11-09 17:00:26 In (S7/P36) -> (ACT1-3 - Recuperation Infos chez Agence/Connect : 1 Base Agence)
2012-11-09 17:00:26 [Solid][SOLID ODBC Driver][SOLID]SOLID Communication Error 21306: Server ''tcp 10.97.4.1 1317'' not found, connection failed
2012-11-09 17:00:26 ### ERREUR - EC
2012-11-09 17:00:26 ### CPCOM-ERREUR EC
2012-11-09 17:00:26 
2012-11-09 17:00:26 -----------------------------------------------------------------------------------------------------------
2012-11-09 17:00:26 Le 09-11-2012 à 17:00:26 CP2000AlerteMailDocumentExpert-OCEANINDIEN-PROD-CP2000AlerteMailDocumentExpert
2012-11-09 17:00:26 
2012-11-09 17:00:26 Erreur : EC - Erreur DB - Erreur lors de la connexion à la base de donnée de l''agence - IP Agence="10.97.4.1" -  ODBC="9740" ; User = "admincpx"
2012-11-09 17:00:26 Libellé... : Erreur DB - Erreur lors de la connexion à la base de donnée de l''agence - IP Agence="10.97.4.1"
2012-11-09 17:00:26 Statut.... : -1200
2012-11-09 17:00:26 
2012-11-09 17:00:26 Localisation Error in (S7/P36) : Connect : 1 Base Agence
2012-11-09 17:00:26 Type...... : EC
2012-11-09 17:00:26 Paramètres :  ODBC="9740" ; User = "admincpx"
2012-11-09 17:00:26 
2012-11-09 17:00:26 Message technique :
2012-11-09 17:00:26     E9001: Erreur de thread dans ExecuteConnect
2012-11-09 17:00:26     [Solid][SOLID ODBC Driver][SOLID]SOLID Communication Error 21306: Server ''tcp 10.97.4.1 1317'' not found, connection failed
2012-11-09 17:00:26     
2012-11-09 17:00:26 
2012-11-09 17:00:26 Message ODBC :
2012-11-09 17:00:26     
2012-11-09 17:00:26     
2012-11-09 17:00:26     
2012-11-09 17:00:26 -----------------------------------------------------------------------------------------------------------
2012-11-09 17:00:48 E9001: Erreur de thread dans ExecuteETLEngine
2012-11-09 17:00:48 In (S7/P38) -> (ACT1-3 - Recuperation Infos chez Agence/ETL Engine: requête sortant la liste des dossiers)
2012-11-09 17:00:48 Extraction - Conversion d''une requête de base de données en données intermédiaires échouée :Exception [TEngineETL.BuildConnectPoolSrc]
[Solid][SOLID ODBC Driver][SOLID]SOLID Communication Error 21306: Server ''tcp 10.97.4.1 1317'' not found, connection failed
Access violation at address 00A865F5 in module ''DEX_Server.exe''. Read of address 00000098

2012-11-09 17:00:48 ### ERREUR - EC
2012-11-09 17:00:48 ### CPCOM-ERREUR EC
2012-11-09 17:00:48 
2012-11-09 17:00:48 -----------------------------------------------------------------------------------------------------------
2012-11-09 17:00:48 Le 09-11-2012 à 17:00:48 CP2000AlerteMailDocumentExpert-OCEANINDIEN-PROD-CP2000AlerteMailDocumentExpert
2012-11-09 17:00:48 
2012-11-09 17:00:48 Erreur :  EC - Erreur DB : Erreur Connexion ou Requete -  ODBC="9740" ; User = "admincpx"
2012-11-09 17:00:48 Libellé... : Erreur DB : Erreur Connexion ou Requete
2012-11-09 17:00:48 Statut.... : -1200
2012-11-09 17:00:48 
2012-11-09 17:00:48 Localisation Error in (S7/P38) : ETL Engine: requête sortant la liste des dossiers
2012-11-09 17:00:48 Type...... :  EC
2012-11-09 17:00:48 Paramètres :  ODBC="9740" ; User = "admincpx"
2012-11-09 17:00:48 
2012-11-09 17:00:48 Message technique :
2012-11-09 17:00:48     E9001: Erreur de thread dans ExecuteETLEngine
2012-11-09 17:00:48     Extraction - Conversion d''une requête de base de données en données intermédiaires échouée :Exception [TEngineETL.BuildConnectPoolSrc]
[Solid][SOLID ODBC Driver][SOLID]SOLID Communication Error 21306: Server ''tcp 10.97.4.1 1317'' not found, connection failed
Access violation at address 00A865F5 in module ''DEX_Server.exe''. Read of address 00000098

2012-11-09 17:00:48     
2012-11-09 17:00:48 
2012-11-09 17:00:48 Message ODBC :
',null,28,5,17,4,85,11,4,'2012-11-12 10:26:22','2013-02-13 11:21:09','2012-11-12',90,null,null,802,1,2,0,'2012-11-30 15:55:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (803,2,4,'Onglet Documents (Synthèse)','Faire un nouvel onglet documents comportant uniquement les documents intéressants pour le gestionnaire assureur.

Afficher dans cet onglet uniquement les documents correspondants à une liste définie.
Comme les document internes n''ont pas de nature bien définie. La recherche est obligée de se faire sur le libellé.

Donner la liste des libellés correspondants aux documents éligible à cet onglet.

',null,39,1,null,4,74,4,3,'2012-11-12 10:32:05','2013-07-04 22:27:49',null,0,null,null,803,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (804,2,4,'Utiliser outlook pour envoyer un mail','Permettre de lancer le client courrier lors d''un clic sur un lien "mail".
Cette fonction doit être paramétrable en fonction du client majeur.

Certains clients choisiront le mail interne Hermès.
D''autres choisiront le mail avec "outlook" par exemple.

Ticket CitéSI 271','2013-05-27',3,2,25,4,62,4,17,'2012-11-12 10:34:54','2013-07-04 22:27:49','2013-05-27',60,16,null,804,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (805,2,4,'Ajouter J60, J90','Sur l''affichage d''un dossier, pour tous les profils, ajouter en haut a droite, en dessous du Jour J :
* le J60 (dos_date_j60)
* le J90 (dos_date_j90)

Ticket CiteSI 272','2012-11-30',30,10,24,4,62,4,10,'2012-11-12 10:36:03','2013-01-23 09:55:00','2012-11-19',90,null,null,805,1,2,0,'2012-12-04 16:56:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (806,2,2,'Allianz - Mail AR Gestion Classique','A l''instar de ce qui a été fait pour la gestion déléguée. Allianz demande à être prévenu lors d''un passage gestion déléguée --> gestion classique.

Donc compléter ce qui a été fait sur le sujet #125 (pour la gestion déléguée)

Sur changement d''état gestion déléguée --> gestion classique :

* Poser la question pour confirmer : Souhaitez-vous envoyer un mail "AR mode de gestion" au gestionnaire Allianz ?
** Si oui, envoyer un mail à Allianz
*** avec le lien Hermes dans le corps du mail (voir #124).
*** différence : Ne pas mettre "DELEX" dans l''objet. 

** Tracer l''envoi (CPXCHRONO)
** Archiver le mail (CPXDOC)
',null,6,1,null,4,null,4,4,'2012-11-12 10:50:57','2013-07-04 22:27:50',null,0,null,null,806,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (807,2,4,'Modification droit profil superviseur client','* Modifier les libellés
** Experts --> Annuaire experts
** Agences --> Annuaire agences

* Supprimer les accès dans le menu
** Documents annexes
** Internvenants
** Gestionnaires
** Ordre de mission

Ticket CitéSI 273','2013-05-28',2,2,25,4,62,4,17,'2012-11-12 11:04:06','2013-07-04 22:27:50','2013-05-28',60,8,null,807,1,2,0,'2013-01-23 10:16:14');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (808,2,4,'Vue accès direct','Modifier la mise en page pour les accès a partir des liens directs des notifications par mail.

2 colonnes :
* information à gauche
* document à droite

Voir pour proposition avec menu en haut ?

',null,30,1,null,4,74,4,3,'2012-11-12 11:10:01','2013-07-04 22:27:50',null,0,null,null,808,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (809,1,4,'Push mail inaccessible pour les dossiers de la Corse','A étudier !
Trouver un lien direct d''un dossier corse pour commencer.',null,2,2,4,4,null,4,3,'2012-11-12 11:13:14','2013-07-04 22:27:51',null,20,null,null,809,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (810,2,2,'DARVA - Création mission','Récupérer Opération Commentaire / Texte libre (balise xml <DE00080201>) pour renseigner le libellé Dommage

Manque de date de déclaration et date de mission lors de la récupération de données.

Plus regarder erreur de Jour J sur l''OM DARVA : VABFDO12075276 (voir sur le serveur 40)',null,33,5,5,4,59,5,36,'2012-11-13 11:05:25','2012-12-16 08:36:48','2012-11-13',90,null,769,769,14,15,0,'2012-12-12 17:14:18');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (811,2,2,'Darva - stocker les intervenants de l''OM','h2. Demande

Créer une table qui stocke à part les informations sur les "parties" de l''OM.

Cela peut contenir :
* le gestionnaire
* l''assuré / souscripteur
* l''expert

Cela sera utile, par exemple, pour :
* retrouver l''assuré obligatoire pour un AR Darva
* Renseigner automatiquement l''expert du dossier ou gestionnaire de la compagnie (a voir dans le futur, si c''est faisable)


h2. Structure de la table

Code "Qualité" Darva (se référer au référentiel Darva)
OMSID (pour faire le lien avec l''OM)
NOM + Adresse + Tel + Mail + Référence
','2012-11-14',33,5,5,4,59,4,10,'2012-11-13 18:16:45','2012-12-16 08:36:49','2012-11-14',90,8,769,769,16,17,0,'2012-11-23 10:15:51');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (812,2,6,'CP2000 AlerteDocumentExpert gérer la reprise sur erreur','lorsque le script ne peut se connecter à une ou plusieurs agences faire un fichier pour les enregistrer ainsi que la date de lancement pour pouvoir rejouer le script',null,28,5,17,4,85,17,2,'2012-11-14 09:32:14','2013-02-13 14:42:01','2012-11-14',100,null,null,812,1,2,0,'2013-02-13 14:42:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (813,2,6,'GSICASS: Ajouter les informations nom du gestionnaire, son téléphone et adresses mails du gestionnaire dans les mails de de blocage PFT et le mail de routage (SCRIPT GSICASSTRAITEMENTMISSION )','Ajouter les informations nom du gestionnaire et son téléphone dans les mails de de blocage PFT et le mail de routage (SCRIPT GSICASSTRAITEMENTMISSION)',null,40,5,11,4,90,11,4,'2012-11-14 10:04:22','2013-02-13 12:08:49','2012-11-14',70,null,null,813,1,2,0,'2012-11-30 15:59:09');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (814,2,2,'Gsicass - Libellé type traitement mission','Afficher le libellé dans le type de traitement de mission (utiliser une liste déroulante non modifiable)

|01|Expertise à faire sur place|
|02|Expertise nominative|
|03|Expertise téléphonique|



!CPXG097.png!','2012-11-15',4,5,5,4,59,4,6,'2012-11-14 14:23:47','2012-12-16 08:36:50','2012-11-15',60,2,null,814,1,2,0,'2012-11-23 14:19:44');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (815,1,6,'GSICASS: Dans envoyer rapport Champ convoque non cohérent avec base cp2000','Si plusieurs intervenant sont convoqué seul le le dernier est mis à true (voir brique ETL)',null,40,5,11,4,90,11,5,'2012-11-15 11:26:29','2013-02-13 11:39:12','2012-11-15',90,null,null,815,1,2,0,'2013-01-17 11:57:11');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (816,2,5,'CRUD Assurance + CRUD ASSUREUR','',null,null,5,8,4,null,10,6,'2012-11-16 15:52:43','2013-04-17 14:41:15','2012-11-16',90,null,null,816,1,2,0,'2013-04-17 14:41:15');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (817,2,5,'CRUD Clausier + CRUD Phrases','',null,null,5,8,4,null,10,4,'2012-11-16 15:53:12','2013-04-17 14:40:59','2012-11-16',90,null,null,817,1,2,0,'2013-04-17 14:40:59');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (818,2,5,'Document convocation','',null,null,5,18,4,null,10,3,'2012-11-16 16:04:16','2013-04-17 14:40:46','2012-11-16',100,null,738,738,6,7,0,'2013-04-17 14:40:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (819,2,6,'GSICASS: Changement sur le message des mails dans GSICASS traitement mission + ajout d''info','Changement sur le message des mails dans GSICASS traitement mission + ajout d''info',null,40,5,11,4,90,11,4,'2012-11-19 09:49:01','2013-02-13 12:09:14','2012-11-13',90,null,null,819,1,2,0,'2012-11-30 15:58:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (820,2,5,'Intégrer une librairie de communication exchange','',null,null,5,10,4,null,10,1,'2012-11-20 14:13:14','2013-04-17 14:40:22','2012-11-20',0,null,null,820,1,2,0,'2013-04-17 14:40:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (821,1,2,'DT726 DEVIS AXACAPE = pb signature expert','Le nom de l''expert = Expert signataire mais la signature ne correspond pas à celle de l''expert signataire --> c''est celle de l''expert rédacteur qui s''affiche ','2012-11-23',5,5,5,4,59,6,4,'2012-11-21 11:53:36','2012-12-16 08:36:50','2012-11-21',60,4,null,821,1,2,0,'2012-11-21 17:56:15');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (822,2,2,'bloquer la suppression d''un envoi EDI','Bloquer la suppression d''un envoi EDI à partir de l''écran CPXL266
!CPXL266!','2012-11-23',27,5,5,6,59,6,5,'2012-11-21 16:45:14','2012-12-16 08:36:50','2012-11-21',60,4,null,822,1,2,0,'2012-11-23 10:11:30');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (823,1,2,'DEVIS AXACAPE VALIDE est MODIFIABLE','Le devis AXA CAPE une fois validé se modifie : j''ai changé le nom de l''expert signataire sur le dossier et quand je consulte le document word que j''ai validé celui-ci s''est modifié --> ce n''est plus le nom de l''expert avec lequel je l''ai créé mais sa signature est restée. 
(exemple : doc avec Expert P. VERQUIERE + sa signature je modifie avec Expert L. GIROD et quand je consulte le doc = Expert L. GIROD + Signature de P. VERQUIERE)',null,null,1,null,4,null,6,0,'2012-11-21 18:02:51','2012-11-21 18:02:51','2012-11-21',0,null,null,823,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (824,1,2,'Validation HS des convocations après controles','Si on fait un contrôle des convocations, ensuite lorsqu''on valide, seul 1 destinataire est validé.

exemple sur le 0830/311866 sur le serveur 40.
Il y avait 11 destinataires.
Ici c''est le bénéficiaire qui est validé seulement',null,5,2,5,4,null,4,5,'2012-11-22 11:32:23','2012-11-29 18:09:47',null,0,8,null,824,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (825,3,2,'Export AQC','Faire un export des fiches crac/sycodes

voir [[Export AQC]]',null,21,5,4,4,null,4,2,'2012-11-22 17:08:43','2012-11-27 16:46:06','2012-11-21',20,24,null,825,1,2,0,'2012-11-27 16:46:06');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (826,2,2,'Remplacer la procédure de calcul de taille d''un document','Cause des lenteurs sur ouverture de document dans les grosses agences ! (comme Villecresnes)

CPXT051 : LP_CALCUL_TAILLE à revoir car
# cela fonctionne dans le dossier racine CP2000 --> collision entre les utilisateurs
# les commandes dos avec création bidon.bat surement optimisable (pourquoi creer un .bat ?)
# Pourquoi on boucle sur tous les documents ?

Voir si une telle procédure existe ailleurs.

','2012-11-26',5,5,4,4,75,4,4,'2012-11-26 15:31:01','2012-11-26 18:07:51','2012-11-26',100,8,null,826,1,2,0,'2012-11-26 18:07:51');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (827,1,15,'ReunionPartciper -','Prévoir le cas ou aucun fichier n''est saisis',null,null,5,18,4,null,10,4,'2012-11-27 14:38:04','2013-01-07 11:45:22','2012-11-27',100,null,null,827,1,2,0,'2013-01-07 11:45:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (828,1,15,'ReunionParticiper','Détail en lecture
On ne peut pas accéder au fichier
',null,null,5,18,4,null,10,5,'2012-11-27 15:53:15','2013-02-14 16:33:03','2012-11-27',100,null,null,828,1,2,0,'2013-02-14 16:33:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (829,1,15,'ReunionParticiper','Commentaire
Meme si je ne renseigne pas mon commentaire, il semble s''enregister à vide en BD',null,null,5,18,4,null,10,3,'2012-11-27 15:53:55','2013-01-07 11:45:37','2012-11-27',100,null,null,829,1,2,0,'2013-01-07 11:45:37');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (830,2,6,'DARVA:  Les flux XML provenant de Darva pour CP2000 doivent être modifiés pour remplacer les caractères majuscules accentué','les caractères en majuscule accentués doivent être remplacés par leurs équivalents non accentués',null,32,5,17,6,87,17,3,'2012-11-28 10:35:38','2013-04-02 16:12:25','2012-11-27',100,2,null,830,1,2,0,'2013-04-02 16:12:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (831,1,2,'Mise en page date de mission','Dans les documents, la date de mission (DOSDTDA) est écrite en toute lettre : 5 novembre 2012 au lieu de 05/11/2012 comme les autres dates
',null,5,1,null,4,null,4,0,'2012-11-28 11:36:44','2012-11-28 11:36:44',null,0,null,null,831,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (832,1,2,'Suppression des doublons sur OPEG','h2. Contexte

Un numero de G est censé être unique. Or il existe des doublons dans nos bases en production.
C''est problématique pour la migration vers Kastor

h2. Objectif

Trouver une solution pour fusionner les opérations en doublon

h2. Elements technique

h3. etat des lieux en production

voir attachment:resultat.txt

h3. requete sql utile

<pre><code class="sql">select * from cpxope where opeg in (select opeg from cpxope group by opeg having count(*) > 1); </code></pre>

h3. Tables sql à corriger

* CPXOPE
* CPXDOS
* CPXIOP
* CPXLOP
* CPXASO
* CPXPVREC',null,29,5,5,4,72,4,7,'2012-11-29 11:31:58','2013-02-10 09:34:08',null,90,null,null,832,1,2,0,'2013-01-15 10:34:15');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (833,1,15,'Pagination ajout de template','app_dev.php/document/template/lister

Le numéro des pages en bas du template est farfelu !
J''ai 83 pages alors que je n''ai qu un document',null,null,9,18,4,null,10,2,'2012-11-29 13:52:35','2012-11-30 11:12:23','2012-11-29',100,null,null,833,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (834,1,2,'dossier 0560/176003 - Anomalie Ecran CPXL010B','Ecran CPXG123 Envoi GSICASS - NOTE D''HONORAIRES --> Anomalie Ecran CPXL010B : 2 NH s''affiche sur l''écran de sélection (si consultation de la NH sans date de valorisation = document non présent) alors qu''une seule NH valorisée (cf copie d''écran)
!176003.PNG!','2012-11-30',26,5,4,4,null,6,1,'2012-11-30 11:33:45','2012-11-30 12:12:13','2012-11-30',100,null,null,834,1,2,0,'2012-11-30 12:12:13');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (835,1,2,'Fusion : autoriser uniquement les fichiers PDF','Suite #6.

Il faut filtrer les CPXDOC pour afficher uniquement que les PDF.
 

Origine : Sacha / Villescresnes 0940/474569

',null,5,5,5,4,59,4,3,'2012-12-03 11:19:52','2012-12-16 08:36:51','2012-12-03',90,4,null,835,1,2,0,'2012-12-14 16:15:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (836,2,2,'Gsicass - Date de réception obligatoire pour envoi de rapport','Ajouter dans les contrôles sur envois de rapports Gsicass :

la saisie d''au moins une date de réception dans l''opération est obligatoire.
Si absent, envoi de rapport Gsicass interdit.

h2. a faire 

tester si au moins 1 enregistrement présent dans CPXPVREC pour l''OPEID du dossier concerné','2012-12-04',47,5,5,4,59,4,14,'2012-12-03 14:45:55','2012-12-16 08:36:51','2012-12-03',90,4,null,836,1,2,0,'2012-12-14 16:22:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (837,2,6,'DataExchanger : Ajout des hooks pour les stats dans les scripts','Ajouter  les evenement et les hooks pour gérer les stats',null,31,2,null,4,137,11,4,'2012-12-04 09:54:38','2013-04-26 10:59:23','2012-12-04',50,null,null,837,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (838,2,2,'séparation PDF rend un document illisible avec les pages grisés','Besançon 0250/692967

La séparation des documents donne un résultat inutilisable.
Les pages des PDF deviennent toutes grises.

Note : Les agences utilisent Ghostcript 8.63 (datant de aout 2008)
Ghostcript est passé en version 9.06 en aout 2012.

',null,5,5,4,4,72,4,9,'2012-12-04 11:48:51','2013-02-10 09:34:09',null,90,40,null,838,1,2,0,'2013-01-21 10:20:28');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (839,1,15,'Adresse','J''ai affecté une adresse sans remplir les champs sauf pays, département, type et il c''est rattaché sur une adresse existante avec le même pays, département et type mais avec des adraddr1,2,3,4 rempli.',null,null,9,null,4,null,8,1,'2012-12-04 17:29:53','2013-05-14 13:28:48','2012-12-04',100,null,null,839,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (840,1,6,'GSICASS: Bug sur la gestion des erreurs du script GsicassEnvoiRapport le paragraphe erreur n''est pas exécuté','GSICASS: Bug sur la gestion des erreurs le paragraphe erreur n''est pas exécuté dans certains cas','2013-07-30',40,9,11,4,122,11,5,'2012-12-05 16:55:04','2013-05-13 12:04:14','2012-12-05',10,null,null,840,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (841,1,15,'count pour pagination without migration','Revoir la méthode count des repository manière de pouvoir rajouter l''option avec ou sans l''entrée MIGRATION car la plus par des entités paginé compte une entrée supplémentaire ce qui fausse le calcule du nombre de page total.',null,null,1,null,4,null,8,0,'2012-12-05 17:56:23','2012-12-05 17:56:23','2012-12-05',0,null,null,841,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (842,1,2,'Blocage dans DEMARRAGE de la sélection d''un Rapport Trame Expert pour Devis AXACAPE','Anomalie remontée par l''agence d''AMIENS (n''utilise pas la fonction "Mise à jour du DT" pour l''imprimer) et veut ajouter le DT717 ou DT060 dans les documents Existants. Ces 2 DT sont des trames EXPERT imprimés à l''ouverture du dossier et que l''on sélectionne dans la Procédure DEMARRAGE. Ils sont de nature "Rapport Expert" 
Impossible de les ajouter dans les documents existants --> Message bloquant "Avant tout ajout de rapport, veuillez créer un devis estimatif DT726"
ce message ne doit concerner que les documents de nature "Rapport secrétaire".
Supprimer si possible blocage pour les DT717 et DT060',null,null,1,null,4,null,6,0,'2012-12-10 11:47:42','2012-12-10 11:47:42','2012-12-10',0,null,null,842,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (843,2,2,'Reserves en rapport avec le dommage','h2. Impératif certification Darva 

h4. Anomalie n° 2 : 

La donnée 1253  « En rapport avec les désordres déclarés O/N » du segment 313 "Information tranche ou bâtiment" dans le groupe 03 "Groupe opération de construction" doit être renseignée car la donnée 1218 "Reserve à la réception O/N" est renseignée à O.

h2. Todo CP2000

Il faut créer un nouveau champs dans le dommage (Flag Oui/Non) pour spécifier si le dommage est concerné par une reserve de la date de réception

-+Todo+ : Voir pour un update SQL qui positionne le flag correctement selon la date de réception du dommage-

h2. Ecran dommage
!reserve-en-rapport-avec-le-dommage.png!

h3. Règle de Gestion (cf image)

# Si dans le dommage, je sélectionne une date de réception comportant une reserve, alors il faut afficher et renseigner le choix oui/non (obligatoire), sinon cacher le champs',null,9,5,5,6,59,4,9,'2012-12-10 12:19:39','2012-12-16 08:36:51','2012-12-10',90,null,null,843,1,2,0,'2012-12-14 16:15:11');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (845,2,6,'Darva Accusé de reception OM','Mettre les dates de rendez vous lorsqu elles sont renseignées dans CP2000',null,32,5,17,4,87,17,3,'2012-12-10 14:38:07','2013-04-02 16:11:46',null,0,null,null,845,1,2,0,'2013-04-02 16:11:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (846,2,4,'Filtre enjeu initial','h2. Demande

Mettre un filtre sur l’écran de recherche des dossiers portant sur l’enjeu initial.

h2. Objectif

Suite au RV avec ACS, Anne Marie Ponthier serait intéressé à avoir dans Hermès un flag / un champ lui indiquant que la mission est à fort enjeu (grands risques pour ACS soit enjeu global > 100k€)',null,30,1,null,4,null,4,2,'2012-12-10 18:18:52','2013-07-04 22:27:51',null,0,4,null,846,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (847,2,2,'Rapport - Calcul de l''année de garantie','Dans le rapport, CP2000 doit calculer le nombre d''année de garantie.
Date de déclaration - Date de réception = nombre d''année de garantie.
Valoriser le champs concerné dans le rapport :

!rapport-enieme-annee-garantie.png!
','2013-01-02',5,5,5,4,72,4,23,'2012-12-11 09:41:24','2013-02-10 09:34:09','2013-01-02',90,8,null,847,1,2,0,'2013-01-25 15:20:58');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (848,2,2,'Affichage de l''assuré dans l''onglet dossier','h2. Demande

Uniquement pour les missions hors construction, il faut afficher le nom de l''assuré dans onglet dossier.

h2. A faire

Pour les missions hors consctruction : (MISINDCST = ''F'')
Voir CPXG008 pour exemple : il faut afficher REPNOM du REPID de CPXPAS.

h2. Ecran

Remonter d''1 ligne l''état de traitement.
Supprimer la "grosseur" du dossier
En remplacement, afficher l''assuré dans le cadre d''un dossier hors construction


!assure-dossier.png!',null,16,5,5,4,72,4,11,'2012-12-11 10:05:01','2013-02-10 09:34:09',null,90,8,null,848,1,2,0,'2013-01-25 15:23:32');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (849,1,6,'DARVA: Script darvaenvoyerNH prb arrondi au millieme sur le total TVA','Script darvaenvoyerNH prb arrondi au millieme sur le total TVA : si le total TVA fini par .005 la fonction Round arrondi à la valeur inférieur or pour être cohérent avec CP2000 c''est un arrondi à la valeur supérieur qu''il faut faire',null,32,5,11,4,87,11,2,'2012-12-12 15:30:31','2013-04-02 16:14:27','2012-12-12',100,null,null,849,1,2,0,'2013-02-12 16:26:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (850,2,2,'Gsicass - Date Droc obligatoire pour envoi de rapport','Ajouter dans les contrôles sur envois de rapports Gsicass :

Date Droc (ou Doc) de l''opération obligatoire
Si absent, envoi de rapport Gsicass interdit.',null,27,5,5,4,72,4,6,'2012-12-12 17:08:10','2013-02-10 09:34:09',null,90,8,null,850,1,2,0,'2013-01-15 16:11:49');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (851,3,2,'Livraison 1.9.9 - Darva','Préparation livraison CP2000 pour mise en production du module Darva

h2. Contenu

Anomalie #629: Suppression d''un mail envoyé Ecran CPXL266
Anomalie #696: ANOMALIE ECRAN CPXL010B Envoi de document (en base de test comme en agence)
Anomalie #749: OM - controle sur l''existence d''un dossier HS
Anomalie #795: Bug dans la gestion des doublons à la création de mission à partir d''un OM DARVA
Anomalie #821: DT726 DEVIS AXACAPE = pb signature expert
Anomalie #835: Fusion : autoriser uniquement les fichiers PDF
Evolution #249: Gestionnaire/client ou expert Inactif
Evolution #484: A la création du dossier, générer une clé d''accès publique (Hermes/Allianz)
Evolution #574: Darva - envoi rapport
Evolution #593: Rotation des documents PDF
Evolution #611: Darva - accusé de reception d''un OM
Evolution #615: Darva - Détail d''un OM
Evolution #616: Choix client - ne pas autoriser de travailler avec un client inactif
Evolution #617: Darva - Envoi note d''honoraire
Evolution #618: Darva - Reception des messages en attente
Evolution #646: Ajout filtrage Origine/Expéditeur à partir de l''onglet Documents Ext.
Evolution #652: Separation des documents - info sur l''ordre de séparation
Evolution #711: Civilité dans les destinataires pour les assureurs
Evolution #730: SMART - paramétrage : ajouter un PDF
Evolution #769: Darva
Evolution #787: Modification de l''arborescence DARVA/GSICASS pour Data Exchanger dans la gestion d''envoi de documents
Evolution #792: Darva - envoi SD commentaire
Evolution #794: Contrôle d''infos obligatoires pour l envoi de rapport DARVA
Evolution #810: DARVA - Création mission
Evolution #811: Darva - stocker les intervenants de l''OM
Evolution #814: Gsicass - Libellé type traitement mission
Evolution #822: bloquer la suppression d''un envoi EDI
Evolution #836: Gsicass - Date de réception obligatoire pour envoi de rapport
Evolution #843: Reserves en rapport avec le dommage

h2. Infos spécifiques

h3. Support - Brice

Sujet ayant pour origine le support :
Anomalie #835: Fusion : autoriser uniquement les fichiers PDF (sacha)

h3. Dex - Guillaume

Nécessite mise en prod changement scénario Dex :
Evolution #787: Modification de l''arborescence DARVA/GSICASS pour Data Exchanger dans la gestion d''envoi de documents

h3. Kastor - Sabrina

Modification de structure :
Evolution #843: Reserves en rapport avec le dommage

h2. Module Darva

La passerelle entre CP2000 et Darva sera ouverte sur feu vert d''Eurisk, vraisemblablement début janvier avec 3 agences pilotes (Villecresnes, Toulon, Toulouse)
','2012-12-15',null,5,4,4,59,4,1,'2012-12-14 09:56:49','2012-12-17 14:32:55','2012-12-14',100,16,null,851,1,2,0,'2012-12-17 14:32:55');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (852,1,6,'GSICASS: Script NoteHonoraire Bug les conversions en double génére l''ajout de 0.00000000001 sur le pTotalTTCFrais','Appliquer un round pour compenser l''erreur ',null,40,5,11,4,90,11,3,'2012-12-17 14:55:17','2013-02-13 11:39:28','2012-12-17',100,null,null,852,1,2,0,'2013-01-17 11:58:44');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (853,1,2,'Liste des envois - affichage des documents non corrects','Suite à l''évolution Darva - SD commentaire #792 (voir r469).
Nous avons un problème sur l''affichage des documents Eurisk Interne avec Destinataire :
Le document affiché est le tronc commun au lieu du document CPXDES correspondant.

h2. A faire 

* Faire évoluer l''affichage pour prendre en compte l''existence d''un document avec destinataire
* Afficher les document interne Eurisk en +lecture seule+ (se calquer sur ce qui existe dans le CPXG014)',null,27,5,5,4,77,4,4,'2012-12-18 17:20:49','2012-12-20 13:04:04',null,100,16,null,853,1,2,0,'2012-12-20 10:26:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (854,2,2,'Ajout lbocage DARVA : Souscripteur','Bloquer avec messages si le souscripteur n''est pas renseigné dans la POLICE',null,null,1,null,4,null,6,0,'2012-12-19 18:14:07','2012-12-19 18:14:07','2012-12-19',0,null,null,854,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (855,1,2,'Bloquer Possibilité de supprimer l''envoi d''un FAX','dans l''écran CPXL266 on peut supprimer l''envoi d''un FAX : à bloquer SVP',null,27,5,5,4,72,6,7,'2012-12-19 18:18:52','2013-02-10 09:34:10',null,90,8,null,855,1,2,0,'2013-01-15 14:31:04');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (856,1,15,'Path Logo Société','Le chemin du Logo des sociétés n''est pas correctement entré en BASE
socPath : NULL
socLogo: C:\\wamp\\tmp\\phpFE68.tmp

=> on s''attend a:
socPath : /uploads/societes/societe{soc_id}/
socLogo: imagenom.jpg

CF -> expert.php',null,null,5,18,4,null,10,2,'2012-12-20 16:39:00','2013-01-07 11:47:17','2012-12-20',100,null,null,856,1,2,0,'2013-01-02 10:32:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (857,1,6,'CP2000: Anomalie dans CP2000AlerteMailDocumentExpert sur les documents remontés','Problème levé suite à l''échange suivant:


En effet, nous vous confirmons le problème dans le script générant le mail de synthèse sur les documents  intégrés (uniquement les documents intégrés et indexés à la date du jour sont remontés), ce problème est lié à une faiblesse des informations dans la base CP2000 (difficulté à discerner un document non indexé sans générer une redondance dans la « synthèse » du lendemain). Nous travaillons sur le sujet et nous vous tiendrons informé.

Merci

Cordialement,
Guillaume Bonnet
Service Informatique
Groupe Prunay
05 61 39 70 39


_____________________________________________
De : Eurisk Aquitaine/BARSACQ Martin 
Envoyé : jeudi 27 décembre 2012 09:13
À : CPCOM/BONNET Guillaume
Cc : Eurisk Aquitaine/HENRY Karine; Eurisk Aquitaine/JUNIUS Patrice
Objet : pb data exchanger


Bjr

Suite à notre echange tel d hier concernant les remontées ou non de doc dans les dataexchanger:

notre secretaire a effectivement pu vérifier en indexant des docs hier qui avaient antérieurement été intégrés l''avant veille….: ils ne remontent pas dans le data exchanger de l''expert…
Je pense que vous avez là un élément de confirmation allant dans le sens de votre explication…

Pour notre part nous faisons remonter l''info en interne pour voir si la procedure doit donc imposer une indexation en même temps que l''intégration (et non a postériori)...
Nous vous laissons le soin de voir en interne si ce point informatique ne pourrait pas être amélioré…

bonne journée 
Martin BARSACQ
Expert Généraliste EURISK
5, rue Raymond LAVIGNE - 33 100 BORDEAUX             
Tel: 05 56 52 42 43
Fax: 01 77 56 60 33
',null,28,5,17,4,85,11,4,'2013-01-03 11:42:07','2013-04-02 15:39:45','2013-01-03',90,null,null,857,1,2,0,'2013-04-02 15:39:45');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (858,1,15,'Document type','S''assurer que la table documenttemplate est supprimée du modèle
S''assurer que le "path" soit bien rentré en BD
Le chemin doit être égal à
templates/template_{dtyId}/{naturedudocument}.{extansion}',null,null,5,18,4,null,10,1,'2013-01-03 12:58:06','2013-01-07 11:50:48','2013-01-03',0,null,null,858,1,2,0,'2013-01-07 11:50:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (859,1,15,'Génération de document','Lorsque l''on rentre un modele de document de type DocumentOm, on obtient l''erreur suivante lors de la génération


Neither property "docComplement" nor method "getDocComplement()" nor method "isDocComplement()" exists in class "CPCOM\\CPWebCommonBundle\\Entity\\DocumentPhysique" ',null,null,5,18,4,null,10,4,'2013-01-03 13:01:09','2013-02-14 16:31:45','2013-01-07',100,null,null,859,1,4,0,'2013-02-14 16:31:45');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (860,1,15,'Path dans destinataire','Le path dans le destinataire est vide
Il devrait etre de la forme
uploads\*dossiers*\{2013}\dossier_{2054}\documents\document_{217840}\destinataires*\destinataire_{00001}*\Convocation.rtf

Dossier -> au plurieul
Ajouter un rep : destinataire + id dest',null,null,3,18,4,null,10,2,'2013-01-03 13:18:38','2013-03-05 11:24:17','2013-01-03',0,null,null,860,1,2,0,'2013-03-05 11:24:17');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (861,1,2,'Fiche barème ','Dossier TOULON 311789 : problème sur fiche barème qui bloque = accès à la sycodes mais pas à la CRAC, alors que le montant de l''indemnité est valable (sachant que tt est correctement renseigné).',null,null,2,4,4,null,21,10,'2013-01-04 13:16:35','2013-05-16 14:52:29',null,0,8,null,861,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (862,2,5,'Substitution de connexion','L''objectif étant de permettre aux utilisateurs *ayant les droit de "création"* sur un objet "USER" de se substituer à cet utilisateur.
- Proposer une interface de sélection (nouvelle page) dans le menu Profil -> Utilisateur > Substitution
- Imposer une page de validation qui affiche l''utilisateur ainsi que tous ses UserRoleAgence actif
- Si il y a confirmation de substitution : remplacer le profil existant dans le SecurityContext par le profil sélectionné (Cumul + Droit)

Restriction : étant donné qu''aucun mot de passe ne sera demandé, il n''y aura pas d''authentification Exchange avec cet utilisateur
=> l''envoi de mail et les échanges avec le calendrier seront impossibles
OU 
=> l''envoi de mail et les échanges avec le calendrier se fera avec l''utilisateur initialement connecté -- attention aux répercutions métiers

Pour se reconnecter, l''utilisateur original devra se réhautentifier !
',null,null,5,18,4,null,10,2,'2013-01-07 10:55:09','2013-04-17 14:39:59','2013-01-07',0,null,371,365,31,32,0,'2013-04-17 14:39:59');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (863,1,15,'Probleme lorsque le modele est de type interne','Neither property "docRemonteeHermes" nor method "getDocRemonteeHermes()" nor method "isDocRemonteeHermes()" exists in class "CPCOM\CPWebCommonBundle\Entity\DocumentInterne" ',null,null,5,18,4,null,10,2,'2013-01-07 11:40:01','2013-02-14 16:31:01','2013-01-07',0,null,859,859,2,3,0,'2013-02-14 16:31:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (864,1,15,'Reunion - Erreur lors de la suppression','all to a member function getDocumentDynamicFieldValues() on a non-object in C:\wamp\www\cpweb\src\CPCOM\DossierBundle\Controller\DefaultController.php on line 2086',null,null,5,18,4,null,10,3,'2013-01-07 11:46:55','2013-02-14 16:30:22','2013-01-07',0,null,null,864,1,2,0,'2013-02-14 16:30:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (865,1,6,'NOE: Prb sur la gestion des entités XML et sur l''encodage dans le script NOEExport','Le fichier source du message est encodé en UTF-8 or les opérations en VB utilise l''ANSI. De plus, un moyen optimum doit être trouvé pour gérer les entités XML',null,59,5,null,4,93,11,3,'2013-01-08 16:41:38','2013-02-13 11:40:20','2013-01-08',100,null,null,865,1,2,0,'2013-01-17 12:03:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (866,1,2,'Date de mise a jour d''un dossier fausse','Sur ouverture d''un dossier terminé, meme si on ne modifie aucun champs, lorsqu''on ferme de dossier avec "enregistrer et quitter" ou "enregistrer sans quitter", la date de modification du dossier est mise à jour.

Le dossier est donc marquer comme à mettre à jour sur Hermes, or il a évité les controles des champs obligatoire de la fermeture du dossier et donc il y a des "trous" d''information dans le dossier bloquant pour la remontée Hermès.

Pb vu par Serge/François le 10/01/2013 sur un dossier de Toulouse (datant de 2001)

h2. A faire

Piste :
Si etat dossier = MTA ou MTE, sans etre passé par les controles (dans le value change normalement), alors ne pas passer par le store CPXDOS.

',null,16,5,5,6,72,4,6,'2013-01-10 09:25:35','2013-02-10 09:34:10',null,90,16,null,866,1,2,0,'2013-01-21 10:10:59');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (867,2,20,'Creer programme externe pour tuer des process','Creer un programme qui a pour role de tuer une liste de processus (ce programme servira notamment pour le transfert hermes lorsque celui ci se passe mal)',null,null,5,17,4,107,17,3,'2013-01-10 09:35:50','2013-02-13 14:48:24','2013-01-09',100,null,null,867,1,2,0,'2013-02-13 14:48:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (868,2,2,'FUSION DE DOCUMENTS A ELARGIR sur image + fichier texte','Ajouter la possibilité de fusionner des documents de type Image et Fichier texte pour permettre aux assistantes la fusion de photos ou de mails : actuellement seuls les documents PDF peuvent être fusionnés',null,5,5,5,6,72,6,18,'2013-01-10 11:47:42','2013-02-10 09:34:11','2013-01-10',90,8,null,868,1,2,0,'2013-01-25 15:24:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (869,3,6,'CP2000AlerteMailDocument re envoyer les mails à un experts sur toute la période','suite à un changement d''IP d''une machine on doit renvoyer les mails à un expert sur toute la période',null,28,5,17,5,85,17,3,'2013-01-10 14:38:36','2013-04-02 15:43:23','2013-01-10',100,null,null,869,1,2,0,'2013-04-02 15:43:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (870,2,2,'MODIFICATIONS DES ECRANS DE RECEPTION SD COMMENTAIRE DARVA','SVP, voir mes demandes de modifications des différents écrans pour la RECEPTION D''UN SD COMMENTAIRE

h2. A faire

Concerne notamment l''onglet des messages en attente CPXT095
',null,null,1,4,4,null,6,3,'2013-01-10 15:48:48','2013-03-08 14:39:46','2013-01-10',0,null,null,870,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (871,1,2,'Bloquer la suppression d''un envoi FAX','lors des précédents tests on avait pu supprimer l''envoi d''un FAX; merci de vérifier si un blocage existe en Agence si non le mettre bloquer la suppression',null,null,5,4,6,null,6,2,'2013-01-10 15:54:28','2013-01-10 17:56:22','2013-01-10',0,null,null,871,1,2,0,'2013-01-10 17:56:22');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (872,2,2,'Ajout d''un contrôle avant d''intégrer des Docs Externes','Rajouter un contrôle avant de donner la possibilité d''intégrer des documents externes :
 - Les 2 codes client doivent être renseignés
 - Les 2 Gestionnaires doivent être sélectionnés
 - Chaque client mineur doit être rattaché à un client majeur

Afficher un message d''interdiction pour chaque condition non remplie.',null,5,5,5,4,72,5,24,'2013-01-10 16:13:23','2013-02-10 09:34:11','2013-01-10',60,null,null,872,1,2,0,'2013-02-07 14:52:57');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (873,1,15,'Modele de document','- la date de fin de valeur du modèle ne doit pas être obligatoire
- lorsque l''on crée un document, il s''agit forcément d''un *document interne* -> pas de liste de choix de modèle
- dans la liste des modèle, si le fichier n''existe pas, revenir sur la page précédant (historique ou liste)
',null,null,5,18,4,null,10,4,'2013-01-11 09:16:07','2013-02-15 14:28:40','2013-01-11',0,null,null,873,1,2,0,'2013-02-15 14:28:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (874,1,15,'Convocation','S''assurer que lors que la génération d''une convocation, on prend bien:
- l''expert rédacteur/signataire courant (dossier_user / date)
- l''agence courante (dossier_agence / date)
',null,null,3,18,4,null,10,1,'2013-01-11 09:19:19','2013-04-24 11:30:00','2013-01-11',100,null,null,874,1,2,0,'2013-04-24 11:30:00');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (875,1,6,'Darva : valeur type de base incorrecte','La valeur du type de la  base dans la balise contexte doit être Prod et non pas PROD',null,32,5,17,4,87,17,3,'2013-01-14 16:21:55','2013-04-02 16:14:27','2013-01-14',90,null,null,875,1,2,0,'2013-04-02 16:14:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (876,2,6,'DARVA deploiement sur agences eurisk','Mettre à jour en fonction du code postal le code agence dans le referentiel dans la table darvacpagence',null,32,5,17,4,87,17,4,'2013-01-14 16:34:29','2013-04-02 16:10:28','2013-01-14',100,null,null,876,1,2,0,'2013-04-02 16:10:28');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (877,2,6,'Darva : faire doc support','mettre a jour celui du traitement mission
faire les suivants :
accusé de reception
envoi commentaire
envoi rapport
envoi NH','2013-07-31',32,1,null,4,121,17,4,'2013-01-14 16:44:22','2013-05-13 10:59:19','2013-07-12',0,null,null,877,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (878,2,6,'REUNION GSICASS','Préparation et Réunion',null,40,1,11,4,122,11,3,'2013-01-15 11:53:30','2013-04-26 15:28:27','2013-01-15',0,null,null,878,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (879,2,2,'Extraction pour la compta - ajouter la notion d''indemnité','E. Beeker

Dans l''export CA vers la compta, enrichir le fichier pour pouvoir répartir différent la rétrocession pour les experts
si DO et < TM, alors 90%
si DO et > TM, alors 100%
si RC/RCD, alors 105%

h2. a faire

Modifier la procédure CaVac de D. Gouveia','2013-01-15',null,5,4,4,null,4,1,'2013-01-15 16:56:44','2013-01-15 17:09:08','2013-01-09',0,null,null,879,1,2,0,'2013-01-15 17:09:08');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (880,1,6,'GSICASS: Prb pour rechercher le bon code ACAM','La requete la plus proche de la solution serait celle-ci:

SELECT 
CPXREP.REPID ,
CPXREP.REPNOM,
CPXREP.REPADR1,
CPXREP.REPADR2,
CPXREP.REPADR3,
CPXREP.REPCP,
CPXREP.REPVILLE,
CPXREP.REPPAYS,
CPXIDO.IDOINTER,
CPXASP.ASPACP

FROM
CPXOMS,CPXDOS,CPXIDO,CPXREP,CPXASS,CPXASP  
WHERE 
CPXOMS.OMSNUM = CPXDOS.DOSDRVOM
AND CPXDOS.DOSNUM = CPXIDO.DOSNUM 
AND CPXDOS.AGECOD = CPXIDO.AGECOD
AND CPXIDO.REPID =  CPXREP.REPID
AND CPXIDO.REPIREP = CPXREP.REPIREP
AND CPXIDO.IDOIPI= ''I''
AND CPXIDO.REPID = CPXASS.REPID
AND CPXASS.ASPID= CPXASP.ASPID
AND CPXOMS.OMSNUM = ''<<pNumeroMission>>''

Malheureusement la base CP2000 ne permet pas de connaître le contrat en cours, cette requête retourne donc plus de tuple que nécessaire
 


','2013-04-22',40,2,null,4,122,11,4,'2013-01-15 17:01:57','2013-05-13 11:54:19','2013-01-15',90,null,null,880,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (881,1,6,'Darva : mettre le code abonne darva ','mettre le code abonne darva en fonction de l''agence
ajout du code dans le referentiel dans la table darvautilisateur',null,32,5,17,5,87,17,3,'2013-01-15 17:19:46','2013-04-02 16:14:27','2013-01-15',90,null,null,881,1,2,0,'2013-04-02 16:14:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (882,1,6,'Darva : Lors de la recuperation des OMs ignorer les erreurs de documents extraits','Lors de la recuperation des OMs ignorez les erreurs de documents extraits car celà peut arriver s ''il y a par exemple des SD65 (V3) à disposition',null,32,5,17,5,87,17,3,'2013-01-15 17:21:31','2013-04-02 16:14:27','2013-01-15',90,null,null,882,1,2,0,'2013-04-02 16:14:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (883,2,5,'Recherche Bundle','- Proposer un module de recherche pour l''application
- Étudier la possible intégration de Solr',null,null,5,8,4,null,10,5,'2013-01-16 09:52:04','2013-04-17 14:39:40','2013-01-16',50,null,null,883,1,2,0,'2013-04-17 14:39:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (884,1,6,'GSICASS: La note honoraire est envoyée avant le rapport ','Lors d''une émission d''un rapport suivi d''une NH. Le Trigger DeX détecte la NH avant le rapport  ',null,40,9,11,4,122,11,6,'2013-01-17 13:54:42','2013-05-13 11:52:50','2013-01-17',80,null,null,884,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (885,1,2,'Indemnité ','Dossier 311945 et autres (après vérif) : onglet dommage, écran CPXG015 saisie d''une indemnité sur le dommage 2 que j''ai mis par erreur en garanti : je clique sur non garantie (sans me préoccuper de l''indemnité pensant que celle-ci s''annule auto) et mets un enjeu. j''enregistre et quitte cet écran. Dans l''onglet date, récap. du dossier : j''ai l''enjeu du dossier ainsi que l''indemnité calculée correctement alors que si je retourne dans l''onglet dommage l''indemnité du dommage 2 ne s''est pas annulée. Je voulais vérifier sur Hermès si elle apparaissait alors que le rapport était validé mais ne peux y accéder de la base test. Pour que l''indemnité disparaisse, je dois retourner dans le dossier, l''effacer par moi-même (la remettre puis l''enlever) et alors tt se remet à 0. (Faut il encore que je me rende compte de cette erreur sur le dossier).',null,null,1,4,4,null,21,0,'2013-01-17 15:25:01','2013-01-17 15:25:01','2013-01-17',0,null,null,885,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (886,1,2,'Indemnité ','Dossier 311945 et autres (après vérif) : onglet dommage, écran CPXG015 saisie d''une indemnité sur le dommage 2 que j''ai mis par erreur en garanti : je clique sur non garantie (sans me préoccuper de l''indemnité pensant que celle-ci s''annule auto) et mets un enjeu. j''enregistre et quitte cet écran. Dans l''onglet date, récap. du dossier : j''ai l''enjeu du dossier ainsi que l''indemnité calculée correctement alors que si je retourne dans l''onglet dommage l''indemnité du dommage 2 ne s''est pas annulée. Je voulais vérifier sur Hermès si elle apparaissait alors que le rapport était validé mais ne peux y accéder de la base test. Pour que l''indemnité disparaisse, je dois retourner dans le dossier, l''effacer par moi-même (la remettre puis l''enlever) et alors tt se remet à 0. (Faut il encore que je me rende compte de cette erreur sur le dossier).',null,null,5,4,4,null,21,2,'2013-01-17 15:25:25','2013-01-17 17:05:15','2013-01-17',0,null,null,886,1,2,0,'2013-01-17 17:05:15');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (887,2,6,'CP2000DuplicationDeDossier gestions du lien entre les agences source et destination','actuellement on a une liste d agence source et une liste d agence destination autorisées, il faut gérer le fait que certaines agences source ne puissent pas interagir avec certaines agences dest, pour celà crééer une table dans le référentiel a ajoutant les couples agence autorisés ',null,28,5,17,4,86,17,2,'2013-01-18 16:00:25','2013-02-12 16:09:48','2013-01-18',100,null,null,887,1,2,0,'2013-02-12 16:09:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (888,1,2,'OM GSICASS : Erreur création mission lorsqu''il s''agit d''une police par Aliment ','Lorsqu''il s''agit d''une police par Aliment, le système n''identifie pas si l''opération de construction existe. Automatiquement à partir de l''OM il est créé une nouvelle opération sur une police par aliment existante, alors que l''opération existe. 
Sera-t-il possible d''affiner la recherche d''antérioté : 
* police
** opération de construction existante
 
DOSSIER sur LORIENT 0560/176184

h2. a faire

Si test

* Nom de l''opération = nom du chantier de l''OM
* code postal opération = code postal du chantier de l''OM

+alors+

* Ne pas créer d''opération supplémentaire
* Mais récupérer l''OPEID trouvé et l''affecter au dossier en cours de création.
','2013-06-13',4,1,5,6,89,6,11,'2013-01-21 16:21:16','2013-05-21 17:23:24','2013-06-12',0,16,null,888,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (889,1,15,'Choix du gestionnaire dans le dossier','Proposer uniquement la liste des gestionnaire liée au client du dossier

-> 1 Client -> 1 Client Majeur -> N Gestionnaire',null,null,5,8,4,null,10,3,'2013-01-22 13:37:16','2013-02-14 16:51:03','2013-01-22',100,null,null,889,1,2,0,'2013-02-14 16:51:03');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (890,1,15,'Role Intervenant','Lors de la migration, le role par défaut est à migration -> OK
Comme le rôle n''est pas présent dans le "select" du rôle assuré, la valeur "Assuré" est visible par défaut -> NOK Il faut que la valeur vide soit sélectionnée par défaut',null,null,5,18,4,null,10,2,'2013-01-22 13:48:06','2013-02-14 16:19:54','2013-01-22',100,null,null,890,1,2,0,'2013-02-14 16:19:54');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (891,2,2,'RESERVES A LA RECEPTION en rapport avec les dommages :à insérer dans les rapports','Dans Ecran CPXG015 on renseigne : Réserves en rapport avec les dommages OUI NON --> il faudrait que cette notion s''insère automatiquement dans nos rapports. cf #843
Au niveau de la date de la réception (voir copie d''écran : actuellement c''est l''assistante qui démasque la phrase et sélection OUI ou NON)= c''est la notion Réserves en rapport OUI qui est prioritaire si plusieurs dommages  
!RESERVESRECEP.PNG!',null,null,1,4,4,null,6,1,'2013-01-22 14:00:41','2013-01-22 14:14:58','2013-01-22',0,null,null,891,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (892,2,5,'Onglet document','- ajout d''un document de type mail
 1 - Lister les dossier
 2 - afficher les mails d''un dossier
 3 - voir le contenu du mail d''un dossier
 4 - enregistrer le mail dans notre base - _Voir comment enregistrer les pièces jointes_** --',null,null,4,18,4,null,10,38,'2013-01-22 15:13:37','2013-04-26 09:47:59','2013-03-26',100,28,391,391,14,19,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (893,2,2,'Modification blocage Reserves en rapport avec le dommage impératif DARVA','Suite demande évolution #843 : modifier le blocage sur Ecran CPXG015 --> seulement quand on complète le Dommage constaté et la position Eurisk

Anomalie actuelle en base TOULON : le blocage "Réserve émise sur la date de réception. Veuillez renseigner si la réserve est en rapport avec le dommage" s''affiche à l''enregistrement de la mission ce qui empêche toute mise à jour du dommage avant expertise. On est au début du dossier et l''assistante ne sait pas si réserves en rapport ou pas --> c''est l''expert qui donnera ce renseignement lors de la dictée du rapport. 

h2. A faire 

Enlever contrôle sur l''enregistrement des dommages.
Ajouter le contrôle sur la validation des rapports (comme pour le message a propos de compléter l''enjeux)
',null,9,1,5,6,89,6,11,'2013-01-22 15:17:38','2013-05-16 11:01:12',null,0,8,null,893,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (894,1,6,'NOE: NOEExportMessage génére une erreur si la package a une taille à 0','NOEExportMessage génére une erreur si la package à une taille à 0. Gestion du mode paresseux à ajouter',null,59,5,11,4,93,11,3,'2013-01-22 15:37:32','2013-04-02 16:16:33','2013-01-22',100,null,null,894,1,2,0,'2013-04-02 16:16:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (895,1,15,'Ajout d''un intervenant','http://dev.cpcom.com/app_dev.php/dossier/intervenants/intervenantconstruction/ajouter/14186/INTI

Call to a member function getIntervenant() on a non-object in C:\wamp\www\cpweb\src\CPCOM\DossierBundle\Controller\DossierIntervenantsController.php ',null,null,5,18,4,null,10,1,'2013-01-22 15:37:48','2013-02-14 16:21:43','2013-01-22',0,null,null,895,1,2,0,'2013-02-14 16:21:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (896,3,2,'Etat RP + NH','Demande DS/EB :

liste des dossiers ayant les critères suivants :
Climaj = SMABTP
RP + NH
pour 2012

Afficher l''indemnité','2013-01-23',null,5,4,4,null,4,2,'2013-01-23 17:37:41','2013-01-24 12:26:18','2013-01-23',100,2,null,896,1,2,0,'2013-01-24 12:26:18');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (897,2,2,'Indemnité obligatoire  (EB)','h2. Demande E. Beeker

Rendre l''indemnité obligatoire

h2. Spécifications

Blocage à réaliser sur la création de NH + Valorisation NH
* Si dossier non judiciaire
** Si Type de mission = construction (DO)
*** -Si présence d''un rapport préliminaire +ET+ plus d''un rapport présent dans le dossier-
*** -+OU+ Si absence d''un rapport préliminaire +ET+ Au moins un rapport présent-
*** Si présence d''un rapport ayant une sous nature = RU +OU+ RE
*** +OU+ si absence d''un document de nature "rapport secrétaire"
### Vérifier si tous les dommages ont une prise de position renseignée
### Vérifier si tous les dommages *garantis* ont une indemnité renseignée

si KO, alors bloquer la création de la NH + message explicatif sur la raison du blocage
### "Le dommage n°XXX n''a pas de prise de position"
### "L''indemnité du dommage garanti n° XXX n''est pas renseignée"

-Rapport Préliminaire = présence DT059 ou DT791)-
Une indemnité à 0 est possible (réparation par l''entreprise). Donc ne tester que la présence d''une indemnité.

','2013-01-30',9,5,5,6,72,4,40,'2013-01-25 10:25:33','2013-02-10 09:34:12','2013-01-28',50,24,null,897,1,2,0,'2013-02-07 12:49:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (898,1,2,'A partir d''un OM Gsicass l''enregistrement d''une mission se fait sans création de police et opération','0440/519998 : Cette mission a été enregistrée à partir d’un OM GSICASS mais la police et l’opération ne se sont pas créées. 


',null,null,1,4,4,null,6,0,'2013-01-25 16:06:55','2013-01-25 16:06:55','2013-01-25',0,null,null,898,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (899,2,6,'GSICASS: Analyse et développement pour la Version 7 ','Analyse et développement pour la Version 7 ','2013-09-02',40,2,11,4,83,11,3,'2013-01-28 10:11:16','2013-05-13 11:50:44','2013-01-28',10,null,null,899,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (900,1,6,'Darva : Traitement mission bug lors de l ouverture a plusieurs agences','lorsqu''une erreur intervient sur une agence il faut poursuivre sur les autres
+ bug dans le erreur libdarva ne pas terminer le script avec la condition présente',null,32,5,17,5,87,17,3,'2013-01-28 17:17:15','2013-04-02 16:14:28','2013-01-28',90,null,null,900,1,2,0,'2013-04-02 16:14:28');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (901,2,6,'CP2000AlerteDocumentMail Gerer la reprise sur erreur lors du non envoi d''un mail','Lorsqu ''un email n''a pu être envoyé à l''expert enregistrer son adresse mail et creer un fichier reprise pour pouvoir rejouer l''envoi',null,28,5,17,5,85,17,4,'2013-01-29 10:20:27','2013-04-02 15:43:46','2013-01-29',100,null,null,901,1,2,0,'2013-04-02 15:43:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (902,2,6,'Darva V3 pour AXA','Faire évoluer les scripts et développer les scripts pour les nouveaux messages','2013-06-28',75,2,4,4,82,17,77,'2013-01-30 12:09:43','2013-07-04 22:27:51','2013-02-07',70,1,null,902,1,46,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (903,1,6,'Darva TraitemementMission : Bug lors du split du fichier XML','le fichier XML de l''OM reçu a l air bon mais le split en fichier partie et date échoue',null,32,5,17,6,87,17,3,'2013-01-30 14:59:09','2013-04-02 16:14:28','2013-01-30',90,null,null,903,1,2,0,'2013-04-02 16:14:28');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (904,1,6,'Darva TraitementMission caractère interdit XML','il faut que le programme externe qui transforme le XML de l''OM remplace les caractères interdits (& ==> &amp;)',null,32,5,17,5,87,17,3,'2013-02-01 13:47:10','2013-04-02 16:14:29','2013-02-01',90,null,null,904,1,2,0,'2013-04-02 16:14:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (905,2,6,'CP2000: script CP2000RedirectionIntegrationMail rediriger les mails n''ayant pu être intégrés dans la bonne agence','rediriger les mails n''ayant pu être intégrés dans la bonne agence','2013-02-12',28,5,11,4,81,11,4,'2013-02-04 10:17:53','2013-03-08 14:01:01','2013-02-04',100,null,null,905,1,2,0,'2013-03-08 14:01:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (906,2,2,'Rému - Export "CaVac"','Dans l''existant CaVac (source:trunk/tools/CaVac)

h3. Ajouter l''enjeu

DOSEHTMNTNG de CPXDOS

h3. Ajouter une colonne avec l''information A,B,C ou D

* Si forfait, attente informations de Zohra
* Si Régie
** Si type de mission +NON Construction+
*** Si Type de mission RC ou RCD, alors Catégorie C
*** Sinon Catégorie D
** Sinon ( = si mission de type construction)
*** Si DOJ, alors Catégorie B
*** Si DOA
**** Si Enjeu >= 129 000, catégorie B
**** Si Enjeu < 129 000
***** Si Indemnité inexistant ou si Indemnité < TM, catégorie A
***** Si Indemnité > TM, catégorie B

note : TM est présent dans CPXSEUIL. Prendre 1530 pour la requete SQL',null,26,2,4,4,null,4,4,'2013-02-04 16:49:05','2013-03-15 12:17:16',null,0,null,null,906,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (907,2,2,'Blocage valorisation NH si pas de rapport validé','Suite entretien avec M. BEEKER .
- Pour les Dossiers traités en Gestion classique (tous types de missions): bloquer la valorisation de la NH si le rapport n’est pas validé. 
',null,null,1,4,4,null,6,0,'2013-02-04 18:27:47','2013-02-04 18:27:47','2013-02-04',0,null,null,907,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (908,2,4,'Image service présentation service juridique','h2. Demande

Paul Boyer souhaite diffuser une nouvelle image dans le menu Hermes Construction.

h2. A faire

* optimiser le poids de l''image en vue d''une utilisation en vignette dans le menu Hermes
* préparer les fichiers / package pour livraison à destination de Pascal.

h2. Contrainte Mise en production

Privilégier une mise en production entre 12h30 et 13h30
',null,null,5,25,4,null,4,4,'2013-02-06 14:13:51','2013-03-04 10:48:06','2013-02-06',0,16,null,908,1,2,0,'2013-03-04 10:48:06');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (909,2,19,'Désactivation bouton Maisoning','"Maisoning" actif  (sinon inactif  Grisé) si et seulement si 
le dossier possède un document dont le titre est "Commande travaux MAISONING" ou "Commande travaux "
',null,null,5,null,4,78,4,1,'2013-02-07 14:13:49','2013-03-18 11:14:19','2013-02-07',0,null,null,909,1,2,0,'2013-03-18 11:14:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (910,2,19,'Accès direct - Restriction @IP','* 193.57.141.0/24
* 193.57.145.0/24
Si KO message, « Vous n’êtes pas autorisé à  visualiser ce dossier (IP = [Adresse IP]) ».
','2013-02-08',null,5,null,4,78,4,2,'2013-02-07 14:14:40','2013-03-18 11:14:29',null,0,null,null,910,1,2,0,'2013-03-18 11:14:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (911,2,19,'Changement import selon STI 1_24 (ou 25?)','','2013-02-08',null,5,null,4,78,4,1,'2013-02-07 14:15:57','2013-03-18 11:14:59',null,0,null,null,911,1,2,0,'2013-03-18 11:14:59');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (912,2,19,'changement import selon STI 1_27','','2013-02-28',null,5,4,4,79,4,4,'2013-02-07 14:16:39','2013-03-18 11:16:18','2013-01-01',100,null,null,912,1,2,0,'2013-02-22 11:12:30');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (913,2,6,'OM - Recuperation des missions et integration dans CP2000','En partant de l''existant faire evoluer le message pour gerer la récupération des OMs avec PJ dans CP2000
avec la version WS Darva 2012-10 norme cst2005_v3 ','2013-03-29',75,9,17,4,82,17,16,'2013-02-07 15:29:51','2013-05-13 11:26:08','2013-02-07',90,null,902,902,2,3,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (914,2,4,'QBE - Export table Chantier + Police','h2. Demande 

Olivier Rambaud 
tél. +33.1.44.20.34.56 
fax +33.1.44.20.34.92 
QBE 
Etoile St Honoré  21 rue Balzac
75406 Paris cedex 08

Moyen d''exporter vers csv/excel en 1 seule action les enregistrements des objets chantiers + polices

h2. Etat des lieux

Actuellement dans l''application, il est possible d''exporter tout objet unitairement avec la petite icone export en haut du tableau
Exemple :
!hermesC-exportchantier.png!

!hermesC-exportpolice.png!

h2. A faire

Etudier la possibilité de rassembler les données de la table chantier + police dans 1 seul export.
# Par le paramétrage (demander info à CitéSI)
# Par un développement correspondant
** Estimation du temps
',null,51,1,25,4,null,4,0,'2013-02-07 15:49:07','2013-02-07 15:49:07','2013-02-07',0,16,null,914,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (915,2,4,'Arret des notifications mails pour les dossiers ACS en provenance de Gsicass','h2. Demande

Ne plus envoyer de mail sur import de rapport pour le client ACS concernant les dossiers traités par Gsicass

h2. Origine

> De : Xavier COLLET [mailto:x.collet@acsservices.eu] 
> Envoyé : mardi 5 février 2013 09:44
> À : Eurisk Siege/SANTONI Dominique
> Cc : Eurisk Siege/CHARTIER Sylvie; Eurisk Siege/COLOMBEL Candice; CPCOM/BOUREZ Vincent; CPCOM/BONNET Guillaume; Nathalie BOULLU; Cédric RIGAULT; Anne-Marie PONTHIER; Pascale CORDESSE
> Objet : Demande d''arrêt des push mails de EURISK pour les rapports et notes d''honoraires relatifs aux missions confiées par le portail d''échanges.
> 
> 
> Bonjour Madame SANTONI,
> 
> Je vous confirme notre demande de ne plus recevoir de push mails de EURISK
> pour les rapports et notes d''honoraires relatifs aux missions confiées par
> le portail d''échanges.
> 
> Cordialement,
>  
> Xavier COLLET - ACS
> Responsable Technique & Expertises - Chargé de Missions
> +33(0)146966008  +33(0)622920889
> x.collet@acsservices.eu

h2. A faire

* Nouvel STI pour ajouter dans le flux du dossier le champs "origine"
* Adapter l''envoi des emails à l''import en conséquence

','2013-02-01',3,3,25,4,110,4,4,'2013-02-07 15:59:12','2013-04-25 14:10:49',null,100,null,null,915,1,2,0,'2013-04-25 14:10:49');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (916,2,8,'Ajouter le champs origine du dossier','h2. Demande

Hermes doit connaitre l''origine du dossier Darva/Gsicass/Etc...

h2. A faire

* Dans le flux xml du dossier, ajouter le champs DOSDRVORI
* Attendre Hermes + STI correspondant à ce changement. cf #915','2013-03-01',null,1,5,4,null,4,0,'2013-02-07 15:59:58','2013-02-07 15:59:58','2013-02-07',0,null,null,916,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (917,3,1,'Tester et installer la derniere version stable d''alfresco ','h2. demande

Tester et installer la dernière version stable d''Alfresco 
(Sur la vm de recette inutilisée actuellement par exemple)

',null,null,1,25,4,null,4,0,'2013-02-07 16:07:28','2013-02-07 16:07:28','2013-02-07',0,null,null,917,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (918,3,2,'Livraison 1.10.1','h2. Contenu

Anomalie #832: Suppression des doublons sur OPEG
Anomalie #855: Bloquer Possibilité de supprimer l''envoi d''un FAX
Anomalie #866: Date de mise a jour d''un dossier fausse
Evolution #424: Liste des documents d''une police
Evolution #758: Intermédiaire Allianz - paramétrage siège
Evolution #759: Intermédiaire Allianz - Transfert CP2000 Agence
Evolution #838: séparation PDF rend un document illisible avec les pages grisés
Evolution #847: Rapport - Calcul de l''année de garantie
Evolution #848: Affichage de l''assuré dans l''onglet dossier
Evolution #850: Gsicass - Date Droc obligatoire pour envoi de rapport
Evolution #868: FUSION DE DOCUMENTS A ELARGIR sur image + fichier texte
Evolution #872: Ajout d''un contrôle avant d''intégrer des Docs Externes
Evolution #897: Indemnité obligatoire (EB)

+ livré mais désactivé en attente de contenu Allianz

Evolution #760: Intermédiaire Allianz - Affectation en agence


h2. Update SQL

838-updatecpxlog.sql
847-insertcpxchp.sql
758-createcpxinter.sql
758-insertcpxpta.sql
760-altercpxdos.sql','2013-02-08',null,5,4,4,72,4,3,'2013-02-07 22:54:30','2013-02-10 09:34:53','2013-02-07',100,8,null,918,1,2,0,'2013-02-10 09:34:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (919,2,6,'Darva V3 Faire evoluer la librairie lib_ws_darva','Faire evoluer la librairie lib_ws_darva pour prendre en compte les appels WS V3','2013-06-28',75,2,17,4,82,17,9,'2013-02-08 14:42:59','2013-05-13 11:24:55','2013-02-08',80,null,902,902,4,5,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (920,1,6,'Darva : Traitement mission villecresne non prise en compte','Bug car les OMs de l''agence villecresne ne sont pas pris en compte',null,32,5,17,6,87,17,3,'2013-02-08 14:53:21','2013-04-02 16:14:29','2013-02-06',90,null,null,920,1,2,0,'2013-04-02 16:14:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (921,1,2,'Pas accès à fiche CRAC ','DOSSIERS :  0830/312436 indemnité 5000 euros - 0830/312426 Ind. 16360.30 euros - 0740/301660 Ind 3809 euros - 0800/509854 Ind 2 033 euros 
aucun accès à la fiche BAREME - Vérficaitons faites, présence : date DOC, réception, coût de la construction, présence Contrôleur technique.',null,21,5,4,6,108,6,5,'2013-02-08 15:51:28','2013-02-20 16:13:24',null,100,8,null,921,1,2,0,'2013-02-20 16:13:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (922,1,19,'Ecran recherche dossier modifié','h2. Problèmes

Probleme 1 :
* des champs ont été supprimés
* des champs ont été ajoutés

Probleme 2 :
* Des champs semblent inutiles sur la production
* Les champs utiles ne sont pas connus

h2. Etat des lieux - Ecran

!recherchedossier_prod.png!

!recherchedossier_recette_KO.png!','2013-03-28',null,5,4,4,79,4,4,'2013-02-08 17:04:14','2013-03-18 11:16:05','2013-02-08',100,null,null,922,1,2,0,'2013-02-22 11:12:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (923,2,5,'Onglet dommage','Revue de l''onglet dommage','2013-05-16',null,5,18,4,null,10,21,'2013-02-11 09:39:53','2013-05-21 10:54:23','2013-04-18',60,64,null,923,1,16,0,'2013-04-17 14:38:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (924,3,5,'Plannification Kastor','Gantt + Mise à jour planification équipe',null,null,1,10,4,null,10,0,'2013-02-11 09:42:54','2013-02-11 09:42:54','2013-02-11',0,null,null,924,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (925,2,2,'Remu 2013','h2. Objectif 

Chaque note d''honoraire doit pouvoir être catégorisée A/B/C/D

h2. A faire

Sujet en 3 étapes :

* Cas des forfaits - Modifier les attributs des NH/Forcod
* Valorisation de la NH - Choix de la catégorie
* Extraction CaVac avec prise en compte de la rétroactivité 

Attention : Prendre en compte un rattrapage des NH existantes','2013-05-31',77,8,null,6,null,4,87,'2013-02-11 11:40:20','2013-07-04 22:27:51','2013-02-12',81,384,null,925,1,84,0,'2013-03-10 00:32:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (926,2,2,'Remu - ajouter l''attribut catégorie sur un forfait','h2. Demande

Ajouter l''attribut "catégorie" sur chaque forfait, 

h2. A faire

* Table FORCOD, ajouter un champs alphanumérique de 2 caractères.
* Ajouter une liste (CPXLIS/CPXVAL) avec les valeures A/B/C/D
* Ajouter cette liste en paramétrage des forfaits (CPXG031)

h2. Valorisation par défaut

Préparer l''initialisation par mise à jour SQL du paramétrage des forfaits en fonction du fichier excel fourni par Zohra.
','2013-02-15',26,5,5,6,15,4,18,'2013-02-11 11:53:37','2013-03-10 00:32:24','2013-02-12',90,32,925,925,6,7,0,'2013-02-22 15:48:45');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (927,2,2,'Valorisation de la NH - Choix de la catégorie','h2. Objectif

Valoriser chaque ligne de forfait (cpxfef pour les forfaits) et de rubrique (cpxref) pour les régie avec la bonne lettre A/B/C/D

h2. A faire

Ajouter un champs, caractère de 2 dans cpxfef + cpxref

Sur valorisation de la NH, attributer la bonne lettre selon la règle suivante

Forfait

* Renvoi de la vacation nouvelle formule présente dans CPXFOR

Si forfait sans catégorie, alors comparer le montant de l''indemnité par rapport au ticket modérateur.
Si I < TM, alors A
Si I >= TM alors B

Régie

* Tout type de mission non construction
** Si RC ou RCD -> catégorie C
** Sinon catégorie D
* Sinon si type mission construction
** TRCA ou TRCJ -> Catégorie C
** MSEC --> Catégorie D
** DOJ ou PUCJ -> Catégorie B
** sinon (DOA, PUCA, DOSE)
*** Si E (enjeu) >129 000 euros -> catégorie B
*** Si E < 129 000 euros
**** Si I est inexistant ou si I< TM -> catégorie A
**** Si I > TM (voir CPXSEUIL) --> catégorie B
','2013-02-19',26,5,5,6,15,4,21,'2013-02-11 12:04:51','2013-03-10 00:32:24','2013-02-18',60,32,925,925,2,3,0,'2013-03-10 00:32:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (928,2,2,'Extraction CaVac sans prise en compte de la rétroactivité','h2. objectif

Faire un écran pour extraire un csv des informations nécessaire à la comptabilité.

h2. A faire

Se baser sur le SQL CAVAC. (source:/trunk/tools/CaVac)
* Recréer l''équivalent dans un écran CP2000 pour le siège 
** Appeler l''écran export CaVac et le rendre accessible dans le menu "interface comptable"
** Critère = fourchette de date à extraire
* Ajouter les lignes de vacations concernant la rétroactivité
* permettre l''extraction CSV

prendre en compte les avoirs (cavac2)

h2. Coefficient

|A|0.9|
|B|1|
|C|1.05|
|D|1|

A appliquer sur les montants exporté a partir du 1er février 2013.

h2. Rétroactivité

*Principe*

Dans le cas où une note d''honoraire n''est pas la première dans le dossier, il faut comparer la catégorie de la 2e avec celle de la 1ere.
Si les catégories sont différentes, il faut ressortir toutes les lignes de vacations (cavac précédent) + coller les catégories des 2 NH.

+Exemple+ 
Si régie nh1 = A,
si régie nh2 = B,
alors ajouter les lignes (cpxref) de la NH1 avec la catégorie "AB"

ou encore

Si forfait nh1 = A,
si forfait nh2 = B,
alors ajouter les lignes (cpxfef) de la NH1 avec la catégorie "AB"
','2013-03-06',26,5,5,6,15,4,13,'2013-02-11 12:17:14','2013-03-10 00:32:24','2013-02-20',60,48,925,925,4,5,0,'2013-03-10 00:32:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (929,1,2,'DT validé document word reste en l''état "brouillon"','Je valide un document par l''onglet "DOCUMENT" (sans cliquer dans REDACTION, sans passer par l''écran CPXG014). Quand je veux le consulter après validation (par Rédaction ouveture écran CPXG014) : un message me demande "voulez-vous mettre votre document à jour OUI NON" et dans le document word "brouillon" est toujours affiché. Voir en base de TEST le dossier 311961 
Ce bug nous a été remonté par l''agence de BASTIA. Mais j''ai déjà pu consulter sur HERMES des documents toujours en l''état "brouillon" alors qu''ils étaient validés dans CP2000.',null,5,5,5,5,15,6,10,'2013-02-11 17:02:06','2013-03-10 00:32:25',null,90,null,null,929,1,2,0,'2013-03-08 09:46:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (930,2,6,'Autexia-Transport des NH et gestion des doublons','h2. Demande

Ph. Morin - Autexia

Il y a actuellement un envoi automatique de NH par FTP à la compta.
Trouver un système pour éviter d''envoyer plusieurs fois la même NH à la comptabilité.

h2. Existant

Stéphane a réalisé un script powershell pour effectuer le travail.
voir doc stéphane giraudeau en PJ

h2. A faire

* Analyser ce qui a déjà été fait par S.Giraudeau et qui est actuellement en production
* Transformer l''existant en scénario Dataexchanger
* Trouver un systeme anti doublon

',null,72,5,11,4,84,4,6,'2013-02-11 17:37:32','2013-04-02 15:47:36',null,100,null,null,930,1,2,0,'2013-04-02 15:07:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (931,2,2,'Darva - Axa','h2. Info Darva

Dans le cadre de la gestion des dossiers sinistres AXA, seuls les messages suivants sont échangés :
# Ordre de Mission,
# Rapport et lettre d’accompagnement
** LR02 Préliminaire  
** LR07 Complexe 
** LR03 Définitif
** LR01 Unique
# Suivi De Dossier
** SD25 Annulation de la mission
** SD20 Relance de l’expert
** SD14 Refus de la mission
** SD16 Accusé de réception
** SD66 Investigation
** SD67 Prolongation
** SD68 Indices d’alerte
** SD17 Information postérieure au rapport
# Note d’honoraires

h2. Changement dans les process EURISK

OM DARVA AXA : réception Point Entrée Unique Mission DOA et DOJ 
*1 seul n° d’ Abonné EURISK puis transfert sur l’agence concernée.*

* SD16  AR Mission : envoi auto généré sur DARVA à l’enregistrement de la mission dans CP2000 (comme SMABTP) 
* #937 SD16 pour prise de RDV date + heure -> à créer : envoi auto généré à la validation de la convocation (nouveau message si nouvelle convocation)
* #938 SD16 pour changement de type de gestion  (selon application des procédures actuelles) à créer : 
## Si Gestion classique : envoi auto généré à la validation du DT103 et si changement de gestion de classique à déléguée : nouvel envoi généré dès que la case Gestion déléguée est cochée.
## Si gestion déléguée : aucun envoi mais si changement de gestion déléguée à gestion classique :  envoi auto généré dès que la case Gestion déléguée est décochée _mais je vérifierai avec S. Chartier si validation DT103 et si AXA maintient les règles actuelles??_
* #939 SD67 pour demande de prolongation de délai : j’avais noté lors de la réunion du 5/11/2012 l’envoi du SD à réception de l’accord signé (et non à la demande) _demander confirmation à AXA_
Donc à la réception de l’APD signé -> à créer envoi généré à l’indexation en 2.10 Prolongation de délai validée + récupération de la date saisie dans  "Fin de prolongation" (onglet DATE) _+ voir si on peut joindre le document avec nouvelle version DARVA_
* #940 SD66 pour motif d’investigation :
SD à faire manuellement dans Ecran cpxg123 – Envoi de document DARVA : bouton SD COMMENTAIRE -> créer un menu déroulant avec  Investigations + voir si on peut joindre un document avec nouvelle version DARVA (devis, facture)
* #941 SD68 pour Alerte :
Actuellement, validation du DT918 avec cases à cocher -> il faut créer un nouvel écran dans CP2000 (voir le DEVIS CAPE créé pour AXA CORPORATE) qui reprend les rubriques du DT918','2013-06-14',73,2,4,6,89,4,90,'2013-02-11 17:46:18','2013-07-04 22:27:52','2013-03-26',39,289,null,931,1,40,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (932,2,11,'MDM - gestion du parc des tablettes','Mobile Device Management

www.kaspersky.fr/business ','2013-06-28',null,1,null,4,null,4,2,'2013-02-11 17:49:51','2013-04-30 17:55:59','2013-06-03',0,null,null,932,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (933,1,2,'Complément règle métier pour Indemnité obligatoire  (EB)','Rajouter dans les règles "métier" que :
 - Si présence d''un RC dans le dossier même si présence d''un RU ou RE et/ou absence d''un rapport secrétaire, on ne rentre pas dans le test pour autoriser ou non la création/valorisation d''une NH.','2013-02-12',26,5,5,6,80,5,1,'2013-02-12 09:26:23','2013-02-12 11:01:33','2013-02-12',100,null,null,933,1,2,0,'2013-02-12 09:26:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (934,1,15,'URA lors de user non défini en base','Constaté sur SANDBOX lors de la connexion de Pascal
[2013-02-12 10:27:41] request.CRITICAL: Doctrine\\ORM\\ORMException: Entity of type IMAG\\LdapBundle\\Entity\\UserRoleAgence is missing an assigned ID. The identifier generation strategy for this entity requires the ID field to be populated before EntityManager#persist() is called. If you want automatically generated identifiers instead you need to adjust the metadata mapping accordingly. (uncaught exception) at /var/www/html/kastor/vendor/doctrine/lib/Doctrine/ORM/ORMException.php line 51 [] []

Il faut donc tester le cas d''un user non identifier en se supprimant de la BD
',null,null,8,10,4,null,10,4,'2013-02-12 11:15:31','2013-02-14 17:11:46','2013-02-12',100,null,null,934,1,2,0,'2013-02-14 15:54:59');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (935,3,10,'Engimo - Ouvrir contrat Darva','> De : Eurisk Siege/BOYER Paul 
> Envoyé : lundi 28 janvier 2013 10:59
> Objet : TR: lettre 1- experts SOLARIS LOT 3 - RC


Ci-dessous la demande d’AXA pour faire passer ENGIMO sous DRAVA et ce que j’ai répondu à NBR :

En résumé, il faut :
# Obtenir un N° d’abonné (abonnement payant)
# Attendre de voir leur cahier des charges (prendra des mois)
# Décider si vous faites ou non une passerelle à partir de Gestisoft (gros investissement certainement pas rentable) ou si vous optez pour une ressource de frappe complémentaire
# Evaluer si vous pouvez/voulez refacturer les surcout induits à AXA qui sera le seul bénéficiaire de ce système pendant un moment.
# Organiser une formation

Pour l’instant, on peut se limiter au point N°1 que je vous laisse le soin de traiter.
Pour la suite on verra en fonction des évolutions avec DARVA et ENGIMO.

Paul
','2013-02-15',null,5,4,4,null,4,1,'2013-02-12 13:46:24','2013-03-26 11:21:46','2013-02-15',100,8,null,935,1,2,0,'2013-03-26 11:21:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (936,2,2,'Statistique utilisation des cartes','h2. Objectif

Connaitre la fréquence d''utilisation des 3 boutons GGMap/Yahoo/Viamichelin
Utile pour Kastor.

h2. A faire

A chaque fois qu''1 des bouton GGMap/Yahoo/Viamichelin est cliqué dans un dossier,
descendre une ligne d''info dans un fichier log :
|Bouton|Date|Agence|Dossier|CP depart|Ville Depart|CP arrivée|Ville Arrivée|

Utiliser le dossier log présent dans l''arborescence CP2000.
Nommer le fichier par code agence, exemple : [Agecod]mapstats.log
',null,74,5,5,5,115,4,5,'2013-02-12 16:15:04','2013-03-15 13:21:12',null,100,8,null,936,1,2,0,'2013-03-15 11:14:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (937,2,2,'SD16 - envoi auto à la validation de la convocation','h2. A faire

Créer un déclencheur à la validation des convocations pour lancer le scénario SD16 (Dataexchanger)
1 signal pour 1 lot de convocation','2013-03-27',73,9,5,4,89,4,20,'2013-02-12 17:19:02','2013-04-30 15:28:07','2013-03-26',60,16,931,931,2,3,0,'2013-04-18 11:14:18');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (938,2,2,'SD16 - changement de type de gestion','SD16 pour changement de type de gestion (selon application des procédures actuelles) à créer :

# Si pas de gestion délégue et pas de DT103 = mode de gestion inconnu.
# Si on est en gestion classique : envoi auto généré à la validation du DT103
# Si on passe en gestion délégué : nouvel envoi généré dès que la case Gestion déléguée est cochée.
# Si on est gestion déléguée et que l''on passe en gestion classique : envoi auto généré dès que la case Gestion déléguée est décochée


Dominique doit vérifier avec S. Chartier si validation DT103 et si AXA maintient les règles actuelles??
','2013-03-29',73,9,5,4,89,4,39,'2013-02-12 17:43:26','2013-05-07 14:31:38','2013-03-28',60,16,931,931,4,5,0,'2013-04-26 14:27:39');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (939,2,2,'SD67 - demande de prolongation de délai','h2. Demande Axa

Si l’expert estime après rapport préliminaire avec garantie totale ou partielle devoir solliciter une prolongation de délai et qu’il a obtenu du bénéficiaire le report, il convient qu’il utilise le formulaire SD66 "prolongation" en saisissant la date d’échéance de la prolongation, date qui sera automatiquement intégrée dans le système AXA, en complétant la zone commentaire pour expliciter la raison du report.

h2. A faire

Nouvel écran à partir du CPXG123 : "Demande prolongation de délai"

Test :
* si rapport préliminaire déjà envoyé
* si au moins un dommage garanti

Afficher la date de fin de prolongation de délai (DOSDTFINPROD) + permettre la saisie d''un commentaire

Déclencheur pour Data Exchanger à passer en xml avec la structure suivante :

<sd67>
   <commentaire> ... </commentaire>
   <pjs>
      <pj> ... </pj>
      ...
      ...
   </pjs>
</sd67>','2013-04-09',73,9,5,4,89,4,42,'2013-02-12 17:45:45','2013-05-21 10:03:27','2013-04-08',60,16,931,931,6,7,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (940,2,2,'SD66 - motif d’investigation','SD à faire manuellement avec bouton "SD Investigation" dans Ecran cpxg123

SD Investigation sensiblement équivalent à SD commentaire + qq liste déroulante supplémentaire. (cf liste Darva 9489)

voir si on doit joindre un document avec nouvelle version DARVA (devis, facture)

Déclencheur pour Data Exchanger à passer en xml avec la structure suivante :

<sd66>
   <motif> VALRG.CPXVAL de l''item de liste correspnodant</motif>
   <commentaire> ... </commentaire>
   <pjs>
      <pj> ... </pj>
      ...
      ...
   </pjs>
</sd66>','2013-06-11',73,9,5,5,89,4,29,'2013-02-12 17:54:16','2013-05-17 09:10:26','2013-06-11',50,24,931,931,8,9,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (941,2,2,'SD68 - Alerte','h2. Actuellement

Validation du DT918 avec cases à cocher

h2. A faire

Créer un nouvel écran dans CP2000 à l''image du fonctionnement du devis CAPE
Reprendre les rubriques attendues par le SD68 de Darva pour Axa

Prevoir dans CPXVAL les listes DARVA :
* 9490 - valeur possible 3 (Valide) ou 4 (invalide), si ‘4’ cela signifie pour AXA qu’il y a eu une fausse déclaration à la souscription.
* 9491 - valeur possible 1 (oui), 2 (Non)
* 9492 - valeur possible 3 (Valide) ou 4 (invalide), si ‘4’ cela signifie pour AXA qu’il y a eu une fausse attestation d’assurance.
* 9493 - valeur possible 3 (Valide) ou 4 (invalide), si ‘4’ cela signifie pour AXA que les pièces produites ne sont pas authentiques
* 9494 - valeur possible 1 (Facture), 2 (Non facture) si ‘2’ cela signifie pour AXA que les travaux d’origine n’ont pas été facturés.
* 9495 - valeur possible 1 (Conforme), 2 (Exagéré) si ‘2’ cela signifie pour AXA qu’il y a eu une exagération manifeste des préjudices subis (matériel, immatériel).

Déclencheur pour Data Exchanger à passer en xml avec la structure suivante :

<sd68>
   <ds> VALRG.CPXVAL de l''item de liste sélectionné </ds>
   <da> VALRG.CPXVAL de l''item de liste sélectionné </da>
   <aa> VALRG.CPXVAL de l''item de liste sélectionné </aa>
   <pp> VALRG.CPXVAL de l''item de liste sélectionné </pp>
   <to> VALRG.CPXVAL de l''item de liste sélectionné </to>
   <ps> VALRG.CPXVAL de l''item de liste sélectionné </ps>
   <commentaire> ... </commentaire>
   <pjs>
      <pj> ... </pj>
      ...
      ...
   </pjs>
</sd68>','2013-04-16',73,2,5,4,89,4,15,'2013-02-12 17:57:04','2013-05-16 15:52:22','2013-04-12',50,24,931,931,10,11,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (942,2,2,'Taille des documents','h2. Objectif

Connaitre la taille des documents d''un dossier.

Utile pour :
* Définir le poids d''un dossier
* Volume Hermes
* Volume Kastor

h2. A faire

Ajouter un champs dans la table des documents CPXDOC
A chaque création/mise à jour des documents, enregistrer en table le poids en octet du document en question
(S''inspirer de la procédure existante "dosgros" mais pour 1 document)
',null,5,1,5,4,89,4,2,'2013-02-13 10:07:46','2013-03-25 17:07:57',null,0,16,null,942,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (943,1,6,'Darva V1 envoi rapport pb conversion double montantTTC','',null,32,5,17,6,87,17,3,'2013-02-13 10:26:32','2013-04-02 16:14:29','2013-02-13',100,null,null,943,1,2,0,'2013-04-02 16:14:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (944,1,6,'Darva V3 WSDL fourni par Darva ne se charge pas dans Dex','le WSDL de la V3 de Darva ne se charge pas dans Dex car les soapaction et les nom des input/output ne sont pas renseignés','2013-02-14',75,5,17,6,82,17,5,'2013-02-13 16:44:59','2013-04-08 14:33:14','2013-02-13',90,null,null,944,1,2,0,'2013-02-14 16:54:13');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (945,2,2,'Paramétrage envoi alerte mail dans la fiche Expert','h2. objectif

Permettre pour chaque expert de choisir entre 
* un état groupé journalier
* un envoi au détail -> 1 document éligible = 1 mail

h2. A faire

* Créer un champs dans CPXPRT, char de 1 pour différencier le cas G (groupé) du cas D (détail) 
* Rendre le paramétrage disponible dans l''écran de gestion des prestataires','2013-02-26',48,5,5,4,15,4,9,'2013-02-13 18:52:33','2013-03-10 00:32:25','2013-02-26',60,8,null,945,1,2,0,'2013-03-10 00:32:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (946,1,4,'Caractères spéciaux - sujet des mails Outlook','Supprimer dans le code le caractère "°" généré dans le lien "mailTo" lorsqu''un user utilise le client mail outlook :

classe MailHermes.java
Ligne 80',null,3,1,25,3,null,25,0,'2013-02-14 10:41:41','2013-02-14 10:41:41','2013-02-14',0,null,null,946,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (947,2,6,'SD17 - Information posterieure au rapport','developper le script SD17 Information Posterieure au rapport avec gestion des PJs
','2013-04-24',75,9,17,4,82,17,11,'2013-02-14 11:24:23','2013-05-02 17:56:22','2013-02-14',90,null,902,902,18,19,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (948,2,6,'LR07 - Rapport Complexe ','développer un nouveau message rapport :
LR07 Rapport complexe "preliminaire sans suite" "préliminaire avec suite" "définitif si précédé d un préliminaire avec suite" ','2013-06-04',75,2,17,4,82,17,10,'2013-02-14 11:37:17','2013-05-15 14:37:43','2013-02-14',30,null,902,902,20,21,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (949,2,6,'Darva V3 SMABTP Message LR06 Rapport de Carence','Pour la SMABTP développer le message LR06 Rapport de carence','2013-09-13',32,1,17,4,131,17,4,'2013-02-14 11:45:13','2013-05-13 10:57:31','2013-02-14',0,null,null,949,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (950,2,6,'SD66 -  Suivi de dossier Investigation','Pour AXA Developper le message SD66 Suivi de dossier Investigation','2013-05-03',75,9,17,4,82,17,10,'2013-02-14 11:49:00','2013-05-10 16:05:58','2013-02-14',90,null,902,902,22,23,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (951,2,6,'SD67 - Suivi de dossier Prolongation','Pour AXA Developper le message SD67 Suivi de dossier Prolongation','2013-03-29',75,9,17,4,82,17,8,'2013-02-14 11:49:54','2013-04-26 15:47:29','2013-02-14',100,null,902,902,24,25,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (952,2,6,'SD68 - Suivi de dossier Alerte','Pour AXA Developper le message SD68 Suivi de dossier Alerte','2013-04-16',75,9,17,4,82,17,10,'2013-02-14 11:50:57','2013-05-02 17:55:14','2013-03-22',90,null,902,902,26,27,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (953,2,6,'SD16 - Accuse de reception ajouter rendez vous','','2013-03-29',75,10,17,4,82,17,8,'2013-02-14 13:18:56','2013-04-26 15:44:32','2013-02-14',100,null,902,902,28,29,0,'2013-04-25 17:09:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (954,1,2,'Changement champ OMINTCOD','Dans les OM, pour les "Parties" Darva, le Code de la Partie est un Alphanuméric que 2.

Il faut donc changer dans la table CPXOMINT, le champs OMINTCOD en Char(2)',null,33,1,5,5,89,4,1,'2013-02-14 16:17:48','2013-05-02 15:44:16',null,0,4,null,954,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (955,2,6,'SD14 - Refus de la mission','A faire dans TraitementMissions ?','2013-02-26',75,9,17,4,82,17,16,'2013-02-14 16:48:27','2013-05-16 10:13:25','2013-02-14',100,null,902,902,30,31,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (956,2,6,'SD20 - Relance Expert','Attente informations....','2013-04-15',75,9,17,4,82,17,10,'2013-02-14 16:49:17','2013-04-26 15:31:03','2013-04-02',100,null,902,902,32,33,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (957,2,6,'Darva SMABTP Annulation de mission lorsqu un dossier a été créé','Pour le moment on met juste un warning, quelle est l''action a effectuer dans ce cas ? envoyer un mail etc... ?','2013-06-28',32,1,17,4,121,17,3,'2013-02-14 16:50:57','2013-05-16 13:10:22','2013-06-03',0,null,null,957,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (958,1,15,'Tableau de Bord et  recherche de dossier','Description du cas dans le tableau de bord
- je sélectionne l''expert TOMI VULIC
- j''ai un tableau de dossier de 3 pages en résultat
- je veux sélectionner la page 2 

-> la condition sur l''expert "TOMI VULIC" n''est pas passée

Description du cas dans la recherche de dossier
- je rentre des critères de recherches et je valide
- j''ai un tableau de dossier de 3 pages en résultat
- je veux sélectionner la page 2 

-> les condition de recherche ne sont pas envoyées
-> DANS CE CAS, il serait peut être intéressant d''intégrer le RECHERCHEBUNDLE
',null,null,9,8,4,null,10,1,'2013-02-15 10:18:43','2013-05-03 15:52:45','2013-02-15',0,null,null,958,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (959,3,19,'Mise à jour de la licence Simplicite®','- Vérifier la date d''expiration de la licence du socle (a priori prévue pour début mars 2013)

- Initier la procédure de MAJ avec CiteSi le cas échéant',null,null,5,25,5,null,25,2,'2013-02-15 15:13:19','2013-03-04 10:30:52','2013-02-15',100,null,null,959,1,2,0,'2013-03-04 10:30:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (960,1,4,'Paramétrage Client majeur AXA-PESSAC','Sur le login et mdp pour Axa France (AxaFr, u4f56w), il faudrait rajouter le client majeur AXA-PESSAC.',null,null,1,25,4,74,25,1,'2013-02-15 15:18:46','2013-02-15 15:19:08','2013-02-15',0,null,null,960,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (962,3,11,'Test application sur tablette','Note a propos du tests de l''application NOE sur tablette android',null,null,1,null,4,null,4,0,'2013-02-18 15:56:10','2013-02-18 15:56:10','2013-01-09',0,null,null,962,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (963,3,10,'Entretien Individuel 2013','','2013-01-18',null,5,null,4,null,4,0,'2013-02-18 16:35:26','2013-02-18 16:35:26','2013-01-14',100,12,null,963,1,2,1,'2013-02-18 16:35:26');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (964,3,4,'QBE - reinitialisation des données','h2. Demande

> Bonjour Vincent,
> Désolé de vous ennuyer mais on m’a fait remonter un vieux problème, le décalage de certains documents PDF et leur affection à de mauvais chantiers.
> Par exemple sur le chantier 239731/032, le document « Contrat 1 » n’appartient par à CB 16 mais à Estée Lauder.
> Chaque semaine, je n’envoie que les chantiers créés ou modifiés et je me demande si ce n’est pas un ancien décallage.
> Plutôt que d’essayer de corriger les erreurs, je me demande s’il ne serait pas plus simple de vider la base et de refaire import complet (il y a moins de 30 chantiers).
> Si cette solution est possible, je prépare un export complet depuis DbCons, je le dépose comme d’habitude, comme cela au moment où vous vider la base vous pourrez la recharger tout de suite et on verra si tout est OK.
> Qu’en pensez-vous ?
> Cordialement,
> Olivier Kauf

h2. info

[[Accès_QBEMarshchantier]]

h2. A faire

Purge SQL des chantiers et tables annexes
',null,51,5,25,4,109,4,1,'2013-02-19 09:29:07','2013-03-04 13:20:24','2013-02-19',0,8,null,964,1,2,0,'2013-03-04 13:20:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (965,1,4,'Impossible de supprimer un utilisateur','h2. Probleme

Impossible de supprimer un utilisateur par l''interface

!delete_user.png!',null,67,1,25,4,null,4,3,'2013-02-19 09:40:16','2013-05-17 15:44:53',null,0,8,null,965,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (966,1,6,'Darva V1 : Mauvais routage de mission','Les OMs sont routés actuellement vers l agence concerné en fonction du cope postal sinistre or pour la SMABTP elle doit être routée selon le numéro d''abonné',null,32,5,17,6,87,17,2,'2013-02-19 15:20:58','2013-04-02 16:14:30','2013-02-19',90,null,null,966,1,2,0,'2013-04-02 16:14:30');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (967,1,2,'CPXT095 - message en attente : affichage trop long','Dès que l''on arrive sur l''onglet Message en attente (dans les OM), nous avons le droit à un sablier systématique d''1 minute en production.
--> Voir pour ajouter les bons index ??? sur le nadcod si pas déjà existant.',null,5,5,5,4,15,4,4,'2013-02-19 18:06:19','2013-03-10 00:32:25',null,90,null,null,967,1,2,0,'2013-03-08 14:26:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (968,2,6,'Darva V1 insertion de commentaire sans avoir d id OM','Pouvoir inserer un commentaire meme si l''om n existe pas dans cpxoms',null,32,5,17,6,87,17,2,'2013-02-20 12:57:07','2013-04-02 16:08:45','2013-02-19',100,null,null,968,1,2,0,'2013-04-02 16:08:45');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (969,1,4,'Regroupement client majeur','Sur le login et mdp pour Axa France (AxaFr, u4f56w), il faudrait rajouter le client majeur AXA-PESSAC.',null,null,1,25,4,null,25,2,'2013-02-21 10:04:38','2013-02-21 16:38:30','2013-02-21',0,null,null,969,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (970,2,6,'Hermes Statistique envoyer mail trimestriel a Nelson','Envoyer un mail de fréquence trimestrielle avec pour contenu le fichier excel contenant le récap des connexions sur hermes Dommage',null,34,5,17,4,92,17,3,'2013-02-21 11:46:17','2013-04-02 16:24:51','2013-02-21',90,null,523,523,2,3,0,'2013-04-02 16:24:51');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (971,1,2,'Rajout règle de gestion pour OM annulé','Empêcher la création d''une mission à partir d''un OM annulé.

À partir du moment où un OM est a l''état Annulé, on ne doit plus déclencher un envoi sur Darva.

Aujourd''hui il faut donc permettre à l''assistante de créer un dossier dans l''optique de garder une trace de cette mission mais aucune suite (AR, commentaire, rapport, NH) ne sera donnée --> donc normalement on ne doit pas donner la possibilité d''envoyer un message vers Darva sur un OM annulé a l''initiative du client.
',null,4,5,5,4,15,5,8,'2013-02-25 16:14:23','2013-03-10 00:32:25','2013-02-25',60,null,null,971,1,2,0,'2013-03-10 00:32:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (972,3,21,'Etude préalable','h2. Périmètre 

Tout ce qui concerne nos interventions sur les sujets suivants liés à l''étude avant démarrage concret du projet :
* organisation
* planning
* budget

h2. Historique

26-02-2013 - Réunion expert présentation Knoe à Louveciennes
15-05-2013 - Réunion secrétaire + gestionnaire + expert à Louveciennes
',null,null,1,null,4,null,4,1,'2013-02-27 10:04:33','2013-05-21 10:24:29',null,0,null,null,972,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (973,1,2,'Problème de correspondance entre message d''erreur et n° de dommage','Dans le cadre du contrôle avant de pouvoir créer une NH, le message d''erreur en retour se base sur le DOMNUM alors que ce que les secrétaires voient à l''écran en tant que compteur le DOMRG.
Modifier le service CPXS021, l''onglet CPXT006 et l''écran CPXG018 pour rendre cohérent le message retour.',null,26,5,5,4,15,5,4,'2013-02-28 13:43:06','2013-03-10 00:32:26','2013-02-28',90,null,null,973,1,2,0,'2013-03-05 13:52:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (974,2,6,'Rapport LR01 LR02 LR03 (Rapport Unique, Preliminaire et Definitif)','faire evoluer le script gerant les rapports unique, préliminaire et définitif en V3 pour AXA','2013-06-03',75,9,17,4,82,17,21,'2013-02-28 15:48:20','2013-05-21 11:58:50','2013-02-28',90,null,902,902,6,7,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (975,2,6,'SD 16 - Accusé Réception','faire évoluer le script accusé reception pour la V3 AXA','2013-03-05',75,9,17,4,82,17,16,'2013-02-28 15:49:00','2013-05-02 17:49:12','2013-02-25',100,null,902,902,8,9,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (976,2,6,'Darva V1 : Annulation Mission a signaler dans CP2000','Lorsqu une mission est annulé par l assureur on doit faire apparaitre dans la liste des messages en attente un commentaire indiquant cette information','2013-05-31',32,9,17,6,154,17,6,'2013-03-04 10:21:52','2013-05-13 10:54:27','2013-05-02',100,null,null,976,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (977,3,4,'Push mails agences 0640 et 0641 / suppression des doublons','Des adresses de même rang susbistent sur les deux agences et provoquent des redirections de mail non désirées.

=> les supprimer.

en PJ :
- les captures liées à la demande
- les XML Simplicité liées aux données supprimées',null,3,5,25,4,null,25,1,'2013-03-04 12:18:25','2013-04-09 11:01:51','2013-03-04',100,1,null,977,1,2,0,'2013-03-04 12:18:25');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (978,2,4,'Mise en place Plateforme de recette interne CPCOM','Full install d''Hermes CT : 

- install CentOS sur VMPlayer
- install MySQL, dump de prod
- install Ant, Java, Jboss
- config des comptes (hermes/hermes), proxy AJP...

Accessible sur http://10.31.2.218/hermes/jsp/index.jsp',null,null,9,25,4,null,25,1,'2013-03-04 13:47:26','2013-03-13 14:21:05','2013-02-20',100,null,null,978,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (979,1,6,'GSICASS: Reprise des mission avec PJ volumineuse','Du fait d''un Bug concernant les briques ReadXML et WebService sur la version 8 de DeX les missions volumineuse génére un bug ',null,40,2,null,4,122,11,4,'2013-03-05 10:36:38','2013-05-13 11:48:35','2013-03-05',50,null,null,979,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (980,1,15,'Visualisation d''un mail lié au dossier','URL : http://dev.cpcom.com/app_dev.php/dossier/documents/document/mail/detail/127523
CF fichier joint
A coté de l’icône des pièces jointes, mettre leur nom
A coté de l’icône du contenu mettre "Corps du texte" ',null,null,9,18,4,null,10,1,'2013-03-05 10:58:16','2013-03-06 13:49:02','2013-03-05',0,null,null,980,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (981,1,15,'Visualisation des mails de la "boite mail"','Lorsqu''on veut visualiser une pièce jointe après enregistrement => OK
La visualisation directe ne fonctionne pas - effectivement, on a un enregistrement dans "temp" qui se termine par html
Exemple
file:///C:/Users/Sabrina/AppData/Local/Temp/05032013_COMMANDE_C65E25888O7771.pdf.htm

 ',null,null,9,18,4,null,10,1,'2013-03-05 11:01:40','2013-03-06 13:49:43','2013-03-05',0,null,null,981,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (982,2,2,'Préciser l''origine de l''annulation du dossier','h2. Objectif

Connaitre automatiquement l''origine de l''annulation d''un dossier --> expert ou client.
Cette demande est réalisée dans le cadre de l''élaboration de statistiques client sous la tutelle d''Emmanuel BEEKER et permettrait de répondre précisément aux attentes du client GROUPAMA.

h2. A faire

Sur annulation du dossier, vérifier que l''état du traitement est renseigné sur l''une des valeurs suivantes :

|_.Mission|_.Code liste|_.Valeur|_.Libellé|
|DOA ou PUCA|ETDOA|ETD13|Mission annulée par le client|
|||ETD14|Mission annulée par Eurisk|
|RCDA|ETRDA|ETD38|Mission annulée par le client|
|||ETD39|Mission annulée par Eurisk|
|||ETD40|Mission annulée/conflit d''intérêt|
|RCA ou PJA|ETRCA|ETD20|Mission annulée par le client|
|||ETD21|Mission annulée par Eurisk|
|||ETD30|Mission annulée par intermédiaire|
|Autres non judiciaire|ETAUT|n/a|n/a|
|Judiciaire|ETDJU|JUD10|Mission annulée par Eurisk|
|||JUD13|Mission annulée par Assureur|
|||JUD18|Mission annulée/conflit d''intérêt|

Si la valeur est différente de cette liste alors impossible de sélectionner l''état ANNULE sur le dossier.
*+Attention+* : avec l''etat actuel des valeurs de traitement, il est impossible d''annuler un dossier "AUTRE" (cf ''n/a'' du tableau)

h2. Questions

L''état de traitement n''étant pas disponible pour la catégorie de mission "Autre". Quel est le comportement souhaité ?
* Pas de contrôle et de blocage ?
* Modification du comportement de l''affichage de l''information ''traitement du dossier'' ? si oui lequel ?',null,16,1,null,4,null,4,5,'2013-03-05 11:12:47','2013-07-04 22:27:52',null,0,24,null,982,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (983,1,15,'Modification d''un contenu de doc externe','1 - je rentre un doc externe
2 - je clique sur l''icone modifier -> erreur

URL = http://dev.cpcom.com/app_dev.php/document/contenu/modifier/127526
MESSAGE = Item "document" for "" does not exist in CPCOMDocumentBundle:Contenu:modifierDocument.html.twig at line 20 ',null,null,9,18,4,null,10,1,'2013-03-05 11:21:31','2013-03-06 13:51:18','2013-03-05',0,null,null,983,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (984,3,2,'Martinique - Probleme envoi Epost (Maileva)','La Martinique nous informe que les envois maileva ne se font pas correctement : beaucoup de courriers sont sans date de remise en poste.
Le support a regarder dans l''interface Maileva --> les courriers ne sont pas présent chez Maileva.

h2. A faire

* Analyser la rupture entre le serveur Eurisk et le FTP Maileva
** Regarder la fréquence
** Comparer le comportement de la Martinique par rapport à celui de la Réunion

Trouver une solution pour rendre plus fiable le système !',null,7,1,4,5,null,4,5,'2013-03-05 16:52:25','2013-07-04 22:27:53',null,0,80,null,984,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (985,1,2,'Message en attente - référence dossier fausse','Mauvaise information affichée à l''écran pour un SD commentaire reçu concernant des OM dont on a pas la trace en base de donnée car reçu avant le démarrage EDI Darva

','2013-03-05',33,5,4,4,15,4,4,'2013-03-05 17:01:33','2013-03-10 00:32:26','2013-03-05',90,4,null,985,1,2,0,'2013-03-08 10:24:40');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (986,2,2,'Darva-Axa : Ajout descriptif dommage normalisé','h2. A faire

Ajouter un champ dans la table des dommages.
Pour chaque dommage, ajouter une liste déroulante avec la liste des descriptifs (CPXVAL) de la "nature du désordre" par défaut de AXA pour DARVA.
(source 20120504-Construweb-AXA nature du désordre.xlsx)

# Défaut étanchéité à l’eau (infiltrations, humidité…)
# Défaut étanchéité à l’air (perte de chaleur, salissures…)
# Décollement (carrelages, revêtement sol, ….)
# Décollement/ cloquage (peinture, étanchéité…)
# Condensation (moisissures…)
# Insuffisance chauffage/ climatisation
# Dysfonctionnement installation chauffage clim/ legionnelle
# Fissuration/ Tassement/ affaissement / instabilité/déformation
# Envol/ chute (tuiles, chassis, autres ...)
# Rupture  (canalisation, charpente….)
# Fuite canalisation
# Corrosion
# Dysfonctionnement électrique
# Non-conformité 
# Acoustique
# Autre
# Autre désordre mineur
# Non précisément décrit 
# Incendie

Champs obligatoire si Dossier Darva.
Bloquer uniquement l''accès au rapport si donnée absente.
','2013-04-18',73,9,5,4,89,4,19,'2013-03-05 18:45:32','2013-04-29 09:57:28','2013-04-17',60,16,931,931,12,13,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (987,2,2,'Darva-Axa : Ajout info investigation','Pour chaque dommage, ajouter un radiobouton :
investigation : oui/non

Champs obligatoire pour dossier Darva','2013-04-22',73,9,5,4,89,4,13,'2013-03-05 18:49:34','2013-04-30 10:42:40','2013-04-19',60,32,931,931,14,15,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (988,2,5,'Fiche CRAC/SYCODES','Décrire et développer les cas de traitement CRAC/Sycodes','2013-05-16',null,5,18,4,null,10,22,'2013-03-06 13:57:01','2013-05-21 10:54:23','2013-04-18',60,64,923,923,2,15,0,'2013-04-17 15:04:59');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (989,1,15,'Connexion impossible sous Kastor','J''ai les droits sur toutes les agences en administrateurs
Et après la connexion, je récupère l''erreur suivante :
SQLSTATE[42000]: Syntax error or access violation: 1064 You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''ORDER BY usrNom, usrPrenom ASC'' at line 1 

URL = http://dev.cpcom.com/app_dev.php/',null,null,9,8,4,null,10,1,'2013-03-07 10:50:51','2013-05-03 15:52:26','2013-03-07',0,null,null,989,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (990,2,6,'LR - Rapport - Ajouter les fiches SYCODES dans les messages des rapports','Ajouter les fiches syscodes dans les messages des rapports','2013-05-09',75,9,17,4,82,17,10,'2013-03-07 13:47:29','2013-05-02 17:46:24','2013-05-02',100,null,902,902,10,13,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (991,2,2,'Rétroactivité sur les notes d''honoraires','Ajouter la notion de rétroactivité sur l''export CaVac.

h2. Principe

Dans le cas où une note d''honoraire n''est pas la première dans le dossier, il faut comparer la catégorie de la 2e avec celle de la 1ere.
Si les catégories sont différentes, il faut ressortir toutes les lignes de vacations (cavac précédent) + coller les catégories des 2 NH.

Exemple
Si régie nh1 = A,
si régie nh2 = B,
alors ajouter les lignes (cpxref) de la NH1 avec la catégorie "AB"

ou encore

Si forfait nh1 = A,
si forfait nh2 = B,
alors ajouter les lignes (cpxfef) de la NH1 avec la catégorie "AB"','2013-03-21',26,5,5,4,118,4,4,'2013-03-07 15:38:49','2013-03-22 16:18:08','2013-03-18',100,32,925,925,8,9,0,'2013-03-22 16:18:08');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (992,1,19,'Erreur à l''import','Des erreurs à l''import sont présents dans les logs sur la production.

h2. A faire 

* demander les logs d''un import en production à Pascal
* vérifier les logs et le comportement d''un import en recette.
* Analyse les logs jboss pour identifier le problème.

',null,78,5,25,3,116,4,5,'2013-03-07 15:42:41','2013-04-29 09:59:51',null,100,null,null,992,1,2,0,'2013-04-11 11:08:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (993,1,6,'Darva EnvoiLettreRapport caractere &','des caracteres ''&'' faisaient planter l''envoi de la requête on les change maintenant par des ''&amp;''',null,32,5,17,6,87,17,2,'2013-03-07 15:58:20','2013-04-02 16:14:30','2013-03-07',90,null,null,993,1,2,0,'2013-04-02 16:14:30');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (994,2,2,'SD 99/17 - ajout des pieces jointes','Dans Darva V3 il est possible d''ajouter des pièces jointes dans un commentaire.

h2. A faire

* Permettre la sélection de PJ 
** pour le SD 99 - commentaire (SMABTP)
** pour le SD 17 - information postérieur au rapport (AXA)
** pour le SD 66 - 67 - 68 aussi
* Nicolas : Changer le fonctionnement du fichier déclencheur
** 1ere ligne = lien vers le fichier texte de commentaire
** ligne suivante (facultative) : lien vers les pieces jointes préparée','2013-06-04',73,2,5,6,89,4,5,'2013-03-08 11:43:38','2013-05-16 12:21:10','2013-06-03',10,16,931,931,16,17,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (995,3,2,'Livraison 1.10.2','Livraison 1.10.2

Concerne les sujets suivants :

Anomalie #929: DT validé document word reste en l''état "brouillon"
Anomalie #971: Rajout règle de gestion pour OM annulé
Anomalie #973: Problème de correspondance entre message d''erreur et n° de dommage
Anomalie #985: Message en attente - référence dossier fausse
Evolution #925: Remu 2013
Evolution #926: Remu - ajouter l''attribut catégorie sur un forfait
Evolution #927: Valorisation de la NH - Choix de la catégorie
Evolution #928: Extraction CaVac sans prise en compte de la rétroactivité
Evolution #945: Paramétrage envoi alerte mail dans la fiche Expert


h2. Modification structure SQL

#926 : CPXFOR
#927 : CPXREF, CPXFEF
#945 : CPXPRT
','2013-03-09',null,5,4,4,15,4,2,'2013-03-08 13:58:23','2013-03-10 00:32:56','2013-03-08',0,8,null,995,1,2,0,'2013-03-10 00:32:56');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (996,1,2,'Rattrapage remontée Avoir et annulation d''un OM manuellement','2 anomalies à corriger:

Prendre en compte FEFCAT et REFCAT dans la remontée des Avoirs (CPXS038).

Corriger l''annulation d''un OM manuellement, date d''utilisation à renseigner pour ne plus apparaître dans le listing des OM disponibles (CPXT094).','2013-03-11',4,5,5,4,113,5,3,'2013-03-11 16:28:20','2013-04-10 10:18:04','2013-03-11',100,1,1073,925,39,40,0,'2013-03-11 16:28:20');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (997,2,2,'Passage à Acrobat 7','h2. Tester Acrobat Reader 7

Installer sur le serveur de test agence
Tester l''ouverture du fichier PDF en PJ (actuellement, avec Acrobat reader 6, il ne s''ouvre pas)',null,5,9,4,4,null,4,2,'2013-03-11 16:54:01','2013-03-15 14:56:22',null,0,8,null,997,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (998,2,2,'ENVOI RAPPORT DARVA / SMABTP','Lorsque l''envoi d''un rapport est en échec : laisser la possibilité de générer un nouvel envoi de rapport ','2013-03-19',33,5,4,4,118,6,2,'2013-03-11 17:31:50','2013-03-22 16:18:23','2013-03-11',100,8,null,998,1,2,0,'2013-03-22 16:18:23');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (999,1,6,'Darva V1 : Bug nettoyage ligne dans CSV','Il y a un bug lors du nettoyage du csv genere par l''ETL en effet il y a un Instr <> 0 ce qui peut indiquer que la chaine peut se trouver n''importe ou et pas en position 1',null,32,5,17,6,87,17,2,'2013-03-11 17:34:22','2013-04-02 16:14:30','2013-03-11',90,null,null,999,1,2,0,'2013-04-02 16:14:30');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1000,3,4,'Mise à jour Tests fonctionnels SELENIUM','h3.  récupérer sources des TUs (svn)

h3.  mettre à jour l''environnement :

* installation des JAR compatibles avec Firefox 19.0.2
* mise en place d''un profil Firefox (bypass certificat SSL absent / https)
* mise à jour des paramètres java
* mise à jour de la doc

h3. mettre à jour les TUs',null,null,2,25,3,null,25,2,'2013-03-11 17:35:50','2013-03-21 16:50:32','2013-03-11',30,null,null,1000,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1001,1,2,'Bug sur l''extraction csv du CaVac','> Dominique Gouveia : 
> j''ai mis en jaune les données qui me paraissent suspectes

Erreur lors de l''extraction excel du CaVac.

Colonnes concernées :
* Code Agence Expert
* Nom Agence Expert','2013-03-12',77,5,5,6,113,5,6,'2013-03-12 10:53:30','2013-04-10 22:20:15','2013-03-12',100,2,1073,925,41,42,0,'2013-03-12 11:02:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1002,1,2,'Extraction CaVac','Vu avec Zohra :

* l''info type construction est toujours à F --> c''est faux.
* il faut ajouter 2 colonnes avec les anciens montants (sans correction du coefficient) à la fin du fichier
* la catégorie est systématiquement vide ... pourquoi ???',null,77,5,5,4,113,4,5,'2013-03-12 14:21:36','2013-04-10 22:20:15','2013-03-12',100,8,1073,925,43,44,0,'2013-03-13 00:03:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1003,2,5,'Administration de serveur ','Optimisation MySql: Serveur
Installation librairie Linux
',null,null,1,10,4,null,10,5,'2013-03-12 14:46:52','2013-04-29 11:34:24','2013-03-12',0,null,null,1003,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1004,1,19,'Generali - Acces impossible au dossier 70602390 12/359 ss','h2. A faire 

# demander à CitéSI comment est construite l''url
# comprendre pourquoi le dossier 70602390 12/359 ss  n''est pas accessible à Generali

h2. Origine de la demande

> De : ExpertiseDab [mailto:ExpertiseDab@generali.fr] 
> Envoyé : mardi 12 mars 2013 15:13
> À : Eurexo Direction/Service Client
> Cc : SANCHEZ Sandrine
> Objet : TR: Dossier 70602390 12/359 ss
> 
> Bonjour, 
> Le lien URL depuis ULIS ne fonctionne pas, ne permettant pas d’accéder directement à HERMES ; 
> En rentrant mes codes d’accès sur Hermès, la référence du dossier Generali ne me permet d’accéder au dossier. 
> Bref, je n’ai aucune visibilité sur ce dossier, pour lequel vous avez accusé réception de la mission le 23.08.2012.
> Merci de bien vouloir indiquer à l’agent qui nous lit en copie, l’état d’avancement de vos opérations d’expertise ; 
> Cordialement, 
> Frédéric MARAT 
> direction indemnisation 
> expert - pilotage des prestataires DAB 
> Tél : 01 58 38 70 45 - E-mail : expertisedab@generali.fr - Adresse postale : 7 bvd Haussmann 75456 Paris Cedex 09 
',null,null,5,25,4,null,4,4,'2013-03-12 16:24:07','2013-03-18 11:20:09',null,100,null,null,1004,1,2,0,'2013-03-13 11:51:16');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1005,3,2,'Cecanord - ajout agence 1590','Il faut ajouter une nouvelle agence CP2000.
C''est pour l''agence CECANORD EUREXO.

Le code agence est 1590

* Serveur 10.59.1.1 / bdd port 1319
* Prévoir le paramétrage siège
** Adresse agence ?
** Paramétrage Expert/Secrétaire D. Gouveia ?
* Procédure Hermès
* Flydoc (prévoir mail notifesker_1590@eurisk.fr)
** Paramétrage chez Esker
** Installation sur le serveur Eurisk
* Mail -> Alias cecanord@eurisk.fr sur lille@eurexo.fr
* Pas de compte Fax Rte


* PC Scan ? cout ?
* Licences TSE ?
','2013-03-29',15,5,4,4,null,4,1,'2013-03-13 10:35:49','2013-03-26 10:44:27','2013-03-14',100,32,null,1005,1,2,0,'2013-03-26 10:44:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1006,1,2,'PJ manquante','> Dossier base 65  - 0650/660644
> Les 3 derniers mails ont des pièces jointes qui n''y sont pas 

idem

> 0640/205714/RCA


même probleme ailleurs :

!PieceJointeManquante.png!
le mail est intégré, les pièces jointes ne suivent pas alors que la nature "pieces jointe" apparait mais le libellé est vide ?',null,6,2,4,4,null,4,1,'2013-03-13 14:39:02','2013-03-13 16:41:17',null,0,null,null,1006,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1007,2,2,'Indemnité obligatoire pour DOA et PUCA seulement','Suite à evol #897 :

L''indemnité obligatoire ne concerne finalement que la DOA et PUCA.
Remplacer le(s) test(s) sur le type de la construction par un test sur le type de mission

',null,9,5,5,6,113,4,4,'2013-03-13 16:07:17','2013-03-13 16:37:43',null,100,null,null,1007,1,2,0,'2013-03-13 16:32:05');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1008,1,2,'Plus de génération d''AR Mission Darva','L''AR Mission DARVA n''est plus généré.','2013-03-13',33,5,5,6,113,5,7,'2013-03-13 16:13:48','2013-03-14 12:30:43','2013-03-13',100,4,null,1008,1,2,0,'2013-03-13 16:15:39');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1009,1,2,'Correction du calcul de la catégorie pour l''extraction REMU','',null,77,5,5,6,114,5,5,'2013-03-14 09:30:25','2013-04-10 22:20:15','2013-03-14',90,null,1073,925,45,46,0,'2013-03-14 09:30:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1010,2,2,'Nouvelles règles pour CaVac','Dans le CPXG045 :
 - Prendre en compte les lignes "DEP" en plus des lignes "010" dans les Compositions Forfait (CPXCFO_01)
 - Pour les lignes CA, pas de coeff à appliquer
 - Pour les lignes VAC, coeff à appliquer
 - Regarder le soucis des lignes RégieVac à 0','2013-03-14',77,5,5,6,114,5,9,'2013-03-14 10:56:54','2013-04-10 22:20:16','2013-03-14',90,4,1073,925,47,48,0,'2013-03-14 11:55:38');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1011,2,6,'Referentiel pour trouver l agence en fonction du code postal','Pour AXA on aura que 1 seul utilisateur WS et le routage vers la bonne agence se fera avec une table dans le référentiel on prendra en fonction du code postal sinistre le code agence','2013-03-15',75,10,17,4,82,17,8,'2013-03-14 17:50:52','2013-04-26 15:59:31','2013-03-14',100,null,902,902,14,15,0,'2013-04-26 15:59:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1012,2,5,'Agence d''affectation','Ajouter l''agence d''affectation à l''utilisateur
-> formulaire
-> connexion
-> migration',null,null,5,8,4,null,10,1,'2013-03-15 15:22:35','2013-04-17 14:35:44','2013-03-15',0,null,29,29,2,3,0,'2013-04-17 14:35:44');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1013,2,6,'Referentiel pour referencer les agences pilotes','faire une table pour mettre les agences pilotes car avec AXA on va recevoir les messages pour toutes les agences','2013-05-31',75,9,17,4,82,17,5,'2013-03-15 16:01:57','2013-05-13 11:08:17','2013-03-15',90,null,902,902,16,17,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1014,2,5,'Test avec les droits restreints','- Tester l accès a chaque interface :
-> sans les droits sur l''objet
-> avec des droits en lecture seule
-> avec les droits en lecture + modification
-> avec les droits en lecture + modification + Suppression
-> avec les droits en lecture + modification + Suppression + Création
-> sans les droits sur les objects liés
-> avec les droits en lecture sur les objets liés
-> avec les droits en lecture et modification sur les objets liés

Commencer par l''interface ADMIN
Lors de chaque "commit" préciser la vue testée

En profiter pour faire un check W3C
',null,null,5,8,4,null,10,40,'2013-03-18 10:32:01','2013-04-17 15:04:10','2013-04-17',0,80,29,29,4,7,0,'2013-04-17 15:04:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1015,2,19,'ARDI - Hermes -  Ajout ventilation NH par expert ( %)','
h2. Structure de la table "nho_expert"
 
* <nho_exp_dos_id.dos_numeurexo>
* <nho_exp_dos_id.origine>
* <nho_exp_dos_id.dos_cle_emetteur>
* <nho_exp_nho_cle_emetteur>
* <nho_exp_exp_id.exp_code>
* <nho_exp_exp_id.origine>
* <nho_exp_nho_montantht>

h2. A faire

* Modification de la spécification
* Voir avec Card pour livraison du TransfertHermes.exe (export d''Ardi)
* Adaptation de l''import / de la base de données

h2. Reférence

DEM-244 du 01/03/13 de Perruisset.P','2013-04-10',null,1,25,4,117,4,1,'2013-03-18 11:26:39','2013-04-02 11:54:16','2013-04-08',0,null,null,1015,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1016,2,19,'Ajout doh_date_traitement + doh_date_relance ','h2. Ajout Structure SQL

Dans la table document hermes (doh), 2 champs
* doh_date_traitement
* doh_date_relance


h2. A faire

* Modification de la spécification
* Voir avec Card pour livraison du TransfertHermes.exe (export d''Ardi)
* Adaptation de l''import / de la base de données

h2. Référence

DEM-239 du 07/12/12 de Perruisset.P
note : 06/02/2013 : ajout dans cahier des charges Cité SI','2013-04-10',null,1,25,4,117,4,1,'2013-03-18 11:30:24','2013-04-02 11:54:17','2013-04-08',0,null,null,1016,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1017,1,2,'Demande d''enregistrement sur document validé en lecture seule','h2. Problème

Après avoir validé un document dans l''écran CPXG014. J''imprime le document. 
Je reviens dans CP2000 (sans fermer le document word) par un clic dans la barre des taches.
Je fais "enregistrer et quitter" 
Word me demande si je veux "enregistrer sous" le document word.

h2. A faire

Regarder pourquoi le document word est considéré comme "modifié"
Corriger cette anomalie, CP2000 ne doit pas provoquer un enregistrement sur un document en lecture seule.

',null,5,1,5,4,null,4,1,'2013-03-18 15:05:02','2013-04-10 10:07:50',null,0,null,null,1017,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1018,1,2,'Catégorisation des Avoirs oubliée et corriger le calcul du coef pour les Régie Vac dans l''extraction','','2013-03-19',26,5,5,6,119,5,5,'2013-03-19 16:07:22','2013-03-25 14:42:51','2013-03-19',90,null,null,1018,1,2,0,'2013-03-19 16:10:44');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1019,1,4,'Purge des logs d''import','Tester sur la recette l''impact de la suppression des logs :
purge applicative (fonction dans l''appli) et/ou purge de la table ad hoc (m_log)

(les logs occupent 500Mo dans .../XMLSupervisor/',null,null,7,25,3,null,25,0,'2013-03-20 11:02:59','2013-03-20 11:02:59','2013-03-20',20,null,null,1019,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1020,1,6,'Darva : Envoi de NH avant envoi du Rapport','comme pour GSICASS l''envoi de NH peut partir avant que l''envoi de rapport soit terminé, pour palier à ça
on met un wait de 70 secondes en debut d''execution du script de la NH',null,32,5,17,6,87,17,2,'2013-03-20 16:10:51','2013-04-02 16:14:30','2013-03-20',90,null,null,1020,1,2,0,'2013-04-02 16:14:30');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1021,2,19,'Ajout Vérification risque & mail assuré','Ajouter 2 champs dans le dossier :

* <dos_verif_ris>  
** 0 :  non coché dans ARDI
** 1 :  coché dans ARDI
* <dos_assure_email>



DEM-247 - Perruisset.P','2013-04-10',null,1,25,4,117,4,2,'2013-03-20 17:57:50','2013-04-02 11:54:17','2013-04-08',0,null,null,1021,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1022,2,19,'Entité rdv - Ajout Expert','Dans l''entité <Rendez-vous>, ajouter le champ :
* <nho_exp_exp_id.exp_code>

DEM-250	- Dos Santos.N','2013-04-10',null,1,25,4,117,4,1,'2013-03-20 17:59:22','2013-04-02 11:54:17','2013-04-08',0,null,null,1022,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1023,2,5,'Mettre un verrou sur la modification des dossiers','L''objectif est de mettre en place un verrou sur la consultation des dossiers en modification
BUT => un utilisateur à la fois a des acces en modification sur le dossier

Créer un table lier au dossier avec l''utilisateur connecté et l''heure de début de validité de sa session (un session sera défini arbitrairement valide 5 minutes = *delaideconsultation * - parameter.yml)

Exemple
dossier_dossierid user_userid *datedebutvalidite*
1                 1           20-03-2013 14:00

Ainsi à chaque fois qu''un utilisateur veut accéder à une page d''un dossier en modification :
si le dossier n''est pas en cours de lecture : on crée un enregistrement dans la table des verrous
si le dossier est en cours de modification par lui même, on met à jour la table en ajoutant à la datedebutvalidite le delaideconsultation 
si le dossier est en cours de modification par un autre, et si datedebutvalidite + delaideconsultation  >= daterheurecourante : l''utilisateur ne peut accéder au dossier
si le dossier est en cours de modification par un autre, et si datedebutvalidite + delaideconsultation  < daterheurecourante : cela signifie que l''utilisateur actif n''est pas connecté à un dossier de manière active, on met à jour la table verrou avec user = usercourant et datedebutvalidite = date courante


Aussi a chaque fois qu''un utilisateur accède à une page qui ne concerne pas la modification d''un dossier : on vérifie que l''utilisateur courant n''a pas d''entrée dans la table verrou - si il a des eentrée on les supprime

De plus, a chaque fois qu''un utilisateur décide de modifier un dossier, on s''assure par une pres requete qu''il est bien propriétaire de ce dossier ds la table des verrous

Enfin pour assurer une fluidité dans la mise à jour de cette table, il faudra créer une reqete Ajax qui mettra à jour la table des verrous toutes les ( delaideconsultation - 1 minutes ) cette requete permettra la mise a jour de la table par  datedebutvalidite = date courante

SUPPORT
Le support devra voir les dossiers verrouiller et pourra faire "sauter" ce verrou par la suppression d''une ligne dans la base

Contrainte : 
- un user n''a un verrou que sur un et un seul dossier
- un dossier est verrouillé que par un seul user

Il faudra voir comment optimiser les développement (listener) afin de ne pas copier-coller nos codes de verrou/dé-verrou a chaque action
','2013-04-24',null,9,8,4,null,10,11,'2013-03-22 16:15:20','2013-04-30 10:47:02','2013-04-23',100,16,null,1023,1,4,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1024,1,6,'GSICASS: Prb du n°SIRET pour angers','Les NH émis par angers sont refusées par GSICASS: Le n° SIRET n''est pas enregistré par GSICASS','2013-05-13',40,9,11,5,122,11,3,'2013-03-25 10:58:22','2013-05-13 11:46:10','2013-03-25',100,null,null,1024,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1025,2,5,'Société','Actuellement le logo dépend de la société de la table document (copiée lors de la création du doc depuis la table agence)
-> la société sort de la table document
-> pour générer le logo , on fait document->getDossier->getAgenceCourante->getSociété',null,null,5,18,4,null,10,4,'2013-03-26 11:30:54','2013-04-17 14:32:25','2013-03-26',0,4,892,391,15,16,0,'2013-03-29 16:06:38');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1026,1,2,'Transfert CPXVAL - délai anormalement long','Le transfert de la table CPXVAL est anormalement long (10 minutes).

!transfertref.png!

voir précédente anomalie : #185',null,42,1,4,4,null,4,0,'2013-03-26 14:47:59','2013-03-26 14:47:59',null,0,null,null,1026,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1027,1,6,'Darva: NH avec une valeur taux TVA 20.6','Une NH est émise sur Darva avec une TVA de 20.6 à la place de 19.6 

Demande de Karine (Toulon)

Re, 

-	Suite 1er message -> même problème rencontré agence CRETEIL : 0940/475209 et 0940/475211 : TVA CP2000 et DARVA différentes ….(20,6 % au lieu de 19,6 %)

-	Sur les dossiers 0940/475105 et 0940/475124 : il y a 1 centime d’écart entre montant CP2000 et DARVA. 


Bon courage 
Kn 


-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Bonjour Guillaume, 

Sur dossier 0340/681694, le montant de la NH CP2000 est différent du montant HN DARVA : TVA à 20.6 % au lieu de 19.6 % ?????? 
Merci de regarder ; j’ai vérif. les autres dossiers en amont et après => no problème…
(je vérif. à Créteil.)

Merci 
Good day 
Kn 

',null,32,5,11,4,130,11,3,'2013-03-26 15:37:35','2013-04-08 17:36:09','2013-03-26',100,null,null,1027,1,2,0,'2013-04-08 17:36:09');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1028,2,19,'Entite  Document - Ajout Code secrétaire','* Ajout de l''attribut dans l''entité <Document>
** <doh_secretaire_id.sec_cod>
** <doh_secretaire_id.sec_nom>



DEM-251	- 25/03/13 - Perruisset.P
		
','2013-04-10',null,1,25,4,117,4,1,'2013-03-27 14:38:37','2013-04-02 11:54:17','2013-04-08',0,null,null,1028,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1029,1,2,'Impression ne fonctionne pas systématiquement','L’agence de Montpellier rencontre un problème d’impression depuis l’utilisation des imprimantes RICOH.

Le problème se produit lors d’impression manuel de document de type CONVOCATION EN CONVENTION.

Depuis CP2000, elles impriment par ex trois convocations avec trois destinataires, dans des impressions différentes afin de conserver l’historique des envois (fonction « Nouvelle Impression »). 
Sur trois documents, seulement deux vont s’imprimer, et aucune erreur ne remonte. 

Le spooler Windows ne reçoit que deux demandes d’impressions
Un des documents passe donc à la trappe…

Ce problème se produit systématiquement pour l’ensemble des utilisateurs de Montpellier, mais uniquement sur des imprimantes RICOH (depuis une session admin ou pas). 
Les impressions sur les BROTHER fonctionnent.

',null,27,5,4,4,120,4,2,'2013-03-28 14:30:50','2013-03-28 14:44:57',null,100,null,null,1029,1,2,0,'2013-03-28 14:44:57');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1030,2,6,'Fermeture automatique des dossiers','h2. Objectif

pour les dossiers répondant à certains critères, il faut :

# Affecter le statut "Mission terminée" automatiquement
# Affecter le statut "Mission annulée" automatiquement

h2. Règles

!fermeturedossierautomatique.png!',null,28,1,null,4,52,4,3,'2013-03-28 15:13:33','2013-04-26 10:29:41',null,0,null,null,1030,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1031,2,4,'TELE-EXPERTISE dans l''objet du mail pour un envoi de rapport chez Allianz','Ajouter le mot clé "TELE-EXPERTISE" dans l''objet du mail pour les alertes sur envoi de rapport pour le client ALLIANZ.

Critère :
* Attente info Sylvie Chartier',null,3,1,null,4,null,4,0,'2013-03-28 16:10:41','2013-03-28 16:10:41','2013-03-28',0,null,null,1031,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1032,2,5,'Intégrer l''API FAXBOX','',null,null,5,8,4,null,10,3,'2013-03-29 09:48:23','2013-05-21 17:35:19','2013-03-29',0,40,null,1032,1,2,0,'2013-04-17 15:03:43');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1033,1,6,'Darva V1 Erreur requete sql pour la cabinet','la requête SQL pour récupérer les informations sur le cabinet n etait pas bonne elle doit être : 
select * FROM cpxadr WHERE  cpxadr.agecod=''0004'' AND  cpxadr.adrnum=1',null,32,5,17,6,87,17,2,'2013-03-29 10:21:25','2013-04-02 16:14:31','2013-03-29',90,null,null,1033,1,2,0,'2013-04-02 16:14:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1034,1,6,'Darva: NH Darva 3 cents de difference','NH Darva n°dossier : 
Agence Creteil/Villecrennes
475105  
475124

',null,32,5,null,4,130,11,3,'2013-03-29 14:10:29','2013-04-08 17:35:38',null,100,null,null,1034,1,2,0,'2013-04-08 17:35:38');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1035,2,6,'HermesStatsConnexion suppression du fichier LogDuJour sur hermes apres transfert','',null,34,5,17,6,92,17,2,'2013-03-29 16:33:14','2013-04-02 16:24:51','2013-03-29',90,null,null,1035,1,2,0,'2013-04-02 16:24:51');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1036,2,6,'Note d''honoraires','','2013-06-14',75,2,17,4,82,17,8,'2013-04-02 10:09:33','2013-05-14 17:49:33','2013-04-15',70,null,902,902,34,35,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1037,1,2,'erreur #1 : Avoir sans catégorie','h2. demande

> Vous trouverez ci-joint un extrait de l’extraction Vacations sur les NH de mars 2013.
> Il y a de nombreuses NH sans catégorie. Pouvez-vous regarder pourquoi ? et apporter si possible une réponse


h2. SQL

<pre><code class="sql">
select agecod, dosnum, nhonum, nhodtemi, nhecode, cpxfef.forcod, fefcat, forcat, forlib
from cpxnho, cpxfef, cpxfor
where nhodtemi > ''2013-03-01 00:00:00'' and nhodtemi < ''2013-04-01 00:00:00''
and cpxfef.nhonumage = cpxnho.nhonumage and fefcat is null
and cpxfef.forcod = cpxfor.forcod;
</code></pre>','2013-04-04',77,5,4,4,127,4,9,'2013-04-02 10:55:46','2013-04-04 17:42:32','2013-04-02',100,null,1050,925,11,12,0,'2013-04-04 15:15:31');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1038,2,6,'Stats Hermes Mettre fichiers temporaires dans repertoires dedies sur hermes','mettre les fichiers SQL dans prod
et les fichiers logs dans log',null,34,5,17,4,92,17,3,'2013-04-02 11:00:14','2013-04-02 16:24:51','2013-04-02',90,null,null,1038,1,2,0,'2013-04-02 16:24:51');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1039,3,19,'installation environnement développement Hermes DOMMAGE','install de la VM...','2013-04-03',null,1,25,4,null,4,1,'2013-04-02 11:51:38','2013-04-04 09:49:32','2013-04-02',0,16,null,1039,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1040,2,19,'STI 1.28','évolution de la norme d''échange STI 1.27 pour ajouter les dernières demandes

','2013-05-13',null,9,25,4,117,4,6,'2013-04-02 11:52:45','2013-05-21 10:37:50','2013-05-13',100,24,null,1040,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1041,1,2,'Tenir compte de la date du jour pour selectionner le TM','la recherche des seuils doit tenir compte de la date du jour pour récupérer le montant attendu.

voir #927 :
<pre><code>
         ;========================
         ;Evol #927 - NG - Cat NH
         ;========================
         if (FORCAT.CPXFOR.CPD != "")
            FEFCAT.CPXFEF.CPX = FORCAT.CPXFOR.CPD
         else
            ;Si I < TM, alors A
            ;Si I >= TM alors B
            clear/e "CPXSEUILS"
               TYPE.CPXSEUILS/init = "TM"
            retrieve/e "CPXSEUILS"
            if ($status < 0)
               FEFCAT.CPXFEF.CPX = ""
            else
               clear/e "CPXDOS_01"
                  DOSNUM.CPXDOS_01/init = DOSNUM
               retrieve/e "CPXDOS_01"
               if (DOSETTCMNTNG.CPXDOS_01 < MONTANT.CPXSEUILS)
                  FEFCAT.CPXFEF.CPX = "A"
               elseif (DOSETTCMNTNG.CPXDOS_01 >= MONTANT.CPXSEUILS)
                  FEFCAT.CPXFEF.CPX = "B"
               endif
            endif
         endif
</code></pre>',null,26,5,5,4,128,4,4,'2013-04-02 13:44:22','2013-05-02 15:37:45','2013-04-08',100,8,1050,925,35,36,0,'2013-05-02 15:37:07');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1042,2,2,'Transmettre en agence la suppression du règlement d''une NH','h2. Demande

Info Irina :
> Il y a un décalage entre l''etat "Réglé" de la NH en agence par rapport au siège.
> La compta peut enlever le règlement sur une NH sans que le changement soit transmis en agence

h2. A faire

* se renseigner sur le mode opératoire de la compta pour la suppression de l''affectation d''un règlement
* lors de cette action, transferer l''état en agence.
',null,37,1,null,4,null,4,0,'2013-04-02 13:55:11','2013-04-02 13:55:11',null,0,36,null,1042,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1043,2,4,'Recette fonctionnelle / tests de non régression V5.1.1','Vérifier que les évolutions de la V5.1.0 n''ont pas été impactées par les derniers dev (conflits entre modules XML Simplicité?)',null,null,1,25,4,74,25,0,'2013-04-02 14:47:04','2013-04-02 14:47:04','2013-04-02',0,null,null,1043,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1044,2,6,'Transfert Hermes Nettoyage des fichiers logs','Les fichiers log de plus de 10 jours générés par Print2PDF et CopyBatchCommander doivent etre effacés','2013-05-22',34,9,17,4,91,17,4,'2013-04-02 15:43:46','2013-05-17 15:13:03','2013-04-02',90,null,581,581,2,3,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1045,2,6,'Certification AXA','','2013-06-17',75,2,17,4,82,4,7,'2013-04-02 15:58:57','2013-05-23 13:47:32','2013-05-27',0,null,902,902,36,37,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1046,2,4,'bloc actualité','h2. Demande

Paul Boyer - le 03/04/2013
> On voudrait pouvoir disposer d’un pavé "Actualité" sur Hermès comme sur la maquette ci-dessous :

!exemple_pave_actu.png!

h2. Principe

cette zone de texte est accessible via un user admin et peut être modifiée à volonté pour faire passer facilement des petites infos aux gestionnaires.
Prévoir une date de début et de fin de validité.
Prévoir de pouvoir cumuler plusieurs infos. 
',null,76,1,null,4,null,4,0,'2013-04-03 13:57:24','2013-04-03 13:57:24',null,0,null,null,1046,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1047,1,6,'CP2000RedirectionIntegrationEmail: Prb de transfert de fichier si  les agences src et destination sont les mêmes','h2. Probleme

Les fichiers du serveur de chartres (10.28.1.1) ne sont pas transféré comme attendu sur le 45 alors que le code agence est correctement renseigné dans les scripts xml.

Source : Etienne Faure

h2. Analyse

Prb de transfert de fichier si les agences src et destination sont les mêmes',null,28,5,11,5,126,11,3,'2013-04-03 17:39:13','2013-04-26 10:27:36','2013-04-03',100,null,null,1047,1,2,0,'2013-04-26 10:27:36');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1048,1,6,'Selectionner les dossiers non annulés','Pour Darva Dans les messages d''envoi de rapport et de NH faire attention lorsque l''on récupère le dosnum et sur toute opération sur CPXDOS bien prendre le dossier qui a un etat différent de ANNU

CPXDOS.ETDCOD != ''ANN''',null,32,5,17,6,121,17,2,'2013-04-04 10:47:08','2013-04-26 14:51:06','2013-04-04',100,null,null,1048,1,2,0,'2013-04-26 14:51:06');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1049,1,6,'Darva insertion commentaire erreur quand il n y a pas de dossier','Lorsque l on reçoit un commentaire de l assureur et que l''on a pas encore créé de dossier sur l OM en question la requete SQL d insertion du doc provoque une erreur car le dosnum est vide',null,32,10,17,6,121,17,2,'2013-04-04 11:39:01','2013-04-26 14:47:39','2013-04-04',100,null,null,1049,1,2,0,'2013-04-26 14:47:39');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1050,3,2,'Remu - Paye Mars 2013','Projet Rému

Synthèse erreurs relevés sur l''export de Mars 2013','2013-04-11',77,5,4,6,null,4,41,'2013-04-04 15:32:30','2013-05-16 14:57:50','2013-04-02',100,60,925,925,10,37,0,'2013-05-16 14:57:51');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1051,1,2,'erreur #7 - la rétroactivité doit tenir compte de la présence d''un avoir','h2. Problème

Dans le dossier 0640/205849 nous avons lignes dans l’extraction
* 3 correspondant aux NH ou avoir (8999 9752 9795)
* 1 est redondante avec une fausse catégorie AB

note :
L’indemnité n’est pas rentré (la catégorie est cependant bien renseignée).
L’avoir est bien calculé.

h2. Analyse 

nous avons donc
|NH1 | A | Annulée |
|NH2 | B ||
|Retroactivité | AB | faux car en fait 1 seule NH|
|Avoir | A ||

h2. A faire

Pour ajouter la ligne de rétroactivité, il faut compter le nombre de NH sauf celles qui sont annulées (NHECODE = ''A'')

','2013-04-04',77,5,4,4,127,4,3,'2013-04-04 16:13:58','2013-04-09 12:18:40','2013-04-04',100,null,1050,925,13,14,0,'2013-04-04 16:43:51');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1052,1,2,'erreur #9 - base de la rétroactivité = NH1','h2. Problème

Dans le dossier suivant 9720/360414 nous avons deux NH l’un pour le RP l’autre pour le RE.
Ils se trouvent que c’est sur le même mois, à quelques jours d’intervalle.
La 1ère NH est correcte, il a été mis en catégorie A car on ne connait pas l’indemnité.
La 2ème NH est également correcte car nous sommes bien en catégorie B (I>TM).
Il y a bien une ligne pour venir corriger la 1ère NH (8850) pour reprendre avec le coefficient correcteur 0.1 le fait que nous sommes maintenant en catégorie B.
*Il se trouve que l’application du coefficient correcteur (qui est bon) se fait sur la 2ème NH (9720) et non la 1ère*.
C’est évidemment pas ce que l’on souhaite faire.

A corriger d’urgence.

h2. A faire

Modifier la base de calcul de la rétrocession :
Se baser sur les montants de la 1ere NH.','2013-04-11',77,5,5,6,133,4,14,'2013-04-04 16:18:49','2013-04-11 10:52:29','2013-04-04',100,40,1050,925,15,16,0,'2013-04-11 10:52:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1053,1,2,'erreur #6 - pas de rétroactivité sur des NH au forfait','h2. probleme

Dans le dossier 0380/489465, nous avons une NH de gestion déléguée (2013/8513) avec un code forfait 230 pour Gan/Groupama, 
La catégorie ressort à AB avec 0,1. 
Je ne comprends pas pourquoi 

h2. A faire

Faire de rétroactivité si concernant des NH en Régie',null,77,5,5,6,128,4,6,'2013-04-04 17:38:55','2013-04-09 12:19:46',null,100,null,1050,925,17,18,0,'2013-04-09 12:19:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1054,3,4,'vérifier les PJ des chantiers','h2. Problème

Dans le chantier 590569/001, les documents en PJ accessibles sont les logs d''import au lieu des vrais PDF attendu !
exemple :

!chantier_doc_log.png!

h2. Questions

# Est-ce localisé seulement à ce chantier ?
# As-t-on une idée du pourquoi ?
# Comment corrige-t-on ?',null,51,3,25,4,null,4,2,'2013-04-05 10:25:09','2013-04-16 14:19:46',null,90,null,null,1054,1,2,0,'2013-04-16 14:19:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1055,2,6,'Autexia: Le script doit vérifier que le document respect le format d*.pdf (d  = chiffre) ','Le script doit vérifier que le document respect le format d*.pdf (d  = chiffre) sinon mail',null,72,5,11,4,129,11,2,'2013-04-05 10:33:02','2013-04-15 10:38:02','2013-04-05',100,null,null,1055,1,2,0,'2013-04-15 10:38:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1056,2,2,'erreur #3 : ajouter la position du dossier dans l''extraction','h2. Demande

Ajouter 1 colonne dans l''extraction sur la position du dossier :
- garanti/garanti partiel/non garanti',null,77,5,5,6,128,4,6,'2013-04-05 15:29:09','2013-04-09 12:19:46',null,100,null,1050,925,19,20,0,'2013-04-09 12:19:46');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1057,1,2,'erreur #5 - regievac en A au lieu de B','h2. problème

Certains lignes Regievac sont marquée A alors que l''on devrait être B.
cf excel en PJ.

h2. Piste

Comme le calcul A/B tiens compte du seuil du TM, 
Voir si c''est lié à l''anomalie #1041

',null,77,5,5,6,128,4,8,'2013-04-05 15:57:40','2013-04-09 12:19:47',null,100,null,1050,925,21,22,0,'2013-04-09 12:19:47');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1058,3,2,'Transfert CP2000 Amberieu vers Annecy','h2. Demande

Déplacer l''application CP2000 Amberieu vers le serveur Annecy

h2. A faire

* déplacer la bdd solid
* déplacer le répertoire Agence
* paramétrage Flydoc
* paramétrage Dataexchanger
* paramétrage HermesProc
* mettre à jour le fichier BasesEurisk.doc

Trouver une date pour le faire...
',null,15,5,4,4,null,4,3,'2013-04-05 16:20:05','2013-04-21 00:59:24','2013-04-05',100,16,null,1058,1,2,0,'2013-04-21 00:59:24');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1059,1,2,'erreur #11 - ligne CB ou CA = impossible','h2. Problème

La ligne CB est fonctionnellement impossible
Le problème de cette erreur semble venir du fait qu’il y a un autre dossier  dans une autre agence qui a le même numéro classé en C

|NH|Agce|Dossier|Cat|
|8184|641|1059|B|
|8184|641|1059|CB|
|11730|680|1059|C|

problème avec la catégorie CA

h2. A faire

Trouver pourquoi le filtre sur l''agence ne fonctionne pas dans le selectdb !!!',null,77,5,5,6,128,4,5,'2013-04-05 16:21:53','2013-04-09 12:19:47',null,100,null,1050,925,23,24,0,'2013-04-09 12:19:47');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1060,3,2,'Mise à Plat BDD Siege','h2. Problème

La base de donnée du si_ge (10.78.1.8 1313) ne répond plus correctement sur la table des forfaits

h2. A faire

Reinitialiser la base de donnée SOLID du siège production
cf : [[Mise_à_plat_du_siège]]
',null,15,2,4,6,null,4,1,'2013-04-08 15:15:36','2013-04-08 17:50:09','2013-04-08',10,24,null,1060,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1061,2,2,'Darva - Libellé de l''opération obligatoire','h2. Problème

L''absence d''un libellé dans l''opération empeche l''envoi du rapport Darva SMABTP

h2. A faire

Ajouter un contrôle sur la présence d''une valeur dans OPELIB.','2013-06-17',29,1,5,5,89,4,1,'2013-04-08 15:41:30','2013-05-23 10:55:39','2013-06-17',0,4,null,1061,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1062,3,6,'OM Darva suppression d OM dans base et re insertion dans une autre base','supprimer l''OM 001SDO11010090 de la base 0790
et l''inserer dans la base 0781

supprimer l''OM 002SDO13002672 de la base 9720
et l''inserer dans l''agence guadeloupe 9710 car le code postal sinistre est 97190',null,32,5,17,6,null,17,9,'2013-04-08 16:46:25','2013-04-26 14:49:44','2013-04-08',100,null,null,1062,1,2,0,'2013-04-09 09:19:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1063,2,6,'Darva V1 routage d OM en fonction du code postal pour l expert Averlant','Pour l expert Averlant code abonné E97000022047 router les OM en fonction du code postal du sinistre',null,32,5,17,6,null,17,2,'2013-04-09 09:21:45','2013-04-26 14:50:42','2013-04-09',100,null,null,1063,1,2,0,'2013-04-26 14:50:42');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1064,2,5,'Intégration de Bundle Symfony','',null,null,5,18,4,null,10,2,'2013-04-09 10:51:15','2013-04-17 14:30:54','2013-04-09',0,null,null,1064,1,2,0,'2013-04-17 14:30:54');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1065,3,6,'Darva V1 supprimer les 28 OMs AXA insérés dans la base 0780','supprimer les 20 OMs AXA reçus le 08/04 et les 8 OMs AXA reçus le 09/04 sur la base 0780','2013-04-16',32,5,17,6,121,17,5,'2013-04-09 11:31:26','2013-04-26 15:57:15','2013-04-09',100,null,null,1065,1,2,0,'2013-04-09 11:42:39');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1066,2,2,'P. Lagrange : Ajouter les espaces dans les données','h2. Problème

Impossible d''importer les fichiers générés dans la compta car il manque les espaces.
Exemple le champs nom d''un expert doit faire 32 caractères

h3. Exemple avant
 
attachment:avant.txt

h3. Exemple après

<pre>
ForfaitCa;11/03/2013;2013;9009;ALSACE;0680;167;0680;ALSACE;1583;MICHEL CARRE;567;0;0;GROU067-01;GROUPAMA GRAND EST;6800000000242;RCDA;SAL;F;;2126;C;;;;540;0;;
</pre>

h2. A faire

Remettre exactement le format d''export de l''ancienne méthode d''extraction',null,77,5,5,4,132,4,5,'2013-04-09 12:29:40','2013-04-10 21:46:13',null,90,8,1050,925,25,26,0,'2013-04-10 20:58:15');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1067,2,2,'SD14 - refus de mission','h2. demande

A partir de l''écran "télécommande" Darva. Permettre à la secrétaire de déclencher au besoin, le message SD 14, refus de mission

h2. SD14

Prévoir un pavé de commentaire en libre saisie de la part de l''utilisateur, voir #955

<sd14>
   <mr> VALRG.CPXVAL de l''item de liste sélectionné </mr>
   <commentaire> ... </commentaire>
</sd14>','2013-04-26',73,9,5,4,89,4,13,'2013-04-09 13:27:33','2013-05-15 10:42:49','2013-04-25',60,16,931,931,18,19,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1068,3,6,'Darva V1 re jouer l insertion des missions pour les expert DHUIEGE,ZAVAGNO et VACHERAND','après affectation du code compagnie sur ces 3 experts il faut re jouer les missions reçues qui leur sont destinées',null,32,5,17,6,null,17,4,'2013-04-09 15:42:28','2013-04-26 14:46:43','2013-04-09',100,null,null,1068,1,2,0,'2013-04-11 17:24:16');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1069,1,2,'DG - NH manquante','Les NH suivantes sont manquantes à l''appel dans le fichier du mois de mars 

|NHONUM|Date|Agence|Dossier||Type|
|2013-8896|11/03|0690|339230|NH|Forfait|
|2013-8997|11/03|0490|673185|NH|Régie|
|2013-9890|17/03|0490|673185|AV|Régie|

Trouver pourquoi',null,77,5,5,4,133,4,5,'2013-04-09 17:40:39','2013-04-11 17:36:12',null,100,null,1050,925,27,28,0,'2013-04-11 17:36:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1070,1,2,'DG - facture 2013-11197 : catégorie manquante','Sur le dossier 902065 de l''agence 781, il y a une ligne de vacation sans catégorie

c''est un RCJ --> Il faut un C',null,77,5,4,4,128,4,1,'2013-04-09 17:44:10','2013-04-09 17:52:27',null,100,null,1050,925,29,30,0,'2013-04-09 17:52:28');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1071,2,2,'ZR - ajouter 4 colonnes d''info rétrocession expert','!cpxg104.png!

Pourrais-tu ajouter 4 colonnes en fin de tableau sur le nouveau fichier paie, qui correspondent à l’ imprim.écran ci-dessus,  à savoir :

* Colonne 1 : Rétrocession Régie – Base de calcul : mettre vacation expert ou C.A
* Colonne 2 : Rétrocession Régie – Taux
* Colonne 3 : Rétrocession Forfait – Base de calcul : mettre vacation expert ou C.A
* Colonne 4 : Rétrocession Forfait – Taux.',null,77,5,5,6,132,4,4,'2013-04-09 17:47:31','2013-04-10 21:46:13',null,90,4,1050,925,31,32,0,'2013-04-10 20:58:15');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1072,2,19,'Mise à jour de l''algorithme d''import des dossiers','Suite à nos recherches sur l''erreur à l''import, cf #992

*Algo actuel* : 

1) Recherche du dossier par le champ [cle_emetteur]
2) Si [cle_emetteur] trouvé : 
3) Vérification de la clé fonctionnelle Hermes des dossier = couple (dos_numeurexo, origine)
4) Si le couple (dos_numeurexo, origine) correspond à celui dans le XML importé, update validé. Sinon, update KO.

+Note+ : l''implémentation de l''algo ne va pas plus loin que la recherche sur la cle_emetteur. C''est le socle technique Simplicité qui vérifie la clé fonctionnelle (dos_numeurexo, origine)


*Algo souhaité :*

!algo_import_eurexo.jpg!

',null,78,2,25,5,151,25,7,'2013-04-09 17:49:46','2013-07-04 22:27:53',null,0,16,null,1072,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1073,3,2,'Rému - synthese correctif sur paye février','Synthese pour lien vers fiches correctives concernant l''extraction février 2013','2013-03-14',77,5,4,6,null,4,5,'2013-04-10 10:16:58','2013-04-10 10:20:03','2013-03-11',100,15,925,925,38,49,0,'2013-04-10 10:16:58');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1074,1,2,'Catégorie manquante sur les avoirs régies','Il manque des catégories sur les lignes concernant des avoirs sur des NH Régies.
',null,77,5,4,4,null,4,3,'2013-04-10 10:31:06','2013-05-13 14:16:50',null,0,null,1050,925,33,34,0,'2013-05-13 14:16:50');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1075,2,6,'Darva ajouter numéro abonne dans table darva_om sur referentiel','pour les accusés de reception, envoi de commentaire/ envoi de rapport/ envoi de NH on ne sait pas retrouve le bon user/mdp pour les agences 0350,0940',null,32,5,17,6,121,17,4,'2013-04-11 09:43:16','2013-04-26 14:47:02','2013-04-11',100,null,null,1075,1,2,0,'2013-04-26 14:47:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1076,1,6,'DataExchanger:Migration de DeX sur serveur installé en salle machine','Tester le nouveau serveur installé','2013-06-07',31,2,11,4,134,11,4,'2013-04-11 09:56:51','2013-05-13 11:36:34','2013-04-11',50,null,null,1076,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1077,1,6,'DataExchanger: Mise à jour du Référentiel SIRET ','Suite à divers prb liés au référentiel SIRET, un nouveau référentiel a été communiqué (voir doc ci-jointe incorporant les mesures correctives)
',null,31,5,11,4,134,11,2,'2013-04-11 10:02:19','2013-04-26 10:54:34','2013-04-11',100,null,null,1077,1,2,0,'2013-04-26 10:54:34');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1078,2,2,'CPXG124 - Ajouter un accès vers les PJ de l''OM','Un OM Axa est fourni avec des Pièces jointes.
Il faut les rendre disponible lors de la consultation de l''ordre de mission.','2013-06-06',73,1,5,6,89,21,2,'2013-04-11 12:22:23','2013-05-16 12:19:59','2013-06-05',0,16,931,931,20,21,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1079,1,2,'CPXG123 - afficher l''expert signataire','Lors de l''envoi d''un message DARVA, il faut les attribuer à l''expert signataire
(Aujourd''hui c''est l''expert rédacteur et ce n''est pas bon)

!cpxg123-pb-expert-signataire.png!',null,33,5,5,4,89,21,2,'2013-04-11 12:29:34','2013-04-29 10:05:59',null,100,8,931,931,22,23,0,'2013-04-29 10:05:59');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1080,3,6,'Darva rejouer les scripts accuse de reception suite erreur numero abonne','rejouer les accusés de reception emis de la base 0350,0940 et 0350 depuis le 10/04 12h00 au 11/04 12h00',null,32,5,17,6,null,17,2,'2013-04-11 13:15:33','2013-04-26 14:49:21','2013-04-11',100,null,null,1080,1,2,0,'2013-04-11 17:24:01');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1081,2,6,'Darva NH faire evoluer le script NH car segment SE0840301 9 itérations autorisées max ','si ce segment qui représente le détail de l''opération est présent plus de 9 fois alors envoyer email a la comptabilité (karine trappes, dominique santoni et l''agence concernée pour indiquer qu''il faut refaire la NH avec 9 opérations maximum

email de type :
Bonjour
Il y a plus de 9 lignes dans la note d''honoraire du dossier xxxx/xxxxxx ce qui est non conforme à la norme Darva SMABTP l''envoi vers Darva est donc impossible. Merci de refaire la note d''honoraire avec moins de 9 lignes et de la renvoyer.

Cordialement,



exemple d ''opérations :

<SE0840301 xmlns="http://www.darva.com/messages/construction/me200701">
					<DE02320501>00100</DE02320501>
					<DT92100101 code="1"/>
					<DE02330101>Temps passé en réunion          </DE02330101>
					<DE02340101>0000084.20</DE02340101>
					<DE02350101>019.60</DE02350101>
					<DE02370101>2013-03-26</DE02370101>
				</SE0840301>
				<SE0840301 xmlns="http://www.darva.com/messages/construction/me200701">
					<DE02320501>00075</DE02320501>
					<DT92100101 code="1"/>
					<DE02330101>Etude de l''affaire              </DE02330101>
					<DE02340101>0000084.20</DE02340101>
					<DE02350101>019.60</DE02350101>
					<DE02370101>2013-03-26</DE02370101>
				</SE0840301>
				<SE0840301 xmlns="http://www.darva.com/messages/construction/me200701">
					<DE02320501>00075</DE02320501>
					<DT92100101 code="1"/>
					<DE02330101>Temps de déplacement au dossier </DE02330101>
					<DE02340101>0000084.20</DE02340101>
					<DE02350101>019.60</DE02350101>
					<DE02370101>2013-03-26</DE02370101>
				</SE0840301>
				<SE0840301 xmlns="http://www.darva.com/messages/construction/me200701">
					<DE02320501>01500</DE02320501>
					<DT92100101 code="2"/>
					<DE02330101>Kilomètres affectés au dossier  </DE02330101>
					<DE02340101>0000000.61</DE02340101>
					<DE02350101>019.60</DE02350101>
					<DE02370101>2013-03-26</DE02370101>
				</SE0840301>
				<SE0840301 xmlns="http://www.darva.com/messages/construction/me200701">
					<DE02320501>00100</DE02320501>
					<DT92100101 code="A"/>
					<DE02330101>Forfait ouverture               </DE02330101>
					<DE02340101>0000136.40</DE02340101>
					<DE02350101>019.60</DE02350101>
					<DE02370101>2013-03-26</DE02370101>
				</SE0840301>
				<SE0840301 xmlns="http://www.darva.com/messages/construction/me200701">
					<DE02320501>00000</DE02320501>
					<DT92100101 code="A"/>
					<DE02330101>Forfait horaire (au-delà de 5 H)</DE02330101>
					<DE02340101>0000013.90</DE02340101>
					<DE02350101>019.60</DE02350101>
					<DE02370101>2013-03-26</DE02370101>
				</SE0840301>
				<SE0840301 xmlns="http://www.darva.com/messages/construction/me200701">
					<DE02320501>00200</DE02320501>
					<DT92100101 code="1"/>
					<DE02330101>Temps passé en réunion          </DE02330101>
					<DE02340101>0000084.20</DE02340101>
					<DE02350101>019.60</DE02350101>
					<DE02370101>2013-04-09</DE02370101>
				</SE0840301>
				<SE0840301 xmlns="http://www.darva.com/messages/construction/me200701">
					<DE02320501>00075</DE02320501>
					<DT92100101 code="1"/>
					<DE02330101>Etude de l''affaire              </DE02330101>
					<DE02340101>0000084.20</DE02340101>
					<DE02350101>019.60</DE02350101>
					<DE02370101>2013-04-09</DE02370101>
				</SE0840301>
				<SE0840301 xmlns="http://www.darva.com/messages/construction/me200701">
					<DE02320501>00050</DE02320501>
					<DT92100101 code="1"/>
					<DE02330101>Temps de déplacement au dossier </DE02330101>
					<DE02340101>0000084.20</DE02340101>
					<DE02350101>019.60</DE02350101>
					<DE02370101>2013-04-09</DE02370101>
				</SE0840301>
				<SE0840301 xmlns="http://www.darva.com/messages/construction/me200701">
					<DE02320501>00175</DE02320501>
					<DT92100101 code="1"/>
					<DE02330101>Rédaction du rapport            </DE02330101>
					<DE02340101>0000084.20</DE02340101>
					<DE02350101>019.60</DE02350101>
					<DE02370101>2013-04-09</DE02370101>
				</SE0840301>
				<SE0840301 xmlns="http://www.darva.com/messages/construction/me200701">
					<DE02320501>01000</DE02320501>
					<DT92100101 code="2"/>
					<DE02330101>Kilomètres affectés au dossier  </DE02330101>
					<DE02340101>0000000.61</DE02340101>
					<DE02350101>019.60</DE02350101>
					<DE02370101>2013-04-09</DE02370101>
				</SE0840301>
				<SE0840301 xmlns="http://www.darva.com/messages/construction/me200701">
					<DE02320501>00000</DE02320501>
					<DT92100101 code="A"/>
					<DE02330101>Forfait ouverture               </DE02330101>
					<DE02340101>0000136.40</DE02340101>
					<DE02350101>019.60</DE02350101>
					<DE02370101>2013-04-09</DE02370101>
				</SE0840301>
				<SE0840301 xmlns="http://www.darva.com/messages/construction/me200701">
					<DE02320501>00250</DE02320501>
					<DT92100101 code="A"/>
					<DE02330101>Forfait horaire (au-delà de 5 H)</DE02330101>
					<DE02340101>0000013.90</DE02340101>
					<DE02350101>019.60</DE02350101>
					<DE02370101>2013-04-09</DE02370101>
				</SE0840301>','2013-04-18',32,5,17,6,121,17,8,'2013-04-11 13:53:10','2013-04-26 15:56:10','2013-04-11',100,null,null,1081,1,2,0,'2013-04-26 15:56:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1082,2,16,'Période par défaut','Par défaut, positionner la période sur "Ce Mois-ci" au lieu de "Toute la période"',null,null,1,null,4,null,4,0,'2013-04-11 13:55:17','2013-04-11 13:55:17',null,0,null,null,1082,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1083,2,2,'Valeur par défaut du libellé Dommage quand celui-ci est vide.','Les OM avec un desordre non renseignés sont problématiques : l''utilisateur est bloqué dans l''écran CPXG001 avec un message champs obligatoire.

h2. Demande

Par défaut, si le désordre est vide dans l''OM, renseigner un point "." dans le libellé du dommage des écrans CPXG001, CPXG002
',null,33,10,5,4,89,4,8,'2013-04-12 15:36:40','2013-04-26 15:10:09',null,100,8,931,931,24,25,0,'2013-04-26 15:10:09');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1084,3,6,'Darva: Mise en place des trigger','Trigger pour toutes les agences pour Darva',null,null,5,11,4,130,11,0,'2013-04-15 10:23:59','2013-04-15 10:23:59','2013-04-15',100,null,null,1084,1,2,0,'2013-04-15 10:23:59');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1085,2,6,'Darva faire referentiel avec code postaux pour SMABTP','pour certains abonnés il faut rediriger l''OM en fonction du code postal du sinistre
code expert 
E97000022047 (martinique, guadeloupe...)
E59000022057 (0590)','2013-04-17',32,5,17,6,121,17,3,'2013-04-15 10:31:44','2013-04-26 15:55:10','2013-04-15',100,null,null,1085,1,2,0,'2013-04-26 15:55:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1086,2,6,'DeX: Mise à jour du référentiel SIRET Agence','Le référenrtiel SIRET Agence doit être mis à jour','2013-05-24',31,9,11,4,135,11,2,'2013-04-15 10:41:21','2013-05-13 11:28:19','2013-04-15',80,null,null,1086,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1087,2,5,'Indexation des PJ','Voir le document de spécification sur le serveur',null,null,5,18,4,null,10,2,'2013-04-15 11:21:01','2013-04-26 09:47:59','2013-04-17',0,24,892,391,17,18,0,'2013-04-26 09:47:59');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1088,2,5,'Création d''un serveur SOAP dans Symfony2','- Permettre à notre application d''exposer des Webservice SOAP
- Spécifier avec l''équipe dataexchanger les besoins de WSDL
- Vérifier que l''accès est sécurisé : username/password ou IP DEX
','2013-06-12',null,2,18,4,null,10,9,'2013-04-15 11:23:35','2013-05-03 15:23:23','2013-04-15',80,64,null,1088,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1089,2,5,'Fusionner les tables ClientMajeur, Assureur Recours et Assureurs Police','',null,null,1,10,4,null,10,2,'2013-04-15 11:27:02','2013-05-03 15:20:11','2013-04-15',0,80,null,1089,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1090,1,6,'Scripts suppression d OM dans CP2000 ne pas supprimer la police dans tous les cas','CP2000SuppressionOMDarva.vsl et CP2000SuppressionOMGSicass.vsl ','2013-06-14',31,9,17,4,52,17,9,'2013-04-15 12:01:53','2013-05-17 16:15:20','2013-04-15',100,null,null,1090,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1091,2,5,'Ajouter l''attribut CliMessageAuto au Client','Dans le le backend 
- ajouter client
- modifier client
- lister client',null,null,5,8,4,null,10,3,'2013-04-15 15:36:28','2013-04-26 09:49:27','2013-04-15',100,4,560,365,43,44,0,'2013-04-26 09:49:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1092,3,2,'Darva - Axa','Test VB',null,73,6,4,4,null,4,1,'2013-04-16 16:00:40','2013-04-16 16:04:19',null,0,null,null,1092,1,2,0,'2013-04-16 16:04:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1093,3,10,'Présentations - reunion groupe','Réunion interne Groupe Prunay
non directement lié à un projet.',null,null,4,4,4,null,4,1,'2013-04-16 18:54:41','2013-04-16 18:55:19',null,0,null,null,1093,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1094,1,15,'BUG Test Unitaires','.PHP Fatal error:  Call to undefined method CPCOM\CPWebCommonBundle\Entity\Operation::setAgence() in /var/lib/jenkins/workspace/KASTOR-ANALYSIS/kastor-src-analysys/src/CPCOM/AdminBundle/Tests/Entity/OperationTest.php on line 225
',null,null,9,null,4,null,10,1,'2013-04-17 10:30:30','2013-04-17 10:48:32','2013-04-17',100,null,null,1094,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1095,2,5,'Affectation de client à un dossier','Nouvelle règle de gestion à prendre en compte.
- tous les clients sont associés à des types de mission
- lorsque l''on ajoute/modifie un client à un dossier, il faut que la liste des clients soit affichée en fonction du type de mission du dossier
Exemple: un dossier DOA ne sera "affectable" qu''à des clients qui ont le type de mission DOA dans leur liste.


Attention, la recherche de client dans ce contexte devra prendre en compte cette problématique.',null,null,5,8,4,null,10,3,'2013-04-17 10:41:47','2013-04-26 09:47:14','2013-04-17',100,8,null,1095,1,2,0,'2013-04-26 09:47:14');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1096,2,5,'Mettre les droits sur le front-office','En attente du retour de EB sur la gestion des droits',null,null,1,10,4,null,10,2,'2013-04-17 14:33:50','2013-04-17 15:01:28','2013-04-17',0,80,1014,29,5,6,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1097,2,5,'Développer l''écran de saisie de crac/sys','# Suivre les spec de SLY','2013-05-16',null,2,8,4,null,10,14,'2013-04-17 14:38:23','2013-05-21 10:37:08','2013-05-13',67,32,988,923,3,10,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1098,2,5,'Specification CRAC/SYCODES','- Modele de BD
- Maquette','2013-04-19',null,2,10,4,null,10,6,'2013-04-17 15:08:40','2013-05-21 10:54:23','2013-04-18',100,16,988,923,11,12,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1099,2,2,'Alerte Mail à envoyer sur boîte mail PFT dès archivage d''un mail dans un dossier bloqué PFT','La Plateforme n''est pas informée des mails reçus et archivés par les agences dans les dossiers suivis par la PFT.

--> Dès qu''un mail est archivé dans un dossier identifié comme suivi par la PFT (indexation du mail en 2.11): envoi de l''alerte mail sur la boîte mail de la PFT.',null,null,1,4,4,null,6,1,'2013-04-18 12:41:31','2013-04-19 15:06:35',null,0,null,null,1099,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1100,3,2,'SITE FORMATION à mettre à jour','SVP mise à jour du site FORMATION pour semaine prochaine (avant le 24/04)','2013-04-22',null,5,4,6,null,6,3,'2013-04-18 12:43:02','2013-05-13 17:44:33','2013-04-21',0,4,null,1100,1,2,0,'2013-05-13 17:44:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1101,1,6,'Darva: Prb d''encodage de caractère sur la donnée DE05360101  du message SD16 ','Réponse Darva concernant un AR qui n''a pu être traité (fichier requête ci-joint) 

En ce qui est le SD16, la donnée DE05360101 contient des caractères qui ne vérifie pas le pattern défini pour cette donnée :
XML validation error on request: cvc-pattern-valid: Value ''PavÃ© de la chapelle'' is not facet-valid with respect to pattern ''[\\p{IsBasicLatin}àéèùÁÈÉÙâêîôûÂÊÎÔÛäëïöüÄËÏÖÜ§!ç\\-_$*£%=+:/;.,?@?²~''`«»#(){}°øØµ^|]*'' for type ''#AnonType_DE05360101''.
',null,32,5,17,4,130,11,2,'2013-04-19 10:41:23','2013-04-26 14:51:36','2013-04-19',100,null,null,1101,1,2,0,'2013-04-26 14:48:27');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1102,1,6,'Darva: Donnée DE05410101 vide pour LR','Réponse de Darva 

Pour le RE, le problème vient de la donnée DE05410101. Cette donnée ne peut pas être vide et doit contenir au moins un caractère :
XML validation error on request: cvc-minLength-valid: Value '''' with length = ''0'' is not facet-valid with respect to minLength ''1'' for type ''#AnonType_DE05410101''.
 
',null,32,5,17,4,130,11,2,'2013-04-19 10:49:53','2013-04-26 14:51:23','2013-04-19',100,null,null,1102,1,2,0,'2013-04-26 14:50:06');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1103,2,6,'NOE: Modification dans les scripts NOE pour prendre en compte la NOE STI 2.4.4','NOE STI version 2.4.4
Les nouveaux champs et rubriques(mandant, gestionnaire, origine etc)
Modification dans les scripts NOE pour prendre en compte la modification de l''identifiant tier ','2013-05-31',59,2,11,4,93,11,1,'2013-04-19 10:58:59','2013-05-13 11:59:39','2013-04-19',20,null,null,1103,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1104,2,19,'Ne pas afficher Modèle et désignation du modèle ','Document - Ecran – Ne pas afficher Modèle et désignation du modèle 

!suppression_modele.png!

DEM-255 - PP',null,79,10,25,4,136,4,4,'2013-04-19 13:09:34','2013-04-30 10:16:50',null,100,null,null,1104,1,2,0,'2013-04-19 13:45:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1105,2,19,' Enrichissement - Dos / Doc / Rdv / Nh / Détail NH ','12/04/2013 : suite à réunion NDS + JFB + RH

h1. OBJET DU DOCUMENT

h2. Contexte
La base Hermès alimentée par les différents systèmes de gestion est utilisée dans le cadre de reportings statistiques.

Nous souhaitons étendre les possibilités de reportings. Dans cette optique, il se révèle que les données présentes dans les SI de gestion ne sont pas remontées dans Hermès.

Dans cet objectif, la base Hermès doit être enrichie.

Ceci nécessite :
* la mise à jour de la norme d’échange intitulée "Spécification" --> #1040
* ARDIs : 
** l’adaptation du traitement d’export des données
** l’export des données enrichies (1) liés aux dossiers dans le but de la mise à jour du stock des dossiers déjà dans HERMES 
** Rem : Les autres SI de gestion en voie d’obsolesence ne sont pas concernés (ex : Winexpert)
* HERMES : 
** l’enrichissement de la base de données
** l’adaptation du traitement d’import
** la mise à jour de la base HERMES à partir du fichier d’export mentionné ci-dessus en (1)

h2. Objectif

L’objectif de ce document est d’identifier les entités à enrichir et les données complémentaires.


h1. DESCRIPTION DE LA FONCTIONALITE

h2. Entité Dossier 

* Type de police
* Champs à cocher « Responsable/Lésé »
* Début de date de sinistre (si sélection d’une plage de dates)
* Fin de date de sinistre (si sélection d’une plage de dates)
* Qualité de l’assuré
* Numéro de téléphone de l’assuré
* Adresse mail de l’assuré
* Numéro de téléphone du sinistre
* Vérification du risque (booléen – 0 si non coché dans ARDI – 1 si coché dans ARDI) #1021
* Convention
* Date de création du dossier
* Date de dernière relance
* +Date de remise frappe+
* +Date de frappe+
* Date d’envoi de la lettre d’accord                  
* Date de retour de la lettre d’accord
* Référence Mandant
* Référence Intermédiaire
* Référence Expert
* Référence Gestionnaire
* Référence Inspecteur

h2. Entité Document 

* Code secrétaire #1028
* Nom de la secrétaire #1028
* Date de traitement (doh_date_traitement) #1016
* Date de relance (doh_date_relance) #1016

Point important :
Tous les évènements doivent remontés même s’ils ne sont pas associés à des documents
Règle :
* Les évènements DARVA sans document associé seront indiqués « Visible par tous » à HERMES 
* Les évènements hors DARVA sans document associés seront indiqués « Visible par personne » à HERMES 

h2. Entité "NH" 

* Payeur 
* Imprimé
* Frais
* code secrétaire (dem. complémentaire P.PERRUISSET)
* nom secrétaire (dem. complémentaire P.PERRUISSET)
* code agence (dem. complémentaire P.PERRUISSET)

h2. Création d’une nouvelle entité NH-Experts

Cette entité contient la ventilation de la NH par expert
(rejoins #1015)

* Clé NH 
* Code Expert
* Nom Expert
* Montant HT

h2. Création d’une nouvelle entité NH-Types

Cette entité contient la ventilation par type de facturation (Frais / Honoraires / …)

* Clé NH 
* Type ligne 
* Désignation
* Quantité
* Montant HT
* Taux TVA
* Montant TVA

h2. Entité  Rendez-vous

(reprise de la demande #1022)

* Code expert 
* Nom Expert
* Date « Contact du »
* Adresse 1 RDV
* Adresse 2 RDV
* Adresse 3 RDV
* Code postal du RDV
* Ville du RDV','2013-05-21',78,2,25,4,117,4,7,'2013-04-19 13:37:04','2013-05-21 15:52:02','2013-05-17',40,32,null,1105,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1106,2,5,'Clé primaire du dossier','Ajouter la clé primaire du dossier dans toutes les actions de modification/ détail liées au dossier
Ajouter l''annotation correspondante dans le controller cf: consigne DB','2013-04-24',null,5,18,4,null,10,3,'2013-04-23 09:59:15','2013-04-26 09:47:33','2013-04-23',100,16,1023,1023,2,3,0,'2013-04-26 09:47:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1107,2,5,'Développer le document type CRAC/Sycodes','En attente des retours de DS et SC
Mail du 23/04/2013

Nous sommes en cours d’analyse des fiches CRAC/SYCODES et je viens d’obtenir le document généré (DTY165 ind 06)
Bien que le document type lié ne soit pas prioritaire dans l’analyse des documents, pouvez-vous me dire si :
-	il s’éloigne énormément de ce que nous avons à produire dans Kastor ou s’il s’agit d’un document « légal » qui n’a pas vocation à évoluer de manière significative.

','2013-05-14',null,2,18,4,null,10,3,'2013-04-23 10:09:45','2013-05-21 11:00:08','2013-05-13',0,16,988,923,13,14,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1108,2,5,'Migration de Symfony','Décrire les contraintes de passage de Symfony 2.0 à Symfony 2.3',null,null,2,18,4,null,10,18,'2013-04-24 13:54:54','2013-05-23 14:02:31',null,36,null,null,1108,1,14,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1109,3,2,' points au 23/04/2013','Voici une liste de points dont il nous semble qu’ils n’ont pas été traités au niveau de la paye de mars.
L’objectif reste de pouvoir tester directement sur la fin avril afin de sécuriser la paye de mai.','2013-05-16',77,2,4,6,null,4,23,'2013-04-25 14:51:44','2013-07-04 22:27:52','2013-05-13',58,49,925,925,50,63,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1110,1,2,'1 - Traitement des avoirs','La règle de catégorisation des avoirs est la suivante : 
Si l’avoir pris en compte dans mois correspond à une NH annulée antérieure au 01/03/2013 alors la catégorie de l’avoir est D (coefficient correcteur de 1). En revanche si la NH annulée est postérieure au 01/03/2013 alors la catégorie de l’avoir est celle de la NH annulée. Par conséquent si un avoir est édité juste après la NH annulée (dans le même mois) la catégorie sera identique pour les deux factures.
','2013-04-29',77,5,5,6,144,4,4,'2013-04-25 14:53:14','2013-05-02 11:23:33',null,100,4,1109,925,51,52,0,'2013-04-26 15:00:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1111,1,2,'2 - Martial Floc’h','Martial Floc’h doit être correctement orthographié (Floch n’est pas bon)','2013-04-29',77,5,4,6,144,4,5,'2013-04-25 14:54:06','2013-05-02 11:23:55',null,100,4,1109,925,53,54,0,'2013-04-29 11:58:19');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1112,1,2,'3 - NH sans catégorie','La dernière extraction réalisée montre qu’il manque un certain nombre de catégorie. Le fichier en PJ (NH sans catégorie) reprend les NH sans catégorie avec une indication dans la colonne de la catégorie à rentrer.


','2013-05-16',77,9,5,6,null,4,7,'2013-04-25 15:15:24','2013-05-16 16:18:19','2013-05-15',60,16,1109,925,55,56,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1113,2,2,'4 - Ajout de colonne - calcul','Serait-il possible pour faciliter la paye des experts délégués de rajouter au-delà des 4 colonnes récemment rajoutées, une colonne ou plusieurs colonnes avec un calcul ? Le calcul n’est effectué que pour les experts délégués. -Le mode de calcul sera donné ultérieurement.-

h2. calcul

Si la colonne [Rétrocession Régie base de calcul (colonne AJ)] = VacationExpert alors :

* Uniquement pour colonne A = RégieVac :
** Calculer le produit P1 de la cellule [Rétrocession Régie Taux (colonne AK, sous forme de pourcentage)] et de la cellule [Base rétrocession (colonne M)]

** Si la cellule [Rétrocession Forfait base de calcul (colonne AM)] = VacationExpert et seulement pour les lignes où la cellule de la colonne A = ForfaitVac :
*** Calculer le produit P2 de la cellule [Rétrocession Forfait Taux (colonne AN sous forme de pourcentage)] et de la cellule [Base rétrocession (colonne M)]

** si la cellule [Rétrocession Forfait base de calcul (colonne AM)] = C.A. et seulement pour les lignes où la cellule de la colonne A = ForfaitCa :
*** Calculer le produit P2 de la cellule [Retrocession Forfait Taux (colonne AN sous forme de pourcentage)] et de la cellule [Montant TTC sans coefficient (colonne AB)]


Si la colonne [Rétrocession Régie base de calcul (colonne AJ)] = C.A. alors :

* Uniquement pour colonne A = RégieCa :
** calculer le produit P1 de la cellule [Rétrocession Régie Taux (colonne AK, sous forme de pourcentage)] et de la cellule [Montant TTC sans coefficient (colonne AB)]

* Uniquement pour colonne A = ForfaitCa:
** calculer le produit P2 de la cellule [Rétrocession Forfait Taux (colonne AN, sous forme de pourcentage)] et de la cellule [Montant TTC sans coefficient (colonne AB)]','2013-05-14',77,9,5,4,null,4,9,'2013-04-25 15:18:24','2013-05-16 14:59:24','2013-05-13',60,16,1109,925,57,58,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1114,2,2,'5 - intitulé des colonnes montant','Changer l’intitulé des colonnes "Montant TTC sans coefficient" en "montant HT sans coefficient" et "Montant TTC" en "Montant HT"','2013-04-29',77,5,5,6,144,4,4,'2013-04-25 15:19:26','2013-05-02 11:24:42',null,100,1,1109,925,59,60,0,'2013-04-26 10:11:58');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1115,3,2,'6 - poste de Dominique Gouveia ','Il semblerait qu’il y ait maintenant des problèmes sur le poste de Dominique Gouveia (lors de l’extraction des informations sont manquantes, alors que l’extraction réalisée sur le poste de Zohra Romany toutes les informations sont présentes). A voir directement avec Dominique G.

',null,77,1,4,4,null,4,4,'2013-04-25 15:21:01','2013-05-16 11:26:42',null,0,8,1109,925,61,62,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1116,1,2,'Darva : Traitement Mission insertion des champs des parties mauvais encodage','lorsque l''on insere les nom et adresse des différentes parties (assuré...) on lit le fichier xml des parties encodé en utf 8 ce qui a pour effet de mettre des caracteres bizarres dans la base de données','2013-05-15',33,9,17,6,null,17,3,'2013-04-26 09:40:29','2013-05-16 11:09:34','2013-04-30',100,null,null,1116,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1117,3,6,'Darva agence 0641 mauvais libellé sur le portail','sur le portail Darva l''agence 0641 apparait comme EUREXO BASSIN DE L’ADOUR 
il devrait y avoir Eurisk Agirdex','2013-05-31',32,2,17,4,153,17,3,'2013-04-26 14:06:13','2013-05-13 10:51:44','2013-05-02',60,null,null,1117,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1118,2,6,'Darva Accuse de reception partie Cabinet','suite a une demande de Dominique on ne met plus le Cabinet Louveciennes systematiquement mais celui de l agence concernée','2013-05-20',32,9,17,4,138,17,5,'2013-04-26 14:44:11','2013-05-15 10:09:38','2013-05-01',100,0.5,null,1118,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1119,2,2,'CPXG124 - ajout info gestion déléguée','Affichage : ajouter l''information pour la gestion déléguée

correspondance xml :

<pre>
<SE3210101><DT94000101>2</DT94000101>
<DT94870101>0</DT94870101>
<DT94010101>2</DT94010101>
<DE00650102>2013-04-02</DE00650102>
</SE3210101>
</pre>','2013-06-07',73,1,5,6,89,4,1,'2013-04-26 14:56:41','2013-05-16 12:20:50','2013-06-07',0,8,931,931,26,27,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1120,1,6,'suite mauvaise manip GSICass rediriger un om darva vers gsicass','De : Eurisk Siege/SANTONI Dominique 
Envoyé : vendredi 26 avril 2013 09:44
À : CPCOM/BOUREZ Vincent; CPCOM/BONNET Guillaume
Cc : Eurisk Côte Varoise/TRAPES Karine
Objet : TR: 9720/360534

Bonjour, 

Suite erreur manip de l’assistante (voir message ci-après) 

Vous serait-il possible de rediriger l’OM suivant sur l’agence de la Martinique 720#2013041914341100018-ACS-DO-DO-ALP-10800949-13005130-11/04/2013

On pourra ainsi annuler le dossier 360534 et recréer un nouveau dossier  à partir de l’OM.
Merci de me tenir informée 
Domi
','2013-05-24',32,9,17,4,149,17,4,'2013-04-26 15:42:28','2013-05-13 16:57:12','2013-04-26',100,null,null,1120,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1121,1,6,'Darva : Mettre Statut Echec Envoi dans Message LR, SD et NH','refaire une passe sur les scripts SD, LR et NH pour que lorsqu il y a une erreur de validation XML ou erreur quelconque mettre le statut CP2000 en echec','2013-05-17',32,9,17,4,141,17,4,'2013-04-26 16:36:11','2013-05-13 10:39:44','2013-04-26',90,null,null,1121,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1122,3,19,'Support - Aide hotline','Consigner ici toute aide apportée au Support sur les sujets Hermes Dommage.

1 aide = 1 commentaire

* Ne pas hésiter à mentionner le ticket Track-it
* Si cela donne lieu à une autre demande (exemple correction d''anomalie), noter la référence
',null,null,4,null,3,null,4,1,'2013-04-29 11:12:09','2013-04-29 11:16:07',null,0,null,null,1122,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1123,1,19,'Problème import Hermès : documents absents','Documents absents sur les dossiers 312241 – 328321 – 334146 – 338020 – 339405 – 343727 – 346940 – 349415 – 350244 – 351741 – 351932 – 352599 – 352598 – 354565 – 355462 – 358717 – 351894 (entre autres?...)

CARD a déployé le 19/04/2013 un nouveau transfert Hermes, a priori des champs supprimés depuis longtemps apparaissent dans les XML d''import.

Tâches : 

* confirmer l''origine du problème
* redéployer la précédente version du transfert hermes et aider à la reprise de données
* boucler avec CiteSI sur le problème de champs non valides toujours présents dans le code java (doh_emetteur, dos_montantdom, et peut-être d''autres) et d''exceptions muettes',null,78,7,25,5,null,25,3,'2013-04-29 12:04:54','2013-05-06 16:18:27',null,0,null,null,1123,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1124,1,6,'GSICASS: La MAJ de SIRET entraine une erreur sur la plateforme GSICASS','Les NH envoyées concernant les OMs antérieurs à la maj du référentiel SIRET sont en erreur (pas de versioning du référentiel chez apria) ','2013-05-31',40,9,11,5,122,11,2,'2013-04-29 12:05:09','2013-05-13 16:57:30','2013-04-17',90,null,null,1124,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1125,1,6,'Darva : suppression de l agence lors de la recherche du user/mdp Darva WS','En consequence de la reception d OM avec le code abonne martinique que l on doit inserer en sur la base Guadeloupe (qui a un autre code abonne)
lors de l''envoi de l''accusé reception/commentaire, Rapports et NH on ne doit pas tenir compte de l agence pour selectionner les user/mdp de la table darva_utilisateur mais simplement du code abonne exemple de cas :
on reçoit un OM avec le code abonne martinique
celui ci doit etre inserer dans la base agence guadeloupe
lors de l''ar celui ci est generer sur l agence guadeloupe
si l on tient compte de l agence indiquée ici guadeloupe on ne retrouvera pas le user/mdp car le code abonne de l om est celui de la martinique','2013-04-29',32,5,17,6,142,17,3,'2013-04-29 12:32:19','2013-05-13 10:38:57','2013-04-29',100,null,null,1125,1,2,0,'2013-05-13 10:38:57');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1126,1,6,'Darva V1 WebService En Erreur','recensement des erreurs d accessiblité WebService',null,32,2,null,4,null,17,16,'2013-04-29 16:12:07','2013-05-21 16:33:53',null,0,null,null,1126,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1127,2,2,'Passage des champs commentaires de tous les SD à Spécial String pour accepter le €','',null,73,9,5,4,89,5,2,'2013-04-29 16:21:39','2013-05-02 15:19:42',null,60,1,931,931,28,29,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1128,2,5,'Gestion des responsable CRAC/SYS','',null,null,9,8,4,null,10,2,'2013-04-30 10:49:49','2013-05-16 15:12:50','2013-05-13',100,16,1097,923,4,5,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1129,2,5,'Tests unitaires CRAC/SYS + RESPONSABLE','','2013-05-16',null,9,8,4,null,10,2,'2013-04-30 10:50:50','2013-05-15 17:59:03','2013-05-15',100,16,1097,923,6,7,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1130,1,6,'Darva pb caractere " dans les commentaires','suppression du guillemet dans les commentaires envoyés','2013-04-30',32,5,17,6,143,17,2,'2013-04-30 15:17:23','2013-05-13 10:38:11','2013-04-30',100,0.5,null,1130,1,2,0,'2013-05-13 10:38:11');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1131,2,2,'Service Viamichelin HS - évolution de l''accès','Le service Viamichelin a modifié son mode de fonctionnement.
Il faut revoir le système de génération de lien pour le bouton viamichelin dans CP2000

Cf Ticket chez Sacha le 30/04/2013
',null,null,1,null,4,null,4,0,'2013-04-30 15:48:05','2013-04-30 15:48:05',null,0,24,null,1131,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1132,2,6,'Darva Mettre a jour les triggers pour les SD pour la version SMABTP V1','Lors de la mise en prod Darva AXA Mettre a jour le chemin des triggers accusé reception et commentaire pour la version SMABTP V1','2013-06-19',75,1,null,4,82,17,1,'2013-04-30 16:15:47','2013-05-13 11:04:02','2013-05-21',0,null,902,902,38,39,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1133,1,6,'GSICass Envoi rapport retour WS KO et statut Dex en succes','lors de l''envoi de rapport GSICass il peut arriver que le WS retourne KO donc erreur mais le statut DeX est à success il faut qu il soit en erreur aussi','2013-05-13',40,5,17,4,145,17,3,'2013-04-30 16:24:32','2013-05-13 11:44:43','2013-04-30',100,null,null,1133,1,2,0,'2013-05-13 11:44:33');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1134,3,10,'Infra','To do list avec l''infra',null,null,4,4,5,null,4,3,'2013-04-30 16:58:14','2013-04-30 17:00:43',null,0,null,null,1134,1,4,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1135,3,10,'Accès SQL Redmine pour Christophe Maroni','Sujet demandé à JH Delcourt le 30/04 :

* Ouvrir un accès SQL en lecture seul pour CM.
* Voir pour ajouter un DNS sur le 10.31.2.42',null,null,2,4,5,null,4,1,'2013-04-30 16:59:53','2013-04-30 17:00:56',null,0,null,1134,1134,2,3,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1136,3,10,'Direction','To do list avec les directeurs

* en attente de débriefing
* jusqu''à ouverture d''un sujet dev',null,null,4,4,4,null,4,0,'2013-04-30 17:02:03','2013-04-30 17:02:03',null,0,null,null,1136,1,10,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1137,3,10,'Paul Boyer','',null,null,4,4,4,null,4,0,'2013-04-30 17:02:48','2013-04-30 17:02:48',null,0,null,1136,1136,2,3,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1138,3,10,'Emmanuel Beeker','',null,null,4,4,4,null,4,0,'2013-04-30 17:03:09','2013-04-30 17:03:09',null,0,null,1136,1136,4,5,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1139,3,10,'Christophe Maroni','',null,null,4,4,4,null,4,1,'2013-04-30 17:03:33','2013-05-13 18:55:19',null,0,null,1136,1136,6,7,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1140,3,10,'François Amant','',null,null,4,4,4,null,4,0,'2013-04-30 17:03:42','2013-04-30 17:03:42',null,0,null,1136,1136,8,9,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1141,2,6,'Darva AXA Faire table dans referentiel pour correspondance code sycodes','dans le referentiel dataexchanger creer une nouvelle table qui fera le lien entre les codes que demandent DARVA pour les données 9493 et 9434 avec les 2 premiers code de la cause desordre dans la table CP2000 CPXSCY
exemple :
A1 01
A2 02
...

B1 10

','2013-05-09',75,9,17,4,82,17,3,'2013-05-02 14:42:55','2013-05-02 17:32:41','2013-05-02',100,null,990,902,11,12,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1142,3,6,'Faire Page Wiki pour decrire le fonctionnement de repartition des OMs dans les agences','1. La règle générale de répartition des OM
2. Les spécificités des différents systemes (Gsicass, Darva)
3. Les spécificités de chaque Client (SMABTP/AXA)
4. Les règles spécifiques pour certaines agences
5. Les exceptions diverses et variées...
','2013-05-31',70,2,null,4,155,17,1,'2013-05-02 16:15:10','2013-05-13 11:39:44','2013-04-02',90,null,null,1142,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1143,2,6,'Darva AXA Mettre les fiches baremes dans les rapports','','2013-05-17',75,2,17,4,82,17,2,'2013-05-02 17:57:01','2013-05-14 16:27:14','2013-05-02',20,null,902,902,40,41,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1144,3,2,'Paye de MAI','Liste d’anomalies rencontrées sur l’extraction paye mai (sur vacations d’avril) 

Au-delà de la correction à réaliser pour cette paye de mai il est nécessaire de comprendre :
* pourquoi il y a un tel écart
* comment faire pour ne pas reproduire ces anomalies ?','2013-05-31',77,1,4,6,null,4,20,'2013-05-03 10:19:35','2013-05-21 10:05:28','2013-05-03',32,104,925,925,64,79,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1145,1,2,'1 - Ecart de chiffre d’affaires','Il manque une quarantaine de NH pour obtenir le CA d’avril (environ 37 000 EUR). La liste est en cours de constitution, voici des premiers éléments de NH manquantes :
* 12353
* 12357
* 12447
* 12472
* 12512
* 12526
* 12591
* 12636
* 12648
* 12660
* 12695
* 12765
* 12843
* 12880
* 12908
* 12974
* 13018
* 13208


h2. A faire

Trouver pourquoi ces NH sont manquante dans l''extraction.',null,77,5,4,4,146,4,4,'2013-05-03 10:23:00','2013-05-05 15:17:12','2013-05-03',100,16,1144,925,65,66,0,'2013-05-05 15:17:12');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1146,1,2,'2 - Référence NH fausses','Lors de la vérification des catégories, il apparait que des numéros de NH ne correspondent pas sur la liste d’extraction excel, aux références dossier.

Par exemple : 

La NH 2013 13130 ne correspond pas au dossier 0450/630182 (sur feuille excel car Hermès donne 0130/136613)
<pre>ForfaitVac	08/04/2013	2013	13130	TOURAINE                        	450	630182</pre>

La  NH 2013 12468 est pointé sur 0380/489452 alors qu’Hermès donne 0060/621197
<pre>ForfaitVac	02/04/2013	2013	12468	DAUPHINE - SAVOIE               	380	489452</pre>

Il y a sûrement d’autres cas difficilement décelables. Comprenez-vous la raison ? 
Ce qui m’inquiète c’est que l’extraction ancienne méthode donne les bons dossiers.
','2013-05-22',77,1,5,6,null,4,5,'2013-05-03 10:24:51','2013-05-16 11:22:49','2013-05-21',0,16,1144,925,67,68,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1147,1,2,'3 - Erreur de destination de la catégorie','* La NH 2013/132216 sur le dossier 0370/333571/DOA est renseignée avec une indemnité de 79€. La catégorie devrait donc être A alors que nous sommes en B.
* La NH 2013/12476 sur le dossier 0380/489378/DOA est renseignée avec une indemnité de 0(ou pas d’indemnité je ne sais pas). La catégorie devrait donc être A alors que nous sommes en B.
* La NH 2013/12871 sur le dossier 0740/301941/DOA est renseignée avec une indemnité de 0. La catégorie devrait donc être A alors que nous sommes en B.
* La NH 2013/14702 sur le dossier 0130/135998/DOA est renseignée avec une indemnité de 100€. La catégorie devrait donc être A alors que nous sommes en B.
','2013-05-24',77,2,5,6,null,4,5,'2013-05-03 10:27:45','2013-05-21 10:05:28','2013-05-23',10,16,1144,925,69,70,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1148,1,2,'4 - Rétroactivité sur un dossier DOA avec RP et RE','Sur le dossier 0060/621669 nous avons une première NH (correspondant au RP) avec la bonne catégorie (A). Elle génère deux lignes l’une pour l’expertise l’autre pour les frais km.

La 2ème NH (sur le même mois d’avril) correspond au RE qui indique une indemnité > TM et la catégorie est effectivement B.
Une ligne est créée mais uniquement pour rattraper avec un coefficient correcteur de 0.1 la partie non versée sur la 1ère NH.
La catégorie de cette ligne est correcte (AB) en revanche le mode de calcul ne l’est pas : le montant de la vacation en rattrapage devrait être de 56.62 EUR et il n’est que de 3€. Ce calcul semble venir de :

* Prise en compte uniquement de la facture de km (43.8 dans la colonne rétrocession)
* Application dans la cellule base de rétrocession sans coefficient du coefficient de vacation pour l’expertise (et non pour les km)
* Puis application du coefficient correcteur de 0.1 correspondant à la catégorie.

Le modèle aurait dû être à mon avis génération de deux lignes avec une pour l’expertise, l’autre pour les km avec à chaque fois application du bon coefficient de vacation (expertise et km)
','2013-05-30',77,1,5,6,null,4,2,'2013-05-03 15:28:31','2013-05-16 11:25:38','2013-05-27',0,32,1144,925,71,72,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1149,1,2,'5 - aucune catégorie sur le serveur agence central','Toutes les NH de l’agence centrale sont sans catégorie à l’exception des 2 NH 16455 et 16456.
Ceci fait que nous avons à chaque fois un coefficient correcteur de 1 (il y a cependant des dossiers en RC et des DO<TM)
Il semblerait que le problème soit le même (expert délégué) pour l’agence de Corse. 
','2013-05-17',77,5,4,6,null,4,6,'2013-05-03 15:48:25','2013-05-16 16:18:02','2013-05-16',100,16,1144,925,73,76,0,'2013-05-05 15:50:04');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1150,1,15,'Role intervenant','Et j''ai eu un comportement bizarre sur l''affectation d''un role d''un intervenant car j''ai perdu le role des intervenants existant du dossier',null,null,2,18,4,null,10,3,'2013-05-03 15:53:59','2013-05-13 09:32:15',null,0,null,null,1150,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1151,2,6,'Darva Enlever le nom de la partie dans le XML lorsque celle ci est vide','la donnée DE05350201 n est pas necessaire lorsque l adresse est bien renseignée du coup on enleve le tag si le nom de la partie est vide pour eviter une erreur de validation XML
le travail est a faire dans la librairie lib-ws-darva.vsl et à reporter dans son equivalent en V3','2013-05-06',32,5,17,6,148,17,2,'2013-05-06 14:23:16','2013-05-13 10:37:53','2013-05-06',100,1,null,1151,1,2,0,'2013-05-13 10:37:53');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1152,1,5,'Comprendre l''echec des r$ole des intervenants','les rôles des intervenants n''ont pas été migré et sont à "vide"
Investiguer grâce aux fichiers de LOG',null,null,5,10,4,null,10,1,'2013-05-07 11:15:05','2013-05-07 11:34:10',null,0,null,399,365,15,16,0,'2013-05-07 11:34:10');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1153,2,5,'Lister l''ensemble des fonctionalités métier de Kastor','Faire une liste de l''ensemble des fonctionnalités dé-corrélée de Kastor',null,null,1,10,4,null,10,3,'2013-05-07 14:22:25','2013-05-23 11:37:47',null,0,null,null,1153,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1154,1,6,'Darva adresse cabinet caractere ""','dans l adresse du cabinet il y a le caractere " qui pose problème car quand on passe cette information a la librairie c est interprete comme un ligne par DEX','2013-05-10',32,5,17,6,149,17,3,'2013-05-10 10:39:30','2013-05-13 10:37:36','2013-05-09',100,null,null,1154,1,2,0,'2013-05-13 10:37:36');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1155,1,6,'GSICAss OM non insere le 22/03 identifiant mission 2013032018041700862','la mission GSICass 2013032018041700862 n a pas été inséré le 22/03 a 15h00 car apparemment il y avait un conflit de clé DOCID dans la base solid par rapport a l identifiant CPTVAL (pb connu car on ne peut pas verouille celà a partir de Dex) : 
Integration des pieces jointes dans la base
***Log: pDocID = 9400000549975
2013-03-22 15:05:40 E9233: Erreur ODBC pendant ExecSqlODBCCmd:UPDATE CPXCPT SET CPTVAL = CPTVAL + 1 WHERE CPTCOD = ''DOCID''
2013-03-22 15:05:40 In ACT4: Integration des pieces jointes dans la base/Sql : Mise à jour du compteur DOCID - Table CPXCPT
2013-03-22 15:05:40 E9401: Etat d''erreur ODBC : 40001
2013-03-22 15:05:40 E9402: Numéro d''erreur natif à la base : 10006
2013-03-22 15:05:40 E9403: Erreur ODBC : [Solid][SOLID ODBC Driver][SOLID]SOLID Database Error 10006: Concurrency conflict, two transactions updated or deleted the same row
2013-03-22 15:05:40 Sql: UPDATE CPXCPT SET CPTVAL = CPTVAL + 1 WHERE CPTCOD = ''DOCID''
2013-03-22 15:05:40 E9001: Erreur de thread dans ExecuteSql
2013-03-22 15:05:40 In (S16/P106) -> (ACT4: Integration des pieces jointes dans la base/Sql : Mise à jour du compteur DOCID - Table CPXCPT)
2013-03-22 15:05:40 E9574: Erreur détectée dans la commande SQL.
2013-03-22 15:05:40 ### ACT4: Integration des pieces jointes dans la base


j ai essayé de re inséré la mission le 10/05 mais maintenant on a un problème Dex WS out of memory probablement a cause des pieces jointes

il faut donc recuperer la mission avec SOAP UI et l ''inserer dans CP2000 avec le script Dex','2013-05-13',40,2,17,6,150,17,3,'2013-05-10 13:18:20','2013-05-10 16:26:47','2013-05-10',80,null,null,1155,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1156,2,6,'GSICass recuperation mission archiver requete et reponse pour une mission','pour pouvoir rejouer la requete/reponse d une mission on archive les fichiers','2013-05-10',40,5,17,4,null,17,2,'2013-05-10 13:23:39','2013-05-13 11:43:48','2013-05-10',100,null,null,1156,1,2,0,'2013-05-13 11:43:48');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1157,2,6,'AXA V3 Mettre toutes les parties a l identique envoyée dans l OM voir JIRA Darva PRJ0009EC-11','Par rapport à une evolution Darva il faudra mettre toute les parties (Expert, Cabinet Expertise) et leurs informations (adresse...) à l identique telles qu elles sont envoyées dans le message de reception de l''OM pour les messages SD, LR et NH
voir JIRA Darva PRJ0009EC-11','2013-06-17',75,2,17,4,82,17,5,'2013-05-10 13:47:12','2013-05-16 13:15:35','2013-05-15',30,null,902,902,42,43,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1158,2,19,'modification écrans dossier','* Déplacer les champs suivants :
** Champ "Estimation expert"
*** A placer sous le champ "montant d’ouverture"
*** Libellé du champ à modifier "estimation expert"
** Champ "montant attribué mobilier"
*** A placer sous le champ "estimation expert"
*** Libellé du champ à modifier "dont mobilier"
** Champ "montant indemnité immédiate"
*** A placer sous le champ "montant indemnité"
** Champ "montant indemnité différée"
*** A placer sous le champ "montant indemnité immédiate"
* Changer les noms des onglets :
** Onglet "Documents"
*** Libellé à modifier "Historique"
** Onglet "Historique"
*** Libellé à modifier "Rendez-vous"
','2013-05-15',80,1,25,4,152,4,2,'2013-05-13 10:45:30','2013-05-13 17:44:47','2013-05-14',60,16,null,1158,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1159,3,19,'Contacter CARD STI 1.28','Contacter Card pour date de livraison prévisionnelle sur les changements de la STI 1.28 (voir #1040)
','2013-05-17',null,1,25,4,117,4,4,'2013-05-13 11:06:10','2013-05-21 10:12:28','2013-05-15',0,null,null,1159,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1160,2,19,'Test Export Card Import Hermes','Suite à livraison de Card, tester 
* l''export ARDI
* L''import Hermes dommage',null,78,1,25,4,117,4,2,'2013-05-13 11:08:03','2013-05-21 10:08:45',null,0,24,null,1160,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1161,1,6,'Darva Envoi NH avec plus de 9 operations','il y avait un bug dans le script d envoi de NH lorsque plus de 9 opérations sont envoyés il manquait le référencement au paragraphe envoiEmail','2013-05-13',32,9,17,6,153,17,1,'2013-05-13 13:55:16','2013-05-13 13:55:44','2013-05-13',100,1,null,1161,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1162,1,2,'double numérotation entre les avoirs et les factures','Revoir la numérotation des avoirs
cf bug #1146
voir historique dans #69
',null,26,9,5,4,null,4,1,'2013-05-13 14:15:25','2013-05-22 16:54:42',null,60,24,null,1162,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1163,3,2,'5 - Rattrapage catégorie NH agence centrale','Faire un rattrapage sur le serveur 10.78.1.11 (toutes les bases)

# rattrapage des forfaits (cpxfef)
# rattrapage des régies (cpxref)','2013-05-17',77,5,5,6,null,4,4,'2013-05-13 14:25:23','2013-05-16 16:18:02','2013-05-16',100,16,1149,925,74,75,0,'2013-05-16 16:18:02');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1164,2,2,'NH - ajouter la PJ','Dans le fichier déclencheur texte, il faut renseigner le chemin vers la PJ précédemment selectionnée dans l''écran "Darva".
Faire comme Gsicass.','2013-06-10',73,1,5,6,89,4,1,'2013-05-14 10:51:43','2013-05-16 12:22:43','2013-06-10',0,8,931,931,30,31,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1165,3,11,'Envoi Mail','h2. Objectif

Comprendre les envois mails a partir de NOE !

h2. Technique

Dans le fichier eurisk.war\\WEB-INF\\classes\\mail.properties, il faut renseigner le serveur de mail Prunay
mail.smtp.host=mail.groupe-prunay.fr

h2. Fonctionnel

Je ne sais pas ou sont consignées les règles des gestions.
J''ai une version du document suivant : ORD-02404-RC4T_NOE-SFD-v1 2, où tout ce qui concerne les envois était non détaillé.
',null,null,1,4,4,null,4,2,'2013-05-14 11:17:36','2013-05-14 11:44:32',null,0,null,null,1165,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1166,1,6,'DARVA faire le lien entre l’OM qui se trouve toujours dans CP2000 dossier 0800/510025 ','(n° OM : 1 ; n° sinistre 001SDO13008272, police : 444761M7605008) et le dossier 0800/510025 

Bonjour,

Merci de voir pour faire le lien entre l’OM qui se trouve toujours dans CP2000 (n° OM : 1 ; n° sinistre 001SDO13008272, police : 444761M7605008) et le dossier 0800/510025 qui  a été ouvert DARVA en cochant la case, sans passer par « créer » sur l’écran CPXG094.

Bonne journée

Candice Colombel 
EURISK 
Tél : 01 30 78 18 06 
Fax : 01 30 78 18 23 
Courriel : candice.colombel@eurisk.fr
','2013-05-14',32,9,17,3,121,17,3,'2013-05-14 15:06:37','2013-05-16 13:04:10','2013-05-14',90,null,null,1166,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1167,2,11,'Document exploitation','Travail sur le document d''exploitation.
',null,null,2,4,4,null,13,2,'2013-05-14 17:38:19','2013-05-14 18:38:47','2013-05-14',0,null,null,1167,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1168,2,11,'Serveur de production','Mettre à disposition le serveur de production',null,null,1,null,4,null,13,0,'2013-05-14 17:39:56','2013-05-14 17:39:56',null,0,null,null,1168,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1169,1,6,'Darva Segment partie ne pas ajouter de donnees si vide','Darva Segment partie ne pas ajouter de donnees si vide
dans la librairie lib-wsdarva.vsl lorsque l on ajoute les données sur les parties ne pas ajouter les DE... si c est vide','2013-05-15',32,9,17,6,154,17,1,'2013-05-15 10:04:44','2013-05-15 10:05:02','2013-05-15',100,0.5,null,1169,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1170,1,6,'Darva Traitement Mission statut 99 alors qu il y a eut une erreur','','2013-05-17',32,9,17,4,154,17,3,'2013-05-15 12:04:18','2013-05-15 13:23:06','2013-05-15',100,1,null,1170,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1171,1,19,'Anomalie visualisation des documents','CF le mail du 15/05 de Philippe PERRUISSET : 

--------------------------------------------------------------------------------------------------
J’ai des remontées d’agents GENERALI qui nous indiquent que les documents de certains dossiers sont absents d’HERMES. 
 
Après vérification, je confirme. Dans certains cas, je vois dans HERMES
* la ligne dans l’historique mais aucun pdf ou autre document n’est associé
* historique vide : pas de ligne
 
Les dossiers pour lesquels j’ai vérifié font partie du même périmètre que ceux concernés par le « bug » d’avant mes vacances (suite à une intervention dans la programmation de l’import, plus aucun document ne remontait +effacement de tous les documents remontés précédemment – pas de ligne dans historique du dossier). Ce problème devait être réglé par des remontées « massives » qui ont été réalisées (cf Email ci-dessous).
 
Pouvez-vous m’éclairer sur la situation ?
--------------------------------------------------------------------------------------------------

En pièce jointe, le mail complet (avec références de dossiers KO et historique de l''incident)
',null,78,2,25,3,null,25,5,'2013-05-15 13:45:40','2013-05-21 09:29:09',null,0,null,null,1171,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1172,2,6,'Darva ajouter des champs dans la table cpxomint','Pour les besoins Darva il faut ajouter dans la table cpxomint un champs pour le role dans le dossier ce champs peut être un CHAR de 3 caractères et un champs pour le Fax (faire un champs identique au telephone)','2013-05-22',28,1,5,5,null,17,0,'2013-05-16 10:30:31','2013-05-16 10:30:31','2013-05-16',0,null,null,1172,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1173,1,2,'6 - Catégorisation des avoirs','Il y a un problème sur la catégorie des avoirs.

Voici la règle de gestion à appliquer :

* Si date de la NH annulée < Mars 2013 alors coef = 1
* Sinon Coef selon la catégorie de la NH annulée

','2013-05-31',77,1,5,6,null,4,1,'2013-05-16 11:13:53','2013-05-16 11:29:33','2013-05-31',0,8,1144,925,77,78,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1174,3,2,'Création compte accès serveur de test Zohra','Créer un compte pour laisser Zohra accéder au serveur de test','2013-05-21',77,3,4,5,null,4,1,'2013-05-16 11:32:22','2013-05-21 10:37:52','2013-05-21',100,4,925,925,80,81,0,'2013-05-21 10:37:52');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1175,3,2,'Import base de donnée de production','Mettre à jour la base de test 10.31.2.40 avec la base de production','2013-05-22',77,1,4,5,null,4,0,'2013-05-16 11:34:04','2013-05-16 11:34:04','2013-05-22',0,8,925,925,82,83,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1176,1,6,'Injection des log probleme lors de l extraction des champs','dans le script cpcom.vsl lorsque l on insere les logs il peut y avoir des erreurs si la ligne est mal formattée du coup on blinde le code un peu plus pour ne pas insérer ce genre de ligne','2013-05-16',31,9,17,4,null,17,1,'2013-05-16 14:27:27','2013-05-16 14:28:30','2013-05-16',100,1,null,1176,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1177,2,5,'Gestion des exceptions','Dans les répository, s''assurer des gérer les exception par des Throws plutot que par des new exception et de faire un try/catch dans les controllers appelant
A faire après le migration SF3','2013-06-12',null,1,8,4,null,10,0,'2013-05-16 15:01:15','2013-05-16 15:01:15','2013-06-10',0,24,null,1177,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1178,1,2,'Bug lors de la modification d''indexation de document','Dans l''onglet des documents externes, après avoir modifié l''indexation d''un document (via le CPXL250), les documents ré-affichés ne sont pas les bons.',null,5,9,5,4,null,5,3,'2013-05-16 16:12:12','2013-05-17 16:32:57',null,60,null,null,1178,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1179,1,6,'GSICass envoi NH statut en warning quand retour WS pas OK','Actuellement lorsque le WS GSICass renvoi une reponse differente de OK on met le script en statut warning celà peut etre deroutant car le support n est pas averti de l erreur car la NH n a pas été envoyée dans ce cas, on genere donc une vrai erreur dans ce cas là','2013-05-17',40,9,17,6,null,17,1,'2013-05-16 16:51:48','2013-05-16 16:55:20','2013-05-16',100,null,null,1179,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1180,1,6,'GSICASS: Le script GSICASS Envoyer Rapport ne doit plus utiliser la table agenceref pour récupérer le siret d''une agence','Pour simplifier la base de référentiel Eurisk le  Le script GSICASSEnvoyerRapport ne doit pas utiliser la table agenceref pour récupérer le siret d''une agence',null,40,9,11,4,156,11,1,'2013-05-17 10:59:43','2013-05-17 11:00:37',null,90,null,null,1180,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1181,1,2,'recherche doublon police/sinistre','h2. Problème

Dominique :

> Pour ce dossier, l''agence a reçu un nouvel OM n°2 avec même références sinistre 001SDO12002489 (réapparition des infiltrations).
> 
> Quand je coche l''om pour création : la recherche d''antériorité est infructueuse et j''ai donc la possibilité de créer et d''enregistrer le nouvel OM.
> Le système devrait identifier que le dossier existe et me donner la possibilité soit :  Abandonner ou  Annuler l''OM ou Créer le dossier.

h2. Cause

<pre><code class="sql">select * from cpxpas where pasnum = ''417379U7606011'';</code></pre>

Il y a 2 polices dans la base concernant ce numéro.
Actuellement on ne vérifie que les dossiers de la première

h2. A faire

Dans la procédure LP_CHECKDOUBLON du CPXT094, il faut aussi boucler sur les polices
',null,33,1,null,5,89,4,0,'2013-05-17 11:01:55','2013-05-17 11:01:55',null,0,8,null,1181,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1182,3,2,'Supprimer l''agence 1640 de la table des agences CP2000','h2. Problème

Serge

> Est-ce qu’on pourrait arrêter ce ctrl  la base est arrêtée depuis le 23/04/2013
> 1640	E_OPEN - Erreur lors de l''ouverture de la base de données distante. ERROR=-9 MNEM= DESCRIPTION=DBMS logon error COMPONENT=CPXS017 PROCNAME=TRANSPARAM TRIGGER=READ LINE=30


h2. Cause

La base 1640 n''a pas été utilisée.
Aujourd''hui, l''application a été supprimée.
Cependant il reste du paramétrage concernant la base 1640 dans la table CPXAGE.
Cela remonte des erreurs inutiles dans le contrôle des transferts.

h2. A faire

SQL pour supprimer totalement l''agence 1640 de la table cpxage de TOUTES les bases CP2000.',null,15,1,4,5,null,4,0,'2013-05-17 11:09:24','2013-05-17 11:09:24',null,0,2,null,1182,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1183,2,2,'ECRAN CPXG123 DARVA AXA','Modification de l''écran CPXG123 Envoi DARVA AXA suivant document joint 
!"MODIFECRANCPXG123.PNG!','2013-05-17',73,9,5,5,89,6,2,'2013-05-17 14:31:13','2013-05-21 10:12:04','2013-05-17',60,4,931,931,32,33,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1184,1,4,'QBE - problème sur les documents du chantier','> De : Olivier BetK [mailto:olivier@betk.com] 
> Envoyé : vendredi 17 mai 2013 13:15
> À : CPCOM/BOUREZ Vincent
> Cc : ''Lenclud, Nathalie''; ''Romanet, Jean-Claude''; ''Olivier Rambaud''; ''David Pinault''
> Objet : Anomalies sur imports Hermes

Bonjour,

Nous constatons des anomalies lors des imports hebdomadaires vers Hermès.

Il y a, déjà, beaucoup de chantiers il est difficile de faire un test complet de tous les enregistrements.

Voici, les anomalies relevées sur 2 chantiers.

Référence 590569/001

Il y a 7 documents, si les titres sont bien à jour, les documents sont identiques, pourtant le fichier XML d’export indique bien 7 documents différents, comme vous pouvez le constater sur l’extrait XML mis à la fin de ce courriel.

Référence 553145/01

Il y a bien 10 documents, mais en contrôlant, les 2 listes ne sont pas identiques (voir, à la fin de ce courriel)

Pourtant sur les fichiers de logs du résultat de l’import, notamment celui d’il y a 15 jours, il n’y a pas d’erreur (sauf un document pdf d’un intervenant).
J’ai l’impression que lorsque l’on ajoute des informations, elles sont bien prises en compte, mais lorsque l’on modifie le contenu d’un champ (par exemple le libellé d’un document), bien que le « résultat » dans le fichier log indique que tout c’est bien passé ce n’est pas (toujours) le cas.

Un chantier ne peut pas être exporté en une seule fois, l’information se complète au fur et à mesure de l’avancée du dossier. 
Et les informations sont susceptibles d’être modifiées au fil du temps. De ce fait la mise à jour est aussi importante que l’ajout d’un enregistrement. 
Sauf erreur de ma part, la commande « UPSERT » utilisé dans la structure XML d’export est un standard SQL qui signifie INSERT OR UPDATE, donc tout devrait bien se passer. Je ne comprends pas.

Maintenant, en dehors de ces deux chantiers, qu’en est-il des autres chantiers exportés vers Hermès ? Nous ne le savons pas ! 

Que faire ?
Tout détruire chaque semaine pour tout ré-importer ne semble pas raisonnable, de plus lorsqu’il y aura des sinistres attachés aux chantiers cela ne sera plus faisable.

Qu’en pensez-vous ? 

Je reste à votre disposition, 

Bien cordialement,

Olivier Kauf



h2. EXTRAIT XML pour 590569/001

<object>
<name>DocumentChantier</name>
<action>upsert</action>
<data>
<idChantier.numeroRisque>590569/001</idChantier.numeroRisque>
<idLibelle>1</idLibelle>
<compLibelle>028 Questionnaire</compLibelle>
<docChantierUrl>028 - QUESTIONNAIRE MARSH - 16 11 12.PDF</docChantierUrl>
</data>
</object>
<object>
<name>DocumentChantier</name>
<action>upsert</action>
<data>
<idChantier.numeroRisque>590569/001</idChantier.numeroRisque>
<idLibelle>3</idLibelle>
<compLibelle>028 RICT</compLibelle>
<docChantierUrl>028 - RICT.PDF</docChantierUrl>
</data>
</object>
<object>
<name>DocumentChantier</name>
<action>upsert</action>
<data>
<idChantier.numeroRisque>590569/001</idChantier.numeroRisque>
<idLibelle>4</idLibelle>
<compLibelle>028 Liste des intervenants</compLibelle>
<docChantierUrl>028 - RÉCAPITULATIF GÉNÉRAL LOTS - 29 NOV 2012.PDF</docChantierUrl>
</data>
</object>
<object>
<name>DocumentChantier</name>
<action>upsert</action>
<data>
<idChantier.numeroRisque>590569/001</idChantier.numeroRisque>
<idLibelle>9</idLibelle>
<compLibelle>028 RFCT</compLibelle>
<docChantierUrl>028 - RFCT.PDF</docChantierUrl>
</data>
</object>
<object>
<name>DocumentChantier</name>
<action>upsert</action>
<data>
<idChantier.numeroRisque>590569/001</idChantier.numeroRisque>
<idLibelle>8</idLibelle>
<compLibelle>028 DGD</compLibelle>
<docChantierUrl>028 - DGD.PDF</docChantierUrl>
</data>
</object>
<object>
<name>DocumentChantier</name>
<action>upsert</action>
<data>
<idChantier.numeroRisque>590569/001</idChantier.numeroRisque>
<idLibelle>7</idLibelle>
<compLibelle>028 PV de Réception</compLibelle>
<docChantierUrl>028 ATTEST SMEF.PDF</docChantierUrl>
</data>
</object>
<object>
<name>DocumentChantier</name>
<action>upsert</action>
<data>
<idChantier.numeroRisque>590569/001</idChantier.numeroRisque>
<idLibelle>6</idLibelle>
<compLibelle>028 Contrat DO signé</compLibelle>
<docChantierUrl>028 - CP_DOCNR - CONTRAT SIGNÉ.PDF</docChantierUrl>
</data>
</object> 

h2. Chantier 553145/01

Hermès               010 Liste des intervenants
Hermès               010 Contrat DO CNR signé
Hermès               010 QUESTIONNAIRE
Hermès               010 RICT Avis BCT
Hermès               010 RICT avis n°1
Hermès               010 Mission Maitrise d''oeuvre
Hermès               010 PV de réception
Hermès               010 DGD
Hermès               010 RFCT
Hermès               010 Lettre fin de chantier DO CNR

DbCons                010 Liste des intervenants
DbCons                010 QUESTIONNAIRE
DbCons                010 RICT avis n°1
DbCons                010 Mission CT avenant 2
DbCons                010 Mission Maitrise d''œuvre
DbCons                010 PV de réception
DbCons                010 DGD
DbCons                010 RFCT
DbCons                010 Lettre fin de chantier DO CNR
DbCons                010 Contrat DO CNR signé
',null,51,1,25,4,null,4,1,'2013-05-17 16:30:52','2013-05-21 10:54:26',null,0,null,null,1184,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1185,2,5,'Token','Lister l''absence de token dans les FORMS',null,null,10,8,4,null,10,4,'2013-05-21 09:55:59','2013-05-22 16:56:29',null,100,null,1108,1108,2,3,0,'2013-05-22 16:56:29');
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1186,1,15,'Pagination sous IE9','Vérifier la pagination sous IE9',null,null,1,8,4,null,10,0,'2013-05-21 10:11:24','2013-05-21 10:11:24',null,0,null,null,1186,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1187,2,5,'Génération de fiche CRAC/Sycodes','Ajouter le bouton de génération dans la page Sycodes
Exemple :
http://dev.cpcom.com/dossier/dommages/dommage/modifier/1081/1077/fichesycrac/modifier/2',null,null,1,18,4,null,10,0,'2013-05-21 10:37:07','2013-05-21 10:37:07',null,0,null,1097,923,8,9,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1188,2,2,'Non régression SMABTP','Concernant les tests, il faut s''assurer que les échanges DARVA/SMABTP fonctionnent ne comportent pas de régression.

h2. A tester 

* Réception OM + lecture détail OM
* Rapport Unique, Rapport Préliminaire, Rapport Définitif
* Note d''honoraire
* SD16 Accusé de réception
* SD99 Commentaires',null,73,1,6,5,89,4,0,'2013-05-21 12:22:40','2013-05-21 12:22:40',null,0,48,931,931,34,35,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1189,2,5,'Comprendre MDM','','2013-05-24',null,1,10,3,null,10,4,'2013-05-21 14:16:32','2013-05-21 17:31:24','2013-05-21',0,4,null,1189,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1190,1,5,'BUG Détail, Ajouter un mail au dossier','Quand je clique sur détail pour lire un mail ça m''amène sur une page contenant le context du dossier mais sans aucune information sous le thème défaut alors que je suis sous Chawi',null,null,9,18,4,null,8,3,'2013-05-21 15:48:43','2013-05-22 17:28:18',null,80,null,1108,1108,4,5,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1191,2,6,'Darva AXA Traitement Mission ajouter Role et Fax Partie','lors de la reception de la mission il faut rajouter le role et le fax de la partie dans la table cpxomint','2013-05-28',75,1,17,4,82,17,1,'2013-05-21 16:19:11','2013-05-21 16:20:39','2013-05-21',0,1,902,902,44,45,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1192,2,5,'Revue de code suite a la migration','',null,null,2,8,4,null,10,11,'2013-05-21 17:02:31','2013-05-23 11:57:16',null,0,null,1108,1108,6,11,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1193,2,5,'Envisager le passage a php 5.4','Pour info, il y a un BUG identifié en version de PHP 5.4.6 lors de l''authentification sur de la serialisation de roles.
https://github.com/symfony/symfony/issues/3691
Il faudra se pencher sérieusement sur les solutions proposées après la migration de sf2.3',null,null,1,8,4,null,10,0,'2013-05-21 17:04:07','2013-05-21 17:04:07',null,0,null,1108,1108,12,13,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1194,1,5,'Modifier la vue adresse ','Ajouter une option qui permet d''avoir ou non les boutons ajouter/modifier/supprimer ',null,null,1,8,4,null,8,0,'2013-05-21 17:32:14','2013-05-21 17:32:14',null,0,null,1192,1108,7,8,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1195,2,2,'Recherche d''antériorité compatible avec les 0 non significatifs des contrats AXA','h2. Problème

Axa, en passant par Darva va modifier la nomenclature de ses numéros de police.
Il faut adapter le module d''antériorité pour retrouver les dossiers précédents indépendemment des 0 présents ou absent en début de contrat.

on doit retrouver un dossier sur le contrat 123456S pour un nouveau dossier 000000123456S

Par ailleurs, il faut mettre à jour le numéro de police dans le cas ou l''on trouve une antériorité (dont la différence concerne uniquement des 0 non significatifs)
 ','2013-06-14',73,1,5,6,89,4,0,'2013-05-21 18:09:53','2013-05-21 18:09:53','2013-06-14',0,8,931,931,36,37,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1196,2,6,'DeX: DEX-Dumpsqldex doit intégrer le paramètre --max_allowed_packet=512M','DEX-Dumpsqldex doit intégrer le paramètre --max_allowed_packet=512M',null,31,9,11,4,157,11,1,'2013-05-22 15:31:32','2013-05-22 15:32:39',null,90,null,null,1196,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1197,2,23,'Intégration utilisateurs','Intégration de l''ensemble des utilisateurs AD dans GLPI','2013-05-24',null,1,19,4,null,19,0,'2013-05-22 16:04:38','2013-05-22 16:04:38','2013-05-21',0,16,null,1197,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1198,2,6,'DeX: Les scripts ne doivent plus utliser la table GSICASS_SIRET_AGENCE mais la vue adéquat','Les scripts ne doivent plus utliser la table GSICASS_SIRET_AGENCE mais la vue adéquat',null,31,9,11,4,158,11,0,'2013-05-22 17:16:11','2013-05-22 17:16:11',null,90,null,null,1198,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1199,3,10,'Controle de gestion','Timelog !',null,null,4,4,3,null,4,0,'2013-05-22 17:31:57','2013-05-22 17:31:57',null,0,null,null,1199,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1200,3,2,'Ouvrir le contrat Plateforme','Pour avoir un point d''entrée unique, nous devons ouvrir un contrat darva spécifique.','2013-05-22',73,2,4,4,null,4,1,'2013-05-22 17:34:01','2013-05-22 17:35:02','2013-05-22',0,4,931,931,38,39,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1201,2,5,'Création d''une mission à partir d''un OM','',null,null,1,10,4,null,10,0,'2013-05-23 10:53:34','2013-05-23 10:53:34',null,0,null,null,1201,1,4,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1202,2,5,'Spécification','Maquette Balsamiq',null,null,1,10,4,null,10,0,'2013-05-23 10:54:08','2013-05-23 10:54:08',null,0,null,1201,1201,2,3,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1203,2,24,'Arborescence','Mise en place de l''arborescence défini pour la documentation','2013-05-31',null,1,16,6,159,16,2,'2013-05-23 11:41:00','2013-05-23 13:51:51','2013-05-23',0,4,null,1203,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1204,2,24,'Droits d''accès','Gestion des droits sur l''arborescence','2013-05-31',null,1,16,5,159,16,3,'2013-05-23 11:52:30','2013-05-23 13:51:35','2013-05-23',0,4,null,1204,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1205,2,26,'Déploiement serveur production','Deploiement chez NFrance
','2013-06-07',null,1,13,4,160,4,0,'2013-05-23 12:08:25','2013-05-23 12:08:25',null,0,24,null,1205,1,2,0,null);
insert into `issues`(`id`,`tracker_id`,`project_id`,`subject`,`description`,`due_date`,`category_id`,`status_id`,`assigned_to_id`,`priority_id`,`fixed_version_id`,`author_id`,`lock_version`,`created_on`,`updated_on`,`start_date`,`done_ratio`,`estimated_hours`,`parent_id`,`root_id`,`lft`,`rgt`,`is_private`,`closed_on`) values (1206,2,5,'Refonte formulaire ajouterIntervenant et ajouterIntervenantPol','Refaire les formulaires ajouterIntervenant et ajouterIntervenantPol existants afin d ''assurer la conformité symfony 2 :
Jeton CSRF, classé liée, form_widget...',null,null,7,18,4,null,18,0,'2013-05-23 14:02:29','2013-05-23 14:02:29',null,0,null,1192,1108,9,10,0,null);
