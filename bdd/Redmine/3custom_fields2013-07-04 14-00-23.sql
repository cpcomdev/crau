insert into `custom_fields`(`id`,`type`,`name`,`field_format`,`possible_values`,`regexp`,`min_length`,`max_length`,`is_required`,`is_for_all`,`is_filter`,`position`,`searchable`,`default_value`,`editable`,`visible`,`multiple`) values (1,'UserCustomField','Societe','list','--- 
- CPCOM
- EUREXO
- EURISK
- ICUS
- CITESI
','',0,0,1,0,0,1,0,'CPCOM',1,1,0);
insert into `custom_fields`(`id`,`type`,`name`,`field_format`,`possible_values`,`regexp`,`min_length`,`max_length`,`is_required`,`is_for_all`,`is_filter`,`position`,`searchable`,`default_value`,`editable`,`visible`,`multiple`) values (3,'IssueCustomField','Suivi Tableau de bord','bool','--- []

','',0,0,0,1,1,1,0,'0',1,1,0);
insert into `custom_fields`(`id`,`type`,`name`,`field_format`,`possible_values`,`regexp`,`min_length`,`max_length`,`is_required`,`is_for_all`,`is_filter`,`position`,`searchable`,`default_value`,`editable`,`visible`,`multiple`) values (5,'ProjectCustomField','Bénéficiaire','list','--- 
- Autres
- DSI
- Eurisk
- Eurexo
- Maisoning
- Prunay services
- Tous
','',0,0,1,0,1,1,1,'DSI',1,1,0);
insert into `custom_fields`(`id`,`type`,`name`,`field_format`,`possible_values`,`regexp`,`min_length`,`max_length`,`is_required`,`is_for_all`,`is_filter`,`position`,`searchable`,`default_value`,`editable`,`visible`,`multiple`) values (6,'IssueCustomField','Portefeuille projet','bool',null,'',0,0,0,1,1,2,0,null,1,1,0);
