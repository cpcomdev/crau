CRAU (v1.3)
===========

Cette application permet d�afficher les temps entr�s dans Redmine et GLPI sous forme de tableau de bord par service et par utilisateur.
Une liste des t�ches sera disponible par p�riode avec un hyperlien vers l�application web cible.
De plus, il pr�sente par code couleur les temps susceptible d��tre anormaux 
	- oubli de saisie de temps pour les prestataires (orange)
	- erreur de saisie (rouge) 
	- les weekends sont affich� en gris
	- les cong�s sont affich� en vert (prend en compte les demi journ�e d'absence)
	- une lettre permet de donner le type d'absence ou d'indiquer les journ�es de d�placements


1) Choix de la p�riode
----------------------

Pour le choix de la p�riodes il y a un menu d�roulant permetant de choisir des dates pr�d�finies :
	- Entre : Affiche les temps sur une p�riode personnalis�e.
		Possibilit� d'utiliser le calendrier pour s�lectionner une date (JavaScript doit �tre activ� sur votre navigateur) ou de la taper � la main au format jj/mm/aaaa.
			Erreurs possible : 
				- Date de d�but sup�rieur � la date de fin.
				- Date �crite au mauvais format.

	- Aujourd'hui : Affiche les temps le d�but de la journ�e � maintenant.
	- Hier : Affiche les temps de la journ�e d'hier.
	- Cette semaine : Affiche les temps depuis le d�but de la semaine jusqu'� la date du jour.
	- La semaine derni�re : Affiche les temps dans la semaine pr�c�dente. (S�lection par d�faut)
	- Les 7 derniers jours : Affiche les temps dans les 7 jours pr�c�dents la date du jour.
	- Ce mois-ci : Affiche les temps depuis le d�but du mois jusqu'� la date du jour.
	- Le mois dernier : Affiche les temps dans le mois pr�c�dent le mois actuelle.
	- Les 30 derniers jours : Affiche les temps dans les 30 jours pr�c�dents la date du jour.
	- Cette ann�e : Affiche les temps depuis le d�but de l'ann�e jusqu'� la date du jour.


2) Lecture du r�sultat
----------------------

Ligne utilisateur : Contient les heures total cumul�es des taches pour cette utilisateur � la journ�e et sur la p�riode ainsi que la v�rification du quota d'heure (uniquement par jour) ainsi que les d�placements et les absences ainsi que le types d'absences.
D�tail utilisateur : Contient les heures travaill� sur le d�tail des taches � la journ�e et sur la p�riode.
Total du service : Contient le nombre d'heures travaill� par jour et sur la p�riode du service.
Total : Contient le nombre d'heures travaill� par jour et sur la p�riode pour tous les services.


3) Options
----------

PLIER/DEPLIER
	Possibilit� de plier/d�plier par service ou par utilisateur.
LIENS
	Possibilit� de se diriger directement vers une tache en cliquant dessus, fonctionne pour GLPI et Redmine.



