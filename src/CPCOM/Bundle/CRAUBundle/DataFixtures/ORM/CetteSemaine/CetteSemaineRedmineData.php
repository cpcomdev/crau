<?php
namespace CPCOM\Bundle\CRAUBundle\DataFixtures\ORM\CetteSemaine;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use CPCOM\Bundle\CRAUBundle\Entity\Redmine\Users;
use CPCOM\Bundle\CRAUBundle\Entity\Redmine\GroupsUsers;
use CPCOM\Bundle\CRAUBundle\Entity\Redmine\CustomValues;
use CPCOM\Bundle\CRAUBundle\Entity\Redmine\CustomFields;
use CPCOM\Bundle\CRAUBundle\Entity\Redmine\TimeEntries;
use CPCOM\Bundle\CRAUBundle\Entity\Redmine\Issues;
use CPCOM\Bundle\CRAUBundle\Entity\Redmine\Trackers;

/**
 * Data pour cette semaine 
 */
class CetteSemaineRedmineData implements FixtureInterface
{
    /**
     * @param ObjectManager $manager 
     */
    public function load(ObjectManager $manager)
    {
        $dateTimeNow = new \DateTime('now');
        $dateTimeMois = new \DateTime($dateTimeNow->format('Y-m-01'));

        // Redmine
        // Users
        $redmineUsers = new Users();
        $redmineUsers->setLastname('Lastname 1');
        $redmineUsers->setFirstname('Firstname 1');
        $redmineUsers->setLogin(1);
        $redmineUsers->setHashedPassword(1);
        $redmineUsers->setMail(1);
        $redmineUsers->setAdmin(1);
        $redmineUsers->setStatus(1);
        $redmineUsers->setMailNotification(1);
        $manager->persist($redmineUsers);
        $manager->flush();

        // GroupsUsers
        $redmineGroupsUsers = new GroupsUsers();
        $redmineGroupsUsers->setGroupId(1);
        $redmineGroupsUsers->setUserId(1);
        $manager->persist($redmineGroupsUsers);
        $manager->flush();

        // CustomValues
        $redmineCustomValues = new CustomValues();
        $redmineCustomValues->setValue('CPCOM');
        $redmineCustomValues->setCustomizedType(1);
        $redmineCustomValues->setCustomizedId(1);
        $redmineCustomValues->setCustomFieldId(1);
        $manager->persist($redmineCustomValues);
        $manager->flush();

        // CustomFields
        $redmineCustomFields = new CustomFields();
        $redmineCustomFields->setName('Societe');
        $redmineCustomFields->setType('UserCustomField');
        $redmineCustomFields->setFieldFormat(1);
        $redmineCustomFields->setMinLength(1);
        $redmineCustomFields->setMaxLength(1);
        $redmineCustomFields->setIsRequired(1);
        $redmineCustomFields->setIsForAll(1);
        $redmineCustomFields->setIsFilter(1);
        $redmineCustomFields->setVisible(1);
        $manager->persist($redmineCustomFields);
        $manager->flush();

        // Trackers
        $redmineTrackers = new Trackers();
        $redmineTrackers->setName('Name 1');
        $redmineTrackers->setIsInChlog(1);
        $redmineTrackers->setIsInRoadmap(1);
        $manager->persist($redmineTrackers);
        $manager->flush();

        // Issues
        $redmineIssues = new Issues();
        $redmineIssues->setSubject('Subject 1');
        $redmineIssues->setTrackerId(1);
        $redmineIssues->setProjectId(1);
        $redmineIssues->setStatusId(1);
        $redmineIssues->setPriorityId(1);
        $redmineIssues->setAuthorId(1);
        $redmineIssues->setLockVersion(1);
        $redmineIssues->setDoneRatio(1);
        $redmineIssues->setIsPrivate(1);
        $manager->persist($redmineIssues);
        $manager->flush();

        // TimeEntries (Aujourd'hui)
        $redmineTimeEntries = new TimeEntries();
        $redmineTimeEntries->setHours(1);
        $redmineTimeEntries->setUserId(1);
        $redmineTimeEntries->setIssueId(1);
        $redmineTimeEntries->setSpentOn($dateTimeNow);
        $redmineTimeEntries->setProjectId(1);
        $redmineTimeEntries->setActivityId(1);
        $redmineTimeEntries->setTyear(1);
        $redmineTimeEntries->setTmonth(1);
        $redmineTimeEntries->setTweek(1);
        $redmineTimeEntries->setCreatedOn($dateTimeNow);
        $redmineTimeEntries->setUpdatedOn($dateTimeNow);
        $manager->persist($redmineTimeEntries);
        $manager->flush();
    }
}