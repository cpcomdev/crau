<?php
namespace CPCOM\Bundle\CRAUBundle\DataFixtures\ORM\Controller;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use CPCOM\Bundle\CRAUBundle\Entity\Redmine\Users;
use CPCOM\Bundle\CRAUBundle\Entity\Redmine\GroupsUsers;
use CPCOM\Bundle\CRAUBundle\Entity\Redmine\CustomValues;
use CPCOM\Bundle\CRAUBundle\Entity\Redmine\CustomFields;
use CPCOM\Bundle\CRAUBundle\Entity\Redmine\TimeEntries;
use CPCOM\Bundle\CRAUBundle\Entity\Redmine\Issues;
use CPCOM\Bundle\CRAUBundle\Entity\Redmine\Trackers;

/**
 * Data pour ce mois ci 
 */
class ControllerRedmineData implements FixtureInterface
{
    /**
     * @param ObjectManager $manager 
     */
    public function load(ObjectManager $manager)
    {
        $dateTimeNow = new \DateTime('now');
        $dateTimeMois = new \DateTime($dateTimeNow->format('Y-m-01'));

        // Redmine
        // Users
        $redmineUsers = new Users();
        $redmineUsers->setLastname('Lastname1');
        $redmineUsers->setFirstname('Firstname1');
        $redmineUsers->setLogin(1);
        $redmineUsers->setHashedPassword(1);
        $redmineUsers->setMail(1);
        $redmineUsers->setAdmin(1);
        $redmineUsers->setStatus(1);
        $redmineUsers->setMailNotification(1);
        $manager->persist($redmineUsers);
        $manager->flush();

        // Users
        $redmineUsers2 = new Users();
        $redmineUsers2->setLastname('Lastname2');
        $redmineUsers2->setFirstname('Firstname2');
        $redmineUsers2->setLogin(2);
        $redmineUsers2->setHashedPassword(1);
        $redmineUsers2->setMail(1);
        $redmineUsers2->setAdmin(1);
        $redmineUsers2->setStatus(1);
        $redmineUsers2->setMailNotification(1);
        $manager->persist($redmineUsers2);
        $manager->flush();

        // GroupsUsers
        $redmineGroupsUsers = new GroupsUsers();
        $redmineGroupsUsers->setGroupId(1);
        $redmineGroupsUsers->setUserId(1);
        $manager->persist($redmineGroupsUsers);
        $manager->flush();

        // GroupsUsers
        $redmineGroupsUsers2 = new GroupsUsers();
        $redmineGroupsUsers2->setGroupId(2);
        $redmineGroupsUsers2->setUserId(2);
        $manager->persist($redmineGroupsUsers2);
        $manager->flush();

        // CustomValues
        $redmineCustomValues = new CustomValues();
        $redmineCustomValues->setValue('CPCOM');
        $redmineCustomValues->setCustomizedType(1);
        $redmineCustomValues->setCustomizedId(1);
        $redmineCustomValues->setCustomFieldId(1);
        $manager->persist($redmineCustomValues);
        $manager->flush();

        // CustomValues
        $redmineCustomValues2 = new CustomValues();
        $redmineCustomValues2->setValue('CPCOM');
        $redmineCustomValues2->setCustomizedType(1);
        $redmineCustomValues2->setCustomizedId(2);
        $redmineCustomValues2->setCustomFieldId(1);
        $manager->persist($redmineCustomValues2);
        $manager->flush();

        // CustomFields
        $redmineCustomFields = new CustomFields();
        $redmineCustomFields->setName('Societe');
        $redmineCustomFields->setType('UserCustomField');
        $redmineCustomFields->setFieldFormat(1);
        $redmineCustomFields->setMinLength(1);
        $redmineCustomFields->setMaxLength(1);
        $redmineCustomFields->setIsRequired(1);
        $redmineCustomFields->setIsForAll(1);
        $redmineCustomFields->setIsFilter(1);
        $redmineCustomFields->setVisible(1);
        $manager->persist($redmineCustomFields);
        $manager->flush();

        // Trackers
        $redmineTrackers = new Trackers();
        $redmineTrackers->setName('Name 1');
        $redmineTrackers->setIsInChlog(1);
        $redmineTrackers->setIsInRoadmap(1);
        $manager->persist($redmineTrackers);
        $manager->flush();

        // Trackers
        $redmineTrackers2 = new Trackers();
        $redmineTrackers2->setName('Name 2');
        $redmineTrackers2->setIsInChlog(2);
        $redmineTrackers2->setIsInRoadmap(2);
        $manager->persist($redmineTrackers2);
        $manager->flush();

        // Issues
        $redmineIssues = new Issues();
        $redmineIssues->setSubject('Subject 1');
        $redmineIssues->setTrackerId(1);
        $redmineIssues->setProjectId(1);
        $redmineIssues->setStatusId(1);
        $redmineIssues->setPriorityId(1);
        $redmineIssues->setAuthorId(1);
        $redmineIssues->setLockVersion(1);
        $redmineIssues->setDoneRatio(1);
        $redmineIssues->setIsPrivate(1);
        $manager->persist($redmineIssues);
        $manager->flush();

        // TimeEntries (Aujourd'hui)
        $redmineTimeEntries = new TimeEntries();
        $redmineTimeEntries->setHours(1);
        $redmineTimeEntries->setUserId(1);
        $redmineTimeEntries->setIssueId(1);
        $redmineTimeEntries->setSpentOn($dateTimeNow);
        $redmineTimeEntries->setProjectId(1);
        $redmineTimeEntries->setActivityId(1);
        $redmineTimeEntries->setTyear(1);
        $redmineTimeEntries->setTmonth(1);
        $redmineTimeEntries->setTweek(1);
        $redmineTimeEntries->setCreatedOn($dateTimeNow);
        $redmineTimeEntries->setUpdatedOn($dateTimeNow);
        $manager->persist($redmineTimeEntries);
        $manager->flush();

        // Issues
        $redmineIssues2 = new Issues();
        $redmineIssues2->setSubject('Subject 2');
        $redmineIssues2->setTrackerId(2);
        $redmineIssues2->setProjectId(1);
        $redmineIssues2->setStatusId(1);
        $redmineIssues2->setPriorityId(1);
        $redmineIssues2->setAuthorId(2);
        $redmineIssues2->setLockVersion(1);
        $redmineIssues2->setDoneRatio(1);
        $redmineIssues2->setIsPrivate(1);
        $manager->persist($redmineIssues2);
        $manager->flush();

        // TimeEntries (Mois)
        $redmineTimeEntries2 = new TimeEntries();
        $redmineTimeEntries2->setHours(1);
        $redmineTimeEntries2->setUserId(2);
        $redmineTimeEntries2->setIssueId(2);
        $redmineTimeEntries2->setSpentOn($dateTimeNow);
        $redmineTimeEntries2->setProjectId(2);
        $redmineTimeEntries2->setActivityId(2);
        $redmineTimeEntries2->setTyear(1);
        $redmineTimeEntries2->setTmonth(1);
        $redmineTimeEntries2->setTweek(1);
        $redmineTimeEntries2->setCreatedOn($dateTimeNow);
        $redmineTimeEntries2->setUpdatedOn($dateTimeNow);
        $manager->persist($redmineTimeEntries2);
        $manager->flush();
    }
}