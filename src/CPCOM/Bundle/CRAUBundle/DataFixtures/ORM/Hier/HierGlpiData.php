<?php
namespace CPCOM\Bundle\CRAUBundle\DataFixtures\ORM\Hier;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use CPCOM\Bundle\CRAUBundle\Entity\Glpi\GlpiLocations;
use CPCOM\Bundle\CRAUBundle\Entity\Glpi\GlpiUsers;
use CPCOM\Bundle\CRAUBundle\Entity\Glpi\GlpiTickettasks;
use CPCOM\Bundle\CRAUBundle\Entity\Glpi\GlpiTickets;

/**
 * Data pour aujourd'hui
 */
class HierGlpiData implements FixtureInterface
{
    /**
     * @param ObjectManager $manager 
     */
    public function load(ObjectManager $manager)
    {
        $dateTimeNow = new \DateTime('now');
        $dateTimeHier = new \DateTime(\date('Y-m-d', strtotime("last day", strtotime($dateTimeNow->format('Y-m-d')))));

        // GLPI
        // Users
        $glpiUsers = new GlpiUsers();
        $glpiUsers->setRealname('Realname 1');
        $glpiUsers->setFirstname('Firstname 1');
        $glpiUsers->setLocationsId(1);
        $glpiUsers->setUseMode(1);
        $glpiUsers->setIsActive(1);
        $glpiUsers->setAuthsId(1);
        $glpiUsers->setAuthtype(1);
        $glpiUsers->setIsDeleted(0);
        $glpiUsers->setProfilesId(1);
        $glpiUsers->setEntitiesId(1);
        $glpiUsers->setUsertitlesId(1);
        $glpiUsers->setUsercategoriesId(1);
        $manager->persist($glpiUsers);
        $manager->flush();

        // Tickets
        $glpiTickets = new GlpiTickets();
        $glpiTickets->setName('Name 1');
        $glpiTickets->setEntitiesId(1);
        $glpiTickets->setUsersIdLastupdater(1);
        $glpiTickets->setUsersIdRecipient(1);
        $glpiTickets->setRequesttypesId(1);
        $glpiTickets->setSuppliersIdAssign(1);
        $glpiTickets->setItemtype(1);
        $glpiTickets->setItemsId(1);
        $glpiTickets->setUrgency(1);
        $glpiTickets->setImpact(1);
        $glpiTickets->setPriority(1);
        $glpiTickets->setItilcategoriesId(1);
        $glpiTickets->setType(1);
        $glpiTickets->setCostFixed(1);
        $glpiTickets->setCostTime(1);
        $glpiTickets->setCostMaterial(1);
        $glpiTickets->setSolutiontypesId(1);
        $glpiTickets->setSlasId(1);
        $glpiTickets->setSlalevelsId(1);
        $glpiTickets->setSlaWaitingDuration(1);
        $glpiTickets->setWaitingDuration(1);
        $glpiTickets->setCloseDelayStat(1);
        $glpiTickets->setSolveDelayStat(1);
        $glpiTickets->setTakeintoaccountDelayStat(1);
        $glpiTickets->setActiontime(1);
        $glpiTickets->setIsDeleted(0);
        $glpiTickets->setDate($dateTimeNow);
        $manager->persist($glpiTickets);
        $manager->flush();

        // TicketTasks (Aujourd'hui)
        $glpiTickettasks = new GlpiTickettasks();
        $glpiTickettasks->setActiontime($dateTimeNow->getTimestamp());
        $glpiTickettasks->setDate($dateTimeNow);
        $glpiTickettasks->setUsersId(1);
        $glpiTickettasks->setTicketsId(1);
        $glpiTickettasks->setTaskcategoriesId(1);
        $glpiTickettasks->setIsPrivate(1);
        $glpiTickettasks->setState(1);
        $glpiTickettasks->setUsersIdTech(1);
        $manager->persist($glpiTickettasks);
        $manager->flush();

        // Tickets
        $glpiTickets2 = new GlpiTickets();
        $glpiTickets2->setName('Name 2');
        $glpiTickets2->setEntitiesId(1);
        $glpiTickets2->setUsersIdLastupdater(1);
        $glpiTickets2->setUsersIdRecipient(1);
        $glpiTickets2->setRequesttypesId(1);
        $glpiTickets2->setSuppliersIdAssign(1);
        $glpiTickets2->setItemtype(1);
        $glpiTickets2->setItemsId(1);
        $glpiTickets2->setUrgency(1);
        $glpiTickets2->setImpact(1);
        $glpiTickets2->setPriority(1);
        $glpiTickets2->setItilcategoriesId(1);
        $glpiTickets2->setType(1);
        $glpiTickets2->setCostFixed(1);
        $glpiTickets2->setCostTime(1);
        $glpiTickets2->setCostMaterial(1);
        $glpiTickets2->setSolutiontypesId(1);
        $glpiTickets2->setSlasId(1);
        $glpiTickets2->setSlalevelsId(1);
        $glpiTickets2->setSlaWaitingDuration(1);
        $glpiTickets2->setWaitingDuration(1);
        $glpiTickets2->setCloseDelayStat(1);
        $glpiTickets2->setSolveDelayStat(1);
        $glpiTickets2->setTakeintoaccountDelayStat(1);
        $glpiTickets2->setActiontime(1);
        $glpiTickets2->setIsDeleted(0);
        $glpiTickets2->setDate($dateTimeHier);
        $manager->persist($glpiTickets2);
        $manager->flush();

        // TicketTasks (Le Mois dernier)
        $glpiTickettasks2 = new GlpiTickettasks();
        $glpiTickettasks2->setActiontime($dateTimeHier->getTimestamp());
        $glpiTickettasks2->setDate($dateTimeHier);
        $glpiTickettasks2->setUsersId(1);
        $glpiTickettasks2->setTicketsId(2);
        $glpiTickettasks2->setTaskcategoriesId(1);
        $glpiTickettasks2->setIsPrivate(1);
        $glpiTickettasks2->setState(1);
        $glpiTickettasks2->setUsersIdTech(1);
        $manager->persist($glpiTickettasks2);
        $manager->flush();
    }
}