<?php
namespace CPCOM\Bundle\CRAUBundle\Form\CRAU;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

/**
 * Formulaire index 
 */
class CRAUType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * 
     * @return type 
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('datePredefini', 'choice', array(
            'choices'         => array(
                '' => 'Entre',
                1 => 'Aujourd\'hui',
                2 => 'Hier',
                3 => 'Cette semaine',
                4 => 'La semaine dernière',
                5 => 'Les 7 derniers jours',
                6 => 'Ce mois-ci',
                7 => 'Le mois dernier',
                8 => 'Les 30 derniers jours',
                9 => 'Cette année'
            ),
            'required'  => false,
            'label'  => 'Dates',
            'data' => 3
        ));

        $builder->add('dateDebut', 'date', array(
            'label'  => 'Du',
            'input'  => 'datetime',
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'required'  => false
        ));
        $builder->add('dateFin', 'date', array(
            'label'  => 'au',
            'input'  => 'datetime',
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'required'  => false
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     * 
     * @return type 
     */
    public function getDefaultOptions(OptionsResolverInterface $resolver)
    {
        return array(
            'data_class'        => 'CPCOM\CRAUBundle\Entity\CRAU',
            'csrf_protection'   => true,
            'csrf_field_name'   => '_token',
        );
    }

    /**
     * @return string 
     */
    public function getName()
    {
        return 'CRAU';
    }
}
