<?php

namespace CPCOM\Bundle\CRAUBundle\Twig\Extension;

use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Bundle\TwigBundle\Loader\FilesystemLoader;
/**
 * Extension Twig 
 */
class TwigExtension extends \Twig_Extension
{
    /**
    * @return type 
    */
    public function getFunctions()
    {
        return array(
            'is_weekend' => new \Twig_Function_Method($this, 'is_weekend', array('is_safe' => array('html'))),
            'is_ferie' => new \Twig_Function_Method($this, 'is_ferie', array('is_safe' => array('html'))),
            'type_conge' => new \Twig_Function_Method($this, 'type_conge', array('is_safe' => array('html'))),
            'substr' => new \Twig_Function_Method($this, 'substr', array('is_safe' => array('html'))),
            'newDateTimeCreateFromFormat' => new \Twig_Function_Method($this, 'newDateTimeCreateFromFormat', array('is_safe' => array('html'))),
            'newDateTime' => new \Twig_Function_Method($this, 'newDateTime', array('is_safe' => array('html')))
        );
    }
    /**
     * @param String $dateString
     * 
     * @return boolean 
     */
    public function is_weekend($dateString)
    {
        $dateTime = \DateTime::createFromFormat('d/m/Y', $dateString);
        if ($dateTime) {
            if ($dateTime->format('w') == 0 || $dateTime->format('w') == 6) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }
    /**
     * @param String $dateString
     * 
     * @return boolean 
     */
    public function is_ferie($dateString)
    {
        $dateTime = \DateTime::createFromFormat('d/m/Y', $dateString);
        if ($dateTime) {
            $easterDate  = \easter_date($dateTime->format('Y'));
            $easterDay   = \date('j', $easterDate);
            $easterMonth = \date('n', $easterDate);
            $easterYear   = \date('Y', $easterDate);

            $holidays = array(
                // Dates fixes
                \mktime(0, 0, 0, 1, 1, $dateTime->format('Y')),  // 1er janvier
                \mktime(0, 0, 0, 5, 1, $dateTime->format('Y')),  // Fête du travail
                \mktime(0, 0, 0, 5, 8, $dateTime->format('Y')),  // Victoire des alliés
                \mktime(0, 0, 0, 7, 14, $dateTime->format('Y')),  // Fête nationale
                \mktime(0, 0, 0, 8, 15, $dateTime->format('Y')),  // Assomption
                \mktime(0, 0, 0, 11, 1, $dateTime->format('Y')),  // Toussaint
                \mktime(0, 0, 0, 11, 11, $dateTime->format('Y')),  // Armistice
                \mktime(0, 0, 0, 12, 25, $dateTime->format('Y')),  // Noel

                // Dates variables
                \mktime(0, 0, 0, $easterMonth, $easterDay + 2, $easterYear), //paques
                \mktime(0, 0, 0, $easterMonth, $easterDay + 39, $easterYear),  //ascencion
                \mktime(0, 0, 0, $easterMonth, $easterDay + 50, $easterYear), //pencote
            );

            \sort($holidays);

            if (\in_array(\mktime(0, 0, 0, $dateTime->format('m'), $dateTime->format('d'), $dateTime->format('Y')), $holidays)) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }
    /**
     * @param String $type
     *
     * @return string Type de congés pour style
     */
    public function type_conge($type)
    {
        if (\mb_substr($type, 0, 3, 'UTF-8') == 'RTT') {

            return 'Rtt';
        } elseif(\mb_substr($type, 0, 5, 'UTF-8') == 'Congé') {

            return 'Conge';
        } elseif(\mb_substr($type, 0, 11, 'UTF-8') == 'Déplacement') {

            return 'Deplacement';
        } elseif(\mb_substr($type, 0, 7, 'UTF-8') == 'Maladie') {

            return 'Maladie';
        }

        return 'Autre';
    }
    /**
     * @param String $chaine Chaine de caractere
     * @param String $start  Début du tronquage
     * @param String $lenght Fin du tronquage
     *
     * @return string Chaine tronqué
     */
    public function substr($chaine, $start, $lenght)
    {
        return \substr($chaine, $start, $lenght);
    }
    /**
     * @param String $format Format de la date pris en compte
     * @param String $time   Date
     *
     * @return string Date au format demandé
     */
    public function newDateTimeCreateFromFormat($format , $time)
    {
        return \DateTime::createFromFormat($format, $time);
    }
    /**
     * @param String $date Date
     *
     * @return string The extension name
     */
    public function newDateTime($date)
    {
        return new \DateTime($date);
    }
    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'twigExtension';
    }
}
