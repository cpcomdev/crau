<?php

namespace CPCOM\Bundle\CRAUBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use CPCOM\Bundle\CRAUBundle\Model\GlpiRequest;
use CPCOM\Bundle\CRAUBundle\Model\RedmineRequest;
use CPCOM\Bundle\CRAUBundle\Form\CRAU\CRAUType;
use CPCOM\Bundle\CRAUBundle\Entity\CRAU;

/**
 * DefaultController
 */
class DefaultController extends Controller
{
    /**
     * @Route("/index", name="index")
     * @Template()
     * 
     * @return \Symfony\Component\HttpFoundation\RedirectResponse | RenderView
     */
    public function indexAction()
    {
        /** Appel entity manager Redmine et Glpi */
        $emRedmine = $this->get('doctrine')->getManager('redmine');
        $emGlpi = $this->get('doctrine')->getManager('glpi');
        /** Création du formulaire */
        $crau = new CRAU();
        $formCRAU = $this->get('form.factory')->create(new CRAUType(), $crau);
        /** Validation du formulaire */
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $formCRAU->bind($request);
            if (!$formCRAU->isValid()) {
                $crau->setDatePredefini(3);
            }
        } else {
            $crau->setDatePredefini(3);
        }
        /** Appel des models Redmine et Glpi */
        $redmineRequest = new RedmineRequest($emRedmine);
        $glpiRequest = new GlpiRequest($emGlpi);
        $allUsers = $redmineRequest->findAllUsers();
        $redmineElapsedTime = $redmineRequest->findElapsedTime($crau);
        $glpiElapsedTime = $glpiRequest->findElapsedTime($crau);
        $result = $crau->resultFusion($allUsers, $redmineElapsedTime, $glpiElapsedTime);

        return $this->render('CPCOMCRAUBundle:Default:resultatTabulaire.html.twig', array(
            'formCRAU'   => $formCRAU->createView(),
            'crau'       => $crau,
            'result'     => $result
        ));
    }
}
