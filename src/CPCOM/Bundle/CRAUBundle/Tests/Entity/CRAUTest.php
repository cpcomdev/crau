<?php

namespace CPCOM\Bundle\CRAUBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;

use Doctrine\Common\DataFixtures\Loader;
use CPCOM\Bundle\CRAUBundle\DataFixtures\ORM\Controller\ControllerGlpiData;
use CPCOM\Bundle\CRAUBundle\DataFixtures\ORM\Controller\ControllerRedmineData;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

use CPCOM\Bundle\CRAUBundle\Entity\CRAU;
use CPCOM\Bundle\CRAUBundle\Model\GlpiRequest;
use CPCOM\Bundle\CRAUBundle\Model\RedmineRequest;

/**
 * Description of DateParticuliereTest
 *
 * @author David Bouvencourt <david.bouvencourt@groupeprunay-si.fr>
 */
class CRAUTest extends WebTestCase
{
    private $emRedmine;
    private $emGlpi;
    private $session;

    protected function setUp()
    {
        //Création de l'em
        $kernelNameClass = $this->getKernelClass(); // Récupération du nom de la classe qui sert de kernel
        $kernel = new $kernelNameClass('test', true); // Instanciation de la classe et exécution du kernel dans un environnement de test avec débogage
        $kernel->boot(); // On boot le kernel

        $this->emRedmine = $kernel->getContainer()->get('doctrine')->getManager('redmine_test'); // On récupère entity manager
        $this->emGlpi = $kernel->getContainer()->get('doctrine')->getManager('glpi_test'); // On récupère entity manager
        $this->session = $kernel->getContainer()->get('session');

        // Redmine
        $dbh = new \PDO('mysql:host=localhost;dbname='.$kernel->getContainer()->getParameter('database_name_redmine_test').';port='.$kernel->getContainer()->getParameter('database_port_redmine'), $kernel->getContainer()->getParameter('database_user_redmine_test'), $kernel->getContainer()->getParameter('database_password_redmine_test'));

        $dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $query = $dbh->exec("SET foreign_key_checks = 0;");
        $sql = "SHOW TABLES FROM ".$kernel->getContainer()->getParameter('database_name_redmine_test');
        foreach ($dbh->query($sql) as $row) {
           $query = $dbh->exec("TRUNCATE TABLE ".$row[0]."");
        }
        $query = $dbh->exec("SET foreign_key_checks = 1;");
        // GLPI
        $dbh = new \PDO('mysql:host=localhost;dbname='.$kernel->getContainer()->getParameter('database_name_glpi_test').';port='.$kernel->getContainer()->getParameter('database_port_glpi'), $kernel->getContainer()->getParameter('database_user_glpi_test'), $kernel->getContainer()->getParameter('database_password_glpi_test'));

        $dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $query = $dbh->exec("SET foreign_key_checks = 0;");
        $sql = "SHOW TABLES FROM ".$kernel->getContainer()->getParameter('database_name_glpi_test');
        foreach ($dbh->query($sql) as $row) {
           $query = $dbh->exec("TRUNCATE TABLE ".$row[0]."");
        }
        $query = $dbh->exec("SET foreign_key_checks = 1;");

        //Création de la BDD test
        $loader = new Loader();
        $loader->addFixture(new ControllerRedmineData());

        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->emRedmine, $purger);
        $executor->execute($loader->getFixtures());

        //Création de la BDD test
        $loader = new Loader();
        $loader->addFixture(new ControllerGlpiData());

        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->emGlpi, $purger);
        $executor->execute($loader->getFixtures());
    }

    /**
     * Test ResultFusion
     */
    public function testResultFusion()
    {
        $crau = new CRAU();
        $crau->setDatePredefini(1);

        $glpiRequest = new GlpiRequest($this->emGlpi);
        $glpiResponse = $glpiRequest->findElapsedTime($crau);

        $redmineRequest = new RedmineRequest($this->emRedmine);
        $allUsers = $redmineRequest->findAllUsers();
        $redmineResponse = $redmineRequest->findElapsedTime($crau);

        $result = $crau->resultFusion($allUsers, $redmineResponse, $glpiResponse);
        $dateTimeNow = new \DateTime('now');

        // HEAD
        $this->assertEquals('Utilisateurs', $result['head'][0]);
        $this->assertEquals($dateTimeNow->format('d/m'), $result['head'][1]);
        $this->assertEquals('Total', $result['head'][2]);

        // BODY
        // User 1
        // Redmine
        $this->assertEquals('Name 1 #1 Subject 1', $result['body']['Lastname1']['lastname1_firstname1']['tacheRedmine'][1]['nom']);
        $this->assertEquals(array($dateTimeNow->format('d/m/Y')=>1), $result['body']['Lastname1']['lastname1_firstname1']['tacheRedmine'][1]['date']);
        $this->assertEquals(1, $result['body']['Lastname1']['lastname1_firstname1']['tacheRedmine'][1]['total']);
        // GLPI
        $this->assertEquals('Incident #1 Name 1', $result['body']['Lastname1']['lastname1_firstname1']['tacheGLPI'][1]['nom']);
        $this->assertEquals(array($dateTimeNow->format('d/m/Y')=>1), $result['body']['Lastname1']['lastname1_firstname1']['tacheGLPI'][1]['date']);
        $this->assertEquals(1, $result['body']['Lastname1']['lastname1_firstname1']['tacheGLPI'][1]['total']);
        $this->assertEquals('Incident #2 Name 2', $result['body']['Lastname1']['lastname1_firstname1']['tacheGLPI'][2]['nom']);
        $this->assertEquals(array($dateTimeNow->format('d/m/Y')=>1), $result['body']['Lastname1']['lastname1_firstname1']['tacheGLPI'][2]['date']);
        $this->assertEquals(1, $result['body']['Lastname1']['lastname1_firstname1']['tacheGLPI'][2]['total']);
        // Total
        $this->assertEquals(3, $result['body']['Lastname1']['lastname1_firstname1']['totalRedmineGLPI'][$dateTimeNow->format('d/m/Y')]['heures']);
        $this->assertEquals(3, $result['body']['Lastname1']['lastname1_firstname1']['totalRedmineGLPI']['total']['heures']);
        // User 2
        // Redmine
        $this->assertEquals('Name 2 #2 Subject 2', $result['body']['Lastname2']['lastname2_firstname2']['tacheRedmine'][2]['nom']);
        $this->assertEquals(array($dateTimeNow->format('d/m/Y')=>1), $result['body']['Lastname2']['lastname2_firstname2']['tacheRedmine'][2]['date']);
        $this->assertEquals(1, $result['body']['Lastname2']['lastname2_firstname2']['tacheRedmine'][2]['total']);
        // TotalService
        $this->assertEquals('Total', $result['body']['Lastname1']['totalService']['redminePrenom']);
        $this->assertEquals('Lastname1', $result['body']['Lastname1']['totalService']['redmineNom']);
        $this->assertEquals(3, $result['body']['Lastname1']['totalService'][$dateTimeNow->format('d/m/Y')]);
        $this->assertEquals(3, $result['body']['Lastname1']['totalService']['total']);

        $this->assertEquals('Total', $result['body']['Lastname2']['totalService']['redminePrenom']);
        $this->assertEquals('Lastname2', $result['body']['Lastname2']['totalService']['redmineNom']);
        $this->assertEquals(1, $result['body']['Lastname2']['totalService'][$dateTimeNow->format('d/m/Y')]);
        $this->assertEquals(1, $result['body']['Lastname2']['totalService']['total']);

        // Total
        $this->assertEquals(1, $result['body']['Lastname2']['lastname2_firstname2']['totalRedmineGLPI'][$dateTimeNow->format('d/m/Y')]['heures']);
        $this->assertEquals(1, $result['body']['Lastname2']['lastname2_firstname2']['totalRedmineGLPI']['total']['heures']);

        // FOOT
        $this->assertEquals('Total', $result['foot'][0]);
        $this->assertEquals(4, $result['foot'][1]);
        $this->assertEquals(4, $result['foot'][2]);
    }
}

