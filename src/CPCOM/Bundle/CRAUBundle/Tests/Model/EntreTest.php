<?php

namespace CPCOM\Bundle\CRAUBundle\Tests\Model;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;

use Doctrine\Common\DataFixtures\Loader;
use CPCOM\Bundle\CRAUBundle\DataFixtures\ORM\Entre\EntreGlpiData;
use CPCOM\Bundle\CRAUBundle\DataFixtures\ORM\Entre\EntreRedmineData;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

use CPCOM\Bundle\CRAUBundle\Entity\CRAU;
use CPCOM\Bundle\CRAUBundle\Model\GlpiRequest;
use CPCOM\Bundle\CRAUBundle\Model\RedmineRequest;

/**
 * Description of DateParticuliereTest
 *
 * @author David Bouvencourt <david.bouvencourt@groupeprunay-si.fr>
 */
class EntreTest extends WebTestCase
{
    private $emRedmine;
    private $emGlpi;
    private $session;

    protected function setUp()
    {
        //Création de l'em
        $kernelNameClass = $this->getKernelClass(); // Récupération du nom de la classe qui sert de kernel
        $kernel = new $kernelNameClass('test', true); // Instanciation de la classe et exécution du kernel dans un environnement de test avec débogage
        $kernel->boot(); // On boot le kernel

        $this->emRedmine = $kernel->getContainer()->get('doctrine')->getManager('redmine_test'); // On récupère entity manager
        $this->emGlpi = $kernel->getContainer()->get('doctrine')->getManager('glpi_test'); // On récupère entity manager
        $this->session = $kernel->getContainer()->get('session');

        // Redmine
        $dbh = new \PDO('mysql:host=localhost;dbname='.$kernel->getContainer()->getParameter('database_name_redmine_test').';port='.$kernel->getContainer()->getParameter('database_port_redmine'), $kernel->getContainer()->getParameter('database_user_redmine_test'), $kernel->getContainer()->getParameter('database_password_redmine_test'));

        $dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $query = $dbh->exec("SET foreign_key_checks = 0;");
        $sql = "SHOW TABLES FROM ".$kernel->getContainer()->getParameter('database_name_redmine_test');
        foreach ($dbh->query($sql) as $row) {
           $query = $dbh->exec("TRUNCATE TABLE ".$row[0]."");
        }
        $query = $dbh->exec("SET foreign_key_checks = 1;");
        // GLPI
        $dbh = new \PDO('mysql:host=localhost;dbname='.$kernel->getContainer()->getParameter('database_name_glpi_test').';port='.$kernel->getContainer()->getParameter('database_port_glpi'), $kernel->getContainer()->getParameter('database_user_glpi_test'), $kernel->getContainer()->getParameter('database_password_glpi_test'));

        $dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $query = $dbh->exec("SET foreign_key_checks = 0;");
        $sql = "SHOW TABLES FROM ".$kernel->getContainer()->getParameter('database_name_glpi_test');
        foreach ($dbh->query($sql) as $row) {
           $query = $dbh->exec("TRUNCATE TABLE ".$row[0]."");
        }
        $query = $dbh->exec("SET foreign_key_checks = 1;");

        //Création de la BDD test
        $loader = new Loader();
        $loader->addFixture(new EntreRedmineData());

        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->emRedmine, $purger);
        $executor->execute($loader->getFixtures());

        //Création de la BDD test
        $loader = new Loader();
        $loader->addFixture(new EntreGlpiData());

        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->emGlpi, $purger);
        $executor->execute($loader->getFixtures());
    }

    /**
     * Test Entre
     */
    public function testEntreOK()
    {
        $dateTimeNow = new \DateTime('now');
        $dateTimeEntre = new \DateTime(\date('Y-m-d', strtotime("-20 day", strtotime($dateTimeNow->format('Y-m-d')))));

        $crau = new CRAU();
        // Entre
        $crau->setDatePredefini(null);
        $crau->setDateDebut($dateTimeEntre->format('Y-m-d'));
        $crau->setDateFin($dateTimeNow->format('Y-m-d'));

        $glpiRequest = new GlpiRequest($this->emGlpi);
        $glpiResponse = $glpiRequest->findElapsedTime($crau);

        $redmineRequest = new RedmineRequest($this->emRedmine);
        $redmineResponse = $redmineRequest->findElapsedTime($crau);

        $this->assertEquals(2, count($glpiResponse));
        $this->assertEquals(2, count($redmineResponse));
    }
}

