<?php

namespace CPCOM\Bundle\CRAUBundle\Tests\Model;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;

use Doctrine\Common\DataFixtures\Loader;
use CPCOM\Bundle\CRAUBundle\DataFixtures\ORM\LeMoisDernier\LeMoisDernierGlpiData;
use CPCOM\Bundle\CRAUBundle\DataFixtures\ORM\LeMoisDernier\LeMoisDernierRedmineData;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

use CPCOM\Bundle\CRAUBundle\Entity\CRAU;
use CPCOM\Bundle\CRAUBundle\Model\GlpiRequest;
use CPCOM\Bundle\CRAUBundle\Model\RedmineRequest;

/**
 * Description of DateParticuliereTest
 *
 * @author David Bouvencourt <david.bouvencourt@groupeprunay-si.fr>
 */
class LeMoisDernierTest extends WebTestCase
{
    private $emRedmine;
    private $emGlpi;
    private $session;

    protected function setUp()
    {
        //Création de l'em
        $kernelNameClass = $this->getKernelClass(); // Récupération du nom de la classe qui sert de kernel
        $kernel = new $kernelNameClass('test', true); // Instanciation de la classe et exécution du kernel dans un environnement de test avec débogage
        $kernel->boot(); // On boot le kernel

        $this->emRedmine = $kernel->getContainer()->get('doctrine')->getManager('redmine_test'); // On récupère entity manager
        $this->emGlpi = $kernel->getContainer()->get('doctrine')->getManager('glpi_test'); // On récupère entity manager
        $this->session = $kernel->getContainer()->get('session');

        // Redmine
        $dbh = new \PDO('mysql:host=localhost;dbname='.$kernel->getContainer()->getParameter('database_name_redmine_test').';port='.$kernel->getContainer()->getParameter('database_port_redmine'), $kernel->getContainer()->getParameter('database_user_redmine_test'), $kernel->getContainer()->getParameter('database_password_redmine_test'));

        $dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $query = $dbh->exec("SET foreign_key_checks = 0;");
        $sql = "SHOW TABLES FROM ".$kernel->getContainer()->getParameter('database_name_redmine_test');
        foreach ($dbh->query($sql) as $row) {
           $query = $dbh->exec("TRUNCATE TABLE ".$row[0]."");
        }
        $query = $dbh->exec("SET foreign_key_checks = 1;");
        // GLPI
        $dbh = new \PDO('mysql:host=localhost;dbname='.$kernel->getContainer()->getParameter('database_name_glpi_test').';port='.$kernel->getContainer()->getParameter('database_port_glpi'), $kernel->getContainer()->getParameter('database_user_glpi_test'), $kernel->getContainer()->getParameter('database_password_glpi_test'));

        $dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $query = $dbh->exec("SET foreign_key_checks = 0;");
        $sql = "SHOW TABLES FROM ".$kernel->getContainer()->getParameter('database_name_glpi_test');
        foreach ($dbh->query($sql) as $row) {
           $query = $dbh->exec("TRUNCATE TABLE ".$row[0]."");
        }
        $query = $dbh->exec("SET foreign_key_checks = 1;");

        //Création de la BDD test
        $loader = new Loader();
        $loader->addFixture(new LeMoisDernierRedmineData());

        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->emRedmine, $purger);
        $executor->execute($loader->getFixtures());

        //Création de la BDD test
        $loader = new Loader();
        $loader->addFixture(new LeMoisDernierGlpiData());

        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->emGlpi, $purger);
        $executor->execute($loader->getFixtures());
    }

    /**
     * test le mois dernier 
     */
    public function testLeMoisDernierOK()
    {
        $crau = new CRAU();
        // Le mois dernier
        $crau->setDatePredefini(7);

        $glpiRequest = new GlpiRequest($this->emGlpi);
        $glpiResponse = $glpiRequest->findElapsedTime($crau);

        $redmineRequest = new RedmineRequest($this->emRedmine);
        $redmineResponse = $redmineRequest->findElapsedTime($crau);

        $this->assertEquals(1, count($glpiResponse));
        $this->assertEquals(1, count($redmineResponse));
    }
}

