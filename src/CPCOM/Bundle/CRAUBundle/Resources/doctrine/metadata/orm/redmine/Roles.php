<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Roles
 *
 * @ORM\Table(name="roles")
 * @ORM\Entity
 */
class Roles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var boolean
     *
     * @ORM\Column(name="assignable", type="boolean", nullable=true)
     */
    private $assignable;

    /**
     * @var integer
     *
     * @ORM\Column(name="builtin", type="integer", nullable=false)
     */
    private $builtin;

    /**
     * @var string
     *
     * @ORM\Column(name="permissions", type="text", nullable=true)
     */
    private $permissions;

    /**
     * @var string
     *
     * @ORM\Column(name="issues_visibility", type="string", length=30, nullable=false)
     */
    private $issuesVisibility;


}
