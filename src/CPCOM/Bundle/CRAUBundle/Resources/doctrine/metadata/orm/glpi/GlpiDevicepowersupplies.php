<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiDevicepowersupplies
 *
 * @ORM\Table(name="glpi_devicepowersupplies")
 * @ORM\Entity
 */
class GlpiDevicepowersupplies
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="power", type="string", length=255, nullable=true)
     */
    private $power;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_atx", type="boolean", nullable=false)
     */
    private $isAtx;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;


}
