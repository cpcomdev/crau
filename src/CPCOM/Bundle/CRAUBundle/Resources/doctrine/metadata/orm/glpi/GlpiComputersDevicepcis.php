<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicepcis
 *
 * @ORM\Table(name="glpi_computers_devicepcis")
 * @ORM\Entity
 */
class GlpiComputersDevicepcis
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicepcis_id", type="integer", nullable=false)
     */
    private $devicepcisId;


}
