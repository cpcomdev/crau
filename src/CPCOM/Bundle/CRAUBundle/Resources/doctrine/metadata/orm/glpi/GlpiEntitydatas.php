<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiEntitydatas
 *
 * @ORM\Table(name="glpi_entitydatas")
 * @ORM\Entity
 */
class GlpiEntitydatas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=255, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="town", type="string", length=255, nullable=true)
     */
    private $town;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="phonenumber", type="string", length=255, nullable=true)
     */
    private $phonenumber;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_email", type="string", length=255, nullable=true)
     */
    private $adminEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_email_name", type="string", length=255, nullable=true)
     */
    private $adminEmailName;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_reply", type="string", length=255, nullable=true)
     */
    private $adminReply;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_reply_name", type="string", length=255, nullable=true)
     */
    private $adminReplyName;

    /**
     * @var string
     *
     * @ORM\Column(name="notification_subject_tag", type="string", length=255, nullable=true)
     */
    private $notificationSubjectTag;

    /**
     * @var string
     *
     * @ORM\Column(name="notepad", type="text", nullable=true)
     */
    private $notepad;

    /**
     * @var string
     *
     * @ORM\Column(name="ldap_dn", type="string", length=255, nullable=true)
     */
    private $ldapDn;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255, nullable=true)
     */
    private $tag;

    /**
     * @var integer
     *
     * @ORM\Column(name="authldaps_id", type="integer", nullable=false)
     */
    private $authldapsId;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_domain", type="string", length=255, nullable=true)
     */
    private $mailDomain;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_ldapfilter", type="text", nullable=true)
     */
    private $entityLdapfilter;

    /**
     * @var string
     *
     * @ORM\Column(name="mailing_signature", type="text", nullable=true)
     */
    private $mailingSignature;

    /**
     * @var integer
     *
     * @ORM\Column(name="cartridges_alert_repeat", type="integer", nullable=false)
     */
    private $cartridgesAlertRepeat;

    /**
     * @var integer
     *
     * @ORM\Column(name="consumables_alert_repeat", type="integer", nullable=false)
     */
    private $consumablesAlertRepeat;

    /**
     * @var integer
     *
     * @ORM\Column(name="use_licenses_alert", type="integer", nullable=false)
     */
    private $useLicensesAlert;

    /**
     * @var integer
     *
     * @ORM\Column(name="use_contracts_alert", type="integer", nullable=false)
     */
    private $useContractsAlert;

    /**
     * @var integer
     *
     * @ORM\Column(name="use_infocoms_alert", type="integer", nullable=false)
     */
    private $useInfocomsAlert;

    /**
     * @var integer
     *
     * @ORM\Column(name="use_reservations_alert", type="integer", nullable=false)
     */
    private $useReservationsAlert;

    /**
     * @var integer
     *
     * @ORM\Column(name="autoclose_delay", type="integer", nullable=false)
     */
    private $autocloseDelay;

    /**
     * @var integer
     *
     * @ORM\Column(name="notclosed_delay", type="integer", nullable=false)
     */
    private $notclosedDelay;

    /**
     * @var integer
     *
     * @ORM\Column(name="calendars_id", type="integer", nullable=false)
     */
    private $calendarsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="auto_assign_mode", type="integer", nullable=false)
     */
    private $autoAssignMode;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickettype", type="integer", nullable=false)
     */
    private $tickettype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="max_closedate", type="datetime", nullable=true)
     */
    private $maxClosedate;

    /**
     * @var integer
     *
     * @ORM\Column(name="inquest_config", type="integer", nullable=false)
     */
    private $inquestConfig;

    /**
     * @var integer
     *
     * @ORM\Column(name="inquest_rate", type="integer", nullable=false)
     */
    private $inquestRate;

    /**
     * @var integer
     *
     * @ORM\Column(name="inquest_delay", type="integer", nullable=false)
     */
    private $inquestDelay;

    /**
     * @var string
     *
     * @ORM\Column(name="inquest_URL", type="string", length=255, nullable=true)
     */
    private $inquestUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="autofill_warranty_date", type="string", length=255, nullable=true)
     */
    private $autofillWarrantyDate;

    /**
     * @var string
     *
     * @ORM\Column(name="autofill_use_date", type="string", length=255, nullable=true)
     */
    private $autofillUseDate;

    /**
     * @var string
     *
     * @ORM\Column(name="autofill_buy_date", type="string", length=255, nullable=true)
     */
    private $autofillBuyDate;

    /**
     * @var string
     *
     * @ORM\Column(name="autofill_delivery_date", type="string", length=255, nullable=true)
     */
    private $autofillDeliveryDate;

    /**
     * @var string
     *
     * @ORM\Column(name="autofill_order_date", type="string", length=255, nullable=true)
     */
    private $autofillOrderDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickettemplates_id", type="integer", nullable=false)
     */
    private $tickettemplatesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id_software", type="integer", nullable=false)
     */
    private $entitiesIdSoftware;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_contract_alert", type="integer", nullable=false)
     */
    private $defaultContractAlert;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_infocom_alert", type="integer", nullable=false)
     */
    private $defaultInfocomAlert;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_alarm_threshold", type="integer", nullable=false)
     */
    private $defaultAlarmThreshold;


}
