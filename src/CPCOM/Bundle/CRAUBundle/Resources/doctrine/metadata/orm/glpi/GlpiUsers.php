<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiUsers
 *
 * @ORM\Table(name="glpi_users")
 * @ORM\Entity
 */
class GlpiUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=40, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="phone2", type="string", length=255, nullable=true)
     */
    private $phone2;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=255, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="realname", type="string", length=255, nullable=true)
     */
    private $realname;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var integer
     *
     * @ORM\Column(name="locations_id", type="integer", nullable=false)
     */
    private $locationsId;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=10, nullable=true)
     */
    private $language;

    /**
     * @var integer
     *
     * @ORM\Column(name="use_mode", type="integer", nullable=false)
     */
    private $useMode;

    /**
     * @var integer
     *
     * @ORM\Column(name="list_limit", type="integer", nullable=true)
     */
    private $listLimit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="auths_id", type="integer", nullable=false)
     */
    private $authsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="authtype", type="integer", nullable=false)
     */
    private $authtype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    private $lastLogin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_sync", type="datetime", nullable=true)
     */
    private $dateSync;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="profiles_id", type="integer", nullable=false)
     */
    private $profilesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="usertitles_id", type="integer", nullable=false)
     */
    private $usertitlesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="usercategories_id", type="integer", nullable=false)
     */
    private $usercategoriesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_format", type="integer", nullable=true)
     */
    private $dateFormat;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_format", type="integer", nullable=true)
     */
    private $numberFormat;

    /**
     * @var integer
     *
     * @ORM\Column(name="names_format", type="integer", nullable=true)
     */
    private $namesFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="csv_delimiter", type="string", length=1, nullable=true)
     */
    private $csvDelimiter;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_ids_visible", type="boolean", nullable=true)
     */
    private $isIdsVisible;

    /**
     * @var integer
     *
     * @ORM\Column(name="dropdown_chars_limit", type="integer", nullable=true)
     */
    private $dropdownCharsLimit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_flat_dropdowntree", type="boolean", nullable=true)
     */
    private $useFlatDropdowntree;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_jobs_at_login", type="boolean", nullable=true)
     */
    private $showJobsAtLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_1", type="string", length=20, nullable=true)
     */
    private $priority1;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_2", type="string", length=20, nullable=true)
     */
    private $priority2;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_3", type="string", length=20, nullable=true)
     */
    private $priority3;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_4", type="string", length=20, nullable=true)
     */
    private $priority4;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_5", type="string", length=20, nullable=true)
     */
    private $priority5;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_6", type="string", length=20, nullable=true)
     */
    private $priority6;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_categorized_soft_expanded", type="boolean", nullable=true)
     */
    private $isCategorizedSoftExpanded;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_not_categorized_soft_expanded", type="boolean", nullable=true)
     */
    private $isNotCategorizedSoftExpanded;

    /**
     * @var boolean
     *
     * @ORM\Column(name="followup_private", type="boolean", nullable=true)
     */
    private $followupPrivate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="task_private", type="boolean", nullable=true)
     */
    private $taskPrivate;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_requesttypes_id", type="integer", nullable=true)
     */
    private $defaultRequesttypesId;

    /**
     * @var string
     *
     * @ORM\Column(name="password_forget_token", type="string", length=40, nullable=true)
     */
    private $passwordForgetToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="password_forget_token_date", type="datetime", nullable=true)
     */
    private $passwordForgetTokenDate;

    /**
     * @var string
     *
     * @ORM\Column(name="user_dn", type="text", nullable=true)
     */
    private $userDn;

    /**
     * @var string
     *
     * @ORM\Column(name="registration_number", type="string", length=255, nullable=true)
     */
    private $registrationNumber;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_count_on_tabs", type="boolean", nullable=true)
     */
    private $showCountOnTabs;

    /**
     * @var integer
     *
     * @ORM\Column(name="refresh_ticket_list", type="integer", nullable=true)
     */
    private $refreshTicketList;

    /**
     * @var boolean
     *
     * @ORM\Column(name="set_default_tech", type="boolean", nullable=true)
     */
    private $setDefaultTech;

    /**
     * @var string
     *
     * @ORM\Column(name="personal_token", type="string", length=255, nullable=true)
     */
    private $personalToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="personal_token_date", type="datetime", nullable=true)
     */
    private $personalTokenDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="display_count_on_home", type="integer", nullable=true)
     */
    private $displayCountOnHome;


}
