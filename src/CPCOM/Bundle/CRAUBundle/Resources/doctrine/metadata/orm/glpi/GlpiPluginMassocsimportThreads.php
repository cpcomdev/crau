<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginMassocsimportThreads
 *
 * @ORM\Table(name="glpi_plugin_massocsimport_threads")
 * @ORM\Entity
 */
class GlpiPluginMassocsimportThreads
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="threadid", type="integer", nullable=false)
     */
    private $threadid;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="rules_id", type="text", nullable=true)
     */
    private $rulesId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="datetime", nullable=true)
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_time", type="datetime", nullable=true)
     */
    private $endTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="error_msg", type="text", nullable=false)
     */
    private $errorMsg;

    /**
     * @var integer
     *
     * @ORM\Column(name="imported_machines_number", type="integer", nullable=false)
     */
    private $importedMachinesNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="synchronized_machines_number", type="integer", nullable=false)
     */
    private $synchronizedMachinesNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="failed_rules_machines_number", type="integer", nullable=false)
     */
    private $failedRulesMachinesNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="linked_machines_number", type="integer", nullable=false)
     */
    private $linkedMachinesNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="notupdated_machines_number", type="integer", nullable=false)
     */
    private $notupdatedMachinesNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="not_unique_machines_number", type="integer", nullable=false)
     */
    private $notUniqueMachinesNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="link_refused_machines_number", type="integer", nullable=false)
     */
    private $linkRefusedMachinesNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_number_machines", type="integer", nullable=false)
     */
    private $totalNumberMachines;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsservers_id", type="integer", nullable=false)
     */
    private $ocsserversId;

    /**
     * @var integer
     *
     * @ORM\Column(name="processid", type="integer", nullable=false)
     */
    private $processid;


}
