<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Boards
 *
 * @ORM\Table(name="boards")
 * @ORM\Entity
 */
class Boards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var integer
     *
     * @ORM\Column(name="topics_count", type="integer", nullable=false)
     */
    private $topicsCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="messages_count", type="integer", nullable=false)
     */
    private $messagesCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_message_id", type="integer", nullable=true)
     */
    private $lastMessageId;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true)
     */
    private $parentId;


}
