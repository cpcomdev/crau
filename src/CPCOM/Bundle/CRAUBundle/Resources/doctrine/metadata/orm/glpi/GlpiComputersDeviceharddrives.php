<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDeviceharddrives
 *
 * @ORM\Table(name="glpi_computers_deviceharddrives")
 * @ORM\Entity
 */
class GlpiComputersDeviceharddrives
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="deviceharddrives_id", type="integer", nullable=false)
     */
    private $deviceharddrivesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="specificity", type="integer", nullable=false)
     */
    private $specificity;


}
