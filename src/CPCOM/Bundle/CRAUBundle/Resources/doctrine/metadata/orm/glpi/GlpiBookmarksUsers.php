<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiBookmarksUsers
 *
 * @ORM\Table(name="glpi_bookmarks_users")
 * @ORM\Entity
 */
class GlpiBookmarksUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=100, nullable=false)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="bookmarks_id", type="integer", nullable=false)
     */
    private $bookmarksId;


}
