<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiSlalevels
 *
 * @ORM\Table(name="glpi_slalevels")
 * @ORM\Entity
 */
class GlpiSlalevels
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="slas_id", type="integer", nullable=false)
     */
    private $slasId;

    /**
     * @var integer
     *
     * @ORM\Column(name="execution_time", type="integer", nullable=false)
     */
    private $executionTime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;


}
