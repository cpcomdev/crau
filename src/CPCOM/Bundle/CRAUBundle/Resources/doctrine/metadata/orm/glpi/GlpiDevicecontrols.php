<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiDevicecontrols
 *
 * @ORM\Table(name="glpi_devicecontrols")
 * @ORM\Entity
 */
class GlpiDevicecontrols
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_raid", type="boolean", nullable=false)
     */
    private $isRaid;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="interfacetypes_id", type="integer", nullable=false)
     */
    private $interfacetypesId;


}
