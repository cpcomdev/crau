<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginDatainjectionModelcsvs
 *
 * @ORM\Table(name="glpi_plugin_datainjection_modelcsvs")
 * @ORM\Entity
 */
class GlpiPluginDatainjectionModelcsvs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="models_id", type="integer", nullable=false)
     */
    private $modelsId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255, nullable=false)
     */
    private $itemtype;

    /**
     * @var string
     *
     * @ORM\Column(name="delimiter", type="string", length=1, nullable=false)
     */
    private $delimiter;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_header_present", type="boolean", nullable=false)
     */
    private $isHeaderPresent;


}
