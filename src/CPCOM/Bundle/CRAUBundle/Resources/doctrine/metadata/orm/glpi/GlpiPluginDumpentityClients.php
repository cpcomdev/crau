<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginDumpentityClients
 *
 * @ORM\Table(name="glpi_plugin_dumpentity_clients")
 * @ORM\Entity
 */
class GlpiPluginDumpentityClients
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=false)
     */
    private $ip;

    /**
     * @var integer
     *
     * @ORM\Column(name="models_id", type="integer", nullable=false)
     */
    private $modelsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="use_time_exclusion", type="smallint", nullable=false)
     */
    private $useTimeExclusion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="allow_start_time", type="time", nullable=false)
     */
    private $allowStartTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="allow_end_time", type="time", nullable=false)
     */
    private $allowEndTime;


}
