<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginGenericobjectTypes
 *
 * @ORM\Table(name="glpi_plugin_genericobject_types")
 * @ORM\Entity
 */
class GlpiPluginGenericobjectTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255, nullable=true)
     */
    private $itemtype;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_unicity", type="boolean", nullable=false)
     */
    private $useUnicity;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_history", type="boolean", nullable=false)
     */
    private $useHistory;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_infocoms", type="boolean", nullable=false)
     */
    private $useInfocoms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_contracts", type="boolean", nullable=false)
     */
    private $useContracts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_documents", type="boolean", nullable=false)
     */
    private $useDocuments;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_tickets", type="boolean", nullable=false)
     */
    private $useTickets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_links", type="boolean", nullable=false)
     */
    private $useLinks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_loans", type="boolean", nullable=false)
     */
    private $useLoans;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_network_ports", type="boolean", nullable=false)
     */
    private $useNetworkPorts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_direct_connections", type="boolean", nullable=false)
     */
    private $useDirectConnections;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_plugin_datainjection", type="boolean", nullable=false)
     */
    private $usePluginDatainjection;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_plugin_pdf", type="boolean", nullable=false)
     */
    private $usePluginPdf;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_plugin_order", type="boolean", nullable=false)
     */
    private $usePluginOrder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_plugin_uninstall", type="boolean", nullable=false)
     */
    private $usePluginUninstall;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_plugin_geninventorynumber", type="boolean", nullable=false)
     */
    private $usePluginGeninventorynumber;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_menu_entry", type="boolean", nullable=false)
     */
    private $useMenuEntry;


}
