<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPhones
 *
 * @ORM\Table(name="glpi_phones")
 * @ORM\Entity
 */
class GlpiPhones
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable=true)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_num", type="string", length=255, nullable=true)
     */
    private $contactNum;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id_tech", type="integer", nullable=false)
     */
    private $usersIdTech;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id_tech", type="integer", nullable=false)
     */
    private $groupsIdTech;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=255, nullable=true)
     */
    private $serial;

    /**
     * @var string
     *
     * @ORM\Column(name="otherserial", type="string", length=255, nullable=true)
     */
    private $otherserial;

    /**
     * @var string
     *
     * @ORM\Column(name="firmware", type="string", length=255, nullable=true)
     */
    private $firmware;

    /**
     * @var integer
     *
     * @ORM\Column(name="locations_id", type="integer", nullable=false)
     */
    private $locationsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="phonetypes_id", type="integer", nullable=false)
     */
    private $phonetypesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="phonemodels_id", type="integer", nullable=false)
     */
    private $phonemodelsId;

    /**
     * @var string
     *
     * @ORM\Column(name="brand", type="string", length=255, nullable=true)
     */
    private $brand;

    /**
     * @var integer
     *
     * @ORM\Column(name="phonepowersupplies_id", type="integer", nullable=false)
     */
    private $phonepowersuppliesId;

    /**
     * @var string
     *
     * @ORM\Column(name="number_line", type="string", length=255, nullable=true)
     */
    private $numberLine;

    /**
     * @var boolean
     *
     * @ORM\Column(name="have_headset", type="boolean", nullable=false)
     */
    private $haveHeadset;

    /**
     * @var boolean
     *
     * @ORM\Column(name="have_hp", type="boolean", nullable=false)
     */
    private $haveHp;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_global", type="boolean", nullable=false)
     */
    private $isGlobal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_template", type="boolean", nullable=false)
     */
    private $isTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="template_name", type="string", length=255, nullable=true)
     */
    private $templateName;

    /**
     * @var string
     *
     * @ORM\Column(name="notepad", type="text", nullable=true)
     */
    private $notepad;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id", type="integer", nullable=false)
     */
    private $groupsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="states_id", type="integer", nullable=false)
     */
    private $statesId;

    /**
     * @var float
     *
     * @ORM\Column(name="ticket_tco", type="decimal", nullable=true)
     */
    private $ticketTco;


}
