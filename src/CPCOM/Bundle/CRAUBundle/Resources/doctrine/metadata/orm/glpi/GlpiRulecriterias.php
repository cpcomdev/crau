<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiRulecriterias
 *
 * @ORM\Table(name="glpi_rulecriterias")
 * @ORM\Entity
 */
class GlpiRulecriterias
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="rules_id", type="integer", nullable=false)
     */
    private $rulesId;

    /**
     * @var string
     *
     * @ORM\Column(name="criteria", type="string", length=255, nullable=true)
     */
    private $criteria;

    /**
     * @var integer
     *
     * @ORM\Column(name="condition", type="integer", nullable=false)
     */
    private $condition;

    /**
     * @var string
     *
     * @ORM\Column(name="pattern", type="string", length=255, nullable=true)
     */
    private $pattern;


}
