<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Journals
 *
 * @ORM\Table(name="journals")
 * @ORM\Entity
 */
class Journals
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="journalized_id", type="integer", nullable=false)
     */
    private $journalizedId;

    /**
     * @var string
     *
     * @ORM\Column(name="journalized_type", type="string", length=30, nullable=false)
     */
    private $journalizedType;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     */
    private $createdOn;

    /**
     * @var boolean
     *
     * @ORM\Column(name="private_notes", type="boolean", nullable=false)
     */
    private $privateNotes;


}
