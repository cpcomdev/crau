<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Watchers
 *
 * @ORM\Table(name="watchers")
 * @ORM\Entity
 */
class Watchers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="watchable_type", type="string", length=255, nullable=false)
     */
    private $watchableType;

    /**
     * @var integer
     *
     * @ORM\Column(name="watchable_id", type="integer", nullable=false)
     */
    private $watchableId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;


}
