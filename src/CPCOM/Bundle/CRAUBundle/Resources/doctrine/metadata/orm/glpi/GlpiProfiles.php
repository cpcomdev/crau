<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiProfiles
 *
 * @ORM\Table(name="glpi_profiles")
 * @ORM\Entity
 */
class GlpiProfiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="interface", type="string", length=255, nullable=true)
     */
    private $interface;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_default", type="boolean", nullable=false)
     */
    private $isDefault;

    /**
     * @var string
     *
     * @ORM\Column(name="computer", type="string", length=1, nullable=true)
     */
    private $computer;

    /**
     * @var string
     *
     * @ORM\Column(name="monitor", type="string", length=1, nullable=true)
     */
    private $monitor;

    /**
     * @var string
     *
     * @ORM\Column(name="software", type="string", length=1, nullable=true)
     */
    private $software;

    /**
     * @var string
     *
     * @ORM\Column(name="networking", type="string", length=1, nullable=true)
     */
    private $networking;

    /**
     * @var string
     *
     * @ORM\Column(name="printer", type="string", length=1, nullable=true)
     */
    private $printer;

    /**
     * @var string
     *
     * @ORM\Column(name="peripheral", type="string", length=1, nullable=true)
     */
    private $peripheral;

    /**
     * @var string
     *
     * @ORM\Column(name="cartridge", type="string", length=1, nullable=true)
     */
    private $cartridge;

    /**
     * @var string
     *
     * @ORM\Column(name="consumable", type="string", length=1, nullable=true)
     */
    private $consumable;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=1, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="string", length=1, nullable=true)
     */
    private $notes;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_enterprise", type="string", length=1, nullable=true)
     */
    private $contactEnterprise;

    /**
     * @var string
     *
     * @ORM\Column(name="document", type="string", length=1, nullable=true)
     */
    private $document;

    /**
     * @var string
     *
     * @ORM\Column(name="contract", type="string", length=1, nullable=true)
     */
    private $contract;

    /**
     * @var string
     *
     * @ORM\Column(name="infocom", type="string", length=1, nullable=true)
     */
    private $infocom;

    /**
     * @var string
     *
     * @ORM\Column(name="knowbase", type="string", length=1, nullable=true)
     */
    private $knowbase;

    /**
     * @var string
     *
     * @ORM\Column(name="knowbase_admin", type="string", length=1, nullable=true)
     */
    private $knowbaseAdmin;

    /**
     * @var string
     *
     * @ORM\Column(name="faq", type="string", length=1, nullable=true)
     */
    private $faq;

    /**
     * @var string
     *
     * @ORM\Column(name="reservation_helpdesk", type="string", length=1, nullable=true)
     */
    private $reservationHelpdesk;

    /**
     * @var string
     *
     * @ORM\Column(name="reservation_central", type="string", length=1, nullable=true)
     */
    private $reservationCentral;

    /**
     * @var string
     *
     * @ORM\Column(name="reports", type="string", length=1, nullable=true)
     */
    private $reports;

    /**
     * @var string
     *
     * @ORM\Column(name="ocsng", type="string", length=1, nullable=true)
     */
    private $ocsng;

    /**
     * @var string
     *
     * @ORM\Column(name="view_ocsng", type="string", length=1, nullable=true)
     */
    private $viewOcsng;

    /**
     * @var string
     *
     * @ORM\Column(name="sync_ocsng", type="string", length=1, nullable=true)
     */
    private $syncOcsng;

    /**
     * @var string
     *
     * @ORM\Column(name="dropdown", type="string", length=1, nullable=true)
     */
    private $dropdown;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_dropdown", type="string", length=1, nullable=true)
     */
    private $entityDropdown;

    /**
     * @var string
     *
     * @ORM\Column(name="device", type="string", length=1, nullable=true)
     */
    private $device;

    /**
     * @var string
     *
     * @ORM\Column(name="typedoc", type="string", length=1, nullable=true)
     */
    private $typedoc;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=1, nullable=true)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="config", type="string", length=1, nullable=true)
     */
    private $config;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_ticket", type="string", length=1, nullable=true)
     */
    private $ruleTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_rule_ticket", type="string", length=1, nullable=true)
     */
    private $entityRuleTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_ocs", type="string", length=1, nullable=true)
     */
    private $ruleOcs;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_ldap", type="string", length=1, nullable=true)
     */
    private $ruleLdap;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_softwarecategories", type="string", length=1, nullable=true)
     */
    private $ruleSoftwarecategories;

    /**
     * @var string
     *
     * @ORM\Column(name="search_config", type="string", length=1, nullable=true)
     */
    private $searchConfig;

    /**
     * @var string
     *
     * @ORM\Column(name="search_config_global", type="string", length=1, nullable=true)
     */
    private $searchConfigGlobal;

    /**
     * @var string
     *
     * @ORM\Column(name="check_update", type="string", length=1, nullable=true)
     */
    private $checkUpdate;

    /**
     * @var string
     *
     * @ORM\Column(name="profile", type="string", length=1, nullable=true)
     */
    private $profile;

    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=1, nullable=true)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="user_authtype", type="string", length=1, nullable=true)
     */
    private $userAuthtype;

    /**
     * @var string
     *
     * @ORM\Column(name="group", type="string", length=1, nullable=true)
     */
    private $group;

    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=1, nullable=true)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="transfer", type="string", length=1, nullable=true)
     */
    private $transfer;

    /**
     * @var string
     *
     * @ORM\Column(name="logs", type="string", length=1, nullable=true)
     */
    private $logs;

    /**
     * @var string
     *
     * @ORM\Column(name="reminder_public", type="string", length=1, nullable=true)
     */
    private $reminderPublic;

    /**
     * @var string
     *
     * @ORM\Column(name="bookmark_public", type="string", length=1, nullable=true)
     */
    private $bookmarkPublic;

    /**
     * @var string
     *
     * @ORM\Column(name="backup", type="string", length=1, nullable=true)
     */
    private $backup;

    /**
     * @var string
     *
     * @ORM\Column(name="create_ticket", type="string", length=1, nullable=true)
     */
    private $createTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="delete_ticket", type="string", length=1, nullable=true)
     */
    private $deleteTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="add_followups", type="string", length=1, nullable=true)
     */
    private $addFollowups;

    /**
     * @var string
     *
     * @ORM\Column(name="group_add_followups", type="string", length=1, nullable=true)
     */
    private $groupAddFollowups;

    /**
     * @var string
     *
     * @ORM\Column(name="global_add_followups", type="string", length=1, nullable=true)
     */
    private $globalAddFollowups;

    /**
     * @var string
     *
     * @ORM\Column(name="global_add_tasks", type="string", length=1, nullable=true)
     */
    private $globalAddTasks;

    /**
     * @var string
     *
     * @ORM\Column(name="update_ticket", type="string", length=1, nullable=true)
     */
    private $updateTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="update_priority", type="string", length=1, nullable=true)
     */
    private $updatePriority;

    /**
     * @var string
     *
     * @ORM\Column(name="own_ticket", type="string", length=1, nullable=true)
     */
    private $ownTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="steal_ticket", type="string", length=1, nullable=true)
     */
    private $stealTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="assign_ticket", type="string", length=1, nullable=true)
     */
    private $assignTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="show_all_ticket", type="string", length=1, nullable=true)
     */
    private $showAllTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="show_assign_ticket", type="string", length=1, nullable=true)
     */
    private $showAssignTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="show_full_ticket", type="string", length=1, nullable=true)
     */
    private $showFullTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="observe_ticket", type="string", length=1, nullable=true)
     */
    private $observeTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="update_followups", type="string", length=1, nullable=true)
     */
    private $updateFollowups;

    /**
     * @var string
     *
     * @ORM\Column(name="update_tasks", type="string", length=1, nullable=true)
     */
    private $updateTasks;

    /**
     * @var string
     *
     * @ORM\Column(name="show_planning", type="string", length=1, nullable=true)
     */
    private $showPlanning;

    /**
     * @var string
     *
     * @ORM\Column(name="show_group_planning", type="string", length=1, nullable=true)
     */
    private $showGroupPlanning;

    /**
     * @var string
     *
     * @ORM\Column(name="show_all_planning", type="string", length=1, nullable=true)
     */
    private $showAllPlanning;

    /**
     * @var string
     *
     * @ORM\Column(name="statistic", type="string", length=1, nullable=true)
     */
    private $statistic;

    /**
     * @var string
     *
     * @ORM\Column(name="password_update", type="string", length=1, nullable=true)
     */
    private $passwordUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="helpdesk_hardware", type="integer", nullable=false)
     */
    private $helpdeskHardware;

    /**
     * @var string
     *
     * @ORM\Column(name="helpdesk_item_type", type="text", nullable=true)
     */
    private $helpdeskItemType;

    /**
     * @var string
     *
     * @ORM\Column(name="ticket_status", type="text", nullable=true)
     */
    private $ticketStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="show_group_ticket", type="string", length=1, nullable=true)
     */
    private $showGroupTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="show_group_hardware", type="string", length=1, nullable=true)
     */
    private $showGroupHardware;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_dictionnary_software", type="string", length=1, nullable=true)
     */
    private $ruleDictionnarySoftware;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_dictionnary_dropdown", type="string", length=1, nullable=true)
     */
    private $ruleDictionnaryDropdown;

    /**
     * @var string
     *
     * @ORM\Column(name="budget", type="string", length=1, nullable=true)
     */
    private $budget;

    /**
     * @var string
     *
     * @ORM\Column(name="import_externalauth_users", type="string", length=1, nullable=true)
     */
    private $importExternalauthUsers;

    /**
     * @var string
     *
     * @ORM\Column(name="notification", type="string", length=1, nullable=true)
     */
    private $notification;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_mailcollector", type="string", length=1, nullable=true)
     */
    private $ruleMailcollector;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="validate_ticket", type="string", length=1, nullable=true)
     */
    private $validateTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="create_validation", type="string", length=1, nullable=true)
     */
    private $createValidation;

    /**
     * @var string
     *
     * @ORM\Column(name="calendar", type="string", length=1, nullable=true)
     */
    private $calendar;

    /**
     * @var string
     *
     * @ORM\Column(name="sla", type="string", length=1, nullable=true)
     */
    private $sla;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_dictionnary_printer", type="string", length=1, nullable=true)
     */
    private $ruleDictionnaryPrinter;

    /**
     * @var string
     *
     * @ORM\Column(name="clean_ocsng", type="string", length=1, nullable=true)
     */
    private $cleanOcsng;

    /**
     * @var string
     *
     * @ORM\Column(name="update_own_followups", type="string", length=1, nullable=true)
     */
    private $updateOwnFollowups;

    /**
     * @var string
     *
     * @ORM\Column(name="delete_followups", type="string", length=1, nullable=true)
     */
    private $deleteFollowups;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_helpdesk", type="string", length=1, nullable=true)
     */
    private $entityHelpdesk;

    /**
     * @var string
     *
     * @ORM\Column(name="show_my_problem", type="string", length=1, nullable=true)
     */
    private $showMyProblem;

    /**
     * @var string
     *
     * @ORM\Column(name="show_all_problem", type="string", length=1, nullable=true)
     */
    private $showAllProblem;

    /**
     * @var string
     *
     * @ORM\Column(name="edit_all_problem", type="string", length=1, nullable=true)
     */
    private $editAllProblem;

    /**
     * @var string
     *
     * @ORM\Column(name="problem_status", type="text", nullable=true)
     */
    private $problemStatus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="create_ticket_on_login", type="boolean", nullable=false)
     */
    private $createTicketOnLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="tickettemplate", type="string", length=1, nullable=true)
     */
    private $tickettemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="ticketrecurrent", type="string", length=1, nullable=true)
     */
    private $ticketrecurrent;


}
