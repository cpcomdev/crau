<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginDatainjectionModels
 *
 * @ORM\Table(name="glpi_plugin_datainjection_models")
 * @ORM\Entity
 */
class GlpiPluginDatainjectionModels
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=false)
     */
    private $dateMod;

    /**
     * @var string
     *
     * @ORM\Column(name="filetype", type="string", length=255, nullable=false)
     */
    private $filetype;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255, nullable=false)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="behavior_add", type="boolean", nullable=false)
     */
    private $behaviorAdd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="behavior_update", type="boolean", nullable=false)
     */
    private $behaviorUpdate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="can_add_dropdown", type="boolean", nullable=false)
     */
    private $canAddDropdown;

    /**
     * @var integer
     *
     * @ORM\Column(name="can_overwrite_if_not_empty", type="integer", nullable=false)
     */
    private $canOverwriteIfNotEmpty;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_private", type="boolean", nullable=false)
     */
    private $isPrivate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="perform_network_connection", type="boolean", nullable=false)
     */
    private $performNetworkConnection;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var string
     *
     * @ORM\Column(name="date_format", type="string", length=11, nullable=false)
     */
    private $dateFormat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="float_format", type="boolean", nullable=false)
     */
    private $floatFormat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="port_unicity", type="boolean", nullable=false)
     */
    private $portUnicity;

    /**
     * @var integer
     *
     * @ORM\Column(name="step", type="integer", nullable=false)
     */
    private $step;


}
