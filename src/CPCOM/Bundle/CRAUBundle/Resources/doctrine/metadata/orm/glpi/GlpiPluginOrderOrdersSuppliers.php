<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderOrdersSuppliers
 *
 * @ORM\Table(name="glpi_plugin_order_orders_suppliers")
 * @ORM\Entity
 */
class GlpiPluginOrderOrdersSuppliers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_orders_id", type="integer", nullable=false)
     */
    private $pluginOrderOrdersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var string
     *
     * @ORM\Column(name="num_quote", type="string", length=255, nullable=true)
     */
    private $numQuote;

    /**
     * @var string
     *
     * @ORM\Column(name="num_order", type="string", length=255, nullable=true)
     */
    private $numOrder;


}
