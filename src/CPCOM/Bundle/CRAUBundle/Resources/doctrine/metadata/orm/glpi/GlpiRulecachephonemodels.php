<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiRulecachephonemodels
 *
 * @ORM\Table(name="glpi_rulecachephonemodels")
 * @ORM\Entity
 */
class GlpiRulecachephonemodels
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="old_value", type="string", length=255, nullable=true)
     */
    private $oldValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="rules_id", type="integer", nullable=false)
     */
    private $rulesId;

    /**
     * @var string
     *
     * @ORM\Column(name="new_value", type="string", length=255, nullable=true)
     */
    private $newValue;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer", type="string", length=255, nullable=true)
     */
    private $manufacturer;


}
