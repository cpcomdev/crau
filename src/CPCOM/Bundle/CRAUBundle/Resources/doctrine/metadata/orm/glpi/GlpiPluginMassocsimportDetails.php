<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginMassocsimportDetails
 *
 * @ORM\Table(name="glpi_plugin_massocsimport_details")
 * @ORM\Entity
 */
class GlpiPluginMassocsimportDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_massocsimport_threads_id", type="integer", nullable=false)
     */
    private $pluginMassocsimportThreadsId;

    /**
     * @var string
     *
     * @ORM\Column(name="rules_id", type="text", nullable=true)
     */
    private $rulesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="threadid", type="integer", nullable=false)
     */
    private $threadid;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsid", type="integer", nullable=false)
     */
    private $ocsid;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="action", type="integer", nullable=false)
     */
    private $action;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="process_time", type="datetime", nullable=true)
     */
    private $processTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsservers_id", type="integer", nullable=false)
     */
    private $ocsserversId;


}
