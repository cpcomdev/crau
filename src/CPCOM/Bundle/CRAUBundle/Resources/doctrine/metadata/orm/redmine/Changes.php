<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Changes
 *
 * @ORM\Table(name="changes")
 * @ORM\Entity
 */
class Changes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="changeset_id", type="integer", nullable=false)
     */
    private $changesetId;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=1, nullable=false)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="text", nullable=false)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="from_path", type="text", nullable=true)
     */
    private $fromPath;

    /**
     * @var string
     *
     * @ORM\Column(name="from_revision", type="string", length=255, nullable=true)
     */
    private $fromRevision;

    /**
     * @var string
     *
     * @ORM\Column(name="revision", type="string", length=255, nullable=true)
     */
    private $revision;

    /**
     * @var string
     *
     * @ORM\Column(name="branch", type="string", length=255, nullable=true)
     */
    private $branch;


}
