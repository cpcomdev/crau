<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiDevicenetworkcards
 *
 * @ORM\Table(name="glpi_devicenetworkcards")
 * @ORM\Entity
 */
class GlpiDevicenetworkcards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="bandwidth", type="string", length=255, nullable=true)
     */
    private $bandwidth;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var string
     *
     * @ORM\Column(name="specif_default", type="string", length=255, nullable=true)
     */
    private $specifDefault;


}
