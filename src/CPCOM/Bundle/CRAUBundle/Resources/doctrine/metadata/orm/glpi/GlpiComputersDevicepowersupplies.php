<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicepowersupplies
 *
 * @ORM\Table(name="glpi_computers_devicepowersupplies")
 * @ORM\Entity
 */
class GlpiComputersDevicepowersupplies
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicepowersupplies_id", type="integer", nullable=false)
     */
    private $devicepowersuppliesId;


}
