<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiFieldunicities
 *
 * @ORM\Table(name="glpi_fieldunicities")
 * @ORM\Entity
 */
class GlpiFieldunicities
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255, nullable=false)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="fields", type="text", nullable=true)
     */
    private $fields;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="action_refuse", type="boolean", nullable=false)
     */
    private $actionRefuse;

    /**
     * @var boolean
     *
     * @ORM\Column(name="action_notify", type="boolean", nullable=false)
     */
    private $actionNotify;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


}
