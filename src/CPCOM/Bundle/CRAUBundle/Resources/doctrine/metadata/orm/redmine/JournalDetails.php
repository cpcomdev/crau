<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * JournalDetails
 *
 * @ORM\Table(name="journal_details")
 * @ORM\Entity
 */
class JournalDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="journal_id", type="integer", nullable=false)
     */
    private $journalId;

    /**
     * @var string
     *
     * @ORM\Column(name="property", type="string", length=30, nullable=false)
     */
    private $property;

    /**
     * @var string
     *
     * @ORM\Column(name="prop_key", type="string", length=30, nullable=false)
     */
    private $propKey;

    /**
     * @var string
     *
     * @ORM\Column(name="old_value", type="text", nullable=true)
     */
    private $oldValue;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;


}
