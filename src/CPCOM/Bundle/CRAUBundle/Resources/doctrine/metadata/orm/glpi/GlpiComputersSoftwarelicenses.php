<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersSoftwarelicenses
 *
 * @ORM\Table(name="glpi_computers_softwarelicenses")
 * @ORM\Entity
 */
class GlpiComputersSoftwarelicenses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwarelicenses_id", type="integer", nullable=false)
     */
    private $softwarelicensesId;


}
