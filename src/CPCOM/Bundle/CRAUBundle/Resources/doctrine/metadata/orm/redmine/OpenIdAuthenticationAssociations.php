<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * OpenIdAuthenticationAssociations
 *
 * @ORM\Table(name="open_id_authentication_associations")
 * @ORM\Entity
 */
class OpenIdAuthenticationAssociations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="issued", type="integer", nullable=true)
     */
    private $issued;

    /**
     * @var integer
     *
     * @ORM\Column(name="lifetime", type="integer", nullable=true)
     */
    private $lifetime;

    /**
     * @var string
     *
     * @ORM\Column(name="handle", type="string", length=255, nullable=true)
     */
    private $handle;

    /**
     * @var string
     *
     * @ORM\Column(name="assoc_type", type="string", length=255, nullable=true)
     */
    private $assocType;

    /**
     * @var string
     *
     * @ORM\Column(name="server_url", type="blob", nullable=true)
     */
    private $serverUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="secret", type="blob", nullable=true)
     */
    private $secret;


}
