<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputerdisks
 *
 * @ORM\Table(name="glpi_computerdisks")
 * @ORM\Entity
 */
class GlpiComputerdisks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="device", type="string", length=255, nullable=true)
     */
    private $device;

    /**
     * @var string
     *
     * @ORM\Column(name="mountpoint", type="string", length=255, nullable=true)
     */
    private $mountpoint;

    /**
     * @var integer
     *
     * @ORM\Column(name="filesystems_id", type="integer", nullable=false)
     */
    private $filesystemsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="totalsize", type="integer", nullable=false)
     */
    private $totalsize;

    /**
     * @var integer
     *
     * @ORM\Column(name="freesize", type="integer", nullable=false)
     */
    private $freesize;


}
