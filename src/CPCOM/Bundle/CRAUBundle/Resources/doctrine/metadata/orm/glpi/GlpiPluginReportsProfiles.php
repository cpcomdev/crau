<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginReportsProfiles
 *
 * @ORM\Table(name="glpi_plugin_reports_profiles")
 * @ORM\Entity
 */
class GlpiPluginReportsProfiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="profiles_id", type="integer", nullable=false)
     */
    private $profilesId;

    /**
     * @var string
     *
     * @ORM\Column(name="report", type="string", length=255, nullable=true)
     */
    private $report;

    /**
     * @var string
     *
     * @ORM\Column(name="access", type="string", length=1, nullable=true)
     */
    private $access;


}
