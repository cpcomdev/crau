<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TimeEntries
 *
 * @ORM\Table(name="time_entries")
 * @ORM\Entity
 */
class TimeEntries
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="issue_id", type="integer", nullable=true)
     */
    private $issueId;

    /**
     * @var float
     *
     * @ORM\Column(name="hours", type="float", nullable=false)
     */
    private $hours;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=255, nullable=true)
     */
    private $comments;

    /**
     * @var integer
     *
     * @ORM\Column(name="activity_id", type="integer", nullable=false)
     */
    private $activityId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="spent_on", type="date", nullable=false)
     */
    private $spentOn;

    /**
     * @var integer
     *
     * @ORM\Column(name="tyear", type="integer", nullable=false)
     */
    private $tyear;

    /**
     * @var integer
     *
     * @ORM\Column(name="tmonth", type="integer", nullable=false)
     */
    private $tmonth;

    /**
     * @var integer
     *
     * @ORM\Column(name="tweek", type="integer", nullable=false)
     */
    private $tweek;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     */
    private $createdOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_on", type="datetime", nullable=false)
     */
    private $updatedOn;


}
