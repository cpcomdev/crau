<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicemotherboards
 *
 * @ORM\Table(name="glpi_computers_devicemotherboards")
 * @ORM\Entity
 */
class GlpiComputersDevicemotherboards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicemotherboards_id", type="integer", nullable=false)
     */
    private $devicemotherboardsId;


}
