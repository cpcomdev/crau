<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiAuthldapreplicates
 *
 * @ORM\Table(name="glpi_authldapreplicates")
 * @ORM\Entity
 */
class GlpiAuthldapreplicates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="authldaps_id", type="integer", nullable=false)
     */
    private $authldapsId;

    /**
     * @var string
     *
     * @ORM\Column(name="host", type="string", length=255, nullable=true)
     */
    private $host;

    /**
     * @var integer
     *
     * @ORM\Column(name="port", type="integer", nullable=false)
     */
    private $port;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;


}
