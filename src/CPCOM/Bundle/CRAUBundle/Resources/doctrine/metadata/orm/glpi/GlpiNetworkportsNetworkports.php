<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiNetworkportsNetworkports
 *
 * @ORM\Table(name="glpi_networkports_networkports")
 * @ORM\Entity
 */
class GlpiNetworkportsNetworkports
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="networkports_id_1", type="integer", nullable=false)
     */
    private $networkportsId1;

    /**
     * @var integer
     *
     * @ORM\Column(name="networkports_id_2", type="integer", nullable=false)
     */
    private $networkportsId2;


}
