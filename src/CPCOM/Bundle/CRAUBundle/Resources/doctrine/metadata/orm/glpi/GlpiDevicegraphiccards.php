<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiDevicegraphiccards
 *
 * @ORM\Table(name="glpi_devicegraphiccards")
 * @ORM\Entity
 */
class GlpiDevicegraphiccards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var integer
     *
     * @ORM\Column(name="interfacetypes_id", type="integer", nullable=false)
     */
    private $interfacetypesId;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="specif_default", type="integer", nullable=false)
     */
    private $specifDefault;


}
