<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputervirtualmachines
 *
 * @ORM\Table(name="glpi_computervirtualmachines")
 * @ORM\Entity
 */
class GlpiComputervirtualmachines
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="virtualmachinestates_id", type="integer", nullable=false)
     */
    private $virtualmachinestatesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="virtualmachinesystems_id", type="integer", nullable=false)
     */
    private $virtualmachinesystemsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="virtualmachinetypes_id", type="integer", nullable=false)
     */
    private $virtualmachinetypesId;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=255, nullable=false)
     */
    private $uuid;

    /**
     * @var integer
     *
     * @ORM\Column(name="vcpu", type="integer", nullable=false)
     */
    private $vcpu;

    /**
     * @var string
     *
     * @ORM\Column(name="ram", type="string", length=255, nullable=false)
     */
    private $ram;


}
