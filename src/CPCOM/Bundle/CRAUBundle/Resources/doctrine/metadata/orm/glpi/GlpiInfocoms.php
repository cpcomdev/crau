<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiInfocoms
 *
 * @ORM\Table(name="glpi_infocoms")
 * @ORM\Entity
 */
class GlpiInfocoms
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="items_id", type="integer", nullable=false)
     */
    private $itemsId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=100, nullable=false)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="buy_date", type="date", nullable=true)
     */
    private $buyDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="use_date", type="date", nullable=true)
     */
    private $useDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="warranty_duration", type="integer", nullable=false)
     */
    private $warrantyDuration;

    /**
     * @var string
     *
     * @ORM\Column(name="warranty_info", type="string", length=255, nullable=true)
     */
    private $warrantyInfo;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var string
     *
     * @ORM\Column(name="order_number", type="string", length=255, nullable=true)
     */
    private $orderNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_number", type="string", length=255, nullable=true)
     */
    private $deliveryNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="immo_number", type="string", length=255, nullable=true)
     */
    private $immoNumber;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="decimal", nullable=false)
     */
    private $value;

    /**
     * @var float
     *
     * @ORM\Column(name="warranty_value", type="decimal", nullable=false)
     */
    private $warrantyValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="sink_time", type="integer", nullable=false)
     */
    private $sinkTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="sink_type", type="integer", nullable=false)
     */
    private $sinkType;

    /**
     * @var float
     *
     * @ORM\Column(name="sink_coeff", type="float", nullable=false)
     */
    private $sinkCoeff;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="bill", type="string", length=255, nullable=true)
     */
    private $bill;

    /**
     * @var integer
     *
     * @ORM\Column(name="budgets_id", type="integer", nullable=false)
     */
    private $budgetsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="alert", type="integer", nullable=false)
     */
    private $alert;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_date", type="date", nullable=true)
     */
    private $orderDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="delivery_date", type="date", nullable=true)
     */
    private $deliveryDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inventory_date", type="date", nullable=true)
     */
    private $inventoryDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="warranty_date", type="date", nullable=true)
     */
    private $warrantyDate;


}
