<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiCrontasks
 *
 * @ORM\Table(name="glpi_crontasks")
 * @ORM\Entity
 */
class GlpiCrontasks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=100, nullable=false)
     */
    private $itemtype;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="frequency", type="integer", nullable=false)
     */
    private $frequency;

    /**
     * @var integer
     *
     * @ORM\Column(name="param", type="integer", nullable=true)
     */
    private $param;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer", nullable=false)
     */
    private $state;

    /**
     * @var integer
     *
     * @ORM\Column(name="mode", type="integer", nullable=false)
     */
    private $mode;

    /**
     * @var integer
     *
     * @ORM\Column(name="allowmode", type="integer", nullable=false)
     */
    private $allowmode;

    /**
     * @var integer
     *
     * @ORM\Column(name="hourmin", type="integer", nullable=false)
     */
    private $hourmin;

    /**
     * @var integer
     *
     * @ORM\Column(name="hourmax", type="integer", nullable=false)
     */
    private $hourmax;

    /**
     * @var integer
     *
     * @ORM\Column(name="logs_lifetime", type="integer", nullable=false)
     */
    private $logsLifetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastrun", type="datetime", nullable=true)
     */
    private $lastrun;

    /**
     * @var integer
     *
     * @ORM\Column(name="lastcode", type="integer", nullable=true)
     */
    private $lastcode;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


}
