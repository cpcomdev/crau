<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiContractsItems
 *
 * @ORM\Table(name="glpi_contracts_items")
 * @ORM\Entity
 */
class GlpiContractsItems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="contracts_id", type="integer", nullable=false)
     */
    private $contractsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="items_id", type="integer", nullable=false)
     */
    private $itemsId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=100, nullable=false)
     */
    private $itemtype;


}
