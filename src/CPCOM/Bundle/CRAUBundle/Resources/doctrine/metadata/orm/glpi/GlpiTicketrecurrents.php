<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiTicketrecurrents
 *
 * @ORM\Table(name="glpi_ticketrecurrents")
 * @ORM\Entity
 */
class GlpiTicketrecurrents
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickettemplates_id", type="integer", nullable=false)
     */
    private $tickettemplatesId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin_date", type="datetime", nullable=true)
     */
    private $beginDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="periodicity", type="integer", nullable=false)
     */
    private $periodicity;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_before", type="integer", nullable=false)
     */
    private $createBefore;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="next_creation_date", type="datetime", nullable=true)
     */
    private $nextCreationDate;


}
