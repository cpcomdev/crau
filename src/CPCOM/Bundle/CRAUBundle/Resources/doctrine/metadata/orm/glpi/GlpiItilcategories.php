<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiItilcategories
 *
 * @ORM\Table(name="glpi_itilcategories")
 * @ORM\Entity
 */
class GlpiItilcategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="itilcategories_id", type="integer", nullable=false)
     */
    private $itilcategoriesId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="completename", type="text", nullable=true)
     */
    private $completename;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="integer", nullable=false)
     */
    private $level;

    /**
     * @var integer
     *
     * @ORM\Column(name="knowbaseitemcategories_id", type="integer", nullable=false)
     */
    private $knowbaseitemcategoriesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id", type="integer", nullable=false)
     */
    private $groupsId;

    /**
     * @var string
     *
     * @ORM\Column(name="ancestors_cache", type="text", nullable=true)
     */
    private $ancestorsCache;

    /**
     * @var string
     *
     * @ORM\Column(name="sons_cache", type="text", nullable=true)
     */
    private $sonsCache;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_helpdeskvisible", type="boolean", nullable=false)
     */
    private $isHelpdeskvisible;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickettemplates_id_incident", type="integer", nullable=false)
     */
    private $tickettemplatesIdIncident;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickettemplates_id_demand", type="integer", nullable=false)
     */
    private $tickettemplatesIdDemand;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_incident", type="integer", nullable=false)
     */
    private $isIncident;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_request", type="integer", nullable=false)
     */
    private $isRequest;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_problem", type="integer", nullable=false)
     */
    private $isProblem;


}
