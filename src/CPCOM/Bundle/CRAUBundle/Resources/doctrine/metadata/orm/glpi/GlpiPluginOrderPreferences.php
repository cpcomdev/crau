<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderPreferences
 *
 * @ORM\Table(name="glpi_plugin_order_preferences")
 * @ORM\Entity
 */
class GlpiPluginOrderPreferences
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var string
     *
     * @ORM\Column(name="template", type="string", length=255, nullable=true)
     */
    private $template;

    /**
     * @var string
     *
     * @ORM\Column(name="sign", type="string", length=255, nullable=true)
     */
    private $sign;


}
