<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicegraphiccards
 *
 * @ORM\Table(name="glpi_computers_devicegraphiccards")
 * @ORM\Entity
 */
class GlpiComputersDevicegraphiccards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicegraphiccards_id", type="integer", nullable=false)
     */
    private $devicegraphiccardsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="specificity", type="integer", nullable=false)
     */
    private $specificity;


}
