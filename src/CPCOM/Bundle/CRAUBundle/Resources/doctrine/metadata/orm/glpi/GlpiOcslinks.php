<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiOcslinks
 *
 * @ORM\Table(name="glpi_ocslinks")
 * @ORM\Entity
 */
class GlpiOcslinks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsid", type="integer", nullable=false)
     */
    private $ocsid;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_deviceid", type="string", length=255, nullable=true)
     */
    private $ocsDeviceid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_auto_update", type="boolean", nullable=false)
     */
    private $useAutoUpdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_ocs_update", type="datetime", nullable=true)
     */
    private $lastOcsUpdate;

    /**
     * @var string
     *
     * @ORM\Column(name="computer_update", type="text", nullable=true)
     */
    private $computerUpdate;

    /**
     * @var string
     *
     * @ORM\Column(name="import_device", type="text", nullable=true)
     */
    private $importDevice;

    /**
     * @var string
     *
     * @ORM\Column(name="import_disk", type="text", nullable=true)
     */
    private $importDisk;

    /**
     * @var string
     *
     * @ORM\Column(name="import_software", type="text", nullable=true)
     */
    private $importSoftware;

    /**
     * @var string
     *
     * @ORM\Column(name="import_monitor", type="text", nullable=true)
     */
    private $importMonitor;

    /**
     * @var string
     *
     * @ORM\Column(name="import_peripheral", type="text", nullable=true)
     */
    private $importPeripheral;

    /**
     * @var string
     *
     * @ORM\Column(name="import_printer", type="text", nullable=true)
     */
    private $importPrinter;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsservers_id", type="integer", nullable=false)
     */
    private $ocsserversId;

    /**
     * @var string
     *
     * @ORM\Column(name="import_ip", type="text", nullable=true)
     */
    private $importIp;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_agent_version", type="string", length=255, nullable=true)
     */
    private $ocsAgentVersion;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255, nullable=true)
     */
    private $tag;

    /**
     * @var string
     *
     * @ORM\Column(name="import_vm", type="text", nullable=true)
     */
    private $importVm;


}
