<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Queries
 *
 * @ORM\Table(name="queries")
 * @ORM\Entity
 */
class Queries
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer", nullable=true)
     */
    private $projectId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="filters", type="text", nullable=true)
     */
    private $filters;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_public", type="boolean", nullable=false)
     */
    private $isPublic;

    /**
     * @var string
     *
     * @ORM\Column(name="column_names", type="text", nullable=true)
     */
    private $columnNames;

    /**
     * @var string
     *
     * @ORM\Column(name="sort_criteria", type="text", nullable=true)
     */
    private $sortCriteria;

    /**
     * @var string
     *
     * @ORM\Column(name="group_by", type="string", length=255, nullable=true)
     */
    private $groupBy;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;


}
