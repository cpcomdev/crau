<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiSlalevelactions
 *
 * @ORM\Table(name="glpi_slalevelactions")
 * @ORM\Entity
 */
class GlpiSlalevelactions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="slalevels_id", type="integer", nullable=false)
     */
    private $slalevelsId;

    /**
     * @var string
     *
     * @ORM\Column(name="action_type", type="string", length=255, nullable=true)
     */
    private $actionType;

    /**
     * @var string
     *
     * @ORM\Column(name="field", type="string", length=255, nullable=true)
     */
    private $field;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=true)
     */
    private $value;


}
