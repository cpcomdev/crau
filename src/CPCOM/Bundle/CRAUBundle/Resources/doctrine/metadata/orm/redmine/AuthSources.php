<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * AuthSources
 *
 * @ORM\Table(name="auth_sources")
 * @ORM\Entity
 */
class AuthSources
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=60, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="host", type="string", length=60, nullable=true)
     */
    private $host;

    /**
     * @var integer
     *
     * @ORM\Column(name="port", type="integer", nullable=true)
     */
    private $port;

    /**
     * @var string
     *
     * @ORM\Column(name="account", type="string", length=255, nullable=true)
     */
    private $account;

    /**
     * @var string
     *
     * @ORM\Column(name="account_password", type="string", length=255, nullable=true)
     */
    private $accountPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="base_dn", type="string", length=255, nullable=true)
     */
    private $baseDn;

    /**
     * @var string
     *
     * @ORM\Column(name="attr_login", type="string", length=30, nullable=true)
     */
    private $attrLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="attr_firstname", type="string", length=30, nullable=true)
     */
    private $attrFirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="attr_lastname", type="string", length=30, nullable=true)
     */
    private $attrLastname;

    /**
     * @var string
     *
     * @ORM\Column(name="attr_mail", type="string", length=30, nullable=true)
     */
    private $attrMail;

    /**
     * @var boolean
     *
     * @ORM\Column(name="onthefly_register", type="boolean", nullable=false)
     */
    private $ontheflyRegister;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tls", type="boolean", nullable=false)
     */
    private $tls;

    /**
     * @var string
     *
     * @ORM\Column(name="filter", type="string", length=255, nullable=true)
     */
    private $filter;

    /**
     * @var integer
     *
     * @ORM\Column(name="timeout", type="integer", nullable=true)
     */
    private $timeout;


}
