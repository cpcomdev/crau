<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicecontrols
 *
 * @ORM\Table(name="glpi_computers_devicecontrols")
 * @ORM\Entity
 */
class GlpiComputersDevicecontrols
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicecontrols_id", type="integer", nullable=false)
     */
    private $devicecontrolsId;


}
