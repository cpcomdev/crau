<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Changesets
 *
 * @ORM\Table(name="changesets")
 * @ORM\Entity
 */
class Changesets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="repository_id", type="integer", nullable=false)
     */
    private $repositoryId;

    /**
     * @var string
     *
     * @ORM\Column(name="revision", type="string", length=255, nullable=false)
     */
    private $revision;

    /**
     * @var string
     *
     * @ORM\Column(name="committer", type="string", length=255, nullable=true)
     */
    private $committer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="committed_on", type="datetime", nullable=false)
     */
    private $committedOn;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="text", nullable=true)
     */
    private $comments;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="commit_date", type="date", nullable=true)
     */
    private $commitDate;

    /**
     * @var string
     *
     * @ORM\Column(name="scmid", type="string", length=255, nullable=true)
     */
    private $scmid;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;


}
