<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginDumpentityModels
 *
 * @ORM\Table(name="glpi_plugin_dumpentity_models")
 * @ORM\Entity
 */
class GlpiPluginDumpentityModels
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_alerts", type="boolean", nullable=false)
     */
    private $glpiAlerts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_authldaps", type="boolean", nullable=false)
     */
    private $glpiAuthldaps;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_authmails", type="boolean", nullable=false)
     */
    private $glpiAuthmails;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_autoupdatesystems", type="boolean", nullable=false)
     */
    private $glpiAutoupdatesystems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_bookmarks", type="boolean", nullable=false)
     */
    private $glpiBookmarks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_bookmarks_users", type="boolean", nullable=false)
     */
    private $glpiBookmarksUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_budgets", type="boolean", nullable=false)
     */
    private $glpiBudgets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_calendars", type="boolean", nullable=false)
     */
    private $glpiCalendars;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_calendarsegments", type="boolean", nullable=false)
     */
    private $glpiCalendarsegments;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_calendars_holidays", type="boolean", nullable=false)
     */
    private $glpiCalendarsHolidays;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_cartridgeitems", type="boolean", nullable=false)
     */
    private $glpiCartridgeitems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_cartridgeitems_printermodels", type="boolean", nullable=false)
     */
    private $glpiCartridgeitemsPrintermodels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_cartridgeitemtypes", type="boolean", nullable=false)
     */
    private $glpiCartridgeitemtypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_cartridges", type="boolean", nullable=false)
     */
    private $glpiCartridges;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_changes", type="boolean", nullable=false)
     */
    private $glpiChanges;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_changes_groups", type="boolean", nullable=false)
     */
    private $glpiChangesGroups;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_changes_items", type="boolean", nullable=false)
     */
    private $glpiChangesItems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_changes_problems", type="boolean", nullable=false)
     */
    private $glpiChangesProblems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_changes_tickets", type="boolean", nullable=false)
     */
    private $glpiChangesTickets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_changes_users", type="boolean", nullable=false)
     */
    private $glpiChangesUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_changetasks", type="boolean", nullable=false)
     */
    private $glpiChangetasks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computerdisks", type="boolean", nullable=false)
     */
    private $glpiComputerdisks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computermodels", type="boolean", nullable=false)
     */
    private $glpiComputermodels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers", type="boolean", nullable=false)
     */
    private $glpiComputers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicecases", type="boolean", nullable=false)
     */
    private $glpiComputersDevicecases;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicecontrols", type="boolean", nullable=false)
     */
    private $glpiComputersDevicecontrols;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicedrives", type="boolean", nullable=false)
     */
    private $glpiComputersDevicedrives;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicegraphiccards", type="boolean", nullable=false)
     */
    private $glpiComputersDevicegraphiccards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_deviceharddrives", type="boolean", nullable=false)
     */
    private $glpiComputersDeviceharddrives;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicememories", type="boolean", nullable=false)
     */
    private $glpiComputersDevicememories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicemotherboards", type="boolean", nullable=false)
     */
    private $glpiComputersDevicemotherboards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicenetworkcards", type="boolean", nullable=false)
     */
    private $glpiComputersDevicenetworkcards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicepcis", type="boolean", nullable=false)
     */
    private $glpiComputersDevicepcis;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicepowersupplies", type="boolean", nullable=false)
     */
    private $glpiComputersDevicepowersupplies;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_deviceprocessors", type="boolean", nullable=false)
     */
    private $glpiComputersDeviceprocessors;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicesoundcards", type="boolean", nullable=false)
     */
    private $glpiComputersDevicesoundcards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_items", type="boolean", nullable=false)
     */
    private $glpiComputersItems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_softwarelicenses", type="boolean", nullable=false)
     */
    private $glpiComputersSoftwarelicenses;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_softwareversions", type="boolean", nullable=false)
     */
    private $glpiComputersSoftwareversions;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computertypes", type="boolean", nullable=false)
     */
    private $glpiComputertypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computervirtualmachines", type="boolean", nullable=false)
     */
    private $glpiComputervirtualmachines;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_consumableitems", type="boolean", nullable=false)
     */
    private $glpiConsumableitems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_consumableitemtypes", type="boolean", nullable=false)
     */
    private $glpiConsumableitemtypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_consumables", type="boolean", nullable=false)
     */
    private $glpiConsumables;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_contacts", type="boolean", nullable=false)
     */
    private $glpiContacts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_contacts_suppliers", type="boolean", nullable=false)
     */
    private $glpiContactsSuppliers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_contacttypes", type="boolean", nullable=false)
     */
    private $glpiContacttypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_contracts", type="boolean", nullable=false)
     */
    private $glpiContracts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_contracts_items", type="boolean", nullable=false)
     */
    private $glpiContractsItems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_contracts_suppliers", type="boolean", nullable=false)
     */
    private $glpiContractsSuppliers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_contracttypes", type="boolean", nullable=false)
     */
    private $glpiContracttypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicecases", type="boolean", nullable=false)
     */
    private $glpiDevicecases;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicecasetypes", type="boolean", nullable=false)
     */
    private $glpiDevicecasetypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicecontrols", type="boolean", nullable=false)
     */
    private $glpiDevicecontrols;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicedrives", type="boolean", nullable=false)
     */
    private $glpiDevicedrives;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicegraphiccards", type="boolean", nullable=false)
     */
    private $glpiDevicegraphiccards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_deviceharddrives", type="boolean", nullable=false)
     */
    private $glpiDeviceharddrives;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicememories", type="boolean", nullable=false)
     */
    private $glpiDevicememories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicememorytypes", type="boolean", nullable=false)
     */
    private $glpiDevicememorytypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicemotherboards", type="boolean", nullable=false)
     */
    private $glpiDevicemotherboards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicenetworkcards", type="boolean", nullable=false)
     */
    private $glpiDevicenetworkcards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicepcis", type="boolean", nullable=false)
     */
    private $glpiDevicepcis;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicepowersupplies", type="boolean", nullable=false)
     */
    private $glpiDevicepowersupplies;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_deviceprocessors", type="boolean", nullable=false)
     */
    private $glpiDeviceprocessors;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicesoundcards", type="boolean", nullable=false)
     */
    private $glpiDevicesoundcards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_documentcategories", type="boolean", nullable=false)
     */
    private $glpiDocumentcategories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_documents", type="boolean", nullable=false)
     */
    private $glpiDocuments;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_documents_items", type="boolean", nullable=false)
     */
    private $glpiDocumentsItems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_documenttypes", type="boolean", nullable=false)
     */
    private $glpiDocumenttypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_domains", type="boolean", nullable=false)
     */
    private $glpiDomains;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_entities", type="boolean", nullable=false)
     */
    private $glpiEntities;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_entities_reminders", type="boolean", nullable=false)
     */
    private $glpiEntitiesReminders;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_entitydatas", type="boolean", nullable=false)
     */
    private $glpiEntitydatas;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_filesystems", type="boolean", nullable=false)
     */
    private $glpiFilesystems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_groups", type="boolean", nullable=false)
     */
    private $glpiGroups;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_groups_problems", type="boolean", nullable=false)
     */
    private $glpiGroupsProblems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_groups_reminders", type="boolean", nullable=false)
     */
    private $glpiGroupsReminders;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_groups_tickets", type="boolean", nullable=false)
     */
    private $glpiGroupsTickets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_groups_users", type="boolean", nullable=false)
     */
    private $glpiGroupsUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_holidays", type="boolean", nullable=false)
     */
    private $glpiHolidays;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_infocoms", type="boolean", nullable=false)
     */
    private $glpiInfocoms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_interfacetypes", type="boolean", nullable=false)
     */
    private $glpiInterfacetypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_items_problems", type="boolean", nullable=false)
     */
    private $glpiItemsProblems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_itilcategories", type="boolean", nullable=false)
     */
    private $glpiItilcategories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_knowbaseitemcategories", type="boolean", nullable=false)
     */
    private $glpiKnowbaseitemcategories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_knowbaseitems", type="boolean", nullable=false)
     */
    private $glpiKnowbaseitems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_links", type="boolean", nullable=false)
     */
    private $glpiLinks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_links_itemtypes", type="boolean", nullable=false)
     */
    private $glpiLinksItemtypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_locations", type="boolean", nullable=false)
     */
    private $glpiLocations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_manufacturers", type="boolean", nullable=false)
     */
    private $glpiManufacturers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_monitormodels", type="boolean", nullable=false)
     */
    private $glpiMonitormodels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_monitors", type="boolean", nullable=false)
     */
    private $glpiMonitors;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_monitortypes", type="boolean", nullable=false)
     */
    private $glpiMonitortypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_netpoints", type="boolean", nullable=false)
     */
    private $glpiNetpoints;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkequipmentfirmwares", type="boolean", nullable=false)
     */
    private $glpiNetworkequipmentfirmwares;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkequipmentmodels", type="boolean", nullable=false)
     */
    private $glpiNetworkequipmentmodels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkequipments", type="boolean", nullable=false)
     */
    private $glpiNetworkequipments;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkequipmenttypes", type="boolean", nullable=false)
     */
    private $glpiNetworkequipmenttypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkinterfaces", type="boolean", nullable=false)
     */
    private $glpiNetworkinterfaces;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkports", type="boolean", nullable=false)
     */
    private $glpiNetworkports;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkports_networkports", type="boolean", nullable=false)
     */
    private $glpiNetworkportsNetworkports;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkports_vlans", type="boolean", nullable=false)
     */
    private $glpiNetworkportsVlans;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networks", type="boolean", nullable=false)
     */
    private $glpiNetworks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_operatingsystems", type="boolean", nullable=false)
     */
    private $glpiOperatingsystems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_operatingsystemservicepacks", type="boolean", nullable=false)
     */
    private $glpiOperatingsystemservicepacks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_operatingsystemversions", type="boolean", nullable=false)
     */
    private $glpiOperatingsystemversions;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_peripheralmodels", type="boolean", nullable=false)
     */
    private $glpiPeripheralmodels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_peripherals", type="boolean", nullable=false)
     */
    private $glpiPeripherals;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_peripheraltypes", type="boolean", nullable=false)
     */
    private $glpiPeripheraltypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_phonemodels", type="boolean", nullable=false)
     */
    private $glpiPhonemodels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_phonepowersupplies", type="boolean", nullable=false)
     */
    private $glpiPhonepowersupplies;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_phones", type="boolean", nullable=false)
     */
    private $glpiPhones;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_phonetypes", type="boolean", nullable=false)
     */
    private $glpiPhonetypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_printermodels", type="boolean", nullable=false)
     */
    private $glpiPrintermodels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_printers", type="boolean", nullable=false)
     */
    private $glpiPrinters;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_printertypes", type="boolean", nullable=false)
     */
    private $glpiPrintertypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_problems", type="boolean", nullable=false)
     */
    private $glpiProblems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_problems_tickets", type="boolean", nullable=false)
     */
    private $glpiProblemsTickets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_problems_users", type="boolean", nullable=false)
     */
    private $glpiProblemsUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_problemtasks", type="boolean", nullable=false)
     */
    private $glpiProblemtasks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_profiles", type="boolean", nullable=false)
     */
    private $glpiProfiles;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_profiles_reminders", type="boolean", nullable=false)
     */
    private $glpiProfilesReminders;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_profiles_users", type="boolean", nullable=false)
     */
    private $glpiProfilesUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_registrykeys", type="boolean", nullable=false)
     */
    private $glpiRegistrykeys;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_reminders", type="boolean", nullable=false)
     */
    private $glpiReminders;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_reminders_users", type="boolean", nullable=false)
     */
    private $glpiRemindersUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_reservationitems", type="boolean", nullable=false)
     */
    private $glpiReservationitems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_reservations", type="boolean", nullable=false)
     */
    private $glpiReservations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_softwarecategories", type="boolean", nullable=false)
     */
    private $glpiSoftwarecategories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_softwarelicenses", type="boolean", nullable=false)
     */
    private $glpiSoftwarelicenses;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_softwarelicensetypes", type="boolean", nullable=false)
     */
    private $glpiSoftwarelicensetypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_softwares", type="boolean", nullable=false)
     */
    private $glpiSoftwares;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_softwareversions", type="boolean", nullable=false)
     */
    private $glpiSoftwareversions;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_solutiontypes", type="boolean", nullable=false)
     */
    private $glpiSolutiontypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_states", type="boolean", nullable=false)
     */
    private $glpiStates;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_suppliers", type="boolean", nullable=false)
     */
    private $glpiSuppliers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_suppliertypes", type="boolean", nullable=false)
     */
    private $glpiSuppliertypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_taskcategories", type="boolean", nullable=false)
     */
    private $glpiTaskcategories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_ticketfollowups", type="boolean", nullable=false)
     */
    private $glpiTicketfollowups;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_ticketrecurrents", type="boolean", nullable=false)
     */
    private $glpiTicketrecurrents;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_tickets", type="boolean", nullable=false)
     */
    private $glpiTickets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_ticketsatisfactions", type="boolean", nullable=false)
     */
    private $glpiTicketsatisfactions;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_tickets_tickets", type="boolean", nullable=false)
     */
    private $glpiTicketsTickets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_tickets_users", type="boolean", nullable=false)
     */
    private $glpiTicketsUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_tickettasks", type="boolean", nullable=false)
     */
    private $glpiTickettasks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_ticketvalidations", type="boolean", nullable=false)
     */
    private $glpiTicketvalidations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_usercategories", type="boolean", nullable=false)
     */
    private $glpiUsercategories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_useremails", type="boolean", nullable=false)
     */
    private $glpiUseremails;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_users", type="boolean", nullable=false)
     */
    private $glpiUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_usertitles", type="boolean", nullable=false)
     */
    private $glpiUsertitles;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_vlans", type="boolean", nullable=false)
     */
    private $glpiVlans;


}
