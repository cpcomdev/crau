<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginDatainjectionInfos
 *
 * @ORM\Table(name="glpi_plugin_datainjection_infos")
 * @ORM\Entity
 */
class GlpiPluginDatainjectionInfos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="models_id", type="integer", nullable=false)
     */
    private $modelsId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255, nullable=false)
     */
    private $itemtype;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=false)
     */
    private $value;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_mandatory", type="boolean", nullable=false)
     */
    private $isMandatory;


}
