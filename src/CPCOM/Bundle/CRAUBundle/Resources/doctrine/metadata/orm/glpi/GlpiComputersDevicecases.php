<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicecases
 *
 * @ORM\Table(name="glpi_computers_devicecases")
 * @ORM\Entity
 */
class GlpiComputersDevicecases
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicecases_id", type="integer", nullable=false)
     */
    private $devicecasesId;


}
