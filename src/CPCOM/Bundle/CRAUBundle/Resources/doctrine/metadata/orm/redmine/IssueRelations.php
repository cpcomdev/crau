<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IssueRelations
 *
 * @ORM\Table(name="issue_relations")
 * @ORM\Entity
 */
class IssueRelations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="issue_from_id", type="integer", nullable=false)
     */
    private $issueFromId;

    /**
     * @var integer
     *
     * @ORM\Column(name="issue_to_id", type="integer", nullable=false)
     */
    private $issueToId;

    /**
     * @var string
     *
     * @ORM\Column(name="relation_type", type="string", length=255, nullable=false)
     */
    private $relationType;

    /**
     * @var integer
     *
     * @ORM\Column(name="delay", type="integer", nullable=true)
     */
    private $delay;


}
