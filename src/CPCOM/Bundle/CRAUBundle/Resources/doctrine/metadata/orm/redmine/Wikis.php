<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Wikis
 *
 * @ORM\Table(name="wikis")
 * @ORM\Entity
 */
class Wikis
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var string
     *
     * @ORM\Column(name="start_page", type="string", length=255, nullable=false)
     */
    private $startPage;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;


}
