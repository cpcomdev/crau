<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CustomValues
 *
 * @ORM\Table(name="custom_values")
 * @ORM\Entity
 */
class CustomValues
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="customized_type", type="string", length=30, nullable=false)
     */
    private $customizedType;

    /**
     * @var integer
     *
     * @ORM\Column(name="customized_id", type="integer", nullable=false)
     */
    private $customizedId;

    /**
     * @var integer
     *
     * @ORM\Column(name="custom_field_id", type="integer", nullable=false)
     */
    private $customFieldId;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;


}
