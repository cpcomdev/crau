<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiOperatingsystems
 *
 * @ORM\Table(name="glpi_operatingsystems")
 * @ORM\Entity
 */
class GlpiOperatingsystems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


}
