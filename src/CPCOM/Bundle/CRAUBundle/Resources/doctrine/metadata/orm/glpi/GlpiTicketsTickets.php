<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiTicketsTickets
 *
 * @ORM\Table(name="glpi_tickets_tickets")
 * @ORM\Entity
 */
class GlpiTicketsTickets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickets_id_1", type="integer", nullable=false)
     */
    private $ticketsId1;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickets_id_2", type="integer", nullable=false)
     */
    private $ticketsId2;

    /**
     * @var integer
     *
     * @ORM\Column(name="link", type="integer", nullable=false)
     */
    private $link;


}
