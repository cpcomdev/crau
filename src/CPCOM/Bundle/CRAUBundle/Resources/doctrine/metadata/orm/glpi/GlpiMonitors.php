<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiMonitors
 *
 * @ORM\Table(name="glpi_monitors")
 * @ORM\Entity
 */
class GlpiMonitors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable=true)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_num", type="string", length=255, nullable=true)
     */
    private $contactNum;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id_tech", type="integer", nullable=false)
     */
    private $usersIdTech;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id_tech", type="integer", nullable=false)
     */
    private $groupsIdTech;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=255, nullable=true)
     */
    private $serial;

    /**
     * @var string
     *
     * @ORM\Column(name="otherserial", type="string", length=255, nullable=true)
     */
    private $otherserial;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer", nullable=false)
     */
    private $size;

    /**
     * @var boolean
     *
     * @ORM\Column(name="have_micro", type="boolean", nullable=false)
     */
    private $haveMicro;

    /**
     * @var boolean
     *
     * @ORM\Column(name="have_speaker", type="boolean", nullable=false)
     */
    private $haveSpeaker;

    /**
     * @var boolean
     *
     * @ORM\Column(name="have_subd", type="boolean", nullable=false)
     */
    private $haveSubd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="have_bnc", type="boolean", nullable=false)
     */
    private $haveBnc;

    /**
     * @var boolean
     *
     * @ORM\Column(name="have_dvi", type="boolean", nullable=false)
     */
    private $haveDvi;

    /**
     * @var boolean
     *
     * @ORM\Column(name="have_pivot", type="boolean", nullable=false)
     */
    private $havePivot;

    /**
     * @var boolean
     *
     * @ORM\Column(name="have_hdmi", type="boolean", nullable=false)
     */
    private $haveHdmi;

    /**
     * @var boolean
     *
     * @ORM\Column(name="have_displayport", type="boolean", nullable=false)
     */
    private $haveDisplayport;

    /**
     * @var integer
     *
     * @ORM\Column(name="locations_id", type="integer", nullable=false)
     */
    private $locationsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="monitortypes_id", type="integer", nullable=false)
     */
    private $monitortypesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="monitormodels_id", type="integer", nullable=false)
     */
    private $monitormodelsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_global", type="boolean", nullable=false)
     */
    private $isGlobal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_template", type="boolean", nullable=false)
     */
    private $isTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="template_name", type="string", length=255, nullable=true)
     */
    private $templateName;

    /**
     * @var string
     *
     * @ORM\Column(name="notepad", type="text", nullable=true)
     */
    private $notepad;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id", type="integer", nullable=false)
     */
    private $groupsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="states_id", type="integer", nullable=false)
     */
    private $statesId;

    /**
     * @var float
     *
     * @ORM\Column(name="ticket_tco", type="decimal", nullable=true)
     */
    private $ticketTco;


}
