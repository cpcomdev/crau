<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginReportsDoublonsBacklists
 *
 * @ORM\Table(name="glpi_plugin_reports_doublons_backlists")
 * @ORM\Entity
 */
class GlpiPluginReportsDoublonsBacklists
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="addr", type="string", length=255, nullable=true)
     */
    private $addr;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;


}
