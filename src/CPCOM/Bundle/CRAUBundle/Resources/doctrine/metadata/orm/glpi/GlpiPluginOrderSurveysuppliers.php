<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderSurveysuppliers
 *
 * @ORM\Table(name="glpi_plugin_order_surveysuppliers")
 * @ORM\Entity
 */
class GlpiPluginOrderSurveysuppliers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_orders_id", type="integer", nullable=false)
     */
    private $pluginOrderOrdersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer1", type="integer", nullable=false)
     */
    private $answer1;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer2", type="integer", nullable=false)
     */
    private $answer2;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer3", type="integer", nullable=false)
     */
    private $answer3;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer4", type="integer", nullable=false)
     */
    private $answer4;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer5", type="integer", nullable=false)
     */
    private $answer5;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


}
