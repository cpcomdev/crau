<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Workflows
 *
 * @ORM\Table(name="workflows")
 * @ORM\Entity
 */
class Workflows
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="tracker_id", type="integer", nullable=false)
     */
    private $trackerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="old_status_id", type="integer", nullable=false)
     */
    private $oldStatusId;

    /**
     * @var integer
     *
     * @ORM\Column(name="new_status_id", type="integer", nullable=false)
     */
    private $newStatusId;

    /**
     * @var integer
     *
     * @ORM\Column(name="role_id", type="integer", nullable=false)
     */
    private $roleId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="assignee", type="boolean", nullable=false)
     */
    private $assignee;

    /**
     * @var boolean
     *
     * @ORM\Column(name="author", type="boolean", nullable=false)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="field_name", type="string", length=30, nullable=true)
     */
    private $fieldName;

    /**
     * @var string
     *
     * @ORM\Column(name="rule", type="string", length=30, nullable=true)
     */
    private $rule;


}
