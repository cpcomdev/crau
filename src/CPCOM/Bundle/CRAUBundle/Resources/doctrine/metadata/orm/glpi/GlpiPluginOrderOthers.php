<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderOthers
 *
 * @ORM\Table(name="glpi_plugin_order_others")
 * @ORM\Entity
 */
class GlpiPluginOrderOthers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="othertypes_id", type="integer", nullable=false)
     */
    private $othertypesId;


}
