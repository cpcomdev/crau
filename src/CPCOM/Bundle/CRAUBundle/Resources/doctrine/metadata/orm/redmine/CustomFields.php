<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CustomFields
 *
 * @ORM\Table(name="custom_fields")
 * @ORM\Entity
 */
class CustomFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="field_format", type="string", length=30, nullable=false)
     */
    private $fieldFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="possible_values", type="text", nullable=true)
     */
    private $possibleValues;

    /**
     * @var string
     *
     * @ORM\Column(name="regexp", type="string", length=255, nullable=true)
     */
    private $regexp;

    /**
     * @var integer
     *
     * @ORM\Column(name="min_length", type="integer", nullable=false)
     */
    private $minLength;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_length", type="integer", nullable=false)
     */
    private $maxLength;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_required", type="boolean", nullable=false)
     */
    private $isRequired;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_for_all", type="boolean", nullable=false)
     */
    private $isForAll;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_filter", type="boolean", nullable=false)
     */
    private $isFilter;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var boolean
     *
     * @ORM\Column(name="searchable", type="boolean", nullable=true)
     */
    private $searchable;

    /**
     * @var string
     *
     * @ORM\Column(name="default_value", type="text", nullable=true)
     */
    private $defaultValue;

    /**
     * @var boolean
     *
     * @ORM\Column(name="editable", type="boolean", nullable=true)
     */
    private $editable;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", nullable=false)
     */
    private $visible;

    /**
     * @var boolean
     *
     * @ORM\Column(name="multiple", type="boolean", nullable=true)
     */
    private $multiple;


}
