<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IssueCategories
 *
 * @ORM\Table(name="issue_categories")
 * @ORM\Entity
 */
class IssueCategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="assigned_to_id", type="integer", nullable=true)
     */
    private $assignedToId;


}
