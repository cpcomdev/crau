<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiCrontasklogs
 *
 * @ORM\Table(name="glpi_crontasklogs")
 * @ORM\Entity
 */
class GlpiCrontasklogs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="crontasks_id", type="integer", nullable=false)
     */
    private $crontasksId;

    /**
     * @var integer
     *
     * @ORM\Column(name="crontasklogs_id", type="integer", nullable=false)
     */
    private $crontasklogsId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer", nullable=false)
     */
    private $state;

    /**
     * @var float
     *
     * @ORM\Column(name="elapsed", type="float", nullable=false)
     */
    private $elapsed;

    /**
     * @var integer
     *
     * @ORM\Column(name="volume", type="integer", nullable=false)
     */
    private $volume;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=255, nullable=true)
     */
    private $content;


}
