<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginGenericobjectProfiles
 *
 * @ORM\Table(name="glpi_plugin_genericobject_profiles")
 * @ORM\Entity
 */
class GlpiPluginGenericobjectProfiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="profiles_id", type="integer", nullable=false)
     */
    private $profilesId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255, nullable=true)
     */
    private $itemtype;

    /**
     * @var string
     *
     * @ORM\Column(name="right", type="string", length=1, nullable=true)
     */
    private $right;

    /**
     * @var string
     *
     * @ORM\Column(name="open_ticket", type="string", length=1, nullable=false)
     */
    private $openTicket;


}
