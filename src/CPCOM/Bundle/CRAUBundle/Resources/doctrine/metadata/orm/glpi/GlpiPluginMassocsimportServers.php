<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginMassocsimportServers
 *
 * @ORM\Table(name="glpi_plugin_massocsimport_servers")
 * @ORM\Entity
 */
class GlpiPluginMassocsimportServers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsservers_id", type="integer", nullable=false)
     */
    private $ocsserversId;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_ocsid", type="integer", nullable=true)
     */
    private $maxOcsid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="max_glpidate", type="datetime", nullable=true)
     */
    private $maxGlpidate;


}
