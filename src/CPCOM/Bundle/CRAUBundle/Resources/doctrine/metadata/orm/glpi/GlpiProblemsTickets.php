<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiProblemsTickets
 *
 * @ORM\Table(name="glpi_problems_tickets")
 * @ORM\Entity
 */
class GlpiProblemsTickets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="problems_id", type="integer", nullable=false)
     */
    private $problemsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickets_id", type="integer", nullable=false)
     */
    private $ticketsId;


}
