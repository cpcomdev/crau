<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiContracts
 *
 * @ORM\Table(name="glpi_contracts")
 * @ORM\Entity
 */
class GlpiContracts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="num", type="string", length=255, nullable=true)
     */
    private $num;

    /**
     * @var float
     *
     * @ORM\Column(name="cost", type="decimal", nullable=false)
     */
    private $cost;

    /**
     * @var integer
     *
     * @ORM\Column(name="contracttypes_id", type="integer", nullable=false)
     */
    private $contracttypesId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin_date", type="date", nullable=true)
     */
    private $beginDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="integer", nullable=false)
     */
    private $duration;

    /**
     * @var integer
     *
     * @ORM\Column(name="notice", type="integer", nullable=false)
     */
    private $notice;

    /**
     * @var integer
     *
     * @ORM\Column(name="periodicity", type="integer", nullable=false)
     */
    private $periodicity;

    /**
     * @var integer
     *
     * @ORM\Column(name="billing", type="integer", nullable=false)
     */
    private $billing;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="accounting_number", type="string", length=255, nullable=true)
     */
    private $accountingNumber;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="week_begin_hour", type="time", nullable=false)
     */
    private $weekBeginHour;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="week_end_hour", type="time", nullable=false)
     */
    private $weekEndHour;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="saturday_begin_hour", type="time", nullable=false)
     */
    private $saturdayBeginHour;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="saturday_end_hour", type="time", nullable=false)
     */
    private $saturdayEndHour;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_saturday", type="boolean", nullable=false)
     */
    private $useSaturday;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="monday_begin_hour", type="time", nullable=false)
     */
    private $mondayBeginHour;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="monday_end_hour", type="time", nullable=false)
     */
    private $mondayEndHour;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_monday", type="boolean", nullable=false)
     */
    private $useMonday;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_links_allowed", type="integer", nullable=false)
     */
    private $maxLinksAllowed;

    /**
     * @var string
     *
     * @ORM\Column(name="notepad", type="text", nullable=true)
     */
    private $notepad;

    /**
     * @var integer
     *
     * @ORM\Column(name="alert", type="integer", nullable=false)
     */
    private $alert;

    /**
     * @var integer
     *
     * @ORM\Column(name="renewal", type="integer", nullable=false)
     */
    private $renewal;

    /**
     * @var string
     *
     * @ORM\Column(name="template_name", type="string", length=255, nullable=true)
     */
    private $templateName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_template", type="boolean", nullable=false)
     */
    private $isTemplate;


}
