<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiKnowbaseitems
 *
 * @ORM\Table(name="glpi_knowbaseitems")
 * @ORM\Entity
 */
class GlpiKnowbaseitems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="knowbaseitemcategories_id", type="integer", nullable=false)
     */
    private $knowbaseitemcategoriesId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="text", nullable=true)
     */
    private $answer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_faq", type="boolean", nullable=false)
     */
    private $isFaq;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="view", type="integer", nullable=false)
     */
    private $view;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;


}
