<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiGroups
 *
 * @ORM\Table(name="glpi_groups")
 * @ORM\Entity
 */
class GlpiGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="ldap_field", type="string", length=255, nullable=true)
     */
    private $ldapField;

    /**
     * @var string
     *
     * @ORM\Column(name="ldap_value", type="text", nullable=true)
     */
    private $ldapValue;

    /**
     * @var string
     *
     * @ORM\Column(name="ldap_group_dn", type="text", nullable=true)
     */
    private $ldapGroupDn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id", type="integer", nullable=false)
     */
    private $groupsId;

    /**
     * @var string
     *
     * @ORM\Column(name="completename", type="text", nullable=true)
     */
    private $completename;

    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="integer", nullable=false)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="ancestors_cache", type="text", nullable=true)
     */
    private $ancestorsCache;

    /**
     * @var string
     *
     * @ORM\Column(name="sons_cache", type="text", nullable=true)
     */
    private $sonsCache;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_requester", type="boolean", nullable=false)
     */
    private $isRequester;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_assign", type="boolean", nullable=false)
     */
    private $isAssign;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_notify", type="boolean", nullable=false)
     */
    private $isNotify;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_itemgroup", type="boolean", nullable=false)
     */
    private $isItemgroup;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_usergroup", type="boolean", nullable=false)
     */
    private $isUsergroup;


}
