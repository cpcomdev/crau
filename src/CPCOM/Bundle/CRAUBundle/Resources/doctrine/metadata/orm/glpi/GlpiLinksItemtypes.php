<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiLinksItemtypes
 *
 * @ORM\Table(name="glpi_links_itemtypes")
 * @ORM\Entity
 */
class GlpiLinksItemtypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="links_id", type="integer", nullable=false)
     */
    private $linksId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=100, nullable=false)
     */
    private $itemtype;


}
