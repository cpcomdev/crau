<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderReferencesSuppliers
 *
 * @ORM\Table(name="glpi_plugin_order_references_suppliers")
 * @ORM\Entity
 */
class GlpiPluginOrderReferencesSuppliers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_references_id", type="integer", nullable=false)
     */
    private $pluginOrderReferencesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var float
     *
     * @ORM\Column(name="price_taxfree", type="decimal", nullable=false)
     */
    private $priceTaxfree;

    /**
     * @var string
     *
     * @ORM\Column(name="reference_code", type="string", length=255, nullable=true)
     */
    private $referenceCode;


}
