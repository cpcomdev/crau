<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiNotificationtargets
 *
 * @ORM\Table(name="glpi_notificationtargets")
 * @ORM\Entity
 */
class GlpiNotificationtargets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="items_id", type="integer", nullable=false)
     */
    private $itemsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="notifications_id", type="integer", nullable=false)
     */
    private $notificationsId;


}
