<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDeviceprocessors
 *
 * @ORM\Table(name="glpi_computers_deviceprocessors")
 * @ORM\Entity
 */
class GlpiComputersDeviceprocessors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="deviceprocessors_id", type="integer", nullable=false)
     */
    private $deviceprocessorsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="specificity", type="integer", nullable=false)
     */
    private $specificity;


}
