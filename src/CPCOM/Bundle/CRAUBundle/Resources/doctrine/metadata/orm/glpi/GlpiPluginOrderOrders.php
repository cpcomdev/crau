<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderOrders
 *
 * @ORM\Table(name="glpi_plugin_order_orders")
 * @ORM\Entity
 */
class GlpiPluginOrderOrders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_template", type="boolean", nullable=false)
     */
    private $isTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="template_name", type="string", length=255, nullable=true)
     */
    private $templateName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="num_order", type="string", length=255, nullable=true)
     */
    private $numOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="budgets_id", type="integer", nullable=false)
     */
    private $budgetsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_ordertaxes_id", type="integer", nullable=false)
     */
    private $pluginOrderOrdertaxesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_orderpayments_id", type="integer", nullable=false)
     */
    private $pluginOrderOrderpaymentsId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_date", type="date", nullable=true)
     */
    private $orderDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="duedate", type="date", nullable=true)
     */
    private $duedate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deliverydate", type="date", nullable=true)
     */
    private $deliverydate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_late", type="boolean", nullable=false)
     */
    private $isLate;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="contacts_id", type="integer", nullable=false)
     */
    private $contactsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="locations_id", type="integer", nullable=false)
     */
    private $locationsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_orderstates_id", type="integer", nullable=false)
     */
    private $pluginOrderOrderstatesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_billstates_id", type="integer", nullable=false)
     */
    private $pluginOrderBillstatesId;

    /**
     * @var float
     *
     * @ORM\Column(name="port_price", type="float", nullable=false)
     */
    private $portPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="notepad", type="text", nullable=true)
     */
    private $notepad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id", type="integer", nullable=false)
     */
    private $groupsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id_delivery", type="integer", nullable=false)
     */
    private $usersIdDelivery;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id_delivery", type="integer", nullable=false)
     */
    private $groupsIdDelivery;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_ordertypes_id", type="integer", nullable=false)
     */
    private $pluginOrderOrdertypesId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;


}
