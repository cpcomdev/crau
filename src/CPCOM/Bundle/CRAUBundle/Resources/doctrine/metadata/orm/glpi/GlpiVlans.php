<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiVlans
 *
 * @ORM\Table(name="glpi_vlans")
 * @ORM\Entity
 */
class GlpiVlans
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="tag", type="integer", nullable=false)
     */
    private $tag;


}
