<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiTicketsatisfactions
 *
 * @ORM\Table(name="glpi_ticketsatisfactions")
 * @ORM\Entity
 */
class GlpiTicketsatisfactions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickets_id", type="integer", nullable=false)
     */
    private $ticketsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_begin", type="datetime", nullable=true)
     */
    private $dateBegin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_answered", type="datetime", nullable=true)
     */
    private $dateAnswered;

    /**
     * @var integer
     *
     * @ORM\Column(name="satisfaction", type="integer", nullable=true)
     */
    private $satisfaction;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


}
