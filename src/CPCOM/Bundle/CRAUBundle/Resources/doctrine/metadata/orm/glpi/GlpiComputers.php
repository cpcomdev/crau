<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputers
 *
 * @ORM\Table(name="glpi_computers")
 * @ORM\Entity
 */
class GlpiComputers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=255, nullable=true)
     */
    private $serial;

    /**
     * @var string
     *
     * @ORM\Column(name="otherserial", type="string", length=255, nullable=true)
     */
    private $otherserial;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable=true)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_num", type="string", length=255, nullable=true)
     */
    private $contactNum;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id_tech", type="integer", nullable=false)
     */
    private $usersIdTech;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id_tech", type="integer", nullable=false)
     */
    private $groupsIdTech;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var integer
     *
     * @ORM\Column(name="operatingsystems_id", type="integer", nullable=false)
     */
    private $operatingsystemsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="operatingsystemversions_id", type="integer", nullable=false)
     */
    private $operatingsystemversionsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="operatingsystemservicepacks_id", type="integer", nullable=false)
     */
    private $operatingsystemservicepacksId;

    /**
     * @var string
     *
     * @ORM\Column(name="os_license_number", type="string", length=255, nullable=true)
     */
    private $osLicenseNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="os_licenseid", type="string", length=255, nullable=true)
     */
    private $osLicenseid;

    /**
     * @var integer
     *
     * @ORM\Column(name="autoupdatesystems_id", type="integer", nullable=false)
     */
    private $autoupdatesystemsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="locations_id", type="integer", nullable=false)
     */
    private $locationsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="domains_id", type="integer", nullable=false)
     */
    private $domainsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="networks_id", type="integer", nullable=false)
     */
    private $networksId;

    /**
     * @var integer
     *
     * @ORM\Column(name="computermodels_id", type="integer", nullable=false)
     */
    private $computermodelsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="computertypes_id", type="integer", nullable=false)
     */
    private $computertypesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_template", type="boolean", nullable=false)
     */
    private $isTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="template_name", type="string", length=255, nullable=true)
     */
    private $templateName;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted;

    /**
     * @var string
     *
     * @ORM\Column(name="notepad", type="text", nullable=true)
     */
    private $notepad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_ocs_import", type="boolean", nullable=false)
     */
    private $isOcsImport;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id", type="integer", nullable=false)
     */
    private $groupsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="states_id", type="integer", nullable=false)
     */
    private $statesId;

    /**
     * @var float
     *
     * @ORM\Column(name="ticket_tco", type="decimal", nullable=true)
     */
    private $ticketTco;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=255, nullable=true)
     */
    private $uuid;


}
