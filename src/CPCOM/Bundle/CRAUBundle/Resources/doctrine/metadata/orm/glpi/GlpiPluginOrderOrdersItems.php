<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderOrdersItems
 *
 * @ORM\Table(name="glpi_plugin_order_orders_items")
 * @ORM\Entity
 */
class GlpiPluginOrderOrdersItems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_orders_id", type="integer", nullable=false)
     */
    private $pluginOrderOrdersId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=100, nullable=false)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="items_id", type="integer", nullable=false)
     */
    private $itemsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_references_id", type="integer", nullable=false)
     */
    private $pluginOrderReferencesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_deliverystates_id", type="integer", nullable=false)
     */
    private $pluginOrderDeliverystatesId;

    /**
     * @var float
     *
     * @ORM\Column(name="plugin_order_ordertaxes_id", type="float", nullable=false)
     */
    private $pluginOrderOrdertaxesId;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_number", type="string", length=255, nullable=true)
     */
    private $deliveryNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_comment", type="text", nullable=true)
     */
    private $deliveryComment;

    /**
     * @var float
     *
     * @ORM\Column(name="price_taxfree", type="decimal", nullable=false)
     */
    private $priceTaxfree;

    /**
     * @var float
     *
     * @ORM\Column(name="price_discounted", type="decimal", nullable=false)
     */
    private $priceDiscounted;

    /**
     * @var float
     *
     * @ORM\Column(name="discount", type="decimal", nullable=false)
     */
    private $discount;

    /**
     * @var float
     *
     * @ORM\Column(name="price_ati", type="decimal", nullable=false)
     */
    private $priceAti;

    /**
     * @var integer
     *
     * @ORM\Column(name="states_id", type="integer", nullable=false)
     */
    private $statesId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="delivery_date", type="date", nullable=true)
     */
    private $deliveryDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_bills_id", type="integer", nullable=false)
     */
    private $pluginOrderBillsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_billstates_id", type="integer", nullable=false)
     */
    private $pluginOrderBillstatesId;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


}
