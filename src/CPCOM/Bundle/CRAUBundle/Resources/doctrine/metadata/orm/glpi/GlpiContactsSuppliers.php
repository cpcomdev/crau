<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiContactsSuppliers
 *
 * @ORM\Table(name="glpi_contacts_suppliers")
 * @ORM\Entity
 */
class GlpiContactsSuppliers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="contacts_id", type="integer", nullable=false)
     */
    private $contactsId;


}
