<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiDeviceharddrives
 *
 * @ORM\Table(name="glpi_deviceharddrives")
 * @ORM\Entity
 */
class GlpiDeviceharddrives
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="rpm", type="string", length=255, nullable=true)
     */
    private $rpm;

    /**
     * @var integer
     *
     * @ORM\Column(name="interfacetypes_id", type="integer", nullable=false)
     */
    private $interfacetypesId;

    /**
     * @var string
     *
     * @ORM\Column(name="cache", type="string", length=255, nullable=true)
     */
    private $cache;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="specif_default", type="integer", nullable=false)
     */
    private $specifDefault;


}
