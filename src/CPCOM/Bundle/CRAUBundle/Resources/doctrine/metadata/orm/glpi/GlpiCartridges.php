<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiCartridges
 *
 * @ORM\Table(name="glpi_cartridges")
 * @ORM\Entity
 */
class GlpiCartridges
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="cartridgeitems_id", type="integer", nullable=false)
     */
    private $cartridgeitemsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="printers_id", type="integer", nullable=false)
     */
    private $printersId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_in", type="date", nullable=true)
     */
    private $dateIn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_use", type="date", nullable=true)
     */
    private $dateUse;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_out", type="date", nullable=true)
     */
    private $dateOut;

    /**
     * @var integer
     *
     * @ORM\Column(name="pages", type="integer", nullable=false)
     */
    private $pages;


}
