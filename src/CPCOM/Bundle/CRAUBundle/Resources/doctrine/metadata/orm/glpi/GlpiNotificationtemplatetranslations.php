<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiNotificationtemplatetranslations
 *
 * @ORM\Table(name="glpi_notificationtemplatetranslations")
 * @ORM\Entity
 */
class GlpiNotificationtemplatetranslations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="notificationtemplates_id", type="integer", nullable=false)
     */
    private $notificationtemplatesId;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=5, nullable=false)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=false)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="content_text", type="text", nullable=true)
     */
    private $contentText;

    /**
     * @var string
     *
     * @ORM\Column(name="content_html", type="text", nullable=true)
     */
    private $contentHtml;


}
