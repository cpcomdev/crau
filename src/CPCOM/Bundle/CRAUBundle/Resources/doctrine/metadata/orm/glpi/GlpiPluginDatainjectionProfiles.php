<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginDatainjectionProfiles
 *
 * @ORM\Table(name="glpi_plugin_datainjection_profiles")
 * @ORM\Entity
 */
class GlpiPluginDatainjectionProfiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_default", type="boolean", nullable=false)
     */
    private $isDefault;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=1, nullable=true)
     */
    private $model;


}
