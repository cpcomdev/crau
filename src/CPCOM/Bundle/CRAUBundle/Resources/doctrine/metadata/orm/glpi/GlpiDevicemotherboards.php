<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiDevicemotherboards
 *
 * @ORM\Table(name="glpi_devicemotherboards")
 * @ORM\Entity
 */
class GlpiDevicemotherboards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="chipset", type="string", length=255, nullable=true)
     */
    private $chipset;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;


}
