<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginMassocsimportConfigs
 *
 * @ORM\Table(name="glpi_plugin_massocsimport_configs")
 * @ORM\Entity
 */
class GlpiPluginMassocsimportConfigs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="thread_log_frequency", type="integer", nullable=false)
     */
    private $threadLogFrequency;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_displayempty", type="integer", nullable=false)
     */
    private $isDisplayempty;

    /**
     * @var integer
     *
     * @ORM\Column(name="import_limit", type="integer", nullable=false)
     */
    private $importLimit;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsservers_id", type="integer", nullable=false)
     */
    private $ocsserversId;

    /**
     * @var integer
     *
     * @ORM\Column(name="delay_refresh", type="integer", nullable=false)
     */
    private $delayRefresh;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_ocs_update", type="boolean", nullable=false)
     */
    private $allowOcsUpdate;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


}
