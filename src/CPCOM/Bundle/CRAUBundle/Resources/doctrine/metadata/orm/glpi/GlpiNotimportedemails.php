<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiNotimportedemails
 *
 * @ORM\Table(name="glpi_notimportedemails")
 * @ORM\Entity
 */
class GlpiNotimportedemails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="from", type="string", length=255, nullable=false)
     */
    private $from;

    /**
     * @var string
     *
     * @ORM\Column(name="to", type="string", length=255, nullable=false)
     */
    private $to;

    /**
     * @var integer
     *
     * @ORM\Column(name="mailcollectors_id", type="integer", nullable=false)
     */
    private $mailcollectorsId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="text", nullable=true)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="messageid", type="string", length=255, nullable=false)
     */
    private $messageid;

    /**
     * @var integer
     *
     * @ORM\Column(name="reason", type="integer", nullable=false)
     */
    private $reason;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;


}
