<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IssueStatuses
 *
 * @ORM\Table(name="issue_statuses")
 * @ORM\Entity
 */
class IssueStatuses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_closed", type="boolean", nullable=false)
     */
    private $isClosed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_default", type="boolean", nullable=false)
     */
    private $isDefault;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_done_ratio", type="integer", nullable=true)
     */
    private $defaultDoneRatio;


}
