<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderBills
 *
 * @ORM\Table(name="glpi_plugin_order_bills")
 * @ORM\Entity
 */
class GlpiPluginOrderBills
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255, nullable=true)
     */
    private $number;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="billdate", type="datetime", nullable=true)
     */
    private $billdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="validationdate", type="datetime", nullable=true)
     */
    private $validationdate;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_billstates_id", type="integer", nullable=false)
     */
    private $pluginOrderBillstatesId;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="decimal", nullable=false)
     */
    private $value;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_billtypes_id", type="integer", nullable=false)
     */
    private $pluginOrderBilltypesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_orders_id", type="integer", nullable=false)
     */
    private $pluginOrderOrdersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id_validation", type="integer", nullable=false)
     */
    private $usersIdValidation;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_recursive", type="integer", nullable=false)
     */
    private $isRecursive;

    /**
     * @var string
     *
     * @ORM\Column(name="notepad", type="text", nullable=true)
     */
    private $notepad;


}
