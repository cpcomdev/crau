<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiRequesttypes
 *
 * @ORM\Table(name="glpi_requesttypes")
 * @ORM\Entity
 */
class GlpiRequesttypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_helpdesk_default", type="boolean", nullable=false)
     */
    private $isHelpdeskDefault;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_mail_default", type="boolean", nullable=false)
     */
    private $isMailDefault;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


}
