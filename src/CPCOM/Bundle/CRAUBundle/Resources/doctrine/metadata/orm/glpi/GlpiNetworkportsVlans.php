<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiNetworkportsVlans
 *
 * @ORM\Table(name="glpi_networkports_vlans")
 * @ORM\Entity
 */
class GlpiNetworkportsVlans
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="networkports_id", type="integer", nullable=false)
     */
    private $networkportsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="vlans_id", type="integer", nullable=false)
     */
    private $vlansId;


}
