<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiCartridgeitemsPrintermodels
 *
 * @ORM\Table(name="glpi_cartridgeitems_printermodels")
 * @ORM\Entity
 */
class GlpiCartridgeitemsPrintermodels
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="cartridgeitems_id", type="integer", nullable=false)
     */
    private $cartridgeitemsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="printermodels_id", type="integer", nullable=false)
     */
    private $printermodelsId;


}
