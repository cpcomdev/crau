<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiConfigs
 *
 * @ORM\Table(name="glpi_configs")
 * @ORM\Entity
 */
class GlpiConfigs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_jobs_at_login", type="boolean", nullable=false)
     */
    private $showJobsAtLogin;

    /**
     * @var integer
     *
     * @ORM\Column(name="cut", type="integer", nullable=false)
     */
    private $cut;

    /**
     * @var integer
     *
     * @ORM\Column(name="list_limit", type="integer", nullable=false)
     */
    private $listLimit;

    /**
     * @var integer
     *
     * @ORM\Column(name="list_limit_max", type="integer", nullable=false)
     */
    private $listLimitMax;

    /**
     * @var integer
     *
     * @ORM\Column(name="url_maxlength", type="integer", nullable=false)
     */
    private $urlMaxlength;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=10, nullable=true)
     */
    private $version;

    /**
     * @var integer
     *
     * @ORM\Column(name="event_loglevel", type="integer", nullable=false)
     */
    private $eventLoglevel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_mailing", type="boolean", nullable=false)
     */
    private $useMailing;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_email", type="string", length=255, nullable=true)
     */
    private $adminEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_email_name", type="string", length=255, nullable=true)
     */
    private $adminEmailName;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_reply", type="string", length=255, nullable=true)
     */
    private $adminReply;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_reply_name", type="string", length=255, nullable=true)
     */
    private $adminReplyName;

    /**
     * @var string
     *
     * @ORM\Column(name="mailing_signature", type="text", nullable=true)
     */
    private $mailingSignature;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_anonymous_helpdesk", type="boolean", nullable=false)
     */
    private $useAnonymousHelpdesk;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_anonymous_followups", type="boolean", nullable=false)
     */
    private $useAnonymousFollowups;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=10, nullable=true)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_1", type="string", length=20, nullable=true)
     */
    private $priority1;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_2", type="string", length=20, nullable=true)
     */
    private $priority2;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_3", type="string", length=20, nullable=true)
     */
    private $priority3;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_4", type="string", length=20, nullable=true)
     */
    private $priority4;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_5", type="string", length=20, nullable=true)
     */
    private $priority5;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_6", type="string", length=20, nullable=false)
     */
    private $priority6;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_tax", type="date", nullable=false)
     */
    private $dateTax;

    /**
     * @var string
     *
     * @ORM\Column(name="cas_host", type="string", length=255, nullable=true)
     */
    private $casHost;

    /**
     * @var integer
     *
     * @ORM\Column(name="cas_port", type="integer", nullable=false)
     */
    private $casPort;

    /**
     * @var string
     *
     * @ORM\Column(name="cas_uri", type="string", length=255, nullable=true)
     */
    private $casUri;

    /**
     * @var string
     *
     * @ORM\Column(name="cas_logout", type="string", length=255, nullable=true)
     */
    private $casLogout;

    /**
     * @var integer
     *
     * @ORM\Column(name="authldaps_id_extra", type="integer", nullable=false)
     */
    private $authldapsIdExtra;

    /**
     * @var string
     *
     * @ORM\Column(name="existing_auth_server_field", type="string", length=255, nullable=true)
     */
    private $existingAuthServerField;

    /**
     * @var boolean
     *
     * @ORM\Column(name="existing_auth_server_field_clean_domain", type="boolean", nullable=false)
     */
    private $existingAuthServerFieldCleanDomain;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="planning_begin", type="time", nullable=false)
     */
    private $planningBegin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="planning_end", type="time", nullable=false)
     */
    private $planningEnd;

    /**
     * @var integer
     *
     * @ORM\Column(name="utf8_conv", type="integer", nullable=false)
     */
    private $utf8Conv;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_public_faq", type="boolean", nullable=false)
     */
    private $usePublicFaq;

    /**
     * @var string
     *
     * @ORM\Column(name="url_base", type="string", length=255, nullable=true)
     */
    private $urlBase;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_link_in_mail", type="boolean", nullable=false)
     */
    private $showLinkInMail;

    /**
     * @var string
     *
     * @ORM\Column(name="text_login", type="text", nullable=true)
     */
    private $textLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="founded_new_version", type="string", length=10, nullable=true)
     */
    private $foundedNewVersion;

    /**
     * @var integer
     *
     * @ORM\Column(name="dropdown_max", type="integer", nullable=false)
     */
    private $dropdownMax;

    /**
     * @var string
     *
     * @ORM\Column(name="ajax_wildcard", type="string", length=1, nullable=true)
     */
    private $ajaxWildcard;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_ajax", type="boolean", nullable=false)
     */
    private $useAjax;

    /**
     * @var integer
     *
     * @ORM\Column(name="ajax_min_textsearch_load", type="integer", nullable=false)
     */
    private $ajaxMinTextsearchLoad;

    /**
     * @var integer
     *
     * @ORM\Column(name="ajax_limit_count", type="integer", nullable=false)
     */
    private $ajaxLimitCount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_ajax_autocompletion", type="boolean", nullable=false)
     */
    private $useAjaxAutocompletion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_users_auto_add", type="boolean", nullable=false)
     */
    private $isUsersAutoAdd;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_format", type="integer", nullable=false)
     */
    private $dateFormat;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_format", type="integer", nullable=false)
     */
    private $numberFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="csv_delimiter", type="string", length=1, nullable=false)
     */
    private $csvDelimiter;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_ids_visible", type="boolean", nullable=false)
     */
    private $isIdsVisible;

    /**
     * @var integer
     *
     * @ORM\Column(name="dropdown_chars_limit", type="integer", nullable=false)
     */
    private $dropdownCharsLimit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_ocs_mode", type="boolean", nullable=false)
     */
    private $useOcsMode;

    /**
     * @var integer
     *
     * @ORM\Column(name="smtp_mode", type="integer", nullable=false)
     */
    private $smtpMode;

    /**
     * @var string
     *
     * @ORM\Column(name="smtp_host", type="string", length=255, nullable=true)
     */
    private $smtpHost;

    /**
     * @var integer
     *
     * @ORM\Column(name="smtp_port", type="integer", nullable=false)
     */
    private $smtpPort;

    /**
     * @var string
     *
     * @ORM\Column(name="smtp_username", type="string", length=255, nullable=true)
     */
    private $smtpUsername;

    /**
     * @var string
     *
     * @ORM\Column(name="proxy_name", type="string", length=255, nullable=true)
     */
    private $proxyName;

    /**
     * @var integer
     *
     * @ORM\Column(name="proxy_port", type="integer", nullable=false)
     */
    private $proxyPort;

    /**
     * @var string
     *
     * @ORM\Column(name="proxy_user", type="string", length=255, nullable=true)
     */
    private $proxyUser;

    /**
     * @var boolean
     *
     * @ORM\Column(name="add_followup_on_update_ticket", type="boolean", nullable=false)
     */
    private $addFollowupOnUpdateTicket;

    /**
     * @var boolean
     *
     * @ORM\Column(name="keep_tickets_on_delete", type="boolean", nullable=false)
     */
    private $keepTicketsOnDelete;

    /**
     * @var integer
     *
     * @ORM\Column(name="time_step", type="integer", nullable=true)
     */
    private $timeStep;

    /**
     * @var integer
     *
     * @ORM\Column(name="decimal_number", type="integer", nullable=true)
     */
    private $decimalNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="helpdesk_doc_url", type="string", length=255, nullable=true)
     */
    private $helpdeskDocUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="central_doc_url", type="string", length=255, nullable=true)
     */
    private $centralDocUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="documentcategories_id_forticket", type="integer", nullable=false)
     */
    private $documentcategoriesIdForticket;

    /**
     * @var integer
     *
     * @ORM\Column(name="monitors_management_restrict", type="integer", nullable=false)
     */
    private $monitorsManagementRestrict;

    /**
     * @var integer
     *
     * @ORM\Column(name="phones_management_restrict", type="integer", nullable=false)
     */
    private $phonesManagementRestrict;

    /**
     * @var integer
     *
     * @ORM\Column(name="peripherals_management_restrict", type="integer", nullable=false)
     */
    private $peripheralsManagementRestrict;

    /**
     * @var integer
     *
     * @ORM\Column(name="printers_management_restrict", type="integer", nullable=false)
     */
    private $printersManagementRestrict;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_log_in_files", type="boolean", nullable=false)
     */
    private $useLogInFiles;

    /**
     * @var integer
     *
     * @ORM\Column(name="time_offset", type="integer", nullable=false)
     */
    private $timeOffset;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_contact_autoupdate", type="boolean", nullable=false)
     */
    private $isContactAutoupdate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_user_autoupdate", type="boolean", nullable=false)
     */
    private $isUserAutoupdate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_group_autoupdate", type="boolean", nullable=false)
     */
    private $isGroupAutoupdate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_location_autoupdate", type="boolean", nullable=false)
     */
    private $isLocationAutoupdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="state_autoupdate_mode", type="integer", nullable=false)
     */
    private $stateAutoupdateMode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_contact_autoclean", type="boolean", nullable=false)
     */
    private $isContactAutoclean;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_user_autoclean", type="boolean", nullable=false)
     */
    private $isUserAutoclean;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_group_autoclean", type="boolean", nullable=false)
     */
    private $isGroupAutoclean;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_location_autoclean", type="boolean", nullable=false)
     */
    private $isLocationAutoclean;

    /**
     * @var integer
     *
     * @ORM\Column(name="state_autoclean_mode", type="integer", nullable=false)
     */
    private $stateAutocleanMode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_flat_dropdowntree", type="boolean", nullable=false)
     */
    private $useFlatDropdowntree;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_autoname_by_entity", type="boolean", nullable=false)
     */
    private $useAutonameByEntity;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_categorized_soft_expanded", type="boolean", nullable=false)
     */
    private $isCategorizedSoftExpanded;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_not_categorized_soft_expanded", type="boolean", nullable=false)
     */
    private $isNotCategorizedSoftExpanded;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwarecategories_id_ondelete", type="integer", nullable=false)
     */
    private $softwarecategoriesIdOndelete;

    /**
     * @var string
     *
     * @ORM\Column(name="x509_email_field", type="string", length=255, nullable=true)
     */
    private $x509EmailField;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_mailcollector_filesize_max", type="integer", nullable=false)
     */
    private $defaultMailcollectorFilesizeMax;

    /**
     * @var boolean
     *
     * @ORM\Column(name="followup_private", type="boolean", nullable=false)
     */
    private $followupPrivate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="task_private", type="boolean", nullable=false)
     */
    private $taskPrivate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="default_software_helpdesk_visible", type="boolean", nullable=false)
     */
    private $defaultSoftwareHelpdeskVisible;

    /**
     * @var integer
     *
     * @ORM\Column(name="names_format", type="integer", nullable=false)
     */
    private $namesFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="default_graphtype", type="string", length=3, nullable=false)
     */
    private $defaultGraphtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_requesttypes_id", type="integer", nullable=false)
     */
    private $defaultRequesttypesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_noright_users_add", type="boolean", nullable=false)
     */
    private $useNorightUsersAdd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cron_limit", type="boolean", nullable=false)
     */
    private $cronLimit;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_matrix", type="string", length=255, nullable=true)
     */
    private $priorityMatrix;

    /**
     * @var integer
     *
     * @ORM\Column(name="urgency_mask", type="integer", nullable=false)
     */
    private $urgencyMask;

    /**
     * @var integer
     *
     * @ORM\Column(name="impact_mask", type="integer", nullable=false)
     */
    private $impactMask;

    /**
     * @var boolean
     *
     * @ORM\Column(name="user_deleted_ldap", type="boolean", nullable=false)
     */
    private $userDeletedLdap;

    /**
     * @var boolean
     *
     * @ORM\Column(name="auto_create_infocoms", type="boolean", nullable=false)
     */
    private $autoCreateInfocoms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_slave_for_search", type="boolean", nullable=false)
     */
    private $useSlaveForSearch;

    /**
     * @var string
     *
     * @ORM\Column(name="proxy_passwd", type="string", length=255, nullable=true)
     */
    private $proxyPasswd;

    /**
     * @var string
     *
     * @ORM\Column(name="smtp_passwd", type="string", length=255, nullable=true)
     */
    private $smtpPasswd;

    /**
     * @var integer
     *
     * @ORM\Column(name="transfers_id_auto", type="integer", nullable=false)
     */
    private $transfersIdAuto;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_count_on_tabs", type="boolean", nullable=false)
     */
    private $showCountOnTabs;

    /**
     * @var integer
     *
     * @ORM\Column(name="refresh_ticket_list", type="integer", nullable=false)
     */
    private $refreshTicketList;

    /**
     * @var boolean
     *
     * @ORM\Column(name="set_default_tech", type="boolean", nullable=false)
     */
    private $setDefaultTech;

    /**
     * @var integer
     *
     * @ORM\Column(name="allow_search_view", type="integer", nullable=false)
     */
    private $allowSearchView;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_search_all", type="boolean", nullable=false)
     */
    private $allowSearchAll;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_search_global", type="boolean", nullable=false)
     */
    private $allowSearchGlobal;

    /**
     * @var integer
     *
     * @ORM\Column(name="display_count_on_home", type="integer", nullable=false)
     */
    private $displayCountOnHome;


}
