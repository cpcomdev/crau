<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiSoftwarelicenses
 *
 * @ORM\Table(name="glpi_softwarelicenses")
 * @ORM\Entity
 */
class GlpiSoftwarelicenses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwares_id", type="integer", nullable=false)
     */
    private $softwaresId;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", nullable=false)
     */
    private $number;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwarelicensetypes_id", type="integer", nullable=false)
     */
    private $softwarelicensetypesId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=255, nullable=true)
     */
    private $serial;

    /**
     * @var string
     *
     * @ORM\Column(name="otherserial", type="string", length=255, nullable=true)
     */
    private $otherserial;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwareversions_id_buy", type="integer", nullable=false)
     */
    private $softwareversionsIdBuy;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwareversions_id_use", type="integer", nullable=false)
     */
    private $softwareversionsIdUse;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire", type="date", nullable=true)
     */
    private $expire;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;


}
