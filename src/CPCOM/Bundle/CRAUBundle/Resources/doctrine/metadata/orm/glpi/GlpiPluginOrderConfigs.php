<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderConfigs
 *
 * @ORM\Table(name="glpi_plugin_order_configs")
 * @ORM\Entity
 */
class GlpiPluginOrderConfigs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_validation", type="boolean", nullable=false)
     */
    private $useValidation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_supplier_satisfaction", type="boolean", nullable=false)
     */
    private $useSupplierSatisfaction;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_supplier_informations", type="boolean", nullable=false)
     */
    private $useSupplierInformations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_supplier_infos", type="boolean", nullable=false)
     */
    private $useSupplierInfos;

    /**
     * @var boolean
     *
     * @ORM\Column(name="generate_order_pdf", type="boolean", nullable=false)
     */
    private $generateOrderPdf;

    /**
     * @var boolean
     *
     * @ORM\Column(name="copy_documents", type="boolean", nullable=false)
     */
    private $copyDocuments;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_taxes", type="integer", nullable=false)
     */
    private $defaultTaxes;

    /**
     * @var integer
     *
     * @ORM\Column(name="generate_assets", type="integer", nullable=false)
     */
    private $generateAssets;

    /**
     * @var string
     *
     * @ORM\Column(name="generated_name", type="string", length=255, nullable=true)
     */
    private $generatedName;

    /**
     * @var string
     *
     * @ORM\Column(name="generated_serial", type="string", length=255, nullable=true)
     */
    private $generatedSerial;

    /**
     * @var string
     *
     * @ORM\Column(name="generated_otherserial", type="string", length=255, nullable=true)
     */
    private $generatedOtherserial;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_asset_states_id", type="integer", nullable=false)
     */
    private $defaultAssetStatesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="generate_ticket", type="integer", nullable=false)
     */
    private $generateTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="generated_title", type="string", length=255, nullable=true)
     */
    private $generatedTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="generated_content", type="text", nullable=true)
     */
    private $generatedContent;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_ticketcategories_id", type="integer", nullable=false)
     */
    private $defaultTicketcategoriesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_status_draft", type="integer", nullable=false)
     */
    private $orderStatusDraft;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_status_waiting_approval", type="integer", nullable=false)
     */
    private $orderStatusWaitingApproval;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_status_approved", type="integer", nullable=false)
     */
    private $orderStatusApproved;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_status_partially_delivred", type="integer", nullable=false)
     */
    private $orderStatusPartiallyDelivred;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_status_completly_delivered", type="integer", nullable=false)
     */
    private $orderStatusCompletlyDelivered;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_status_canceled", type="integer", nullable=false)
     */
    private $orderStatusCanceled;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_status_paid", type="integer", nullable=false)
     */
    private $orderStatusPaid;

    /**
     * @var string
     *
     * @ORM\Column(name="shoudbedelivered_color", type="string", length=20, nullable=true)
     */
    private $shoudbedeliveredColor;

    /**
     * @var integer
     *
     * @ORM\Column(name="documentcategories_id", type="integer", nullable=false)
     */
    private $documentcategoriesId;


}
