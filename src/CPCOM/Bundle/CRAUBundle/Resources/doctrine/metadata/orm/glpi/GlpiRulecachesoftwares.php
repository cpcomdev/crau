<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiRulecachesoftwares
 *
 * @ORM\Table(name="glpi_rulecachesoftwares")
 * @ORM\Entity
 */
class GlpiRulecachesoftwares
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="old_value", type="string", length=255, nullable=true)
     */
    private $oldValue;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer", type="string", length=255, nullable=false)
     */
    private $manufacturer;

    /**
     * @var integer
     *
     * @ORM\Column(name="rules_id", type="integer", nullable=false)
     */
    private $rulesId;

    /**
     * @var string
     *
     * @ORM\Column(name="new_value", type="string", length=255, nullable=true)
     */
    private $newValue;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=255, nullable=true)
     */
    private $version;

    /**
     * @var string
     *
     * @ORM\Column(name="new_manufacturer", type="string", length=255, nullable=false)
     */
    private $newManufacturer;

    /**
     * @var string
     *
     * @ORM\Column(name="ignore_ocs_import", type="string", length=1, nullable=true)
     */
    private $ignoreOcsImport;

    /**
     * @var string
     *
     * @ORM\Column(name="is_helpdesk_visible", type="string", length=1, nullable=true)
     */
    private $isHelpdeskVisible;

    /**
     * @var string
     *
     * @ORM\Column(name="entities_id", type="string", length=255, nullable=true)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="new_entities_id", type="string", length=255, nullable=true)
     */
    private $newEntitiesId;


}
