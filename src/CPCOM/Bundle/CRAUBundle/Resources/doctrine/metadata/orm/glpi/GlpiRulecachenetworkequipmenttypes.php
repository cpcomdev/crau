<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiRulecachenetworkequipmenttypes
 *
 * @ORM\Table(name="glpi_rulecachenetworkequipmenttypes")
 * @ORM\Entity
 */
class GlpiRulecachenetworkequipmenttypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="old_value", type="string", length=255, nullable=true)
     */
    private $oldValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="rules_id", type="integer", nullable=false)
     */
    private $rulesId;

    /**
     * @var string
     *
     * @ORM\Column(name="new_value", type="string", length=255, nullable=true)
     */
    private $newValue;


}
