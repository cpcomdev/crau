<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiOcsservers
 *
 * @ORM\Table(name="glpi_ocsservers")
 * @ORM\Entity
 */
class GlpiOcsservers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_db_user", type="string", length=255, nullable=true)
     */
    private $ocsDbUser;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_db_passwd", type="string", length=255, nullable=true)
     */
    private $ocsDbPasswd;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_db_host", type="string", length=255, nullable=true)
     */
    private $ocsDbHost;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_db_name", type="string", length=255, nullable=true)
     */
    private $ocsDbName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ocs_db_utf8", type="boolean", nullable=false)
     */
    private $ocsDbUtf8;

    /**
     * @var integer
     *
     * @ORM\Column(name="checksum", type="integer", nullable=false)
     */
    private $checksum;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_periph", type="boolean", nullable=false)
     */
    private $importPeriph;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_monitor", type="boolean", nullable=false)
     */
    private $importMonitor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_software", type="boolean", nullable=false)
     */
    private $importSoftware;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_printer", type="boolean", nullable=false)
     */
    private $importPrinter;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_name", type="boolean", nullable=false)
     */
    private $importGeneralName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_os", type="boolean", nullable=false)
     */
    private $importGeneralOs;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_serial", type="boolean", nullable=false)
     */
    private $importGeneralSerial;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_model", type="boolean", nullable=false)
     */
    private $importGeneralModel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_manufacturer", type="boolean", nullable=false)
     */
    private $importGeneralManufacturer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_type", type="boolean", nullable=false)
     */
    private $importGeneralType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_domain", type="boolean", nullable=false)
     */
    private $importGeneralDomain;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_contact", type="boolean", nullable=false)
     */
    private $importGeneralContact;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_comment", type="boolean", nullable=false)
     */
    private $importGeneralComment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_processor", type="boolean", nullable=false)
     */
    private $importDeviceProcessor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_memory", type="boolean", nullable=false)
     */
    private $importDeviceMemory;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_hdd", type="boolean", nullable=false)
     */
    private $importDeviceHdd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_iface", type="boolean", nullable=false)
     */
    private $importDeviceIface;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_gfxcard", type="boolean", nullable=false)
     */
    private $importDeviceGfxcard;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_sound", type="boolean", nullable=false)
     */
    private $importDeviceSound;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_drive", type="boolean", nullable=false)
     */
    private $importDeviceDrive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_port", type="boolean", nullable=false)
     */
    private $importDevicePort;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_modem", type="boolean", nullable=false)
     */
    private $importDeviceModem;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_registry", type="boolean", nullable=false)
     */
    private $importRegistry;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_os_serial", type="boolean", nullable=false)
     */
    private $importOsSerial;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_ip", type="boolean", nullable=false)
     */
    private $importIp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_disk", type="boolean", nullable=false)
     */
    private $importDisk;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_monitor_comment", type="boolean", nullable=false)
     */
    private $importMonitorComment;

    /**
     * @var integer
     *
     * @ORM\Column(name="states_id_default", type="integer", nullable=false)
     */
    private $statesIdDefault;

    /**
     * @var string
     *
     * @ORM\Column(name="tag_limit", type="string", length=255, nullable=true)
     */
    private $tagLimit;

    /**
     * @var string
     *
     * @ORM\Column(name="tag_exclude", type="string", length=255, nullable=true)
     */
    private $tagExclude;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_soft_dict", type="boolean", nullable=false)
     */
    private $useSoftDict;

    /**
     * @var integer
     *
     * @ORM\Column(name="cron_sync_number", type="integer", nullable=true)
     */
    private $cronSyncNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="deconnection_behavior", type="string", length=255, nullable=true)
     */
    private $deconnectionBehavior;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_url", type="string", length=255, nullable=true)
     */
    private $ocsUrl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var string
     *
     * @ORM\Column(name="deleted_behavior", type="string", length=255, nullable=false)
     */
    private $deletedBehavior;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_vms", type="boolean", nullable=false)
     */
    private $importVms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_uuid", type="boolean", nullable=false)
     */
    private $importGeneralUuid;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_version", type="string", length=255, nullable=true)
     */
    private $ocsVersion;


}
