<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginDumpentityProfiles
 *
 * @ORM\Table(name="glpi_plugin_dumpentity_profiles")
 * @ORM\Entity
 */
class GlpiPluginDumpentityProfiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="models_id", type="integer", nullable=false)
     */
    private $modelsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="profiles_id", type="integer", nullable=false)
     */
    private $profilesId;


}
