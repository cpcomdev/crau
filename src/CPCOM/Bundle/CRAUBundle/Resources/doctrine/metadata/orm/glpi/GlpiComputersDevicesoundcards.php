<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicesoundcards
 *
 * @ORM\Table(name="glpi_computers_devicesoundcards")
 * @ORM\Entity
 */
class GlpiComputersDevicesoundcards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicesoundcards_id", type="integer", nullable=false)
     */
    private $devicesoundcardsId;


}
