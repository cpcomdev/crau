<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * WikiRedirects
 *
 * @ORM\Table(name="wiki_redirects")
 * @ORM\Entity
 */
class WikiRedirects
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="wiki_id", type="integer", nullable=false)
     */
    private $wikiId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="redirects_to", type="string", length=255, nullable=true)
     */
    private $redirectsTo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     */
    private $createdOn;


}
