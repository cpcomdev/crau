<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiLogs
 *
 * @ORM\Table(name="glpi_logs")
 * @ORM\Entity
 */
class GlpiLogs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=100, nullable=false)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="items_id", type="integer", nullable=false)
     */
    private $itemsId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype_link", type="string", length=100, nullable=false)
     */
    private $itemtypeLink;

    /**
     * @var integer
     *
     * @ORM\Column(name="linked_action", type="integer", nullable=false)
     */
    private $linkedAction;

    /**
     * @var string
     *
     * @ORM\Column(name="user_name", type="string", length=255, nullable=true)
     */
    private $userName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_search_option", type="integer", nullable=false)
     */
    private $idSearchOption;

    /**
     * @var string
     *
     * @ORM\Column(name="old_value", type="string", length=255, nullable=true)
     */
    private $oldValue;

    /**
     * @var string
     *
     * @ORM\Column(name="new_value", type="string", length=255, nullable=true)
     */
    private $newValue;


}
