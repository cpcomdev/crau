<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiRegistrykeys
 *
 * @ORM\Table(name="glpi_registrykeys")
 * @ORM\Entity
 */
class GlpiRegistrykeys
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var string
     *
     * @ORM\Column(name="hive", type="string", length=255, nullable=true)
     */
    private $hive;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=true)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_name", type="string", length=255, nullable=true)
     */
    private $ocsName;


}
