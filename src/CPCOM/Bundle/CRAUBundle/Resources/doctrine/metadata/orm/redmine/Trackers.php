<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Trackers
 *
 * @ORM\Table(name="trackers")
 * @ORM\Entity
 */
class Trackers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_in_chlog", type="boolean", nullable=false)
     */
    private $isInChlog;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_in_roadmap", type="boolean", nullable=false)
     */
    private $isInRoadmap;

    /**
     * @var integer
     *
     * @ORM\Column(name="fields_bits", type="integer", nullable=true)
     */
    private $fieldsBits;


}
