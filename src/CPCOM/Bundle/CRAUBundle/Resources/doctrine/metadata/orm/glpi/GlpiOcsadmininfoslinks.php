<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiOcsadmininfoslinks
 *
 * @ORM\Table(name="glpi_ocsadmininfoslinks")
 * @ORM\Entity
 */
class GlpiOcsadmininfoslinks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="glpi_column", type="string", length=255, nullable=true)
     */
    private $glpiColumn;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_column", type="string", length=255, nullable=true)
     */
    private $ocsColumn;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsservers_id", type="integer", nullable=false)
     */
    private $ocsserversId;


}
