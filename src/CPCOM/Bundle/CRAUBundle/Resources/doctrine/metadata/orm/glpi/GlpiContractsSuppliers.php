<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiContractsSuppliers
 *
 * @ORM\Table(name="glpi_contracts_suppliers")
 * @ORM\Entity
 */
class GlpiContractsSuppliers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="contracts_id", type="integer", nullable=false)
     */
    private $contractsId;


}
