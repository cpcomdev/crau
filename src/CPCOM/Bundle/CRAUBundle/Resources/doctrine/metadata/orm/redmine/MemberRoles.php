<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MemberRoles
 *
 * @ORM\Table(name="member_roles")
 * @ORM\Entity
 */
class MemberRoles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="member_id", type="integer", nullable=false)
     */
    private $memberId;

    /**
     * @var integer
     *
     * @ORM\Column(name="role_id", type="integer", nullable=false)
     */
    private $roleId;

    /**
     * @var integer
     *
     * @ORM\Column(name="inherited_from", type="integer", nullable=true)
     */
    private $inheritedFrom;


}
