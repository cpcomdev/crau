<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiTransfers
 *
 * @ORM\Table(name="glpi_transfers")
 * @ORM\Entity
 */
class GlpiTransfers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_ticket", type="integer", nullable=false)
     */
    private $keepTicket;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_networklink", type="integer", nullable=false)
     */
    private $keepNetworklink;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_reservation", type="integer", nullable=false)
     */
    private $keepReservation;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_history", type="integer", nullable=false)
     */
    private $keepHistory;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_device", type="integer", nullable=false)
     */
    private $keepDevice;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_infocom", type="integer", nullable=false)
     */
    private $keepInfocom;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_dc_monitor", type="integer", nullable=false)
     */
    private $keepDcMonitor;

    /**
     * @var integer
     *
     * @ORM\Column(name="clean_dc_monitor", type="integer", nullable=false)
     */
    private $cleanDcMonitor;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_dc_phone", type="integer", nullable=false)
     */
    private $keepDcPhone;

    /**
     * @var integer
     *
     * @ORM\Column(name="clean_dc_phone", type="integer", nullable=false)
     */
    private $cleanDcPhone;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_dc_peripheral", type="integer", nullable=false)
     */
    private $keepDcPeripheral;

    /**
     * @var integer
     *
     * @ORM\Column(name="clean_dc_peripheral", type="integer", nullable=false)
     */
    private $cleanDcPeripheral;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_dc_printer", type="integer", nullable=false)
     */
    private $keepDcPrinter;

    /**
     * @var integer
     *
     * @ORM\Column(name="clean_dc_printer", type="integer", nullable=false)
     */
    private $cleanDcPrinter;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_supplier", type="integer", nullable=false)
     */
    private $keepSupplier;

    /**
     * @var integer
     *
     * @ORM\Column(name="clean_supplier", type="integer", nullable=false)
     */
    private $cleanSupplier;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_contact", type="integer", nullable=false)
     */
    private $keepContact;

    /**
     * @var integer
     *
     * @ORM\Column(name="clean_contact", type="integer", nullable=false)
     */
    private $cleanContact;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_contract", type="integer", nullable=false)
     */
    private $keepContract;

    /**
     * @var integer
     *
     * @ORM\Column(name="clean_contract", type="integer", nullable=false)
     */
    private $cleanContract;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_software", type="integer", nullable=false)
     */
    private $keepSoftware;

    /**
     * @var integer
     *
     * @ORM\Column(name="clean_software", type="integer", nullable=false)
     */
    private $cleanSoftware;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_document", type="integer", nullable=false)
     */
    private $keepDocument;

    /**
     * @var integer
     *
     * @ORM\Column(name="clean_document", type="integer", nullable=false)
     */
    private $cleanDocument;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_cartridgeitem", type="integer", nullable=false)
     */
    private $keepCartridgeitem;

    /**
     * @var integer
     *
     * @ORM\Column(name="clean_cartridgeitem", type="integer", nullable=false)
     */
    private $cleanCartridgeitem;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_cartridge", type="integer", nullable=false)
     */
    private $keepCartridge;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_consumable", type="integer", nullable=false)
     */
    private $keepConsumable;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="keep_disk", type="integer", nullable=false)
     */
    private $keepDisk;


}
