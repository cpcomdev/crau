<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Repositories
 *
 * @ORM\Table(name="repositories")
 * @ORM\Entity
 */
class Repositories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=60, nullable=true)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="root_url", type="string", length=255, nullable=true)
     */
    private $rootUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="path_encoding", type="string", length=64, nullable=true)
     */
    private $pathEncoding;

    /**
     * @var string
     *
     * @ORM\Column(name="log_encoding", type="string", length=64, nullable=true)
     */
    private $logEncoding;

    /**
     * @var string
     *
     * @ORM\Column(name="extra_info", type="text", nullable=true)
     */
    private $extraInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=255, nullable=true)
     */
    private $identifier;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_default", type="boolean", nullable=true)
     */
    private $isDefault;


}
