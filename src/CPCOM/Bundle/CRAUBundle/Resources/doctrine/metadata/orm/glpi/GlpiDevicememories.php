<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiDevicememories
 *
 * @ORM\Table(name="glpi_devicememories")
 * @ORM\Entity
 */
class GlpiDevicememories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="frequence", type="string", length=255, nullable=true)
     */
    private $frequence;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="specif_default", type="integer", nullable=false)
     */
    private $specifDefault;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicememorytypes_id", type="integer", nullable=false)
     */
    private $devicememorytypesId;


}
