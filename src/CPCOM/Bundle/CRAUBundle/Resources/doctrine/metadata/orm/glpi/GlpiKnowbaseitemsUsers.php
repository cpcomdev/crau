<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiKnowbaseitemsUsers
 *
 * @ORM\Table(name="glpi_knowbaseitems_users")
 * @ORM\Entity
 */
class GlpiKnowbaseitemsUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="knowbaseitems_id", type="integer", nullable=false)
     */
    private $knowbaseitemsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;


}
