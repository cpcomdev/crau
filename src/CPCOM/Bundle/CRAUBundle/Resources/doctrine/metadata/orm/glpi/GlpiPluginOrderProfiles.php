<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderProfiles
 *
 * @ORM\Table(name="glpi_plugin_order_profiles")
 * @ORM\Entity
 */
class GlpiPluginOrderProfiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="profiles_id", type="integer", nullable=false)
     */
    private $profilesId;

    /**
     * @var string
     *
     * @ORM\Column(name="order", type="string", length=1, nullable=true)
     */
    private $order;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=1, nullable=true)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="validation", type="string", length=1, nullable=true)
     */
    private $validation;

    /**
     * @var string
     *
     * @ORM\Column(name="cancel", type="string", length=1, nullable=true)
     */
    private $cancel;

    /**
     * @var string
     *
     * @ORM\Column(name="undo_validation", type="string", length=1, nullable=true)
     */
    private $undoValidation;

    /**
     * @var string
     *
     * @ORM\Column(name="bill", type="string", length=1, nullable=true)
     */
    private $bill;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery", type="string", length=1, nullable=true)
     */
    private $delivery;

    /**
     * @var string
     *
     * @ORM\Column(name="generate_order_odt", type="string", length=1, nullable=true)
     */
    private $generateOrderOdt;


}
