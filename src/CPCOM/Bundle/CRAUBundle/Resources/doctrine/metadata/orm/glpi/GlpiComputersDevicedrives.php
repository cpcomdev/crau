<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicedrives
 *
 * @ORM\Table(name="glpi_computers_devicedrives")
 * @ORM\Entity
 */
class GlpiComputersDevicedrives
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicedrives_id", type="integer", nullable=false)
     */
    private $devicedrivesId;


}
