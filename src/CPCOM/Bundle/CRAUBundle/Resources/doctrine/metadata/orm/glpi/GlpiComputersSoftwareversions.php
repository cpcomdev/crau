<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersSoftwareversions
 *
 * @ORM\Table(name="glpi_computers_softwareversions")
 * @ORM\Entity
 */
class GlpiComputersSoftwareversions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwareversions_id", type="integer", nullable=false)
     */
    private $softwareversionsId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_template", type="boolean", nullable=false)
     */
    private $isTemplate;


}
