<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiCalendarsHolidays
 *
 * @ORM\Table(name="glpi_calendars_holidays")
 * @ORM\Entity
 */
class GlpiCalendarsHolidays
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="calendars_id", type="integer", nullable=false)
     */
    private $calendarsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="holidays_id", type="integer", nullable=false)
     */
    private $holidaysId;


}
