<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiRemindersUsers
 *
 * @ORM\Table(name="glpi_reminders_users")
 * @ORM\Entity
 */
class GlpiRemindersUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="reminders_id", type="integer", nullable=false)
     */
    private $remindersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;


}
