<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiNetworkports
 *
 * @ORM\Table(name="glpi_networkports")
 * @ORM\Entity
 */
class GlpiNetworkports
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="items_id", type="integer", nullable=false)
     */
    private $itemsId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=100, nullable=false)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="logical_number", type="integer", nullable=false)
     */
    private $logicalNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=true)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="mac", type="string", length=255, nullable=true)
     */
    private $mac;

    /**
     * @var integer
     *
     * @ORM\Column(name="networkinterfaces_id", type="integer", nullable=false)
     */
    private $networkinterfacesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="netpoints_id", type="integer", nullable=false)
     */
    private $netpointsId;

    /**
     * @var string
     *
     * @ORM\Column(name="netmask", type="string", length=255, nullable=true)
     */
    private $netmask;

    /**
     * @var string
     *
     * @ORM\Column(name="gateway", type="string", length=255, nullable=true)
     */
    private $gateway;

    /**
     * @var string
     *
     * @ORM\Column(name="subnet", type="string", length=255, nullable=true)
     */
    private $subnet;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


}
