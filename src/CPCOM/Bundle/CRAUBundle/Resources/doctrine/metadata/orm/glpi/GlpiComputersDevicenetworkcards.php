<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicenetworkcards
 *
 * @ORM\Table(name="glpi_computers_devicenetworkcards")
 * @ORM\Entity
 */
class GlpiComputersDevicenetworkcards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicenetworkcards_id", type="integer", nullable=false)
     */
    private $devicenetworkcardsId;

    /**
     * @var string
     *
     * @ORM\Column(name="specificity", type="string", length=255, nullable=true)
     */
    private $specificity;


}
