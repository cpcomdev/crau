<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicememories
 *
 * @ORM\Table(name="glpi_computers_devicememories")
 * @ORM\Entity
 */
class GlpiComputersDevicememories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicememories_id", type="integer", nullable=false)
     */
    private $devicememoriesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="specificity", type="integer", nullable=false)
     */
    private $specificity;


}
