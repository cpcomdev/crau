<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiTicketvalidations
 *
 * @ORM\Table(name="glpi_ticketvalidations")
 * @ORM\Entity
 */
class GlpiTicketvalidations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickets_id", type="integer", nullable=false)
     */
    private $ticketsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id_validate", type="integer", nullable=false)
     */
    private $usersIdValidate;

    /**
     * @var string
     *
     * @ORM\Column(name="comment_submission", type="text", nullable=true)
     */
    private $commentSubmission;

    /**
     * @var string
     *
     * @ORM\Column(name="comment_validation", type="text", nullable=true)
     */
    private $commentValidation;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="submission_date", type="datetime", nullable=true)
     */
    private $submissionDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="validation_date", type="datetime", nullable=true)
     */
    private $validationDate;


}
