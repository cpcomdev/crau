<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiVirtualmachinetypes
 *
 * @ORM\Table(name="glpi_virtualmachinetypes")
 * @ORM\Entity
 */
class GlpiVirtualmachinetypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=false)
     */
    private $comment;


}
