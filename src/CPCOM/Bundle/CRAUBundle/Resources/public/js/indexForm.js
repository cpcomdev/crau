$(document).ready(function() {
    // Initialise Hidden
    // Date perso
    if ($('select#CRAU_datePredefini').val() != '') {
        $('.date_perso').hide();
    }

    // Affiche Date Perso
    $('select#CRAU_datePredefini').change( function() {
        if ($('select#CRAU_datePredefini').val() == '') {
            $('.date_perso').show();
        } else {
            $('.date_perso').hide();
            $('#CRAU_dateDebut').val(null);
            $('#CRAU_dateFin').val(null);
        }
    });

    // Effet Accordeon
    // Utilisateurs
    $("tr.tache").hide();
    $("tr.ligneutilisateur").click(function(){
        $("tr."+$(this).attr('id')).toggle();
        if ($(this).children(':first').children('div').attr('id') == 'close') {
            $(this).children(':first').children('div').attr('id', 'open');
        } else {
            $(this).children(':first').children('div').attr('id', 'close');
        }
    });
    $("tr.ligneutilisateur").each(function() {
        if($("tr."+$(this).attr('id'))[0]) {
            $(this).css('cursor', 'pointer');
            $(this).children(':first').prepend('<div id="close"></div>');
        }
    });
    // Service
    $("tr.services").click(function(){
        $("tr."+$(this).attr('id')).toggle();
        $("tr.tache."+$(this).attr('id')).hide();
        $("tr.ligneutilisateur."+$(this).attr('id')).children('td').children('div').attr('id', 'close');
        if ($(this).children(':first').children('div').attr('id') == 'close') {
            $(this).children(':first').children('div').attr('id', 'open');
        } else {
            $(this).children(':first').children('div').attr('id', 'close');
        }
    });
    $("tr.services").each(function() {
        if($("tr."+$(this).attr('class').split(' ')[1])) {
            $(this).css('cursor', 'pointer');
            $(this).children(':first').prepend('<div id="open"></div>');
        }
    });
});