<?php

namespace CPCOM\Bundle\CRAUBundle\Model;

use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Requêtes Glpi
 */
class GlpiRequest
{
    private $em;

    /**
     * @param type $em 
     */
    public function __construct($em)
    {
        $this->em = $em;
    }

    /**
     * Temps groupés par utilisateur, par jour sur la période sélectionnée
     * 
     * @param object $crau
     * 
     * @return array
     */
    public function findElapsedTime($crau)
    {
        $requetenative = "
            SELECT CONCAT(LOWER(glpi_users.realname), '_', LOWER(glpi_users.firstname)) AS 'glpi_keyname',
                glpi_users.realname AS 'glpi_nom',
                glpi_users.firstname AS 'glpi_prenom',
                SUM(glpi_tickettasks.actiontime) AS 'glpi_tpsBrutSeconde',
                SUM(glpi_tickettasks.actiontime / 60 / 60) AS 'glpi_tpsHeure',
                IF (glpi_tickettasks.begin IS NOT NULL, DATE(glpi_tickettasks.begin), DATE(glpi_tickettasks.date)) AS 'glpi_dateTps',
                glpi_tickets.id AS 'glpi_taskId',
                glpi_tickets.name AS 'glpi_taskSubject',
                'Incident' AS 'glip_taskCategorie'
            FROM glpi_tickettasks 
            JOIN glpi_users ON users_id = glpi_users.id
            JOIN glpi_tickets on glpi_tickettasks.tickets_id= glpi_tickets.id
        ";

        // Entre
        if ($crau->getDatePredefini() == null) {
            if (is_object($crau->getDateDebut())) {
                $crau->setDateDebut($crau->getDateDebut()->format('Y-m-d'));
            }
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Aujourd'hui
        } else if ($crau->getDatePredefini() == 1) {
            $dateNow = new \DateTime('now');

            $crau->setDateDebut($dateNow->format('Y-m-d'));
            $crau->setDateFin($dateNow->format('Y-m-d'));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Hier
        } else if ($crau->getDatePredefini() == 2) {
            $dateNow = new \DateTime('now');

            $crau->setDateDebut(date('Y-m-d', strtotime("-1 days", strtotime($dateNow->format('Y-m-d')))));
            $crau->setDateFin(date('Y-m-d', strtotime("-1 days", strtotime($dateNow->format('Y-m-d')))));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Cette Semaine
        } else if ($crau->getDatePredefini() == 3) {
            $dateNow = new \DateTime('now');

            // Si c'est lundi on donne le resultat du jour
            if (date('Y-m-d', strtotime("last monday", strtotime($dateNow->format('Y-m-d')))) == date('Y-m-d', strtotime("-7 days", strtotime($dateNow->format('Y-m-d'))))) {
                $crau->setDateDebut($dateNow->format('Y-m-d'));
                $crau->setDateFin($dateNow->format('Y-m-d'));

            // Sinon la semaine jusqu'a aujourd'hui
            } else {
                $crau->setDateDebut(date('Y-m-d', strtotime("last monday", strtotime($dateNow->format('Y-m-d')))));
                $crau->setDateFin($dateNow->format('Y-m-d'));
            }
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // La semaine dernière
        } else if ($crau->getDatePredefini() == 4) {
            $dateNow = new \DateTime('now');

            // Si c'est lundi on donne le lundi de la semaine dernière
            if (date('Y-m-d', strtotime("last monday", strtotime($dateNow->format('Y-m-d')))) == date('Y-m-d', strtotime("-7 days", strtotime($dateNow->format('Y-m-d'))))) {
                $crau->setDateDebut(date('Y-m-d', strtotime("last monday", strtotime($dateNow->format('Y-m-d')))));

            // Sinon le dernier lundi du dernier lundi
            } else {
                $lastMonday = date('Y-m-d', strtotime("last monday", strtotime($dateNow->format('Y-m-d'))));
                $crau->setDateDebut(date('Y-m-d', strtotime("last monday", strtotime($lastMonday))));
            }
            $crau->setDateFin(date('Y-m-d', strtotime("+6 days", strtotime($crau->getDateDebut()))));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Les 7 derniers jours
        } else if ($crau->getDatePredefini() == 5) {
            $dateNow = new \DateTime('now');

            $crau->setDateDebut(date('Y-m-d', strtotime("-7 days", strtotime($dateNow->format('Y-m-d')))));
            $crau->setDateFin($dateNow->format('Y-m-d'));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Ce mois-ci
        } else if ($crau->getDatePredefini() == 6) {
            $dateNow = new \DateTime('now');

            $crau->setDateDebut($dateNow->format('Y-m-01'));
            $crau->setDateFin($dateNow->format('Y-m-d'));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Le mois dernier
        } else if ($crau->getDatePredefini() == 7) {
            $dateNow = new \DateTime('now');

            $crau->setDateDebut(date('Y-m-d', strtotime("last month", strtotime($dateNow->format('Y-m')))));
            $crau->setDateFin(date('Y-m-d', strtotime("-1 days", strtotime($dateNow->format('Y-m')))));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Les 30 derniers jours
        } else if ($crau->getDatePredefini() == 8) {
            $dateNow = new \DateTime('now');

            $crau->setDateDebut(date('Y-m-d', strtotime("-30 days", strtotime($dateNow->format('Y-m-d')))));
            $crau->setDateFin($dateNow->format('Y-m-d'));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Cette année
        } else if ($crau->getDatePredefini() == 9) {
            $dateNow = new \DateTime('now');

            $crau->setDateDebut($dateNow->format('Y-01-01'));
            $crau->setDateFin($dateNow->format('Y-m-d'));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);
        }

        $requetenative .= "
            GROUP BY IF (glpi_tickettasks.begin IS NOT NULL, DATE(glpi_tickettasks.begin), DATE(glpi_tickettasks.date)),
                glpi_users.realname,
                glpi_users.firstname,
                glpi_tickets.id
            ORDER BY IF (glpi_tickettasks.begin IS NOT NULL, DATE(glpi_tickettasks.begin), DATE(glpi_tickettasks.date)), glpi_users.realname, glpi_users.firstname, glpi_tickets.id
        ";

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('glpi_keyname', 'glpiKeyname');
        $rsm->addScalarResult('glpi_nom', 'glpiNom');
        $rsm->addScalarResult('glpi_prenom', 'glpiPrenom');
        $rsm->addScalarResult('glpi_tpsBrutSeconde', 'glpiTempsBrutSeconde');
        $rsm->addScalarResult('glpi_tpsHeure', 'glpiTempsHeure');
        $rsm->addScalarResult('glpi_dateTps', 'glpiDateTemps');
        $rsm->addScalarResult('glpi_taskId', 'glpiTaskId');
        $rsm->addScalarResult('glpi_taskSubject', 'glpiTaskSubject');
        $rsm->addScalarResult('glip_taskCategorie', 'glpiTaskCategorie');

        $query = $this->em->createNativeQuery($requetenative, $rsm);

        $result = $query->getResult();

        return $result;
    }

    /**
     * Permet d'ajouter un jour a la date de fin pour compter la journée complète
     * 
     * @param object $crau 
     */
    private function dateFinAddOneDay($crau)
    {
        //Pour afficher le jour de la date de fin je rajoute +1 jour et je met en strictement inférieur
        if (is_object($crau->getDateFin())) {
            $crau->setDateFin(date('Y-m-d', strtotime("+1 days", strtotime($crau->getDateFin()->format('Y-m-d')))));
        } else {
            $crau->setDateFin(date('Y-m-d', strtotime("+1 days", strtotime($crau->getDateFin()))));
        }
    }

    /**
     * Permet d'ajouter un jour a la date de fin pour compter la journée complète
     * 
     * @param string $requetenative requetenative
     * @param object $crau          crau
     * 
     * @return string 
     */
    private function addClauseWhereForDate($requetenative, $crau)
    {
        $requetenative .= "
            WHERE 
                IF(glpi_tickettasks.begin IS NOT NULL, DATE(glpi_tickettasks.begin), DATE(glpi_tickettasks.date)) >= '".$crau->getDateDebut()."'
                AND IF(glpi_tickettasks.begin IS NOT NULL, DATE(glpi_tickettasks.begin), DATE(glpi_tickettasks.date)) < '".$crau->getDateFin()."'
        ";

        return $requetenative;
    }

    /**
     * @return type 
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param type $em 
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

}
