<?php

namespace CPCOM\Bundle\CRAUBundle\Model;

use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Requêtes Redmine
 */
class RedmineRequest
{
    private $em;

    /**
     * @param type $em 
     */
    public function __construct($em)
    {
        $this->em = $em;
    }

    /**
     * Liste des utilisateurs avec societe/service/nom/prenom/clef redmine
     * 
     * @return type 
     */
    public function findAllUsers()
    {
        $requetenative = "
            SELECT IF(u2.lastname IS NULL, 'Autres', u2.lastname) as 'redmine_groupe',
                u1.lastname as 'redmine_nom',
                u1.firstname as 'redmine_prenom',
                u1.mail as 'redmine_mail',
                CONCAT(LOWER(u1.lastname),'_',LOWER(u1.firstname)) as 'redmine_keyname',
                custom_values.value as 'redmine_societe'
            FROM users u1
                LEFT JOIN groups_users gu ON u1.id = gu.user_id
                LEFT JOIN users u2 ON u2.id = gu.group_id
                JOIN custom_values ON customized_id = u1.id
                JOIN custom_fields ON custom_values.custom_field_id = custom_fields.id
            WHERE custom_fields.type = 'UserCustomField'
                AND custom_fields.name = 'Societe'
                AND (custom_values.value = 'CPCOM'
                OR custom_values.value = 'CPCOM-Presta')
            ORDER BY u2.lastname, u1.lastname, u1.firstname, u1.firstname
        ";

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('redmine_groupe', 'redmineGroupe');
        $rsm->addScalarResult('redmine_nom', 'redmineNom');
        $rsm->addScalarResult('redmine_prenom', 'redminePrenom');
        $rsm->addScalarResult('redmine_mail', 'redmineMail');
        $rsm->addScalarResult('redmine_keyname', 'redmineKeyname');
        $rsm->addScalarResult('redmine_societe', 'redmineSociete');

        $query = $this->em->createNativeQuery($requetenative, $rsm);

        $result = $query->getResult();

        return $result;
    }

    /**
     * Temps groupés par utilisateur, par jour sur la période sélectionnée
     * 
     * @param type $crau
     * 
     * @return type 
     */
    public function findElapsedTime($crau)
    {
        $requetenative = "
            SELECT CONCAT(LOWER(u1.lastname),'_',LOWER(u1.firstname)) as 'redmine_keyname',
                u1.lastname as 'redmine_nom',
                u1.firstname as 'redmine_prenom',
                SUM(time_entries.hours) AS 'redmine_tpsHeure',
                DATE(time_entries.spent_on) AS 'redmine_dateTps',
                issues.id AS 'redmine_taskId',
                issues.subject AS 'redmine_taskSubject',
                trackers.name AS 'redmine_taskCategorie',
                custom_values.value AS 'redmine_societe'
            FROM users u1
                LEFT JOIN groups_users gu ON u1.id = gu.user_id
                LEFT JOIN users u2 ON u2.id = gu.group_id
                JOIN custom_values ON customized_id = u1.id
                JOIN custom_fields ON custom_values.custom_field_id = custom_fields.id
                JOIN time_entries ON time_entries.user_id = u1.id
                JOIN issues ON time_entries.issue_id = issues.id
                JOIN  trackers on issues.tracker_id = trackers.id
            WHERE custom_fields.type = 'UserCustomField'
                AND custom_fields.name = 'Societe'
                AND (custom_values.value = 'CPCOM'
                OR custom_values.value = 'CPCOM-Presta')
        ";

        // Entre
        if ($crau->getDatePredefini() == null) {
            if (is_object($crau->getDateDebut())) {
                $crau->setDateDebut($crau->getDateDebut()->format('Y-m-d'));
            }
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);
            $crau->setDateFin(date('Y-m-d', strtotime("-1 days", strtotime($crau->getDateFin()))));

        // Aujourd'hui
        } else if ($crau->getDatePredefini() == 1) {
            $dateNow = new \DateTime('now');

            $crau->setDateDebut($dateNow->format('Y-m-d'));
            $crau->setDateFin($dateNow->format('Y-m-d'));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Hier
        } else if ($crau->getDatePredefini() == 2) {
            $dateNow = new \DateTime('now');

            $crau->setDateDebut(date('Y-m-d', strtotime("-1 days", strtotime($dateNow->format('Y-m-d')))));
            $crau->setDateFin(date('Y-m-d', strtotime("-1 days", strtotime($dateNow->format('Y-m-d')))));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Cette Semaine
        } else if ($crau->getDatePredefini() == 3) {
            $dateNow = new \DateTime('now');

            // Si c'est lundi on donne le resultat du jour
            if (date('Y-m-d', strtotime("last monday", strtotime($dateNow->format('Y-m-d')))) == date('Y-m-d', strtotime("-7 days", strtotime($dateNow->format('Y-m-d'))))) {
                $crau->setDateDebut($dateNow->format('Y-m-d'));
                $crau->setDateFin($dateNow->format('Y-m-d'));

            // Sinon la semaine jusqu'a aujourd'hui
            } else {
                $crau->setDateDebut(date('Y-m-d', strtotime("last monday", strtotime($dateNow->format('Y-m-d')))));
                $crau->setDateFin($dateNow->format('Y-m-d'));
            }
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // La semaine dernière
        } else if ($crau->getDatePredefini() == 4) {
            $dateNow = new \DateTime('now');

            // Si c'est lundi on donne le lundi de la semaine dernière
            if (date('Y-m-d', strtotime("last monday", strtotime($dateNow->format('Y-m-d')))) == date('Y-m-d', strtotime("-7 days", strtotime($dateNow->format('Y-m-d'))))) {
                $crau->setDateDebut(date('Y-m-d', strtotime("last monday", strtotime($dateNow->format('Y-m-d')))));

            // Sinon le dernier lundi du dernier lundi
            } else {
                $lastMonday = date('Y-m-d', strtotime("last monday", strtotime($dateNow->format('Y-m-d'))));
                $crau->setDateDebut(date('Y-m-d', strtotime("last monday", strtotime($lastMonday))));
            }
            $crau->setDateFin(date('Y-m-d', strtotime("+6 days", strtotime($crau->getDateDebut()))));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Les 7 derniers jours
        } else if ($crau->getDatePredefini() == 5) {
            $dateNow = new \DateTime('now');

            $crau->setDateDebut(date('Y-m-d', strtotime("-7 days", strtotime($dateNow->format('Y-m-d')))));
            $crau->setDateFin($dateNow->format('Y-m-d'));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Ce mois-ci
        } else if ($crau->getDatePredefini() == 6) {
            $dateNow = new \DateTime('now');

            $crau->setDateDebut($dateNow->format('Y-m-01'));
            $crau->setDateFin($dateNow->format('Y-m-d'));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Le mois dernier
        } else if ($crau->getDatePredefini() == 7) {
            $dateNow = new \DateTime('now');

            $crau->setDateDebut(date('Y-m-d', strtotime("last month", strtotime($dateNow->format('Y-m')))));
            $crau->setDateFin(date('Y-m-d', strtotime("-1 days", strtotime($dateNow->format('Y-m')))));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Les 30 derniers jours
        } else if ($crau->getDatePredefini() == 8) {
            $dateNow = new \DateTime('now');

            $crau->setDateDebut(date('Y-m-d', strtotime("-30 days", strtotime($dateNow->format('Y-m-d')))));
            $crau->setDateFin($dateNow->format('Y-m-d'));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);

        // Cette année
        } else if ($crau->getDatePredefini() == 9) {
            $dateNow = new \DateTime('now');

            $crau->setDateDebut($dateNow->format('Y-01-01'));
            $crau->setDateFin($dateNow->format('Y-m-d'));
            $this->dateFinAddOneDay($crau);
            $requetenative = $this->addClauseWhereForDate($requetenative, $crau);
        }

        $requetenative .= "
            GROUP BY time_entries.spent_on, u1.lastname, u1.firstname, time_entries.issue_id
            ORDER BY DATE(time_entries.spent_on), u1.lastname, u1.firstname, issues.id
        ";

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('redmine_tpsHeure', 'redmineTempsHeure');
        $rsm->addScalarResult('redmine_dateTps', 'redmineDateTemps');
        $rsm->addScalarResult('redmine_taskId', 'redmineTaskId');
        $rsm->addScalarResult('redmine_taskSubject', 'redmineTaskSubject');
        $rsm->addScalarResult('redmine_taskCategorie', 'redmineTaskCategorie');
        $rsm->addScalarResult('redmine_nom', 'redmineNom');
        $rsm->addScalarResult('redmine_prenom', 'redminePrenom');
        $rsm->addScalarResult('redmine_keyname', 'redmineKeyname');
        $rsm->addScalarResult('redmine_societe', 'redmineSociete');

        $query = $this->em->createNativeQuery($requetenative, $rsm);

        $result = $query->getResult();

        return $result;
    }

    /**
     * Permet d'ajouter un jour a la date de fin pour compter la journée complète
     * 
     * @param type $crau 
     */
    private function dateFinAddOneDay($crau)
    {
        //Pour afficher le jour de la date de fin je rajoute +1 jour et je met en strictement inférieur
        if (is_object($crau->getDateFin())) {
            $crau->setDateFin(date('Y-m-d', strtotime("+1 days", strtotime($crau->getDateFin()->format('Y-m-d')))));
        } else {
            $crau->setDateFin(date('Y-m-d', strtotime("+1 days", strtotime($crau->getDateFin()))));
        }
    }

    /**
     * Permet d'ajouter un jour a la date de fin pour compter la journée complète
     * 
     * @param string $requetenative requetenative
     * @param type   $crau          crau
     * 
     * @return string
     */
    private function addClauseWhereForDate($requetenative, $crau)
    {
        $requetenative .= "
            AND time_entries.spent_on >= '".$crau->getDateDebut()."'
            AND time_entries.spent_on < '".$crau->getDateFin()."'
        ";

        return $requetenative;
    }

    /**
     * @return type 
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param type $em 
     */
    public function setEm($em)
    {
        $this->em = $em;
    }
}
