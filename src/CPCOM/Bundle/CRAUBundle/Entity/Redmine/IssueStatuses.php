<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * IssueStatuses
 *
 * @ORM\Table(name="issue_statuses")
 * @ORM\Entity
 */
class IssueStatuses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_closed", type="boolean", nullable=false)
     */
    private $isClosed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_default", type="boolean", nullable=false)
     */
    private $isDefault;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_done_ratio", type="integer", nullable=true)
     */
    private $defaultDoneRatio;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return IssueStatuses
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isClosed
     *
     * @param boolean $isClosed
     * 
     * @return IssueStatuses
     */
    public function setIsClosed($isClosed)
    {
        $this->isClosed = $isClosed;

        return $this;
    }

    /**
     * Get isClosed
     *
     * @return boolean 
     */
    public function getIsClosed()
    {
        return $this->isClosed;
    }

    /**
     * Set isDefault
     *
     * @param boolean $isDefault
     * 
     * @return IssueStatuses
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return boolean 
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set position
     *
     * @param integer $position
     * 
     * @return IssueStatuses
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set defaultDoneRatio
     *
     * @param integer $defaultDoneRatio
     * 
     * @return IssueStatuses
     */
    public function setDefaultDoneRatio($defaultDoneRatio)
    {
        $this->defaultDoneRatio = $defaultDoneRatio;

        return $this;
    }

    /**
     * Get defaultDoneRatio
     *
     * @return integer 
     */
    public function getDefaultDoneRatio()
    {
        return $this->defaultDoneRatio;
    }
}