<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comments
 *
 * @ORM\Table(name="comments")
 * @ORM\Entity
 */
class Comments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="commented_type", type="string", length=30, nullable=false)
     */
    private $commentedType;

    /**
     * @var integer
     *
     * @ORM\Column(name="commented_id", type="integer", nullable=false)
     */
    private $commentedId;

    /**
     * @var integer
     *
     * @ORM\Column(name="author_id", type="integer", nullable=false)
     */
    private $authorId;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="text", nullable=true)
     */
    private $comments;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     */
    private $createdOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_on", type="datetime", nullable=false)
     */
    private $updatedOn;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set commentedType
     *
     * @param string $commentedType
     * 
     * @return Comments
     */
    public function setCommentedType($commentedType)
    {
        $this->commentedType = $commentedType;

        return $this;
    }

    /**
     * Get commentedType
     *
     * @return string 
     */
    public function getCommentedType()
    {
        return $this->commentedType;
    }

    /**
     * Set commentedId
     *
     * @param integer $commentedId
     * 
     * @return Comments
     */
    public function setCommentedId($commentedId)
    {
        $this->commentedId = $commentedId;

        return $this;
    }

    /**
     * Get commentedId
     *
     * @return integer 
     */
    public function getCommentedId()
    {
        return $this->commentedId;
    }

    /**
     * Set authorId
     *
     * @param integer $authorId
     * 
     * @return Comments
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;

        return $this;
    }

    /**
     * Get authorId
     *
     * @return integer 
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * 
     * @return Comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * 
     * @return Comments
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set updatedOn
     *
     * @param \DateTime $updatedOn
     * 
     * @return Comments
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    /**
     * Get updatedOn
     *
     * @return \DateTime 
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }
}