<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomFields
 *
 * @ORM\Table(name="custom_fields")
 * @ORM\Entity
 */
class CustomFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="field_format", type="string", length=30, nullable=false)
     */
    private $fieldFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="possible_values", type="text", nullable=true)
     */
    private $possibleValues;

    /**
     * @var string
     *
     * @ORM\Column(name="`regexp`", type="string", length=255, nullable=true)
     */
    private $regexp;

    /**
     * @var integer
     *
     * @ORM\Column(name="min_length", type="integer", nullable=false)
     */
    private $minLength;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_length", type="integer", nullable=false)
     */
    private $maxLength;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_required", type="boolean", nullable=false)
     */
    private $isRequired;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_for_all", type="boolean", nullable=false)
     */
    private $isForAll;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_filter", type="boolean", nullable=false)
     */
    private $isFilter;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var boolean
     *
     * @ORM\Column(name="searchable", type="boolean", nullable=true)
     */
    private $searchable;

    /**
     * @var string
     *
     * @ORM\Column(name="default_value", type="text", nullable=true)
     */
    private $defaultValue;

    /**
     * @var boolean
     *
     * @ORM\Column(name="editable", type="boolean", nullable=true)
     */
    private $editable;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", nullable=false)
     */
    private $visible;

    /**
     * @var boolean
     *
     * @ORM\Column(name="multiple", type="boolean", nullable=true)
     */
    private $multiple;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * 
     * @return CustomFields
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return CustomFields
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set fieldFormat
     *
     * @param string $fieldFormat
     * 
     * @return CustomFields
     */
    public function setFieldFormat($fieldFormat)
    {
        $this->fieldFormat = $fieldFormat;

        return $this;
    }

    /**
     * Get fieldFormat
     *
     * @return string 
     */
    public function getFieldFormat()
    {
        return $this->fieldFormat;
    }

    /**
     * Set possibleValues
     *
     * @param string $possibleValues
     * 
     * @return CustomFields
     */
    public function setPossibleValues($possibleValues)
    {
        $this->possibleValues = $possibleValues;

        return $this;
    }

    /**
     * Get possibleValues
     *
     * @return string 
     */
    public function getPossibleValues()
    {
        return $this->possibleValues;
    }

    /**
     * Set regexp
     *
     * @param string $regexp
     * 
     * @return CustomFields
     */
    public function setRegexp($regexp)
    {
        $this->regexp = $regexp;

        return $this;
    }

    /**
     * Get regexp
     *
     * @return string 
     */
    public function getRegexp()
    {
        return $this->regexp;
    }

    /**
     * Set minLength
     *
     * @param integer $minLength
     * 
     * @return CustomFields
     */
    public function setMinLength($minLength)
    {
        $this->minLength = $minLength;

        return $this;
    }

    /**
     * Get minLength
     *
     * @return integer 
     */
    public function getMinLength()
    {
        return $this->minLength;
    }

    /**
     * Set maxLength
     *
     * @param integer $maxLength
     * 
     * @return CustomFields
     */
    public function setMaxLength($maxLength)
    {
        $this->maxLength = $maxLength;

        return $this;
    }

    /**
     * Get maxLength
     *
     * @return integer 
     */
    public function getMaxLength()
    {
        return $this->maxLength;
    }

    /**
     * Set isRequired
     *
     * @param boolean $isRequired
     * 
     * @return CustomFields
     */
    public function setIsRequired($isRequired)
    {
        $this->isRequired = $isRequired;

        return $this;
    }

    /**
     * Get isRequired
     *
     * @return boolean 
     */
    public function getIsRequired()
    {
        return $this->isRequired;
    }

    /**
     * Set isForAll
     *
     * @param boolean $isForAll
     * 
     * @return CustomFields
     */
    public function setIsForAll($isForAll)
    {
        $this->isForAll = $isForAll;

        return $this;
    }

    /**
     * Get isForAll
     *
     * @return boolean 
     */
    public function getIsForAll()
    {
        return $this->isForAll;
    }

    /**
     * Set isFilter
     *
     * @param boolean $isFilter
     * 
     * @return CustomFields
     */
    public function setIsFilter($isFilter)
    {
        $this->isFilter = $isFilter;

        return $this;
    }

    /**
     * Get isFilter
     *
     * @return boolean 
     */
    public function getIsFilter()
    {
        return $this->isFilter;
    }

    /**
     * Set position
     *
     * @param integer $position
     * 
     * @return CustomFields
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set searchable
     *
     * @param boolean $searchable
     * 
     * @return CustomFields
     */
    public function setSearchable($searchable)
    {
        $this->searchable = $searchable;

        return $this;
    }

    /**
     * Get searchable
     *
     * @return boolean 
     */
    public function getSearchable()
    {
        return $this->searchable;
    }

    /**
     * Set defaultValue
     *
     * @param string $defaultValue
     * 
     * @return CustomFields
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Get defaultValue
     *
     * @return string 
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Set editable
     *
     * @param boolean $editable
     * 
     * @return CustomFields
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;

        return $this;
    }

    /**
     * Get editable
     *
     * @return boolean 
     */
    public function getEditable()
    {
        return $this->editable;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * 
     * @return CustomFields
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set multiple
     *
     * @param boolean $multiple
     * 
     * @return CustomFields
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * Get multiple
     *
     * @return boolean 
     */
    public function getMultiple()
    {
        return $this->multiple;
    }
}