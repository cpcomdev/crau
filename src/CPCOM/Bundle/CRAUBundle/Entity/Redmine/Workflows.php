<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Workflows
 *
 * @ORM\Table(name="workflows")
 * @ORM\Entity
 */
class Workflows
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="tracker_id", type="integer", nullable=false)
     */
    private $trackerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="old_status_id", type="integer", nullable=false)
     */
    private $oldStatusId;

    /**
     * @var integer
     *
     * @ORM\Column(name="new_status_id", type="integer", nullable=false)
     */
    private $newStatusId;

    /**
     * @var integer
     *
     * @ORM\Column(name="role_id", type="integer", nullable=false)
     */
    private $roleId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="assignee", type="boolean", nullable=false)
     */
    private $assignee;

    /**
     * @var boolean
     *
     * @ORM\Column(name="author", type="boolean", nullable=false)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="field_name", type="string", length=30, nullable=true)
     */
    private $fieldName;

    /**
     * @var string
     *
     * @ORM\Column(name="rule", type="string", length=30, nullable=true)
     */
    private $rule;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set trackerId
     *
     * @param integer $trackerId
     * 
     * @return Workflows
     */
    public function setTrackerId($trackerId)
    {
        $this->trackerId = $trackerId;

        return $this;
    }

    /**
     * Get trackerId
     *
     * @return integer 
     */
    public function getTrackerId()
    {
        return $this->trackerId;
    }

    /**
     * Set oldStatusId
     *
     * @param integer $oldStatusId
     * 
     * @return Workflows
     */
    public function setOldStatusId($oldStatusId)
    {
        $this->oldStatusId = $oldStatusId;

        return $this;
    }

    /**
     * Get oldStatusId
     *
     * @return integer 
     */
    public function getOldStatusId()
    {
        return $this->oldStatusId;
    }

    /**
     * Set newStatusId
     *
     * @param integer $newStatusId
     * 
     * @return Workflows
     */
    public function setNewStatusId($newStatusId)
    {
        $this->newStatusId = $newStatusId;

        return $this;
    }

    /**
     * Get newStatusId
     *
     * @return integer 
     */
    public function getNewStatusId()
    {
        return $this->newStatusId;
    }

    /**
     * Set roleId
     *
     * @param integer $roleId
     * 
     * @return Workflows
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;

        return $this;
    }

    /**
     * Get roleId
     *
     * @return integer 
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

    /**
     * Set assignee
     *
     * @param boolean $assignee
     * 
     * @return Workflows
     */
    public function setAssignee($assignee)
    {
        $this->assignee = $assignee;

        return $this;
    }

    /**
     * Get assignee
     *
     * @return boolean 
     */
    public function getAssignee()
    {
        return $this->assignee;
    }

    /**
     * Set author
     *
     * @param boolean $author
     * 
     * @return Workflows
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return boolean 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set type
     *
     * @param string $type
     * 
     * @return Workflows
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set fieldName
     *
     * @param string $fieldName
     * 
     * @return Workflows
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * Get fieldName
     *
     * @return string 
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * Set rule
     *
     * @param string $rule
     * 
     * @return Workflows
     */
    public function setRule($rule)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Get rule
     *
     * @return string 
     */
    public function getRule()
    {
        return $this->rule;
    }
}