<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomValues
 *
 * @ORM\Table(name="custom_values")
 * @ORM\Entity
 */
class CustomValues
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="customized_type", type="string", length=30, nullable=false)
     */
    private $customizedType;

    /**
     * @var integer
     *
     * @ORM\Column(name="customized_id", type="integer", nullable=false)
     */
    private $customizedId;

    /**
     * @var integer
     *
     * @ORM\Column(name="custom_field_id", type="integer", nullable=false)
     */
    private $customFieldId;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customizedType
     *
     * @param string $customizedType
     * 
     * @return CustomValues
     */
    public function setCustomizedType($customizedType)
    {
        $this->customizedType = $customizedType;

        return $this;
    }

    /**
     * Get customizedType
     *
     * @return string 
     */
    public function getCustomizedType()
    {
        return $this->customizedType;
    }

    /**
     * Set customizedId
     *
     * @param integer $customizedId
     * 
     * @return CustomValues
     */
    public function setCustomizedId($customizedId)
    {
        $this->customizedId = $customizedId;

        return $this;
    }

    /**
     * Get customizedId
     *
     * @return integer 
     */
    public function getCustomizedId()
    {
        return $this->customizedId;
    }

    /**
     * Set customFieldId
     *
     * @param integer $customFieldId
     * 
     * @return CustomValues
     */
    public function setCustomFieldId($customFieldId)
    {
        $this->customFieldId = $customFieldId;

        return $this;
    }

    /**
     * Get customFieldId
     *
     * @return integer 
     */
    public function getCustomFieldId()
    {
        return $this->customFieldId;
    }

    /**
     * Set value
     *
     * @param string $value
     * 
     * @return CustomValues
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }
}