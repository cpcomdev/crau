<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Repositories
 *
 * @ORM\Table(name="repositories")
 * @ORM\Entity
 */
class Repositories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=60, nullable=true)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="root_url", type="string", length=255, nullable=true)
     */
    private $rootUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="path_encoding", type="string", length=64, nullable=true)
     */
    private $pathEncoding;

    /**
     * @var string
     *
     * @ORM\Column(name="log_encoding", type="string", length=64, nullable=true)
     */
    private $logEncoding;

    /**
     * @var string
     *
     * @ORM\Column(name="extra_info", type="text", nullable=true)
     */
    private $extraInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=255, nullable=true)
     */
    private $identifier;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_default", type="boolean", nullable=true)
     */
    private $isDefault;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set projectId
     *
     * @param integer $projectId
     * 
     * @return Repositories
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * Get projectId
     *
     * @return integer 
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Set url
     *
     * @param string $url
     * 
     * @return Repositories
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set login
     *
     * @param string $login
     * 
     * @return Repositories
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string 
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password
     *
     * @param string $password
     * 
     * @return Repositories
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set rootUrl
     *
     * @param string $rootUrl
     * 
     * @return Repositories
     */
    public function setRootUrl($rootUrl)
    {
        $this->rootUrl = $rootUrl;

        return $this;
    }

    /**
     * Get rootUrl
     *
     * @return string 
     */
    public function getRootUrl()
    {
        return $this->rootUrl;
    }

    /**
     * Set type
     *
     * @param string $type
     * 
     * @return Repositories
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set pathEncoding
     *
     * @param string $pathEncoding
     * 
     * @return Repositories
     */
    public function setPathEncoding($pathEncoding)
    {
        $this->pathEncoding = $pathEncoding;

        return $this;
    }

    /**
     * Get pathEncoding
     *
     * @return string 
     */
    public function getPathEncoding()
    {
        return $this->pathEncoding;
    }

    /**
     * Set logEncoding
     *
     * @param string $logEncoding
     * 
     * @return Repositories
     */
    public function setLogEncoding($logEncoding)
    {
        $this->logEncoding = $logEncoding;

        return $this;
    }

    /**
     * Get logEncoding
     *
     * @return string 
     */
    public function getLogEncoding()
    {
        return $this->logEncoding;
    }

    /**
     * Set extraInfo
     *
     * @param string $extraInfo
     * 
     * @return Repositories
     */
    public function setExtraInfo($extraInfo)
    {
        $this->extraInfo = $extraInfo;

        return $this;
    }

    /**
     * Get extraInfo
     *
     * @return string 
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     * 
     * @return Repositories
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return string 
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set isDefault
     *
     * @param boolean $isDefault
     * 
     * @return Repositories
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return boolean 
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }
}