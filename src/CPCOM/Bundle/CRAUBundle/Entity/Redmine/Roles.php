<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Roles
 *
 * @ORM\Table(name="roles")
 * @ORM\Entity
 */
class Roles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var boolean
     *
     * @ORM\Column(name="assignable", type="boolean", nullable=true)
     */
    private $assignable;

    /**
     * @var integer
     *
     * @ORM\Column(name="builtin", type="integer", nullable=false)
     */
    private $builtin;

    /**
     * @var string
     *
     * @ORM\Column(name="permissions", type="text", nullable=true)
     */
    private $permissions;

    /**
     * @var string
     *
     * @ORM\Column(name="issues_visibility", type="string", length=30, nullable=false)
     */
    private $issuesVisibility;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return Roles
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set position
     *
     * @param integer $position
     * 
     * @return Roles
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set assignable
     *
     * @param boolean $assignable
     * 
     * @return Roles
     */
    public function setAssignable($assignable)
    {
        $this->assignable = $assignable;

        return $this;
    }

    /**
     * Get assignable
     *
     * @return boolean 
     */
    public function getAssignable()
    {
        return $this->assignable;
    }

    /**
     * Set builtin
     *
     * @param integer $builtin
     * 
     * @return Roles
     */
    public function setBuiltin($builtin)
    {
        $this->builtin = $builtin;

        return $this;
    }

    /**
     * Get builtin
     *
     * @return integer 
     */
    public function getBuiltin()
    {
        return $this->builtin;
    }

    /**
     * Set permissions
     *
     * @param string $permissions
     * 
     * @return Roles
     */
    public function setPermissions($permissions)
    {
        $this->permissions = $permissions;

        return $this;
    }

    /**
     * Get permissions
     *
     * @return string 
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * Set issuesVisibility
     *
     * @param string $issuesVisibility
     * 
     * @return Roles
     */
    public function setIssuesVisibility($issuesVisibility)
    {
        $this->issuesVisibility = $issuesVisibility;

        return $this;
    }

    /**
     * Get issuesVisibility
     *
     * @return string 
     */
    public function getIssuesVisibility()
    {
        return $this->issuesVisibility;
    }
}