<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Attachments
 *
 * @ORM\Table(name="attachments")
 * @ORM\Entity
 */
class Attachments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="container_id", type="integer", nullable=true)
     */
    private $containerId;

    /**
     * @var string
     *
     * @ORM\Column(name="container_type", type="string", length=30, nullable=true)
     */
    private $containerType;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=false)
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="disk_filename", type="string", length=255, nullable=false)
     */
    private $diskFilename;

    /**
     * @var integer
     *
     * @ORM\Column(name="filesize", type="integer", nullable=false)
     */
    private $filesize;

    /**
     * @var string
     *
     * @ORM\Column(name="content_type", type="string", length=255, nullable=true)
     */
    private $contentType;

    /**
     * @var string
     *
     * @ORM\Column(name="digest", type="string", length=40, nullable=false)
     */
    private $digest;

    /**
     * @var integer
     *
     * @ORM\Column(name="downloads", type="integer", nullable=false)
     */
    private $downloads;

    /**
     * @var integer
     *
     * @ORM\Column(name="author_id", type="integer", nullable=false)
     */
    private $authorId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=true)
     */
    private $createdOn;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="disk_directory", type="string", length=255, nullable=true)
     */
    private $diskDirectory;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set containerId
     *
     * @param integer $containerId
     * 
     * @return Attachments
     */
    public function setContainerId($containerId)
    {
        $this->containerId = $containerId;

        return $this;
    }

    /**
     * Get containerId
     *
     * @return integer 
     */
    public function getContainerId()
    {
        return $this->containerId;
    }

    /**
     * Set containerType
     *
     * @param string $containerType
     * 
     * @return Attachments
     */
    public function setContainerType($containerType)
    {
        $this->containerType = $containerType;

        return $this;
    }

    /**
     * Get containerType
     *
     * @return string 
     */
    public function getContainerType()
    {
        return $this->containerType;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * 
     * @return Attachments
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set diskFilename
     *
     * @param string $diskFilename
     * 
     * @return Attachments
     */
    public function setDiskFilename($diskFilename)
    {
        $this->diskFilename = $diskFilename;

        return $this;
    }

    /**
     * Get diskFilename
     *
     * @return string 
     */
    public function getDiskFilename()
    {
        return $this->diskFilename;
    }

    /**
     * Set filesize
     *
     * @param integer $filesize
     * 
     * @return Attachments
     */
    public function setFilesize($filesize)
    {
        $this->filesize = $filesize;

        return $this;
    }

    /**
     * Get filesize
     *
     * @return integer 
     */
    public function getFilesize()
    {
        return $this->filesize;
    }

    /**
     * Set contentType
     *
     * @param string $contentType
     * 
     * @return Attachments
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * Get contentType
     *
     * @return string 
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * Set digest
     *
     * @param string $digest
     * 
     * @return Attachments
     */
    public function setDigest($digest)
    {
        $this->digest = $digest;

        return $this;
    }

    /**
     * Get digest
     *
     * @return string 
     */
    public function getDigest()
    {
        return $this->digest;
    }

    /**
     * Set downloads
     *
     * @param integer $downloads
     * 
     * @return Attachments
     */
    public function setDownloads($downloads)
    {
        $this->downloads = $downloads;

        return $this;
    }

    /**
     * Get downloads
     *
     * @return integer 
     */
    public function getDownloads()
    {
        return $this->downloads;
    }

    /**
     * Set authorId
     *
     * @param integer $authorId
     * 
     * @return Attachments
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;

        return $this;
    }

    /**
     * Get authorId
     *
     * @return integer 
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * 
     * @return Attachments
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set description
     *
     * @param string $description
     * 
     * @return Attachments
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set diskDirectory
     *
     * @param string $diskDirectory
     * 
     * @return Attachments
     */
    public function setDiskDirectory($diskDirectory)
    {
        $this->diskDirectory = $diskDirectory;

        return $this;
    }

    /**
     * Get diskDirectory
     *
     * @return string 
     */
    public function getDiskDirectory()
    {
        return $this->diskDirectory;
    }
}