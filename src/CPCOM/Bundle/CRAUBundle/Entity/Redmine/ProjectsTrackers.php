<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectsTrackers
 *
 * @ORM\Table(name="projects_trackers")
 * @ORM\Entity
 */
class ProjectsTrackers
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="tracker_id", type="integer", nullable=false)
     */
    private $trackerId;

    /**
     * @return type 
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * @param type $projectId 
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;
    }

    /**
     * @return type 
     */
    public function getTrackerId()
    {
        return $this->trackerId;
    }

    /**
     * @param type $trackerId 
     */
    public function setTrackerId($trackerId)
    {
        $this->trackerId = $trackerId;
    }
}