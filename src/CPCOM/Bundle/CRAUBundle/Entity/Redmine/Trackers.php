<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trackers
 *
 * @ORM\Table(name="trackers")
 * @ORM\Entity
 */
class Trackers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_in_chlog", type="boolean", nullable=false)
     */
    private $isInChlog;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_in_roadmap", type="boolean", nullable=false)
     */
    private $isInRoadmap;

    /**
     * @var integer
     *
     * @ORM\Column(name="fields_bits", type="integer", nullable=true)
     */
    private $fieldsBits;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return Trackers
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isInChlog
     *
     * @param boolean $isInChlog
     * 
     * @return Trackers
     */
    public function setIsInChlog($isInChlog)
    {
        $this->isInChlog = $isInChlog;

        return $this;
    }

    /**
     * Get isInChlog
     *
     * @return boolean 
     */
    public function getIsInChlog()
    {
        return $this->isInChlog;
    }

    /**
     * Set position
     *
     * @param integer $position
     * 
     * @return Trackers
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set isInRoadmap
     *
     * @param boolean $isInRoadmap
     * 
     * @return Trackers
     */
    public function setIsInRoadmap($isInRoadmap)
    {
        $this->isInRoadmap = $isInRoadmap;

        return $this;
    }

    /**
     * Get isInRoadmap
     *
     * @return boolean 
     */
    public function getIsInRoadmap()
    {
        return $this->isInRoadmap;
    }

    /**
     * Set fieldsBits
     *
     * @param integer $fieldsBits
     * 
     * @return Trackers
     */
    public function setFieldsBits($fieldsBits)
    {
        $this->fieldsBits = $fieldsBits;

        return $this;
    }

    /**
     * Get fieldsBits
     *
     * @return integer 
     */
    public function getFieldsBits()
    {
        return $this->fieldsBits;
    }
}