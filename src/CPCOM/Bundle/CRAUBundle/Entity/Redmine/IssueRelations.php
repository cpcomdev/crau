<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * IssueRelations
 *
 * @ORM\Table(name="issue_relations")
 * @ORM\Entity
 */
class IssueRelations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="issue_from_id", type="integer", nullable=false)
     */
    private $issueFromId;

    /**
     * @var integer
     *
     * @ORM\Column(name="issue_to_id", type="integer", nullable=false)
     */
    private $issueToId;

    /**
     * @var string
     *
     * @ORM\Column(name="relation_type", type="string", length=255, nullable=false)
     */
    private $relationType;

    /**
     * @var integer
     *
     * @ORM\Column(name="delay", type="integer", nullable=true)
     */
    private $delay;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set issueFromId
     *
     * @param integer $issueFromId
     * 
     * @return IssueRelations
     */
    public function setIssueFromId($issueFromId)
    {
        $this->issueFromId = $issueFromId;

        return $this;
    }

    /**
     * Get issueFromId
     *
     * @return integer 
     */
    public function getIssueFromId()
    {
        return $this->issueFromId;
    }

    /**
     * Set issueToId
     *
     * @param integer $issueToId
     * 
     * @return IssueRelations
     */
    public function setIssueToId($issueToId)
    {
        $this->issueToId = $issueToId;

        return $this;
    }

    /**
     * Get issueToId
     *
     * @return integer 
     */
    public function getIssueToId()
    {
        return $this->issueToId;
    }

    /**
     * Set relationType
     *
     * @param string $relationType
     * 
     * @return IssueRelations
     */
    public function setRelationType($relationType)
    {
        $this->relationType = $relationType;

        return $this;
    }

    /**
     * Get relationType
     *
     * @return string 
     */
    public function getRelationType()
    {
        return $this->relationType;
    }

    /**
     * Set delay
     *
     * @param integer $delay
     * 
     * @return IssueRelations
     */
    public function setDelay($delay)
    {
        $this->delay = $delay;

        return $this;
    }

    /**
     * Get delay
     *
     * @return integer 
     */
    public function getDelay()
    {
        return $this->delay;
    }
}