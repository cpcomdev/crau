<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * JournalDetails
 *
 * @ORM\Table(name="journal_details")
 * @ORM\Entity
 */
class JournalDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="journal_id", type="integer", nullable=false)
     */
    private $journalId;

    /**
     * @var string
     *
     * @ORM\Column(name="property", type="string", length=30, nullable=false)
     */
    private $property;

    /**
     * @var string
     *
     * @ORM\Column(name="prop_key", type="string", length=30, nullable=false)
     */
    private $propKey;

    /**
     * @var string
     *
     * @ORM\Column(name="old_value", type="text", nullable=true)
     */
    private $oldValue;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set journalId
     *
     * @param integer $journalId
     * 
     * @return JournalDetails
     */
    public function setJournalId($journalId)
    {
        $this->journalId = $journalId;

        return $this;
    }

    /**
     * Get journalId
     *
     * @return integer 
     */
    public function getJournalId()
    {
        return $this->journalId;
    }

    /**
     * Set property
     *
     * @param string $property
     * 
     * @return JournalDetails
     */
    public function setProperty($property)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return string 
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set propKey
     *
     * @param string $propKey
     * 
     * @return JournalDetails
     */
    public function setPropKey($propKey)
    {
        $this->propKey = $propKey;

        return $this;
    }

    /**
     * Get propKey
     *
     * @return string 
     */
    public function getPropKey()
    {
        return $this->propKey;
    }

    /**
     * Set oldValue
     *
     * @param string $oldValue
     * 
     * @return JournalDetails
     */
    public function setOldValue($oldValue)
    {
        $this->oldValue = $oldValue;

        return $this;
    }

    /**
     * Get oldValue
     *
     * @return string 
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    /**
     * Set value
     *
     * @param string $value
     * 
     * @return JournalDetails
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }
}