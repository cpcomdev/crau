<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Watchers
 *
 * @ORM\Table(name="watchers")
 * @ORM\Entity
 */
class Watchers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="watchable_type", type="string", length=255, nullable=false)
     */
    private $watchableType;

    /**
     * @var integer
     *
     * @ORM\Column(name="watchable_id", type="integer", nullable=false)
     */
    private $watchableId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set watchableType
     *
     * @param string $watchableType
     * 
     * @return Watchers
     */
    public function setWatchableType($watchableType)
    {
        $this->watchableType = $watchableType;

        return $this;
    }

    /**
     * Get watchableType
     *
     * @return string 
     */
    public function getWatchableType()
    {
        return $this->watchableType;
    }

    /**
     * Set watchableId
     *
     * @param integer $watchableId
     * 
     * @return Watchers
     */
    public function setWatchableId($watchableId)
    {
        $this->watchableId = $watchableId;

        return $this;
    }

    /**
     * Get watchableId
     *
     * @return integer 
     */
    public function getWatchableId()
    {
        return $this->watchableId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * 
     * @return Watchers
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }
}