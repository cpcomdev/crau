<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * OpenIdAuthenticationAssociations
 *
 * @ORM\Table(name="open_id_authentication_associations")
 * @ORM\Entity
 */
class OpenIdAuthenticationAssociations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="issued", type="integer", nullable=true)
     */
    private $issued;

    /**
     * @var integer
     *
     * @ORM\Column(name="lifetime", type="integer", nullable=true)
     */
    private $lifetime;

    /**
     * @var string
     *
     * @ORM\Column(name="handle", type="string", length=255, nullable=true)
     */
    private $handle;

    /**
     * @var string
     *
     * @ORM\Column(name="assoc_type", type="string", length=255, nullable=true)
     */
    private $assocType;

    /**
     * @var string
     *
     * @ORM\Column(name="server_url", type="blob", nullable=true)
     */
    private $serverUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="secret", type="blob", nullable=true)
     */
    private $secret;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set issued
     *
     * @param integer $issued
     * 
     * @return OpenIdAuthenticationAssociations
     */
    public function setIssued($issued)
    {
        $this->issued = $issued;

        return $this;
    }

    /**
     * Get issued
     *
     * @return integer 
     */
    public function getIssued()
    {
        return $this->issued;
    }

    /**
     * Set lifetime
     *
     * @param integer $lifetime
     * 
     * @return OpenIdAuthenticationAssociations
     */
    public function setLifetime($lifetime)
    {
        $this->lifetime = $lifetime;

        return $this;
    }

    /**
     * Get lifetime
     *
     * @return integer 
     */
    public function getLifetime()
    {
        return $this->lifetime;
    }

    /**
     * Set handle
     *
     * @param string $handle
     * 
     * @return OpenIdAuthenticationAssociations
     */
    public function setHandle($handle)
    {
        $this->handle = $handle;

        return $this;
    }

    /**
     * Get handle
     *
     * @return string 
     */
    public function getHandle()
    {
        return $this->handle;
    }

    /**
     * Set assocType
     *
     * @param string $assocType
     * 
     * @return OpenIdAuthenticationAssociations
     */
    public function setAssocType($assocType)
    {
        $this->assocType = $assocType;

        return $this;
    }

    /**
     * Get assocType
     *
     * @return string 
     */
    public function getAssocType()
    {
        return $this->assocType;
    }

    /**
     * Set serverUrl
     *
     * @param string $serverUrl
     * 
     * @return OpenIdAuthenticationAssociations
     */
    public function setServerUrl($serverUrl)
    {
        $this->serverUrl = $serverUrl;

        return $this;
    }

    /**
     * Get serverUrl
     *
     * @return string 
     */
    public function getServerUrl()
    {
        return $this->serverUrl;
    }

    /**
     * Set secret
     *
     * @param string $secret
     * 
     * @return OpenIdAuthenticationAssociations
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * Get secret
     *
     * @return string 
     */
    public function getSecret()
    {
        return $this->secret;
    }
}