<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Changes
 *
 * @ORM\Table(name="changes")
 * @ORM\Entity
 */
class Changes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="changeset_id", type="integer", nullable=false)
     */
    private $changesetId;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=1, nullable=false)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="text", nullable=false)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="from_path", type="text", nullable=true)
     */
    private $fromPath;

    /**
     * @var string
     *
     * @ORM\Column(name="from_revision", type="string", length=255, nullable=true)
     */
    private $fromRevision;

    /**
     * @var string
     *
     * @ORM\Column(name="revision", type="string", length=255, nullable=true)
     */
    private $revision;

    /**
     * @var string
     *
     * @ORM\Column(name="branch", type="string", length=255, nullable=true)
     */
    private $branch;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set changesetId
     *
     * @param integer $changesetId
     * 
     * @return Changes
     */
    public function setChangesetId($changesetId)
    {
        $this->changesetId = $changesetId;

        return $this;
    }

    /**
     * Get changesetId
     *
     * @return integer 
     */
    public function getChangesetId()
    {
        return $this->changesetId;
    }

    /**
     * Set action
     *
     * @param string $action
     * 
     * @return Changes
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set path
     *
     * @param string $path
     * 
     * @return Changes
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set fromPath
     *
     * @param string $fromPath
     * 
     * @return Changes
     */
    public function setFromPath($fromPath)
    {
        $this->fromPath = $fromPath;

        return $this;
    }

    /**
     * Get fromPath
     *
     * @return string 
     */
    public function getFromPath()
    {
        return $this->fromPath;
    }

    /**
     * Set fromRevision
     *
     * @param string $fromRevision
     * 
     * @return Changes
     */
    public function setFromRevision($fromRevision)
    {
        $this->fromRevision = $fromRevision;

        return $this;
    }

    /**
     * Get fromRevision
     *
     * @return string 
     */
    public function getFromRevision()
    {
        return $this->fromRevision;
    }

    /**
     * Set revision
     *
     * @param string $revision
     * 
     * @return Changes
     */
    public function setRevision($revision)
    {
        $this->revision = $revision;

        return $this;
    }

    /**
     * Get revision
     *
     * @return string 
     */
    public function getRevision()
    {
        return $this->revision;
    }

    /**
     * Set branch
     *
     * @param string $branch
     * 
     * @return Changes
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;

        return $this;
    }

    /**
     * Get branch
     *
     * @return string 
     */
    public function getBranch()
    {
        return $this->branch;
    }
}