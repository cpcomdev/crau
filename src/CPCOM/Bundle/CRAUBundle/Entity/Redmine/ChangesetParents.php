<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Changeset Parents
 *
 * @ORM\Table(name="changeset_parents")
 * @ORM\Entity
 */
class ChangesetParents
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="changeset_id", type="integer", nullable=false)
     */
    private $changesetId;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="parent_id", type="integer", nullable=false)
     */
    private $parentId;

    /**
     * @return type 
     */
    public function getChangesetId()
    {
        return $this->changesetId;
    }

    /**
     * @param type $changesetId 
     */
    public function setChangesetId($changesetId)
    {
        $this->changesetId = $changesetId;
    }

    /**
     * @return type 
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param type $parentId 
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }
}