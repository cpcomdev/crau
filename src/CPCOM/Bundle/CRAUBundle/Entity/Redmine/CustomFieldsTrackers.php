<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomFieldsTrackers
 *
 * @ORM\Table(name="custom_fields_trackers")
 * @ORM\Entity
 */
class CustomFieldsTrackers
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="custil_field_id", type="integer", nullable=false)
     */
    private $customFildId;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="tracker_id", type="integer", nullable=false)
     */
    private $trackerId;

    /**
     * @return type 
     */
    public function getCustomFildId()
    {
        return $this->customFildId;
    }

    /**
     * @param type $customFildId 
     */
    public function setCustomFildId($customFildId)
    {
        $this->customFildId = $customFildId;
    }

    /**
     * @return type 
     */
    public function getTrackerId()
    {
        return $this->trackerId;
    }

    /**
     * @param type $trackerId 
     */
    public function setTrackerId($trackerId)
    {
        $this->trackerId = $trackerId;
    }


}