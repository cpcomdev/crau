<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Issues
 *
 * @ORM\Table(name="issues")
 * @ORM\Entity
 */
class Issues
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="tracker_id", type="integer", nullable=false)
     */
    private $trackerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=false)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="due_date", type="date", nullable=true)
     */
    private $dueDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer", nullable=true)
     */
    private $categoryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="status_id", type="integer", nullable=false)
     */
    private $statusId;

    /**
     * @var integer
     *
     * @ORM\Column(name="assigned_to_id", type="integer", nullable=true)
     */
    private $assignedToId;

    /**
     * @var integer
     *
     * @ORM\Column(name="priority_id", type="integer", nullable=false)
     */
    private $priorityId;

    /**
     * @var integer
     *
     * @ORM\Column(name="fixed_version_id", type="integer", nullable=true)
     */
    private $fixedVersionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="author_id", type="integer", nullable=false)
     */
    private $authorId;

    /**
     * @var integer
     *
     * @ORM\Column(name="lock_version", type="integer", nullable=false)
     */
    private $lockVersion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=true)
     */
    private $createdOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_on", type="datetime", nullable=true)
     */
    private $updatedOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="date", nullable=true)
     */
    private $startDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="done_ratio", type="integer", nullable=false)
     */
    private $doneRatio;

    /**
     * @var float
     *
     * @ORM\Column(name="estimated_hours", type="float", nullable=true)
     */
    private $estimatedHours;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="root_id", type="integer", nullable=true)
     */
    private $rootId;

    /**
     * @var integer
     *
     * @ORM\Column(name="lft", type="integer", nullable=true)
     */
    private $lft;

    /**
     * @var integer
     *
     * @ORM\Column(name="rgt", type="integer", nullable=true)
     */
    private $rgt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_private", type="boolean", nullable=false)
     */
    private $isPrivate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="closed_on", type="datetime", nullable=true)
     */
    private $closedOn;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set trackerId
     *
     * @param integer $trackerId
     * 
     * @return Issues
     */
    public function setTrackerId($trackerId)
    {
        $this->trackerId = $trackerId;

        return $this;
    }

    /**
     * Get trackerId
     *
     * @return integer 
     */
    public function getTrackerId()
    {
        return $this->trackerId;
    }

    /**
     * Set projectId
     *
     * @param integer $projectId
     * 
     * @return Issues
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * Get projectId
     *
     * @return integer 
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * 
     * @return Issues
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set description
     *
     * @param string $description
     * 
     * @return Issues
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     * 
     * @return Issues
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime 
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     * 
     * @return Issues
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer 
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     * 
     * @return Issues
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer 
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set assignedToId
     *
     * @param integer $assignedToId
     * 
     * @return Issues
     */
    public function setAssignedToId($assignedToId)
    {
        $this->assignedToId = $assignedToId;

        return $this;
    }

    /**
     * Get assignedToId
     *
     * @return integer 
     */
    public function getAssignedToId()
    {
        return $this->assignedToId;
    }

    /**
     * Set priorityId
     *
     * @param integer $priorityId
     * 
     * @return Issues
     */
    public function setPriorityId($priorityId)
    {
        $this->priorityId = $priorityId;

        return $this;
    }

    /**
     * Get priorityId
     *
     * @return integer 
     */
    public function getPriorityId()
    {
        return $this->priorityId;
    }

    /**
     * Set fixedVersionId
     *
     * @param integer $fixedVersionId
     * 
     * @return Issues
     */
    public function setFixedVersionId($fixedVersionId)
    {
        $this->fixedVersionId = $fixedVersionId;

        return $this;
    }

    /**
     * Get fixedVersionId
     *
     * @return integer 
     */
    public function getFixedVersionId()
    {
        return $this->fixedVersionId;
    }

    /**
     * Set authorId
     *
     * @param integer $authorId
     * 
     * @return Issues
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;

        return $this;
    }

    /**
     * Get authorId
     *
     * @return integer 
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * Set lockVersion
     *
     * @param integer $lockVersion
     * 
     * @return Issues
     */
    public function setLockVersion($lockVersion)
    {
        $this->lockVersion = $lockVersion;

        return $this;
    }

    /**
     * Get lockVersion
     *
     * @return integer 
     */
    public function getLockVersion()
    {
        return $this->lockVersion;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * 
     * @return Issues
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set updatedOn
     *
     * @param \DateTime $updatedOn
     * 
     * @return Issues
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    /**
     * Get updatedOn
     *
     * @return \DateTime 
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * 
     * @return Issues
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set doneRatio
     *
     * @param integer $doneRatio
     * 
     * @return Issues
     */
    public function setDoneRatio($doneRatio)
    {
        $this->doneRatio = $doneRatio;

        return $this;
    }

    /**
     * Get doneRatio
     *
     * @return integer 
     */
    public function getDoneRatio()
    {
        return $this->doneRatio;
    }

    /**
     * Set estimatedHours
     *
     * @param float $estimatedHours
     * 
     * @return Issues
     */
    public function setEstimatedHours($estimatedHours)
    {
        $this->estimatedHours = $estimatedHours;

        return $this;
    }

    /**
     * Get estimatedHours
     *
     * @return float 
     */
    public function getEstimatedHours()
    {
        return $this->estimatedHours;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     * 
     * @return Issues
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set rootId
     *
     * @param integer $rootId
     * 
     * @return Issues
     */
    public function setRootId($rootId)
    {
        $this->rootId = $rootId;

        return $this;
    }

    /**
     * Get rootId
     *
     * @return integer 
     */
    public function getRootId()
    {
        return $this->rootId;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     * 
     * @return Issues
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer 
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     * 
     * @return Issues
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer 
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set isPrivate
     *
     * @param boolean $isPrivate
     * 
     * @return Issues
     */
    public function setIsPrivate($isPrivate)
    {
        $this->isPrivate = $isPrivate;

        return $this;
    }

    /**
     * Get isPrivate
     *
     * @return boolean 
     */
    public function getIsPrivate()
    {
        return $this->isPrivate;
    }

    /**
     * Set closedOn
     *
     * @param \DateTime $closedOn
     * 
     * @return Issues
     */
    public function setClosedOn($closedOn)
    {
        $this->closedOn = $closedOn;

        return $this;
    }

    /**
     * Get closedOn
     *
     * @return \DateTime 
     */
    public function getClosedOn()
    {
        return $this->closedOn;
    }
}