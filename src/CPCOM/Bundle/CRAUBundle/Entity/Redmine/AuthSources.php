<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * AuthSources
 *
 * @ORM\Table(name="auth_sources")
 * @ORM\Entity
 */
class AuthSources
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=60, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="host", type="string", length=60, nullable=true)
     */
    private $host;

    /**
     * @var integer
     *
     * @ORM\Column(name="port", type="integer", nullable=true)
     */
    private $port;

    /**
     * @var string
     *
     * @ORM\Column(name="account", type="string", length=255, nullable=true)
     */
    private $account;

    /**
     * @var string
     *
     * @ORM\Column(name="account_password", type="string", length=255, nullable=true)
     */
    private $accountPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="base_dn", type="string", length=255, nullable=true)
     */
    private $baseDn;

    /**
     * @var string
     *
     * @ORM\Column(name="attr_login", type="string", length=30, nullable=true)
     */
    private $attrLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="attr_firstname", type="string", length=30, nullable=true)
     */
    private $attrFirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="attr_lastname", type="string", length=30, nullable=true)
     */
    private $attrLastname;

    /**
     * @var string
     *
     * @ORM\Column(name="attr_mail", type="string", length=30, nullable=true)
     */
    private $attrMail;

    /**
     * @var boolean
     *
     * @ORM\Column(name="onthefly_register", type="boolean", nullable=false)
     */
    private $ontheflyRegister;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tls", type="boolean", nullable=false)
     */
    private $tls;

    /**
     * @var string
     *
     * @ORM\Column(name="filter", type="string", length=255, nullable=true)
     */
    private $filter;

    /**
     * @var integer
     *
     * @ORM\Column(name="timeout", type="integer", nullable=true)
     */
    private $timeout;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * 
     * @return AuthSources
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return AuthSources
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set host
     *
     * @param string $host
     * 
     * @return AuthSources
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get host
     *
     * @return string 
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set port
     *
     * @param integer $port
     * 
     * @return AuthSources
     */
    public function setPort($port)
    {
        $this->port = $port;

        return $this;
    }

    /**
     * Get port
     *
     * @return integer 
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Set account
     *
     * @param string $account
     * 
     * @return AuthSources
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return string 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set accountPassword
     *
     * @param string $accountPassword
     * 
     * @return AuthSources
     */
    public function setAccountPassword($accountPassword)
    {
        $this->accountPassword = $accountPassword;

        return $this;
    }

    /**
     * Get accountPassword
     *
     * @return string 
     */
    public function getAccountPassword()
    {
        return $this->accountPassword;
    }

    /**
     * Set baseDn
     *
     * @param string $baseDn
     * 
     * @return AuthSources
     */
    public function setBaseDn($baseDn)
    {
        $this->baseDn = $baseDn;

        return $this;
    }

    /**
     * Get baseDn
     *
     * @return string 
     */
    public function getBaseDn()
    {
        return $this->baseDn;
    }

    /**
     * Set attrLogin
     *
     * @param string $attrLogin
     * 
     * @return AuthSources
     */
    public function setAttrLogin($attrLogin)
    {
        $this->attrLogin = $attrLogin;

        return $this;
    }

    /**
     * Get attrLogin
     *
     * @return string 
     */
    public function getAttrLogin()
    {
        return $this->attrLogin;
    }

    /**
     * Set attrFirstname
     *
     * @param string $attrFirstname
     * 
     * @return AuthSources
     */
    public function setAttrFirstname($attrFirstname)
    {
        $this->attrFirstname = $attrFirstname;

        return $this;
    }

    /**
     * Get attrFirstname
     *
     * @return string 
     */
    public function getAttrFirstname()
    {
        return $this->attrFirstname;
    }

    /**
     * Set attrLastname
     *
     * @param string $attrLastname
     * 
     * @return AuthSources
     */
    public function setAttrLastname($attrLastname)
    {
        $this->attrLastname = $attrLastname;

        return $this;
    }

    /**
     * Get attrLastname
     *
     * @return string 
     */
    public function getAttrLastname()
    {
        return $this->attrLastname;
    }

    /**
     * Set attrMail
     *
     * @param string $attrMail
     * 
     * @return AuthSources
     */
    public function setAttrMail($attrMail)
    {
        $this->attrMail = $attrMail;

        return $this;
    }

    /**
     * Get attrMail
     *
     * @return string 
     */
    public function getAttrMail()
    {
        return $this->attrMail;
    }

    /**
     * Set ontheflyRegister
     *
     * @param boolean $ontheflyRegister
     * 
     * @return AuthSources
     */
    public function setOntheflyRegister($ontheflyRegister)
    {
        $this->ontheflyRegister = $ontheflyRegister;

        return $this;
    }

    /**
     * Get ontheflyRegister
     *
     * @return boolean 
     */
    public function getOntheflyRegister()
    {
        return $this->ontheflyRegister;
    }

    /**
     * Set tls
     *
     * @param boolean $tls
     * 
     * @return AuthSources
     */
    public function setTls($tls)
    {
        $this->tls = $tls;

        return $this;
    }

    /**
     * Get tls
     *
     * @return boolean 
     */
    public function getTls()
    {
        return $this->tls;
    }

    /**
     * Set filter
     *
     * @param string $filter
     * 
     * @return AuthSources
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * Get filter
     *
     * @return string 
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * Set timeout
     *
     * @param integer $timeout
     * 
     * @return AuthSources
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;

        return $this;
    }

    /**
     * Get timeout
     *
     * @return integer 
     */
    public function getTimeout()
    {
        return $this->timeout;
    }
}