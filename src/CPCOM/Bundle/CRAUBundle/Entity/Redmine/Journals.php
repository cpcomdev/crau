<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Journals
 *
 * @ORM\Table(name="journals")
 * @ORM\Entity
 */
class Journals
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="journalized_id", type="integer", nullable=false)
     */
    private $journalizedId;

    /**
     * @var string
     *
     * @ORM\Column(name="journalized_type", type="string", length=30, nullable=false)
     */
    private $journalizedType;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     */
    private $createdOn;

    /**
     * @var boolean
     *
     * @ORM\Column(name="private_notes", type="boolean", nullable=false)
     */
    private $privateNotes;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set journalizedId
     *
     * @param integer $journalizedId
     * 
     * @return Journals
     */
    public function setJournalizedId($journalizedId)
    {
        $this->journalizedId = $journalizedId;

        return $this;
    }

    /**
     * Get journalizedId
     *
     * @return integer 
     */
    public function getJournalizedId()
    {
        return $this->journalizedId;
    }

    /**
     * Set journalizedType
     *
     * @param string $journalizedType
     * 
     * @return Journals
     */
    public function setJournalizedType($journalizedType)
    {
        $this->journalizedType = $journalizedType;

        return $this;
    }

    /**
     * Get journalizedType
     *
     * @return string 
     */
    public function getJournalizedType()
    {
        return $this->journalizedType;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * 
     * @return Journals
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * 
     * @return Journals
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * 
     * @return Journals
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set privateNotes
     *
     * @param boolean $privateNotes
     * 
     * @return Journals
     */
    public function setPrivateNotes($privateNotes)
    {
        $this->privateNotes = $privateNotes;

        return $this;
    }

    /**
     * Get privateNotes
     *
     * @return boolean 
     */
    public function getPrivateNotes()
    {
        return $this->privateNotes;
    }
}