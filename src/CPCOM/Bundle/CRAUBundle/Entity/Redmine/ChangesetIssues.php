<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Changeset Issues
 *
 * @ORM\Table(name="changeset_issues")
 * @ORM\Entity
 */
class ChangesetIssues
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="changeset_id", type="integer", nullable=false)
     */
    private $changesetId;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="issue_id", type="integer", nullable=false)
     */
    private $issueId;

    /**
     * @return type 
     */
    public function getChangesetId()
    {
        return $this->changesetId;
    }

    /**
     * @param type $changesetId 
     */
    public function setChangesetId($changesetId)
    {
        $this->changesetId = $changesetId;
    }

    /**
     * @return type 
     */
    public function getIssueId()
    {
        return $this->issueId;
    }

    /**
     * @param type $issueId 
     */
    public function setIssueId($issueId)
    {
        $this->issueId = $issueId;
    }
}