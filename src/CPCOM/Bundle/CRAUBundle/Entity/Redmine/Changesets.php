<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Changesets
 *
 * @ORM\Table(name="changesets")
 * @ORM\Entity
 */
class Changesets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="repository_id", type="integer", nullable=false)
     */
    private $repositoryId;

    /**
     * @var string
     *
     * @ORM\Column(name="revision", type="string", length=255, nullable=false)
     */
    private $revision;

    /**
     * @var string
     *
     * @ORM\Column(name="committer", type="string", length=255, nullable=true)
     */
    private $committer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="committed_on", type="datetime", nullable=false)
     */
    private $committedOn;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="text", nullable=true)
     */
    private $comments;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="commit_date", type="date", nullable=true)
     */
    private $commitDate;

    /**
     * @var string
     *
     * @ORM\Column(name="scmid", type="string", length=255, nullable=true)
     */
    private $scmid;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set repositoryId
     *
     * @param integer $repositoryId
     * 
     * @return Changesets
     */
    public function setRepositoryId($repositoryId)
    {
        $this->repositoryId = $repositoryId;

        return $this;
    }

    /**
     * Get repositoryId
     *
     * @return integer 
     */
    public function getRepositoryId()
    {
        return $this->repositoryId;
    }

    /**
     * Set revision
     *
     * @param string $revision
     * 
     * @return Changesets
     */
    public function setRevision($revision)
    {
        $this->revision = $revision;

        return $this;
    }

    /**
     * Get revision
     *
     * @return string 
     */
    public function getRevision()
    {
        return $this->revision;
    }

    /**
     * Set committer
     *
     * @param string $committer
     * 
     * @return Changesets
     */
    public function setCommitter($committer)
    {
        $this->committer = $committer;

        return $this;
    }

    /**
     * Get committer
     *
     * @return string 
     */
    public function getCommitter()
    {
        return $this->committer;
    }

    /**
     * Set committedOn
     *
     * @param \DateTime $committedOn
     * 
     * @return Changesets
     */
    public function setCommittedOn($committedOn)
    {
        $this->committedOn = $committedOn;

        return $this;
    }

    /**
     * Get committedOn
     *
     * @return \DateTime 
     */
    public function getCommittedOn()
    {
        return $this->committedOn;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * 
     * @return Changesets
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set commitDate
     *
     * @param \DateTime $commitDate
     * 
     * @return Changesets
     */
    public function setCommitDate($commitDate)
    {
        $this->commitDate = $commitDate;

        return $this;
    }

    /**
     * Get commitDate
     *
     * @return \DateTime 
     */
    public function getCommitDate()
    {
        return $this->commitDate;
    }

    /**
     * Set scmid
     *
     * @param string $scmid
     * 
     * @return Changesets
     */
    public function setScmid($scmid)
    {
        $this->scmid = $scmid;

        return $this;
    }

    /**
     * Get scmid
     *
     * @return string 
     */
    public function getScmid()
    {
        return $this->scmid;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * 
     * @return Changesets
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }
}