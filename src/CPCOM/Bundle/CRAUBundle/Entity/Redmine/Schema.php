<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Schema
 *
 * @ORM\Table(name="schema_migrations")
 * @ORM\Entity
 */
class Schema
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="version", type="string", length=255, nullable=false)
     */
    private $version;

    /**
     * @return type 
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param type $version 
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }
}