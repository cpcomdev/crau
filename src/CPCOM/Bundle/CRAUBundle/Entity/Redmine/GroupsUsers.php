<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * GroupsUsers
 *
 * @ORM\Table(name="groups_users")
 * @ORM\Entity
 */
class GroupsUsers
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     */
    private $groupId;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @return type 
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param type $groupId 
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @return type 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param type $userId 
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
}