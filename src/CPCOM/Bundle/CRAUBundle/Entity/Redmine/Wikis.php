<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wikis
 *
 * @ORM\Table(name="wikis")
 * @ORM\Entity
 */
class Wikis
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var string
     *
     * @ORM\Column(name="start_page", type="string", length=255, nullable=false)
     */
    private $startPage;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set projectId
     *
     * @param integer $projectId
     * 
     * @return Wikis
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * Get projectId
     *
     * @return integer 
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Set startPage
     *
     * @param string $startPage
     * 
     * @return Wikis
     */
    public function setStartPage($startPage)
    {
        $this->startPage = $startPage;

        return $this;
    }

    /**
     * Get startPage
     *
     * @return string 
     */
    public function getStartPage()
    {
        return $this->startPage;
    }

    /**
     * Set status
     *
     * @param integer $status
     * 
     * @return Wikis
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}