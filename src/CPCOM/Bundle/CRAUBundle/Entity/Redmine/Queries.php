<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Queries
 *
 * @ORM\Table(name="queries")
 * @ORM\Entity
 */
class Queries
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer", nullable=true)
     */
    private $projectId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="filters", type="text", nullable=true)
     */
    private $filters;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_public", type="boolean", nullable=false)
     */
    private $isPublic;

    /**
     * @var string
     *
     * @ORM\Column(name="column_names", type="text", nullable=true)
     */
    private $columnNames;

    /**
     * @var string
     *
     * @ORM\Column(name="sort_criteria", type="text", nullable=true)
     */
    private $sortCriteria;

    /**
     * @var string
     *
     * @ORM\Column(name="group_by", type="string", length=255, nullable=true)
     */
    private $groupBy;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set projectId
     *
     * @param integer $projectId
     * 
     * @return Queries
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * Get projectId
     *
     * @return integer 
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return Queries
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set filters
     *
     * @param string $filters
     * 
     * @return Queries
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * Get filters
     *
     * @return string 
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * 
     * @return Queries
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set isPublic
     *
     * @param boolean $isPublic
     * 
     * @return Queries
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic
     *
     * @return boolean 
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set columnNames
     *
     * @param string $columnNames
     * 
     * @return Queries
     */
    public function setColumnNames($columnNames)
    {
        $this->columnNames = $columnNames;

        return $this;
    }

    /**
     * Get columnNames
     *
     * @return string 
     */
    public function getColumnNames()
    {
        return $this->columnNames;
    }

    /**
     * Set sortCriteria
     *
     * @param string $sortCriteria
     * 
     * @return Queries
     */
    public function setSortCriteria($sortCriteria)
    {
        $this->sortCriteria = $sortCriteria;

        return $this;
    }

    /**
     * Get sortCriteria
     *
     * @return string 
     */
    public function getSortCriteria()
    {
        return $this->sortCriteria;
    }

    /**
     * Set groupBy
     *
     * @param string $groupBy
     * 
     * @return Queries
     */
    public function setGroupBy($groupBy)
    {
        $this->groupBy = $groupBy;

        return $this;
    }

    /**
     * Get groupBy
     *
     * @return string 
     */
    public function getGroupBy()
    {
        return $this->groupBy;
    }

    /**
     * Set type
     *
     * @param string $type
     * 
     * @return Queries
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
}