<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomFieldsProjects
 *
 * @ORM\Table(name="custom_fields_projects")
 * @ORM\Entity
 */
class CustomFieldsProjects
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="custil_field_id", type="integer", nullable=false)
     */
    private $customFildId;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @return type 
     */
    public function getCustomFildId()
    {
        return $this->customFildId;
    }

    /**
     * @param type $customFildId 
     */
    public function setCustomFildId($customFildId)
    {
        $this->customFildId = $customFildId;
    }

    /**
     * @return type 
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * @param type $projectId 
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;
    }
}