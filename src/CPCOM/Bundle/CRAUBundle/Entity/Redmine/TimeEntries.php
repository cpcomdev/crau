<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * TimeEntries
 *
 * @ORM\Table(name="time_entries")
 * @ORM\Entity
 */
class TimeEntries
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="issue_id", type="integer", nullable=true)
     */
    private $issueId;

    /**
     * @var float
     *
     * @ORM\Column(name="hours", type="float", nullable=false)
     */
    private $hours;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=255, nullable=true)
     */
    private $comments;

    /**
     * @var integer
     *
     * @ORM\Column(name="activity_id", type="integer", nullable=false)
     */
    private $activityId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="spent_on", type="date", nullable=false)
     */
    private $spentOn;

    /**
     * @var integer
     *
     * @ORM\Column(name="tyear", type="integer", nullable=false)
     */
    private $tyear;

    /**
     * @var integer
     *
     * @ORM\Column(name="tmonth", type="integer", nullable=false)
     */
    private $tmonth;

    /**
     * @var integer
     *
     * @ORM\Column(name="tweek", type="integer", nullable=false)
     */
    private $tweek;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     */
    private $createdOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_on", type="datetime", nullable=false)
     */
    private $updatedOn;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set projectId
     *
     * @param integer $projectId
     * 
     * @return TimeEntries
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * Get projectId
     *
     * @return integer 
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * 
     * @return TimeEntries
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set issueId
     *
     * @param integer $issueId
     * 
     * @return TimeEntries
     */
    public function setIssueId($issueId)
    {
        $this->issueId = $issueId;

        return $this;
    }

    /**
     * Get issueId
     *
     * @return integer 
     */
    public function getIssueId()
    {
        return $this->issueId;
    }

    /**
     * Set hours
     *
     * @param float $hours
     * 
     * @return TimeEntries
     */
    public function setHours($hours)
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get hours
     *
     * @return float 
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * 
     * @return TimeEntries
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set activityId
     *
     * @param integer $activityId
     * 
     * @return TimeEntries
     */
    public function setActivityId($activityId)
    {
        $this->activityId = $activityId;

        return $this;
    }

    /**
     * Get activityId
     *
     * @return integer 
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * Set spentOn
     *
     * @param \DateTime $spentOn
     * 
     * @return TimeEntries
     */
    public function setSpentOn($spentOn)
    {
        $this->spentOn = $spentOn;

        return $this;
    }

    /**
     * Get spentOn
     *
     * @return \DateTime 
     */
    public function getSpentOn()
    {
        return $this->spentOn;
    }

    /**
     * Set tyear
     *
     * @param integer $tyear
     * 
     * @return TimeEntries
     */
    public function setTyear($tyear)
    {
        $this->tyear = $tyear;

        return $this;
    }

    /**
     * Get tyear
     *
     * @return integer 
     */
    public function getTyear()
    {
        return $this->tyear;
    }

    /**
     * Set tmonth
     *
     * @param integer $tmonth
     * 
     * @return TimeEntries
     */
    public function setTmonth($tmonth)
    {
        $this->tmonth = $tmonth;

        return $this;
    }

    /**
     * Get tmonth
     *
     * @return integer 
     */
    public function getTmonth()
    {
        return $this->tmonth;
    }

    /**
     * Set tweek
     *
     * @param integer $tweek
     * 
     * @return TimeEntries
     */
    public function setTweek($tweek)
    {
        $this->tweek = $tweek;

        return $this;
    }

    /**
     * Get tweek
     *
     * @return integer 
     */
    public function getTweek()
    {
        return $this->tweek;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * 
     * @return TimeEntries
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set updatedOn
     *
     * @param \DateTime $updatedOn
     * 
     * @return TimeEntries
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    /**
     * Get updatedOn
     *
     * @return \DateTime 
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }
}