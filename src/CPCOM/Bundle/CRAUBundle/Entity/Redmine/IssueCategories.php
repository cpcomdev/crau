<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * IssueCategories
 *
 * @ORM\Table(name="issue_categories")
 * @ORM\Entity
 */
class IssueCategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="assigned_to_id", type="integer", nullable=true)
     */
    private $assignedToId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set projectId
     *
     * @param integer $projectId
     * 
     * @return IssueCategories
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * Get projectId
     *
     * @return integer 
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return IssueCategories
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set assignedToId
     *
     * @param integer $assignedToId
     * 
     * @return IssueCategories
     */
    public function setAssignedToId($assignedToId)
    {
        $this->assignedToId = $assignedToId;

        return $this;
    }

    /**
     * Get assignedToId
     *
     * @return integer 
     */
    public function getAssignedToId()
    {
        return $this->assignedToId;
    }
}