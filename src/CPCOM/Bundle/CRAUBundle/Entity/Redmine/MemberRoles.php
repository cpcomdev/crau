<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * MemberRoles
 *
 * @ORM\Table(name="member_roles")
 * @ORM\Entity
 */
class MemberRoles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="member_id", type="integer", nullable=false)
     */
    private $memberId;

    /**
     * @var integer
     *
     * @ORM\Column(name="role_id", type="integer", nullable=false)
     */
    private $roleId;

    /**
     * @var integer
     *
     * @ORM\Column(name="inherited_from", type="integer", nullable=true)
     */
    private $inheritedFrom;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set memberId
     *
     * @param integer $memberId
     * 
     * @return MemberRoles
     */
    public function setMemberId($memberId)
    {
        $this->memberId = $memberId;

        return $this;
    }

    /**
     * Get memberId
     *
     * @return integer 
     */
    public function getMemberId()
    {
        return $this->memberId;
    }

    /**
     * Set roleId
     *
     * @param integer $roleId
     * 
     * @return MemberRoles
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;

        return $this;
    }

    /**
     * Get roleId
     *
     * @return integer 
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

    /**
     * Set inheritedFrom
     *
     * @param integer $inheritedFrom
     * 
     * @return MemberRoles
     */
    public function setInheritedFrom($inheritedFrom)
    {
        $this->inheritedFrom = $inheritedFrom;

        return $this;
    }

    /**
     * Get inheritedFrom
     *
     * @return integer 
     */
    public function getInheritedFrom()
    {
        return $this->inheritedFrom;
    }
}