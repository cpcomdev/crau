<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Boards
 *
 * @ORM\Table(name="boards")
 * @ORM\Entity
 */
class Boards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var integer
     *
     * @ORM\Column(name="topics_count", type="integer", nullable=false)
     */
    private $topicsCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="messages_count", type="integer", nullable=false)
     */
    private $messagesCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_message_id", type="integer", nullable=true)
     */
    private $lastMessageId;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true)
     */
    private $parentId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set projectId
     *
     * @param integer $projectId
     * 
     * @return Boards
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * Get projectId
     *
     * @return integer 
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return Boards
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * 
     * @return Boards
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set position
     *
     * @param integer $position
     * 
     * @return Boards
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set topicsCount
     *
     * @param integer $topicsCount
     * 
     * @return Boards
     */
    public function setTopicsCount($topicsCount)
    {
        $this->topicsCount = $topicsCount;

        return $this;
    }

    /**
     * Get topicsCount
     *
     * @return integer 
     */
    public function getTopicsCount()
    {
        return $this->topicsCount;
    }

    /**
     * Set messagesCount
     *
     * @param integer $messagesCount
     * 
     * @return Boards
     */
    public function setMessagesCount($messagesCount)
    {
        $this->messagesCount = $messagesCount;

        return $this;
    }

    /**
     * Get messagesCount
     *
     * @return integer 
     */
    public function getMessagesCount()
    {
        return $this->messagesCount;
    }

    /**
     * Set lastMessageId
     *
     * @param integer $lastMessageId
     * 
     * @return Boards
     */
    public function setLastMessageId($lastMessageId)
    {
        $this->lastMessageId = $lastMessageId;

        return $this;
    }

    /**
     * Get lastMessageId
     *
     * @return integer 
     */
    public function getLastMessageId()
    {
        return $this->lastMessageId;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     * 
     * @return Boards
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parentId;
    }
}