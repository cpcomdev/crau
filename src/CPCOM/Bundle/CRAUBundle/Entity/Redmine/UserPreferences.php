<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserPreferences
 *
 * @ORM\Table(name="user_preferences")
 * @ORM\Entity
 */
class UserPreferences
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="others", type="text", nullable=true)
     */
    private $others;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hide_mail", type="boolean", nullable=true)
     */
    private $hideMail;

    /**
     * @var string
     *
     * @ORM\Column(name="time_zone", type="string", length=255, nullable=true)
     */
    private $timeZone;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * 
     * @return UserPreferences
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set others
     *
     * @param string $others
     * 
     * @return UserPreferences
     */
    public function setOthers($others)
    {
        $this->others = $others;

        return $this;
    }

    /**
     * Get others
     *
     * @return string 
     */
    public function getOthers()
    {
        return $this->others;
    }

    /**
     * Set hideMail
     *
     * @param boolean $hideMail
     * 
     * @return UserPreferences
     */
    public function setHideMail($hideMail)
    {
        $this->hideMail = $hideMail;

        return $this;
    }

    /**
     * Get hideMail
     *
     * @return boolean 
     */
    public function getHideMail()
    {
        return $this->hideMail;
    }

    /**
     * Set timeZone
     *
     * @param string $timeZone
     * 
     * @return UserPreferences
     */
    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;

        return $this;
    }

    /**
     * Get timeZone
     *
     * @return string 
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }
}