<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * WikiContentVersions
 *
 * @ORM\Table(name="wiki_content_versions")
 * @ORM\Entity
 */
class WikiContentVersions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="wiki_content_id", type="integer", nullable=false)
     */
    private $wikiContentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="page_id", type="integer", nullable=false)
     */
    private $pageId;

    /**
     * @var integer
     *
     * @ORM\Column(name="author_id", type="integer", nullable=true)
     */
    private $authorId;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="blob", nullable=true)
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="compression", type="string", length=6, nullable=true)
     */
    private $compression;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=255, nullable=true)
     */
    private $comments;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_on", type="datetime", nullable=false)
     */
    private $updatedOn;

    /**
     * @var integer
     *
     * @ORM\Column(name="version", type="integer", nullable=false)
     */
    private $version;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set wikiContentId
     *
     * @param integer $wikiContentId
     * 
     * @return WikiContentVersions
     */
    public function setWikiContentId($wikiContentId)
    {
        $this->wikiContentId = $wikiContentId;

        return $this;
    }

    /**
     * Get wikiContentId
     *
     * @return integer 
     */
    public function getWikiContentId()
    {
        return $this->wikiContentId;
    }

    /**
     * Set pageId
     *
     * @param integer $pageId
     * 
     * @return WikiContentVersions
     */
    public function setPageId($pageId)
    {
        $this->pageId = $pageId;

        return $this;
    }

    /**
     * Get pageId
     *
     * @return integer 
     */
    public function getPageId()
    {
        return $this->pageId;
    }

    /**
     * Set authorId
     *
     * @param integer $authorId
     * 
     * @return WikiContentVersions
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;

        return $this;
    }

    /**
     * Get authorId
     *
     * @return integer 
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * Set data
     *
     * @param string $data
     * 
     * @return WikiContentVersions
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set compression
     *
     * @param string $compression
     * 
     * @return WikiContentVersions
     */
    public function setCompression($compression)
    {
        $this->compression = $compression;

        return $this;
    }

    /**
     * Get compression
     *
     * @return string 
     */
    public function getCompression()
    {
        return $this->compression;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * 
     * @return WikiContentVersions
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set updatedOn
     *
     * @param \DateTime $updatedOn
     * 
     * @return WikiContentVersions
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    /**
     * Get updatedOn
     *
     * @return \DateTime 
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * Set version
     *
     * @param integer $version
     * 
     * @return WikiContentVersions
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer 
     */
    public function getVersion()
    {
        return $this->version;
    }
}