<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Redmine;

use Doctrine\ORM\Mapping as ORM;

/**
 * WikiRedirects
 *
 * @ORM\Table(name="wiki_redirects")
 * @ORM\Entity
 */
class WikiRedirects
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="wiki_id", type="integer", nullable=false)
     */
    private $wikiId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="redirects_to", type="string", length=255, nullable=true)
     */
    private $redirectsTo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     */
    private $createdOn;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set wikiId
     *
     * @param integer $wikiId
     * 
     * @return WikiRedirects
     */
    public function setWikiId($wikiId)
    {
        $this->wikiId = $wikiId;

        return $this;
    }

    /**
     * Get wikiId
     *
     * @return integer 
     */
    public function getWikiId()
    {
        return $this->wikiId;
    }

    /**
     * Set title
     *
     * @param string $title
     * 
     * @return WikiRedirects
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set redirectsTo
     *
     * @param string $redirectsTo
     * 
     * @return WikiRedirects
     */
    public function setRedirectsTo($redirectsTo)
    {
        $this->redirectsTo = $redirectsTo;

        return $this;
    }

    /**
     * Get redirectsTo
     *
     * @return string 
     */
    public function getRedirectsTo()
    {
        return $this->redirectsTo;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * 
     * @return WikiRedirects
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }
}