<?php
namespace CPCOM\Bundle\CRAUBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
/**
 * Entité stream reporting
 * 
 * @Assert\Callback(methods={"isdateDebutValid"})
 */
class CRAU
{
    protected $datePredefini;

    /**
     * Période début
     *
     * @assert\Date()
     */
    protected $dateDebut;

    /**
     * Période fin
     *
     * @assert\Date()
     */
    protected $dateFin;

    /**
     * Construct 
     */
    public function __construct()
    {
    }

    /**
     * Fusionne les résultats des requêtes Redmine et GLPI
     * 
     * @param type $allUsers           AllUsers
     * @param type $redmineElapsedTime RedmineElapsedTime
     * @param type $glpiElapsedTime    GlpiElapsedTime
     * 
     * @return Array
     */
    public function resultFusion($allUsers, $redmineElapsedTime, $glpiElapsedTime)
    {
        // Récupération des congés
        $dateDebut = new \DateTime($this->getDateDebut());
        $dateFin = new \DateTime($this->getDateFin());
        $fichier = "https://eurisk.mesconges.net/api/leaves?date=between,".$dateDebut->format('Y-m-d').",".$dateFin->format('Y-m-d')."&fields=owner.name,owner.login,owner.mail,owner.matricule,name,date,status=1,duration&department.id=1";

        $opts = array(
            'http'=>array(
                'method'=>'GET',
                'header'=>'Authorization: Lucca application=05100360-21d7-4401-a67b-472ecd1c3473'
            )
        );

        $context = \stream_context_create($opts);
        $datas = \file_get_contents($fichier, false, $context);
        $listConges = \json_decode($datas);

        /** Création du head */
        $result['head'] = array('Utilisateurs');
        // A décommenter si on veut vraiment toute la période en attendant toute la période a été supprimer
//        if ($this->getDatePredefini() == null) {
//            $periode = $this->generePeriode($redmineElapsedTime, $glpiElapsedTime);
//            $this->setDateDebut($periode['dateDebut']);
//            $this->setDateFin($periode['dateFin']);
//        }
        $datesSurPeriode = array();
        $totalColonne = array();
        while ($dateDebut != $dateFin) {
            $result['head'][] = $dateDebut->format('d/m');
            $datesSurPeriode[] = $dateDebut->format('Y-m-d');
            $totalColonne[$dateDebut->format('d/m/Y')] = 0;
            $dateDebut = new \DateTime(date('Y-m-d', strtotime("+1 days", strtotime($dateDebut->format('Y-m-d')))));
        }
        $result['head'][] = 'Total';

        /** Création du body */
        // Services
        foreach ($allUsers as $oneUser) {
            $result['body'][] = $oneUser['redmineGroupe'];
        }
        $result['body'] = \array_unique($result['body']);
        /**  Utilisateur par service */
        $serviceTabUser = array();
        foreach ($result['body'] as $service) {
            $totalOneService = array('redminePrenom' => 'Total', 'redmineNom' => $service);
            foreach ($allUsers as $oneUser) {
                $totalLigne = array();
                // Si l'user fait partie d'un des service on l'intègre dans le service
                if ($service == $oneUser['redmineGroupe']) {
                    // Si le service n'existe pas dans le $serviceTabUser on l'ajoute
                    if (!isset($serviceTabUser[$service])) {
                        $serviceTabUser += array($service=>null);
                    }
                    // Ajout nom/prenom/prestataire
                    $presta = $this->isPresta($oneUser['redmineSociete']);
                    $nomPrenomTab = array(
                        'redmineNom' => $oneUser['redmineNom'],
                        'redminePrenom' => $oneUser['redminePrenom'],
                        'prestataire' => $presta
                    );
                    // Ajout tache redmine
                    $tacheRedmine = array();
                    foreach ($redmineElapsedTime as $oneTacheRedmine) {
                        if (!isset($totalLigne['redmine#'.$oneTacheRedmine['redmineTaskId']])) {
                            $totalLigne += array('redmine#'.$oneTacheRedmine['redmineTaskId'] => 0);
                        }
                        // Si l'user courant à une tache redmine
                        if ($oneUser['redmineKeyname'] == $oneTacheRedmine['redmineKeyname']) {
                            // Si il a déjà initialiser avec une tache redmine
                            if (!empty($tacheRedmine)) {
                                // Ajout de tache ou fusion si la même tache est déjà présente
                                // Si la tache est déjà initialiser on ajoute les heures à la date
                                if (isset($tacheRedmine['tacheRedmine'][$oneTacheRedmine['redmineTaskId']])) {
                                    $date = new \DateTime($oneTacheRedmine['redmineDateTemps']);
                                    $totalColonne[$date->format('d/m/Y')] =  \round($totalColonne[$date->format('d/m/Y')] + (float) $oneTacheRedmine['redmineTempsHeure'], 2);
                                    $totalLigne['redmine#'.$oneTacheRedmine['redmineTaskId']] = \round($totalLigne['redmine#'.$oneTacheRedmine['redmineTaskId']] + (float) $oneTacheRedmine['redmineTempsHeure'], 2);
                                    $tacheRedmine['tacheRedmine'][$oneTacheRedmine['redmineTaskId']]['date'][$date->format('d/m/Y')] = \round($oneTacheRedmine['redmineTempsHeure'], 2);
                                    $tacheRedmine['tacheRedmine'][$oneTacheRedmine['redmineTaskId']]['total'] = $totalLigne['redmine#'.$oneTacheRedmine['redmineTaskId']];
                                // Sinon on initialise la tache
                                } else {
                                    $arrayResult = $this->initialiseDatePourTache($datesSurPeriode, $oneTacheRedmine, $totalColonne);
                                    $totalColonne = $arrayResult['totalColonne'];
                                    $totalLigne['redmine#'.$oneTacheRedmine['redmineTaskId']] = \round($totalLigne['redmine#'.$oneTacheRedmine['redmineTaskId']] + (float) $oneTacheRedmine['redmineTempsHeure'], 2);
                                    $tacheRedmine['tacheRedmine'][$oneTacheRedmine['redmineTaskId']] = array(
                                        'nom' => $oneTacheRedmine['redmineTaskCategorie'].' #'.$oneTacheRedmine['redmineTaskId'].' '.$oneTacheRedmine['redmineTaskSubject'],
                                        'date' => $arrayResult['arrayDate'],
                                        'total' => $totalLigne['redmine#'.$oneTacheRedmine['redmineTaskId']]
                                    );
                                }
                            // Sinon on initialise la tache
                            } else {
                                $arrayResult = $this->initialiseDatePourTache($datesSurPeriode, $oneTacheRedmine, $totalColonne);
                                $totalColonne = $arrayResult['totalColonne'];
                                $totalLigne['redmine#'.$oneTacheRedmine['redmineTaskId']] = \round($totalLigne['redmine#'.$oneTacheRedmine['redmineTaskId']] + (float) $oneTacheRedmine['redmineTempsHeure'], 2);
                                $tacheRedmine = array(
                                    'tacheRedmine' => array(
                                        $oneTacheRedmine['redmineTaskId'] => array(
                                            'nom' => $oneTacheRedmine['redmineTaskCategorie'].' #'.$oneTacheRedmine['redmineTaskId'].' '.$oneTacheRedmine['redmineTaskSubject'],
                                            'date' => $arrayResult['arrayDate'],
                                            'total' => $totalLigne['redmine#'.$oneTacheRedmine['redmineTaskId']]
                                        )
                                    )
                                );
                            }
                        }
                    }
                    // Ajout tache GLPI
                    $tacheGLPI = array();
                    foreach ($glpiElapsedTime as $oneTacheGLPI) {
                        if (!isset($totalLigne['glpi#'.$oneTacheGLPI['glpiTaskId']])) {
                            $totalLigne += array('glpi#'.$oneTacheGLPI['glpiTaskId'] => 0);
                        }
                        // Si l'user courant à une tache glpi
                        if ($oneUser['redmineKeyname'] == $oneTacheGLPI['glpiKeyname']) {
                            // Si il a déjà initialiser avec une tache glpi
                            if (!empty($tacheGLPI)) {
                                // Ajout de tache ou fusion si la même tache est déjà présente
                                // Si la tache est déjà initialiser on ajoute les heures à la date
                                if (isset($tacheGLPI['tacheGLPI'][$oneTacheGLPI['glpiTaskId']])) {
                                    $date = new \DateTime($oneTacheGLPI['glpiDateTemps']);
                                    $totalColonne[$date->format('d/m/Y')] = \round($totalColonne[$date->format('d/m/Y')] + (float) $oneTacheGLPI['glpiTempsHeure'], 2);
                                    $totalLigne['glpi#'.$oneTacheGLPI['glpiTaskId']] = \round($totalLigne['glpi#'.$oneTacheGLPI['glpiTaskId']] + (float) $oneTacheGLPI['glpiTempsHeure'], 2);
                                    $tacheGLPI['tacheGLPI'][$oneTacheGLPI['glpiTaskId']]['date'][$date->format('d/m/Y')] = \round($oneTacheGLPI['glpiTempsHeure'], 2);
                                    $tacheGLPI['tacheGLPI'][$oneTacheGLPI['glpiTaskId']]['total'] = $totalLigne['glpi#'.$oneTacheGLPI['glpiTaskId']];
                                // Sinon on initialise la tache
                                } else {
                                    $arrayResult = $this->initialiseDatePourTache($datesSurPeriode, $oneTacheGLPI, $totalColonne);
                                    $totalColonne = $arrayResult['totalColonne'];
                                    $totalLigne['glpi#'.$oneTacheGLPI['glpiTaskId']] = \round($totalLigne['glpi#'.$oneTacheGLPI['glpiTaskId']] + (float) $oneTacheGLPI['glpiTempsHeure'], 2);
                                    $tacheGLPI['tacheGLPI'][$oneTacheGLPI['glpiTaskId']] = array(
                                        'nom' => $oneTacheGLPI['glpiTaskCategorie'].' #'.$oneTacheGLPI['glpiTaskId'].' '.$oneTacheGLPI['glpiTaskSubject'],
                                        'date' => $arrayResult['arrayDate'],
                                        'total' => $totalLigne['glpi#'.$oneTacheGLPI['glpiTaskId']]
                                    );
                                }
                            // Sinon on initialise la tache
                            } else {
                                $arrayResult = $this->initialiseDatePourTache($datesSurPeriode, $oneTacheGLPI, $totalColonne);
                                $totalColonne = $arrayResult['totalColonne'];
                                $totalLigne['glpi#'.$oneTacheGLPI['glpiTaskId']] = \round($totalLigne['glpi#'.$oneTacheGLPI['glpiTaskId']] + (float) $oneTacheGLPI['glpiTempsHeure'], 2);
                                $tacheGLPI = array(
                                    'tacheGLPI' => array(
                                        $oneTacheGLPI['glpiTaskId'] => array(
                                            'nom' => $oneTacheGLPI['glpiTaskCategorie'].' #'.$oneTacheGLPI['glpiTaskId'].' '.$oneTacheGLPI['glpiTaskSubject'],
                                            'date' => $arrayResult['arrayDate'],
                                            'total' => $totalLigne['glpi#'.$oneTacheGLPI['glpiTaskId']]
                                        )
                                    )
                                );
                            }
                        }
                    }

                    // Ajout des totaux Redmine/GLPI fusionner
                    $totalRedmineGLPI = array();
                    if (isset($tacheRedmine['tacheRedmine'])) {
                        foreach ($tacheRedmine['tacheRedmine'] as $oneTacheRedmine) {
                            foreach ($oneTacheRedmine['date'] as $date => $heure) {
                                if (isset($totalRedmineGLPI['totalRedmineGLPI'][$date]['heures'])) {
                                    $totalRedmineGLPI['totalRedmineGLPI'][$date]['heures'] = \round($totalRedmineGLPI['totalRedmineGLPI'][$date]['heures'] + $heure, 2);
                                } else {
                                    $totalRedmineGLPI['totalRedmineGLPI'][$date] = array('heures' => \round($heure, 2));
                                }
                            }
                        }
                    }
                    if (isset($tacheGLPI['tacheGLPI'])) {
                        foreach ($tacheGLPI['tacheGLPI'] as $oneTacheGLPI) {
                            foreach ($oneTacheGLPI['date'] as $date => $heure) {
                                if (isset($totalRedmineGLPI['totalRedmineGLPI'][$date]['heures'])) {
                                    $totalRedmineGLPI['totalRedmineGLPI'][$date]['heures'] = \round($totalRedmineGLPI['totalRedmineGLPI'][$date]['heures'] + $heure, 2);
                                } else {
                                    $totalRedmineGLPI['totalRedmineGLPI'][$date] = array('heures' => \round($heure, 2));
                                }
                            }
                        }
                    }
                    if (isset($totalRedmineGLPI['totalRedmineGLPI'])) {
                        foreach ($totalRedmineGLPI['totalRedmineGLPI'] as $oneDate => $allEvent) {
                            if (isset($totalRedmineGLPI['totalRedmineGLPI']['total']['heures'])) {
                                if (isset($allEvent['heures'])) {
                                    $totalRedmineGLPI['totalRedmineGLPI']['total']['heures'] = \round($totalRedmineGLPI['totalRedmineGLPI']['total']['heures'] + $allEvent['heures'], 2);
                                }
                            } else {
                                if (!isset($allEvent['heures'])) {
                                    $totalRedmineGLPI['totalRedmineGLPI']['total']['heures'] = 0;
                                } else {
                                    $totalRedmineGLPI['totalRedmineGLPI']['total'] = array('heures' => $allEvent['heures']);
                                }
                            }
                            if (!isset($totalRedmineGLPI['totalRedmineGLPI'][$oneDate]['heures'])) {
                                $totalRedmineGLPI['totalRedmineGLPI'][$oneDate] = array('heure' => 0);
                            }
                            $totalRedmineGLPI = $this->gestionConges($listConges, $oneUser, $totalRedmineGLPI, $oneDate);
                        }

                    // Si aucune tache
                    } else {
                        foreach ($datesSurPeriode as $oneDate) {
                            $datetime = new \DateTime($oneDate);
                            $totalRedmineGLPI['totalRedmineGLPI'][$datetime->format('d/m/Y')] = array('heures' => 0);
                            $totalRedmineGLPI = $this->gestionConges($listConges, $oneUser, $totalRedmineGLPI, $oneDate);
                        }
                        $totalRedmineGLPI['totalRedmineGLPI']['total']['heures'] = 0;
                    }

                    // Fusion des tableaux
                    $serviceTabUser[$service][$oneUser['redmineKeyname']] = \array_merge($nomPrenomTab, $tacheRedmine, $tacheGLPI, $totalRedmineGLPI);

                    // Gestion totaux par service
                    foreach ($totalRedmineGLPI['totalRedmineGLPI'] as $k => $oneTotal) {
                        if (!isset($totalOneService[$k])) {
                            $totalOneService[$k] = 0;
                        }
                        $totalOneService[$k] = \round($totalOneService[$k] + (float) $oneTotal['heures'], 2);
                    }
                }
            }
            $serviceTabUser[$service]['totalService'] = $totalOneService;
        }
        $result['body'] = $serviceTabUser;

        /** Création du footer */
        $total = \round(\array_sum($totalColonne), 2);
        $result['foot'] = array('Total');
        foreach ($totalColonne as $oneTotalColonne) {
            $result['foot'][] = $oneTotalColonne;
        }
        $result['foot'][] = $total;

        return $result;
    }

    /**
     * Initialisation des dates a 0 sur la période sauf pour la tache en cour avec le temps heure
     * 
     * @param type $datesSurPeriode DateSurPeriode
     * @param type $oneTache        OneTache
     * @param type $totalColonne    TotalColonne
     * 
     * @return Array
     */
    protected function initialiseDatePourTache($datesSurPeriode, $oneTache, $totalColonne)
    {
        $arrayDate = array();
        // Pour Redmine
        if (isset($oneTache['redmineDateTemps']) && isset($oneTache['redmineTempsHeure'])) {
            foreach ($datesSurPeriode as $oneDate) {
                if ($oneTache['redmineDateTemps'] == $oneDate) {
                    $datetime = new \DateTime($oneDate);
                    $totalColonne[$datetime->format('d/m/Y')] = \round(\round($totalColonne[$datetime->format('d/m/Y')], 2) + \round((float) $oneTache['redmineTempsHeure'], 2), 2);
                    $arrayDate[$datetime->format('d/m/Y')] = \round($oneTache['redmineTempsHeure'], 2);
                } else {
                    $datetime = new \DateTime($oneDate);
                    $arrayDate[$datetime->format('d/m/Y')] = 0;
                }
            }
        // Pour GLPI
        } elseif (isset($oneTache['glpiDateTemps']) && isset($oneTache['glpiTempsHeure'])) {
            foreach ($datesSurPeriode as $oneDate) {
                if ($oneTache['glpiDateTemps'] == $oneDate) {
                    $datetime = new \DateTime($oneDate);
                    $totalColonne[$datetime->format('d/m/Y')] = \round(\round($totalColonne[$datetime->format('d/m/Y')], 2) + \round((float) $oneTache['glpiTempsHeure'], 2), 2);
                    $arrayDate[$datetime->format('d/m/Y')] = \round($oneTache['glpiTempsHeure'], 2);
                } else {
                    $datetime = new \DateTime($oneDate);
                    $arrayDate[$datetime->format('d/m/Y')] = 0;
                }
            }
        }

        $arrayReturn = array(
            'arrayDate' => $arrayDate,
            'totalColonne' => $totalColonne
        );

        return $arrayReturn;
    }

    /**
     * Génére la date début et de fin en fonction du résultat de la requête
     * 
     * @param type $redmineElapsedTime RedmineElapsedTime
     * @param type $glpiElapsedTime    GlpiElapsedTime
     * 
     * @return Array |Boolean
     */
    protected function generePeriode($redmineElapsedTime, $glpiElapsedTime)
    {
        // Date début
        if (isset($redmineElapsedTime) || isset($glpiElapsedTime)) {
            if (isset($redmineElapsedTime[0]['redmineDateTemps'])) {
                $dateDebutRedmine = $redmineElapsedTime[0]['redmineDateTemps'];
            }
            if (isset($glpiElapsedTime[0]['glpiDateTemps'])) {
                $dateDebutGLPI = $glpiElapsedTime[0]['glpiDateTemps'];
            }
            if (new \DateTime($dateDebutRedmine) < new \DateTime($dateDebutGLPI)) {
                $arrayReturn['dateDebut'] = $dateDebutRedmine;
            } else {
                $arrayReturn['dateDebut'] = $dateDebutGLPI;
            }
            // Date fin
            if (isset($redmineElapsedTime)) {
                $resultCountRedmine = \count($redmineElapsedTime)-1;
                $dateFinRedmine = $redmineElapsedTime[$resultCountRedmine]['redmineDateTemps'];
                $dateFinRedmine = \date('Y-m-d', \strtotime("+1 days", strtotime($dateFinRedmine)));
            }
            if (isset($glpiElapsedTime)) {
                $resultCountGLPI = \count($glpiElapsedTime)-1;
                $dateFinGLPI = $glpiElapsedTime[$resultCountGLPI]['glpiDateTemps'];
                $dateFinGLPI = \date('Y-m-d', \strtotime("+1 days", strtotime($dateFinGLPI)));
            }
            if (new \DateTime($dateFinRedmine) < new \DateTime($dateFinGLPI)) {
                $arrayReturn['dateFin'] = $dateFinGLPI;
            } else {
                $arrayReturn['dateFin'] = $dateFinRedmine;
            }

            return $arrayReturn;
        }

        return false;
    }

    /**
     * @param array  $listConges       Liste des Conges
     * @param array  $oneUser          Un utilisateur
     * @param array  $totalRedmineGLPI Le total glpi redmine
     * @param string $date             La date
     * 
     * @return type 
     */
    public function gestionConges($listConges, $oneUser, $totalRedmineGLPI, $date)
    {
        foreach ($listConges->data as $oneConge) {
            $dateConge = new \DateTime($oneConge->date);
            if ($dateConge->format('d/m/Y') == $date || $dateConge->format('Y-m-d') == $date) {
                if (\strtolower($oneConge->owner->mail) == \strtolower($oneUser['redmineMail'])) {
                    if ($dateConge->format('H') == '00' && $oneConge->duration == 1440) {
                        $matinApresMidiJournee = 'j';
                    } elseif($dateConge->format('H') == '00' && $oneConge->duration == 720) {
                        $matinApresMidiJournee = 'm';
                    } elseif($dateConge->format('H') == '12' && $oneConge->duration == 720) {
                        $matinApresMidiJournee = 'a';
                    } else {
                        $matinApresMidiJournee = 'j';
                    }
                    $totalRedmineGLPI['totalRedmineGLPI'][$dateConge->format('d/m/Y')]['conges'] = array(
                        $matinApresMidiJournee => array(
                            'type' => $oneConge->name,
                            'date' => $oneConge->date,
                            'duree' => $oneConge->duration
                        )
                    );
                }
            }
        }

        return $totalRedmineGLPI;
    }

    /**
     * @param string $redmineSociete Societe de l'utilisateur
     * 
     * @return boolean 
     */
    public function isPresta($redmineSociete)
    {
        if ($redmineSociete == 'CPCOM-Presta') {
            $presta = true;
        } else {
            $presta = false;
        }

        return $presta;
    }

    /**
     * @return type 
     */
    public function getDatePredefini()
    {
        return $this->datePredefini;
    }

    /**
     * @param type $datePredefini 
     */
    public function setDatePredefini($datePredefini)
    {
        $this->datePredefini = $datePredefini;
    }

    /**
     * @return type 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * @param type $dateDebut 
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;
    }

    /**
     * @return type 
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * @param type $dateFin 
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    }

    /**
     * @param ExecutionContext $context 
     */
    public function isdateDebutValid(ExecutionContextInterface $context)
    {
        if ($this->dateDebut != null && $this->dateFin != null) {
            if ($this->dateDebut > $this->dateFin) {
                $propertyPath = $context->getPropertyPath() . '.dateDebut';
                $context->addViolationAt($propertyPath, 'La date de fin doit être supérieur à la date de début', array(), null);
            }
        }elseif($this->dateDebut == null && $this->datePredefini == null){
            $propertyPath = $context->getPropertyPath() . '.dateDebut';
            $context->addViolationAt($propertyPath, 'La date de début n\'est pas rempli', array(), null);
        }elseif($this->dateFin == null && $this->datePredefini == null){
            $propertyPath = $context->getPropertyPath() . '.dateFin';
            $context->addViolationAt($propertyPath, 'La date de fin n\'est pas rempli', array(), null);
        }
    }

}