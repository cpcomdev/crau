<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiRulecacheperipheralmodels
 *
 * @ORM\Table(name="glpi_rulecacheperipheralmodels")
 * @ORM\Entity
 */
class GlpiRulecacheperipheralmodels
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="old_value", type="string", length=255, nullable=true)
     */
    private $oldValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="rules_id", type="integer", nullable=false)
     */
    private $rulesId;

    /**
     * @var string
     *
     * @ORM\Column(name="new_value", type="string", length=255, nullable=true)
     */
    private $newValue;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer", type="string", length=255, nullable=true)
     */
    private $manufacturer;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set oldValue
     *
     * @param string $oldValue
     * 
     * @return GlpiRulecacheperipheralmodels
     */
    public function setOldValue($oldValue)
    {
        $this->oldValue = $oldValue;

        return $this;
    }

    /**
     * Get oldValue
     *
     * @return string 
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    /**
     * Set rulesId
     *
     * @param integer $rulesId
     * 
     * @return GlpiRulecacheperipheralmodels
     */
    public function setRulesId($rulesId)
    {
        $this->rulesId = $rulesId;

        return $this;
    }

    /**
     * Get rulesId
     *
     * @return integer 
     */
    public function getRulesId()
    {
        return $this->rulesId;
    }

    /**
     * Set newValue
     *
     * @param string $newValue
     * 
     * @return GlpiRulecacheperipheralmodels
     */
    public function setNewValue($newValue)
    {
        $this->newValue = $newValue;

        return $this;
    }

    /**
     * Get newValue
     *
     * @return string 
     */
    public function getNewValue()
    {
        return $this->newValue;
    }

    /**
     * Set manufacturer
     *
     * @param string $manufacturer
     * 
     * @return GlpiRulecacheperipheralmodels
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return string 
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }
}