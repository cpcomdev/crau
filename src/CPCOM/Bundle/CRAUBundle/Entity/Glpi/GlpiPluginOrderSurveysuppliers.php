<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderSurveysuppliers
 *
 * @ORM\Table(name="glpi_plugin_order_surveysuppliers")
 * @ORM\Entity
 */
class GlpiPluginOrderSurveysuppliers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_orders_id", type="integer", nullable=false)
     */
    private $pluginOrderOrdersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer1", type="integer", nullable=false)
     */
    private $answer1;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer2", type="integer", nullable=false)
     */
    private $answer2;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer3", type="integer", nullable=false)
     */
    private $answer3;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer4", type="integer", nullable=false)
     */
    private $answer4;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer5", type="integer", nullable=false)
     */
    private $answer5;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiPluginOrderSurveysuppliers
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set isRecursive
     *
     * @param boolean $isRecursive
     * 
     * @return GlpiPluginOrderSurveysuppliers
     */
    public function setIsRecursive($isRecursive)
    {
        $this->isRecursive = $isRecursive;

        return $this;
    }

    /**
     * Get isRecursive
     *
     * @return boolean 
     */
    public function getIsRecursive()
    {
        return $this->isRecursive;
    }

    /**
     * Set pluginOrderOrdersId
     *
     * @param integer $pluginOrderOrdersId
     * 
     * @return GlpiPluginOrderSurveysuppliers
     */
    public function setPluginOrderOrdersId($pluginOrderOrdersId)
    {
        $this->pluginOrderOrdersId = $pluginOrderOrdersId;

        return $this;
    }

    /**
     * Get pluginOrderOrdersId
     *
     * @return integer 
     */
    public function getPluginOrderOrdersId()
    {
        return $this->pluginOrderOrdersId;
    }

    /**
     * Set suppliersId
     *
     * @param integer $suppliersId
     * 
     * @return GlpiPluginOrderSurveysuppliers
     */
    public function setSuppliersId($suppliersId)
    {
        $this->suppliersId = $suppliersId;

        return $this;
    }

    /**
     * Get suppliersId
     *
     * @return integer 
     */
    public function getSuppliersId()
    {
        return $this->suppliersId;
    }

    /**
     * Set answer1
     *
     * @param integer $answer1
     * 
     * @return GlpiPluginOrderSurveysuppliers
     */
    public function setAnswer1($answer1)
    {
        $this->answer1 = $answer1;

        return $this;
    }

    /**
     * Get answer1
     *
     * @return integer 
     */
    public function getAnswer1()
    {
        return $this->answer1;
    }

    /**
     * Set answer2
     *
     * @param integer $answer2
     * 
     * @return GlpiPluginOrderSurveysuppliers
     */
    public function setAnswer2($answer2)
    {
        $this->answer2 = $answer2;

        return $this;
    }

    /**
     * Get answer2
     *
     * @return integer 
     */
    public function getAnswer2()
    {
        return $this->answer2;
    }

    /**
     * Set answer3
     *
     * @param integer $answer3
     * 
     * @return GlpiPluginOrderSurveysuppliers
     */
    public function setAnswer3($answer3)
    {
        $this->answer3 = $answer3;

        return $this;
    }

    /**
     * Get answer3
     *
     * @return integer 
     */
    public function getAnswer3()
    {
        return $this->answer3;
    }

    /**
     * Set answer4
     *
     * @param integer $answer4
     * 
     * @return GlpiPluginOrderSurveysuppliers
     */
    public function setAnswer4($answer4)
    {
        $this->answer4 = $answer4;

        return $this;
    }

    /**
     * Get answer4
     *
     * @return integer 
     */
    public function getAnswer4()
    {
        return $this->answer4;
    }

    /**
     * Set answer5
     *
     * @param integer $answer5
     * 
     * @return GlpiPluginOrderSurveysuppliers
     */
    public function setAnswer5($answer5)
    {
        $this->answer5 = $answer5;

        return $this;
    }

    /**
     * Get answer5
     *
     * @return integer 
     */
    public function getAnswer5()
    {
        return $this->answer5;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiPluginOrderSurveysuppliers
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}