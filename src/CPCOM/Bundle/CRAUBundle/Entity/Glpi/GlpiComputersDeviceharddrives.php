<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDeviceharddrives
 *
 * @ORM\Table(name="glpi_computers_deviceharddrives")
 * @ORM\Entity
 */
class GlpiComputersDeviceharddrives
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="deviceharddrives_id", type="integer", nullable=false)
     */
    private $deviceharddrivesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="specificity", type="integer", nullable=false)
     */
    private $specificity;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiComputersDeviceharddrives
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set deviceharddrivesId
     *
     * @param integer $deviceharddrivesId
     * 
     * @return GlpiComputersDeviceharddrives
     */
    public function setDeviceharddrivesId($deviceharddrivesId)
    {
        $this->deviceharddrivesId = $deviceharddrivesId;

        return $this;
    }

    /**
     * Get deviceharddrivesId
     *
     * @return integer 
     */
    public function getDeviceharddrivesId()
    {
        return $this->deviceharddrivesId;
    }

    /**
     * Set specificity
     *
     * @param integer $specificity
     * 
     * @return GlpiComputersDeviceharddrives
     */
    public function setSpecificity($specificity)
    {
        $this->specificity = $specificity;

        return $this;
    }

    /**
     * Get specificity
     *
     * @return integer 
     */
    public function getSpecificity()
    {
        return $this->specificity;
    }
}