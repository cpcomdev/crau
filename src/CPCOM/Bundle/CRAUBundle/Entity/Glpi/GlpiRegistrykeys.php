<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiRegistrykeys
 *
 * @ORM\Table(name="glpi_registrykeys")
 * @ORM\Entity
 */
class GlpiRegistrykeys
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var string
     *
     * @ORM\Column(name="hive", type="string", length=255, nullable=true)
     */
    private $hive;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=true)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_name", type="string", length=255, nullable=true)
     */
    private $ocsName;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiRegistrykeys
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set hive
     *
     * @param string $hive
     * 
     * @return GlpiRegistrykeys
     */
    public function setHive($hive)
    {
        $this->hive = $hive;

        return $this;
    }

    /**
     * Get hive
     *
     * @return string 
     */
    public function getHive()
    {
        return $this->hive;
    }

    /**
     * Set path
     *
     * @param string $path
     * 
     * @return GlpiRegistrykeys
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set value
     *
     * @param string $value
     * 
     * @return GlpiRegistrykeys
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set ocsName
     *
     * @param string $ocsName
     * 
     * @return GlpiRegistrykeys
     */
    public function setOcsName($ocsName)
    {
        $this->ocsName = $ocsName;

        return $this;
    }

    /**
     * Get ocsName
     *
     * @return string 
     */
    public function getOcsName()
    {
        return $this->ocsName;
    }
}