<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDeviceprocessors
 *
 * @ORM\Table(name="glpi_computers_deviceprocessors")
 * @ORM\Entity
 */
class GlpiComputersDeviceprocessors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="deviceprocessors_id", type="integer", nullable=false)
     */
    private $deviceprocessorsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="specificity", type="integer", nullable=false)
     */
    private $specificity;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiComputersDeviceprocessors
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set deviceprocessorsId
     *
     * @param integer $deviceprocessorsId
     * 
     * @return GlpiComputersDeviceprocessors
     */
    public function setDeviceprocessorsId($deviceprocessorsId)
    {
        $this->deviceprocessorsId = $deviceprocessorsId;

        return $this;
    }

    /**
     * Get deviceprocessorsId
     *
     * @return integer 
     */
    public function getDeviceprocessorsId()
    {
        return $this->deviceprocessorsId;
    }

    /**
     * Set specificity
     *
     * @param integer $specificity
     * 
     * @return GlpiComputersDeviceprocessors
     */
    public function setSpecificity($specificity)
    {
        $this->specificity = $specificity;

        return $this;
    }

    /**
     * Get specificity
     *
     * @return integer 
     */
    public function getSpecificity()
    {
        return $this->specificity;
    }
}