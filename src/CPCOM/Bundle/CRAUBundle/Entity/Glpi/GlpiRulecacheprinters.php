<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiRulecacheprinters
 *
 * @ORM\Table(name="glpi_rulecacheprinters")
 * @ORM\Entity
 */
class GlpiRulecacheprinters
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="old_value", type="string", length=255, nullable=true)
     */
    private $oldValue;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer", type="string", length=255, nullable=false)
     */
    private $manufacturer;

    /**
     * @var integer
     *
     * @ORM\Column(name="rules_id", type="integer", nullable=false)
     */
    private $rulesId;

    /**
     * @var string
     *
     * @ORM\Column(name="new_value", type="string", length=255, nullable=true)
     */
    private $newValue;

    /**
     * @var string
     *
     * @ORM\Column(name="new_manufacturer", type="string", length=255, nullable=false)
     */
    private $newManufacturer;

    /**
     * @var string
     *
     * @ORM\Column(name="ignore_ocs_import", type="string", length=1, nullable=true)
     */
    private $ignoreOcsImport;

    /**
     * @var string
     *
     * @ORM\Column(name="is_global", type="string", length=1, nullable=true)
     */
    private $isGlobal;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set oldValue
     *
     * @param string $oldValue
     * 
     * @return GlpiRulecacheprinters
     */
    public function setOldValue($oldValue)
    {
        $this->oldValue = $oldValue;

        return $this;
    }

    /**
     * Get oldValue
     *
     * @return string 
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    /**
     * Set manufacturer
     *
     * @param string $manufacturer
     * 
     * @return GlpiRulecacheprinters
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return string 
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set rulesId
     *
     * @param integer $rulesId
     * 
     * @return GlpiRulecacheprinters
     */
    public function setRulesId($rulesId)
    {
        $this->rulesId = $rulesId;

        return $this;
    }

    /**
     * Get rulesId
     *
     * @return integer 
     */
    public function getRulesId()
    {
        return $this->rulesId;
    }

    /**
     * Set newValue
     *
     * @param string $newValue
     * 
     * @return GlpiRulecacheprinters
     */
    public function setNewValue($newValue)
    {
        $this->newValue = $newValue;

        return $this;
    }

    /**
     * Get newValue
     *
     * @return string 
     */
    public function getNewValue()
    {
        return $this->newValue;
    }

    /**
     * Set newManufacturer
     *
     * @param string $newManufacturer
     * 
     * @return GlpiRulecacheprinters
     */
    public function setNewManufacturer($newManufacturer)
    {
        $this->newManufacturer = $newManufacturer;

        return $this;
    }

    /**
     * Get newManufacturer
     *
     * @return string 
     */
    public function getNewManufacturer()
    {
        return $this->newManufacturer;
    }

    /**
     * Set ignoreOcsImport
     *
     * @param string $ignoreOcsImport
     * 
     * @return GlpiRulecacheprinters
     */
    public function setIgnoreOcsImport($ignoreOcsImport)
    {
        $this->ignoreOcsImport = $ignoreOcsImport;

        return $this;
    }

    /**
     * Get ignoreOcsImport
     *
     * @return string 
     */
    public function getIgnoreOcsImport()
    {
        return $this->ignoreOcsImport;
    }

    /**
     * Set isGlobal
     *
     * @param string $isGlobal
     * 
     * @return GlpiRulecacheprinters
     */
    public function setIsGlobal($isGlobal)
    {
        $this->isGlobal = $isGlobal;

        return $this;
    }

    /**
     * Get isGlobal
     *
     * @return string 
     */
    public function getIsGlobal()
    {
        return $this->isGlobal;
    }
}