<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginMassocsimportConfigs
 *
 * @ORM\Table(name="glpi_plugin_massocsimport_configs")
 * @ORM\Entity
 */
class GlpiPluginMassocsimportConfigs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="thread_log_frequency", type="integer", nullable=false)
     */
    private $threadLogFrequency;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_displayempty", type="integer", nullable=false)
     */
    private $isDisplayempty;

    /**
     * @var integer
     *
     * @ORM\Column(name="import_limit", type="integer", nullable=false)
     */
    private $importLimit;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsservers_id", type="integer", nullable=false)
     */
    private $ocsserversId;

    /**
     * @var integer
     *
     * @ORM\Column(name="delay_refresh", type="integer", nullable=false)
     */
    private $delayRefresh;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_ocs_update", type="boolean", nullable=false)
     */
    private $allowOcsUpdate;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set threadLogFrequency
     *
     * @param integer $threadLogFrequency
     * 
     * @return GlpiPluginMassocsimportConfigs
     */
    public function setThreadLogFrequency($threadLogFrequency)
    {
        $this->threadLogFrequency = $threadLogFrequency;

        return $this;
    }

    /**
     * Get threadLogFrequency
     *
     * @return integer 
     */
    public function getThreadLogFrequency()
    {
        return $this->threadLogFrequency;
    }

    /**
     * Set isDisplayempty
     *
     * @param integer $isDisplayempty
     * 
     * @return GlpiPluginMassocsimportConfigs
     */
    public function setIsDisplayempty($isDisplayempty)
    {
        $this->isDisplayempty = $isDisplayempty;

        return $this;
    }

    /**
     * Get isDisplayempty
     *
     * @return integer 
     */
    public function getIsDisplayempty()
    {
        return $this->isDisplayempty;
    }

    /**
     * Set importLimit
     *
     * @param integer $importLimit
     * 
     * @return GlpiPluginMassocsimportConfigs
     */
    public function setImportLimit($importLimit)
    {
        $this->importLimit = $importLimit;

        return $this;
    }

    /**
     * Get importLimit
     *
     * @return integer 
     */
    public function getImportLimit()
    {
        return $this->importLimit;
    }

    /**
     * Set ocsserversId
     *
     * @param integer $ocsserversId
     * 
     * @return GlpiPluginMassocsimportConfigs
     */
    public function setOcsserversId($ocsserversId)
    {
        $this->ocsserversId = $ocsserversId;

        return $this;
    }

    /**
     * Get ocsserversId
     *
     * @return integer 
     */
    public function getOcsserversId()
    {
        return $this->ocsserversId;
    }

    /**
     * Set delayRefresh
     *
     * @param integer $delayRefresh
     * 
     * @return GlpiPluginMassocsimportConfigs
     */
    public function setDelayRefresh($delayRefresh)
    {
        $this->delayRefresh = $delayRefresh;

        return $this;
    }

    /**
     * Get delayRefresh
     *
     * @return integer 
     */
    public function getDelayRefresh()
    {
        return $this->delayRefresh;
    }

    /**
     * Set allowOcsUpdate
     *
     * @param boolean $allowOcsUpdate
     * 
     * @return GlpiPluginMassocsimportConfigs
     */
    public function setAllowOcsUpdate($allowOcsUpdate)
    {
        $this->allowOcsUpdate = $allowOcsUpdate;

        return $this;
    }

    /**
     * Get allowOcsUpdate
     *
     * @return boolean 
     */
    public function getAllowOcsUpdate()
    {
        return $this->allowOcsUpdate;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiPluginMassocsimportConfigs
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}