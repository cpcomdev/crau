<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiSoftwarelicenses
 *
 * @ORM\Table(name="glpi_softwarelicenses")
 * @ORM\Entity
 */
class GlpiSoftwarelicenses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwares_id", type="integer", nullable=false)
     */
    private $softwaresId;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", nullable=false)
     */
    private $number;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwarelicensetypes_id", type="integer", nullable=false)
     */
    private $softwarelicensetypesId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=255, nullable=true)
     */
    private $serial;

    /**
     * @var string
     *
     * @ORM\Column(name="otherserial", type="string", length=255, nullable=true)
     */
    private $otherserial;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwareversions_id_buy", type="integer", nullable=false)
     */
    private $softwareversionsIdBuy;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwareversions_id_use", type="integer", nullable=false)
     */
    private $softwareversionsIdUse;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire", type="date", nullable=true)
     */
    private $expire;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set softwaresId
     *
     * @param integer $softwaresId
     * 
     * @return GlpiSoftwarelicenses
     */
    public function setSoftwaresId($softwaresId)
    {
        $this->softwaresId = $softwaresId;

        return $this;
    }

    /**
     * Get softwaresId
     *
     * @return integer 
     */
    public function getSoftwaresId()
    {
        return $this->softwaresId;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiSoftwarelicenses
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set isRecursive
     *
     * @param boolean $isRecursive
     * 
     * @return GlpiSoftwarelicenses
     */
    public function setIsRecursive($isRecursive)
    {
        $this->isRecursive = $isRecursive;

        return $this;
    }

    /**
     * Get isRecursive
     *
     * @return boolean 
     */
    public function getIsRecursive()
    {
        return $this->isRecursive;
    }

    /**
     * Set number
     *
     * @param integer $number
     * 
     * @return GlpiSoftwarelicenses
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set softwarelicensetypesId
     *
     * @param integer $softwarelicensetypesId
     * 
     * @return GlpiSoftwarelicenses
     */
    public function setSoftwarelicensetypesId($softwarelicensetypesId)
    {
        $this->softwarelicensetypesId = $softwarelicensetypesId;

        return $this;
    }

    /**
     * Get softwarelicensetypesId
     *
     * @return integer 
     */
    public function getSoftwarelicensetypesId()
    {
        return $this->softwarelicensetypesId;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiSoftwarelicenses
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set serial
     *
     * @param string $serial
     * 
     * @return GlpiSoftwarelicenses
     */
    public function setSerial($serial)
    {
        $this->serial = $serial;

        return $this;
    }

    /**
     * Get serial
     *
     * @return string 
     */
    public function getSerial()
    {
        return $this->serial;
    }

    /**
     * Set otherserial
     *
     * @param string $otherserial
     * 
     * @return GlpiSoftwarelicenses
     */
    public function setOtherserial($otherserial)
    {
        $this->otherserial = $otherserial;

        return $this;
    }

    /**
     * Get otherserial
     *
     * @return string 
     */
    public function getOtherserial()
    {
        return $this->otherserial;
    }

    /**
     * Set softwareversionsIdBuy
     *
     * @param integer $softwareversionsIdBuy
     * 
     * @return GlpiSoftwarelicenses
     */
    public function setSoftwareversionsIdBuy($softwareversionsIdBuy)
    {
        $this->softwareversionsIdBuy = $softwareversionsIdBuy;

        return $this;
    }

    /**
     * Get softwareversionsIdBuy
     *
     * @return integer 
     */
    public function getSoftwareversionsIdBuy()
    {
        return $this->softwareversionsIdBuy;
    }

    /**
     * Set softwareversionsIdUse
     *
     * @param integer $softwareversionsIdUse
     * 
     * @return GlpiSoftwarelicenses
     */
    public function setSoftwareversionsIdUse($softwareversionsIdUse)
    {
        $this->softwareversionsIdUse = $softwareversionsIdUse;

        return $this;
    }

    /**
     * Get softwareversionsIdUse
     *
     * @return integer 
     */
    public function getSoftwareversionsIdUse()
    {
        return $this->softwareversionsIdUse;
    }

    /**
     * Set expire
     *
     * @param \DateTime $expire
     * 
     * @return GlpiSoftwarelicenses
     */
    public function setExpire($expire)
    {
        $this->expire = $expire;

        return $this;
    }

    /**
     * Get expire
     *
     * @return \DateTime 
     */
    public function getExpire()
    {
        return $this->expire;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiSoftwarelicenses
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set dateMod
     *
     * @param \DateTime $dateMod
     * 
     * @return GlpiSoftwarelicenses
     */
    public function setDateMod($dateMod)
    {
        $this->dateMod = $dateMod;

        return $this;
    }

    /**
     * Get dateMod
     *
     * @return \DateTime 
     */
    public function getDateMod()
    {
        return $this->dateMod;
    }
}