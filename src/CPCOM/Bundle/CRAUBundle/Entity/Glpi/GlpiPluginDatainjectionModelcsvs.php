<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginDatainjectionModelcsvs
 *
 * @ORM\Table(name="glpi_plugin_datainjection_modelcsvs")
 * @ORM\Entity
 */
class GlpiPluginDatainjectionModelcsvs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="models_id", type="integer", nullable=false)
     */
    private $modelsId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255, nullable=false)
     */
    private $itemtype;

    /**
     * @var string
     *
     * @ORM\Column(name="delimiter", type="string", length=1, nullable=false)
     */
    private $delimiter;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_header_present", type="boolean", nullable=false)
     */
    private $isHeaderPresent;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modelsId
     *
     * @param integer $modelsId
     * 
     * @return GlpiPluginDatainjectionModelcsvs
     */
    public function setModelsId($modelsId)
    {
        $this->modelsId = $modelsId;

        return $this;
    }

    /**
     * Get modelsId
     *
     * @return integer 
     */
    public function getModelsId()
    {
        return $this->modelsId;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     * 
     * @return GlpiPluginDatainjectionModelcsvs
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string 
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set delimiter
     *
     * @param string $delimiter
     * 
     * @return GlpiPluginDatainjectionModelcsvs
     */
    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;

        return $this;
    }

    /**
     * Get delimiter
     *
     * @return string 
     */
    public function getDelimiter()
    {
        return $this->delimiter;
    }

    /**
     * Set isHeaderPresent
     *
     * @param boolean $isHeaderPresent
     * 
     * @return GlpiPluginDatainjectionModelcsvs
     */
    public function setIsHeaderPresent($isHeaderPresent)
    {
        $this->isHeaderPresent = $isHeaderPresent;

        return $this;
    }

    /**
     * Get isHeaderPresent
     *
     * @return boolean 
     */
    public function getIsHeaderPresent()
    {
        return $this->isHeaderPresent;
    }
}