<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiBookmarksUsers
 *
 * @ORM\Table(name="glpi_bookmarks_users")
 * @ORM\Entity
 */
class GlpiBookmarksUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=100, nullable=false)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="bookmarks_id", type="integer", nullable=false)
     */
    private $bookmarksId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usersId
     *
     * @param integer $usersId
     * 
     * @return GlpiBookmarksUsers
     */
    public function setUsersId($usersId)
    {
        $this->usersId = $usersId;

        return $this;
    }

    /**
     * Get usersId
     *
     * @return integer 
     */
    public function getUsersId()
    {
        return $this->usersId;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     * 
     * @return GlpiBookmarksUsers
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string 
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set bookmarksId
     *
     * @param integer $bookmarksId
     * 
     * @return GlpiBookmarksUsers
     */
    public function setBookmarksId($bookmarksId)
    {
        $this->bookmarksId = $bookmarksId;

        return $this;
    }

    /**
     * Get bookmarksId
     *
     * @return integer 
     */
    public function getBookmarksId()
    {
        return $this->bookmarksId;
    }
}