<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicesoundcards
 *
 * @ORM\Table(name="glpi_computers_devicesoundcards")
 * @ORM\Entity
 */
class GlpiComputersDevicesoundcards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicesoundcards_id", type="integer", nullable=false)
     */
    private $devicesoundcardsId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiComputersDevicesoundcards
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set devicesoundcardsId
     *
     * @param integer $devicesoundcardsId
     * 
     * @return GlpiComputersDevicesoundcards
     */
    public function setDevicesoundcardsId($devicesoundcardsId)
    {
        $this->devicesoundcardsId = $devicesoundcardsId;

        return $this;
    }

    /**
     * Get devicesoundcardsId
     *
     * @return integer 
     */
    public function getDevicesoundcardsId()
    {
        return $this->devicesoundcardsId;
    }
}