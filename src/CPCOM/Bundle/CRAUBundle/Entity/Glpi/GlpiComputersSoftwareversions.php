<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersSoftwareversions
 *
 * @ORM\Table(name="glpi_computers_softwareversions")
 * @ORM\Entity
 */
class GlpiComputersSoftwareversions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwareversions_id", type="integer", nullable=false)
     */
    private $softwareversionsId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_template", type="boolean", nullable=false)
     */
    private $isTemplate;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiComputersSoftwareversions
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set softwareversionsId
     *
     * @param integer $softwareversionsId
     * 
     * @return GlpiComputersSoftwareversions
     */
    public function setSoftwareversionsId($softwareversionsId)
    {
        $this->softwareversionsId = $softwareversionsId;

        return $this;
    }

    /**
     * Get softwareversionsId
     *
     * @return integer 
     */
    public function getSoftwareversionsId()
    {
        return $this->softwareversionsId;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * 
     * @return GlpiComputersSoftwareversions
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set isTemplate
     *
     * @param boolean $isTemplate
     * 
     * @return GlpiComputersSoftwareversions
     */
    public function setIsTemplate($isTemplate)
    {
        $this->isTemplate = $isTemplate;

        return $this;
    }

    /**
     * Get isTemplate
     *
     * @return boolean 
     */
    public function getIsTemplate()
    {
        return $this->isTemplate;
    }
}