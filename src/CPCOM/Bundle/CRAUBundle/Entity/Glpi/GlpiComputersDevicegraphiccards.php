<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicegraphiccards
 *
 * @ORM\Table(name="glpi_computers_devicegraphiccards")
 * @ORM\Entity
 */
class GlpiComputersDevicegraphiccards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicegraphiccards_id", type="integer", nullable=false)
     */
    private $devicegraphiccardsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="specificity", type="integer", nullable=false)
     */
    private $specificity;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiComputersDevicegraphiccards
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set devicegraphiccardsId
     *
     * @param integer $devicegraphiccardsId
     * 
     * @return GlpiComputersDevicegraphiccards
     */
    public function setDevicegraphiccardsId($devicegraphiccardsId)
    {
        $this->devicegraphiccardsId = $devicegraphiccardsId;

        return $this;
    }

    /**
     * Get devicegraphiccardsId
     *
     * @return integer 
     */
    public function getDevicegraphiccardsId()
    {
        return $this->devicegraphiccardsId;
    }

    /**
     * Set specificity
     *
     * @param integer $specificity
     * 
     * @return GlpiComputersDevicegraphiccards
     */
    public function setSpecificity($specificity)
    {
        $this->specificity = $specificity;

        return $this;
    }

    /**
     * Get specificity
     *
     * @return integer 
     */
    public function getSpecificity()
    {
        return $this->specificity;
    }
}