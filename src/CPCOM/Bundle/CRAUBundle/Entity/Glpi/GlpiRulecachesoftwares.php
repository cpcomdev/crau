<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiRulecachesoftwares
 *
 * @ORM\Table(name="glpi_rulecachesoftwares")
 * @ORM\Entity
 */
class GlpiRulecachesoftwares
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="old_value", type="string", length=255, nullable=true)
     */
    private $oldValue;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer", type="string", length=255, nullable=false)
     */
    private $manufacturer;

    /**
     * @var integer
     *
     * @ORM\Column(name="rules_id", type="integer", nullable=false)
     */
    private $rulesId;

    /**
     * @var string
     *
     * @ORM\Column(name="new_value", type="string", length=255, nullable=true)
     */
    private $newValue;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=255, nullable=true)
     */
    private $version;

    /**
     * @var string
     *
     * @ORM\Column(name="new_manufacturer", type="string", length=255, nullable=false)
     */
    private $newManufacturer;

    /**
     * @var string
     *
     * @ORM\Column(name="ignore_ocs_import", type="string", length=1, nullable=true)
     */
    private $ignoreOcsImport;

    /**
     * @var string
     *
     * @ORM\Column(name="is_helpdesk_visible", type="string", length=1, nullable=true)
     */
    private $isHelpdeskVisible;

    /**
     * @var string
     *
     * @ORM\Column(name="entities_id", type="string", length=255, nullable=true)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="new_entities_id", type="string", length=255, nullable=true)
     */
    private $newEntitiesId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set oldValue
     *
     * @param string $oldValue
     * 
     * @return GlpiRulecachesoftwares
     */
    public function setOldValue($oldValue)
    {
        $this->oldValue = $oldValue;

        return $this;
    }

    /**
     * Get oldValue
     *
     * @return string 
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    /**
     * Set manufacturer
     *
     * @param string $manufacturer
     * 
     * @return GlpiRulecachesoftwares
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return string 
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set rulesId
     *
     * @param integer $rulesId
     * 
     * @return GlpiRulecachesoftwares
     */
    public function setRulesId($rulesId)
    {
        $this->rulesId = $rulesId;

        return $this;
    }

    /**
     * Get rulesId
     *
     * @return integer 
     */
    public function getRulesId()
    {
        return $this->rulesId;
    }

    /**
     * Set newValue
     *
     * @param string $newValue
     * 
     * @return GlpiRulecachesoftwares
     */
    public function setNewValue($newValue)
    {
        $this->newValue = $newValue;

        return $this;
    }

    /**
     * Get newValue
     *
     * @return string 
     */
    public function getNewValue()
    {
        return $this->newValue;
    }

    /**
     * Set version
     *
     * @param string $version
     * 
     * @return GlpiRulecachesoftwares
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string 
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set newManufacturer
     *
     * @param string $newManufacturer
     * 
     * @return GlpiRulecachesoftwares
     */
    public function setNewManufacturer($newManufacturer)
    {
        $this->newManufacturer = $newManufacturer;

        return $this;
    }

    /**
     * Get newManufacturer
     *
     * @return string 
     */
    public function getNewManufacturer()
    {
        return $this->newManufacturer;
    }

    /**
     * Set ignoreOcsImport
     *
     * @param string $ignoreOcsImport
     * 
     * @return GlpiRulecachesoftwares
     */
    public function setIgnoreOcsImport($ignoreOcsImport)
    {
        $this->ignoreOcsImport = $ignoreOcsImport;

        return $this;
    }

    /**
     * Get ignoreOcsImport
     *
     * @return string 
     */
    public function getIgnoreOcsImport()
    {
        return $this->ignoreOcsImport;
    }

    /**
     * Set isHelpdeskVisible
     *
     * @param string $isHelpdeskVisible
     * 
     * @return GlpiRulecachesoftwares
     */
    public function setIsHelpdeskVisible($isHelpdeskVisible)
    {
        $this->isHelpdeskVisible = $isHelpdeskVisible;

        return $this;
    }

    /**
     * Get isHelpdeskVisible
     *
     * @return string 
     */
    public function getIsHelpdeskVisible()
    {
        return $this->isHelpdeskVisible;
    }

    /**
     * Set entitiesId
     *
     * @param string $entitiesId
     * 
     * @return GlpiRulecachesoftwares
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return string 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set newEntitiesId
     *
     * @param string $newEntitiesId
     * 
     * @return GlpiRulecachesoftwares
     */
    public function setNewEntitiesId($newEntitiesId)
    {
        $this->newEntitiesId = $newEntitiesId;

        return $this;
    }

    /**
     * Get newEntitiesId
     *
     * @return string 
     */
    public function getNewEntitiesId()
    {
        return $this->newEntitiesId;
    }
}