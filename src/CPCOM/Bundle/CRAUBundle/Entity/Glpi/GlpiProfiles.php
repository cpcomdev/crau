<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiProfiles
 *
 * @ORM\Table(name="glpi_profiles")
 * @ORM\Entity
 */
class GlpiProfiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="interface", type="string", length=255, nullable=true)
     */
    private $interface;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_default", type="boolean", nullable=false)
     */
    private $isDefault;

    /**
     * @var string
     *
     * @ORM\Column(name="computer", type="string", length=1, nullable=true)
     */
    private $computer;

    /**
     * @var string
     *
     * @ORM\Column(name="monitor", type="string", length=1, nullable=true)
     */
    private $monitor;

    /**
     * @var string
     *
     * @ORM\Column(name="software", type="string", length=1, nullable=true)
     */
    private $software;

    /**
     * @var string
     *
     * @ORM\Column(name="networking", type="string", length=1, nullable=true)
     */
    private $networking;

    /**
     * @var string
     *
     * @ORM\Column(name="printer", type="string", length=1, nullable=true)
     */
    private $printer;

    /**
     * @var string
     *
     * @ORM\Column(name="peripheral", type="string", length=1, nullable=true)
     */
    private $peripheral;

    /**
     * @var string
     *
     * @ORM\Column(name="cartridge", type="string", length=1, nullable=true)
     */
    private $cartridge;

    /**
     * @var string
     *
     * @ORM\Column(name="consumable", type="string", length=1, nullable=true)
     */
    private $consumable;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=1, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="string", length=1, nullable=true)
     */
    private $notes;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_enterprise", type="string", length=1, nullable=true)
     */
    private $contactEnterprise;

    /**
     * @var string
     *
     * @ORM\Column(name="document", type="string", length=1, nullable=true)
     */
    private $document;

    /**
     * @var string
     *
     * @ORM\Column(name="contract", type="string", length=1, nullable=true)
     */
    private $contract;

    /**
     * @var string
     *
     * @ORM\Column(name="infocom", type="string", length=1, nullable=true)
     */
    private $infocom;

    /**
     * @var string
     *
     * @ORM\Column(name="knowbase", type="string", length=1, nullable=true)
     */
    private $knowbase;

    /**
     * @var string
     *
     * @ORM\Column(name="knowbase_admin", type="string", length=1, nullable=true)
     */
    private $knowbaseAdmin;

    /**
     * @var string
     *
     * @ORM\Column(name="faq", type="string", length=1, nullable=true)
     */
    private $faq;

    /**
     * @var string
     *
     * @ORM\Column(name="reservation_helpdesk", type="string", length=1, nullable=true)
     */
    private $reservationHelpdesk;

    /**
     * @var string
     *
     * @ORM\Column(name="reservation_central", type="string", length=1, nullable=true)
     */
    private $reservationCentral;

    /**
     * @var string
     *
     * @ORM\Column(name="reports", type="string", length=1, nullable=true)
     */
    private $reports;

    /**
     * @var string
     *
     * @ORM\Column(name="ocsng", type="string", length=1, nullable=true)
     */
    private $ocsng;

    /**
     * @var string
     *
     * @ORM\Column(name="view_ocsng", type="string", length=1, nullable=true)
     */
    private $viewOcsng;

    /**
     * @var string
     *
     * @ORM\Column(name="sync_ocsng", type="string", length=1, nullable=true)
     */
    private $syncOcsng;

    /**
     * @var string
     *
     * @ORM\Column(name="dropdown", type="string", length=1, nullable=true)
     */
    private $dropdown;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_dropdown", type="string", length=1, nullable=true)
     */
    private $entityDropdown;

    /**
     * @var string
     *
     * @ORM\Column(name="device", type="string", length=1, nullable=true)
     */
    private $device;

    /**
     * @var string
     *
     * @ORM\Column(name="typedoc", type="string", length=1, nullable=true)
     */
    private $typedoc;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=1, nullable=true)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="config", type="string", length=1, nullable=true)
     */
    private $config;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_ticket", type="string", length=1, nullable=true)
     */
    private $ruleTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_rule_ticket", type="string", length=1, nullable=true)
     */
    private $entityRuleTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_ocs", type="string", length=1, nullable=true)
     */
    private $ruleOcs;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_ldap", type="string", length=1, nullable=true)
     */
    private $ruleLdap;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_softwarecategories", type="string", length=1, nullable=true)
     */
    private $ruleSoftwarecategories;

    /**
     * @var string
     *
     * @ORM\Column(name="search_config", type="string", length=1, nullable=true)
     */
    private $searchConfig;

    /**
     * @var string
     *
     * @ORM\Column(name="search_config_global", type="string", length=1, nullable=true)
     */
    private $searchConfigGlobal;

    /**
     * @var string
     *
     * @ORM\Column(name="check_update", type="string", length=1, nullable=true)
     */
    private $checkUpdate;

    /**
     * @var string
     *
     * @ORM\Column(name="profile", type="string", length=1, nullable=true)
     */
    private $profile;

    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=1, nullable=true)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="user_authtype", type="string", length=1, nullable=true)
     */
    private $userAuthtype;

    /**
     * @var string
     *
     * @ORM\Column(name="group", type="string", length=1, nullable=true)
     */
    private $group;

    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=1, nullable=true)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="transfer", type="string", length=1, nullable=true)
     */
    private $transfer;

    /**
     * @var string
     *
     * @ORM\Column(name="logs", type="string", length=1, nullable=true)
     */
    private $logs;

    /**
     * @var string
     *
     * @ORM\Column(name="reminder_public", type="string", length=1, nullable=true)
     */
    private $reminderPublic;

    /**
     * @var string
     *
     * @ORM\Column(name="bookmark_public", type="string", length=1, nullable=true)
     */
    private $bookmarkPublic;

    /**
     * @var string
     *
     * @ORM\Column(name="backup", type="string", length=1, nullable=true)
     */
    private $backup;

    /**
     * @var string
     *
     * @ORM\Column(name="create_ticket", type="string", length=1, nullable=true)
     */
    private $createTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="delete_ticket", type="string", length=1, nullable=true)
     */
    private $deleteTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="add_followups", type="string", length=1, nullable=true)
     */
    private $addFollowups;

    /**
     * @var string
     *
     * @ORM\Column(name="group_add_followups", type="string", length=1, nullable=true)
     */
    private $groupAddFollowups;

    /**
     * @var string
     *
     * @ORM\Column(name="global_add_followups", type="string", length=1, nullable=true)
     */
    private $globalAddFollowups;

    /**
     * @var string
     *
     * @ORM\Column(name="global_add_tasks", type="string", length=1, nullable=true)
     */
    private $globalAddTasks;

    /**
     * @var string
     *
     * @ORM\Column(name="update_ticket", type="string", length=1, nullable=true)
     */
    private $updateTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="update_priority", type="string", length=1, nullable=true)
     */
    private $updatePriority;

    /**
     * @var string
     *
     * @ORM\Column(name="own_ticket", type="string", length=1, nullable=true)
     */
    private $ownTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="steal_ticket", type="string", length=1, nullable=true)
     */
    private $stealTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="assign_ticket", type="string", length=1, nullable=true)
     */
    private $assignTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="show_all_ticket", type="string", length=1, nullable=true)
     */
    private $showAllTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="show_assign_ticket", type="string", length=1, nullable=true)
     */
    private $showAssignTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="show_full_ticket", type="string", length=1, nullable=true)
     */
    private $showFullTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="observe_ticket", type="string", length=1, nullable=true)
     */
    private $observeTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="update_followups", type="string", length=1, nullable=true)
     */
    private $updateFollowups;

    /**
     * @var string
     *
     * @ORM\Column(name="update_tasks", type="string", length=1, nullable=true)
     */
    private $updateTasks;

    /**
     * @var string
     *
     * @ORM\Column(name="show_planning", type="string", length=1, nullable=true)
     */
    private $showPlanning;

    /**
     * @var string
     *
     * @ORM\Column(name="show_group_planning", type="string", length=1, nullable=true)
     */
    private $showGroupPlanning;

    /**
     * @var string
     *
     * @ORM\Column(name="show_all_planning", type="string", length=1, nullable=true)
     */
    private $showAllPlanning;

    /**
     * @var string
     *
     * @ORM\Column(name="statistic", type="string", length=1, nullable=true)
     */
    private $statistic;

    /**
     * @var string
     *
     * @ORM\Column(name="password_update", type="string", length=1, nullable=true)
     */
    private $passwordUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="helpdesk_hardware", type="integer", nullable=false)
     */
    private $helpdeskHardware;

    /**
     * @var string
     *
     * @ORM\Column(name="helpdesk_item_type", type="text", nullable=true)
     */
    private $helpdeskItemType;

    /**
     * @var string
     *
     * @ORM\Column(name="ticket_status", type="text", nullable=true)
     */
    private $ticketStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="show_group_ticket", type="string", length=1, nullable=true)
     */
    private $showGroupTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="show_group_hardware", type="string", length=1, nullable=true)
     */
    private $showGroupHardware;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_dictionnary_software", type="string", length=1, nullable=true)
     */
    private $ruleDictionnarySoftware;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_dictionnary_dropdown", type="string", length=1, nullable=true)
     */
    private $ruleDictionnaryDropdown;

    /**
     * @var string
     *
     * @ORM\Column(name="budget", type="string", length=1, nullable=true)
     */
    private $budget;

    /**
     * @var string
     *
     * @ORM\Column(name="import_externalauth_users", type="string", length=1, nullable=true)
     */
    private $importExternalauthUsers;

    /**
     * @var string
     *
     * @ORM\Column(name="notification", type="string", length=1, nullable=true)
     */
    private $notification;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_mailcollector", type="string", length=1, nullable=true)
     */
    private $ruleMailcollector;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="validate_ticket", type="string", length=1, nullable=true)
     */
    private $validateTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="create_validation", type="string", length=1, nullable=true)
     */
    private $createValidation;

    /**
     * @var string
     *
     * @ORM\Column(name="calendar", type="string", length=1, nullable=true)
     */
    private $calendar;

    /**
     * @var string
     *
     * @ORM\Column(name="sla", type="string", length=1, nullable=true)
     */
    private $sla;

    /**
     * @var string
     *
     * @ORM\Column(name="rule_dictionnary_printer", type="string", length=1, nullable=true)
     */
    private $ruleDictionnaryPrinter;

    /**
     * @var string
     *
     * @ORM\Column(name="clean_ocsng", type="string", length=1, nullable=true)
     */
    private $cleanOcsng;

    /**
     * @var string
     *
     * @ORM\Column(name="update_own_followups", type="string", length=1, nullable=true)
     */
    private $updateOwnFollowups;

    /**
     * @var string
     *
     * @ORM\Column(name="delete_followups", type="string", length=1, nullable=true)
     */
    private $deleteFollowups;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_helpdesk", type="string", length=1, nullable=true)
     */
    private $entityHelpdesk;

    /**
     * @var string
     *
     * @ORM\Column(name="show_my_problem", type="string", length=1, nullable=true)
     */
    private $showMyProblem;

    /**
     * @var string
     *
     * @ORM\Column(name="show_all_problem", type="string", length=1, nullable=true)
     */
    private $showAllProblem;

    /**
     * @var string
     *
     * @ORM\Column(name="edit_all_problem", type="string", length=1, nullable=true)
     */
    private $editAllProblem;

    /**
     * @var string
     *
     * @ORM\Column(name="problem_status", type="text", nullable=true)
     */
    private $problemStatus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="create_ticket_on_login", type="boolean", nullable=false)
     */
    private $createTicketOnLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="tickettemplate", type="string", length=1, nullable=true)
     */
    private $tickettemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="ticketrecurrent", type="string", length=1, nullable=true)
     */
    private $ticketrecurrent;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiProfiles
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set interface
     *
     * @param string $interface
     * 
     * @return GlpiProfiles
     */
    public function setInterface($interface)
    {
        $this->interface = $interface;

        return $this;
    }

    /**
     * Get interface
     *
     * @return string 
     */
    public function getInterface()
    {
        return $this->interface;
    }

    /**
     * Set isDefault
     *
     * @param boolean $isDefault
     * 
     * @return GlpiProfiles
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return boolean 
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set computer
     *
     * @param string $computer
     * 
     * @return GlpiProfiles
     */
    public function setComputer($computer)
    {
        $this->computer = $computer;

        return $this;
    }

    /**
     * Get computer
     *
     * @return string 
     */
    public function getComputer()
    {
        return $this->computer;
    }

    /**
     * Set monitor
     *
     * @param string $monitor
     * 
     * @return GlpiProfiles
     */
    public function setMonitor($monitor)
    {
        $this->monitor = $monitor;

        return $this;
    }

    /**
     * Get monitor
     *
     * @return string 
     */
    public function getMonitor()
    {
        return $this->monitor;
    }

    /**
     * Set software
     *
     * @param string $software
     * 
     * @return GlpiProfiles
     */
    public function setSoftware($software)
    {
        $this->software = $software;

        return $this;
    }

    /**
     * Get software
     *
     * @return string 
     */
    public function getSoftware()
    {
        return $this->software;
    }

    /**
     * Set networking
     *
     * @param string $networking
     * 
     * @return GlpiProfiles
     */
    public function setNetworking($networking)
    {
        $this->networking = $networking;

        return $this;
    }

    /**
     * Get networking
     *
     * @return string 
     */
    public function getNetworking()
    {
        return $this->networking;
    }

    /**
     * Set printer
     *
     * @param string $printer
     * 
     * @return GlpiProfiles
     */
    public function setPrinter($printer)
    {
        $this->printer = $printer;

        return $this;
    }

    /**
     * Get printer
     *
     * @return string 
     */
    public function getPrinter()
    {
        return $this->printer;
    }

    /**
     * Set peripheral
     *
     * @param string $peripheral
     * 
     * @return GlpiProfiles
     */
    public function setPeripheral($peripheral)
    {
        $this->peripheral = $peripheral;

        return $this;
    }

    /**
     * Get peripheral
     *
     * @return string 
     */
    public function getPeripheral()
    {
        return $this->peripheral;
    }

    /**
     * Set cartridge
     *
     * @param string $cartridge
     * 
     * @return GlpiProfiles
     */
    public function setCartridge($cartridge)
    {
        $this->cartridge = $cartridge;

        return $this;
    }

    /**
     * Get cartridge
     *
     * @return string 
     */
    public function getCartridge()
    {
        return $this->cartridge;
    }

    /**
     * Set consumable
     *
     * @param string $consumable
     * 
     * @return GlpiProfiles
     */
    public function setConsumable($consumable)
    {
        $this->consumable = $consumable;

        return $this;
    }

    /**
     * Get consumable
     *
     * @return string 
     */
    public function getConsumable()
    {
        return $this->consumable;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * 
     * @return GlpiProfiles
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * 
     * @return GlpiProfiles
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set contactEnterprise
     *
     * @param string $contactEnterprise
     * 
     * @return GlpiProfiles
     */
    public function setContactEnterprise($contactEnterprise)
    {
        $this->contactEnterprise = $contactEnterprise;

        return $this;
    }

    /**
     * Get contactEnterprise
     *
     * @return string 
     */
    public function getContactEnterprise()
    {
        return $this->contactEnterprise;
    }

    /**
     * Set document
     *
     * @param string $document
     * 
     * @return GlpiProfiles
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return string 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set contract
     *
     * @param string $contract
     * 
     * @return GlpiProfiles
     */
    public function setContract($contract)
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * Get contract
     *
     * @return string 
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * Set infocom
     *
     * @param string $infocom
     * 
     * @return GlpiProfiles
     */
    public function setInfocom($infocom)
    {
        $this->infocom = $infocom;

        return $this;
    }

    /**
     * Get infocom
     *
     * @return string 
     */
    public function getInfocom()
    {
        return $this->infocom;
    }

    /**
     * Set knowbase
     *
     * @param string $knowbase
     * 
     * @return GlpiProfiles
     */
    public function setKnowbase($knowbase)
    {
        $this->knowbase = $knowbase;

        return $this;
    }

    /**
     * Get knowbase
     *
     * @return string 
     */
    public function getKnowbase()
    {
        return $this->knowbase;
    }

    /**
     * Set knowbaseAdmin
     *
     * @param string $knowbaseAdmin
     * 
     * @return GlpiProfiles
     */
    public function setKnowbaseAdmin($knowbaseAdmin)
    {
        $this->knowbaseAdmin = $knowbaseAdmin;

        return $this;
    }

    /**
     * Get knowbaseAdmin
     *
     * @return string 
     */
    public function getKnowbaseAdmin()
    {
        return $this->knowbaseAdmin;
    }

    /**
     * Set faq
     *
     * @param string $faq
     * 
     * @return GlpiProfiles
     */
    public function setFaq($faq)
    {
        $this->faq = $faq;

        return $this;
    }

    /**
     * Get faq
     *
     * @return string 
     */
    public function getFaq()
    {
        return $this->faq;
    }

    /**
     * Set reservationHelpdesk
     *
     * @param string $reservationHelpdesk
     * 
     * @return GlpiProfiles
     */
    public function setReservationHelpdesk($reservationHelpdesk)
    {
        $this->reservationHelpdesk = $reservationHelpdesk;

        return $this;
    }

    /**
     * Get reservationHelpdesk
     *
     * @return string 
     */
    public function getReservationHelpdesk()
    {
        return $this->reservationHelpdesk;
    }

    /**
     * Set reservationCentral
     *
     * @param string $reservationCentral
     * 
     * @return GlpiProfiles
     */
    public function setReservationCentral($reservationCentral)
    {
        $this->reservationCentral = $reservationCentral;

        return $this;
    }

    /**
     * Get reservationCentral
     *
     * @return string 
     */
    public function getReservationCentral()
    {
        return $this->reservationCentral;
    }

    /**
     * Set reports
     *
     * @param string $reports
     * 
     * @return GlpiProfiles
     */
    public function setReports($reports)
    {
        $this->reports = $reports;

        return $this;
    }

    /**
     * Get reports
     *
     * @return string 
     */
    public function getReports()
    {
        return $this->reports;
    }

    /**
     * Set ocsng
     *
     * @param string $ocsng
     * 
     * @return GlpiProfiles
     */
    public function setOcsng($ocsng)
    {
        $this->ocsng = $ocsng;

        return $this;
    }

    /**
     * Get ocsng
     *
     * @return string 
     */
    public function getOcsng()
    {
        return $this->ocsng;
    }

    /**
     * Set viewOcsng
     *
     * @param string $viewOcsng
     * 
     * @return GlpiProfiles
     */
    public function setViewOcsng($viewOcsng)
    {
        $this->viewOcsng = $viewOcsng;

        return $this;
    }

    /**
     * Get viewOcsng
     *
     * @return string 
     */
    public function getViewOcsng()
    {
        return $this->viewOcsng;
    }

    /**
     * Set syncOcsng
     *
     * @param string $syncOcsng
     * 
     * @return GlpiProfiles
     */
    public function setSyncOcsng($syncOcsng)
    {
        $this->syncOcsng = $syncOcsng;

        return $this;
    }

    /**
     * Get syncOcsng
     *
     * @return string 
     */
    public function getSyncOcsng()
    {
        return $this->syncOcsng;
    }

    /**
     * Set dropdown
     *
     * @param string $dropdown
     * 
     * @return GlpiProfiles
     */
    public function setDropdown($dropdown)
    {
        $this->dropdown = $dropdown;

        return $this;
    }

    /**
     * Get dropdown
     *
     * @return string 
     */
    public function getDropdown()
    {
        return $this->dropdown;
    }

    /**
     * Set entityDropdown
     *
     * @param string $entityDropdown
     * 
     * @return GlpiProfiles
     */
    public function setEntityDropdown($entityDropdown)
    {
        $this->entityDropdown = $entityDropdown;

        return $this;
    }

    /**
     * Get entityDropdown
     *
     * @return string 
     */
    public function getEntityDropdown()
    {
        return $this->entityDropdown;
    }

    /**
     * Set device
     *
     * @param string $device
     * 
     * @return GlpiProfiles
     */
    public function setDevice($device)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return string 
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set typedoc
     *
     * @param string $typedoc
     * 
     * @return GlpiProfiles
     */
    public function setTypedoc($typedoc)
    {
        $this->typedoc = $typedoc;

        return $this;
    }

    /**
     * Get typedoc
     *
     * @return string 
     */
    public function getTypedoc()
    {
        return $this->typedoc;
    }

    /**
     * Set link
     *
     * @param string $link
     * 
     * @return GlpiProfiles
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set config
     *
     * @param string $config
     * 
     * @return GlpiProfiles
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Get config
     *
     * @return string 
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set ruleTicket
     *
     * @param string $ruleTicket
     * 
     * @return GlpiProfiles
     */
    public function setRuleTicket($ruleTicket)
    {
        $this->ruleTicket = $ruleTicket;

        return $this;
    }

    /**
     * Get ruleTicket
     *
     * @return string 
     */
    public function getRuleTicket()
    {
        return $this->ruleTicket;
    }

    /**
     * Set entityRuleTicket
     *
     * @param string $entityRuleTicket
     * 
     * @return GlpiProfiles
     */
    public function setEntityRuleTicket($entityRuleTicket)
    {
        $this->entityRuleTicket = $entityRuleTicket;

        return $this;
    }

    /**
     * Get entityRuleTicket
     *
     * @return string 
     */
    public function getEntityRuleTicket()
    {
        return $this->entityRuleTicket;
    }

    /**
     * Set ruleOcs
     *
     * @param string $ruleOcs
     * 
     * @return GlpiProfiles
     */
    public function setRuleOcs($ruleOcs)
    {
        $this->ruleOcs = $ruleOcs;

        return $this;
    }

    /**
     * Get ruleOcs
     *
     * @return string 
     */
    public function getRuleOcs()
    {
        return $this->ruleOcs;
    }

    /**
     * Set ruleLdap
     *
     * @param string $ruleLdap
     * 
     * @return GlpiProfiles
     */
    public function setRuleLdap($ruleLdap)
    {
        $this->ruleLdap = $ruleLdap;

        return $this;
    }

    /**
     * Get ruleLdap
     *
     * @return string 
     */
    public function getRuleLdap()
    {
        return $this->ruleLdap;
    }

    /**
     * Set ruleSoftwarecategories
     *
     * @param string $ruleSoftwarecategories
     * 
     * @return GlpiProfiles
     */
    public function setRuleSoftwarecategories($ruleSoftwarecategories)
    {
        $this->ruleSoftwarecategories = $ruleSoftwarecategories;

        return $this;
    }

    /**
     * Get ruleSoftwarecategories
     *
     * @return string 
     */
    public function getRuleSoftwarecategories()
    {
        return $this->ruleSoftwarecategories;
    }

    /**
     * Set searchConfig
     *
     * @param string $searchConfig
     * 
     * @return GlpiProfiles
     */
    public function setSearchConfig($searchConfig)
    {
        $this->searchConfig = $searchConfig;

        return $this;
    }

    /**
     * Get searchConfig
     *
     * @return string 
     */
    public function getSearchConfig()
    {
        return $this->searchConfig;
    }

    /**
     * Set searchConfigGlobal
     *
     * @param string $searchConfigGlobal
     * 
     * @return GlpiProfiles
     */
    public function setSearchConfigGlobal($searchConfigGlobal)
    {
        $this->searchConfigGlobal = $searchConfigGlobal;

        return $this;
    }

    /**
     * Get searchConfigGlobal
     *
     * @return string 
     */
    public function getSearchConfigGlobal()
    {
        return $this->searchConfigGlobal;
    }

    /**
     * Set checkUpdate
     *
     * @param string $checkUpdate
     * 
     * @return GlpiProfiles
     */
    public function setCheckUpdate($checkUpdate)
    {
        $this->checkUpdate = $checkUpdate;

        return $this;
    }

    /**
     * Get checkUpdate
     *
     * @return string 
     */
    public function getCheckUpdate()
    {
        return $this->checkUpdate;
    }

    /**
     * Set profile
     *
     * @param string $profile
     * 
     * @return GlpiProfiles
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return string 
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set user
     *
     * @param string $user
     * 
     * @return GlpiProfiles
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userAuthtype
     *
     * @param string $userAuthtype
     * 
     * @return GlpiProfiles
     */
    public function setUserAuthtype($userAuthtype)
    {
        $this->userAuthtype = $userAuthtype;

        return $this;
    }

    /**
     * Get userAuthtype
     *
     * @return string 
     */
    public function getUserAuthtype()
    {
        return $this->userAuthtype;
    }

    /**
     * Set group
     *
     * @param string $group
     * 
     * @return GlpiProfiles
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return string 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set entity
     *
     * @param string $entity
     * 
     * @return GlpiProfiles
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return string 
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set transfer
     *
     * @param string $transfer
     * 
     * @return GlpiProfiles
     */
    public function setTransfer($transfer)
    {
        $this->transfer = $transfer;

        return $this;
    }

    /**
     * Get transfer
     *
     * @return string 
     */
    public function getTransfer()
    {
        return $this->transfer;
    }

    /**
     * Set logs
     *
     * @param string $logs
     * 
     * @return GlpiProfiles
     */
    public function setLogs($logs)
    {
        $this->logs = $logs;

        return $this;
    }

    /**
     * Get logs
     *
     * @return string 
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * Set reminderPublic
     *
     * @param string $reminderPublic
     * 
     * @return GlpiProfiles
     */
    public function setReminderPublic($reminderPublic)
    {
        $this->reminderPublic = $reminderPublic;

        return $this;
    }

    /**
     * Get reminderPublic
     *
     * @return string 
     */
    public function getReminderPublic()
    {
        return $this->reminderPublic;
    }

    /**
     * Set bookmarkPublic
     *
     * @param string $bookmarkPublic
     * 
     * @return GlpiProfiles
     */
    public function setBookmarkPublic($bookmarkPublic)
    {
        $this->bookmarkPublic = $bookmarkPublic;

        return $this;
    }

    /**
     * Get bookmarkPublic
     *
     * @return string 
     */
    public function getBookmarkPublic()
    {
        return $this->bookmarkPublic;
    }

    /**
     * Set backup
     *
     * @param string $backup
     * 
     * @return GlpiProfiles
     */
    public function setBackup($backup)
    {
        $this->backup = $backup;

        return $this;
    }

    /**
     * Get backup
     *
     * @return string 
     */
    public function getBackup()
    {
        return $this->backup;
    }

    /**
     * Set createTicket
     *
     * @param string $createTicket
     * 
     * @return GlpiProfiles
     */
    public function setCreateTicket($createTicket)
    {
        $this->createTicket = $createTicket;

        return $this;
    }

    /**
     * Get createTicket
     *
     * @return string 
     */
    public function getCreateTicket()
    {
        return $this->createTicket;
    }

    /**
     * Set deleteTicket
     *
     * @param string $deleteTicket
     * 
     * @return GlpiProfiles
     */
    public function setDeleteTicket($deleteTicket)
    {
        $this->deleteTicket = $deleteTicket;

        return $this;
    }

    /**
     * Get deleteTicket
     *
     * @return string 
     */
    public function getDeleteTicket()
    {
        return $this->deleteTicket;
    }

    /**
     * Set addFollowups
     *
     * @param string $addFollowups
     * 
     * @return GlpiProfiles
     */
    public function setAddFollowups($addFollowups)
    {
        $this->addFollowups = $addFollowups;

        return $this;
    }

    /**
     * Get addFollowups
     *
     * @return string 
     */
    public function getAddFollowups()
    {
        return $this->addFollowups;
    }

    /**
     * Set groupAddFollowups
     *
     * @param string $groupAddFollowups
     * 
     * @return GlpiProfiles
     */
    public function setGroupAddFollowups($groupAddFollowups)
    {
        $this->groupAddFollowups = $groupAddFollowups;

        return $this;
    }

    /**
     * Get groupAddFollowups
     *
     * @return string 
     */
    public function getGroupAddFollowups()
    {
        return $this->groupAddFollowups;
    }

    /**
     * Set globalAddFollowups
     *
     * @param string $globalAddFollowups
     * 
     * @return GlpiProfiles
     */
    public function setGlobalAddFollowups($globalAddFollowups)
    {
        $this->globalAddFollowups = $globalAddFollowups;

        return $this;
    }

    /**
     * Get globalAddFollowups
     *
     * @return string 
     */
    public function getGlobalAddFollowups()
    {
        return $this->globalAddFollowups;
    }

    /**
     * Set globalAddTasks
     *
     * @param string $globalAddTasks
     * 
     * @return GlpiProfiles
     */
    public function setGlobalAddTasks($globalAddTasks)
    {
        $this->globalAddTasks = $globalAddTasks;

        return $this;
    }

    /**
     * Get globalAddTasks
     *
     * @return string 
     */
    public function getGlobalAddTasks()
    {
        return $this->globalAddTasks;
    }

    /**
     * Set updateTicket
     *
     * @param string $updateTicket
     * 
     * @return GlpiProfiles
     */
    public function setUpdateTicket($updateTicket)
    {
        $this->updateTicket = $updateTicket;

        return $this;
    }

    /**
     * Get updateTicket
     *
     * @return string 
     */
    public function getUpdateTicket()
    {
        return $this->updateTicket;
    }

    /**
     * Set updatePriority
     *
     * @param string $updatePriority
     * 
     * @return GlpiProfiles
     */
    public function setUpdatePriority($updatePriority)
    {
        $this->updatePriority = $updatePriority;

        return $this;
    }

    /**
     * Get updatePriority
     *
     * @return string 
     */
    public function getUpdatePriority()
    {
        return $this->updatePriority;
    }

    /**
     * Set ownTicket
     *
     * @param string $ownTicket
     * 
     * @return GlpiProfiles
     */
    public function setOwnTicket($ownTicket)
    {
        $this->ownTicket = $ownTicket;

        return $this;
    }

    /**
     * Get ownTicket
     *
     * @return string 
     */
    public function getOwnTicket()
    {
        return $this->ownTicket;
    }

    /**
     * Set stealTicket
     *
     * @param string $stealTicket
     * 
     * @return GlpiProfiles
     */
    public function setStealTicket($stealTicket)
    {
        $this->stealTicket = $stealTicket;

        return $this;
    }

    /**
     * Get stealTicket
     *
     * @return string 
     */
    public function getStealTicket()
    {
        return $this->stealTicket;
    }

    /**
     * Set assignTicket
     *
     * @param string $assignTicket
     * 
     * @return GlpiProfiles
     */
    public function setAssignTicket($assignTicket)
    {
        $this->assignTicket = $assignTicket;

        return $this;
    }

    /**
     * Get assignTicket
     *
     * @return string 
     */
    public function getAssignTicket()
    {
        return $this->assignTicket;
    }

    /**
     * Set showAllTicket
     *
     * @param string $showAllTicket
     * 
     * @return GlpiProfiles
     */
    public function setShowAllTicket($showAllTicket)
    {
        $this->showAllTicket = $showAllTicket;

        return $this;
    }

    /**
     * Get showAllTicket
     *
     * @return string 
     */
    public function getShowAllTicket()
    {
        return $this->showAllTicket;
    }

    /**
     * Set showAssignTicket
     *
     * @param string $showAssignTicket
     * 
     * @return GlpiProfiles
     */
    public function setShowAssignTicket($showAssignTicket)
    {
        $this->showAssignTicket = $showAssignTicket;

        return $this;
    }

    /**
     * Get showAssignTicket
     *
     * @return string 
     */
    public function getShowAssignTicket()
    {
        return $this->showAssignTicket;
    }

    /**
     * Set showFullTicket
     *
     * @param string $showFullTicket
     * 
     * @return GlpiProfiles
     */
    public function setShowFullTicket($showFullTicket)
    {
        $this->showFullTicket = $showFullTicket;

        return $this;
    }

    /**
     * Get showFullTicket
     *
     * @return string 
     */
    public function getShowFullTicket()
    {
        return $this->showFullTicket;
    }

    /**
     * Set observeTicket
     *
     * @param string $observeTicket
     * 
     * @return GlpiProfiles
     */
    public function setObserveTicket($observeTicket)
    {
        $this->observeTicket = $observeTicket;

        return $this;
    }

    /**
     * Get observeTicket
     *
     * @return string 
     */
    public function getObserveTicket()
    {
        return $this->observeTicket;
    }

    /**
     * Set updateFollowups
     *
     * @param string $updateFollowups
     * 
     * @return GlpiProfiles
     */
    public function setUpdateFollowups($updateFollowups)
    {
        $this->updateFollowups = $updateFollowups;

        return $this;
    }

    /**
     * Get updateFollowups
     *
     * @return string 
     */
    public function getUpdateFollowups()
    {
        return $this->updateFollowups;
    }

    /**
     * Set updateTasks
     *
     * @param string $updateTasks
     * 
     * @return GlpiProfiles
     */
    public function setUpdateTasks($updateTasks)
    {
        $this->updateTasks = $updateTasks;

        return $this;
    }

    /**
     * Get updateTasks
     *
     * @return string 
     */
    public function getUpdateTasks()
    {
        return $this->updateTasks;
    }

    /**
     * Set showPlanning
     *
     * @param string $showPlanning
     * 
     * @return GlpiProfiles
     */
    public function setShowPlanning($showPlanning)
    {
        $this->showPlanning = $showPlanning;

        return $this;
    }

    /**
     * Get showPlanning
     *
     * @return string 
     */
    public function getShowPlanning()
    {
        return $this->showPlanning;
    }

    /**
     * Set showGroupPlanning
     *
     * @param string $showGroupPlanning
     * 
     * @return GlpiProfiles
     */
    public function setShowGroupPlanning($showGroupPlanning)
    {
        $this->showGroupPlanning = $showGroupPlanning;

        return $this;
    }

    /**
     * Get showGroupPlanning
     *
     * @return string 
     */
    public function getShowGroupPlanning()
    {
        return $this->showGroupPlanning;
    }

    /**
     * Set showAllPlanning
     *
     * @param string $showAllPlanning
     * 
     * @return GlpiProfiles
     */
    public function setShowAllPlanning($showAllPlanning)
    {
        $this->showAllPlanning = $showAllPlanning;

        return $this;
    }

    /**
     * Get showAllPlanning
     *
     * @return string 
     */
    public function getShowAllPlanning()
    {
        return $this->showAllPlanning;
    }

    /**
     * Set statistic
     *
     * @param string $statistic
     * 
     * @return GlpiProfiles
     */
    public function setStatistic($statistic)
    {
        $this->statistic = $statistic;

        return $this;
    }

    /**
     * Get statistic
     *
     * @return string 
     */
    public function getStatistic()
    {
        return $this->statistic;
    }

    /**
     * Set passwordUpdate
     *
     * @param string $passwordUpdate
     * 
     * @return GlpiProfiles
     */
    public function setPasswordUpdate($passwordUpdate)
    {
        $this->passwordUpdate = $passwordUpdate;

        return $this;
    }

    /**
     * Get passwordUpdate
     *
     * @return string 
     */
    public function getPasswordUpdate()
    {
        return $this->passwordUpdate;
    }

    /**
     * Set helpdeskHardware
     *
     * @param integer $helpdeskHardware
     * 
     * @return GlpiProfiles
     */
    public function setHelpdeskHardware($helpdeskHardware)
    {
        $this->helpdeskHardware = $helpdeskHardware;

        return $this;
    }

    /**
     * Get helpdeskHardware
     *
     * @return integer 
     */
    public function getHelpdeskHardware()
    {
        return $this->helpdeskHardware;
    }

    /**
     * Set helpdeskItemType
     *
     * @param string $helpdeskItemType
     * 
     * @return GlpiProfiles
     */
    public function setHelpdeskItemType($helpdeskItemType)
    {
        $this->helpdeskItemType = $helpdeskItemType;

        return $this;
    }

    /**
     * Get helpdeskItemType
     *
     * @return string 
     */
    public function getHelpdeskItemType()
    {
        return $this->helpdeskItemType;
    }

    /**
     * Set ticketStatus
     *
     * @param string $ticketStatus
     * 
     * @return GlpiProfiles
     */
    public function setTicketStatus($ticketStatus)
    {
        $this->ticketStatus = $ticketStatus;

        return $this;
    }

    /**
     * Get ticketStatus
     *
     * @return string 
     */
    public function getTicketStatus()
    {
        return $this->ticketStatus;
    }

    /**
     * Set showGroupTicket
     *
     * @param string $showGroupTicket
     * 
     * @return GlpiProfiles
     */
    public function setShowGroupTicket($showGroupTicket)
    {
        $this->showGroupTicket = $showGroupTicket;

        return $this;
    }

    /**
     * Get showGroupTicket
     *
     * @return string 
     */
    public function getShowGroupTicket()
    {
        return $this->showGroupTicket;
    }

    /**
     * Set showGroupHardware
     *
     * @param string $showGroupHardware
     * 
     * @return GlpiProfiles
     */
    public function setShowGroupHardware($showGroupHardware)
    {
        $this->showGroupHardware = $showGroupHardware;

        return $this;
    }

    /**
     * Get showGroupHardware
     *
     * @return string 
     */
    public function getShowGroupHardware()
    {
        return $this->showGroupHardware;
    }

    /**
     * Set ruleDictionnarySoftware
     *
     * @param string $ruleDictionnarySoftware
     * 
     * @return GlpiProfiles
     */
    public function setRuleDictionnarySoftware($ruleDictionnarySoftware)
    {
        $this->ruleDictionnarySoftware = $ruleDictionnarySoftware;

        return $this;
    }

    /**
     * Get ruleDictionnarySoftware
     *
     * @return string 
     */
    public function getRuleDictionnarySoftware()
    {
        return $this->ruleDictionnarySoftware;
    }

    /**
     * Set ruleDictionnaryDropdown
     *
     * @param string $ruleDictionnaryDropdown
     * 
     * @return GlpiProfiles
     */
    public function setRuleDictionnaryDropdown($ruleDictionnaryDropdown)
    {
        $this->ruleDictionnaryDropdown = $ruleDictionnaryDropdown;

        return $this;
    }

    /**
     * Get ruleDictionnaryDropdown
     *
     * @return string 
     */
    public function getRuleDictionnaryDropdown()
    {
        return $this->ruleDictionnaryDropdown;
    }

    /**
     * Set budget
     *
     * @param string $budget
     * 
     * @return GlpiProfiles
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return string 
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set importExternalauthUsers
     *
     * @param string $importExternalauthUsers
     * 
     * @return GlpiProfiles
     */
    public function setImportExternalauthUsers($importExternalauthUsers)
    {
        $this->importExternalauthUsers = $importExternalauthUsers;

        return $this;
    }

    /**
     * Get importExternalauthUsers
     *
     * @return string 
     */
    public function getImportExternalauthUsers()
    {
        return $this->importExternalauthUsers;
    }

    /**
     * Set notification
     *
     * @param string $notification
     * 
     * @return GlpiProfiles
     */
    public function setNotification($notification)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * Get notification
     *
     * @return string 
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Set ruleMailcollector
     *
     * @param string $ruleMailcollector
     * 
     * @return GlpiProfiles
     */
    public function setRuleMailcollector($ruleMailcollector)
    {
        $this->ruleMailcollector = $ruleMailcollector;

        return $this;
    }

    /**
     * Get ruleMailcollector
     *
     * @return string 
     */
    public function getRuleMailcollector()
    {
        return $this->ruleMailcollector;
    }

    /**
     * Set dateMod
     *
     * @param \DateTime $dateMod
     * 
     * @return GlpiProfiles
     */
    public function setDateMod($dateMod)
    {
        $this->dateMod = $dateMod;

        return $this;
    }

    /**
     * Get dateMod
     *
     * @return \DateTime 
     */
    public function getDateMod()
    {
        return $this->dateMod;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiProfiles
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set validateTicket
     *
     * @param string $validateTicket
     * 
     * @return GlpiProfiles
     */
    public function setValidateTicket($validateTicket)
    {
        $this->validateTicket = $validateTicket;

        return $this;
    }

    /**
     * Get validateTicket
     *
     * @return string 
     */
    public function getValidateTicket()
    {
        return $this->validateTicket;
    }

    /**
     * Set createValidation
     *
     * @param string $createValidation
     * 
     * @return GlpiProfiles
     */
    public function setCreateValidation($createValidation)
    {
        $this->createValidation = $createValidation;

        return $this;
    }

    /**
     * Get createValidation
     *
     * @return string 
     */
    public function getCreateValidation()
    {
        return $this->createValidation;
    }

    /**
     * Set calendar
     *
     * @param string $calendar
     * 
     * @return GlpiProfiles
     */
    public function setCalendar($calendar)
    {
        $this->calendar = $calendar;

        return $this;
    }

    /**
     * Get calendar
     *
     * @return string 
     */
    public function getCalendar()
    {
        return $this->calendar;
    }

    /**
     * Set sla
     *
     * @param string $sla
     * 
     * @return GlpiProfiles
     */
    public function setSla($sla)
    {
        $this->sla = $sla;

        return $this;
    }

    /**
     * Get sla
     *
     * @return string 
     */
    public function getSla()
    {
        return $this->sla;
    }

    /**
     * Set ruleDictionnaryPrinter
     *
     * @param string $ruleDictionnaryPrinter
     * 
     * @return GlpiProfiles
     */
    public function setRuleDictionnaryPrinter($ruleDictionnaryPrinter)
    {
        $this->ruleDictionnaryPrinter = $ruleDictionnaryPrinter;

        return $this;
    }

    /**
     * Get ruleDictionnaryPrinter
     *
     * @return string 
     */
    public function getRuleDictionnaryPrinter()
    {
        return $this->ruleDictionnaryPrinter;
    }

    /**
     * Set cleanOcsng
     *
     * @param string $cleanOcsng
     * 
     * @return GlpiProfiles
     */
    public function setCleanOcsng($cleanOcsng)
    {
        $this->cleanOcsng = $cleanOcsng;

        return $this;
    }

    /**
     * Get cleanOcsng
     *
     * @return string 
     */
    public function getCleanOcsng()
    {
        return $this->cleanOcsng;
    }

    /**
     * Set updateOwnFollowups
     *
     * @param string $updateOwnFollowups
     * 
     * @return GlpiProfiles
     */
    public function setUpdateOwnFollowups($updateOwnFollowups)
    {
        $this->updateOwnFollowups = $updateOwnFollowups;

        return $this;
    }

    /**
     * Get updateOwnFollowups
     *
     * @return string 
     */
    public function getUpdateOwnFollowups()
    {
        return $this->updateOwnFollowups;
    }

    /**
     * Set deleteFollowups
     *
     * @param string $deleteFollowups
     * 
     * @return GlpiProfiles
     */
    public function setDeleteFollowups($deleteFollowups)
    {
        $this->deleteFollowups = $deleteFollowups;

        return $this;
    }

    /**
     * Get deleteFollowups
     *
     * @return string 
     */
    public function getDeleteFollowups()
    {
        return $this->deleteFollowups;
    }

    /**
     * Set entityHelpdesk
     *
     * @param string $entityHelpdesk
     * 
     * @return GlpiProfiles
     */
    public function setEntityHelpdesk($entityHelpdesk)
    {
        $this->entityHelpdesk = $entityHelpdesk;

        return $this;
    }

    /**
     * Get entityHelpdesk
     *
     * @return string 
     */
    public function getEntityHelpdesk()
    {
        return $this->entityHelpdesk;
    }

    /**
     * Set showMyProblem
     *
     * @param string $showMyProblem
     * 
     * @return GlpiProfiles
     */
    public function setShowMyProblem($showMyProblem)
    {
        $this->showMyProblem = $showMyProblem;

        return $this;
    }

    /**
     * Get showMyProblem
     *
     * @return string 
     */
    public function getShowMyProblem()
    {
        return $this->showMyProblem;
    }

    /**
     * Set showAllProblem
     *
     * @param string $showAllProblem
     * 
     * @return GlpiProfiles
     */
    public function setShowAllProblem($showAllProblem)
    {
        $this->showAllProblem = $showAllProblem;

        return $this;
    }

    /**
     * Get showAllProblem
     *
     * @return string 
     */
    public function getShowAllProblem()
    {
        return $this->showAllProblem;
    }

    /**
     * Set editAllProblem
     *
     * @param string $editAllProblem
     * 
     * @return GlpiProfiles
     */
    public function setEditAllProblem($editAllProblem)
    {
        $this->editAllProblem = $editAllProblem;

        return $this;
    }

    /**
     * Get editAllProblem
     *
     * @return string 
     */
    public function getEditAllProblem()
    {
        return $this->editAllProblem;
    }

    /**
     * Set problemStatus
     *
     * @param string $problemStatus
     * 
     * @return GlpiProfiles
     */
    public function setProblemStatus($problemStatus)
    {
        $this->problemStatus = $problemStatus;

        return $this;
    }

    /**
     * Get problemStatus
     *
     * @return string 
     */
    public function getProblemStatus()
    {
        return $this->problemStatus;
    }

    /**
     * Set createTicketOnLogin
     *
     * @param boolean $createTicketOnLogin
     * 
     * @return GlpiProfiles
     */
    public function setCreateTicketOnLogin($createTicketOnLogin)
    {
        $this->createTicketOnLogin = $createTicketOnLogin;

        return $this;
    }

    /**
     * Get createTicketOnLogin
     *
     * @return boolean 
     */
    public function getCreateTicketOnLogin()
    {
        return $this->createTicketOnLogin;
    }

    /**
     * Set tickettemplate
     *
     * @param string $tickettemplate
     * 
     * @return GlpiProfiles
     */
    public function setTickettemplate($tickettemplate)
    {
        $this->tickettemplate = $tickettemplate;

        return $this;
    }

    /**
     * Get tickettemplate
     *
     * @return string 
     */
    public function getTickettemplate()
    {
        return $this->tickettemplate;
    }

    /**
     * Set ticketrecurrent
     *
     * @param string $ticketrecurrent
     * 
     * @return GlpiProfiles
     */
    public function setTicketrecurrent($ticketrecurrent)
    {
        $this->ticketrecurrent = $ticketrecurrent;

        return $this;
    }

    /**
     * Get ticketrecurrent
     *
     * @return string 
     */
    public function getTicketrecurrent()
    {
        return $this->ticketrecurrent;
    }
}