<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiSoftwares
 *
 * @ORM\Table(name="glpi_softwares")
 * @ORM\Entity
 */
class GlpiSoftwares
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="locations_id", type="integer", nullable=false)
     */
    private $locationsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id_tech", type="integer", nullable=false)
     */
    private $usersIdTech;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id_tech", type="integer", nullable=false)
     */
    private $groupsIdTech;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_update", type="boolean", nullable=false)
     */
    private $isUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwares_id", type="integer", nullable=false)
     */
    private $softwaresId;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_template", type="boolean", nullable=false)
     */
    private $isTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="template_name", type="string", length=255, nullable=true)
     */
    private $templateName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var string
     *
     * @ORM\Column(name="notepad", type="text", nullable=true)
     */
    private $notepad;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id", type="integer", nullable=false)
     */
    private $groupsId;

    /**
     * @var float
     *
     * @ORM\Column(name="ticket_tco", type="decimal", nullable=true)
     */
    private $ticketTco;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_helpdesk_visible", type="boolean", nullable=false)
     */
    private $isHelpdeskVisible;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwarecategories_id", type="integer", nullable=false)
     */
    private $softwarecategoriesId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiSoftwares
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set isRecursive
     *
     * @param boolean $isRecursive
     * 
     * @return GlpiSoftwares
     */
    public function setIsRecursive($isRecursive)
    {
        $this->isRecursive = $isRecursive;

        return $this;
    }

    /**
     * Get isRecursive
     *
     * @return boolean 
     */
    public function getIsRecursive()
    {
        return $this->isRecursive;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiSoftwares
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiSoftwares
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set locationsId
     *
     * @param integer $locationsId
     * 
     * @return GlpiSoftwares
     */
    public function setLocationsId($locationsId)
    {
        $this->locationsId = $locationsId;

        return $this;
    }

    /**
     * Get locationsId
     *
     * @return integer 
     */
    public function getLocationsId()
    {
        return $this->locationsId;
    }

    /**
     * Set usersIdTech
     *
     * @param integer $usersIdTech
     * 
     * @return GlpiSoftwares
     */
    public function setUsersIdTech($usersIdTech)
    {
        $this->usersIdTech = $usersIdTech;

        return $this;
    }

    /**
     * Get usersIdTech
     *
     * @return integer 
     */
    public function getUsersIdTech()
    {
        return $this->usersIdTech;
    }

    /**
     * Set groupsIdTech
     *
     * @param integer $groupsIdTech
     * 
     * @return GlpiSoftwares
     */
    public function setGroupsIdTech($groupsIdTech)
    {
        $this->groupsIdTech = $groupsIdTech;

        return $this;
    }

    /**
     * Get groupsIdTech
     *
     * @return integer 
     */
    public function getGroupsIdTech()
    {
        return $this->groupsIdTech;
    }

    /**
     * Set isUpdate
     *
     * @param boolean $isUpdate
     * 
     * @return GlpiSoftwares
     */
    public function setIsUpdate($isUpdate)
    {
        $this->isUpdate = $isUpdate;

        return $this;
    }

    /**
     * Get isUpdate
     *
     * @return boolean 
     */
    public function getIsUpdate()
    {
        return $this->isUpdate;
    }

    /**
     * Set softwaresId
     *
     * @param integer $softwaresId
     * 
     * @return GlpiSoftwares
     */
    public function setSoftwaresId($softwaresId)
    {
        $this->softwaresId = $softwaresId;

        return $this;
    }

    /**
     * Get softwaresId
     *
     * @return integer 
     */
    public function getSoftwaresId()
    {
        return $this->softwaresId;
    }

    /**
     * Set manufacturersId
     *
     * @param integer $manufacturersId
     * 
     * @return GlpiSoftwares
     */
    public function setManufacturersId($manufacturersId)
    {
        $this->manufacturersId = $manufacturersId;

        return $this;
    }

    /**
     * Get manufacturersId
     *
     * @return integer 
     */
    public function getManufacturersId()
    {
        return $this->manufacturersId;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * 
     * @return GlpiSoftwares
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set isTemplate
     *
     * @param boolean $isTemplate
     * 
     * @return GlpiSoftwares
     */
    public function setIsTemplate($isTemplate)
    {
        $this->isTemplate = $isTemplate;

        return $this;
    }

    /**
     * Get isTemplate
     *
     * @return boolean 
     */
    public function getIsTemplate()
    {
        return $this->isTemplate;
    }

    /**
     * Set templateName
     *
     * @param string $templateName
     * 
     * @return GlpiSoftwares
     */
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;

        return $this;
    }

    /**
     * Get templateName
     *
     * @return string 
     */
    public function getTemplateName()
    {
        return $this->templateName;
    }

    /**
     * Set dateMod
     *
     * @param \DateTime $dateMod
     * 
     * @return GlpiSoftwares
     */
    public function setDateMod($dateMod)
    {
        $this->dateMod = $dateMod;

        return $this;
    }

    /**
     * Get dateMod
     *
     * @return \DateTime 
     */
    public function getDateMod()
    {
        return $this->dateMod;
    }

    /**
     * Set notepad
     *
     * @param string $notepad
     * 
     * @return GlpiSoftwares
     */
    public function setNotepad($notepad)
    {
        $this->notepad = $notepad;

        return $this;
    }

    /**
     * Get notepad
     *
     * @return string 
     */
    public function getNotepad()
    {
        return $this->notepad;
    }

    /**
     * Set usersId
     *
     * @param integer $usersId
     * 
     * @return GlpiSoftwares
     */
    public function setUsersId($usersId)
    {
        $this->usersId = $usersId;

        return $this;
    }

    /**
     * Get usersId
     *
     * @return integer 
     */
    public function getUsersId()
    {
        return $this->usersId;
    }

    /**
     * Set groupsId
     *
     * @param integer $groupsId
     * 
     * @return GlpiSoftwares
     */
    public function setGroupsId($groupsId)
    {
        $this->groupsId = $groupsId;

        return $this;
    }

    /**
     * Get groupsId
     *
     * @return integer 
     */
    public function getGroupsId()
    {
        return $this->groupsId;
    }

    /**
     * Set ticketTco
     *
     * @param float $ticketTco
     * 
     * @return GlpiSoftwares
     */
    public function setTicketTco($ticketTco)
    {
        $this->ticketTco = $ticketTco;

        return $this;
    }

    /**
     * Get ticketTco
     *
     * @return float 
     */
    public function getTicketTco()
    {
        return $this->ticketTco;
    }

    /**
     * Set isHelpdeskVisible
     *
     * @param boolean $isHelpdeskVisible
     * 
     * @return GlpiSoftwares
     */
    public function setIsHelpdeskVisible($isHelpdeskVisible)
    {
        $this->isHelpdeskVisible = $isHelpdeskVisible;

        return $this;
    }

    /**
     * Get isHelpdeskVisible
     *
     * @return boolean 
     */
    public function getIsHelpdeskVisible()
    {
        return $this->isHelpdeskVisible;
    }

    /**
     * Set softwarecategoriesId
     *
     * @param integer $softwarecategoriesId
     * 
     * @return GlpiSoftwares
     */
    public function setSoftwarecategoriesId($softwarecategoriesId)
    {
        $this->softwarecategoriesId = $softwarecategoriesId;

        return $this;
    }

    /**
     * Get softwarecategoriesId
     *
     * @return integer 
     */
    public function getSoftwarecategoriesId()
    {
        return $this->softwarecategoriesId;
    }
}