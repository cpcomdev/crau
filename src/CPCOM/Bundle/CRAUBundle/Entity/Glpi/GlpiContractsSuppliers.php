<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiContractsSuppliers
 *
 * @ORM\Table(name="glpi_contracts_suppliers")
 * @ORM\Entity
 */
class GlpiContractsSuppliers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="contracts_id", type="integer", nullable=false)
     */
    private $contractsId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set suppliersId
     *
     * @param integer $suppliersId
     * 
     * @return GlpiContractsSuppliers
     */
    public function setSuppliersId($suppliersId)
    {
        $this->suppliersId = $suppliersId;

        return $this;
    }

    /**
     * Get suppliersId
     *
     * @return integer 
     */
    public function getSuppliersId()
    {
        return $this->suppliersId;
    }

    /**
     * Set contractsId
     *
     * @param integer $contractsId
     * 
     * @return GlpiContractsSuppliers
     */
    public function setContractsId($contractsId)
    {
        $this->contractsId = $contractsId;

        return $this;
    }

    /**
     * Get contractsId
     *
     * @return integer 
     */
    public function getContractsId()
    {
        return $this->contractsId;
    }
}