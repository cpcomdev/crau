<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicenetworkcards
 *
 * @ORM\Table(name="glpi_computers_devicenetworkcards")
 * @ORM\Entity
 */
class GlpiComputersDevicenetworkcards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicenetworkcards_id", type="integer", nullable=false)
     */
    private $devicenetworkcardsId;

    /**
     * @var string
     *
     * @ORM\Column(name="specificity", type="string", length=255, nullable=true)
     */
    private $specificity;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiComputersDevicenetworkcards
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set devicenetworkcardsId
     *
     * @param integer $devicenetworkcardsId
     * 
     * @return GlpiComputersDevicenetworkcards
     */
    public function setDevicenetworkcardsId($devicenetworkcardsId)
    {
        $this->devicenetworkcardsId = $devicenetworkcardsId;

        return $this;
    }

    /**
     * Get devicenetworkcardsId
     *
     * @return integer 
     */
    public function getDevicenetworkcardsId()
    {
        return $this->devicenetworkcardsId;
    }

    /**
     * Set specificity
     *
     * @param string $specificity
     * 
     * @return GlpiComputersDevicenetworkcards
     */
    public function setSpecificity($specificity)
    {
        $this->specificity = $specificity;

        return $this;
    }

    /**
     * Get specificity
     *
     * @return string 
     */
    public function getSpecificity()
    {
        return $this->specificity;
    }
}