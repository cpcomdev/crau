<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderReferencesSuppliers
 *
 * @ORM\Table(name="glpi_plugin_order_references_suppliers")
 * @ORM\Entity
 */
class GlpiPluginOrderReferencesSuppliers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_references_id", type="integer", nullable=false)
     */
    private $pluginOrderReferencesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var float
     *
     * @ORM\Column(name="price_taxfree", type="decimal", nullable=false)
     */
    private $priceTaxfree;

    /**
     * @var string
     *
     * @ORM\Column(name="reference_code", type="string", length=255, nullable=true)
     */
    private $referenceCode;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiPluginOrderReferencesSuppliers
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set isRecursive
     *
     * @param boolean $isRecursive
     * 
     * @return GlpiPluginOrderReferencesSuppliers
     */
    public function setIsRecursive($isRecursive)
    {
        $this->isRecursive = $isRecursive;

        return $this;
    }

    /**
     * Get isRecursive
     *
     * @return boolean 
     */
    public function getIsRecursive()
    {
        return $this->isRecursive;
    }

    /**
     * Set pluginOrderReferencesId
     *
     * @param integer $pluginOrderReferencesId
     * 
     * @return GlpiPluginOrderReferencesSuppliers
     */
    public function setPluginOrderReferencesId($pluginOrderReferencesId)
    {
        $this->pluginOrderReferencesId = $pluginOrderReferencesId;

        return $this;
    }

    /**
     * Get pluginOrderReferencesId
     *
     * @return integer 
     */
    public function getPluginOrderReferencesId()
    {
        return $this->pluginOrderReferencesId;
    }

    /**
     * Set suppliersId
     *
     * @param integer $suppliersId
     * 
     * @return GlpiPluginOrderReferencesSuppliers
     */
    public function setSuppliersId($suppliersId)
    {
        $this->suppliersId = $suppliersId;

        return $this;
    }

    /**
     * Get suppliersId
     *
     * @return integer 
     */
    public function getSuppliersId()
    {
        return $this->suppliersId;
    }

    /**
     * Set priceTaxfree
     *
     * @param float $priceTaxfree
     * 
     * @return GlpiPluginOrderReferencesSuppliers
     */
    public function setPriceTaxfree($priceTaxfree)
    {
        $this->priceTaxfree = $priceTaxfree;

        return $this;
    }

    /**
     * Get priceTaxfree
     *
     * @return float 
     */
    public function getPriceTaxfree()
    {
        return $this->priceTaxfree;
    }

    /**
     * Set referenceCode
     *
     * @param string $referenceCode
     * 
     * @return GlpiPluginOrderReferencesSuppliers
     */
    public function setReferenceCode($referenceCode)
    {
        $this->referenceCode = $referenceCode;

        return $this;
    }

    /**
     * Get referenceCode
     *
     * @return string 
     */
    public function getReferenceCode()
    {
        return $this->referenceCode;
    }
}