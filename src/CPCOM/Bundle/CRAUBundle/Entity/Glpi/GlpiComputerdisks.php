<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputerdisks
 *
 * @ORM\Table(name="glpi_computerdisks")
 * @ORM\Entity
 */
class GlpiComputerdisks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="device", type="string", length=255, nullable=true)
     */
    private $device;

    /**
     * @var string
     *
     * @ORM\Column(name="mountpoint", type="string", length=255, nullable=true)
     */
    private $mountpoint;

    /**
     * @var integer
     *
     * @ORM\Column(name="filesystems_id", type="integer", nullable=false)
     */
    private $filesystemsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="totalsize", type="integer", nullable=false)
     */
    private $totalsize;

    /**
     * @var integer
     *
     * @ORM\Column(name="freesize", type="integer", nullable=false)
     */
    private $freesize;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiComputerdisks
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiComputerdisks
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiComputerdisks
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set device
     *
     * @param string $device
     * 
     * @return GlpiComputerdisks
     */
    public function setDevice($device)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return string 
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set mountpoint
     *
     * @param string $mountpoint
     * 
     * @return GlpiComputerdisks
     */
    public function setMountpoint($mountpoint)
    {
        $this->mountpoint = $mountpoint;

        return $this;
    }

    /**
     * Get mountpoint
     *
     * @return string 
     */
    public function getMountpoint()
    {
        return $this->mountpoint;
    }

    /**
     * Set filesystemsId
     *
     * @param integer $filesystemsId
     * 
     * @return GlpiComputerdisks
     */
    public function setFilesystemsId($filesystemsId)
    {
        $this->filesystemsId = $filesystemsId;

        return $this;
    }

    /**
     * Get filesystemsId
     *
     * @return integer 
     */
    public function getFilesystemsId()
    {
        return $this->filesystemsId;
    }

    /**
     * Set totalsize
     *
     * @param integer $totalsize
     * 
     * @return GlpiComputerdisks
     */
    public function setTotalsize($totalsize)
    {
        $this->totalsize = $totalsize;

        return $this;
    }

    /**
     * Get totalsize
     *
     * @return integer 
     */
    public function getTotalsize()
    {
        return $this->totalsize;
    }

    /**
     * Set freesize
     *
     * @param integer $freesize
     * 
     * @return GlpiComputerdisks
     */
    public function setFreesize($freesize)
    {
        $this->freesize = $freesize;

        return $this;
    }

    /**
     * Get freesize
     *
     * @return integer 
     */
    public function getFreesize()
    {
        return $this->freesize;
    }
}