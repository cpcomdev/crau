<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderOrders
 *
 * @ORM\Table(name="glpi_plugin_order_orders")
 * @ORM\Entity
 */
class GlpiPluginOrderOrders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_template", type="boolean", nullable=false)
     */
    private $isTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="template_name", type="string", length=255, nullable=true)
     */
    private $templateName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="num_order", type="string", length=255, nullable=true)
     */
    private $numOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="budgets_id", type="integer", nullable=false)
     */
    private $budgetsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_ordertaxes_id", type="integer", nullable=false)
     */
    private $pluginOrderOrdertaxesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_orderpayments_id", type="integer", nullable=false)
     */
    private $pluginOrderOrderpaymentsId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_date", type="date", nullable=true)
     */
    private $orderDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="duedate", type="date", nullable=true)
     */
    private $duedate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deliverydate", type="date", nullable=true)
     */
    private $deliverydate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_late", type="boolean", nullable=false)
     */
    private $isLate;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="contacts_id", type="integer", nullable=false)
     */
    private $contactsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="locations_id", type="integer", nullable=false)
     */
    private $locationsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_orderstates_id", type="integer", nullable=false)
     */
    private $pluginOrderOrderstatesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_billstates_id", type="integer", nullable=false)
     */
    private $pluginOrderBillstatesId;

    /**
     * @var float
     *
     * @ORM\Column(name="port_price", type="float", nullable=false)
     */
    private $portPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="notepad", type="text", nullable=true)
     */
    private $notepad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id", type="integer", nullable=false)
     */
    private $groupsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id_delivery", type="integer", nullable=false)
     */
    private $usersIdDelivery;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id_delivery", type="integer", nullable=false)
     */
    private $groupsIdDelivery;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_ordertypes_id", type="integer", nullable=false)
     */
    private $pluginOrderOrdertypesId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set isTemplate
     *
     * @param boolean $isTemplate
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setIsTemplate($isTemplate)
    {
        $this->isTemplate = $isTemplate;

        return $this;
    }

    /**
     * Get isTemplate
     *
     * @return boolean 
     */
    public function getIsTemplate()
    {
        return $this->isTemplate;
    }

    /**
     * Set templateName
     *
     * @param string $templateName
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;

        return $this;
    }

    /**
     * Get templateName
     *
     * @return string 
     */
    public function getTemplateName()
    {
        return $this->templateName;
    }

    /**
     * Set isRecursive
     *
     * @param boolean $isRecursive
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setIsRecursive($isRecursive)
    {
        $this->isRecursive = $isRecursive;

        return $this;
    }

    /**
     * Get isRecursive
     *
     * @return boolean 
     */
    public function getIsRecursive()
    {
        return $this->isRecursive;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set numOrder
     *
     * @param string $numOrder
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setNumOrder($numOrder)
    {
        $this->numOrder = $numOrder;

        return $this;
    }

    /**
     * Get numOrder
     *
     * @return string 
     */
    public function getNumOrder()
    {
        return $this->numOrder;
    }

    /**
     * Set budgetsId
     *
     * @param integer $budgetsId
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setBudgetsId($budgetsId)
    {
        $this->budgetsId = $budgetsId;

        return $this;
    }

    /**
     * Get budgetsId
     *
     * @return integer 
     */
    public function getBudgetsId()
    {
        return $this->budgetsId;
    }

    /**
     * Set pluginOrderOrdertaxesId
     *
     * @param integer $pluginOrderOrdertaxesId
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setPluginOrderOrdertaxesId($pluginOrderOrdertaxesId)
    {
        $this->pluginOrderOrdertaxesId = $pluginOrderOrdertaxesId;

        return $this;
    }

    /**
     * Get pluginOrderOrdertaxesId
     *
     * @return integer 
     */
    public function getPluginOrderOrdertaxesId()
    {
        return $this->pluginOrderOrdertaxesId;
    }

    /**
     * Set pluginOrderOrderpaymentsId
     *
     * @param integer $pluginOrderOrderpaymentsId
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setPluginOrderOrderpaymentsId($pluginOrderOrderpaymentsId)
    {
        $this->pluginOrderOrderpaymentsId = $pluginOrderOrderpaymentsId;

        return $this;
    }

    /**
     * Get pluginOrderOrderpaymentsId
     *
     * @return integer 
     */
    public function getPluginOrderOrderpaymentsId()
    {
        return $this->pluginOrderOrderpaymentsId;
    }

    /**
     * Set orderDate
     *
     * @param \DateTime $orderDate
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setOrderDate($orderDate)
    {
        $this->orderDate = $orderDate;

        return $this;
    }

    /**
     * Get orderDate
     *
     * @return \DateTime 
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * Set duedate
     *
     * @param \DateTime $duedate
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setDuedate($duedate)
    {
        $this->duedate = $duedate;

        return $this;
    }

    /**
     * Get duedate
     *
     * @return \DateTime 
     */
    public function getDuedate()
    {
        return $this->duedate;
    }

    /**
     * Set deliverydate
     *
     * @param \DateTime $deliverydate
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setDeliverydate($deliverydate)
    {
        $this->deliverydate = $deliverydate;

        return $this;
    }

    /**
     * Get deliverydate
     *
     * @return \DateTime 
     */
    public function getDeliverydate()
    {
        return $this->deliverydate;
    }

    /**
     * Set isLate
     *
     * @param boolean $isLate
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setIsLate($isLate)
    {
        $this->isLate = $isLate;

        return $this;
    }

    /**
     * Get isLate
     *
     * @return boolean 
     */
    public function getIsLate()
    {
        return $this->isLate;
    }

    /**
     * Set suppliersId
     *
     * @param integer $suppliersId
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setSuppliersId($suppliersId)
    {
        $this->suppliersId = $suppliersId;

        return $this;
    }

    /**
     * Get suppliersId
     *
     * @return integer 
     */
    public function getSuppliersId()
    {
        return $this->suppliersId;
    }

    /**
     * Set contactsId
     *
     * @param integer $contactsId
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setContactsId($contactsId)
    {
        $this->contactsId = $contactsId;

        return $this;
    }

    /**
     * Get contactsId
     *
     * @return integer 
     */
    public function getContactsId()
    {
        return $this->contactsId;
    }

    /**
     * Set locationsId
     *
     * @param integer $locationsId
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setLocationsId($locationsId)
    {
        $this->locationsId = $locationsId;

        return $this;
    }

    /**
     * Get locationsId
     *
     * @return integer 
     */
    public function getLocationsId()
    {
        return $this->locationsId;
    }

    /**
     * Set pluginOrderOrderstatesId
     *
     * @param integer $pluginOrderOrderstatesId
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setPluginOrderOrderstatesId($pluginOrderOrderstatesId)
    {
        $this->pluginOrderOrderstatesId = $pluginOrderOrderstatesId;

        return $this;
    }

    /**
     * Get pluginOrderOrderstatesId
     *
     * @return integer 
     */
    public function getPluginOrderOrderstatesId()
    {
        return $this->pluginOrderOrderstatesId;
    }

    /**
     * Set pluginOrderBillstatesId
     *
     * @param integer $pluginOrderBillstatesId
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setPluginOrderBillstatesId($pluginOrderBillstatesId)
    {
        $this->pluginOrderBillstatesId = $pluginOrderBillstatesId;

        return $this;
    }

    /**
     * Get pluginOrderBillstatesId
     *
     * @return integer 
     */
    public function getPluginOrderBillstatesId()
    {
        return $this->pluginOrderBillstatesId;
    }

    /**
     * Set portPrice
     *
     * @param float $portPrice
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setPortPrice($portPrice)
    {
        $this->portPrice = $portPrice;

        return $this;
    }

    /**
     * Get portPrice
     *
     * @return float 
     */
    public function getPortPrice()
    {
        return $this->portPrice;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set notepad
     *
     * @param string $notepad
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setNotepad($notepad)
    {
        $this->notepad = $notepad;

        return $this;
    }

    /**
     * Get notepad
     *
     * @return string 
     */
    public function getNotepad()
    {
        return $this->notepad;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set usersId
     *
     * @param integer $usersId
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setUsersId($usersId)
    {
        $this->usersId = $usersId;

        return $this;
    }

    /**
     * Get usersId
     *
     * @return integer 
     */
    public function getUsersId()
    {
        return $this->usersId;
    }

    /**
     * Set groupsId
     *
     * @param integer $groupsId
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setGroupsId($groupsId)
    {
        $this->groupsId = $groupsId;

        return $this;
    }

    /**
     * Get groupsId
     *
     * @return integer 
     */
    public function getGroupsId()
    {
        return $this->groupsId;
    }

    /**
     * Set usersIdDelivery
     *
     * @param integer $usersIdDelivery
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setUsersIdDelivery($usersIdDelivery)
    {
        $this->usersIdDelivery = $usersIdDelivery;

        return $this;
    }

    /**
     * Get usersIdDelivery
     *
     * @return integer 
     */
    public function getUsersIdDelivery()
    {
        return $this->usersIdDelivery;
    }

    /**
     * Set groupsIdDelivery
     *
     * @param integer $groupsIdDelivery
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setGroupsIdDelivery($groupsIdDelivery)
    {
        $this->groupsIdDelivery = $groupsIdDelivery;

        return $this;
    }

    /**
     * Get groupsIdDelivery
     *
     * @return integer 
     */
    public function getGroupsIdDelivery()
    {
        return $this->groupsIdDelivery;
    }

    /**
     * Set pluginOrderOrdertypesId
     *
     * @param integer $pluginOrderOrdertypesId
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setPluginOrderOrdertypesId($pluginOrderOrdertypesId)
    {
        $this->pluginOrderOrdertypesId = $pluginOrderOrdertypesId;

        return $this;
    }

    /**
     * Get pluginOrderOrdertypesId
     *
     * @return integer 
     */
    public function getPluginOrderOrdertypesId()
    {
        return $this->pluginOrderOrdertypesId;
    }

    /**
     * Set dateMod
     *
     * @param \DateTime $dateMod
     * 
     * @return GlpiPluginOrderOrders
     */
    public function setDateMod($dateMod)
    {
        $this->dateMod = $dateMod;

        return $this;
    }

    /**
     * Get dateMod
     *
     * @return \DateTime 
     */
    public function getDateMod()
    {
        return $this->dateMod;
    }
}