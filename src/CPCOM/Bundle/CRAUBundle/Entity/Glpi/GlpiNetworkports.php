<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiNetworkports
 *
 * @ORM\Table(name="glpi_networkports")
 * @ORM\Entity
 */
class GlpiNetworkports
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="items_id", type="integer", nullable=false)
     */
    private $itemsId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=100, nullable=false)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="logical_number", type="integer", nullable=false)
     */
    private $logicalNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=true)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="mac", type="string", length=255, nullable=true)
     */
    private $mac;

    /**
     * @var integer
     *
     * @ORM\Column(name="networkinterfaces_id", type="integer", nullable=false)
     */
    private $networkinterfacesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="netpoints_id", type="integer", nullable=false)
     */
    private $netpointsId;

    /**
     * @var string
     *
     * @ORM\Column(name="netmask", type="string", length=255, nullable=true)
     */
    private $netmask;

    /**
     * @var string
     *
     * @ORM\Column(name="gateway", type="string", length=255, nullable=true)
     */
    private $gateway;

    /**
     * @var string
     *
     * @ORM\Column(name="subnet", type="string", length=255, nullable=true)
     */
    private $subnet;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itemsId
     *
     * @param integer $itemsId
     * 
     * @return GlpiNetworkports
     */
    public function setItemsId($itemsId)
    {
        $this->itemsId = $itemsId;

        return $this;
    }

    /**
     * Get itemsId
     *
     * @return integer 
     */
    public function getItemsId()
    {
        return $this->itemsId;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     * 
     * @return GlpiNetworkports
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string 
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiNetworkports
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set isRecursive
     *
     * @param boolean $isRecursive
     * 
     * @return GlpiNetworkports
     */
    public function setIsRecursive($isRecursive)
    {
        $this->isRecursive = $isRecursive;

        return $this;
    }

    /**
     * Get isRecursive
     *
     * @return boolean 
     */
    public function getIsRecursive()
    {
        return $this->isRecursive;
    }

    /**
     * Set logicalNumber
     *
     * @param integer $logicalNumber
     * 
     * @return GlpiNetworkports
     */
    public function setLogicalNumber($logicalNumber)
    {
        $this->logicalNumber = $logicalNumber;

        return $this;
    }

    /**
     * Get logicalNumber
     *
     * @return integer 
     */
    public function getLogicalNumber()
    {
        return $this->logicalNumber;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiNetworkports
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * 
     * @return GlpiNetworkports
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set mac
     *
     * @param string $mac
     * 
     * @return GlpiNetworkports
     */
    public function setMac($mac)
    {
        $this->mac = $mac;

        return $this;
    }

    /**
     * Get mac
     *
     * @return string 
     */
    public function getMac()
    {
        return $this->mac;
    }

    /**
     * Set networkinterfacesId
     *
     * @param integer $networkinterfacesId
     * 
     * @return GlpiNetworkports
     */
    public function setNetworkinterfacesId($networkinterfacesId)
    {
        $this->networkinterfacesId = $networkinterfacesId;

        return $this;
    }

    /**
     * Get networkinterfacesId
     *
     * @return integer 
     */
    public function getNetworkinterfacesId()
    {
        return $this->networkinterfacesId;
    }

    /**
     * Set netpointsId
     *
     * @param integer $netpointsId
     * 
     * @return GlpiNetworkports
     */
    public function setNetpointsId($netpointsId)
    {
        $this->netpointsId = $netpointsId;

        return $this;
    }

    /**
     * Get netpointsId
     *
     * @return integer 
     */
    public function getNetpointsId()
    {
        return $this->netpointsId;
    }

    /**
     * Set netmask
     *
     * @param string $netmask
     * 
     * @return GlpiNetworkports
     */
    public function setNetmask($netmask)
    {
        $this->netmask = $netmask;

        return $this;
    }

    /**
     * Get netmask
     *
     * @return string 
     */
    public function getNetmask()
    {
        return $this->netmask;
    }

    /**
     * Set gateway
     *
     * @param string $gateway
     * 
     * @return GlpiNetworkports
     */
    public function setGateway($gateway)
    {
        $this->gateway = $gateway;

        return $this;
    }

    /**
     * Get gateway
     *
     * @return string 
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * Set subnet
     *
     * @param string $subnet
     * 
     * @return GlpiNetworkports
     */
    public function setSubnet($subnet)
    {
        $this->subnet = $subnet;

        return $this;
    }

    /**
     * Get subnet
     *
     * @return string 
     */
    public function getSubnet()
    {
        return $this->subnet;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiNetworkports
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}