<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiOcsadmininfoslinks
 *
 * @ORM\Table(name="glpi_ocsadmininfoslinks")
 * @ORM\Entity
 */
class GlpiOcsadmininfoslinks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="glpi_column", type="string", length=255, nullable=true)
     */
    private $glpiColumn;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_column", type="string", length=255, nullable=true)
     */
    private $ocsColumn;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsservers_id", type="integer", nullable=false)
     */
    private $ocsserversId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set glpiColumn
     *
     * @param string $glpiColumn
     * 
     * @return GlpiOcsadmininfoslinks
     */
    public function setGlpiColumn($glpiColumn)
    {
        $this->glpiColumn = $glpiColumn;

        return $this;
    }

    /**
     * Get glpiColumn
     *
     * @return string 
     */
    public function getGlpiColumn()
    {
        return $this->glpiColumn;
    }

    /**
     * Set ocsColumn
     *
     * @param string $ocsColumn
     * 
     * @return GlpiOcsadmininfoslinks
     */
    public function setOcsColumn($ocsColumn)
    {
        $this->ocsColumn = $ocsColumn;

        return $this;
    }

    /**
     * Get ocsColumn
     *
     * @return string 
     */
    public function getOcsColumn()
    {
        return $this->ocsColumn;
    }

    /**
     * Set ocsserversId
     *
     * @param integer $ocsserversId
     * 
     * @return GlpiOcsadmininfoslinks
     */
    public function setOcsserversId($ocsserversId)
    {
        $this->ocsserversId = $ocsserversId;

        return $this;
    }

    /**
     * Get ocsserversId
     *
     * @return integer 
     */
    public function getOcsserversId()
    {
        return $this->ocsserversId;
    }
}