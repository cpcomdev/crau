<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiFieldunicities
 *
 * @ORM\Table(name="glpi_fieldunicities")
 * @ORM\Entity
 */
class GlpiFieldunicities
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255, nullable=false)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="fields", type="text", nullable=true)
     */
    private $fields;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="action_refuse", type="boolean", nullable=false)
     */
    private $actionRefuse;

    /**
     * @var boolean
     *
     * @ORM\Column(name="action_notify", type="boolean", nullable=false)
     */
    private $actionNotify;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiFieldunicities
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isRecursive
     *
     * @param boolean $isRecursive
     * 
     * @return GlpiFieldunicities
     */
    public function setIsRecursive($isRecursive)
    {
        $this->isRecursive = $isRecursive;

        return $this;
    }

    /**
     * Get isRecursive
     *
     * @return boolean 
     */
    public function getIsRecursive()
    {
        return $this->isRecursive;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     * 
     * @return GlpiFieldunicities
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string 
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiFieldunicities
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set fields
     *
     * @param string $fields
     * 
     * @return GlpiFieldunicities
     */
    public function setFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * Get fields
     *
     * @return string 
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * 
     * @return GlpiFieldunicities
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set actionRefuse
     *
     * @param boolean $actionRefuse
     * 
     * @return GlpiFieldunicities
     */
    public function setActionRefuse($actionRefuse)
    {
        $this->actionRefuse = $actionRefuse;

        return $this;
    }

    /**
     * Get actionRefuse
     *
     * @return boolean 
     */
    public function getActionRefuse()
    {
        return $this->actionRefuse;
    }

    /**
     * Set actionNotify
     *
     * @param boolean $actionNotify
     * 
     * @return GlpiFieldunicities
     */
    public function setActionNotify($actionNotify)
    {
        $this->actionNotify = $actionNotify;

        return $this;
    }

    /**
     * Get actionNotify
     *
     * @return boolean 
     */
    public function getActionNotify()
    {
        return $this->actionNotify;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiFieldunicities
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}