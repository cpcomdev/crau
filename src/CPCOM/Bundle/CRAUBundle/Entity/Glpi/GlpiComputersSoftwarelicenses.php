<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersSoftwarelicenses
 *
 * @ORM\Table(name="glpi_computers_softwarelicenses")
 * @ORM\Entity
 */
class GlpiComputersSoftwarelicenses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwarelicenses_id", type="integer", nullable=false)
     */
    private $softwarelicensesId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiComputersSoftwarelicenses
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set softwarelicensesId
     *
     * @param integer $softwarelicensesId
     * 
     * @return GlpiComputersSoftwarelicenses
     */
    public function setSoftwarelicensesId($softwarelicensesId)
    {
        $this->softwarelicensesId = $softwarelicensesId;

        return $this;
    }

    /**
     * Get softwarelicensesId
     *
     * @return integer 
     */
    public function getSoftwarelicensesId()
    {
        return $this->softwarelicensesId;
    }
}