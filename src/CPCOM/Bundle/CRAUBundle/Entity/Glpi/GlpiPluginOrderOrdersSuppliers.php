<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderOrdersSuppliers
 *
 * @ORM\Table(name="glpi_plugin_order_orders_suppliers")
 * @ORM\Entity
 */
class GlpiPluginOrderOrdersSuppliers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_orders_id", type="integer", nullable=false)
     */
    private $pluginOrderOrdersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var string
     *
     * @ORM\Column(name="num_quote", type="string", length=255, nullable=true)
     */
    private $numQuote;

    /**
     * @var string
     *
     * @ORM\Column(name="num_order", type="string", length=255, nullable=true)
     */
    private $numOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiPluginOrderOrdersSuppliers
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set isRecursive
     *
     * @param boolean $isRecursive
     * 
     * @return GlpiPluginOrderOrdersSuppliers
     */
    public function setIsRecursive($isRecursive)
    {
        $this->isRecursive = $isRecursive;

        return $this;
    }

    /**
     * Get isRecursive
     *
     * @return boolean 
     */
    public function getIsRecursive()
    {
        return $this->isRecursive;
    }

    /**
     * Set pluginOrderOrdersId
     *
     * @param integer $pluginOrderOrdersId
     * 
     * @return GlpiPluginOrderOrdersSuppliers
     */
    public function setPluginOrderOrdersId($pluginOrderOrdersId)
    {
        $this->pluginOrderOrdersId = $pluginOrderOrdersId;

        return $this;
    }

    /**
     * Get pluginOrderOrdersId
     *
     * @return integer 
     */
    public function getPluginOrderOrdersId()
    {
        return $this->pluginOrderOrdersId;
    }

    /**
     * Set suppliersId
     *
     * @param integer $suppliersId
     * 
     * @return GlpiPluginOrderOrdersSuppliers
     */
    public function setSuppliersId($suppliersId)
    {
        $this->suppliersId = $suppliersId;

        return $this;
    }

    /**
     * Get suppliersId
     *
     * @return integer 
     */
    public function getSuppliersId()
    {
        return $this->suppliersId;
    }

    /**
     * Set numQuote
     *
     * @param string $numQuote
     * 
     * @return GlpiPluginOrderOrdersSuppliers
     */
    public function setNumQuote($numQuote)
    {
        $this->numQuote = $numQuote;

        return $this;
    }

    /**
     * Get numQuote
     *
     * @return string 
     */
    public function getNumQuote()
    {
        return $this->numQuote;
    }

    /**
     * Set numOrder
     *
     * @param string $numOrder
     * 
     * @return GlpiPluginOrderOrdersSuppliers
     */
    public function setNumOrder($numOrder)
    {
        $this->numOrder = $numOrder;

        return $this;
    }

    /**
     * Get numOrder
     *
     * @return string 
     */
    public function getNumOrder()
    {
        return $this->numOrder;
    }
}