<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginMassocsimportThreads
 *
 * @ORM\Table(name="glpi_plugin_massocsimport_threads")
 * @ORM\Entity
 */
class GlpiPluginMassocsimportThreads
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="threadid", type="integer", nullable=false)
     */
    private $threadid;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="rules_id", type="text", nullable=true)
     */
    private $rulesId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="datetime", nullable=true)
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_time", type="datetime", nullable=true)
     */
    private $endTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="error_msg", type="text", nullable=false)
     */
    private $errorMsg;

    /**
     * @var integer
     *
     * @ORM\Column(name="imported_machines_number", type="integer", nullable=false)
     */
    private $importedMachinesNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="synchronized_machines_number", type="integer", nullable=false)
     */
    private $synchronizedMachinesNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="failed_rules_machines_number", type="integer", nullable=false)
     */
    private $failedRulesMachinesNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="linked_machines_number", type="integer", nullable=false)
     */
    private $linkedMachinesNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="notupdated_machines_number", type="integer", nullable=false)
     */
    private $notupdatedMachinesNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="not_unique_machines_number", type="integer", nullable=false)
     */
    private $notUniqueMachinesNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="link_refused_machines_number", type="integer", nullable=false)
     */
    private $linkRefusedMachinesNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_number_machines", type="integer", nullable=false)
     */
    private $totalNumberMachines;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsservers_id", type="integer", nullable=false)
     */
    private $ocsserversId;

    /**
     * @var integer
     *
     * @ORM\Column(name="processid", type="integer", nullable=false)
     */
    private $processid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set threadid
     *
     * @param integer $threadid
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setThreadid($threadid)
    {
        $this->threadid = $threadid;

        return $this;
    }

    /**
     * Get threadid
     *
     * @return integer 
     */
    public function getThreadid()
    {
        return $this->threadid;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set rulesId
     *
     * @param string $rulesId
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setRulesId($rulesId)
    {
        $this->rulesId = $rulesId;

        return $this;
    }

    /**
     * Get rulesId
     *
     * @return string 
     */
    public function getRulesId()
    {
        return $this->rulesId;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime 
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime 
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set status
     *
     * @param integer $status
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set errorMsg
     *
     * @param string $errorMsg
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setErrorMsg($errorMsg)
    {
        $this->errorMsg = $errorMsg;

        return $this;
    }

    /**
     * Get errorMsg
     *
     * @return string 
     */
    public function getErrorMsg()
    {
        return $this->errorMsg;
    }

    /**
     * Set importedMachinesNumber
     *
     * @param integer $importedMachinesNumber
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setImportedMachinesNumber($importedMachinesNumber)
    {
        $this->importedMachinesNumber = $importedMachinesNumber;

        return $this;
    }

    /**
     * Get importedMachinesNumber
     *
     * @return integer 
     */
    public function getImportedMachinesNumber()
    {
        return $this->importedMachinesNumber;
    }

    /**
     * Set synchronizedMachinesNumber
     *
     * @param integer $synchronizedMachinesNumber
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setSynchronizedMachinesNumber($synchronizedMachinesNumber)
    {
        $this->synchronizedMachinesNumber = $synchronizedMachinesNumber;

        return $this;
    }

    /**
     * Get synchronizedMachinesNumber
     *
     * @return integer 
     */
    public function getSynchronizedMachinesNumber()
    {
        return $this->synchronizedMachinesNumber;
    }

    /**
     * Set failedRulesMachinesNumber
     *
     * @param integer $failedRulesMachinesNumber
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setFailedRulesMachinesNumber($failedRulesMachinesNumber)
    {
        $this->failedRulesMachinesNumber = $failedRulesMachinesNumber;

        return $this;
    }

    /**
     * Get failedRulesMachinesNumber
     *
     * @return integer 
     */
    public function getFailedRulesMachinesNumber()
    {
        return $this->failedRulesMachinesNumber;
    }

    /**
     * Set linkedMachinesNumber
     *
     * @param integer $linkedMachinesNumber
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setLinkedMachinesNumber($linkedMachinesNumber)
    {
        $this->linkedMachinesNumber = $linkedMachinesNumber;

        return $this;
    }

    /**
     * Get linkedMachinesNumber
     *
     * @return integer 
     */
    public function getLinkedMachinesNumber()
    {
        return $this->linkedMachinesNumber;
    }

    /**
     * Set notupdatedMachinesNumber
     *
     * @param integer $notupdatedMachinesNumber
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setNotupdatedMachinesNumber($notupdatedMachinesNumber)
    {
        $this->notupdatedMachinesNumber = $notupdatedMachinesNumber;

        return $this;
    }

    /**
     * Get notupdatedMachinesNumber
     *
     * @return integer 
     */
    public function getNotupdatedMachinesNumber()
    {
        return $this->notupdatedMachinesNumber;
    }

    /**
     * Set notUniqueMachinesNumber
     *
     * @param integer $notUniqueMachinesNumber
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setNotUniqueMachinesNumber($notUniqueMachinesNumber)
    {
        $this->notUniqueMachinesNumber = $notUniqueMachinesNumber;

        return $this;
    }

    /**
     * Get notUniqueMachinesNumber
     *
     * @return integer 
     */
    public function getNotUniqueMachinesNumber()
    {
        return $this->notUniqueMachinesNumber;
    }

    /**
     * Set linkRefusedMachinesNumber
     *
     * @param integer $linkRefusedMachinesNumber
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setLinkRefusedMachinesNumber($linkRefusedMachinesNumber)
    {
        $this->linkRefusedMachinesNumber = $linkRefusedMachinesNumber;

        return $this;
    }

    /**
     * Get linkRefusedMachinesNumber
     *
     * @return integer 
     */
    public function getLinkRefusedMachinesNumber()
    {
        return $this->linkRefusedMachinesNumber;
    }

    /**
     * Set totalNumberMachines
     *
     * @param integer $totalNumberMachines
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setTotalNumberMachines($totalNumberMachines)
    {
        $this->totalNumberMachines = $totalNumberMachines;

        return $this;
    }

    /**
     * Get totalNumberMachines
     *
     * @return integer 
     */
    public function getTotalNumberMachines()
    {
        return $this->totalNumberMachines;
    }

    /**
     * Set ocsserversId
     *
     * @param integer $ocsserversId
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setOcsserversId($ocsserversId)
    {
        $this->ocsserversId = $ocsserversId;

        return $this;
    }

    /**
     * Get ocsserversId
     *
     * @return integer 
     */
    public function getOcsserversId()
    {
        return $this->ocsserversId;
    }

    /**
     * Set processid
     *
     * @param integer $processid
     * 
     * @return GlpiPluginMassocsimportThreads
     */
    public function setProcessid($processid)
    {
        $this->processid = $processid;

        return $this;
    }

    /**
     * Get processid
     *
     * @return integer 
     */
    public function getProcessid()
    {
        return $this->processid;
    }
}