<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputervirtualmachines
 *
 * @ORM\Table(name="glpi_computervirtualmachines")
 * @ORM\Entity
 */
class GlpiComputervirtualmachines
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="virtualmachinestates_id", type="integer", nullable=false)
     */
    private $virtualmachinestatesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="virtualmachinesystems_id", type="integer", nullable=false)
     */
    private $virtualmachinesystemsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="virtualmachinetypes_id", type="integer", nullable=false)
     */
    private $virtualmachinetypesId;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=255, nullable=false)
     */
    private $uuid;

    /**
     * @var integer
     *
     * @ORM\Column(name="vcpu", type="integer", nullable=false)
     */
    private $vcpu;

    /**
     * @var string
     *
     * @ORM\Column(name="ram", type="string", length=255, nullable=false)
     */
    private $ram;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiComputervirtualmachines
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiComputervirtualmachines
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiComputervirtualmachines
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set virtualmachinestatesId
     *
     * @param integer $virtualmachinestatesId
     * 
     * @return GlpiComputervirtualmachines
     */
    public function setVirtualmachinestatesId($virtualmachinestatesId)
    {
        $this->virtualmachinestatesId = $virtualmachinestatesId;

        return $this;
    }

    /**
     * Get virtualmachinestatesId
     *
     * @return integer 
     */
    public function getVirtualmachinestatesId()
    {
        return $this->virtualmachinestatesId;
    }

    /**
     * Set virtualmachinesystemsId
     *
     * @param integer $virtualmachinesystemsId
     * 
     * @return GlpiComputervirtualmachines
     */
    public function setVirtualmachinesystemsId($virtualmachinesystemsId)
    {
        $this->virtualmachinesystemsId = $virtualmachinesystemsId;

        return $this;
    }

    /**
     * Get virtualmachinesystemsId
     *
     * @return integer 
     */
    public function getVirtualmachinesystemsId()
    {
        return $this->virtualmachinesystemsId;
    }

    /**
     * Set virtualmachinetypesId
     *
     * @param integer $virtualmachinetypesId
     * 
     * @return GlpiComputervirtualmachines
     */
    public function setVirtualmachinetypesId($virtualmachinetypesId)
    {
        $this->virtualmachinetypesId = $virtualmachinetypesId;

        return $this;
    }

    /**
     * Get virtualmachinetypesId
     *
     * @return integer 
     */
    public function getVirtualmachinetypesId()
    {
        return $this->virtualmachinetypesId;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * 
     * @return GlpiComputervirtualmachines
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set vcpu
     *
     * @param integer $vcpu
     * 
     * @return GlpiComputervirtualmachines
     */
    public function setVcpu($vcpu)
    {
        $this->vcpu = $vcpu;

        return $this;
    }

    /**
     * Get vcpu
     *
     * @return integer 
     */
    public function getVcpu()
    {
        return $this->vcpu;
    }

    /**
     * Set ram
     *
     * @param string $ram
     * 
     * @return GlpiComputervirtualmachines
     */
    public function setRam($ram)
    {
        $this->ram = $ram;

        return $this;
    }

    /**
     * Get ram
     *
     * @return string 
     */
    public function getRam()
    {
        return $this->ram;
    }
}