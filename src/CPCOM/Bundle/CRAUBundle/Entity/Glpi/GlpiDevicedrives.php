<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiDevicedrives
 *
 * @ORM\Table(name="glpi_devicedrives")
 * @ORM\Entity
 */
class GlpiDevicedrives
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_writer", type="boolean", nullable=false)
     */
    private $isWriter;

    /**
     * @var string
     *
     * @ORM\Column(name="speed", type="string", length=255, nullable=true)
     */
    private $speed;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="interfacetypes_id", type="integer", nullable=false)
     */
    private $interfacetypesId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     * 
     * @return GlpiDevicedrives
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string 
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set isWriter
     *
     * @param boolean $isWriter
     * 
     * @return GlpiDevicedrives
     */
    public function setIsWriter($isWriter)
    {
        $this->isWriter = $isWriter;

        return $this;
    }

    /**
     * Get isWriter
     *
     * @return boolean 
     */
    public function getIsWriter()
    {
        return $this->isWriter;
    }

    /**
     * Set speed
     *
     * @param string $speed
     * 
     * @return GlpiDevicedrives
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get speed
     *
     * @return string 
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiDevicedrives
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set manufacturersId
     *
     * @param integer $manufacturersId
     * 
     * @return GlpiDevicedrives
     */
    public function setManufacturersId($manufacturersId)
    {
        $this->manufacturersId = $manufacturersId;

        return $this;
    }

    /**
     * Get manufacturersId
     *
     * @return integer 
     */
    public function getManufacturersId()
    {
        return $this->manufacturersId;
    }

    /**
     * Set interfacetypesId
     *
     * @param integer $interfacetypesId
     * 
     * @return GlpiDevicedrives
     */
    public function setInterfacetypesId($interfacetypesId)
    {
        $this->interfacetypesId = $interfacetypesId;

        return $this;
    }

    /**
     * Get interfacetypesId
     *
     * @return integer 
     */
    public function getInterfacetypesId()
    {
        return $this->interfacetypesId;
    }
}