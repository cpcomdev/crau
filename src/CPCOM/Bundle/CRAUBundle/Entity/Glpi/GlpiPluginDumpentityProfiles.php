<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginDumpentityProfiles
 *
 * @ORM\Table(name="glpi_plugin_dumpentity_profiles")
 * @ORM\Entity
 */
class GlpiPluginDumpentityProfiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="models_id", type="integer", nullable=false)
     */
    private $modelsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="profiles_id", type="integer", nullable=false)
     */
    private $profilesId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modelsId
     *
     * @param integer $modelsId
     * 
     * @return GlpiPluginDumpentityProfiles
     */
    public function setModelsId($modelsId)
    {
        $this->modelsId = $modelsId;

        return $this;
    }

    /**
     * Get modelsId
     *
     * @return integer 
     */
    public function getModelsId()
    {
        return $this->modelsId;
    }

    /**
     * Set profilesId
     *
     * @param integer $profilesId
     * 
     * @return GlpiPluginDumpentityProfiles
     */
    public function setProfilesId($profilesId)
    {
        $this->profilesId = $profilesId;

        return $this;
    }

    /**
     * Get profilesId
     *
     * @return integer 
     */
    public function getProfilesId()
    {
        return $this->profilesId;
    }
}