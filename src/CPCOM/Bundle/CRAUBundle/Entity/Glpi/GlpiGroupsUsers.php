<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiGroupsUsers
 *
 * @ORM\Table(name="glpi_groups_users")
 * @ORM\Entity
 */
class GlpiGroupsUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id", type="integer", nullable=false)
     */
    private $groupsId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_dynamic", type="boolean", nullable=false)
     */
    private $isDynamic;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_manager", type="boolean", nullable=false)
     */
    private $isManager;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_userdelegate", type="boolean", nullable=false)
     */
    private $isUserdelegate;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usersId
     *
     * @param integer $usersId
     * 
     * @return GlpiGroupsUsers
     */
    public function setUsersId($usersId)
    {
        $this->usersId = $usersId;

        return $this;
    }

    /**
     * Get usersId
     *
     * @return integer 
     */
    public function getUsersId()
    {
        return $this->usersId;
    }

    /**
     * Set groupsId
     *
     * @param integer $groupsId
     * 
     * @return GlpiGroupsUsers
     */
    public function setGroupsId($groupsId)
    {
        $this->groupsId = $groupsId;

        return $this;
    }

    /**
     * Get groupsId
     *
     * @return integer 
     */
    public function getGroupsId()
    {
        return $this->groupsId;
    }

    /**
     * Set isDynamic
     *
     * @param boolean $isDynamic
     * 
     * @return GlpiGroupsUsers
     */
    public function setIsDynamic($isDynamic)
    {
        $this->isDynamic = $isDynamic;

        return $this;
    }

    /**
     * Get isDynamic
     *
     * @return boolean 
     */
    public function getIsDynamic()
    {
        return $this->isDynamic;
    }

    /**
     * Set isManager
     *
     * @param boolean $isManager
     * 
     * @return GlpiGroupsUsers
     */
    public function setIsManager($isManager)
    {
        $this->isManager = $isManager;

        return $this;
    }

    /**
     * Get isManager
     *
     * @return boolean 
     */
    public function getIsManager()
    {
        return $this->isManager;
    }

    /**
     * Set isUserdelegate
     *
     * @param boolean $isUserdelegate
     * 
     * @return GlpiGroupsUsers
     */
    public function setIsUserdelegate($isUserdelegate)
    {
        $this->isUserdelegate = $isUserdelegate;

        return $this;
    }

    /**
     * Get isUserdelegate
     *
     * @return boolean 
     */
    public function getIsUserdelegate()
    {
        return $this->isUserdelegate;
    }
}