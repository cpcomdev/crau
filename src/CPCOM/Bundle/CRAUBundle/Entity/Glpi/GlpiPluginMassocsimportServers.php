<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginMassocsimportServers
 *
 * @ORM\Table(name="glpi_plugin_massocsimport_servers")
 * @ORM\Entity
 */
class GlpiPluginMassocsimportServers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsservers_id", type="integer", nullable=false)
     */
    private $ocsserversId;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_ocsid", type="integer", nullable=true)
     */
    private $maxOcsid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="max_glpidate", type="datetime", nullable=true)
     */
    private $maxGlpidate;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ocsserversId
     *
     * @param integer $ocsserversId
     * 
     * @return GlpiPluginMassocsimportServers
     */
    public function setOcsserversId($ocsserversId)
    {
        $this->ocsserversId = $ocsserversId;

        return $this;
    }

    /**
     * Get ocsserversId
     *
     * @return integer 
     */
    public function getOcsserversId()
    {
        return $this->ocsserversId;
    }

    /**
     * Set maxOcsid
     *
     * @param integer $maxOcsid
     * 
     * @return GlpiPluginMassocsimportServers
     */
    public function setMaxOcsid($maxOcsid)
    {
        $this->maxOcsid = $maxOcsid;

        return $this;
    }

    /**
     * Get maxOcsid
     *
     * @return integer 
     */
    public function getMaxOcsid()
    {
        return $this->maxOcsid;
    }

    /**
     * Set maxGlpidate
     *
     * @param \DateTime $maxGlpidate
     * 
     * @return GlpiPluginMassocsimportServers
     */
    public function setMaxGlpidate($maxGlpidate)
    {
        $this->maxGlpidate = $maxGlpidate;

        return $this;
    }

    /**
     * Get maxGlpidate
     *
     * @return \DateTime 
     */
    public function getMaxGlpidate()
    {
        return $this->maxGlpidate;
    }
}