<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginDumpentityClients
 *
 * @ORM\Table(name="glpi_plugin_dumpentity_clients")
 * @ORM\Entity
 */
class GlpiPluginDumpentityClients
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=false)
     */
    private $ip;

    /**
     * @var integer
     *
     * @ORM\Column(name="models_id", type="integer", nullable=false)
     */
    private $modelsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="use_time_exclusion", type="smallint", nullable=false)
     */
    private $useTimeExclusion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="allow_start_time", type="time", nullable=false)
     */
    private $allowStartTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="allow_end_time", type="time", nullable=false)
     */
    private $allowEndTime;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiPluginDumpentityClients
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiPluginDumpentityClients
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set dateMod
     *
     * @param \DateTime $dateMod
     * 
     * @return GlpiPluginDumpentityClients
     */
    public function setDateMod($dateMod)
    {
        $this->dateMod = $dateMod;

        return $this;
    }

    /**
     * Get dateMod
     *
     * @return \DateTime 
     */
    public function getDateMod()
    {
        return $this->dateMod;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * 
     * @return GlpiPluginDumpentityClients
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set modelsId
     *
     * @param integer $modelsId
     * 
     * @return GlpiPluginDumpentityClients
     */
    public function setModelsId($modelsId)
    {
        $this->modelsId = $modelsId;

        return $this;
    }

    /**
     * Get modelsId
     *
     * @return integer 
     */
    public function getModelsId()
    {
        return $this->modelsId;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiPluginDumpentityClients
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set isRecursive
     *
     * @param boolean $isRecursive
     * 
     * @return GlpiPluginDumpentityClients
     */
    public function setIsRecursive($isRecursive)
    {
        $this->isRecursive = $isRecursive;

        return $this;
    }

    /**
     * Get isRecursive
     *
     * @return boolean 
     */
    public function getIsRecursive()
    {
        return $this->isRecursive;
    }

    /**
     * Set useTimeExclusion
     *
     * @param integer $useTimeExclusion
     * 
     * @return GlpiPluginDumpentityClients
     */
    public function setUseTimeExclusion($useTimeExclusion)
    {
        $this->useTimeExclusion = $useTimeExclusion;

        return $this;
    }

    /**
     * Get useTimeExclusion
     *
     * @return integer 
     */
    public function getUseTimeExclusion()
    {
        return $this->useTimeExclusion;
    }

    /**
     * Set allowStartTime
     *
     * @param \DateTime $allowStartTime
     * 
     * @return GlpiPluginDumpentityClients
     */
    public function setAllowStartTime($allowStartTime)
    {
        $this->allowStartTime = $allowStartTime;

        return $this;
    }

    /**
     * Get allowStartTime
     *
     * @return \DateTime 
     */
    public function getAllowStartTime()
    {
        return $this->allowStartTime;
    }

    /**
     * Set allowEndTime
     *
     * @param \DateTime $allowEndTime
     * 
     * @return GlpiPluginDumpentityClients
     */
    public function setAllowEndTime($allowEndTime)
    {
        $this->allowEndTime = $allowEndTime;

        return $this;
    }

    /**
     * Get allowEndTime
     *
     * @return \DateTime 
     */
    public function getAllowEndTime()
    {
        return $this->allowEndTime;
    }
}