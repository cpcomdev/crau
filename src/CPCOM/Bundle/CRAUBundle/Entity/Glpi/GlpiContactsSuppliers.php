<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiContactsSuppliers
 *
 * @ORM\Table(name="glpi_contacts_suppliers")
 * @ORM\Entity
 */
class GlpiContactsSuppliers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="contacts_id", type="integer", nullable=false)
     */
    private $contactsId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set suppliersId
     *
     * @param integer $suppliersId
     * 
     * @return GlpiContactsSuppliers
     */
    public function setSuppliersId($suppliersId)
    {
        $this->suppliersId = $suppliersId;

        return $this;
    }

    /**
     * Get suppliersId
     *
     * @return integer 
     */
    public function getSuppliersId()
    {
        return $this->suppliersId;
    }

    /**
     * Set contactsId
     *
     * @param integer $contactsId
     * 
     * @return GlpiContactsSuppliers
     */
    public function setContactsId($contactsId)
    {
        $this->contactsId = $contactsId;

        return $this;
    }

    /**
     * Get contactsId
     *
     * @return integer 
     */
    public function getContactsId()
    {
        return $this->contactsId;
    }
}