<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderBills
 *
 * @ORM\Table(name="glpi_plugin_order_bills")
 * @ORM\Entity
 */
class GlpiPluginOrderBills
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255, nullable=true)
     */
    private $number;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="billdate", type="datetime", nullable=true)
     */
    private $billdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="validationdate", type="datetime", nullable=true)
     */
    private $validationdate;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_billstates_id", type="integer", nullable=false)
     */
    private $pluginOrderBillstatesId;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="decimal", nullable=false)
     */
    private $value;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_billtypes_id", type="integer", nullable=false)
     */
    private $pluginOrderBilltypesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id", type="integer", nullable=false)
     */
    private $suppliersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_orders_id", type="integer", nullable=false)
     */
    private $pluginOrderOrdersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id_validation", type="integer", nullable=false)
     */
    private $usersIdValidation;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_recursive", type="integer", nullable=false)
     */
    private $isRecursive;

    /**
     * @var string
     *
     * @ORM\Column(name="notepad", type="text", nullable=true)
     */
    private $notepad;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiPluginOrderBills
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param string $number
     * 
     * @return GlpiPluginOrderBills
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set billdate
     *
     * @param \DateTime $billdate
     * 
     * @return GlpiPluginOrderBills
     */
    public function setBilldate($billdate)
    {
        $this->billdate = $billdate;

        return $this;
    }

    /**
     * Get billdate
     *
     * @return \DateTime 
     */
    public function getBilldate()
    {
        return $this->billdate;
    }

    /**
     * Set validationdate
     *
     * @param \DateTime $validationdate
     * 
     * @return GlpiPluginOrderBills
     */
    public function setValidationdate($validationdate)
    {
        $this->validationdate = $validationdate;

        return $this;
    }

    /**
     * Get validationdate
     *
     * @return \DateTime 
     */
    public function getValidationdate()
    {
        return $this->validationdate;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiPluginOrderBills
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set pluginOrderBillstatesId
     *
     * @param integer $pluginOrderBillstatesId
     * 
     * @return GlpiPluginOrderBills
     */
    public function setPluginOrderBillstatesId($pluginOrderBillstatesId)
    {
        $this->pluginOrderBillstatesId = $pluginOrderBillstatesId;

        return $this;
    }

    /**
     * Get pluginOrderBillstatesId
     *
     * @return integer 
     */
    public function getPluginOrderBillstatesId()
    {
        return $this->pluginOrderBillstatesId;
    }

    /**
     * Set value
     *
     * @param float $value
     * 
     * @return GlpiPluginOrderBills
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set pluginOrderBilltypesId
     *
     * @param integer $pluginOrderBilltypesId
     * 
     * @return GlpiPluginOrderBills
     */
    public function setPluginOrderBilltypesId($pluginOrderBilltypesId)
    {
        $this->pluginOrderBilltypesId = $pluginOrderBilltypesId;

        return $this;
    }

    /**
     * Get pluginOrderBilltypesId
     *
     * @return integer 
     */
    public function getPluginOrderBilltypesId()
    {
        return $this->pluginOrderBilltypesId;
    }

    /**
     * Set suppliersId
     *
     * @param integer $suppliersId
     * 
     * @return GlpiPluginOrderBills
     */
    public function setSuppliersId($suppliersId)
    {
        $this->suppliersId = $suppliersId;

        return $this;
    }

    /**
     * Get suppliersId
     *
     * @return integer 
     */
    public function getSuppliersId()
    {
        return $this->suppliersId;
    }

    /**
     * Set pluginOrderOrdersId
     *
     * @param integer $pluginOrderOrdersId
     * 
     * @return GlpiPluginOrderBills
     */
    public function setPluginOrderOrdersId($pluginOrderOrdersId)
    {
        $this->pluginOrderOrdersId = $pluginOrderOrdersId;

        return $this;
    }

    /**
     * Get pluginOrderOrdersId
     *
     * @return integer 
     */
    public function getPluginOrderOrdersId()
    {
        return $this->pluginOrderOrdersId;
    }

    /**
     * Set usersIdValidation
     *
     * @param integer $usersIdValidation
     * 
     * @return GlpiPluginOrderBills
     */
    public function setUsersIdValidation($usersIdValidation)
    {
        $this->usersIdValidation = $usersIdValidation;

        return $this;
    }

    /**
     * Get usersIdValidation
     *
     * @return integer 
     */
    public function getUsersIdValidation()
    {
        return $this->usersIdValidation;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiPluginOrderBills
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set isRecursive
     *
     * @param integer $isRecursive
     * 
     * @return GlpiPluginOrderBills
     */
    public function setIsRecursive($isRecursive)
    {
        $this->isRecursive = $isRecursive;

        return $this;
    }

    /**
     * Get isRecursive
     *
     * @return integer 
     */
    public function getIsRecursive()
    {
        return $this->isRecursive;
    }

    /**
     * Set notepad
     *
     * @param string $notepad
     * 
     * @return GlpiPluginOrderBills
     */
    public function setNotepad($notepad)
    {
        $this->notepad = $notepad;

        return $this;
    }

    /**
     * Get notepad
     *
     * @return string 
     */
    public function getNotepad()
    {
        return $this->notepad;
    }
}