<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiTicketfollowups
 *
 * @ORM\Table(name="glpi_ticketfollowups")
 * @ORM\Entity
 */
class GlpiTicketfollowups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickets_id", type="integer", nullable=false)
     */
    private $ticketsId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_private", type="boolean", nullable=false)
     */
    private $isPrivate;

    /**
     * @var integer
     *
     * @ORM\Column(name="requesttypes_id", type="integer", nullable=false)
     */
    private $requesttypesId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ticketsId
     *
     * @param integer $ticketsId
     * 
     * @return GlpiTicketfollowups
     */
    public function setTicketsId($ticketsId)
    {
        $this->ticketsId = $ticketsId;

        return $this;
    }

    /**
     * Get ticketsId
     *
     * @return integer 
     */
    public function getTicketsId()
    {
        return $this->ticketsId;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * 
     * @return GlpiTicketfollowups
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set usersId
     *
     * @param integer $usersId
     * 
     * @return GlpiTicketfollowups
     */
    public function setUsersId($usersId)
    {
        $this->usersId = $usersId;

        return $this;
    }

    /**
     * Get usersId
     *
     * @return integer 
     */
    public function getUsersId()
    {
        return $this->usersId;
    }

    /**
     * Set content
     *
     * @param string $content
     * 
     * @return GlpiTicketfollowups
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set isPrivate
     *
     * @param boolean $isPrivate
     * 
     * @return GlpiTicketfollowups
     */
    public function setIsPrivate($isPrivate)
    {
        $this->isPrivate = $isPrivate;

        return $this;
    }

    /**
     * Get isPrivate
     *
     * @return boolean 
     */
    public function getIsPrivate()
    {
        return $this->isPrivate;
    }

    /**
     * Set requesttypesId
     *
     * @param integer $requesttypesId
     * 
     * @return GlpiTicketfollowups
     */
    public function setRequesttypesId($requesttypesId)
    {
        $this->requesttypesId = $requesttypesId;

        return $this;
    }

    /**
     * Get requesttypesId
     *
     * @return integer 
     */
    public function getRequesttypesId()
    {
        return $this->requesttypesId;
    }
}