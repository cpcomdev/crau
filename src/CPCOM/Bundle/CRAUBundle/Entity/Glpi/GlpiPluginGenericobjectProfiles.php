<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginGenericobjectProfiles
 *
 * @ORM\Table(name="glpi_plugin_genericobject_profiles")
 * @ORM\Entity
 */
class GlpiPluginGenericobjectProfiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="profiles_id", type="integer", nullable=false)
     */
    private $profilesId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255, nullable=true)
     */
    private $itemtype;

    /**
     * @var string
     *
     * @ORM\Column(name="right", type="string", length=1, nullable=true)
     */
    private $right;

    /**
     * @var string
     *
     * @ORM\Column(name="open_ticket", type="string", length=1, nullable=false)
     */
    private $openTicket;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profilesId
     *
     * @param integer $profilesId
     * 
     * @return GlpiPluginGenericobjectProfiles
     */
    public function setProfilesId($profilesId)
    {
        $this->profilesId = $profilesId;

        return $this;
    }

    /**
     * Get profilesId
     *
     * @return integer 
     */
    public function getProfilesId()
    {
        return $this->profilesId;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     * 
     * @return GlpiPluginGenericobjectProfiles
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string 
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set right
     *
     * @param string $right
     * 
     * @return GlpiPluginGenericobjectProfiles
     */
    public function setRight($right)
    {
        $this->right = $right;

        return $this;
    }

    /**
     * Get right
     *
     * @return string 
     */
    public function getRight()
    {
        return $this->right;
    }

    /**
     * Set openTicket
     *
     * @param string $openTicket
     * 
     * @return GlpiPluginGenericobjectProfiles
     */
    public function setOpenTicket($openTicket)
    {
        $this->openTicket = $openTicket;

        return $this;
    }

    /**
     * Get openTicket
     *
     * @return string 
     */
    public function getOpenTicket()
    {
        return $this->openTicket;
    }
}