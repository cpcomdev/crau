<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginMassocsimportNotimported
 *
 * @ORM\Table(name="glpi_plugin_massocsimport_notimported")
 * @ORM\Entity
 */
class GlpiPluginMassocsimportNotimported
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="rules_id", type="text", nullable=true)
     */
    private $rulesId;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsid", type="integer", nullable=false)
     */
    private $ocsid;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsservers_id", type="integer", nullable=false)
     */
    private $ocsserversId;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_deviceid", type="string", length=255, nullable=false)
     */
    private $ocsDeviceid;

    /**
     * @var string
     *
     * @ORM\Column(name="useragent", type="string", length=255, nullable=false)
     */
    private $useragent;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255, nullable=false)
     */
    private $tag;

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=255, nullable=false)
     */
    private $serial;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="ipaddr", type="string", length=255, nullable=false)
     */
    private $ipaddr;

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=255, nullable=false)
     */
    private $domain;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_inventory", type="datetime", nullable=true)
     */
    private $lastInventory;

    /**
     * @var integer
     *
     * @ORM\Column(name="reason", type="integer", nullable=false)
     */
    private $reason;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiPluginMassocsimportNotimported
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set rulesId
     *
     * @param string $rulesId
     * 
     * @return GlpiPluginMassocsimportNotimported
     */
    public function setRulesId($rulesId)
    {
        $this->rulesId = $rulesId;

        return $this;
    }

    /**
     * Get rulesId
     *
     * @return string 
     */
    public function getRulesId()
    {
        return $this->rulesId;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiPluginMassocsimportNotimported
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set ocsid
     *
     * @param integer $ocsid
     * 
     * @return GlpiPluginMassocsimportNotimported
     */
    public function setOcsid($ocsid)
    {
        $this->ocsid = $ocsid;

        return $this;
    }

    /**
     * Get ocsid
     *
     * @return integer 
     */
    public function getOcsid()
    {
        return $this->ocsid;
    }

    /**
     * Set ocsserversId
     *
     * @param integer $ocsserversId
     * 
     * @return GlpiPluginMassocsimportNotimported
     */
    public function setOcsserversId($ocsserversId)
    {
        $this->ocsserversId = $ocsserversId;

        return $this;
    }

    /**
     * Get ocsserversId
     *
     * @return integer 
     */
    public function getOcsserversId()
    {
        return $this->ocsserversId;
    }

    /**
     * Set ocsDeviceid
     *
     * @param string $ocsDeviceid
     * 
     * @return GlpiPluginMassocsimportNotimported
     */
    public function setOcsDeviceid($ocsDeviceid)
    {
        $this->ocsDeviceid = $ocsDeviceid;

        return $this;
    }

    /**
     * Get ocsDeviceid
     *
     * @return string 
     */
    public function getOcsDeviceid()
    {
        return $this->ocsDeviceid;
    }

    /**
     * Set useragent
     *
     * @param string $useragent
     * 
     * @return GlpiPluginMassocsimportNotimported
     */
    public function setUseragent($useragent)
    {
        $this->useragent = $useragent;

        return $this;
    }

    /**
     * Get useragent
     *
     * @return string 
     */
    public function getUseragent()
    {
        return $this->useragent;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * 
     * @return GlpiPluginMassocsimportNotimported
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set serial
     *
     * @param string $serial
     * 
     * @return GlpiPluginMassocsimportNotimported
     */
    public function setSerial($serial)
    {
        $this->serial = $serial;

        return $this;
    }

    /**
     * Get serial
     *
     * @return string 
     */
    public function getSerial()
    {
        return $this->serial;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiPluginMassocsimportNotimported
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ipaddr
     *
     * @param string $ipaddr
     * 
     * @return GlpiPluginMassocsimportNotimported
     */
    public function setIpaddr($ipaddr)
    {
        $this->ipaddr = $ipaddr;

        return $this;
    }

    /**
     * Get ipaddr
     *
     * @return string 
     */
    public function getIpaddr()
    {
        return $this->ipaddr;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * 
     * @return GlpiPluginMassocsimportNotimported
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set lastInventory
     *
     * @param \DateTime $lastInventory
     * 
     * @return GlpiPluginMassocsimportNotimported
     */
    public function setLastInventory($lastInventory)
    {
        $this->lastInventory = $lastInventory;

        return $this;
    }

    /**
     * Get lastInventory
     *
     * @return \DateTime 
     */
    public function getLastInventory()
    {
        return $this->lastInventory;
    }

    /**
     * Set reason
     *
     * @param integer $reason
     * 
     * @return GlpiPluginMassocsimportNotimported
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return integer 
     */
    public function getReason()
    {
        return $this->reason;
    }
}