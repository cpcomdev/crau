<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiDevicecases
 *
 * @ORM\Table(name="glpi_devicecases")
 * @ORM\Entity
 */
class GlpiDevicecases
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicecasetypes_id", type="integer", nullable=false)
     */
    private $devicecasetypesId;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     * 
     * @return GlpiDevicecases
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string 
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set devicecasetypesId
     *
     * @param integer $devicecasetypesId
     * 
     * @return GlpiDevicecases
     */
    public function setDevicecasetypesId($devicecasetypesId)
    {
        $this->devicecasetypesId = $devicecasetypesId;

        return $this;
    }

    /**
     * Get devicecasetypesId
     *
     * @return integer 
     */
    public function getDevicecasetypesId()
    {
        return $this->devicecasetypesId;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiDevicecases
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set manufacturersId
     *
     * @param integer $manufacturersId
     * 
     * @return GlpiDevicecases
     */
    public function setManufacturersId($manufacturersId)
    {
        $this->manufacturersId = $manufacturersId;

        return $this;
    }

    /**
     * Get manufacturersId
     *
     * @return integer 
     */
    public function getManufacturersId()
    {
        return $this->manufacturersId;
    }
}