<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginDatainjectionInfos
 *
 * @ORM\Table(name="glpi_plugin_datainjection_infos")
 * @ORM\Entity
 */
class GlpiPluginDatainjectionInfos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="models_id", type="integer", nullable=false)
     */
    private $modelsId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255, nullable=false)
     */
    private $itemtype;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=false)
     */
    private $value;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_mandatory", type="boolean", nullable=false)
     */
    private $isMandatory;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modelsId
     *
     * @param integer $modelsId
     * 
     * @return GlpiPluginDatainjectionInfos
     */
    public function setModelsId($modelsId)
    {
        $this->modelsId = $modelsId;

        return $this;
    }

    /**
     * Get modelsId
     *
     * @return integer 
     */
    public function getModelsId()
    {
        return $this->modelsId;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     * 
     * @return GlpiPluginDatainjectionInfos
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string 
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set value
     *
     * @param string $value
     * 
     * @return GlpiPluginDatainjectionInfos
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set isMandatory
     *
     * @param boolean $isMandatory
     * 
     * @return GlpiPluginDatainjectionInfos
     */
    public function setIsMandatory($isMandatory)
    {
        $this->isMandatory = $isMandatory;

        return $this;
    }

    /**
     * Get isMandatory
     *
     * @return boolean 
     */
    public function getIsMandatory()
    {
        return $this->isMandatory;
    }
}