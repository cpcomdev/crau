<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiSoftwareversions
 *
 * @ORM\Table(name="glpi_softwareversions")
 * @ORM\Entity
 */
class GlpiSoftwareversions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwares_id", type="integer", nullable=false)
     */
    private $softwaresId;

    /**
     * @var integer
     *
     * @ORM\Column(name="states_id", type="integer", nullable=false)
     */
    private $statesId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="operatingsystems_id", type="integer", nullable=false)
     */
    private $operatingsystemsId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiSoftwareversions
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set isRecursive
     *
     * @param boolean $isRecursive
     * 
     * @return GlpiSoftwareversions
     */
    public function setIsRecursive($isRecursive)
    {
        $this->isRecursive = $isRecursive;

        return $this;
    }

    /**
     * Get isRecursive
     *
     * @return boolean 
     */
    public function getIsRecursive()
    {
        return $this->isRecursive;
    }

    /**
     * Set softwaresId
     *
     * @param integer $softwaresId
     * 
     * @return GlpiSoftwareversions
     */
    public function setSoftwaresId($softwaresId)
    {
        $this->softwaresId = $softwaresId;

        return $this;
    }

    /**
     * Get softwaresId
     *
     * @return integer 
     */
    public function getSoftwaresId()
    {
        return $this->softwaresId;
    }

    /**
     * Set statesId
     *
     * @param integer $statesId
     * 
     * @return GlpiSoftwareversions
     */
    public function setStatesId($statesId)
    {
        $this->statesId = $statesId;

        return $this;
    }

    /**
     * Get statesId
     *
     * @return integer 
     */
    public function getStatesId()
    {
        return $this->statesId;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiSoftwareversions
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiSoftwareversions
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set operatingsystemsId
     *
     * @param integer $operatingsystemsId
     * 
     * @return GlpiSoftwareversions
     */
    public function setOperatingsystemsId($operatingsystemsId)
    {
        $this->operatingsystemsId = $operatingsystemsId;

        return $this;
    }

    /**
     * Get operatingsystemsId
     *
     * @return integer 
     */
    public function getOperatingsystemsId()
    {
        return $this->operatingsystemsId;
    }
}