<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderOrdersItems
 *
 * @ORM\Table(name="glpi_plugin_order_orders_items")
 * @ORM\Entity
 */
class GlpiPluginOrderOrdersItems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_orders_id", type="integer", nullable=false)
     */
    private $pluginOrderOrdersId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=100, nullable=false)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="items_id", type="integer", nullable=false)
     */
    private $itemsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_references_id", type="integer", nullable=false)
     */
    private $pluginOrderReferencesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_deliverystates_id", type="integer", nullable=false)
     */
    private $pluginOrderDeliverystatesId;

    /**
     * @var float
     *
     * @ORM\Column(name="plugin_order_ordertaxes_id", type="float", nullable=false)
     */
    private $pluginOrderOrdertaxesId;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_number", type="string", length=255, nullable=true)
     */
    private $deliveryNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_comment", type="text", nullable=true)
     */
    private $deliveryComment;

    /**
     * @var float
     *
     * @ORM\Column(name="price_taxfree", type="decimal", nullable=false)
     */
    private $priceTaxfree;

    /**
     * @var float
     *
     * @ORM\Column(name="price_discounted", type="decimal", nullable=false)
     */
    private $priceDiscounted;

    /**
     * @var float
     *
     * @ORM\Column(name="discount", type="decimal", nullable=false)
     */
    private $discount;

    /**
     * @var float
     *
     * @ORM\Column(name="price_ati", type="decimal", nullable=false)
     */
    private $priceAti;

    /**
     * @var integer
     *
     * @ORM\Column(name="states_id", type="integer", nullable=false)
     */
    private $statesId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="delivery_date", type="date", nullable=true)
     */
    private $deliveryDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_bills_id", type="integer", nullable=false)
     */
    private $pluginOrderBillsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_order_billstates_id", type="integer", nullable=false)
     */
    private $pluginOrderBillstatesId;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set isRecursive
     *
     * @param boolean $isRecursive
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setIsRecursive($isRecursive)
    {
        $this->isRecursive = $isRecursive;

        return $this;
    }

    /**
     * Get isRecursive
     *
     * @return boolean 
     */
    public function getIsRecursive()
    {
        return $this->isRecursive;
    }

    /**
     * Set pluginOrderOrdersId
     *
     * @param integer $pluginOrderOrdersId
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setPluginOrderOrdersId($pluginOrderOrdersId)
    {
        $this->pluginOrderOrdersId = $pluginOrderOrdersId;

        return $this;
    }

    /**
     * Get pluginOrderOrdersId
     *
     * @return integer 
     */
    public function getPluginOrderOrdersId()
    {
        return $this->pluginOrderOrdersId;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string 
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set itemsId
     *
     * @param integer $itemsId
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setItemsId($itemsId)
    {
        $this->itemsId = $itemsId;

        return $this;
    }

    /**
     * Get itemsId
     *
     * @return integer 
     */
    public function getItemsId()
    {
        return $this->itemsId;
    }

    /**
     * Set pluginOrderReferencesId
     *
     * @param integer $pluginOrderReferencesId
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setPluginOrderReferencesId($pluginOrderReferencesId)
    {
        $this->pluginOrderReferencesId = $pluginOrderReferencesId;

        return $this;
    }

    /**
     * Get pluginOrderReferencesId
     *
     * @return integer 
     */
    public function getPluginOrderReferencesId()
    {
        return $this->pluginOrderReferencesId;
    }

    /**
     * Set pluginOrderDeliverystatesId
     *
     * @param integer $pluginOrderDeliverystatesId
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setPluginOrderDeliverystatesId($pluginOrderDeliverystatesId)
    {
        $this->pluginOrderDeliverystatesId = $pluginOrderDeliverystatesId;

        return $this;
    }

    /**
     * Get pluginOrderDeliverystatesId
     *
     * @return integer 
     */
    public function getPluginOrderDeliverystatesId()
    {
        return $this->pluginOrderDeliverystatesId;
    }

    /**
     * Set pluginOrderOrdertaxesId
     *
     * @param float $pluginOrderOrdertaxesId
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setPluginOrderOrdertaxesId($pluginOrderOrdertaxesId)
    {
        $this->pluginOrderOrdertaxesId = $pluginOrderOrdertaxesId;

        return $this;
    }

    /**
     * Get pluginOrderOrdertaxesId
     *
     * @return float 
     */
    public function getPluginOrderOrdertaxesId()
    {
        return $this->pluginOrderOrdertaxesId;
    }

    /**
     * Set deliveryNumber
     *
     * @param string $deliveryNumber
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setDeliveryNumber($deliveryNumber)
    {
        $this->deliveryNumber = $deliveryNumber;

        return $this;
    }

    /**
     * Get deliveryNumber
     *
     * @return string 
     */
    public function getDeliveryNumber()
    {
        return $this->deliveryNumber;
    }

    /**
     * Set deliveryComment
     *
     * @param string $deliveryComment
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setDeliveryComment($deliveryComment)
    {
        $this->deliveryComment = $deliveryComment;

        return $this;
    }

    /**
     * Get deliveryComment
     *
     * @return string 
     */
    public function getDeliveryComment()
    {
        return $this->deliveryComment;
    }

    /**
     * Set priceTaxfree
     *
     * @param float $priceTaxfree
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setPriceTaxfree($priceTaxfree)
    {
        $this->priceTaxfree = $priceTaxfree;

        return $this;
    }

    /**
     * Get priceTaxfree
     *
     * @return float 
     */
    public function getPriceTaxfree()
    {
        return $this->priceTaxfree;
    }

    /**
     * Set priceDiscounted
     *
     * @param float $priceDiscounted
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setPriceDiscounted($priceDiscounted)
    {
        $this->priceDiscounted = $priceDiscounted;

        return $this;
    }

    /**
     * Get priceDiscounted
     *
     * @return float 
     */
    public function getPriceDiscounted()
    {
        return $this->priceDiscounted;
    }

    /**
     * Set discount
     *
     * @param float $discount
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return float 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set priceAti
     *
     * @param float $priceAti
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setPriceAti($priceAti)
    {
        $this->priceAti = $priceAti;

        return $this;
    }

    /**
     * Get priceAti
     *
     * @return float 
     */
    public function getPriceAti()
    {
        return $this->priceAti;
    }

    /**
     * Set statesId
     *
     * @param integer $statesId
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setStatesId($statesId)
    {
        $this->statesId = $statesId;

        return $this;
    }

    /**
     * Get statesId
     *
     * @return integer 
     */
    public function getStatesId()
    {
        return $this->statesId;
    }

    /**
     * Set deliveryDate
     *
     * @param \DateTime $deliveryDate
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;

        return $this;
    }

    /**
     * Get deliveryDate
     *
     * @return \DateTime 
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * Set pluginOrderBillsId
     *
     * @param integer $pluginOrderBillsId
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setPluginOrderBillsId($pluginOrderBillsId)
    {
        $this->pluginOrderBillsId = $pluginOrderBillsId;

        return $this;
    }

    /**
     * Get pluginOrderBillsId
     *
     * @return integer 
     */
    public function getPluginOrderBillsId()
    {
        return $this->pluginOrderBillsId;
    }

    /**
     * Set pluginOrderBillstatesId
     *
     * @param integer $pluginOrderBillstatesId
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setPluginOrderBillstatesId($pluginOrderBillstatesId)
    {
        $this->pluginOrderBillstatesId = $pluginOrderBillstatesId;

        return $this;
    }

    /**
     * Get pluginOrderBillstatesId
     *
     * @return integer 
     */
    public function getPluginOrderBillstatesId()
    {
        return $this->pluginOrderBillstatesId;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiPluginOrderOrdersItems
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}