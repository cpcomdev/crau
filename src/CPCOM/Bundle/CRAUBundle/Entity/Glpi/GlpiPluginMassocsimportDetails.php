<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginMassocsimportDetails
 *
 * @ORM\Table(name="glpi_plugin_massocsimport_details")
 * @ORM\Entity
 */
class GlpiPluginMassocsimportDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="plugin_massocsimport_threads_id", type="integer", nullable=false)
     */
    private $pluginMassocsimportThreadsId;

    /**
     * @var string
     *
     * @ORM\Column(name="rules_id", type="text", nullable=true)
     */
    private $rulesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="threadid", type="integer", nullable=false)
     */
    private $threadid;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsid", type="integer", nullable=false)
     */
    private $ocsid;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="action", type="integer", nullable=false)
     */
    private $action;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="process_time", type="datetime", nullable=true)
     */
    private $processTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsservers_id", type="integer", nullable=false)
     */
    private $ocsserversId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiPluginMassocsimportDetails
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set pluginMassocsimportThreadsId
     *
     * @param integer $pluginMassocsimportThreadsId
     * 
     * @return GlpiPluginMassocsimportDetails
     */
    public function setPluginMassocsimportThreadsId($pluginMassocsimportThreadsId)
    {
        $this->pluginMassocsimportThreadsId = $pluginMassocsimportThreadsId;

        return $this;
    }

    /**
     * Get pluginMassocsimportThreadsId
     *
     * @return integer 
     */
    public function getPluginMassocsimportThreadsId()
    {
        return $this->pluginMassocsimportThreadsId;
    }

    /**
     * Set rulesId
     *
     * @param string $rulesId
     * 
     * @return GlpiPluginMassocsimportDetails
     */
    public function setRulesId($rulesId)
    {
        $this->rulesId = $rulesId;

        return $this;
    }

    /**
     * Get rulesId
     *
     * @return string 
     */
    public function getRulesId()
    {
        return $this->rulesId;
    }

    /**
     * Set threadid
     *
     * @param integer $threadid
     * 
     * @return GlpiPluginMassocsimportDetails
     */
    public function setThreadid($threadid)
    {
        $this->threadid = $threadid;

        return $this;
    }

    /**
     * Get threadid
     *
     * @return integer 
     */
    public function getThreadid()
    {
        return $this->threadid;
    }

    /**
     * Set ocsid
     *
     * @param integer $ocsid
     * 
     * @return GlpiPluginMassocsimportDetails
     */
    public function setOcsid($ocsid)
    {
        $this->ocsid = $ocsid;

        return $this;
    }

    /**
     * Get ocsid
     *
     * @return integer 
     */
    public function getOcsid()
    {
        return $this->ocsid;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiPluginMassocsimportDetails
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set action
     *
     * @param integer $action
     * 
     * @return GlpiPluginMassocsimportDetails
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return integer 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set processTime
     *
     * @param \DateTime $processTime
     * 
     * @return GlpiPluginMassocsimportDetails
     */
    public function setProcessTime($processTime)
    {
        $this->processTime = $processTime;

        return $this;
    }

    /**
     * Get processTime
     *
     * @return \DateTime 
     */
    public function getProcessTime()
    {
        return $this->processTime;
    }

    /**
     * Set ocsserversId
     *
     * @param integer $ocsserversId
     * 
     * @return GlpiPluginMassocsimportDetails
     */
    public function setOcsserversId($ocsserversId)
    {
        $this->ocsserversId = $ocsserversId;

        return $this;
    }

    /**
     * Get ocsserversId
     *
     * @return integer 
     */
    public function getOcsserversId()
    {
        return $this->ocsserversId;
    }
}