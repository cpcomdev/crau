<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiConsumables
 *
 * @ORM\Table(name="glpi_consumables")
 * @ORM\Entity
 */
class GlpiConsumables
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="consumableitems_id", type="integer", nullable=false)
     */
    private $consumableitemsId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_in", type="date", nullable=true)
     */
    private $dateIn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_out", type="date", nullable=true)
     */
    private $dateOut;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=100, nullable=true)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="items_id", type="integer", nullable=false)
     */
    private $itemsId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiConsumables
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set consumableitemsId
     *
     * @param integer $consumableitemsId
     * 
     * @return GlpiConsumables
     */
    public function setConsumableitemsId($consumableitemsId)
    {
        $this->consumableitemsId = $consumableitemsId;

        return $this;
    }

    /**
     * Get consumableitemsId
     *
     * @return integer 
     */
    public function getConsumableitemsId()
    {
        return $this->consumableitemsId;
    }

    /**
     * Set dateIn
     *
     * @param \DateTime $dateIn
     * 
     * @return GlpiConsumables
     */
    public function setDateIn($dateIn)
    {
        $this->dateIn = $dateIn;

        return $this;
    }

    /**
     * Get dateIn
     *
     * @return \DateTime 
     */
    public function getDateIn()
    {
        return $this->dateIn;
    }

    /**
     * Set dateOut
     *
     * @param \DateTime $dateOut
     * 
     * @return GlpiConsumables
     */
    public function setDateOut($dateOut)
    {
        $this->dateOut = $dateOut;

        return $this;
    }

    /**
     * Get dateOut
     *
     * @return \DateTime 
     */
    public function getDateOut()
    {
        return $this->dateOut;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     * 
     * @return GlpiConsumables
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string 
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set itemsId
     *
     * @param integer $itemsId
     * 
     * @return GlpiConsumables
     */
    public function setItemsId($itemsId)
    {
        $this->itemsId = $itemsId;

        return $this;
    }

    /**
     * Get itemsId
     *
     * @return integer 
     */
    public function getItemsId()
    {
        return $this->itemsId;
    }
}