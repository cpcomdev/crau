<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiUsers
 *
 * @ORM\Table(name="glpi_users")
 * @ORM\Entity
 */
class GlpiUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=40, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="phone2", type="string", length=255, nullable=true)
     */
    private $phone2;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=255, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="realname", type="string", length=255, nullable=true)
     */
    private $realname;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var integer
     *
     * @ORM\Column(name="locations_id", type="integer", nullable=false)
     */
    private $locationsId;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=10, nullable=true)
     */
    private $language;

    /**
     * @var integer
     *
     * @ORM\Column(name="use_mode", type="integer", nullable=false)
     */
    private $useMode;

    /**
     * @var integer
     *
     * @ORM\Column(name="list_limit", type="integer", nullable=true)
     */
    private $listLimit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="auths_id", type="integer", nullable=false)
     */
    private $authsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="authtype", type="integer", nullable=false)
     */
    private $authtype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    private $lastLogin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_sync", type="datetime", nullable=true)
     */
    private $dateSync;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="profiles_id", type="integer", nullable=false)
     */
    private $profilesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="usertitles_id", type="integer", nullable=false)
     */
    private $usertitlesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="usercategories_id", type="integer", nullable=false)
     */
    private $usercategoriesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_format", type="integer", nullable=true)
     */
    private $dateFormat;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_format", type="integer", nullable=true)
     */
    private $numberFormat;

    /**
     * @var integer
     *
     * @ORM\Column(name="names_format", type="integer", nullable=true)
     */
    private $namesFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="csv_delimiter", type="string", length=1, nullable=true)
     */
    private $csvDelimiter;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_ids_visible", type="boolean", nullable=true)
     */
    private $isIdsVisible;

    /**
     * @var integer
     *
     * @ORM\Column(name="dropdown_chars_limit", type="integer", nullable=true)
     */
    private $dropdownCharsLimit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_flat_dropdowntree", type="boolean", nullable=true)
     */
    private $useFlatDropdowntree;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_jobs_at_login", type="boolean", nullable=true)
     */
    private $showJobsAtLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_1", type="string", length=20, nullable=true)
     */
    private $priority1;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_2", type="string", length=20, nullable=true)
     */
    private $priority2;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_3", type="string", length=20, nullable=true)
     */
    private $priority3;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_4", type="string", length=20, nullable=true)
     */
    private $priority4;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_5", type="string", length=20, nullable=true)
     */
    private $priority5;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_6", type="string", length=20, nullable=true)
     */
    private $priority6;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_categorized_soft_expanded", type="boolean", nullable=true)
     */
    private $isCategorizedSoftExpanded;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_not_categorized_soft_expanded", type="boolean", nullable=true)
     */
    private $isNotCategorizedSoftExpanded;

    /**
     * @var boolean
     *
     * @ORM\Column(name="followup_private", type="boolean", nullable=true)
     */
    private $followupPrivate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="task_private", type="boolean", nullable=true)
     */
    private $taskPrivate;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_requesttypes_id", type="integer", nullable=true)
     */
    private $defaultRequesttypesId;

    /**
     * @var string
     *
     * @ORM\Column(name="password_forget_token", type="string", length=40, nullable=true)
     */
    private $passwordForgetToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="password_forget_token_date", type="datetime", nullable=true)
     */
    private $passwordForgetTokenDate;

    /**
     * @var string
     *
     * @ORM\Column(name="user_dn", type="text", nullable=true)
     */
    private $userDn;

    /**
     * @var string
     *
     * @ORM\Column(name="registration_number", type="string", length=255, nullable=true)
     */
    private $registrationNumber;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_count_on_tabs", type="boolean", nullable=true)
     */
    private $showCountOnTabs;

    /**
     * @var integer
     *
     * @ORM\Column(name="refresh_ticket_list", type="integer", nullable=true)
     */
    private $refreshTicketList;

    /**
     * @var boolean
     *
     * @ORM\Column(name="set_default_tech", type="boolean", nullable=true)
     */
    private $setDefaultTech;

    /**
     * @var string
     *
     * @ORM\Column(name="personal_token", type="string", length=255, nullable=true)
     */
    private $personalToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="personal_token_date", type="datetime", nullable=true)
     */
    private $personalTokenDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="display_count_on_home", type="integer", nullable=true)
     */
    private $displayCountOnHome;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiUsers
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set password
     *
     * @param string $password
     * 
     * @return GlpiUsers
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * 
     * @return GlpiUsers
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone2
     *
     * @param string $phone2
     * 
     * @return GlpiUsers
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get phone2
     *
     * @return string 
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * 
     * @return GlpiUsers
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string 
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set realname
     *
     * @param string $realname
     * 
     * @return GlpiUsers
     */
    public function setRealname($realname)
    {
        $this->realname = $realname;

        return $this;
    }

    /**
     * Get realname
     *
     * @return string 
     */
    public function getRealname()
    {
        return $this->realname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * 
     * @return GlpiUsers
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set locationsId
     *
     * @param integer $locationsId
     * 
     * @return GlpiUsers
     */
    public function setLocationsId($locationsId)
    {
        $this->locationsId = $locationsId;

        return $this;
    }

    /**
     * Get locationsId
     *
     * @return integer 
     */
    public function getLocationsId()
    {
        return $this->locationsId;
    }

    /**
     * Set language
     *
     * @param string $language
     * 
     * @return GlpiUsers
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set useMode
     *
     * @param integer $useMode
     * 
     * @return GlpiUsers
     */
    public function setUseMode($useMode)
    {
        $this->useMode = $useMode;

        return $this;
    }

    /**
     * Get useMode
     *
     * @return integer 
     */
    public function getUseMode()
    {
        return $this->useMode;
    }

    /**
     * Set listLimit
     *
     * @param integer $listLimit
     * 
     * @return GlpiUsers
     */
    public function setListLimit($listLimit)
    {
        $this->listLimit = $listLimit;

        return $this;
    }

    /**
     * Get listLimit
     *
     * @return integer 
     */
    public function getListLimit()
    {
        return $this->listLimit;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * 
     * @return GlpiUsers
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiUsers
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set authsId
     *
     * @param integer $authsId
     * 
     * @return GlpiUsers
     */
    public function setAuthsId($authsId)
    {
        $this->authsId = $authsId;

        return $this;
    }

    /**
     * Get authsId
     *
     * @return integer 
     */
    public function getAuthsId()
    {
        return $this->authsId;
    }

    /**
     * Set authtype
     *
     * @param integer $authtype
     * 
     * @return GlpiUsers
     */
    public function setAuthtype($authtype)
    {
        $this->authtype = $authtype;

        return $this;
    }

    /**
     * Get authtype
     *
     * @return integer 
     */
    public function getAuthtype()
    {
        return $this->authtype;
    }

    /**
     * Set lastLogin
     *
     * @param \DateTime $lastLogin
     * 
     * @return GlpiUsers
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get lastLogin
     *
     * @return \DateTime 
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set dateMod
     *
     * @param \DateTime $dateMod
     * 
     * @return GlpiUsers
     */
    public function setDateMod($dateMod)
    {
        $this->dateMod = $dateMod;

        return $this;
    }

    /**
     * Get dateMod
     *
     * @return \DateTime 
     */
    public function getDateMod()
    {
        return $this->dateMod;
    }

    /**
     * Set dateSync
     *
     * @param \DateTime $dateSync
     * 
     * @return GlpiUsers
     */
    public function setDateSync($dateSync)
    {
        $this->dateSync = $dateSync;

        return $this;
    }

    /**
     * Get dateSync
     *
     * @return \DateTime 
     */
    public function getDateSync()
    {
        return $this->dateSync;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * 
     * @return GlpiUsers
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set profilesId
     *
     * @param integer $profilesId
     * 
     * @return GlpiUsers
     */
    public function setProfilesId($profilesId)
    {
        $this->profilesId = $profilesId;

        return $this;
    }

    /**
     * Get profilesId
     *
     * @return integer 
     */
    public function getProfilesId()
    {
        return $this->profilesId;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiUsers
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set usertitlesId
     *
     * @param integer $usertitlesId
     * 
     * @return GlpiUsers
     */
    public function setUsertitlesId($usertitlesId)
    {
        $this->usertitlesId = $usertitlesId;

        return $this;
    }

    /**
     * Get usertitlesId
     *
     * @return integer 
     */
    public function getUsertitlesId()
    {
        return $this->usertitlesId;
    }

    /**
     * Set usercategoriesId
     *
     * @param integer $usercategoriesId
     * 
     * @return GlpiUsers
     */
    public function setUsercategoriesId($usercategoriesId)
    {
        $this->usercategoriesId = $usercategoriesId;

        return $this;
    }

    /**
     * Get usercategoriesId
     *
     * @return integer 
     */
    public function getUsercategoriesId()
    {
        return $this->usercategoriesId;
    }

    /**
     * Set dateFormat
     *
     * @param integer $dateFormat
     * 
     * @return GlpiUsers
     */
    public function setDateFormat($dateFormat)
    {
        $this->dateFormat = $dateFormat;

        return $this;
    }

    /**
     * Get dateFormat
     *
     * @return integer 
     */
    public function getDateFormat()
    {
        return $this->dateFormat;
    }

    /**
     * Set numberFormat
     *
     * @param integer $numberFormat
     * 
     * @return GlpiUsers
     */
    public function setNumberFormat($numberFormat)
    {
        $this->numberFormat = $numberFormat;

        return $this;
    }

    /**
     * Get numberFormat
     *
     * @return integer 
     */
    public function getNumberFormat()
    {
        return $this->numberFormat;
    }

    /**
     * Set namesFormat
     *
     * @param integer $namesFormat
     * 
     * @return GlpiUsers
     */
    public function setNamesFormat($namesFormat)
    {
        $this->namesFormat = $namesFormat;

        return $this;
    }

    /**
     * Get namesFormat
     *
     * @return integer 
     */
    public function getNamesFormat()
    {
        return $this->namesFormat;
    }

    /**
     * Set csvDelimiter
     *
     * @param string $csvDelimiter
     * 
     * @return GlpiUsers
     */
    public function setCsvDelimiter($csvDelimiter)
    {
        $this->csvDelimiter = $csvDelimiter;

        return $this;
    }

    /**
     * Get csvDelimiter
     *
     * @return string 
     */
    public function getCsvDelimiter()
    {
        return $this->csvDelimiter;
    }

    /**
     * Set isIdsVisible
     *
     * @param boolean $isIdsVisible
     * 
     * @return GlpiUsers
     */
    public function setIsIdsVisible($isIdsVisible)
    {
        $this->isIdsVisible = $isIdsVisible;

        return $this;
    }

    /**
     * Get isIdsVisible
     *
     * @return boolean 
     */
    public function getIsIdsVisible()
    {
        return $this->isIdsVisible;
    }

    /**
     * Set dropdownCharsLimit
     *
     * @param integer $dropdownCharsLimit
     * 
     * @return GlpiUsers
     */
    public function setDropdownCharsLimit($dropdownCharsLimit)
    {
        $this->dropdownCharsLimit = $dropdownCharsLimit;

        return $this;
    }

    /**
     * Get dropdownCharsLimit
     *
     * @return integer 
     */
    public function getDropdownCharsLimit()
    {
        return $this->dropdownCharsLimit;
    }

    /**
     * Set useFlatDropdowntree
     *
     * @param boolean $useFlatDropdowntree
     * 
     * @return GlpiUsers
     */
    public function setUseFlatDropdowntree($useFlatDropdowntree)
    {
        $this->useFlatDropdowntree = $useFlatDropdowntree;

        return $this;
    }

    /**
     * Get useFlatDropdowntree
     *
     * @return boolean 
     */
    public function getUseFlatDropdowntree()
    {
        return $this->useFlatDropdowntree;
    }

    /**
     * Set showJobsAtLogin
     *
     * @param boolean $showJobsAtLogin
     * 
     * @return GlpiUsers
     */
    public function setShowJobsAtLogin($showJobsAtLogin)
    {
        $this->showJobsAtLogin = $showJobsAtLogin;

        return $this;
    }

    /**
     * Get showJobsAtLogin
     *
     * @return boolean 
     */
    public function getShowJobsAtLogin()
    {
        return $this->showJobsAtLogin;
    }

    /**
     * Set priority1
     *
     * @param string $priority1
     * 
     * @return GlpiUsers
     */
    public function setPriority1($priority1)
    {
        $this->priority1 = $priority1;

        return $this;
    }

    /**
     * Get priority1
     *
     * @return string 
     */
    public function getPriority1()
    {
        return $this->priority1;
    }

    /**
     * Set priority2
     *
     * @param string $priority2
     * 
     * @return GlpiUsers
     */
    public function setPriority2($priority2)
    {
        $this->priority2 = $priority2;

        return $this;
    }

    /**
     * Get priority2
     *
     * @return string 
     */
    public function getPriority2()
    {
        return $this->priority2;
    }

    /**
     * Set priority3
     *
     * @param string $priority3
     * 
     * @return GlpiUsers
     */
    public function setPriority3($priority3)
    {
        $this->priority3 = $priority3;

        return $this;
    }

    /**
     * Get priority3
     *
     * @return string 
     */
    public function getPriority3()
    {
        return $this->priority3;
    }

    /**
     * Set priority4
     *
     * @param string $priority4
     * 
     * @return GlpiUsers
     */
    public function setPriority4($priority4)
    {
        $this->priority4 = $priority4;

        return $this;
    }

    /**
     * Get priority4
     *
     * @return string 
     */
    public function getPriority4()
    {
        return $this->priority4;
    }

    /**
     * Set priority5
     *
     * @param string $priority5
     * 
     * @return GlpiUsers
     */
    public function setPriority5($priority5)
    {
        $this->priority5 = $priority5;

        return $this;
    }

    /**
     * Get priority5
     *
     * @return string 
     */
    public function getPriority5()
    {
        return $this->priority5;
    }

    /**
     * Set priority6
     *
     * @param string $priority6
     * 
     * @return GlpiUsers
     */
    public function setPriority6($priority6)
    {
        $this->priority6 = $priority6;

        return $this;
    }

    /**
     * Get priority6
     *
     * @return string 
     */
    public function getPriority6()
    {
        return $this->priority6;
    }

    /**
     * Set isCategorizedSoftExpanded
     *
     * @param boolean $isCategorizedSoftExpanded
     * 
     * @return GlpiUsers
     */
    public function setIsCategorizedSoftExpanded($isCategorizedSoftExpanded)
    {
        $this->isCategorizedSoftExpanded = $isCategorizedSoftExpanded;

        return $this;
    }

    /**
     * Get isCategorizedSoftExpanded
     *
     * @return boolean 
     */
    public function getIsCategorizedSoftExpanded()
    {
        return $this->isCategorizedSoftExpanded;
    }

    /**
     * Set isNotCategorizedSoftExpanded
     *
     * @param boolean $isNotCategorizedSoftExpanded
     * 
     * @return GlpiUsers
     */
    public function setIsNotCategorizedSoftExpanded($isNotCategorizedSoftExpanded)
    {
        $this->isNotCategorizedSoftExpanded = $isNotCategorizedSoftExpanded;

        return $this;
    }

    /**
     * Get isNotCategorizedSoftExpanded
     *
     * @return boolean 
     */
    public function getIsNotCategorizedSoftExpanded()
    {
        return $this->isNotCategorizedSoftExpanded;
    }

    /**
     * Set followupPrivate
     *
     * @param boolean $followupPrivate
     * 
     * @return GlpiUsers
     */
    public function setFollowupPrivate($followupPrivate)
    {
        $this->followupPrivate = $followupPrivate;

        return $this;
    }

    /**
     * Get followupPrivate
     *
     * @return boolean 
     */
    public function getFollowupPrivate()
    {
        return $this->followupPrivate;
    }

    /**
     * Set taskPrivate
     *
     * @param boolean $taskPrivate
     * 
     * @return GlpiUsers
     */
    public function setTaskPrivate($taskPrivate)
    {
        $this->taskPrivate = $taskPrivate;

        return $this;
    }

    /**
     * Get taskPrivate
     *
     * @return boolean 
     */
    public function getTaskPrivate()
    {
        return $this->taskPrivate;
    }

    /**
     * Set defaultRequesttypesId
     *
     * @param integer $defaultRequesttypesId
     * 
     * @return GlpiUsers
     */
    public function setDefaultRequesttypesId($defaultRequesttypesId)
    {
        $this->defaultRequesttypesId = $defaultRequesttypesId;

        return $this;
    }

    /**
     * Get defaultRequesttypesId
     *
     * @return integer 
     */
    public function getDefaultRequesttypesId()
    {
        return $this->defaultRequesttypesId;
    }

    /**
     * Set passwordForgetToken
     *
     * @param string $passwordForgetToken
     * 
     * @return GlpiUsers
     */
    public function setPasswordForgetToken($passwordForgetToken)
    {
        $this->passwordForgetToken = $passwordForgetToken;

        return $this;
    }

    /**
     * Get passwordForgetToken
     *
     * @return string 
     */
    public function getPasswordForgetToken()
    {
        return $this->passwordForgetToken;
    }

    /**
     * Set passwordForgetTokenDate
     *
     * @param \DateTime $passwordForgetTokenDate
     * 
     * @return GlpiUsers
     */
    public function setPasswordForgetTokenDate($passwordForgetTokenDate)
    {
        $this->passwordForgetTokenDate = $passwordForgetTokenDate;

        return $this;
    }

    /**
     * Get passwordForgetTokenDate
     *
     * @return \DateTime 
     */
    public function getPasswordForgetTokenDate()
    {
        return $this->passwordForgetTokenDate;
    }

    /**
     * Set userDn
     *
     * @param string $userDn
     * 
     * @return GlpiUsers
     */
    public function setUserDn($userDn)
    {
        $this->userDn = $userDn;

        return $this;
    }

    /**
     * Get userDn
     *
     * @return string 
     */
    public function getUserDn()
    {
        return $this->userDn;
    }

    /**
     * Set registrationNumber
     *
     * @param string $registrationNumber
     * 
     * @return GlpiUsers
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    /**
     * Get registrationNumber
     *
     * @return string 
     */
    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    /**
     * Set showCountOnTabs
     *
     * @param boolean $showCountOnTabs
     * 
     * @return GlpiUsers
     */
    public function setShowCountOnTabs($showCountOnTabs)
    {
        $this->showCountOnTabs = $showCountOnTabs;

        return $this;
    }

    /**
     * Get showCountOnTabs
     *
     * @return boolean 
     */
    public function getShowCountOnTabs()
    {
        return $this->showCountOnTabs;
    }

    /**
     * Set refreshTicketList
     *
     * @param integer $refreshTicketList
     * 
     * @return GlpiUsers
     */
    public function setRefreshTicketList($refreshTicketList)
    {
        $this->refreshTicketList = $refreshTicketList;

        return $this;
    }

    /**
     * Get refreshTicketList
     *
     * @return integer 
     */
    public function getRefreshTicketList()
    {
        return $this->refreshTicketList;
    }

    /**
     * Set setDefaultTech
     *
     * @param boolean $setDefaultTech
     * 
     * @return GlpiUsers
     */
    public function setSetDefaultTech($setDefaultTech)
    {
        $this->setDefaultTech = $setDefaultTech;

        return $this;
    }

    /**
     * Get setDefaultTech
     *
     * @return boolean 
     */
    public function getSetDefaultTech()
    {
        return $this->setDefaultTech;
    }

    /**
     * Set personalToken
     *
     * @param string $personalToken
     * 
     * @return GlpiUsers
     */
    public function setPersonalToken($personalToken)
    {
        $this->personalToken = $personalToken;

        return $this;
    }

    /**
     * Get personalToken
     *
     * @return string 
     */
    public function getPersonalToken()
    {
        return $this->personalToken;
    }

    /**
     * Set personalTokenDate
     *
     * @param \DateTime $personalTokenDate
     * 
     * @return GlpiUsers
     */
    public function setPersonalTokenDate($personalTokenDate)
    {
        $this->personalTokenDate = $personalTokenDate;

        return $this;
    }

    /**
     * Get personalTokenDate
     *
     * @return \DateTime 
     */
    public function getPersonalTokenDate()
    {
        return $this->personalTokenDate;
    }

    /**
     * Set displayCountOnHome
     *
     * @param integer $displayCountOnHome
     * 
     * @return GlpiUsers
     */
    public function setDisplayCountOnHome($displayCountOnHome)
    {
        $this->displayCountOnHome = $displayCountOnHome;

        return $this;
    }

    /**
     * Get displayCountOnHome
     *
     * @return integer 
     */
    public function getDisplayCountOnHome()
    {
        return $this->displayCountOnHome;
    }
}