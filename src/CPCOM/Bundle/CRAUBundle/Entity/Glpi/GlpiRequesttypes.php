<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiRequesttypes
 *
 * @ORM\Table(name="glpi_requesttypes")
 * @ORM\Entity
 */
class GlpiRequesttypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_helpdesk_default", type="boolean", nullable=false)
     */
    private $isHelpdeskDefault;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_mail_default", type="boolean", nullable=false)
     */
    private $isMailDefault;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiRequesttypes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isHelpdeskDefault
     *
     * @param boolean $isHelpdeskDefault
     * 
     * @return GlpiRequesttypes
     */
    public function setIsHelpdeskDefault($isHelpdeskDefault)
    {
        $this->isHelpdeskDefault = $isHelpdeskDefault;

        return $this;
    }

    /**
     * Get isHelpdeskDefault
     *
     * @return boolean 
     */
    public function getIsHelpdeskDefault()
    {
        return $this->isHelpdeskDefault;
    }

    /**
     * Set isMailDefault
     *
     * @param boolean $isMailDefault
     * 
     * @return GlpiRequesttypes
     */
    public function setIsMailDefault($isMailDefault)
    {
        $this->isMailDefault = $isMailDefault;

        return $this;
    }

    /**
     * Get isMailDefault
     *
     * @return boolean 
     */
    public function getIsMailDefault()
    {
        return $this->isMailDefault;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiRequesttypes
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}