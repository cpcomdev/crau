<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicedrives
 *
 * @ORM\Table(name="glpi_computers_devicedrives")
 * @ORM\Entity
 */
class GlpiComputersDevicedrives
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicedrives_id", type="integer", nullable=false)
     */
    private $devicedrivesId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiComputersDevicedrives
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set devicedrivesId
     *
     * @param integer $devicedrivesId
     * 
     * @return GlpiComputersDevicedrives
     */
    public function setDevicedrivesId($devicedrivesId)
    {
        $this->devicedrivesId = $devicedrivesId;

        return $this;
    }

    /**
     * Get devicedrivesId
     *
     * @return integer 
     */
    public function getDevicedrivesId()
    {
        return $this->devicedrivesId;
    }
}