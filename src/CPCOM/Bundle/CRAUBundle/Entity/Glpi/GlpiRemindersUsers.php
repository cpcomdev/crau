<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiRemindersUsers
 *
 * @ORM\Table(name="glpi_reminders_users")
 * @ORM\Entity
 */
class GlpiRemindersUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="reminders_id", type="integer", nullable=false)
     */
    private $remindersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remindersId
     *
     * @param integer $remindersId
     * 
     * @return GlpiRemindersUsers
     */
    public function setRemindersId($remindersId)
    {
        $this->remindersId = $remindersId;

        return $this;
    }

    /**
     * Get remindersId
     *
     * @return integer 
     */
    public function getRemindersId()
    {
        return $this->remindersId;
    }

    /**
     * Set usersId
     *
     * @param integer $usersId
     * 
     * @return GlpiRemindersUsers
     */
    public function setUsersId($usersId)
    {
        $this->usersId = $usersId;

        return $this;
    }

    /**
     * Get usersId
     *
     * @return integer 
     */
    public function getUsersId()
    {
        return $this->usersId;
    }
}