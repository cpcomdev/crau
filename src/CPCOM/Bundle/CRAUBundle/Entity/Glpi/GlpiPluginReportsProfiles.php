<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginReportsProfiles
 *
 * @ORM\Table(name="glpi_plugin_reports_profiles")
 * @ORM\Entity
 */
class GlpiPluginReportsProfiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="profiles_id", type="integer", nullable=false)
     */
    private $profilesId;

    /**
     * @var string
     *
     * @ORM\Column(name="report", type="string", length=255, nullable=true)
     */
    private $report;

    /**
     * @var string
     *
     * @ORM\Column(name="access", type="string", length=1, nullable=true)
     */
    private $access;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profilesId
     *
     * @param integer $profilesId
     * 
     * @return GlpiPluginReportsProfiles
     */
    public function setProfilesId($profilesId)
    {
        $this->profilesId = $profilesId;

        return $this;
    }

    /**
     * Get profilesId
     *
     * @return integer 
     */
    public function getProfilesId()
    {
        return $this->profilesId;
    }

    /**
     * Set report
     *
     * @param string $report
     * 
     * @return GlpiPluginReportsProfiles
     */
    public function setReport($report)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return string 
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set access
     *
     * @param string $access
     * 
     * @return GlpiPluginReportsProfiles
     */
    public function setAccess($access)
    {
        $this->access = $access;

        return $this;
    }

    /**
     * Get access
     *
     * @return string 
     */
    public function getAccess()
    {
        return $this->access;
    }
}