<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiTicketrecurrents
 *
 * @ORM\Table(name="glpi_ticketrecurrents")
 * @ORM\Entity
 */
class GlpiTicketrecurrents
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickettemplates_id", type="integer", nullable=false)
     */
    private $tickettemplatesId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin_date", type="datetime", nullable=true)
     */
    private $beginDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="periodicity", type="integer", nullable=false)
     */
    private $periodicity;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_before", type="integer", nullable=false)
     */
    private $createBefore;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="next_creation_date", type="datetime", nullable=true)
     */
    private $nextCreationDate;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiTicketrecurrents
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiTicketrecurrents
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiTicketrecurrents
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set isRecursive
     *
     * @param boolean $isRecursive
     * 
     * @return GlpiTicketrecurrents
     */
    public function setIsRecursive($isRecursive)
    {
        $this->isRecursive = $isRecursive;

        return $this;
    }

    /**
     * Get isRecursive
     *
     * @return boolean 
     */
    public function getIsRecursive()
    {
        return $this->isRecursive;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * 
     * @return GlpiTicketrecurrents
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set tickettemplatesId
     *
     * @param integer $tickettemplatesId
     * 
     * @return GlpiTicketrecurrents
     */
    public function setTickettemplatesId($tickettemplatesId)
    {
        $this->tickettemplatesId = $tickettemplatesId;

        return $this;
    }

    /**
     * Get tickettemplatesId
     *
     * @return integer 
     */
    public function getTickettemplatesId()
    {
        return $this->tickettemplatesId;
    }

    /**
     * Set beginDate
     *
     * @param \DateTime $beginDate
     * 
     * @return GlpiTicketrecurrents
     */
    public function setBeginDate($beginDate)
    {
        $this->beginDate = $beginDate;

        return $this;
    }

    /**
     * Get beginDate
     *
     * @return \DateTime 
     */
    public function getBeginDate()
    {
        return $this->beginDate;
    }

    /**
     * Set periodicity
     *
     * @param integer $periodicity
     * 
     * @return GlpiTicketrecurrents
     */
    public function setPeriodicity($periodicity)
    {
        $this->periodicity = $periodicity;

        return $this;
    }

    /**
     * Get periodicity
     *
     * @return integer 
     */
    public function getPeriodicity()
    {
        return $this->periodicity;
    }

    /**
     * Set createBefore
     *
     * @param integer $createBefore
     * 
     * @return GlpiTicketrecurrents
     */
    public function setCreateBefore($createBefore)
    {
        $this->createBefore = $createBefore;

        return $this;
    }

    /**
     * Get createBefore
     *
     * @return integer 
     */
    public function getCreateBefore()
    {
        return $this->createBefore;
    }

    /**
     * Set nextCreationDate
     *
     * @param \DateTime $nextCreationDate
     * 
     * @return GlpiTicketrecurrents
     */
    public function setNextCreationDate($nextCreationDate)
    {
        $this->nextCreationDate = $nextCreationDate;

        return $this;
    }

    /**
     * Get nextCreationDate
     *
     * @return \DateTime 
     */
    public function getNextCreationDate()
    {
        return $this->nextCreationDate;
    }
}