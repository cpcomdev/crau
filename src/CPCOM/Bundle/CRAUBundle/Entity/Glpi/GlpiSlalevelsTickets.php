<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiSlalevelsTickets
 *
 * @ORM\Table(name="glpi_slalevels_tickets")
 * @ORM\Entity
 */
class GlpiSlalevelsTickets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickets_id", type="integer", nullable=false)
     */
    private $ticketsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="slalevels_id", type="integer", nullable=false)
     */
    private $slalevelsId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ticketsId
     *
     * @param integer $ticketsId
     * 
     * @return GlpiSlalevelsTickets
     */
    public function setTicketsId($ticketsId)
    {
        $this->ticketsId = $ticketsId;

        return $this;
    }

    /**
     * Get ticketsId
     *
     * @return integer 
     */
    public function getTicketsId()
    {
        return $this->ticketsId;
    }

    /**
     * Set slalevelsId
     *
     * @param integer $slalevelsId
     * 
     * @return GlpiSlalevelsTickets
     */
    public function setSlalevelsId($slalevelsId)
    {
        $this->slalevelsId = $slalevelsId;

        return $this;
    }

    /**
     * Get slalevelsId
     *
     * @return integer 
     */
    public function getSlalevelsId()
    {
        return $this->slalevelsId;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * 
     * @return GlpiSlalevelsTickets
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
}