<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderConfigs
 *
 * @ORM\Table(name="glpi_plugin_order_configs")
 * @ORM\Entity
 */
class GlpiPluginOrderConfigs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_validation", type="boolean", nullable=false)
     */
    private $useValidation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_supplier_satisfaction", type="boolean", nullable=false)
     */
    private $useSupplierSatisfaction;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_supplier_informations", type="boolean", nullable=false)
     */
    private $useSupplierInformations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_supplier_infos", type="boolean", nullable=false)
     */
    private $useSupplierInfos;

    /**
     * @var boolean
     *
     * @ORM\Column(name="generate_order_pdf", type="boolean", nullable=false)
     */
    private $generateOrderPdf;

    /**
     * @var boolean
     *
     * @ORM\Column(name="copy_documents", type="boolean", nullable=false)
     */
    private $copyDocuments;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_taxes", type="integer", nullable=false)
     */
    private $defaultTaxes;

    /**
     * @var integer
     *
     * @ORM\Column(name="generate_assets", type="integer", nullable=false)
     */
    private $generateAssets;

    /**
     * @var string
     *
     * @ORM\Column(name="generated_name", type="string", length=255, nullable=true)
     */
    private $generatedName;

    /**
     * @var string
     *
     * @ORM\Column(name="generated_serial", type="string", length=255, nullable=true)
     */
    private $generatedSerial;

    /**
     * @var string
     *
     * @ORM\Column(name="generated_otherserial", type="string", length=255, nullable=true)
     */
    private $generatedOtherserial;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_asset_states_id", type="integer", nullable=false)
     */
    private $defaultAssetStatesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="generate_ticket", type="integer", nullable=false)
     */
    private $generateTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="generated_title", type="string", length=255, nullable=true)
     */
    private $generatedTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="generated_content", type="text", nullable=true)
     */
    private $generatedContent;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_ticketcategories_id", type="integer", nullable=false)
     */
    private $defaultTicketcategoriesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_status_draft", type="integer", nullable=false)
     */
    private $orderStatusDraft;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_status_waiting_approval", type="integer", nullable=false)
     */
    private $orderStatusWaitingApproval;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_status_approved", type="integer", nullable=false)
     */
    private $orderStatusApproved;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_status_partially_delivred", type="integer", nullable=false)
     */
    private $orderStatusPartiallyDelivred;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_status_completly_delivered", type="integer", nullable=false)
     */
    private $orderStatusCompletlyDelivered;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_status_canceled", type="integer", nullable=false)
     */
    private $orderStatusCanceled;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_status_paid", type="integer", nullable=false)
     */
    private $orderStatusPaid;

    /**
     * @var string
     *
     * @ORM\Column(name="shoudbedelivered_color", type="string", length=20, nullable=true)
     */
    private $shoudbedeliveredColor;

    /**
     * @var integer
     *
     * @ORM\Column(name="documentcategories_id", type="integer", nullable=false)
     */
    private $documentcategoriesId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set useValidation
     *
     * @param boolean $useValidation
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setUseValidation($useValidation)
    {
        $this->useValidation = $useValidation;

        return $this;
    }

    /**
     * Get useValidation
     *
     * @return boolean 
     */
    public function getUseValidation()
    {
        return $this->useValidation;
    }

    /**
     * Set useSupplierSatisfaction
     *
     * @param boolean $useSupplierSatisfaction
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setUseSupplierSatisfaction($useSupplierSatisfaction)
    {
        $this->useSupplierSatisfaction = $useSupplierSatisfaction;

        return $this;
    }

    /**
     * Get useSupplierSatisfaction
     *
     * @return boolean 
     */
    public function getUseSupplierSatisfaction()
    {
        return $this->useSupplierSatisfaction;
    }

    /**
     * Set useSupplierInformations
     *
     * @param boolean $useSupplierInformations
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setUseSupplierInformations($useSupplierInformations)
    {
        $this->useSupplierInformations = $useSupplierInformations;

        return $this;
    }

    /**
     * Get useSupplierInformations
     *
     * @return boolean 
     */
    public function getUseSupplierInformations()
    {
        return $this->useSupplierInformations;
    }

    /**
     * Set useSupplierInfos
     *
     * @param boolean $useSupplierInfos
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setUseSupplierInfos($useSupplierInfos)
    {
        $this->useSupplierInfos = $useSupplierInfos;

        return $this;
    }

    /**
     * Get useSupplierInfos
     *
     * @return boolean 
     */
    public function getUseSupplierInfos()
    {
        return $this->useSupplierInfos;
    }

    /**
     * Set generateOrderPdf
     *
     * @param boolean $generateOrderPdf
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setGenerateOrderPdf($generateOrderPdf)
    {
        $this->generateOrderPdf = $generateOrderPdf;

        return $this;
    }

    /**
     * Get generateOrderPdf
     *
     * @return boolean 
     */
    public function getGenerateOrderPdf()
    {
        return $this->generateOrderPdf;
    }

    /**
     * Set copyDocuments
     *
     * @param boolean $copyDocuments
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setCopyDocuments($copyDocuments)
    {
        $this->copyDocuments = $copyDocuments;

        return $this;
    }

    /**
     * Get copyDocuments
     *
     * @return boolean 
     */
    public function getCopyDocuments()
    {
        return $this->copyDocuments;
    }

    /**
     * Set defaultTaxes
     *
     * @param integer $defaultTaxes
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setDefaultTaxes($defaultTaxes)
    {
        $this->defaultTaxes = $defaultTaxes;

        return $this;
    }

    /**
     * Get defaultTaxes
     *
     * @return integer 
     */
    public function getDefaultTaxes()
    {
        return $this->defaultTaxes;
    }

    /**
     * Set generateAssets
     *
     * @param integer $generateAssets
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setGenerateAssets($generateAssets)
    {
        $this->generateAssets = $generateAssets;

        return $this;
    }

    /**
     * Get generateAssets
     *
     * @return integer 
     */
    public function getGenerateAssets()
    {
        return $this->generateAssets;
    }

    /**
     * Set generatedName
     *
     * @param string $generatedName
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setGeneratedName($generatedName)
    {
        $this->generatedName = $generatedName;

        return $this;
    }

    /**
     * Get generatedName
     *
     * @return string 
     */
    public function getGeneratedName()
    {
        return $this->generatedName;
    }

    /**
     * Set generatedSerial
     *
     * @param string $generatedSerial
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setGeneratedSerial($generatedSerial)
    {
        $this->generatedSerial = $generatedSerial;

        return $this;
    }

    /**
     * Get generatedSerial
     *
     * @return string 
     */
    public function getGeneratedSerial()
    {
        return $this->generatedSerial;
    }

    /**
     * Set generatedOtherserial
     *
     * @param string $generatedOtherserial
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setGeneratedOtherserial($generatedOtherserial)
    {
        $this->generatedOtherserial = $generatedOtherserial;

        return $this;
    }

    /**
     * Get generatedOtherserial
     *
     * @return string 
     */
    public function getGeneratedOtherserial()
    {
        return $this->generatedOtherserial;
    }

    /**
     * Set defaultAssetStatesId
     *
     * @param integer $defaultAssetStatesId
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setDefaultAssetStatesId($defaultAssetStatesId)
    {
        $this->defaultAssetStatesId = $defaultAssetStatesId;

        return $this;
    }

    /**
     * Get defaultAssetStatesId
     *
     * @return integer 
     */
    public function getDefaultAssetStatesId()
    {
        return $this->defaultAssetStatesId;
    }

    /**
     * Set generateTicket
     *
     * @param integer $generateTicket
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setGenerateTicket($generateTicket)
    {
        $this->generateTicket = $generateTicket;

        return $this;
    }

    /**
     * Get generateTicket
     *
     * @return integer 
     */
    public function getGenerateTicket()
    {
        return $this->generateTicket;
    }

    /**
     * Set generatedTitle
     *
     * @param string $generatedTitle
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setGeneratedTitle($generatedTitle)
    {
        $this->generatedTitle = $generatedTitle;

        return $this;
    }

    /**
     * Get generatedTitle
     *
     * @return string 
     */
    public function getGeneratedTitle()
    {
        return $this->generatedTitle;
    }

    /**
     * Set generatedContent
     *
     * @param string $generatedContent
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setGeneratedContent($generatedContent)
    {
        $this->generatedContent = $generatedContent;

        return $this;
    }

    /**
     * Get generatedContent
     *
     * @return string 
     */
    public function getGeneratedContent()
    {
        return $this->generatedContent;
    }

    /**
     * Set defaultTicketcategoriesId
     *
     * @param integer $defaultTicketcategoriesId
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setDefaultTicketcategoriesId($defaultTicketcategoriesId)
    {
        $this->defaultTicketcategoriesId = $defaultTicketcategoriesId;

        return $this;
    }

    /**
     * Get defaultTicketcategoriesId
     *
     * @return integer 
     */
    public function getDefaultTicketcategoriesId()
    {
        return $this->defaultTicketcategoriesId;
    }

    /**
     * Set orderStatusDraft
     *
     * @param integer $orderStatusDraft
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setOrderStatusDraft($orderStatusDraft)
    {
        $this->orderStatusDraft = $orderStatusDraft;

        return $this;
    }

    /**
     * Get orderStatusDraft
     *
     * @return integer 
     */
    public function getOrderStatusDraft()
    {
        return $this->orderStatusDraft;
    }

    /**
     * Set orderStatusWaitingApproval
     *
     * @param integer $orderStatusWaitingApproval
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setOrderStatusWaitingApproval($orderStatusWaitingApproval)
    {
        $this->orderStatusWaitingApproval = $orderStatusWaitingApproval;

        return $this;
    }

    /**
     * Get orderStatusWaitingApproval
     *
     * @return integer 
     */
    public function getOrderStatusWaitingApproval()
    {
        return $this->orderStatusWaitingApproval;
    }

    /**
     * Set orderStatusApproved
     *
     * @param integer $orderStatusApproved
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setOrderStatusApproved($orderStatusApproved)
    {
        $this->orderStatusApproved = $orderStatusApproved;

        return $this;
    }

    /**
     * Get orderStatusApproved
     *
     * @return integer 
     */
    public function getOrderStatusApproved()
    {
        return $this->orderStatusApproved;
    }

    /**
     * Set orderStatusPartiallyDelivred
     *
     * @param integer $orderStatusPartiallyDelivred
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setOrderStatusPartiallyDelivred($orderStatusPartiallyDelivred)
    {
        $this->orderStatusPartiallyDelivred = $orderStatusPartiallyDelivred;

        return $this;
    }

    /**
     * Get orderStatusPartiallyDelivred
     *
     * @return integer 
     */
    public function getOrderStatusPartiallyDelivred()
    {
        return $this->orderStatusPartiallyDelivred;
    }

    /**
     * Set orderStatusCompletlyDelivered
     *
     * @param integer $orderStatusCompletlyDelivered
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setOrderStatusCompletlyDelivered($orderStatusCompletlyDelivered)
    {
        $this->orderStatusCompletlyDelivered = $orderStatusCompletlyDelivered;

        return $this;
    }

    /**
     * Get orderStatusCompletlyDelivered
     *
     * @return integer 
     */
    public function getOrderStatusCompletlyDelivered()
    {
        return $this->orderStatusCompletlyDelivered;
    }

    /**
     * Set orderStatusCanceled
     *
     * @param integer $orderStatusCanceled
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setOrderStatusCanceled($orderStatusCanceled)
    {
        $this->orderStatusCanceled = $orderStatusCanceled;

        return $this;
    }

    /**
     * Get orderStatusCanceled
     *
     * @return integer 
     */
    public function getOrderStatusCanceled()
    {
        return $this->orderStatusCanceled;
    }

    /**
     * Set orderStatusPaid
     *
     * @param integer $orderStatusPaid
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setOrderStatusPaid($orderStatusPaid)
    {
        $this->orderStatusPaid = $orderStatusPaid;

        return $this;
    }

    /**
     * Get orderStatusPaid
     *
     * @return integer 
     */
    public function getOrderStatusPaid()
    {
        return $this->orderStatusPaid;
    }

    /**
     * Set shoudbedeliveredColor
     *
     * @param string $shoudbedeliveredColor
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setShoudbedeliveredColor($shoudbedeliveredColor)
    {
        $this->shoudbedeliveredColor = $shoudbedeliveredColor;

        return $this;
    }

    /**
     * Get shoudbedeliveredColor
     *
     * @return string 
     */
    public function getShoudbedeliveredColor()
    {
        return $this->shoudbedeliveredColor;
    }

    /**
     * Set documentcategoriesId
     *
     * @param integer $documentcategoriesId
     * 
     * @return GlpiPluginOrderConfigs
     */
    public function setDocumentcategoriesId($documentcategoriesId)
    {
        $this->documentcategoriesId = $documentcategoriesId;

        return $this;
    }

    /**
     * Get documentcategoriesId
     *
     * @return integer 
     */
    public function getDocumentcategoriesId()
    {
        return $this->documentcategoriesId;
    }
}