<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicemotherboards
 *
 * @ORM\Table(name="glpi_computers_devicemotherboards")
 * @ORM\Entity
 */
class GlpiComputersDevicemotherboards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicemotherboards_id", type="integer", nullable=false)
     */
    private $devicemotherboardsId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiComputersDevicemotherboards
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set devicemotherboardsId
     *
     * @param integer $devicemotherboardsId
     * 
     * @return GlpiComputersDevicemotherboards
     */
    public function setDevicemotherboardsId($devicemotherboardsId)
    {
        $this->devicemotherboardsId = $devicemotherboardsId;

        return $this;
    }

    /**
     * Get devicemotherboardsId
     *
     * @return integer 
     */
    public function getDevicemotherboardsId()
    {
        return $this->devicemotherboardsId;
    }
}