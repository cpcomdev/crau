<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiTickets
 *
 * @ORM\Table(name="glpi_tickets")
 * @ORM\Entity
 */
class GlpiTickets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="closedate", type="datetime", nullable=true)
     */
    private $closedate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="solvedate", type="datetime", nullable=true)
     */
    private $solvedate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id_lastupdater", type="integer", nullable=false)
     */
    private $usersIdLastupdater;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id_recipient", type="integer", nullable=false)
     */
    private $usersIdRecipient;

    /**
     * @var integer
     *
     * @ORM\Column(name="requesttypes_id", type="integer", nullable=false)
     */
    private $requesttypesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="suppliers_id_assign", type="integer", nullable=false)
     */
    private $suppliersIdAssign;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=100, nullable=false)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="items_id", type="integer", nullable=false)
     */
    private $itemsId;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="urgency", type="integer", nullable=false)
     */
    private $urgency;

    /**
     * @var integer
     *
     * @ORM\Column(name="impact", type="integer", nullable=false)
     */
    private $impact;

    /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="integer", nullable=false)
     */
    private $priority;

    /**
     * @var integer
     *
     * @ORM\Column(name="itilcategories_id", type="integer", nullable=false)
     */
    private $itilcategoriesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="cost_time", type="decimal", nullable=false)
     */
    private $costTime;

    /**
     * @var float
     *
     * @ORM\Column(name="cost_fixed", type="decimal", nullable=false)
     */
    private $costFixed;

    /**
     * @var float
     *
     * @ORM\Column(name="cost_material", type="decimal", nullable=false)
     */
    private $costMaterial;

    /**
     * @var integer
     *
     * @ORM\Column(name="solutiontypes_id", type="integer", nullable=false)
     */
    private $solutiontypesId;

    /**
     * @var string
     *
     * @ORM\Column(name="solution", type="text", nullable=true)
     */
    private $solution;

    /**
     * @var string
     *
     * @ORM\Column(name="global_validation", type="string", length=255, nullable=true)
     */
    private $globalValidation;

    /**
     * @var integer
     *
     * @ORM\Column(name="slas_id", type="integer", nullable=false)
     */
    private $slasId;

    /**
     * @var integer
     *
     * @ORM\Column(name="slalevels_id", type="integer", nullable=false)
     */
    private $slalevelsId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="due_date", type="datetime", nullable=true)
     */
    private $dueDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin_waiting_date", type="datetime", nullable=true)
     */
    private $beginWaitingDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="sla_waiting_duration", type="integer", nullable=false)
     */
    private $slaWaitingDuration;

    /**
     * @var integer
     *
     * @ORM\Column(name="waiting_duration", type="integer", nullable=false)
     */
    private $waitingDuration;

    /**
     * @var integer
     *
     * @ORM\Column(name="close_delay_stat", type="integer", nullable=false)
     */
    private $closeDelayStat;

    /**
     * @var integer
     *
     * @ORM\Column(name="solve_delay_stat", type="integer", nullable=false)
     */
    private $solveDelayStat;

    /**
     * @var integer
     *
     * @ORM\Column(name="takeintoaccount_delay_stat", type="integer", nullable=false)
     */
    private $takeintoaccountDelayStat;

    /**
     * @var integer
     *
     * @ORM\Column(name="actiontime", type="integer", nullable=false)
     */
    private $actiontime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiTickets
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiTickets
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * 
     * @return GlpiTickets
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set closedate
     *
     * @param \DateTime $closedate
     * 
     * @return GlpiTickets
     */
    public function setClosedate($closedate)
    {
        $this->closedate = $closedate;

        return $this;
    }

    /**
     * Get closedate
     *
     * @return \DateTime 
     */
    public function getClosedate()
    {
        return $this->closedate;
    }

    /**
     * Set solvedate
     *
     * @param \DateTime $solvedate
     * 
     * @return GlpiTickets
     */
    public function setSolvedate($solvedate)
    {
        $this->solvedate = $solvedate;

        return $this;
    }

    /**
     * Get solvedate
     *
     * @return \DateTime 
     */
    public function getSolvedate()
    {
        return $this->solvedate;
    }

    /**
     * Set dateMod
     *
     * @param \DateTime $dateMod
     * 
     * @return GlpiTickets
     */
    public function setDateMod($dateMod)
    {
        $this->dateMod = $dateMod;

        return $this;
    }

    /**
     * Get dateMod
     *
     * @return \DateTime 
     */
    public function getDateMod()
    {
        return $this->dateMod;
    }

    /**
     * Set usersIdLastupdater
     *
     * @param integer $usersIdLastupdater
     * 
     * @return GlpiTickets
     */
    public function setUsersIdLastupdater($usersIdLastupdater)
    {
        $this->usersIdLastupdater = $usersIdLastupdater;

        return $this;
    }

    /**
     * Get usersIdLastupdater
     *
     * @return integer 
     */
    public function getUsersIdLastupdater()
    {
        return $this->usersIdLastupdater;
    }

    /**
     * Set status
     *
     * @param string $status
     * 
     * @return GlpiTickets
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set usersIdRecipient
     *
     * @param integer $usersIdRecipient
     * 
     * @return GlpiTickets
     */
    public function setUsersIdRecipient($usersIdRecipient)
    {
        $this->usersIdRecipient = $usersIdRecipient;

        return $this;
    }

    /**
     * Get usersIdRecipient
     *
     * @return integer 
     */
    public function getUsersIdRecipient()
    {
        return $this->usersIdRecipient;
    }

    /**
     * Set requesttypesId
     *
     * @param integer $requesttypesId
     * 
     * @return GlpiTickets
     */
    public function setRequesttypesId($requesttypesId)
    {
        $this->requesttypesId = $requesttypesId;

        return $this;
    }

    /**
     * Get requesttypesId
     *
     * @return integer 
     */
    public function getRequesttypesId()
    {
        return $this->requesttypesId;
    }

    /**
     * Set suppliersIdAssign
     *
     * @param integer $suppliersIdAssign
     * 
     * @return GlpiTickets
     */
    public function setSuppliersIdAssign($suppliersIdAssign)
    {
        $this->suppliersIdAssign = $suppliersIdAssign;

        return $this;
    }

    /**
     * Get suppliersIdAssign
     *
     * @return integer 
     */
    public function getSuppliersIdAssign()
    {
        return $this->suppliersIdAssign;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     * 
     * @return GlpiTickets
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string 
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set itemsId
     *
     * @param integer $itemsId
     * 
     * @return GlpiTickets
     */
    public function setItemsId($itemsId)
    {
        $this->itemsId = $itemsId;

        return $this;
    }

    /**
     * Get itemsId
     *
     * @return integer 
     */
    public function getItemsId()
    {
        return $this->itemsId;
    }

    /**
     * Set content
     *
     * @param string $content
     * 
     * @return GlpiTickets
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set urgency
     *
     * @param integer $urgency
     * 
     * @return GlpiTickets
     */
    public function setUrgency($urgency)
    {
        $this->urgency = $urgency;

        return $this;
    }

    /**
     * Get urgency
     *
     * @return integer 
     */
    public function getUrgency()
    {
        return $this->urgency;
    }

    /**
     * Set impact
     *
     * @param integer $impact
     * 
     * @return GlpiTickets
     */
    public function setImpact($impact)
    {
        $this->impact = $impact;

        return $this;
    }

    /**
     * Get impact
     *
     * @return integer 
     */
    public function getImpact()
    {
        return $this->impact;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     * 
     * @return GlpiTickets
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set itilcategoriesId
     *
     * @param integer $itilcategoriesId
     * 
     * @return GlpiTickets
     */
    public function setItilcategoriesId($itilcategoriesId)
    {
        $this->itilcategoriesId = $itilcategoriesId;

        return $this;
    }

    /**
     * Get itilcategoriesId
     *
     * @return integer 
     */
    public function getItilcategoriesId()
    {
        return $this->itilcategoriesId;
    }

    /**
     * Set type
     *
     * @param integer $type
     * 
     * @return GlpiTickets
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set costTime
     *
     * @param float $costTime
     * 
     * @return GlpiTickets
     */
    public function setCostTime($costTime)
    {
        $this->costTime = $costTime;

        return $this;
    }

    /**
     * Get costTime
     *
     * @return float 
     */
    public function getCostTime()
    {
        return $this->costTime;
    }

    /**
     * Set costFixed
     *
     * @param float $costFixed
     * 
     * @return GlpiTickets
     */
    public function setCostFixed($costFixed)
    {
        $this->costFixed = $costFixed;

        return $this;
    }

    /**
     * Get costFixed
     *
     * @return float 
     */
    public function getCostFixed()
    {
        return $this->costFixed;
    }

    /**
     * Set costMaterial
     *
     * @param float $costMaterial
     * 
     * @return GlpiTickets
     */
    public function setCostMaterial($costMaterial)
    {
        $this->costMaterial = $costMaterial;

        return $this;
    }

    /**
     * Get costMaterial
     *
     * @return float 
     */
    public function getCostMaterial()
    {
        return $this->costMaterial;
    }

    /**
     * Set solutiontypesId
     *
     * @param integer $solutiontypesId
     * 
     * @return GlpiTickets
     */
    public function setSolutiontypesId($solutiontypesId)
    {
        $this->solutiontypesId = $solutiontypesId;

        return $this;
    }

    /**
     * Get solutiontypesId
     *
     * @return integer 
     */
    public function getSolutiontypesId()
    {
        return $this->solutiontypesId;
    }

    /**
     * Set solution
     *
     * @param string $solution
     * 
     * @return GlpiTickets
     */
    public function setSolution($solution)
    {
        $this->solution = $solution;

        return $this;
    }

    /**
     * Get solution
     *
     * @return string 
     */
    public function getSolution()
    {
        return $this->solution;
    }

    /**
     * Set globalValidation
     *
     * @param string $globalValidation
     * 
     * @return GlpiTickets
     */
    public function setGlobalValidation($globalValidation)
    {
        $this->globalValidation = $globalValidation;

        return $this;
    }

    /**
     * Get globalValidation
     *
     * @return string 
     */
    public function getGlobalValidation()
    {
        return $this->globalValidation;
    }

    /**
     * Set slasId
     *
     * @param integer $slasId
     * 
     * @return GlpiTickets
     */
    public function setSlasId($slasId)
    {
        $this->slasId = $slasId;

        return $this;
    }

    /**
     * Get slasId
     *
     * @return integer 
     */
    public function getSlasId()
    {
        return $this->slasId;
    }

    /**
     * Set slalevelsId
     *
     * @param integer $slalevelsId
     * 
     * @return GlpiTickets
     */
    public function setSlalevelsId($slalevelsId)
    {
        $this->slalevelsId = $slalevelsId;

        return $this;
    }

    /**
     * Get slalevelsId
     *
     * @return integer 
     */
    public function getSlalevelsId()
    {
        return $this->slalevelsId;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     * 
     * @return GlpiTickets
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime 
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set beginWaitingDate
     *
     * @param \DateTime $beginWaitingDate
     * 
     * @return GlpiTickets
     */
    public function setBeginWaitingDate($beginWaitingDate)
    {
        $this->beginWaitingDate = $beginWaitingDate;

        return $this;
    }

    /**
     * Get beginWaitingDate
     *
     * @return \DateTime 
     */
    public function getBeginWaitingDate()
    {
        return $this->beginWaitingDate;
    }

    /**
     * Set slaWaitingDuration
     *
     * @param integer $slaWaitingDuration
     * 
     * @return GlpiTickets
     */
    public function setSlaWaitingDuration($slaWaitingDuration)
    {
        $this->slaWaitingDuration = $slaWaitingDuration;

        return $this;
    }

    /**
     * Get slaWaitingDuration
     *
     * @return integer 
     */
    public function getSlaWaitingDuration()
    {
        return $this->slaWaitingDuration;
    }

    /**
     * Set waitingDuration
     *
     * @param integer $waitingDuration
     * 
     * @return GlpiTickets
     */
    public function setWaitingDuration($waitingDuration)
    {
        $this->waitingDuration = $waitingDuration;

        return $this;
    }

    /**
     * Get waitingDuration
     *
     * @return integer 
     */
    public function getWaitingDuration()
    {
        return $this->waitingDuration;
    }

    /**
     * Set closeDelayStat
     *
     * @param integer $closeDelayStat
     * 
     * @return GlpiTickets
     */
    public function setCloseDelayStat($closeDelayStat)
    {
        $this->closeDelayStat = $closeDelayStat;

        return $this;
    }

    /**
     * Get closeDelayStat
     *
     * @return integer 
     */
    public function getCloseDelayStat()
    {
        return $this->closeDelayStat;
    }

    /**
     * Set solveDelayStat
     *
     * @param integer $solveDelayStat
     * 
     * @return GlpiTickets
     */
    public function setSolveDelayStat($solveDelayStat)
    {
        $this->solveDelayStat = $solveDelayStat;

        return $this;
    }

    /**
     * Get solveDelayStat
     *
     * @return integer 
     */
    public function getSolveDelayStat()
    {
        return $this->solveDelayStat;
    }

    /**
     * Set takeintoaccountDelayStat
     *
     * @param integer $takeintoaccountDelayStat
     * 
     * @return GlpiTickets
     */
    public function setTakeintoaccountDelayStat($takeintoaccountDelayStat)
    {
        $this->takeintoaccountDelayStat = $takeintoaccountDelayStat;

        return $this;
    }

    /**
     * Get takeintoaccountDelayStat
     *
     * @return integer 
     */
    public function getTakeintoaccountDelayStat()
    {
        return $this->takeintoaccountDelayStat;
    }

    /**
     * Set actiontime
     *
     * @param integer $actiontime
     * 
     * @return GlpiTickets
     */
    public function setActiontime($actiontime)
    {
        $this->actiontime = $actiontime;

        return $this;
    }

    /**
     * Get actiontime
     *
     * @return integer 
     */
    public function getActiontime()
    {
        return $this->actiontime;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * 
     * @return GlpiTickets
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }
}