<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiDevicememories
 *
 * @ORM\Table(name="glpi_devicememories")
 * @ORM\Entity
 */
class GlpiDevicememories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="frequence", type="string", length=255, nullable=true)
     */
    private $frequence;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="specif_default", type="integer", nullable=false)
     */
    private $specifDefault;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicememorytypes_id", type="integer", nullable=false)
     */
    private $devicememorytypesId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     * 
     * @return GlpiDevicememories
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string 
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set frequence
     *
     * @param string $frequence
     * 
     * @return GlpiDevicememories
     */
    public function setFrequence($frequence)
    {
        $this->frequence = $frequence;

        return $this;
    }

    /**
     * Get frequence
     *
     * @return string 
     */
    public function getFrequence()
    {
        return $this->frequence;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiDevicememories
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set manufacturersId
     *
     * @param integer $manufacturersId
     * 
     * @return GlpiDevicememories
     */
    public function setManufacturersId($manufacturersId)
    {
        $this->manufacturersId = $manufacturersId;

        return $this;
    }

    /**
     * Get manufacturersId
     *
     * @return integer 
     */
    public function getManufacturersId()
    {
        return $this->manufacturersId;
    }

    /**
     * Set specifDefault
     *
     * @param integer $specifDefault
     * 
     * @return GlpiDevicememories
     */
    public function setSpecifDefault($specifDefault)
    {
        $this->specifDefault = $specifDefault;

        return $this;
    }

    /**
     * Get specifDefault
     *
     * @return integer 
     */
    public function getSpecifDefault()
    {
        return $this->specifDefault;
    }

    /**
     * Set devicememorytypesId
     *
     * @param integer $devicememorytypesId
     * 
     * @return GlpiDevicememories
     */
    public function setDevicememorytypesId($devicememorytypesId)
    {
        $this->devicememorytypesId = $devicememorytypesId;

        return $this;
    }

    /**
     * Get devicememorytypesId
     *
     * @return integer 
     */
    public function getDevicememorytypesId()
    {
        return $this->devicememorytypesId;
    }
}