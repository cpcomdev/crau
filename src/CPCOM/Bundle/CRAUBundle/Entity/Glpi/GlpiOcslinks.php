<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiOcslinks
 *
 * @ORM\Table(name="glpi_ocslinks")
 * @ORM\Entity
 */
class GlpiOcslinks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsid", type="integer", nullable=false)
     */
    private $ocsid;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_deviceid", type="string", length=255, nullable=true)
     */
    private $ocsDeviceid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_auto_update", type="boolean", nullable=false)
     */
    private $useAutoUpdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_ocs_update", type="datetime", nullable=true)
     */
    private $lastOcsUpdate;

    /**
     * @var string
     *
     * @ORM\Column(name="computer_update", type="text", nullable=true)
     */
    private $computerUpdate;

    /**
     * @var string
     *
     * @ORM\Column(name="import_device", type="text", nullable=true)
     */
    private $importDevice;

    /**
     * @var string
     *
     * @ORM\Column(name="import_disk", type="text", nullable=true)
     */
    private $importDisk;

    /**
     * @var string
     *
     * @ORM\Column(name="import_software", type="text", nullable=true)
     */
    private $importSoftware;

    /**
     * @var string
     *
     * @ORM\Column(name="import_monitor", type="text", nullable=true)
     */
    private $importMonitor;

    /**
     * @var string
     *
     * @ORM\Column(name="import_peripheral", type="text", nullable=true)
     */
    private $importPeripheral;

    /**
     * @var string
     *
     * @ORM\Column(name="import_printer", type="text", nullable=true)
     */
    private $importPrinter;

    /**
     * @var integer
     *
     * @ORM\Column(name="ocsservers_id", type="integer", nullable=false)
     */
    private $ocsserversId;

    /**
     * @var string
     *
     * @ORM\Column(name="import_ip", type="text", nullable=true)
     */
    private $importIp;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_agent_version", type="string", length=255, nullable=true)
     */
    private $ocsAgentVersion;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255, nullable=true)
     */
    private $tag;

    /**
     * @var string
     *
     * @ORM\Column(name="import_vm", type="text", nullable=true)
     */
    private $importVm;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiOcslinks
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set ocsid
     *
     * @param integer $ocsid
     * 
     * @return GlpiOcslinks
     */
    public function setOcsid($ocsid)
    {
        $this->ocsid = $ocsid;

        return $this;
    }

    /**
     * Get ocsid
     *
     * @return integer 
     */
    public function getOcsid()
    {
        return $this->ocsid;
    }

    /**
     * Set ocsDeviceid
     *
     * @param string $ocsDeviceid
     * 
     * @return GlpiOcslinks
     */
    public function setOcsDeviceid($ocsDeviceid)
    {
        $this->ocsDeviceid = $ocsDeviceid;

        return $this;
    }

    /**
     * Get ocsDeviceid
     *
     * @return string 
     */
    public function getOcsDeviceid()
    {
        return $this->ocsDeviceid;
    }

    /**
     * Set useAutoUpdate
     *
     * @param boolean $useAutoUpdate
     * 
     * @return GlpiOcslinks
     */
    public function setUseAutoUpdate($useAutoUpdate)
    {
        $this->useAutoUpdate = $useAutoUpdate;

        return $this;
    }

    /**
     * Get useAutoUpdate
     *
     * @return boolean 
     */
    public function getUseAutoUpdate()
    {
        return $this->useAutoUpdate;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     * 
     * @return GlpiOcslinks
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime 
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set lastOcsUpdate
     *
     * @param \DateTime $lastOcsUpdate
     * 
     * @return GlpiOcslinks
     */
    public function setLastOcsUpdate($lastOcsUpdate)
    {
        $this->lastOcsUpdate = $lastOcsUpdate;

        return $this;
    }

    /**
     * Get lastOcsUpdate
     *
     * @return \DateTime 
     */
    public function getLastOcsUpdate()
    {
        return $this->lastOcsUpdate;
    }

    /**
     * Set computerUpdate
     *
     * @param string $computerUpdate
     * 
     * @return GlpiOcslinks
     */
    public function setComputerUpdate($computerUpdate)
    {
        $this->computerUpdate = $computerUpdate;

        return $this;
    }

    /**
     * Get computerUpdate
     *
     * @return string 
     */
    public function getComputerUpdate()
    {
        return $this->computerUpdate;
    }

    /**
     * Set importDevice
     *
     * @param string $importDevice
     * 
     * @return GlpiOcslinks
     */
    public function setImportDevice($importDevice)
    {
        $this->importDevice = $importDevice;

        return $this;
    }

    /**
     * Get importDevice
     *
     * @return string 
     */
    public function getImportDevice()
    {
        return $this->importDevice;
    }

    /**
     * Set importDisk
     *
     * @param string $importDisk
     * 
     * @return GlpiOcslinks
     */
    public function setImportDisk($importDisk)
    {
        $this->importDisk = $importDisk;

        return $this;
    }

    /**
     * Get importDisk
     *
     * @return string 
     */
    public function getImportDisk()
    {
        return $this->importDisk;
    }

    /**
     * Set importSoftware
     *
     * @param string $importSoftware
     * 
     * @return GlpiOcslinks
     */
    public function setImportSoftware($importSoftware)
    {
        $this->importSoftware = $importSoftware;

        return $this;
    }

    /**
     * Get importSoftware
     *
     * @return string 
     */
    public function getImportSoftware()
    {
        return $this->importSoftware;
    }

    /**
     * Set importMonitor
     *
     * @param string $importMonitor
     * 
     * @return GlpiOcslinks
     */
    public function setImportMonitor($importMonitor)
    {
        $this->importMonitor = $importMonitor;

        return $this;
    }

    /**
     * Get importMonitor
     *
     * @return string 
     */
    public function getImportMonitor()
    {
        return $this->importMonitor;
    }

    /**
     * Set importPeripheral
     *
     * @param string $importPeripheral
     * 
     * @return GlpiOcslinks
     */
    public function setImportPeripheral($importPeripheral)
    {
        $this->importPeripheral = $importPeripheral;

        return $this;
    }

    /**
     * Get importPeripheral
     *
     * @return string 
     */
    public function getImportPeripheral()
    {
        return $this->importPeripheral;
    }

    /**
     * Set importPrinter
     *
     * @param string $importPrinter
     * 
     * @return GlpiOcslinks
     */
    public function setImportPrinter($importPrinter)
    {
        $this->importPrinter = $importPrinter;

        return $this;
    }

    /**
     * Get importPrinter
     *
     * @return string 
     */
    public function getImportPrinter()
    {
        return $this->importPrinter;
    }

    /**
     * Set ocsserversId
     *
     * @param integer $ocsserversId
     * 
     * @return GlpiOcslinks
     */
    public function setOcsserversId($ocsserversId)
    {
        $this->ocsserversId = $ocsserversId;

        return $this;
    }

    /**
     * Get ocsserversId
     *
     * @return integer 
     */
    public function getOcsserversId()
    {
        return $this->ocsserversId;
    }

    /**
     * Set importIp
     *
     * @param string $importIp
     * 
     * @return GlpiOcslinks
     */
    public function setImportIp($importIp)
    {
        $this->importIp = $importIp;

        return $this;
    }

    /**
     * Get importIp
     *
     * @return string 
     */
    public function getImportIp()
    {
        return $this->importIp;
    }

    /**
     * Set ocsAgentVersion
     *
     * @param string $ocsAgentVersion
     * 
     * @return GlpiOcslinks
     */
    public function setOcsAgentVersion($ocsAgentVersion)
    {
        $this->ocsAgentVersion = $ocsAgentVersion;

        return $this;
    }

    /**
     * Get ocsAgentVersion
     *
     * @return string 
     */
    public function getOcsAgentVersion()
    {
        return $this->ocsAgentVersion;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiOcslinks
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * 
     * @return GlpiOcslinks
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set importVm
     *
     * @param string $importVm
     * 
     * @return GlpiOcslinks
     */
    public function setImportVm($importVm)
    {
        $this->importVm = $importVm;

        return $this;
    }

    /**
     * Get importVm
     *
     * @return string 
     */
    public function getImportVm()
    {
        return $this->importVm;
    }
}