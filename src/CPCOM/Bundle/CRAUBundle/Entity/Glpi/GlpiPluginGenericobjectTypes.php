<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginGenericobjectTypes
 *
 * @ORM\Table(name="glpi_plugin_genericobject_types")
 * @ORM\Entity
 */
class GlpiPluginGenericobjectTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255, nullable=true)
     */
    private $itemtype;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_unicity", type="boolean", nullable=false)
     */
    private $useUnicity;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_history", type="boolean", nullable=false)
     */
    private $useHistory;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_infocoms", type="boolean", nullable=false)
     */
    private $useInfocoms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_contracts", type="boolean", nullable=false)
     */
    private $useContracts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_documents", type="boolean", nullable=false)
     */
    private $useDocuments;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_tickets", type="boolean", nullable=false)
     */
    private $useTickets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_links", type="boolean", nullable=false)
     */
    private $useLinks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_loans", type="boolean", nullable=false)
     */
    private $useLoans;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_network_ports", type="boolean", nullable=false)
     */
    private $useNetworkPorts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_direct_connections", type="boolean", nullable=false)
     */
    private $useDirectConnections;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_plugin_datainjection", type="boolean", nullable=false)
     */
    private $usePluginDatainjection;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_plugin_pdf", type="boolean", nullable=false)
     */
    private $usePluginPdf;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_plugin_order", type="boolean", nullable=false)
     */
    private $usePluginOrder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_plugin_uninstall", type="boolean", nullable=false)
     */
    private $usePluginUninstall;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_plugin_geninventorynumber", type="boolean", nullable=false)
     */
    private $usePluginGeninventorynumber;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_menu_entry", type="boolean", nullable=false)
     */
    private $useMenuEntry;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string 
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set useUnicity
     *
     * @param boolean $useUnicity
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUseUnicity($useUnicity)
    {
        $this->useUnicity = $useUnicity;

        return $this;
    }

    /**
     * Get useUnicity
     *
     * @return boolean 
     */
    public function getUseUnicity()
    {
        return $this->useUnicity;
    }

    /**
     * Set useHistory
     *
     * @param boolean $useHistory
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUseHistory($useHistory)
    {
        $this->useHistory = $useHistory;

        return $this;
    }

    /**
     * Get useHistory
     *
     * @return boolean 
     */
    public function getUseHistory()
    {
        return $this->useHistory;
    }

    /**
     * Set useInfocoms
     *
     * @param boolean $useInfocoms
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUseInfocoms($useInfocoms)
    {
        $this->useInfocoms = $useInfocoms;

        return $this;
    }

    /**
     * Get useInfocoms
     *
     * @return boolean 
     */
    public function getUseInfocoms()
    {
        return $this->useInfocoms;
    }

    /**
     * Set useContracts
     *
     * @param boolean $useContracts
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUseContracts($useContracts)
    {
        $this->useContracts = $useContracts;

        return $this;
    }

    /**
     * Get useContracts
     *
     * @return boolean 
     */
    public function getUseContracts()
    {
        return $this->useContracts;
    }

    /**
     * Set useDocuments
     *
     * @param boolean $useDocuments
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUseDocuments($useDocuments)
    {
        $this->useDocuments = $useDocuments;

        return $this;
    }

    /**
     * Get useDocuments
     *
     * @return boolean 
     */
    public function getUseDocuments()
    {
        return $this->useDocuments;
    }

    /**
     * Set useTickets
     *
     * @param boolean $useTickets
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUseTickets($useTickets)
    {
        $this->useTickets = $useTickets;

        return $this;
    }

    /**
     * Get useTickets
     *
     * @return boolean 
     */
    public function getUseTickets()
    {
        return $this->useTickets;
    }

    /**
     * Set useLinks
     *
     * @param boolean $useLinks
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUseLinks($useLinks)
    {
        $this->useLinks = $useLinks;

        return $this;
    }

    /**
     * Get useLinks
     *
     * @return boolean 
     */
    public function getUseLinks()
    {
        return $this->useLinks;
    }

    /**
     * Set useLoans
     *
     * @param boolean $useLoans
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUseLoans($useLoans)
    {
        $this->useLoans = $useLoans;

        return $this;
    }

    /**
     * Get useLoans
     *
     * @return boolean 
     */
    public function getUseLoans()
    {
        return $this->useLoans;
    }

    /**
     * Set useNetworkPorts
     *
     * @param boolean $useNetworkPorts
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUseNetworkPorts($useNetworkPorts)
    {
        $this->useNetworkPorts = $useNetworkPorts;

        return $this;
    }

    /**
     * Get useNetworkPorts
     *
     * @return boolean 
     */
    public function getUseNetworkPorts()
    {
        return $this->useNetworkPorts;
    }

    /**
     * Set useDirectConnections
     *
     * @param boolean $useDirectConnections
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUseDirectConnections($useDirectConnections)
    {
        $this->useDirectConnections = $useDirectConnections;

        return $this;
    }

    /**
     * Get useDirectConnections
     *
     * @return boolean 
     */
    public function getUseDirectConnections()
    {
        return $this->useDirectConnections;
    }

    /**
     * Set usePluginDatainjection
     *
     * @param boolean $usePluginDatainjection
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUsePluginDatainjection($usePluginDatainjection)
    {
        $this->usePluginDatainjection = $usePluginDatainjection;

        return $this;
    }

    /**
     * Get usePluginDatainjection
     *
     * @return boolean 
     */
    public function getUsePluginDatainjection()
    {
        return $this->usePluginDatainjection;
    }

    /**
     * Set usePluginPdf
     *
     * @param boolean $usePluginPdf
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUsePluginPdf($usePluginPdf)
    {
        $this->usePluginPdf = $usePluginPdf;

        return $this;
    }

    /**
     * Get usePluginPdf
     *
     * @return boolean 
     */
    public function getUsePluginPdf()
    {
        return $this->usePluginPdf;
    }

    /**
     * Set usePluginOrder
     *
     * @param boolean $usePluginOrder
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUsePluginOrder($usePluginOrder)
    {
        $this->usePluginOrder = $usePluginOrder;

        return $this;
    }

    /**
     * Get usePluginOrder
     *
     * @return boolean 
     */
    public function getUsePluginOrder()
    {
        return $this->usePluginOrder;
    }

    /**
     * Set usePluginUninstall
     *
     * @param boolean $usePluginUninstall
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUsePluginUninstall($usePluginUninstall)
    {
        $this->usePluginUninstall = $usePluginUninstall;

        return $this;
    }

    /**
     * Get usePluginUninstall
     *
     * @return boolean 
     */
    public function getUsePluginUninstall()
    {
        return $this->usePluginUninstall;
    }

    /**
     * Set usePluginGeninventorynumber
     *
     * @param boolean $usePluginGeninventorynumber
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUsePluginGeninventorynumber($usePluginGeninventorynumber)
    {
        $this->usePluginGeninventorynumber = $usePluginGeninventorynumber;

        return $this;
    }

    /**
     * Get usePluginGeninventorynumber
     *
     * @return boolean 
     */
    public function getUsePluginGeninventorynumber()
    {
        return $this->usePluginGeninventorynumber;
    }

    /**
     * Set useMenuEntry
     *
     * @param boolean $useMenuEntry
     * 
     * @return GlpiPluginGenericobjectTypes
     */
    public function setUseMenuEntry($useMenuEntry)
    {
        $this->useMenuEntry = $useMenuEntry;

        return $this;
    }

    /**
     * Get useMenuEntry
     *
     * @return boolean 
     */
    public function getUseMenuEntry()
    {
        return $this->useMenuEntry;
    }
}