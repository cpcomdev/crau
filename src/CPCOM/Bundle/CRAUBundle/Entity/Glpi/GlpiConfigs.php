<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiConfigs
 *
 * @ORM\Table(name="glpi_configs")
 * @ORM\Entity
 */
class GlpiConfigs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_jobs_at_login", type="boolean", nullable=false)
     */
    private $showJobsAtLogin;

    /**
     * @var integer
     *
     * @ORM\Column(name="cut", type="integer", nullable=false)
     */
    private $cut;

    /**
     * @var integer
     *
     * @ORM\Column(name="list_limit", type="integer", nullable=false)
     */
    private $listLimit;

    /**
     * @var integer
     *
     * @ORM\Column(name="list_limit_max", type="integer", nullable=false)
     */
    private $listLimitMax;

    /**
     * @var integer
     *
     * @ORM\Column(name="url_maxlength", type="integer", nullable=false)
     */
    private $urlMaxlength;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=10, nullable=true)
     */
    private $version;

    /**
     * @var integer
     *
     * @ORM\Column(name="event_loglevel", type="integer", nullable=false)
     */
    private $eventLoglevel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_mailing", type="boolean", nullable=false)
     */
    private $useMailing;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_email", type="string", length=255, nullable=true)
     */
    private $adminEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_email_name", type="string", length=255, nullable=true)
     */
    private $adminEmailName;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_reply", type="string", length=255, nullable=true)
     */
    private $adminReply;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_reply_name", type="string", length=255, nullable=true)
     */
    private $adminReplyName;

    /**
     * @var string
     *
     * @ORM\Column(name="mailing_signature", type="text", nullable=true)
     */
    private $mailingSignature;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_anonymous_helpdesk", type="boolean", nullable=false)
     */
    private $useAnonymousHelpdesk;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_anonymous_followups", type="boolean", nullable=false)
     */
    private $useAnonymousFollowups;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=10, nullable=true)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_1", type="string", length=20, nullable=true)
     */
    private $priority1;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_2", type="string", length=20, nullable=true)
     */
    private $priority2;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_3", type="string", length=20, nullable=true)
     */
    private $priority3;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_4", type="string", length=20, nullable=true)
     */
    private $priority4;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_5", type="string", length=20, nullable=true)
     */
    private $priority5;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_6", type="string", length=20, nullable=false)
     */
    private $priority6;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_tax", type="date", nullable=false)
     */
    private $dateTax;

    /**
     * @var string
     *
     * @ORM\Column(name="cas_host", type="string", length=255, nullable=true)
     */
    private $casHost;

    /**
     * @var integer
     *
     * @ORM\Column(name="cas_port", type="integer", nullable=false)
     */
    private $casPort;

    /**
     * @var string
     *
     * @ORM\Column(name="cas_uri", type="string", length=255, nullable=true)
     */
    private $casUri;

    /**
     * @var string
     *
     * @ORM\Column(name="cas_logout", type="string", length=255, nullable=true)
     */
    private $casLogout;

    /**
     * @var integer
     *
     * @ORM\Column(name="authldaps_id_extra", type="integer", nullable=false)
     */
    private $authldapsIdExtra;

    /**
     * @var string
     *
     * @ORM\Column(name="existing_auth_server_field", type="string", length=255, nullable=true)
     */
    private $existingAuthServerField;

    /**
     * @var boolean
     *
     * @ORM\Column(name="existing_auth_server_field_clean_domain", type="boolean", nullable=false)
     */
    private $existingAuthServerFieldCleanDomain;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="planning_begin", type="time", nullable=false)
     */
    private $planningBegin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="planning_end", type="time", nullable=false)
     */
    private $planningEnd;

    /**
     * @var integer
     *
     * @ORM\Column(name="utf8_conv", type="integer", nullable=false)
     */
    private $utf8Conv;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_public_faq", type="boolean", nullable=false)
     */
    private $usePublicFaq;

    /**
     * @var string
     *
     * @ORM\Column(name="url_base", type="string", length=255, nullable=true)
     */
    private $urlBase;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_link_in_mail", type="boolean", nullable=false)
     */
    private $showLinkInMail;

    /**
     * @var string
     *
     * @ORM\Column(name="text_login", type="text", nullable=true)
     */
    private $textLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="founded_new_version", type="string", length=10, nullable=true)
     */
    private $foundedNewVersion;

    /**
     * @var integer
     *
     * @ORM\Column(name="dropdown_max", type="integer", nullable=false)
     */
    private $dropdownMax;

    /**
     * @var string
     *
     * @ORM\Column(name="ajax_wildcard", type="string", length=1, nullable=true)
     */
    private $ajaxWildcard;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_ajax", type="boolean", nullable=false)
     */
    private $useAjax;

    /**
     * @var integer
     *
     * @ORM\Column(name="ajax_min_textsearch_load", type="integer", nullable=false)
     */
    private $ajaxMinTextsearchLoad;

    /**
     * @var integer
     *
     * @ORM\Column(name="ajax_limit_count", type="integer", nullable=false)
     */
    private $ajaxLimitCount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_ajax_autocompletion", type="boolean", nullable=false)
     */
    private $useAjaxAutocompletion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_users_auto_add", type="boolean", nullable=false)
     */
    private $isUsersAutoAdd;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_format", type="integer", nullable=false)
     */
    private $dateFormat;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_format", type="integer", nullable=false)
     */
    private $numberFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="csv_delimiter", type="string", length=1, nullable=false)
     */
    private $csvDelimiter;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_ids_visible", type="boolean", nullable=false)
     */
    private $isIdsVisible;

    /**
     * @var integer
     *
     * @ORM\Column(name="dropdown_chars_limit", type="integer", nullable=false)
     */
    private $dropdownCharsLimit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_ocs_mode", type="boolean", nullable=false)
     */
    private $useOcsMode;

    /**
     * @var integer
     *
     * @ORM\Column(name="smtp_mode", type="integer", nullable=false)
     */
    private $smtpMode;

    /**
     * @var string
     *
     * @ORM\Column(name="smtp_host", type="string", length=255, nullable=true)
     */
    private $smtpHost;

    /**
     * @var integer
     *
     * @ORM\Column(name="smtp_port", type="integer", nullable=false)
     */
    private $smtpPort;

    /**
     * @var string
     *
     * @ORM\Column(name="smtp_username", type="string", length=255, nullable=true)
     */
    private $smtpUsername;

    /**
     * @var string
     *
     * @ORM\Column(name="proxy_name", type="string", length=255, nullable=true)
     */
    private $proxyName;

    /**
     * @var integer
     *
     * @ORM\Column(name="proxy_port", type="integer", nullable=false)
     */
    private $proxyPort;

    /**
     * @var string
     *
     * @ORM\Column(name="proxy_user", type="string", length=255, nullable=true)
     */
    private $proxyUser;

    /**
     * @var boolean
     *
     * @ORM\Column(name="add_followup_on_update_ticket", type="boolean", nullable=false)
     */
    private $addFollowupOnUpdateTicket;

    /**
     * @var boolean
     *
     * @ORM\Column(name="keep_tickets_on_delete", type="boolean", nullable=false)
     */
    private $keepTicketsOnDelete;

    /**
     * @var integer
     *
     * @ORM\Column(name="time_step", type="integer", nullable=true)
     */
    private $timeStep;

    /**
     * @var integer
     *
     * @ORM\Column(name="decimal_number", type="integer", nullable=true)
     */
    private $decimalNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="helpdesk_doc_url", type="string", length=255, nullable=true)
     */
    private $helpdeskDocUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="central_doc_url", type="string", length=255, nullable=true)
     */
    private $centralDocUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="documentcategories_id_forticket", type="integer", nullable=false)
     */
    private $documentcategoriesIdForticket;

    /**
     * @var integer
     *
     * @ORM\Column(name="monitors_management_restrict", type="integer", nullable=false)
     */
    private $monitorsManagementRestrict;

    /**
     * @var integer
     *
     * @ORM\Column(name="phones_management_restrict", type="integer", nullable=false)
     */
    private $phonesManagementRestrict;

    /**
     * @var integer
     *
     * @ORM\Column(name="peripherals_management_restrict", type="integer", nullable=false)
     */
    private $peripheralsManagementRestrict;

    /**
     * @var integer
     *
     * @ORM\Column(name="printers_management_restrict", type="integer", nullable=false)
     */
    private $printersManagementRestrict;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_log_in_files", type="boolean", nullable=false)
     */
    private $useLogInFiles;

    /**
     * @var integer
     *
     * @ORM\Column(name="time_offset", type="integer", nullable=false)
     */
    private $timeOffset;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_contact_autoupdate", type="boolean", nullable=false)
     */
    private $isContactAutoupdate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_user_autoupdate", type="boolean", nullable=false)
     */
    private $isUserAutoupdate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_group_autoupdate", type="boolean", nullable=false)
     */
    private $isGroupAutoupdate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_location_autoupdate", type="boolean", nullable=false)
     */
    private $isLocationAutoupdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="state_autoupdate_mode", type="integer", nullable=false)
     */
    private $stateAutoupdateMode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_contact_autoclean", type="boolean", nullable=false)
     */
    private $isContactAutoclean;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_user_autoclean", type="boolean", nullable=false)
     */
    private $isUserAutoclean;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_group_autoclean", type="boolean", nullable=false)
     */
    private $isGroupAutoclean;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_location_autoclean", type="boolean", nullable=false)
     */
    private $isLocationAutoclean;

    /**
     * @var integer
     *
     * @ORM\Column(name="state_autoclean_mode", type="integer", nullable=false)
     */
    private $stateAutocleanMode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_flat_dropdowntree", type="boolean", nullable=false)
     */
    private $useFlatDropdowntree;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_autoname_by_entity", type="boolean", nullable=false)
     */
    private $useAutonameByEntity;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_categorized_soft_expanded", type="boolean", nullable=false)
     */
    private $isCategorizedSoftExpanded;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_not_categorized_soft_expanded", type="boolean", nullable=false)
     */
    private $isNotCategorizedSoftExpanded;

    /**
     * @var integer
     *
     * @ORM\Column(name="softwarecategories_id_ondelete", type="integer", nullable=false)
     */
    private $softwarecategoriesIdOndelete;

    /**
     * @var string
     *
     * @ORM\Column(name="x509_email_field", type="string", length=255, nullable=true)
     */
    private $x509EmailField;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_mailcollector_filesize_max", type="integer", nullable=false)
     */
    private $defaultMailcollectorFilesizeMax;

    /**
     * @var boolean
     *
     * @ORM\Column(name="followup_private", type="boolean", nullable=false)
     */
    private $followupPrivate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="task_private", type="boolean", nullable=false)
     */
    private $taskPrivate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="default_software_helpdesk_visible", type="boolean", nullable=false)
     */
    private $defaultSoftwareHelpdeskVisible;

    /**
     * @var integer
     *
     * @ORM\Column(name="names_format", type="integer", nullable=false)
     */
    private $namesFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="default_graphtype", type="string", length=3, nullable=false)
     */
    private $defaultGraphtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_requesttypes_id", type="integer", nullable=false)
     */
    private $defaultRequesttypesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_noright_users_add", type="boolean", nullable=false)
     */
    private $useNorightUsersAdd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cron_limit", type="boolean", nullable=false)
     */
    private $cronLimit;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_matrix", type="string", length=255, nullable=true)
     */
    private $priorityMatrix;

    /**
     * @var integer
     *
     * @ORM\Column(name="urgency_mask", type="integer", nullable=false)
     */
    private $urgencyMask;

    /**
     * @var integer
     *
     * @ORM\Column(name="impact_mask", type="integer", nullable=false)
     */
    private $impactMask;

    /**
     * @var boolean
     *
     * @ORM\Column(name="user_deleted_ldap", type="boolean", nullable=false)
     */
    private $userDeletedLdap;

    /**
     * @var boolean
     *
     * @ORM\Column(name="auto_create_infocoms", type="boolean", nullable=false)
     */
    private $autoCreateInfocoms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_slave_for_search", type="boolean", nullable=false)
     */
    private $useSlaveForSearch;

    /**
     * @var string
     *
     * @ORM\Column(name="proxy_passwd", type="string", length=255, nullable=true)
     */
    private $proxyPasswd;

    /**
     * @var string
     *
     * @ORM\Column(name="smtp_passwd", type="string", length=255, nullable=true)
     */
    private $smtpPasswd;

    /**
     * @var integer
     *
     * @ORM\Column(name="transfers_id_auto", type="integer", nullable=false)
     */
    private $transfersIdAuto;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_count_on_tabs", type="boolean", nullable=false)
     */
    private $showCountOnTabs;

    /**
     * @var integer
     *
     * @ORM\Column(name="refresh_ticket_list", type="integer", nullable=false)
     */
    private $refreshTicketList;

    /**
     * @var boolean
     *
     * @ORM\Column(name="set_default_tech", type="boolean", nullable=false)
     */
    private $setDefaultTech;

    /**
     * @var integer
     *
     * @ORM\Column(name="allow_search_view", type="integer", nullable=false)
     */
    private $allowSearchView;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_search_all", type="boolean", nullable=false)
     */
    private $allowSearchAll;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_search_global", type="boolean", nullable=false)
     */
    private $allowSearchGlobal;

    /**
     * @var integer
     *
     * @ORM\Column(name="display_count_on_home", type="integer", nullable=false)
     */
    private $displayCountOnHome;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set showJobsAtLogin
     *
     * @param boolean $showJobsAtLogin
     * 
     * @return GlpiConfigs
     */
    public function setShowJobsAtLogin($showJobsAtLogin)
    {
        $this->showJobsAtLogin = $showJobsAtLogin;

        return $this;
    }

    /**
     * Get showJobsAtLogin
     *
     * @return boolean 
     */
    public function getShowJobsAtLogin()
    {
        return $this->showJobsAtLogin;
    }

    /**
     * Set cut
     *
     * @param integer $cut
     * 
     * @return GlpiConfigs
     */
    public function setCut($cut)
    {
        $this->cut = $cut;

        return $this;
    }

    /**
     * Get cut
     *
     * @return integer 
     */
    public function getCut()
    {
        return $this->cut;
    }

    /**
     * Set listLimit
     *
     * @param integer $listLimit
     * 
     * @return GlpiConfigs
     */
    public function setListLimit($listLimit)
    {
        $this->listLimit = $listLimit;

        return $this;
    }

    /**
     * Get listLimit
     *
     * @return integer 
     */
    public function getListLimit()
    {
        return $this->listLimit;
    }

    /**
     * Set listLimitMax
     *
     * @param integer $listLimitMax
     * 
     * @return GlpiConfigs
     */
    public function setListLimitMax($listLimitMax)
    {
        $this->listLimitMax = $listLimitMax;

        return $this;
    }

    /**
     * Get listLimitMax
     *
     * @return integer 
     */
    public function getListLimitMax()
    {
        return $this->listLimitMax;
    }

    /**
     * Set urlMaxlength
     *
     * @param integer $urlMaxlength
     * 
     * @return GlpiConfigs
     */
    public function setUrlMaxlength($urlMaxlength)
    {
        $this->urlMaxlength = $urlMaxlength;

        return $this;
    }

    /**
     * Get urlMaxlength
     *
     * @return integer 
     */
    public function getUrlMaxlength()
    {
        return $this->urlMaxlength;
    }

    /**
     * Set version
     *
     * @param string $version
     * 
     * @return GlpiConfigs
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string 
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set eventLoglevel
     *
     * @param integer $eventLoglevel
     * 
     * @return GlpiConfigs
     */
    public function setEventLoglevel($eventLoglevel)
    {
        $this->eventLoglevel = $eventLoglevel;

        return $this;
    }

    /**
     * Get eventLoglevel
     *
     * @return integer 
     */
    public function getEventLoglevel()
    {
        return $this->eventLoglevel;
    }

    /**
     * Set useMailing
     *
     * @param boolean $useMailing
     * 
     * @return GlpiConfigs
     */
    public function setUseMailing($useMailing)
    {
        $this->useMailing = $useMailing;

        return $this;
    }

    /**
     * Get useMailing
     *
     * @return boolean 
     */
    public function getUseMailing()
    {
        return $this->useMailing;
    }

    /**
     * Set adminEmail
     *
     * @param string $adminEmail
     * 
     * @return GlpiConfigs
     */
    public function setAdminEmail($adminEmail)
    {
        $this->adminEmail = $adminEmail;

        return $this;
    }

    /**
     * Get adminEmail
     *
     * @return string 
     */
    public function getAdminEmail()
    {
        return $this->adminEmail;
    }

    /**
     * Set adminEmailName
     *
     * @param string $adminEmailName
     * 
     * @return GlpiConfigs
     */
    public function setAdminEmailName($adminEmailName)
    {
        $this->adminEmailName = $adminEmailName;

        return $this;
    }

    /**
     * Get adminEmailName
     *
     * @return string 
     */
    public function getAdminEmailName()
    {
        return $this->adminEmailName;
    }

    /**
     * Set adminReply
     *
     * @param string $adminReply
     * 
     * @return GlpiConfigs
     */
    public function setAdminReply($adminReply)
    {
        $this->adminReply = $adminReply;

        return $this;
    }

    /**
     * Get adminReply
     *
     * @return string 
     */
    public function getAdminReply()
    {
        return $this->adminReply;
    }

    /**
     * Set adminReplyName
     *
     * @param string $adminReplyName
     * 
     * @return GlpiConfigs
     */
    public function setAdminReplyName($adminReplyName)
    {
        $this->adminReplyName = $adminReplyName;

        return $this;
    }

    /**
     * Get adminReplyName
     *
     * @return string 
     */
    public function getAdminReplyName()
    {
        return $this->adminReplyName;
    }

    /**
     * Set mailingSignature
     *
     * @param string $mailingSignature
     * 
     * @return GlpiConfigs
     */
    public function setMailingSignature($mailingSignature)
    {
        $this->mailingSignature = $mailingSignature;

        return $this;
    }

    /**
     * Get mailingSignature
     *
     * @return string 
     */
    public function getMailingSignature()
    {
        return $this->mailingSignature;
    }

    /**
     * Set useAnonymousHelpdesk
     *
     * @param boolean $useAnonymousHelpdesk
     * 
     * @return GlpiConfigs
     */
    public function setUseAnonymousHelpdesk($useAnonymousHelpdesk)
    {
        $this->useAnonymousHelpdesk = $useAnonymousHelpdesk;

        return $this;
    }

    /**
     * Get useAnonymousHelpdesk
     *
     * @return boolean 
     */
    public function getUseAnonymousHelpdesk()
    {
        return $this->useAnonymousHelpdesk;
    }

    /**
     * Set useAnonymousFollowups
     *
     * @param boolean $useAnonymousFollowups
     * 
     * @return GlpiConfigs
     */
    public function setUseAnonymousFollowups($useAnonymousFollowups)
    {
        $this->useAnonymousFollowups = $useAnonymousFollowups;

        return $this;
    }

    /**
     * Get useAnonymousFollowups
     *
     * @return boolean 
     */
    public function getUseAnonymousFollowups()
    {
        return $this->useAnonymousFollowups;
    }

    /**
     * Set language
     *
     * @param string $language
     * 
     * @return GlpiConfigs
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set priority1
     *
     * @param string $priority1
     * 
     * @return GlpiConfigs
     */
    public function setPriority1($priority1)
    {
        $this->priority1 = $priority1;

        return $this;
    }

    /**
     * Get priority1
     *
     * @return string 
     */
    public function getPriority1()
    {
        return $this->priority1;
    }

    /**
     * Set priority2
     *
     * @param string $priority2
     * 
     * @return GlpiConfigs
     */
    public function setPriority2($priority2)
    {
        $this->priority2 = $priority2;

        return $this;
    }

    /**
     * Get priority2
     *
     * @return string 
     */
    public function getPriority2()
    {
        return $this->priority2;
    }

    /**
     * Set priority3
     *
     * @param string $priority3
     * 
     * @return GlpiConfigs
     */
    public function setPriority3($priority3)
    {
        $this->priority3 = $priority3;

        return $this;
    }

    /**
     * Get priority3
     *
     * @return string 
     */
    public function getPriority3()
    {
        return $this->priority3;
    }

    /**
     * Set priority4
     *
     * @param string $priority4
     * 
     * @return GlpiConfigs
     */
    public function setPriority4($priority4)
    {
        $this->priority4 = $priority4;

        return $this;
    }

    /**
     * Get priority4
     *
     * @return string 
     */
    public function getPriority4()
    {
        return $this->priority4;
    }

    /**
     * Set priority5
     *
     * @param string $priority5
     * 
     * @return GlpiConfigs
     */
    public function setPriority5($priority5)
    {
        $this->priority5 = $priority5;

        return $this;
    }

    /**
     * Get priority5
     *
     * @return string 
     */
    public function getPriority5()
    {
        return $this->priority5;
    }

    /**
     * Set priority6
     *
     * @param string $priority6
     * 
     * @return GlpiConfigs
     */
    public function setPriority6($priority6)
    {
        $this->priority6 = $priority6;

        return $this;
    }

    /**
     * Get priority6
     *
     * @return string 
     */
    public function getPriority6()
    {
        return $this->priority6;
    }

    /**
     * Set dateTax
     *
     * @param \DateTime $dateTax
     * 
     * @return GlpiConfigs
     */
    public function setDateTax($dateTax)
    {
        $this->dateTax = $dateTax;

        return $this;
    }

    /**
     * Get dateTax
     *
     * @return \DateTime 
     */
    public function getDateTax()
    {
        return $this->dateTax;
    }

    /**
     * Set casHost
     *
     * @param string $casHost
     * 
     * @return GlpiConfigs
     */
    public function setCasHost($casHost)
    {
        $this->casHost = $casHost;

        return $this;
    }

    /**
     * Get casHost
     *
     * @return string 
     */
    public function getCasHost()
    {
        return $this->casHost;
    }

    /**
     * Set casPort
     *
     * @param integer $casPort
     * 
     * @return GlpiConfigs
     */
    public function setCasPort($casPort)
    {
        $this->casPort = $casPort;

        return $this;
    }

    /**
     * Get casPort
     *
     * @return integer 
     */
    public function getCasPort()
    {
        return $this->casPort;
    }

    /**
     * Set casUri
     *
     * @param string $casUri
     * 
     * @return GlpiConfigs
     */
    public function setCasUri($casUri)
    {
        $this->casUri = $casUri;

        return $this;
    }

    /**
     * Get casUri
     *
     * @return string 
     */
    public function getCasUri()
    {
        return $this->casUri;
    }

    /**
     * Set casLogout
     *
     * @param string $casLogout
     * 
     * @return GlpiConfigs
     */
    public function setCasLogout($casLogout)
    {
        $this->casLogout = $casLogout;

        return $this;
    }

    /**
     * Get casLogout
     *
     * @return string 
     */
    public function getCasLogout()
    {
        return $this->casLogout;
    }

    /**
     * Set authldapsIdExtra
     *
     * @param integer $authldapsIdExtra
     * 
     * @return GlpiConfigs
     */
    public function setAuthldapsIdExtra($authldapsIdExtra)
    {
        $this->authldapsIdExtra = $authldapsIdExtra;

        return $this;
    }

    /**
     * Get authldapsIdExtra
     *
     * @return integer 
     */
    public function getAuthldapsIdExtra()
    {
        return $this->authldapsIdExtra;
    }

    /**
     * Set existingAuthServerField
     *
     * @param string $existingAuthServerField
     * 
     * @return GlpiConfigs
     */
    public function setExistingAuthServerField($existingAuthServerField)
    {
        $this->existingAuthServerField = $existingAuthServerField;

        return $this;
    }

    /**
     * Get existingAuthServerField
     *
     * @return string 
     */
    public function getExistingAuthServerField()
    {
        return $this->existingAuthServerField;
    }

    /**
     * Set existingAuthServerFieldCleanDomain
     *
     * @param boolean $existingAuthServerFieldCleanDomain
     * 
     * @return GlpiConfigs
     */
    public function setExistingAuthServerFieldCleanDomain($existingAuthServerFieldCleanDomain)
    {
        $this->existingAuthServerFieldCleanDomain = $existingAuthServerFieldCleanDomain;

        return $this;
    }

    /**
     * Get existingAuthServerFieldCleanDomain
     *
     * @return boolean 
     */
    public function getExistingAuthServerFieldCleanDomain()
    {
        return $this->existingAuthServerFieldCleanDomain;
    }

    /**
     * Set planningBegin
     *
     * @param \DateTime $planningBegin
     * 
     * @return GlpiConfigs
     */
    public function setPlanningBegin($planningBegin)
    {
        $this->planningBegin = $planningBegin;

        return $this;
    }

    /**
     * Get planningBegin
     *
     * @return \DateTime 
     */
    public function getPlanningBegin()
    {
        return $this->planningBegin;
    }

    /**
     * Set planningEnd
     *
     * @param \DateTime $planningEnd
     * 
     * @return GlpiConfigs
     */
    public function setPlanningEnd($planningEnd)
    {
        $this->planningEnd = $planningEnd;

        return $this;
    }

    /**
     * Get planningEnd
     *
     * @return \DateTime 
     */
    public function getPlanningEnd()
    {
        return $this->planningEnd;
    }

    /**
     * Set utf8Conv
     *
     * @param integer $utf8Conv
     * 
     * @return GlpiConfigs
     */
    public function setUtf8Conv($utf8Conv)
    {
        $this->utf8Conv = $utf8Conv;

        return $this;
    }

    /**
     * Get utf8Conv
     *
     * @return integer 
     */
    public function getUtf8Conv()
    {
        return $this->utf8Conv;
    }

    /**
     * Set usePublicFaq
     *
     * @param boolean $usePublicFaq
     * 
     * @return GlpiConfigs
     */
    public function setUsePublicFaq($usePublicFaq)
    {
        $this->usePublicFaq = $usePublicFaq;

        return $this;
    }

    /**
     * Get usePublicFaq
     *
     * @return boolean 
     */
    public function getUsePublicFaq()
    {
        return $this->usePublicFaq;
    }

    /**
     * Set urlBase
     *
     * @param string $urlBase
     * 
     * @return GlpiConfigs
     */
    public function setUrlBase($urlBase)
    {
        $this->urlBase = $urlBase;

        return $this;
    }

    /**
     * Get urlBase
     *
     * @return string 
     */
    public function getUrlBase()
    {
        return $this->urlBase;
    }

    /**
     * Set showLinkInMail
     *
     * @param boolean $showLinkInMail
     * 
     * @return GlpiConfigs
     */
    public function setShowLinkInMail($showLinkInMail)
    {
        $this->showLinkInMail = $showLinkInMail;

        return $this;
    }

    /**
     * Get showLinkInMail
     *
     * @return boolean 
     */
    public function getShowLinkInMail()
    {
        return $this->showLinkInMail;
    }

    /**
     * Set textLogin
     *
     * @param string $textLogin
     * 
     * @return GlpiConfigs
     */
    public function setTextLogin($textLogin)
    {
        $this->textLogin = $textLogin;

        return $this;
    }

    /**
     * Get textLogin
     *
     * @return string 
     */
    public function getTextLogin()
    {
        return $this->textLogin;
    }

    /**
     * Set foundedNewVersion
     *
     * @param string $foundedNewVersion
     * 
     * @return GlpiConfigs
     */
    public function setFoundedNewVersion($foundedNewVersion)
    {
        $this->foundedNewVersion = $foundedNewVersion;

        return $this;
    }

    /**
     * Get foundedNewVersion
     *
     * @return string 
     */
    public function getFoundedNewVersion()
    {
        return $this->foundedNewVersion;
    }

    /**
     * Set dropdownMax
     *
     * @param integer $dropdownMax
     * 
     * @return GlpiConfigs
     */
    public function setDropdownMax($dropdownMax)
    {
        $this->dropdownMax = $dropdownMax;

        return $this;
    }

    /**
     * Get dropdownMax
     *
     * @return integer 
     */
    public function getDropdownMax()
    {
        return $this->dropdownMax;
    }

    /**
     * Set ajaxWildcard
     *
     * @param string $ajaxWildcard
     * 
     * @return GlpiConfigs
     */
    public function setAjaxWildcard($ajaxWildcard)
    {
        $this->ajaxWildcard = $ajaxWildcard;

        return $this;
    }

    /**
     * Get ajaxWildcard
     *
     * @return string 
     */
    public function getAjaxWildcard()
    {
        return $this->ajaxWildcard;
    }

    /**
     * Set useAjax
     *
     * @param boolean $useAjax
     * 
     * @return GlpiConfigs
     */
    public function setUseAjax($useAjax)
    {
        $this->useAjax = $useAjax;

        return $this;
    }

    /**
     * Get useAjax
     *
     * @return boolean 
     */
    public function getUseAjax()
    {
        return $this->useAjax;
    }

    /**
     * Set ajaxMinTextsearchLoad
     *
     * @param integer $ajaxMinTextsearchLoad
     * 
     * @return GlpiConfigs
     */
    public function setAjaxMinTextsearchLoad($ajaxMinTextsearchLoad)
    {
        $this->ajaxMinTextsearchLoad = $ajaxMinTextsearchLoad;

        return $this;
    }

    /**
     * Get ajaxMinTextsearchLoad
     *
     * @return integer 
     */
    public function getAjaxMinTextsearchLoad()
    {
        return $this->ajaxMinTextsearchLoad;
    }

    /**
     * Set ajaxLimitCount
     *
     * @param integer $ajaxLimitCount
     * 
     * @return GlpiConfigs
     */
    public function setAjaxLimitCount($ajaxLimitCount)
    {
        $this->ajaxLimitCount = $ajaxLimitCount;

        return $this;
    }

    /**
     * Get ajaxLimitCount
     *
     * @return integer 
     */
    public function getAjaxLimitCount()
    {
        return $this->ajaxLimitCount;
    }

    /**
     * Set useAjaxAutocompletion
     *
     * @param boolean $useAjaxAutocompletion
     * 
     * @return GlpiConfigs
     */
    public function setUseAjaxAutocompletion($useAjaxAutocompletion)
    {
        $this->useAjaxAutocompletion = $useAjaxAutocompletion;

        return $this;
    }

    /**
     * Get useAjaxAutocompletion
     *
     * @return boolean 
     */
    public function getUseAjaxAutocompletion()
    {
        return $this->useAjaxAutocompletion;
    }

    /**
     * Set isUsersAutoAdd
     *
     * @param boolean $isUsersAutoAdd
     * 
     * @return GlpiConfigs
     */
    public function setIsUsersAutoAdd($isUsersAutoAdd)
    {
        $this->isUsersAutoAdd = $isUsersAutoAdd;

        return $this;
    }

    /**
     * Get isUsersAutoAdd
     *
     * @return boolean 
     */
    public function getIsUsersAutoAdd()
    {
        return $this->isUsersAutoAdd;
    }

    /**
     * Set dateFormat
     *
     * @param integer $dateFormat
     * 
     * @return GlpiConfigs
     */
    public function setDateFormat($dateFormat)
    {
        $this->dateFormat = $dateFormat;

        return $this;
    }

    /**
     * Get dateFormat
     *
     * @return integer 
     */
    public function getDateFormat()
    {
        return $this->dateFormat;
    }

    /**
     * Set numberFormat
     *
     * @param integer $numberFormat
     * 
     * @return GlpiConfigs
     */
    public function setNumberFormat($numberFormat)
    {
        $this->numberFormat = $numberFormat;

        return $this;
    }

    /**
     * Get numberFormat
     *
     * @return integer 
     */
    public function getNumberFormat()
    {
        return $this->numberFormat;
    }

    /**
     * Set csvDelimiter
     *
     * @param string $csvDelimiter
     * 
     * @return GlpiConfigs
     */
    public function setCsvDelimiter($csvDelimiter)
    {
        $this->csvDelimiter = $csvDelimiter;

        return $this;
    }

    /**
     * Get csvDelimiter
     *
     * @return string 
     */
    public function getCsvDelimiter()
    {
        return $this->csvDelimiter;
    }

    /**
     * Set isIdsVisible
     *
     * @param boolean $isIdsVisible
     * 
     * @return GlpiConfigs
     */
    public function setIsIdsVisible($isIdsVisible)
    {
        $this->isIdsVisible = $isIdsVisible;

        return $this;
    }

    /**
     * Get isIdsVisible
     *
     * @return boolean 
     */
    public function getIsIdsVisible()
    {
        return $this->isIdsVisible;
    }

    /**
     * Set dropdownCharsLimit
     *
     * @param integer $dropdownCharsLimit
     * 
     * @return GlpiConfigs
     */
    public function setDropdownCharsLimit($dropdownCharsLimit)
    {
        $this->dropdownCharsLimit = $dropdownCharsLimit;

        return $this;
    }

    /**
     * Get dropdownCharsLimit
     *
     * @return integer 
     */
    public function getDropdownCharsLimit()
    {
        return $this->dropdownCharsLimit;
    }

    /**
     * Set useOcsMode
     *
     * @param boolean $useOcsMode
     * 
     * @return GlpiConfigs
     */
    public function setUseOcsMode($useOcsMode)
    {
        $this->useOcsMode = $useOcsMode;

        return $this;
    }

    /**
     * Get useOcsMode
     *
     * @return boolean 
     */
    public function getUseOcsMode()
    {
        return $this->useOcsMode;
    }

    /**
     * Set smtpMode
     *
     * @param integer $smtpMode
     * 
     * @return GlpiConfigs
     */
    public function setSmtpMode($smtpMode)
    {
        $this->smtpMode = $smtpMode;

        return $this;
    }

    /**
     * Get smtpMode
     *
     * @return integer 
     */
    public function getSmtpMode()
    {
        return $this->smtpMode;
    }

    /**
     * Set smtpHost
     *
     * @param string $smtpHost
     * 
     * @return GlpiConfigs
     */
    public function setSmtpHost($smtpHost)
    {
        $this->smtpHost = $smtpHost;

        return $this;
    }

    /**
     * Get smtpHost
     *
     * @return string 
     */
    public function getSmtpHost()
    {
        return $this->smtpHost;
    }

    /**
     * Set smtpPort
     *
     * @param integer $smtpPort
     * 
     * @return GlpiConfigs
     */
    public function setSmtpPort($smtpPort)
    {
        $this->smtpPort = $smtpPort;

        return $this;
    }

    /**
     * Get smtpPort
     *
     * @return integer 
     */
    public function getSmtpPort()
    {
        return $this->smtpPort;
    }

    /**
     * Set smtpUsername
     *
     * @param string $smtpUsername
     * 
     * @return GlpiConfigs
     */
    public function setSmtpUsername($smtpUsername)
    {
        $this->smtpUsername = $smtpUsername;

        return $this;
    }

    /**
     * Get smtpUsername
     *
     * @return string 
     */
    public function getSmtpUsername()
    {
        return $this->smtpUsername;
    }

    /**
     * Set proxyName
     *
     * @param string $proxyName
     * 
     * @return GlpiConfigs
     */
    public function setProxyName($proxyName)
    {
        $this->proxyName = $proxyName;

        return $this;
    }

    /**
     * Get proxyName
     *
     * @return string 
     */
    public function getProxyName()
    {
        return $this->proxyName;
    }

    /**
     * Set proxyPort
     *
     * @param integer $proxyPort
     * 
     * @return GlpiConfigs
     */
    public function setProxyPort($proxyPort)
    {
        $this->proxyPort = $proxyPort;

        return $this;
    }

    /**
     * Get proxyPort
     *
     * @return integer 
     */
    public function getProxyPort()
    {
        return $this->proxyPort;
    }

    /**
     * Set proxyUser
     *
     * @param string $proxyUser
     * 
     * @return GlpiConfigs
     */
    public function setProxyUser($proxyUser)
    {
        $this->proxyUser = $proxyUser;

        return $this;
    }

    /**
     * Get proxyUser
     *
     * @return string 
     */
    public function getProxyUser()
    {
        return $this->proxyUser;
    }

    /**
     * Set addFollowupOnUpdateTicket
     *
     * @param boolean $addFollowupOnUpdateTicket
     * 
     * @return GlpiConfigs
     */
    public function setAddFollowupOnUpdateTicket($addFollowupOnUpdateTicket)
    {
        $this->addFollowupOnUpdateTicket = $addFollowupOnUpdateTicket;

        return $this;
    }

    /**
     * Get addFollowupOnUpdateTicket
     *
     * @return boolean 
     */
    public function getAddFollowupOnUpdateTicket()
    {
        return $this->addFollowupOnUpdateTicket;
    }

    /**
     * Set keepTicketsOnDelete
     *
     * @param boolean $keepTicketsOnDelete
     * 
     * @return GlpiConfigs
     */
    public function setKeepTicketsOnDelete($keepTicketsOnDelete)
    {
        $this->keepTicketsOnDelete = $keepTicketsOnDelete;

        return $this;
    }

    /**
     * Get keepTicketsOnDelete
     *
     * @return boolean 
     */
    public function getKeepTicketsOnDelete()
    {
        return $this->keepTicketsOnDelete;
    }

    /**
     * Set timeStep
     *
     * @param integer $timeStep
     * 
     * @return GlpiConfigs
     */
    public function setTimeStep($timeStep)
    {
        $this->timeStep = $timeStep;

        return $this;
    }

    /**
     * Get timeStep
     *
     * @return integer 
     */
    public function getTimeStep()
    {
        return $this->timeStep;
    }

    /**
     * Set decimalNumber
     *
     * @param integer $decimalNumber
     * 
     * @return GlpiConfigs
     */
    public function setDecimalNumber($decimalNumber)
    {
        $this->decimalNumber = $decimalNumber;

        return $this;
    }

    /**
     * Get decimalNumber
     *
     * @return integer 
     */
    public function getDecimalNumber()
    {
        return $this->decimalNumber;
    }

    /**
     * Set helpdeskDocUrl
     *
     * @param string $helpdeskDocUrl
     * 
     * @return GlpiConfigs
     */
    public function setHelpdeskDocUrl($helpdeskDocUrl)
    {
        $this->helpdeskDocUrl = $helpdeskDocUrl;

        return $this;
    }

    /**
     * Get helpdeskDocUrl
     *
     * @return string 
     */
    public function getHelpdeskDocUrl()
    {
        return $this->helpdeskDocUrl;
    }

    /**
     * Set centralDocUrl
     *
     * @param string $centralDocUrl
     * 
     * @return GlpiConfigs
     */
    public function setCentralDocUrl($centralDocUrl)
    {
        $this->centralDocUrl = $centralDocUrl;

        return $this;
    }

    /**
     * Get centralDocUrl
     *
     * @return string 
     */
    public function getCentralDocUrl()
    {
        return $this->centralDocUrl;
    }

    /**
     * Set documentcategoriesIdForticket
     *
     * @param integer $documentcategoriesIdForticket
     * 
     * @return GlpiConfigs
     */
    public function setDocumentcategoriesIdForticket($documentcategoriesIdForticket)
    {
        $this->documentcategoriesIdForticket = $documentcategoriesIdForticket;

        return $this;
    }

    /**
     * Get documentcategoriesIdForticket
     *
     * @return integer 
     */
    public function getDocumentcategoriesIdForticket()
    {
        return $this->documentcategoriesIdForticket;
    }

    /**
     * Set monitorsManagementRestrict
     *
     * @param integer $monitorsManagementRestrict
     * 
     * @return GlpiConfigs
     */
    public function setMonitorsManagementRestrict($monitorsManagementRestrict)
    {
        $this->monitorsManagementRestrict = $monitorsManagementRestrict;

        return $this;
    }

    /**
     * Get monitorsManagementRestrict
     *
     * @return integer 
     */
    public function getMonitorsManagementRestrict()
    {
        return $this->monitorsManagementRestrict;
    }

    /**
     * Set phonesManagementRestrict
     *
     * @param integer $phonesManagementRestrict
     * 
     * @return GlpiConfigs
     */
    public function setPhonesManagementRestrict($phonesManagementRestrict)
    {
        $this->phonesManagementRestrict = $phonesManagementRestrict;

        return $this;
    }

    /**
     * Get phonesManagementRestrict
     *
     * @return integer 
     */
    public function getPhonesManagementRestrict()
    {
        return $this->phonesManagementRestrict;
    }

    /**
     * Set peripheralsManagementRestrict
     *
     * @param integer $peripheralsManagementRestrict
     * 
     * @return GlpiConfigs
     */
    public function setPeripheralsManagementRestrict($peripheralsManagementRestrict)
    {
        $this->peripheralsManagementRestrict = $peripheralsManagementRestrict;

        return $this;
    }

    /**
     * Get peripheralsManagementRestrict
     *
     * @return integer 
     */
    public function getPeripheralsManagementRestrict()
    {
        return $this->peripheralsManagementRestrict;
    }

    /**
     * Set printersManagementRestrict
     *
     * @param integer $printersManagementRestrict
     * 
     * @return GlpiConfigs
     */
    public function setPrintersManagementRestrict($printersManagementRestrict)
    {
        $this->printersManagementRestrict = $printersManagementRestrict;

        return $this;
    }

    /**
     * Get printersManagementRestrict
     *
     * @return integer 
     */
    public function getPrintersManagementRestrict()
    {
        return $this->printersManagementRestrict;
    }

    /**
     * Set useLogInFiles
     *
     * @param boolean $useLogInFiles
     * 
     * @return GlpiConfigs
     */
    public function setUseLogInFiles($useLogInFiles)
    {
        $this->useLogInFiles = $useLogInFiles;

        return $this;
    }

    /**
     * Get useLogInFiles
     *
     * @return boolean 
     */
    public function getUseLogInFiles()
    {
        return $this->useLogInFiles;
    }

    /**
     * Set timeOffset
     *
     * @param integer $timeOffset
     * 
     * @return GlpiConfigs
     */
    public function setTimeOffset($timeOffset)
    {
        $this->timeOffset = $timeOffset;

        return $this;
    }

    /**
     * Get timeOffset
     *
     * @return integer 
     */
    public function getTimeOffset()
    {
        return $this->timeOffset;
    }

    /**
     * Set isContactAutoupdate
     *
     * @param boolean $isContactAutoupdate
     * 
     * @return GlpiConfigs
     */
    public function setIsContactAutoupdate($isContactAutoupdate)
    {
        $this->isContactAutoupdate = $isContactAutoupdate;

        return $this;
    }

    /**
     * Get isContactAutoupdate
     *
     * @return boolean 
     */
    public function getIsContactAutoupdate()
    {
        return $this->isContactAutoupdate;
    }

    /**
     * Set isUserAutoupdate
     *
     * @param boolean $isUserAutoupdate
     * 
     * @return GlpiConfigs
     */
    public function setIsUserAutoupdate($isUserAutoupdate)
    {
        $this->isUserAutoupdate = $isUserAutoupdate;

        return $this;
    }

    /**
     * Get isUserAutoupdate
     *
     * @return boolean 
     */
    public function getIsUserAutoupdate()
    {
        return $this->isUserAutoupdate;
    }

    /**
     * Set isGroupAutoupdate
     *
     * @param boolean $isGroupAutoupdate
     * 
     * @return GlpiConfigs
     */
    public function setIsGroupAutoupdate($isGroupAutoupdate)
    {
        $this->isGroupAutoupdate = $isGroupAutoupdate;

        return $this;
    }

    /**
     * Get isGroupAutoupdate
     *
     * @return boolean 
     */
    public function getIsGroupAutoupdate()
    {
        return $this->isGroupAutoupdate;
    }

    /**
     * Set isLocationAutoupdate
     *
     * @param boolean $isLocationAutoupdate
     * 
     * @return GlpiConfigs
     */
    public function setIsLocationAutoupdate($isLocationAutoupdate)
    {
        $this->isLocationAutoupdate = $isLocationAutoupdate;

        return $this;
    }

    /**
     * Get isLocationAutoupdate
     *
     * @return boolean 
     */
    public function getIsLocationAutoupdate()
    {
        return $this->isLocationAutoupdate;
    }

    /**
     * Set stateAutoupdateMode
     *
     * @param integer $stateAutoupdateMode
     * 
     * @return GlpiConfigs
     */
    public function setStateAutoupdateMode($stateAutoupdateMode)
    {
        $this->stateAutoupdateMode = $stateAutoupdateMode;

        return $this;
    }

    /**
     * Get stateAutoupdateMode
     *
     * @return integer 
     */
    public function getStateAutoupdateMode()
    {
        return $this->stateAutoupdateMode;
    }

    /**
     * Set isContactAutoclean
     *
     * @param boolean $isContactAutoclean
     * 
     * @return GlpiConfigs
     */
    public function setIsContactAutoclean($isContactAutoclean)
    {
        $this->isContactAutoclean = $isContactAutoclean;

        return $this;
    }

    /**
     * Get isContactAutoclean
     *
     * @return boolean 
     */
    public function getIsContactAutoclean()
    {
        return $this->isContactAutoclean;
    }

    /**
     * Set isUserAutoclean
     *
     * @param boolean $isUserAutoclean
     * 
     * @return GlpiConfigs
     */
    public function setIsUserAutoclean($isUserAutoclean)
    {
        $this->isUserAutoclean = $isUserAutoclean;

        return $this;
    }

    /**
     * Get isUserAutoclean
     *
     * @return boolean 
     */
    public function getIsUserAutoclean()
    {
        return $this->isUserAutoclean;
    }

    /**
     * Set isGroupAutoclean
     *
     * @param boolean $isGroupAutoclean
     * 
     * @return GlpiConfigs
     */
    public function setIsGroupAutoclean($isGroupAutoclean)
    {
        $this->isGroupAutoclean = $isGroupAutoclean;

        return $this;
    }

    /**
     * Get isGroupAutoclean
     *
     * @return boolean 
     */
    public function getIsGroupAutoclean()
    {
        return $this->isGroupAutoclean;
    }

    /**
     * Set isLocationAutoclean
     *
     * @param boolean $isLocationAutoclean
     * 
     * @return GlpiConfigs
     */
    public function setIsLocationAutoclean($isLocationAutoclean)
    {
        $this->isLocationAutoclean = $isLocationAutoclean;

        return $this;
    }

    /**
     * Get isLocationAutoclean
     *
     * @return boolean 
     */
    public function getIsLocationAutoclean()
    {
        return $this->isLocationAutoclean;
    }

    /**
     * Set stateAutocleanMode
     *
     * @param integer $stateAutocleanMode
     * 
     * @return GlpiConfigs
     */
    public function setStateAutocleanMode($stateAutocleanMode)
    {
        $this->stateAutocleanMode = $stateAutocleanMode;

        return $this;
    }

    /**
     * Get stateAutocleanMode
     *
     * @return integer 
     */
    public function getStateAutocleanMode()
    {
        return $this->stateAutocleanMode;
    }

    /**
     * Set useFlatDropdowntree
     *
     * @param boolean $useFlatDropdowntree
     * 
     * @return GlpiConfigs
     */
    public function setUseFlatDropdowntree($useFlatDropdowntree)
    {
        $this->useFlatDropdowntree = $useFlatDropdowntree;

        return $this;
    }

    /**
     * Get useFlatDropdowntree
     *
     * @return boolean 
     */
    public function getUseFlatDropdowntree()
    {
        return $this->useFlatDropdowntree;
    }

    /**
     * Set useAutonameByEntity
     *
     * @param boolean $useAutonameByEntity
     * 
     * @return GlpiConfigs
     */
    public function setUseAutonameByEntity($useAutonameByEntity)
    {
        $this->useAutonameByEntity = $useAutonameByEntity;

        return $this;
    }

    /**
     * Get useAutonameByEntity
     *
     * @return boolean 
     */
    public function getUseAutonameByEntity()
    {
        return $this->useAutonameByEntity;
    }

    /**
     * Set isCategorizedSoftExpanded
     *
     * @param boolean $isCategorizedSoftExpanded
     * 
     * @return GlpiConfigs
     */
    public function setIsCategorizedSoftExpanded($isCategorizedSoftExpanded)
    {
        $this->isCategorizedSoftExpanded = $isCategorizedSoftExpanded;

        return $this;
    }

    /**
     * Get isCategorizedSoftExpanded
     *
     * @return boolean 
     */
    public function getIsCategorizedSoftExpanded()
    {
        return $this->isCategorizedSoftExpanded;
    }

    /**
     * Set isNotCategorizedSoftExpanded
     *
     * @param boolean $isNotCategorizedSoftExpanded
     * 
     * @return GlpiConfigs
     */
    public function setIsNotCategorizedSoftExpanded($isNotCategorizedSoftExpanded)
    {
        $this->isNotCategorizedSoftExpanded = $isNotCategorizedSoftExpanded;

        return $this;
    }

    /**
     * Get isNotCategorizedSoftExpanded
     *
     * @return boolean 
     */
    public function getIsNotCategorizedSoftExpanded()
    {
        return $this->isNotCategorizedSoftExpanded;
    }

    /**
     * Set softwarecategoriesIdOndelete
     *
     * @param integer $softwarecategoriesIdOndelete
     * 
     * @return GlpiConfigs
     */
    public function setSoftwarecategoriesIdOndelete($softwarecategoriesIdOndelete)
    {
        $this->softwarecategoriesIdOndelete = $softwarecategoriesIdOndelete;

        return $this;
    }

    /**
     * Get softwarecategoriesIdOndelete
     *
     * @return integer 
     */
    public function getSoftwarecategoriesIdOndelete()
    {
        return $this->softwarecategoriesIdOndelete;
    }

    /**
     * Set x509EmailField
     *
     * @param string $x509EmailField
     * 
     * @return GlpiConfigs
     */
    public function setX509EmailField($x509EmailField)
    {
        $this->x509EmailField = $x509EmailField;

        return $this;
    }

    /**
     * Get x509EmailField
     *
     * @return string 
     */
    public function getX509EmailField()
    {
        return $this->x509EmailField;
    }

    /**
     * Set defaultMailcollectorFilesizeMax
     *
     * @param integer $defaultMailcollectorFilesizeMax
     * 
     * @return GlpiConfigs
     */
    public function setDefaultMailcollectorFilesizeMax($defaultMailcollectorFilesizeMax)
    {
        $this->defaultMailcollectorFilesizeMax = $defaultMailcollectorFilesizeMax;

        return $this;
    }

    /**
     * Get defaultMailcollectorFilesizeMax
     *
     * @return integer 
     */
    public function getDefaultMailcollectorFilesizeMax()
    {
        return $this->defaultMailcollectorFilesizeMax;
    }

    /**
     * Set followupPrivate
     *
     * @param boolean $followupPrivate
     * 
     * @return GlpiConfigs
     */
    public function setFollowupPrivate($followupPrivate)
    {
        $this->followupPrivate = $followupPrivate;

        return $this;
    }

    /**
     * Get followupPrivate
     *
     * @return boolean 
     */
    public function getFollowupPrivate()
    {
        return $this->followupPrivate;
    }

    /**
     * Set taskPrivate
     *
     * @param boolean $taskPrivate
     * 
     * @return GlpiConfigs
     */
    public function setTaskPrivate($taskPrivate)
    {
        $this->taskPrivate = $taskPrivate;

        return $this;
    }

    /**
     * Get taskPrivate
     *
     * @return boolean 
     */
    public function getTaskPrivate()
    {
        return $this->taskPrivate;
    }

    /**
     * Set defaultSoftwareHelpdeskVisible
     *
     * @param boolean $defaultSoftwareHelpdeskVisible
     * 
     * @return GlpiConfigs
     */
    public function setDefaultSoftwareHelpdeskVisible($defaultSoftwareHelpdeskVisible)
    {
        $this->defaultSoftwareHelpdeskVisible = $defaultSoftwareHelpdeskVisible;

        return $this;
    }

    /**
     * Get defaultSoftwareHelpdeskVisible
     *
     * @return boolean 
     */
    public function getDefaultSoftwareHelpdeskVisible()
    {
        return $this->defaultSoftwareHelpdeskVisible;
    }

    /**
     * Set namesFormat
     *
     * @param integer $namesFormat
     * 
     * @return GlpiConfigs
     */
    public function setNamesFormat($namesFormat)
    {
        $this->namesFormat = $namesFormat;

        return $this;
    }

    /**
     * Get namesFormat
     *
     * @return integer 
     */
    public function getNamesFormat()
    {
        return $this->namesFormat;
    }

    /**
     * Set defaultGraphtype
     *
     * @param string $defaultGraphtype
     * 
     * @return GlpiConfigs
     */
    public function setDefaultGraphtype($defaultGraphtype)
    {
        $this->defaultGraphtype = $defaultGraphtype;

        return $this;
    }

    /**
     * Get defaultGraphtype
     *
     * @return string 
     */
    public function getDefaultGraphtype()
    {
        return $this->defaultGraphtype;
    }

    /**
     * Set defaultRequesttypesId
     *
     * @param integer $defaultRequesttypesId
     * 
     * @return GlpiConfigs
     */
    public function setDefaultRequesttypesId($defaultRequesttypesId)
    {
        $this->defaultRequesttypesId = $defaultRequesttypesId;

        return $this;
    }

    /**
     * Get defaultRequesttypesId
     *
     * @return integer 
     */
    public function getDefaultRequesttypesId()
    {
        return $this->defaultRequesttypesId;
    }

    /**
     * Set useNorightUsersAdd
     *
     * @param boolean $useNorightUsersAdd
     * 
     * @return GlpiConfigs
     */
    public function setUseNorightUsersAdd($useNorightUsersAdd)
    {
        $this->useNorightUsersAdd = $useNorightUsersAdd;

        return $this;
    }

    /**
     * Get useNorightUsersAdd
     *
     * @return boolean 
     */
    public function getUseNorightUsersAdd()
    {
        return $this->useNorightUsersAdd;
    }

    /**
     * Set cronLimit
     *
     * @param boolean $cronLimit
     * 
     * @return GlpiConfigs
     */
    public function setCronLimit($cronLimit)
    {
        $this->cronLimit = $cronLimit;

        return $this;
    }

    /**
     * Get cronLimit
     *
     * @return boolean 
     */
    public function getCronLimit()
    {
        return $this->cronLimit;
    }

    /**
     * Set priorityMatrix
     *
     * @param string $priorityMatrix
     * 
     * @return GlpiConfigs
     */
    public function setPriorityMatrix($priorityMatrix)
    {
        $this->priorityMatrix = $priorityMatrix;

        return $this;
    }

    /**
     * Get priorityMatrix
     *
     * @return string 
     */
    public function getPriorityMatrix()
    {
        return $this->priorityMatrix;
    }

    /**
     * Set urgencyMask
     *
     * @param integer $urgencyMask
     * 
     * @return GlpiConfigs
     */
    public function setUrgencyMask($urgencyMask)
    {
        $this->urgencyMask = $urgencyMask;

        return $this;
    }

    /**
     * Get urgencyMask
     *
     * @return integer 
     */
    public function getUrgencyMask()
    {
        return $this->urgencyMask;
    }

    /**
     * Set impactMask
     *
     * @param integer $impactMask
     * 
     * @return GlpiConfigs
     */
    public function setImpactMask($impactMask)
    {
        $this->impactMask = $impactMask;

        return $this;
    }

    /**
     * Get impactMask
     *
     * @return integer 
     */
    public function getImpactMask()
    {
        return $this->impactMask;
    }

    /**
     * Set userDeletedLdap
     *
     * @param boolean $userDeletedLdap
     * 
     * @return GlpiConfigs
     */
    public function setUserDeletedLdap($userDeletedLdap)
    {
        $this->userDeletedLdap = $userDeletedLdap;

        return $this;
    }

    /**
     * Get userDeletedLdap
     *
     * @return boolean 
     */
    public function getUserDeletedLdap()
    {
        return $this->userDeletedLdap;
    }

    /**
     * Set autoCreateInfocoms
     *
     * @param boolean $autoCreateInfocoms
     * 
     * @return GlpiConfigs
     */
    public function setAutoCreateInfocoms($autoCreateInfocoms)
    {
        $this->autoCreateInfocoms = $autoCreateInfocoms;

        return $this;
    }

    /**
     * Get autoCreateInfocoms
     *
     * @return boolean 
     */
    public function getAutoCreateInfocoms()
    {
        return $this->autoCreateInfocoms;
    }

    /**
     * Set useSlaveForSearch
     *
     * @param boolean $useSlaveForSearch
     * 
     * @return GlpiConfigs
     */
    public function setUseSlaveForSearch($useSlaveForSearch)
    {
        $this->useSlaveForSearch = $useSlaveForSearch;

        return $this;
    }

    /**
     * Get useSlaveForSearch
     *
     * @return boolean 
     */
    public function getUseSlaveForSearch()
    {
        return $this->useSlaveForSearch;
    }

    /**
     * Set proxyPasswd
     *
     * @param string $proxyPasswd
     * 
     * @return GlpiConfigs
     */
    public function setProxyPasswd($proxyPasswd)
    {
        $this->proxyPasswd = $proxyPasswd;

        return $this;
    }

    /**
     * Get proxyPasswd
     *
     * @return string 
     */
    public function getProxyPasswd()
    {
        return $this->proxyPasswd;
    }

    /**
     * Set smtpPasswd
     *
     * @param string $smtpPasswd
     * 
     * @return GlpiConfigs
     */
    public function setSmtpPasswd($smtpPasswd)
    {
        $this->smtpPasswd = $smtpPasswd;

        return $this;
    }

    /**
     * Get smtpPasswd
     *
     * @return string 
     */
    public function getSmtpPasswd()
    {
        return $this->smtpPasswd;
    }

    /**
     * Set transfersIdAuto
     *
     * @param integer $transfersIdAuto
     * 
     * @return GlpiConfigs
     */
    public function setTransfersIdAuto($transfersIdAuto)
    {
        $this->transfersIdAuto = $transfersIdAuto;

        return $this;
    }

    /**
     * Get transfersIdAuto
     *
     * @return integer 
     */
    public function getTransfersIdAuto()
    {
        return $this->transfersIdAuto;
    }

    /**
     * Set showCountOnTabs
     *
     * @param boolean $showCountOnTabs
     * 
     * @return GlpiConfigs
     */
    public function setShowCountOnTabs($showCountOnTabs)
    {
        $this->showCountOnTabs = $showCountOnTabs;

        return $this;
    }

    /**
     * Get showCountOnTabs
     *
     * @return boolean 
     */
    public function getShowCountOnTabs()
    {
        return $this->showCountOnTabs;
    }

    /**
     * Set refreshTicketList
     *
     * @param integer $refreshTicketList
     * 
     * @return GlpiConfigs
     */
    public function setRefreshTicketList($refreshTicketList)
    {
        $this->refreshTicketList = $refreshTicketList;

        return $this;
    }

    /**
     * Get refreshTicketList
     *
     * @return integer 
     */
    public function getRefreshTicketList()
    {
        return $this->refreshTicketList;
    }

    /**
     * Set setDefaultTech
     *
     * @param boolean $setDefaultTech
     * 
     * @return GlpiConfigs
     */
    public function setSetDefaultTech($setDefaultTech)
    {
        $this->setDefaultTech = $setDefaultTech;

        return $this;
    }

    /**
     * Get setDefaultTech
     *
     * @return boolean 
     */
    public function getSetDefaultTech()
    {
        return $this->setDefaultTech;
    }

    /**
     * Set allowSearchView
     *
     * @param integer $allowSearchView
     * 
     * @return GlpiConfigs
     */
    public function setAllowSearchView($allowSearchView)
    {
        $this->allowSearchView = $allowSearchView;

        return $this;
    }

    /**
     * Get allowSearchView
     *
     * @return integer 
     */
    public function getAllowSearchView()
    {
        return $this->allowSearchView;
    }

    /**
     * Set allowSearchAll
     *
     * @param boolean $allowSearchAll
     * 
     * @return GlpiConfigs
     */
    public function setAllowSearchAll($allowSearchAll)
    {
        $this->allowSearchAll = $allowSearchAll;

        return $this;
    }

    /**
     * Get allowSearchAll
     *
     * @return boolean 
     */
    public function getAllowSearchAll()
    {
        return $this->allowSearchAll;
    }

    /**
     * Set allowSearchGlobal
     *
     * @param boolean $allowSearchGlobal
     * 
     * @return GlpiConfigs
     */
    public function setAllowSearchGlobal($allowSearchGlobal)
    {
        $this->allowSearchGlobal = $allowSearchGlobal;

        return $this;
    }

    /**
     * Get allowSearchGlobal
     *
     * @return boolean 
     */
    public function getAllowSearchGlobal()
    {
        return $this->allowSearchGlobal;
    }

    /**
     * Set displayCountOnHome
     *
     * @param integer $displayCountOnHome
     * 
     * @return GlpiConfigs
     */
    public function setDisplayCountOnHome($displayCountOnHome)
    {
        $this->displayCountOnHome = $displayCountOnHome;

        return $this;
    }

    /**
     * Get displayCountOnHome
     *
     * @return integer 
     */
    public function getDisplayCountOnHome()
    {
        return $this->displayCountOnHome;
    }
}