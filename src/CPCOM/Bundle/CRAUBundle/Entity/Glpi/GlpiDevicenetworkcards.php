<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiDevicenetworkcards
 *
 * @ORM\Table(name="glpi_devicenetworkcards")
 * @ORM\Entity
 */
class GlpiDevicenetworkcards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="bandwidth", type="string", length=255, nullable=true)
     */
    private $bandwidth;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var string
     *
     * @ORM\Column(name="specif_default", type="string", length=255, nullable=true)
     */
    private $specifDefault;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     * 
     * @return GlpiDevicenetworkcards
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string 
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set bandwidth
     *
     * @param string $bandwidth
     * 
     * @return GlpiDevicenetworkcards
     */
    public function setBandwidth($bandwidth)
    {
        $this->bandwidth = $bandwidth;

        return $this;
    }

    /**
     * Get bandwidth
     *
     * @return string 
     */
    public function getBandwidth()
    {
        return $this->bandwidth;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiDevicenetworkcards
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set manufacturersId
     *
     * @param integer $manufacturersId
     * 
     * @return GlpiDevicenetworkcards
     */
    public function setManufacturersId($manufacturersId)
    {
        $this->manufacturersId = $manufacturersId;

        return $this;
    }

    /**
     * Get manufacturersId
     *
     * @return integer 
     */
    public function getManufacturersId()
    {
        return $this->manufacturersId;
    }

    /**
     * Set specifDefault
     *
     * @param string $specifDefault
     * 
     * @return GlpiDevicenetworkcards
     */
    public function setSpecifDefault($specifDefault)
    {
        $this->specifDefault = $specifDefault;

        return $this;
    }

    /**
     * Get specifDefault
     *
     * @return string 
     */
    public function getSpecifDefault()
    {
        return $this->specifDefault;
    }
}