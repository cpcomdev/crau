<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginDumpentityModels
 *
 * @ORM\Table(name="glpi_plugin_dumpentity_models")
 * @ORM\Entity
 */
class GlpiPluginDumpentityModels
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_alerts", type="boolean", nullable=false)
     */
    private $glpiAlerts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_authldaps", type="boolean", nullable=false)
     */
    private $glpiAuthldaps;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_authmails", type="boolean", nullable=false)
     */
    private $glpiAuthmails;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_autoupdatesystems", type="boolean", nullable=false)
     */
    private $glpiAutoupdatesystems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_bookmarks", type="boolean", nullable=false)
     */
    private $glpiBookmarks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_bookmarks_users", type="boolean", nullable=false)
     */
    private $glpiBookmarksUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_budgets", type="boolean", nullable=false)
     */
    private $glpiBudgets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_calendars", type="boolean", nullable=false)
     */
    private $glpiCalendars;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_calendarsegments", type="boolean", nullable=false)
     */
    private $glpiCalendarsegments;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_calendars_holidays", type="boolean", nullable=false)
     */
    private $glpiCalendarsHolidays;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_cartridgeitems", type="boolean", nullable=false)
     */
    private $glpiCartridgeitems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_cartridgeitems_printermodels", type="boolean", nullable=false)
     */
    private $glpiCartridgeitemsPrintermodels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_cartridgeitemtypes", type="boolean", nullable=false)
     */
    private $glpiCartridgeitemtypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_cartridges", type="boolean", nullable=false)
     */
    private $glpiCartridges;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_changes", type="boolean", nullable=false)
     */
    private $glpiChanges;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_changes_groups", type="boolean", nullable=false)
     */
    private $glpiChangesGroups;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_changes_items", type="boolean", nullable=false)
     */
    private $glpiChangesItems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_changes_problems", type="boolean", nullable=false)
     */
    private $glpiChangesProblems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_changes_tickets", type="boolean", nullable=false)
     */
    private $glpiChangesTickets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_changes_users", type="boolean", nullable=false)
     */
    private $glpiChangesUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_changetasks", type="boolean", nullable=false)
     */
    private $glpiChangetasks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computerdisks", type="boolean", nullable=false)
     */
    private $glpiComputerdisks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computermodels", type="boolean", nullable=false)
     */
    private $glpiComputermodels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers", type="boolean", nullable=false)
     */
    private $glpiComputers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicecases", type="boolean", nullable=false)
     */
    private $glpiComputersDevicecases;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicecontrols", type="boolean", nullable=false)
     */
    private $glpiComputersDevicecontrols;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicedrives", type="boolean", nullable=false)
     */
    private $glpiComputersDevicedrives;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicegraphiccards", type="boolean", nullable=false)
     */
    private $glpiComputersDevicegraphiccards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_deviceharddrives", type="boolean", nullable=false)
     */
    private $glpiComputersDeviceharddrives;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicememories", type="boolean", nullable=false)
     */
    private $glpiComputersDevicememories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicemotherboards", type="boolean", nullable=false)
     */
    private $glpiComputersDevicemotherboards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicenetworkcards", type="boolean", nullable=false)
     */
    private $glpiComputersDevicenetworkcards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicepcis", type="boolean", nullable=false)
     */
    private $glpiComputersDevicepcis;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicepowersupplies", type="boolean", nullable=false)
     */
    private $glpiComputersDevicepowersupplies;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_deviceprocessors", type="boolean", nullable=false)
     */
    private $glpiComputersDeviceprocessors;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_devicesoundcards", type="boolean", nullable=false)
     */
    private $glpiComputersDevicesoundcards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_items", type="boolean", nullable=false)
     */
    private $glpiComputersItems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_softwarelicenses", type="boolean", nullable=false)
     */
    private $glpiComputersSoftwarelicenses;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computers_softwareversions", type="boolean", nullable=false)
     */
    private $glpiComputersSoftwareversions;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computertypes", type="boolean", nullable=false)
     */
    private $glpiComputertypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_computervirtualmachines", type="boolean", nullable=false)
     */
    private $glpiComputervirtualmachines;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_consumableitems", type="boolean", nullable=false)
     */
    private $glpiConsumableitems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_consumableitemtypes", type="boolean", nullable=false)
     */
    private $glpiConsumableitemtypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_consumables", type="boolean", nullable=false)
     */
    private $glpiConsumables;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_contacts", type="boolean", nullable=false)
     */
    private $glpiContacts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_contacts_suppliers", type="boolean", nullable=false)
     */
    private $glpiContactsSuppliers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_contacttypes", type="boolean", nullable=false)
     */
    private $glpiContacttypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_contracts", type="boolean", nullable=false)
     */
    private $glpiContracts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_contracts_items", type="boolean", nullable=false)
     */
    private $glpiContractsItems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_contracts_suppliers", type="boolean", nullable=false)
     */
    private $glpiContractsSuppliers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_contracttypes", type="boolean", nullable=false)
     */
    private $glpiContracttypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicecases", type="boolean", nullable=false)
     */
    private $glpiDevicecases;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicecasetypes", type="boolean", nullable=false)
     */
    private $glpiDevicecasetypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicecontrols", type="boolean", nullable=false)
     */
    private $glpiDevicecontrols;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicedrives", type="boolean", nullable=false)
     */
    private $glpiDevicedrives;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicegraphiccards", type="boolean", nullable=false)
     */
    private $glpiDevicegraphiccards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_deviceharddrives", type="boolean", nullable=false)
     */
    private $glpiDeviceharddrives;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicememories", type="boolean", nullable=false)
     */
    private $glpiDevicememories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicememorytypes", type="boolean", nullable=false)
     */
    private $glpiDevicememorytypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicemotherboards", type="boolean", nullable=false)
     */
    private $glpiDevicemotherboards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicenetworkcards", type="boolean", nullable=false)
     */
    private $glpiDevicenetworkcards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicepcis", type="boolean", nullable=false)
     */
    private $glpiDevicepcis;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicepowersupplies", type="boolean", nullable=false)
     */
    private $glpiDevicepowersupplies;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_deviceprocessors", type="boolean", nullable=false)
     */
    private $glpiDeviceprocessors;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_devicesoundcards", type="boolean", nullable=false)
     */
    private $glpiDevicesoundcards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_documentcategories", type="boolean", nullable=false)
     */
    private $glpiDocumentcategories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_documents", type="boolean", nullable=false)
     */
    private $glpiDocuments;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_documents_items", type="boolean", nullable=false)
     */
    private $glpiDocumentsItems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_documenttypes", type="boolean", nullable=false)
     */
    private $glpiDocumenttypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_domains", type="boolean", nullable=false)
     */
    private $glpiDomains;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_entities", type="boolean", nullable=false)
     */
    private $glpiEntities;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_entities_reminders", type="boolean", nullable=false)
     */
    private $glpiEntitiesReminders;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_entitydatas", type="boolean", nullable=false)
     */
    private $glpiEntitydatas;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_filesystems", type="boolean", nullable=false)
     */
    private $glpiFilesystems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_groups", type="boolean", nullable=false)
     */
    private $glpiGroups;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_groups_problems", type="boolean", nullable=false)
     */
    private $glpiGroupsProblems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_groups_reminders", type="boolean", nullable=false)
     */
    private $glpiGroupsReminders;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_groups_tickets", type="boolean", nullable=false)
     */
    private $glpiGroupsTickets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_groups_users", type="boolean", nullable=false)
     */
    private $glpiGroupsUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_holidays", type="boolean", nullable=false)
     */
    private $glpiHolidays;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_infocoms", type="boolean", nullable=false)
     */
    private $glpiInfocoms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_interfacetypes", type="boolean", nullable=false)
     */
    private $glpiInterfacetypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_items_problems", type="boolean", nullable=false)
     */
    private $glpiItemsProblems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_itilcategories", type="boolean", nullable=false)
     */
    private $glpiItilcategories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_knowbaseitemcategories", type="boolean", nullable=false)
     */
    private $glpiKnowbaseitemcategories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_knowbaseitems", type="boolean", nullable=false)
     */
    private $glpiKnowbaseitems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_links", type="boolean", nullable=false)
     */
    private $glpiLinks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_links_itemtypes", type="boolean", nullable=false)
     */
    private $glpiLinksItemtypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_locations", type="boolean", nullable=false)
     */
    private $glpiLocations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_manufacturers", type="boolean", nullable=false)
     */
    private $glpiManufacturers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_monitormodels", type="boolean", nullable=false)
     */
    private $glpiMonitormodels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_monitors", type="boolean", nullable=false)
     */
    private $glpiMonitors;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_monitortypes", type="boolean", nullable=false)
     */
    private $glpiMonitortypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_netpoints", type="boolean", nullable=false)
     */
    private $glpiNetpoints;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkequipmentfirmwares", type="boolean", nullable=false)
     */
    private $glpiNetworkequipmentfirmwares;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkequipmentmodels", type="boolean", nullable=false)
     */
    private $glpiNetworkequipmentmodels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkequipments", type="boolean", nullable=false)
     */
    private $glpiNetworkequipments;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkequipmenttypes", type="boolean", nullable=false)
     */
    private $glpiNetworkequipmenttypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkinterfaces", type="boolean", nullable=false)
     */
    private $glpiNetworkinterfaces;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkports", type="boolean", nullable=false)
     */
    private $glpiNetworkports;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkports_networkports", type="boolean", nullable=false)
     */
    private $glpiNetworkportsNetworkports;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networkports_vlans", type="boolean", nullable=false)
     */
    private $glpiNetworkportsVlans;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_networks", type="boolean", nullable=false)
     */
    private $glpiNetworks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_operatingsystems", type="boolean", nullable=false)
     */
    private $glpiOperatingsystems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_operatingsystemservicepacks", type="boolean", nullable=false)
     */
    private $glpiOperatingsystemservicepacks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_operatingsystemversions", type="boolean", nullable=false)
     */
    private $glpiOperatingsystemversions;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_peripheralmodels", type="boolean", nullable=false)
     */
    private $glpiPeripheralmodels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_peripherals", type="boolean", nullable=false)
     */
    private $glpiPeripherals;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_peripheraltypes", type="boolean", nullable=false)
     */
    private $glpiPeripheraltypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_phonemodels", type="boolean", nullable=false)
     */
    private $glpiPhonemodels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_phonepowersupplies", type="boolean", nullable=false)
     */
    private $glpiPhonepowersupplies;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_phones", type="boolean", nullable=false)
     */
    private $glpiPhones;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_phonetypes", type="boolean", nullable=false)
     */
    private $glpiPhonetypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_printermodels", type="boolean", nullable=false)
     */
    private $glpiPrintermodels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_printers", type="boolean", nullable=false)
     */
    private $glpiPrinters;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_printertypes", type="boolean", nullable=false)
     */
    private $glpiPrintertypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_problems", type="boolean", nullable=false)
     */
    private $glpiProblems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_problems_tickets", type="boolean", nullable=false)
     */
    private $glpiProblemsTickets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_problems_users", type="boolean", nullable=false)
     */
    private $glpiProblemsUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_problemtasks", type="boolean", nullable=false)
     */
    private $glpiProblemtasks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_profiles", type="boolean", nullable=false)
     */
    private $glpiProfiles;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_profiles_reminders", type="boolean", nullable=false)
     */
    private $glpiProfilesReminders;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_profiles_users", type="boolean", nullable=false)
     */
    private $glpiProfilesUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_registrykeys", type="boolean", nullable=false)
     */
    private $glpiRegistrykeys;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_reminders", type="boolean", nullable=false)
     */
    private $glpiReminders;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_reminders_users", type="boolean", nullable=false)
     */
    private $glpiRemindersUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_reservationitems", type="boolean", nullable=false)
     */
    private $glpiReservationitems;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_reservations", type="boolean", nullable=false)
     */
    private $glpiReservations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_softwarecategories", type="boolean", nullable=false)
     */
    private $glpiSoftwarecategories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_softwarelicenses", type="boolean", nullable=false)
     */
    private $glpiSoftwarelicenses;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_softwarelicensetypes", type="boolean", nullable=false)
     */
    private $glpiSoftwarelicensetypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_softwares", type="boolean", nullable=false)
     */
    private $glpiSoftwares;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_softwareversions", type="boolean", nullable=false)
     */
    private $glpiSoftwareversions;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_solutiontypes", type="boolean", nullable=false)
     */
    private $glpiSolutiontypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_states", type="boolean", nullable=false)
     */
    private $glpiStates;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_suppliers", type="boolean", nullable=false)
     */
    private $glpiSuppliers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_suppliertypes", type="boolean", nullable=false)
     */
    private $glpiSuppliertypes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_taskcategories", type="boolean", nullable=false)
     */
    private $glpiTaskcategories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_ticketfollowups", type="boolean", nullable=false)
     */
    private $glpiTicketfollowups;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_ticketrecurrents", type="boolean", nullable=false)
     */
    private $glpiTicketrecurrents;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_tickets", type="boolean", nullable=false)
     */
    private $glpiTickets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_ticketsatisfactions", type="boolean", nullable=false)
     */
    private $glpiTicketsatisfactions;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_tickets_tickets", type="boolean", nullable=false)
     */
    private $glpiTicketsTickets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_tickets_users", type="boolean", nullable=false)
     */
    private $glpiTicketsUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_tickettasks", type="boolean", nullable=false)
     */
    private $glpiTickettasks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_ticketvalidations", type="boolean", nullable=false)
     */
    private $glpiTicketvalidations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_usercategories", type="boolean", nullable=false)
     */
    private $glpiUsercategories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_useremails", type="boolean", nullable=false)
     */
    private $glpiUseremails;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_users", type="boolean", nullable=false)
     */
    private $glpiUsers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_usertitles", type="boolean", nullable=false)
     */
    private $glpiUsertitles;

    /**
     * @var boolean
     *
     * @ORM\Column(name="glpi_vlans", type="boolean", nullable=false)
     */
    private $glpiVlans;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set dateMod
     *
     * @param \DateTime $dateMod
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setDateMod($dateMod)
    {
        $this->dateMod = $dateMod;

        return $this;
    }

    /**
     * Get dateMod
     *
     * @return \DateTime 
     */
    public function getDateMod()
    {
        return $this->dateMod;
    }

    /**
     * Set glpiAlerts
     *
     * @param boolean $glpiAlerts
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiAlerts($glpiAlerts)
    {
        $this->glpiAlerts = $glpiAlerts;

        return $this;
    }

    /**
     * Get glpiAlerts
     *
     * @return boolean 
     */
    public function getGlpiAlerts()
    {
        return $this->glpiAlerts;
    }

    /**
     * Set glpiAuthldaps
     *
     * @param boolean $glpiAuthldaps
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiAuthldaps($glpiAuthldaps)
    {
        $this->glpiAuthldaps = $glpiAuthldaps;

        return $this;
    }

    /**
     * Get glpiAuthldaps
     *
     * @return boolean 
     */
    public function getGlpiAuthldaps()
    {
        return $this->glpiAuthldaps;
    }

    /**
     * Set glpiAuthmails
     *
     * @param boolean $glpiAuthmails
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiAuthmails($glpiAuthmails)
    {
        $this->glpiAuthmails = $glpiAuthmails;

        return $this;
    }

    /**
     * Get glpiAuthmails
     *
     * @return boolean 
     */
    public function getGlpiAuthmails()
    {
        return $this->glpiAuthmails;
    }

    /**
     * Set glpiAutoupdatesystems
     *
     * @param boolean $glpiAutoupdatesystems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiAutoupdatesystems($glpiAutoupdatesystems)
    {
        $this->glpiAutoupdatesystems = $glpiAutoupdatesystems;

        return $this;
    }

    /**
     * Get glpiAutoupdatesystems
     *
     * @return boolean 
     */
    public function getGlpiAutoupdatesystems()
    {
        return $this->glpiAutoupdatesystems;
    }

    /**
     * Set glpiBookmarks
     *
     * @param boolean $glpiBookmarks
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiBookmarks($glpiBookmarks)
    {
        $this->glpiBookmarks = $glpiBookmarks;

        return $this;
    }

    /**
     * Get glpiBookmarks
     *
     * @return boolean 
     */
    public function getGlpiBookmarks()
    {
        return $this->glpiBookmarks;
    }

    /**
     * Set glpiBookmarksUsers
     *
     * @param boolean $glpiBookmarksUsers
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiBookmarksUsers($glpiBookmarksUsers)
    {
        $this->glpiBookmarksUsers = $glpiBookmarksUsers;

        return $this;
    }

    /**
     * Get glpiBookmarksUsers
     *
     * @return boolean 
     */
    public function getGlpiBookmarksUsers()
    {
        return $this->glpiBookmarksUsers;
    }

    /**
     * Set glpiBudgets
     *
     * @param boolean $glpiBudgets
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiBudgets($glpiBudgets)
    {
        $this->glpiBudgets = $glpiBudgets;

        return $this;
    }

    /**
     * Get glpiBudgets
     *
     * @return boolean 
     */
    public function getGlpiBudgets()
    {
        return $this->glpiBudgets;
    }

    /**
     * Set glpiCalendars
     *
     * @param boolean $glpiCalendars
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiCalendars($glpiCalendars)
    {
        $this->glpiCalendars = $glpiCalendars;

        return $this;
    }

    /**
     * Get glpiCalendars
     *
     * @return boolean 
     */
    public function getGlpiCalendars()
    {
        return $this->glpiCalendars;
    }

    /**
     * Set glpiCalendarsegments
     *
     * @param boolean $glpiCalendarsegments
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiCalendarsegments($glpiCalendarsegments)
    {
        $this->glpiCalendarsegments = $glpiCalendarsegments;

        return $this;
    }

    /**
     * Get glpiCalendarsegments
     *
     * @return boolean 
     */
    public function getGlpiCalendarsegments()
    {
        return $this->glpiCalendarsegments;
    }

    /**
     * Set glpiCalendarsHolidays
     *
     * @param boolean $glpiCalendarsHolidays
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiCalendarsHolidays($glpiCalendarsHolidays)
    {
        $this->glpiCalendarsHolidays = $glpiCalendarsHolidays;

        return $this;
    }

    /**
     * Get glpiCalendarsHolidays
     *
     * @return boolean 
     */
    public function getGlpiCalendarsHolidays()
    {
        return $this->glpiCalendarsHolidays;
    }

    /**
     * Set glpiCartridgeitems
     *
     * @param boolean $glpiCartridgeitems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiCartridgeitems($glpiCartridgeitems)
    {
        $this->glpiCartridgeitems = $glpiCartridgeitems;

        return $this;
    }

    /**
     * Get glpiCartridgeitems
     *
     * @return boolean 
     */
    public function getGlpiCartridgeitems()
    {
        return $this->glpiCartridgeitems;
    }

    /**
     * Set glpiCartridgeitemsPrintermodels
     *
     * @param boolean $glpiCartridgeitemsPrintermodels
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiCartridgeitemsPrintermodels($glpiCartridgeitemsPrintermodels)
    {
        $this->glpiCartridgeitemsPrintermodels = $glpiCartridgeitemsPrintermodels;

        return $this;
    }

    /**
     * Get glpiCartridgeitemsPrintermodels
     *
     * @return boolean 
     */
    public function getGlpiCartridgeitemsPrintermodels()
    {
        return $this->glpiCartridgeitemsPrintermodels;
    }

    /**
     * Set glpiCartridgeitemtypes
     *
     * @param boolean $glpiCartridgeitemtypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiCartridgeitemtypes($glpiCartridgeitemtypes)
    {
        $this->glpiCartridgeitemtypes = $glpiCartridgeitemtypes;

        return $this;
    }

    /**
     * Get glpiCartridgeitemtypes
     *
     * @return boolean 
     */
    public function getGlpiCartridgeitemtypes()
    {
        return $this->glpiCartridgeitemtypes;
    }

    /**
     * Set glpiCartridges
     *
     * @param boolean $glpiCartridges
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiCartridges($glpiCartridges)
    {
        $this->glpiCartridges = $glpiCartridges;

        return $this;
    }

    /**
     * Get glpiCartridges
     *
     * @return boolean 
     */
    public function getGlpiCartridges()
    {
        return $this->glpiCartridges;
    }

    /**
     * Set glpiChanges
     *
     * @param boolean $glpiChanges
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiChanges($glpiChanges)
    {
        $this->glpiChanges = $glpiChanges;

        return $this;
    }

    /**
     * Get glpiChanges
     *
     * @return boolean 
     */
    public function getGlpiChanges()
    {
        return $this->glpiChanges;
    }

    /**
     * Set glpiChangesGroups
     *
     * @param boolean $glpiChangesGroups
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiChangesGroups($glpiChangesGroups)
    {
        $this->glpiChangesGroups = $glpiChangesGroups;

        return $this;
    }

    /**
     * Get glpiChangesGroups
     *
     * @return boolean 
     */
    public function getGlpiChangesGroups()
    {
        return $this->glpiChangesGroups;
    }

    /**
     * Set glpiChangesItems
     *
     * @param boolean $glpiChangesItems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiChangesItems($glpiChangesItems)
    {
        $this->glpiChangesItems = $glpiChangesItems;

        return $this;
    }

    /**
     * Get glpiChangesItems
     *
     * @return boolean 
     */
    public function getGlpiChangesItems()
    {
        return $this->glpiChangesItems;
    }

    /**
     * Set glpiChangesProblems
     *
     * @param boolean $glpiChangesProblems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiChangesProblems($glpiChangesProblems)
    {
        $this->glpiChangesProblems = $glpiChangesProblems;

        return $this;
    }

    /**
     * Get glpiChangesProblems
     *
     * @return boolean 
     */
    public function getGlpiChangesProblems()
    {
        return $this->glpiChangesProblems;
    }

    /**
     * Set glpiChangesTickets
     *
     * @param boolean $glpiChangesTickets
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiChangesTickets($glpiChangesTickets)
    {
        $this->glpiChangesTickets = $glpiChangesTickets;

        return $this;
    }

    /**
     * Get glpiChangesTickets
     *
     * @return boolean 
     */
    public function getGlpiChangesTickets()
    {
        return $this->glpiChangesTickets;
    }

    /**
     * Set glpiChangesUsers
     *
     * @param boolean $glpiChangesUsers
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiChangesUsers($glpiChangesUsers)
    {
        $this->glpiChangesUsers = $glpiChangesUsers;

        return $this;
    }

    /**
     * Get glpiChangesUsers
     *
     * @return boolean 
     */
    public function getGlpiChangesUsers()
    {
        return $this->glpiChangesUsers;
    }

    /**
     * Set glpiChangetasks
     *
     * @param boolean $glpiChangetasks
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiChangetasks($glpiChangetasks)
    {
        $this->glpiChangetasks = $glpiChangetasks;

        return $this;
    }

    /**
     * Get glpiChangetasks
     *
     * @return boolean 
     */
    public function getGlpiChangetasks()
    {
        return $this->glpiChangetasks;
    }

    /**
     * Set glpiComputerdisks
     *
     * @param boolean $glpiComputerdisks
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputerdisks($glpiComputerdisks)
    {
        $this->glpiComputerdisks = $glpiComputerdisks;

        return $this;
    }

    /**
     * Get glpiComputerdisks
     *
     * @return boolean 
     */
    public function getGlpiComputerdisks()
    {
        return $this->glpiComputerdisks;
    }

    /**
     * Set glpiComputermodels
     *
     * @param boolean $glpiComputermodels
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputermodels($glpiComputermodels)
    {
        $this->glpiComputermodels = $glpiComputermodels;

        return $this;
    }

    /**
     * Get glpiComputermodels
     *
     * @return boolean 
     */
    public function getGlpiComputermodels()
    {
        return $this->glpiComputermodels;
    }

    /**
     * Set glpiComputers
     *
     * @param boolean $glpiComputers
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputers($glpiComputers)
    {
        $this->glpiComputers = $glpiComputers;

        return $this;
    }

    /**
     * Get glpiComputers
     *
     * @return boolean 
     */
    public function getGlpiComputers()
    {
        return $this->glpiComputers;
    }

    /**
     * Set glpiComputersDevicecases
     *
     * @param boolean $glpiComputersDevicecases
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersDevicecases($glpiComputersDevicecases)
    {
        $this->glpiComputersDevicecases = $glpiComputersDevicecases;

        return $this;
    }

    /**
     * Get glpiComputersDevicecases
     *
     * @return boolean 
     */
    public function getGlpiComputersDevicecases()
    {
        return $this->glpiComputersDevicecases;
    }

    /**
     * Set glpiComputersDevicecontrols
     *
     * @param boolean $glpiComputersDevicecontrols
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersDevicecontrols($glpiComputersDevicecontrols)
    {
        $this->glpiComputersDevicecontrols = $glpiComputersDevicecontrols;

        return $this;
    }

    /**
     * Get glpiComputersDevicecontrols
     *
     * @return boolean 
     */
    public function getGlpiComputersDevicecontrols()
    {
        return $this->glpiComputersDevicecontrols;
    }

    /**
     * Set glpiComputersDevicedrives
     *
     * @param boolean $glpiComputersDevicedrives
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersDevicedrives($glpiComputersDevicedrives)
    {
        $this->glpiComputersDevicedrives = $glpiComputersDevicedrives;

        return $this;
    }

    /**
     * Get glpiComputersDevicedrives
     *
     * @return boolean 
     */
    public function getGlpiComputersDevicedrives()
    {
        return $this->glpiComputersDevicedrives;
    }

    /**
     * Set glpiComputersDevicegraphiccards
     *
     * @param boolean $glpiComputersDevicegraphiccards
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersDevicegraphiccards($glpiComputersDevicegraphiccards)
    {
        $this->glpiComputersDevicegraphiccards = $glpiComputersDevicegraphiccards;

        return $this;
    }

    /**
     * Get glpiComputersDevicegraphiccards
     *
     * @return boolean 
     */
    public function getGlpiComputersDevicegraphiccards()
    {
        return $this->glpiComputersDevicegraphiccards;
    }

    /**
     * Set glpiComputersDeviceharddrives
     *
     * @param boolean $glpiComputersDeviceharddrives
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersDeviceharddrives($glpiComputersDeviceharddrives)
    {
        $this->glpiComputersDeviceharddrives = $glpiComputersDeviceharddrives;

        return $this;
    }

    /**
     * Get glpiComputersDeviceharddrives
     *
     * @return boolean 
     */
    public function getGlpiComputersDeviceharddrives()
    {
        return $this->glpiComputersDeviceharddrives;
    }

    /**
     * Set glpiComputersDevicememories
     *
     * @param boolean $glpiComputersDevicememories
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersDevicememories($glpiComputersDevicememories)
    {
        $this->glpiComputersDevicememories = $glpiComputersDevicememories;

        return $this;
    }

    /**
     * Get glpiComputersDevicememories
     *
     * @return boolean 
     */
    public function getGlpiComputersDevicememories()
    {
        return $this->glpiComputersDevicememories;
    }

    /**
     * Set glpiComputersDevicemotherboards
     *
     * @param boolean $glpiComputersDevicemotherboards
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersDevicemotherboards($glpiComputersDevicemotherboards)
    {
        $this->glpiComputersDevicemotherboards = $glpiComputersDevicemotherboards;

        return $this;
    }

    /**
     * Get glpiComputersDevicemotherboards
     *
     * @return boolean 
     */
    public function getGlpiComputersDevicemotherboards()
    {
        return $this->glpiComputersDevicemotherboards;
    }

    /**
     * Set glpiComputersDevicenetworkcards
     *
     * @param boolean $glpiComputersDevicenetworkcards
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersDevicenetworkcards($glpiComputersDevicenetworkcards)
    {
        $this->glpiComputersDevicenetworkcards = $glpiComputersDevicenetworkcards;

        return $this;
    }

    /**
     * Get glpiComputersDevicenetworkcards
     *
     * @return boolean 
     */
    public function getGlpiComputersDevicenetworkcards()
    {
        return $this->glpiComputersDevicenetworkcards;
    }

    /**
     * Set glpiComputersDevicepcis
     *
     * @param boolean $glpiComputersDevicepcis
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersDevicepcis($glpiComputersDevicepcis)
    {
        $this->glpiComputersDevicepcis = $glpiComputersDevicepcis;

        return $this;
    }

    /**
     * Get glpiComputersDevicepcis
     *
     * @return boolean 
     */
    public function getGlpiComputersDevicepcis()
    {
        return $this->glpiComputersDevicepcis;
    }

    /**
     * Set glpiComputersDevicepowersupplies
     *
     * @param boolean $glpiComputersDevicepowersupplies
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersDevicepowersupplies($glpiComputersDevicepowersupplies)
    {
        $this->glpiComputersDevicepowersupplies = $glpiComputersDevicepowersupplies;

        return $this;
    }

    /**
     * Get glpiComputersDevicepowersupplies
     *
     * @return boolean 
     */
    public function getGlpiComputersDevicepowersupplies()
    {
        return $this->glpiComputersDevicepowersupplies;
    }

    /**
     * Set glpiComputersDeviceprocessors
     *
     * @param boolean $glpiComputersDeviceprocessors
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersDeviceprocessors($glpiComputersDeviceprocessors)
    {
        $this->glpiComputersDeviceprocessors = $glpiComputersDeviceprocessors;

        return $this;
    }

    /**
     * Get glpiComputersDeviceprocessors
     *
     * @return boolean 
     */
    public function getGlpiComputersDeviceprocessors()
    {
        return $this->glpiComputersDeviceprocessors;
    }

    /**
     * Set glpiComputersDevicesoundcards
     *
     * @param boolean $glpiComputersDevicesoundcards
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersDevicesoundcards($glpiComputersDevicesoundcards)
    {
        $this->glpiComputersDevicesoundcards = $glpiComputersDevicesoundcards;

        return $this;
    }

    /**
     * Get glpiComputersDevicesoundcards
     *
     * @return boolean 
     */
    public function getGlpiComputersDevicesoundcards()
    {
        return $this->glpiComputersDevicesoundcards;
    }

    /**
     * Set glpiComputersItems
     *
     * @param boolean $glpiComputersItems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersItems($glpiComputersItems)
    {
        $this->glpiComputersItems = $glpiComputersItems;

        return $this;
    }

    /**
     * Get glpiComputersItems
     *
     * @return boolean 
     */
    public function getGlpiComputersItems()
    {
        return $this->glpiComputersItems;
    }

    /**
     * Set glpiComputersSoftwarelicenses
     *
     * @param boolean $glpiComputersSoftwarelicenses
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersSoftwarelicenses($glpiComputersSoftwarelicenses)
    {
        $this->glpiComputersSoftwarelicenses = $glpiComputersSoftwarelicenses;

        return $this;
    }

    /**
     * Get glpiComputersSoftwarelicenses
     *
     * @return boolean 
     */
    public function getGlpiComputersSoftwarelicenses()
    {
        return $this->glpiComputersSoftwarelicenses;
    }

    /**
     * Set glpiComputersSoftwareversions
     *
     * @param boolean $glpiComputersSoftwareversions
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputersSoftwareversions($glpiComputersSoftwareversions)
    {
        $this->glpiComputersSoftwareversions = $glpiComputersSoftwareversions;

        return $this;
    }

    /**
     * Get glpiComputersSoftwareversions
     *
     * @return boolean 
     */
    public function getGlpiComputersSoftwareversions()
    {
        return $this->glpiComputersSoftwareversions;
    }

    /**
     * Set glpiComputertypes
     *
     * @param boolean $glpiComputertypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputertypes($glpiComputertypes)
    {
        $this->glpiComputertypes = $glpiComputertypes;

        return $this;
    }

    /**
     * Get glpiComputertypes
     *
     * @return boolean 
     */
    public function getGlpiComputertypes()
    {
        return $this->glpiComputertypes;
    }

    /**
     * Set glpiComputervirtualmachines
     *
     * @param boolean $glpiComputervirtualmachines
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiComputervirtualmachines($glpiComputervirtualmachines)
    {
        $this->glpiComputervirtualmachines = $glpiComputervirtualmachines;

        return $this;
    }

    /**
     * Get glpiComputervirtualmachines
     *
     * @return boolean 
     */
    public function getGlpiComputervirtualmachines()
    {
        return $this->glpiComputervirtualmachines;
    }

    /**
     * Set glpiConsumableitems
     *
     * @param boolean $glpiConsumableitems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiConsumableitems($glpiConsumableitems)
    {
        $this->glpiConsumableitems = $glpiConsumableitems;

        return $this;
    }

    /**
     * Get glpiConsumableitems
     *
     * @return boolean 
     */
    public function getGlpiConsumableitems()
    {
        return $this->glpiConsumableitems;
    }

    /**
     * Set glpiConsumableitemtypes
     *
     * @param boolean $glpiConsumableitemtypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiConsumableitemtypes($glpiConsumableitemtypes)
    {
        $this->glpiConsumableitemtypes = $glpiConsumableitemtypes;

        return $this;
    }

    /**
     * Get glpiConsumableitemtypes
     *
     * @return boolean 
     */
    public function getGlpiConsumableitemtypes()
    {
        return $this->glpiConsumableitemtypes;
    }

    /**
     * Set glpiConsumables
     *
     * @param boolean $glpiConsumables
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiConsumables($glpiConsumables)
    {
        $this->glpiConsumables = $glpiConsumables;

        return $this;
    }

    /**
     * Get glpiConsumables
     *
     * @return boolean 
     */
    public function getGlpiConsumables()
    {
        return $this->glpiConsumables;
    }

    /**
     * Set glpiContacts
     *
     * @param boolean $glpiContacts
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiContacts($glpiContacts)
    {
        $this->glpiContacts = $glpiContacts;

        return $this;
    }

    /**
     * Get glpiContacts
     *
     * @return boolean 
     */
    public function getGlpiContacts()
    {
        return $this->glpiContacts;
    }

    /**
     * Set glpiContactsSuppliers
     *
     * @param boolean $glpiContactsSuppliers
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiContactsSuppliers($glpiContactsSuppliers)
    {
        $this->glpiContactsSuppliers = $glpiContactsSuppliers;

        return $this;
    }

    /**
     * Get glpiContactsSuppliers
     *
     * @return boolean 
     */
    public function getGlpiContactsSuppliers()
    {
        return $this->glpiContactsSuppliers;
    }

    /**
     * Set glpiContacttypes
     *
     * @param boolean $glpiContacttypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiContacttypes($glpiContacttypes)
    {
        $this->glpiContacttypes = $glpiContacttypes;

        return $this;
    }

    /**
     * Get glpiContacttypes
     *
     * @return boolean 
     */
    public function getGlpiContacttypes()
    {
        return $this->glpiContacttypes;
    }

    /**
     * Set glpiContracts
     *
     * @param boolean $glpiContracts
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiContracts($glpiContracts)
    {
        $this->glpiContracts = $glpiContracts;

        return $this;
    }

    /**
     * Get glpiContracts
     *
     * @return boolean 
     */
    public function getGlpiContracts()
    {
        return $this->glpiContracts;
    }

    /**
     * Set glpiContractsItems
     *
     * @param boolean $glpiContractsItems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiContractsItems($glpiContractsItems)
    {
        $this->glpiContractsItems = $glpiContractsItems;

        return $this;
    }

    /**
     * Get glpiContractsItems
     *
     * @return boolean 
     */
    public function getGlpiContractsItems()
    {
        return $this->glpiContractsItems;
    }

    /**
     * Set glpiContractsSuppliers
     *
     * @param boolean $glpiContractsSuppliers
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiContractsSuppliers($glpiContractsSuppliers)
    {
        $this->glpiContractsSuppliers = $glpiContractsSuppliers;

        return $this;
    }

    /**
     * Get glpiContractsSuppliers
     *
     * @return boolean 
     */
    public function getGlpiContractsSuppliers()
    {
        return $this->glpiContractsSuppliers;
    }

    /**
     * Set glpiContracttypes
     *
     * @param boolean $glpiContracttypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiContracttypes($glpiContracttypes)
    {
        $this->glpiContracttypes = $glpiContracttypes;

        return $this;
    }

    /**
     * Get glpiContracttypes
     *
     * @return boolean 
     */
    public function getGlpiContracttypes()
    {
        return $this->glpiContracttypes;
    }

    /**
     * Set glpiDevicecases
     *
     * @param boolean $glpiDevicecases
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDevicecases($glpiDevicecases)
    {
        $this->glpiDevicecases = $glpiDevicecases;

        return $this;
    }

    /**
     * Get glpiDevicecases
     *
     * @return boolean 
     */
    public function getGlpiDevicecases()
    {
        return $this->glpiDevicecases;
    }

    /**
     * Set glpiDevicecasetypes
     *
     * @param boolean $glpiDevicecasetypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDevicecasetypes($glpiDevicecasetypes)
    {
        $this->glpiDevicecasetypes = $glpiDevicecasetypes;

        return $this;
    }

    /**
     * Get glpiDevicecasetypes
     *
     * @return boolean 
     */
    public function getGlpiDevicecasetypes()
    {
        return $this->glpiDevicecasetypes;
    }

    /**
     * Set glpiDevicecontrols
     *
     * @param boolean $glpiDevicecontrols
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDevicecontrols($glpiDevicecontrols)
    {
        $this->glpiDevicecontrols = $glpiDevicecontrols;

        return $this;
    }

    /**
     * Get glpiDevicecontrols
     *
     * @return boolean 
     */
    public function getGlpiDevicecontrols()
    {
        return $this->glpiDevicecontrols;
    }

    /**
     * Set glpiDevicedrives
     *
     * @param boolean $glpiDevicedrives
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDevicedrives($glpiDevicedrives)
    {
        $this->glpiDevicedrives = $glpiDevicedrives;

        return $this;
    }

    /**
     * Get glpiDevicedrives
     *
     * @return boolean 
     */
    public function getGlpiDevicedrives()
    {
        return $this->glpiDevicedrives;
    }

    /**
     * Set glpiDevicegraphiccards
     *
     * @param boolean $glpiDevicegraphiccards
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDevicegraphiccards($glpiDevicegraphiccards)
    {
        $this->glpiDevicegraphiccards = $glpiDevicegraphiccards;

        return $this;
    }

    /**
     * Get glpiDevicegraphiccards
     *
     * @return boolean 
     */
    public function getGlpiDevicegraphiccards()
    {
        return $this->glpiDevicegraphiccards;
    }

    /**
     * Set glpiDeviceharddrives
     *
     * @param boolean $glpiDeviceharddrives
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDeviceharddrives($glpiDeviceharddrives)
    {
        $this->glpiDeviceharddrives = $glpiDeviceharddrives;

        return $this;
    }

    /**
     * Get glpiDeviceharddrives
     *
     * @return boolean 
     */
    public function getGlpiDeviceharddrives()
    {
        return $this->glpiDeviceharddrives;
    }

    /**
     * Set glpiDevicememories
     *
     * @param boolean $glpiDevicememories
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDevicememories($glpiDevicememories)
    {
        $this->glpiDevicememories = $glpiDevicememories;

        return $this;
    }

    /**
     * Get glpiDevicememories
     *
     * @return boolean 
     */
    public function getGlpiDevicememories()
    {
        return $this->glpiDevicememories;
    }

    /**
     * Set glpiDevicememorytypes
     *
     * @param boolean $glpiDevicememorytypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDevicememorytypes($glpiDevicememorytypes)
    {
        $this->glpiDevicememorytypes = $glpiDevicememorytypes;

        return $this;
    }

    /**
     * Get glpiDevicememorytypes
     *
     * @return boolean 
     */
    public function getGlpiDevicememorytypes()
    {
        return $this->glpiDevicememorytypes;
    }

    /**
     * Set glpiDevicemotherboards
     *
     * @param boolean $glpiDevicemotherboards
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDevicemotherboards($glpiDevicemotherboards)
    {
        $this->glpiDevicemotherboards = $glpiDevicemotherboards;

        return $this;
    }

    /**
     * Get glpiDevicemotherboards
     *
     * @return boolean 
     */
    public function getGlpiDevicemotherboards()
    {
        return $this->glpiDevicemotherboards;
    }

    /**
     * Set glpiDevicenetworkcards
     *
     * @param boolean $glpiDevicenetworkcards
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDevicenetworkcards($glpiDevicenetworkcards)
    {
        $this->glpiDevicenetworkcards = $glpiDevicenetworkcards;

        return $this;
    }

    /**
     * Get glpiDevicenetworkcards
     *
     * @return boolean 
     */
    public function getGlpiDevicenetworkcards()
    {
        return $this->glpiDevicenetworkcards;
    }

    /**
     * Set glpiDevicepcis
     *
     * @param boolean $glpiDevicepcis
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDevicepcis($glpiDevicepcis)
    {
        $this->glpiDevicepcis = $glpiDevicepcis;

        return $this;
    }

    /**
     * Get glpiDevicepcis
     *
     * @return boolean 
     */
    public function getGlpiDevicepcis()
    {
        return $this->glpiDevicepcis;
    }

    /**
     * Set glpiDevicepowersupplies
     *
     * @param boolean $glpiDevicepowersupplies
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDevicepowersupplies($glpiDevicepowersupplies)
    {
        $this->glpiDevicepowersupplies = $glpiDevicepowersupplies;

        return $this;
    }

    /**
     * Get glpiDevicepowersupplies
     *
     * @return boolean 
     */
    public function getGlpiDevicepowersupplies()
    {
        return $this->glpiDevicepowersupplies;
    }

    /**
     * Set glpiDeviceprocessors
     *
     * @param boolean $glpiDeviceprocessors
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDeviceprocessors($glpiDeviceprocessors)
    {
        $this->glpiDeviceprocessors = $glpiDeviceprocessors;

        return $this;
    }

    /**
     * Get glpiDeviceprocessors
     *
     * @return boolean 
     */
    public function getGlpiDeviceprocessors()
    {
        return $this->glpiDeviceprocessors;
    }

    /**
     * Set glpiDevicesoundcards
     *
     * @param boolean $glpiDevicesoundcards
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDevicesoundcards($glpiDevicesoundcards)
    {
        $this->glpiDevicesoundcards = $glpiDevicesoundcards;

        return $this;
    }

    /**
     * Get glpiDevicesoundcards
     *
     * @return boolean 
     */
    public function getGlpiDevicesoundcards()
    {
        return $this->glpiDevicesoundcards;
    }

    /**
     * Set glpiDocumentcategories
     *
     * @param boolean $glpiDocumentcategories
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDocumentcategories($glpiDocumentcategories)
    {
        $this->glpiDocumentcategories = $glpiDocumentcategories;

        return $this;
    }

    /**
     * Get glpiDocumentcategories
     *
     * @return boolean 
     */
    public function getGlpiDocumentcategories()
    {
        return $this->glpiDocumentcategories;
    }

    /**
     * Set glpiDocuments
     *
     * @param boolean $glpiDocuments
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDocuments($glpiDocuments)
    {
        $this->glpiDocuments = $glpiDocuments;

        return $this;
    }

    /**
     * Get glpiDocuments
     *
     * @return boolean 
     */
    public function getGlpiDocuments()
    {
        return $this->glpiDocuments;
    }

    /**
     * Set glpiDocumentsItems
     *
     * @param boolean $glpiDocumentsItems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDocumentsItems($glpiDocumentsItems)
    {
        $this->glpiDocumentsItems = $glpiDocumentsItems;

        return $this;
    }

    /**
     * Get glpiDocumentsItems
     *
     * @return boolean 
     */
    public function getGlpiDocumentsItems()
    {
        return $this->glpiDocumentsItems;
    }

    /**
     * Set glpiDocumenttypes
     *
     * @param boolean $glpiDocumenttypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDocumenttypes($glpiDocumenttypes)
    {
        $this->glpiDocumenttypes = $glpiDocumenttypes;

        return $this;
    }

    /**
     * Get glpiDocumenttypes
     *
     * @return boolean 
     */
    public function getGlpiDocumenttypes()
    {
        return $this->glpiDocumenttypes;
    }

    /**
     * Set glpiDomains
     *
     * @param boolean $glpiDomains
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiDomains($glpiDomains)
    {
        $this->glpiDomains = $glpiDomains;

        return $this;
    }

    /**
     * Get glpiDomains
     *
     * @return boolean 
     */
    public function getGlpiDomains()
    {
        return $this->glpiDomains;
    }

    /**
     * Set glpiEntities
     *
     * @param boolean $glpiEntities
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiEntities($glpiEntities)
    {
        $this->glpiEntities = $glpiEntities;

        return $this;
    }

    /**
     * Get glpiEntities
     *
     * @return boolean 
     */
    public function getGlpiEntities()
    {
        return $this->glpiEntities;
    }

    /**
     * Set glpiEntitiesReminders
     *
     * @param boolean $glpiEntitiesReminders
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiEntitiesReminders($glpiEntitiesReminders)
    {
        $this->glpiEntitiesReminders = $glpiEntitiesReminders;

        return $this;
    }

    /**
     * Get glpiEntitiesReminders
     *
     * @return boolean 
     */
    public function getGlpiEntitiesReminders()
    {
        return $this->glpiEntitiesReminders;
    }

    /**
     * Set glpiEntitydatas
     *
     * @param boolean $glpiEntitydatas
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiEntitydatas($glpiEntitydatas)
    {
        $this->glpiEntitydatas = $glpiEntitydatas;

        return $this;
    }

    /**
     * Get glpiEntitydatas
     *
     * @return boolean 
     */
    public function getGlpiEntitydatas()
    {
        return $this->glpiEntitydatas;
    }

    /**
     * Set glpiFilesystems
     *
     * @param boolean $glpiFilesystems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiFilesystems($glpiFilesystems)
    {
        $this->glpiFilesystems = $glpiFilesystems;

        return $this;
    }

    /**
     * Get glpiFilesystems
     *
     * @return boolean 
     */
    public function getGlpiFilesystems()
    {
        return $this->glpiFilesystems;
    }

    /**
     * Set glpiGroups
     *
     * @param boolean $glpiGroups
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiGroups($glpiGroups)
    {
        $this->glpiGroups = $glpiGroups;

        return $this;
    }

    /**
     * Get glpiGroups
     *
     * @return boolean 
     */
    public function getGlpiGroups()
    {
        return $this->glpiGroups;
    }

    /**
     * Set glpiGroupsProblems
     *
     * @param boolean $glpiGroupsProblems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiGroupsProblems($glpiGroupsProblems)
    {
        $this->glpiGroupsProblems = $glpiGroupsProblems;

        return $this;
    }

    /**
     * Get glpiGroupsProblems
     *
     * @return boolean 
     */
    public function getGlpiGroupsProblems()
    {
        return $this->glpiGroupsProblems;
    }

    /**
     * Set glpiGroupsReminders
     *
     * @param boolean $glpiGroupsReminders
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiGroupsReminders($glpiGroupsReminders)
    {
        $this->glpiGroupsReminders = $glpiGroupsReminders;

        return $this;
    }

    /**
     * Get glpiGroupsReminders
     *
     * @return boolean 
     */
    public function getGlpiGroupsReminders()
    {
        return $this->glpiGroupsReminders;
    }

    /**
     * Set glpiGroupsTickets
     *
     * @param boolean $glpiGroupsTickets
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiGroupsTickets($glpiGroupsTickets)
    {
        $this->glpiGroupsTickets = $glpiGroupsTickets;

        return $this;
    }

    /**
     * Get glpiGroupsTickets
     *
     * @return boolean 
     */
    public function getGlpiGroupsTickets()
    {
        return $this->glpiGroupsTickets;
    }

    /**
     * Set glpiGroupsUsers
     *
     * @param boolean $glpiGroupsUsers
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiGroupsUsers($glpiGroupsUsers)
    {
        $this->glpiGroupsUsers = $glpiGroupsUsers;

        return $this;
    }

    /**
     * Get glpiGroupsUsers
     *
     * @return boolean 
     */
    public function getGlpiGroupsUsers()
    {
        return $this->glpiGroupsUsers;
    }

    /**
     * Set glpiHolidays
     *
     * @param boolean $glpiHolidays
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiHolidays($glpiHolidays)
    {
        $this->glpiHolidays = $glpiHolidays;

        return $this;
    }

    /**
     * Get glpiHolidays
     *
     * @return boolean 
     */
    public function getGlpiHolidays()
    {
        return $this->glpiHolidays;
    }

    /**
     * Set glpiInfocoms
     *
     * @param boolean $glpiInfocoms
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiInfocoms($glpiInfocoms)
    {
        $this->glpiInfocoms = $glpiInfocoms;

        return $this;
    }

    /**
     * Get glpiInfocoms
     *
     * @return boolean 
     */
    public function getGlpiInfocoms()
    {
        return $this->glpiInfocoms;
    }

    /**
     * Set glpiInterfacetypes
     *
     * @param boolean $glpiInterfacetypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiInterfacetypes($glpiInterfacetypes)
    {
        $this->glpiInterfacetypes = $glpiInterfacetypes;

        return $this;
    }

    /**
     * Get glpiInterfacetypes
     *
     * @return boolean 
     */
    public function getGlpiInterfacetypes()
    {
        return $this->glpiInterfacetypes;
    }

    /**
     * Set glpiItemsProblems
     *
     * @param boolean $glpiItemsProblems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiItemsProblems($glpiItemsProblems)
    {
        $this->glpiItemsProblems = $glpiItemsProblems;

        return $this;
    }

    /**
     * Get glpiItemsProblems
     *
     * @return boolean 
     */
    public function getGlpiItemsProblems()
    {
        return $this->glpiItemsProblems;
    }

    /**
     * Set glpiItilcategories
     *
     * @param boolean $glpiItilcategories
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiItilcategories($glpiItilcategories)
    {
        $this->glpiItilcategories = $glpiItilcategories;

        return $this;
    }

    /**
     * Get glpiItilcategories
     *
     * @return boolean 
     */
    public function getGlpiItilcategories()
    {
        return $this->glpiItilcategories;
    }

    /**
     * Set glpiKnowbaseitemcategories
     *
     * @param boolean $glpiKnowbaseitemcategories
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiKnowbaseitemcategories($glpiKnowbaseitemcategories)
    {
        $this->glpiKnowbaseitemcategories = $glpiKnowbaseitemcategories;

        return $this;
    }

    /**
     * Get glpiKnowbaseitemcategories
     *
     * @return boolean 
     */
    public function getGlpiKnowbaseitemcategories()
    {
        return $this->glpiKnowbaseitemcategories;
    }

    /**
     * Set glpiKnowbaseitems
     *
     * @param boolean $glpiKnowbaseitems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiKnowbaseitems($glpiKnowbaseitems)
    {
        $this->glpiKnowbaseitems = $glpiKnowbaseitems;

        return $this;
    }

    /**
     * Get glpiKnowbaseitems
     *
     * @return boolean 
     */
    public function getGlpiKnowbaseitems()
    {
        return $this->glpiKnowbaseitems;
    }

    /**
     * Set glpiLinks
     *
     * @param boolean $glpiLinks
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiLinks($glpiLinks)
    {
        $this->glpiLinks = $glpiLinks;

        return $this;
    }

    /**
     * Get glpiLinks
     *
     * @return boolean 
     */
    public function getGlpiLinks()
    {
        return $this->glpiLinks;
    }

    /**
     * Set glpiLinksItemtypes
     *
     * @param boolean $glpiLinksItemtypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiLinksItemtypes($glpiLinksItemtypes)
    {
        $this->glpiLinksItemtypes = $glpiLinksItemtypes;

        return $this;
    }

    /**
     * Get glpiLinksItemtypes
     *
     * @return boolean 
     */
    public function getGlpiLinksItemtypes()
    {
        return $this->glpiLinksItemtypes;
    }

    /**
     * Set glpiLocations
     *
     * @param boolean $glpiLocations
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiLocations($glpiLocations)
    {
        $this->glpiLocations = $glpiLocations;

        return $this;
    }

    /**
     * Get glpiLocations
     *
     * @return boolean 
     */
    public function getGlpiLocations()
    {
        return $this->glpiLocations;
    }

    /**
     * Set glpiManufacturers
     *
     * @param boolean $glpiManufacturers
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiManufacturers($glpiManufacturers)
    {
        $this->glpiManufacturers = $glpiManufacturers;

        return $this;
    }

    /**
     * Get glpiManufacturers
     *
     * @return boolean 
     */
    public function getGlpiManufacturers()
    {
        return $this->glpiManufacturers;
    }

    /**
     * Set glpiMonitormodels
     *
     * @param boolean $glpiMonitormodels
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiMonitormodels($glpiMonitormodels)
    {
        $this->glpiMonitormodels = $glpiMonitormodels;

        return $this;
    }

    /**
     * Get glpiMonitormodels
     *
     * @return boolean 
     */
    public function getGlpiMonitormodels()
    {
        return $this->glpiMonitormodels;
    }

    /**
     * Set glpiMonitors
     *
     * @param boolean $glpiMonitors
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiMonitors($glpiMonitors)
    {
        $this->glpiMonitors = $glpiMonitors;

        return $this;
    }

    /**
     * Get glpiMonitors
     *
     * @return boolean 
     */
    public function getGlpiMonitors()
    {
        return $this->glpiMonitors;
    }

    /**
     * Set glpiMonitortypes
     *
     * @param boolean $glpiMonitortypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiMonitortypes($glpiMonitortypes)
    {
        $this->glpiMonitortypes = $glpiMonitortypes;

        return $this;
    }

    /**
     * Get glpiMonitortypes
     *
     * @return boolean 
     */
    public function getGlpiMonitortypes()
    {
        return $this->glpiMonitortypes;
    }

    /**
     * Set glpiNetpoints
     *
     * @param boolean $glpiNetpoints
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiNetpoints($glpiNetpoints)
    {
        $this->glpiNetpoints = $glpiNetpoints;

        return $this;
    }

    /**
     * Get glpiNetpoints
     *
     * @return boolean 
     */
    public function getGlpiNetpoints()
    {
        return $this->glpiNetpoints;
    }

    /**
     * Set glpiNetworkequipmentfirmwares
     *
     * @param boolean $glpiNetworkequipmentfirmwares
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiNetworkequipmentfirmwares($glpiNetworkequipmentfirmwares)
    {
        $this->glpiNetworkequipmentfirmwares = $glpiNetworkequipmentfirmwares;

        return $this;
    }

    /**
     * Get glpiNetworkequipmentfirmwares
     *
     * @return boolean 
     */
    public function getGlpiNetworkequipmentfirmwares()
    {
        return $this->glpiNetworkequipmentfirmwares;
    }

    /**
     * Set glpiNetworkequipmentmodels
     *
     * @param boolean $glpiNetworkequipmentmodels
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiNetworkequipmentmodels($glpiNetworkequipmentmodels)
    {
        $this->glpiNetworkequipmentmodels = $glpiNetworkequipmentmodels;

        return $this;
    }

    /**
     * Get glpiNetworkequipmentmodels
     *
     * @return boolean 
     */
    public function getGlpiNetworkequipmentmodels()
    {
        return $this->glpiNetworkequipmentmodels;
    }

    /**
     * Set glpiNetworkequipments
     *
     * @param boolean $glpiNetworkequipments
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiNetworkequipments($glpiNetworkequipments)
    {
        $this->glpiNetworkequipments = $glpiNetworkequipments;

        return $this;
    }

    /**
     * Get glpiNetworkequipments
     *
     * @return boolean 
     */
    public function getGlpiNetworkequipments()
    {
        return $this->glpiNetworkequipments;
    }

    /**
     * Set glpiNetworkequipmenttypes
     *
     * @param boolean $glpiNetworkequipmenttypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiNetworkequipmenttypes($glpiNetworkequipmenttypes)
    {
        $this->glpiNetworkequipmenttypes = $glpiNetworkequipmenttypes;

        return $this;
    }

    /**
     * Get glpiNetworkequipmenttypes
     *
     * @return boolean 
     */
    public function getGlpiNetworkequipmenttypes()
    {
        return $this->glpiNetworkequipmenttypes;
    }

    /**
     * Set glpiNetworkinterfaces
     *
     * @param boolean $glpiNetworkinterfaces
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiNetworkinterfaces($glpiNetworkinterfaces)
    {
        $this->glpiNetworkinterfaces = $glpiNetworkinterfaces;

        return $this;
    }

    /**
     * Get glpiNetworkinterfaces
     *
     * @return boolean 
     */
    public function getGlpiNetworkinterfaces()
    {
        return $this->glpiNetworkinterfaces;
    }

    /**
     * Set glpiNetworkports
     *
     * @param boolean $glpiNetworkports
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiNetworkports($glpiNetworkports)
    {
        $this->glpiNetworkports = $glpiNetworkports;

        return $this;
    }

    /**
     * Get glpiNetworkports
     *
     * @return boolean 
     */
    public function getGlpiNetworkports()
    {
        return $this->glpiNetworkports;
    }

    /**
     * Set glpiNetworkportsNetworkports
     *
     * @param boolean $glpiNetworkportsNetworkports
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiNetworkportsNetworkports($glpiNetworkportsNetworkports)
    {
        $this->glpiNetworkportsNetworkports = $glpiNetworkportsNetworkports;

        return $this;
    }

    /**
     * Get glpiNetworkportsNetworkports
     *
     * @return boolean 
     */
    public function getGlpiNetworkportsNetworkports()
    {
        return $this->glpiNetworkportsNetworkports;
    }

    /**
     * Set glpiNetworkportsVlans
     *
     * @param boolean $glpiNetworkportsVlans
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiNetworkportsVlans($glpiNetworkportsVlans)
    {
        $this->glpiNetworkportsVlans = $glpiNetworkportsVlans;

        return $this;
    }

    /**
     * Get glpiNetworkportsVlans
     *
     * @return boolean 
     */
    public function getGlpiNetworkportsVlans()
    {
        return $this->glpiNetworkportsVlans;
    }

    /**
     * Set glpiNetworks
     *
     * @param boolean $glpiNetworks
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiNetworks($glpiNetworks)
    {
        $this->glpiNetworks = $glpiNetworks;

        return $this;
    }

    /**
     * Get glpiNetworks
     *
     * @return boolean 
     */
    public function getGlpiNetworks()
    {
        return $this->glpiNetworks;
    }

    /**
     * Set glpiOperatingsystems
     *
     * @param boolean $glpiOperatingsystems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiOperatingsystems($glpiOperatingsystems)
    {
        $this->glpiOperatingsystems = $glpiOperatingsystems;

        return $this;
    }

    /**
     * Get glpiOperatingsystems
     *
     * @return boolean 
     */
    public function getGlpiOperatingsystems()
    {
        return $this->glpiOperatingsystems;
    }

    /**
     * Set glpiOperatingsystemservicepacks
     *
     * @param boolean $glpiOperatingsystemservicepacks
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiOperatingsystemservicepacks($glpiOperatingsystemservicepacks)
    {
        $this->glpiOperatingsystemservicepacks = $glpiOperatingsystemservicepacks;

        return $this;
    }

    /**
     * Get glpiOperatingsystemservicepacks
     *
     * @return boolean 
     */
    public function getGlpiOperatingsystemservicepacks()
    {
        return $this->glpiOperatingsystemservicepacks;
    }

    /**
     * Set glpiOperatingsystemversions
     *
     * @param boolean $glpiOperatingsystemversions
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiOperatingsystemversions($glpiOperatingsystemversions)
    {
        $this->glpiOperatingsystemversions = $glpiOperatingsystemversions;

        return $this;
    }

    /**
     * Get glpiOperatingsystemversions
     *
     * @return boolean 
     */
    public function getGlpiOperatingsystemversions()
    {
        return $this->glpiOperatingsystemversions;
    }

    /**
     * Set glpiPeripheralmodels
     *
     * @param boolean $glpiPeripheralmodels
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiPeripheralmodels($glpiPeripheralmodels)
    {
        $this->glpiPeripheralmodels = $glpiPeripheralmodels;

        return $this;
    }

    /**
     * Get glpiPeripheralmodels
     *
     * @return boolean 
     */
    public function getGlpiPeripheralmodels()
    {
        return $this->glpiPeripheralmodels;
    }

    /**
     * Set glpiPeripherals
     *
     * @param boolean $glpiPeripherals
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiPeripherals($glpiPeripherals)
    {
        $this->glpiPeripherals = $glpiPeripherals;

        return $this;
    }

    /**
     * Get glpiPeripherals
     *
     * @return boolean 
     */
    public function getGlpiPeripherals()
    {
        return $this->glpiPeripherals;
    }

    /**
     * Set glpiPeripheraltypes
     *
     * @param boolean $glpiPeripheraltypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiPeripheraltypes($glpiPeripheraltypes)
    {
        $this->glpiPeripheraltypes = $glpiPeripheraltypes;

        return $this;
    }

    /**
     * Get glpiPeripheraltypes
     *
     * @return boolean 
     */
    public function getGlpiPeripheraltypes()
    {
        return $this->glpiPeripheraltypes;
    }

    /**
     * Set glpiPhonemodels
     *
     * @param boolean $glpiPhonemodels
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiPhonemodels($glpiPhonemodels)
    {
        $this->glpiPhonemodels = $glpiPhonemodels;

        return $this;
    }

    /**
     * Get glpiPhonemodels
     *
     * @return boolean 
     */
    public function getGlpiPhonemodels()
    {
        return $this->glpiPhonemodels;
    }

    /**
     * Set glpiPhonepowersupplies
     *
     * @param boolean $glpiPhonepowersupplies
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiPhonepowersupplies($glpiPhonepowersupplies)
    {
        $this->glpiPhonepowersupplies = $glpiPhonepowersupplies;

        return $this;
    }

    /**
     * Get glpiPhonepowersupplies
     *
     * @return boolean 
     */
    public function getGlpiPhonepowersupplies()
    {
        return $this->glpiPhonepowersupplies;
    }

    /**
     * Set glpiPhones
     *
     * @param boolean $glpiPhones
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiPhones($glpiPhones)
    {
        $this->glpiPhones = $glpiPhones;

        return $this;
    }

    /**
     * Get glpiPhones
     *
     * @return boolean 
     */
    public function getGlpiPhones()
    {
        return $this->glpiPhones;
    }

    /**
     * Set glpiPhonetypes
     *
     * @param boolean $glpiPhonetypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiPhonetypes($glpiPhonetypes)
    {
        $this->glpiPhonetypes = $glpiPhonetypes;

        return $this;
    }

    /**
     * Get glpiPhonetypes
     *
     * @return boolean 
     */
    public function getGlpiPhonetypes()
    {
        return $this->glpiPhonetypes;
    }

    /**
     * Set glpiPrintermodels
     *
     * @param boolean $glpiPrintermodels
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiPrintermodels($glpiPrintermodels)
    {
        $this->glpiPrintermodels = $glpiPrintermodels;

        return $this;
    }

    /**
     * Get glpiPrintermodels
     *
     * @return boolean 
     */
    public function getGlpiPrintermodels()
    {
        return $this->glpiPrintermodels;
    }

    /**
     * Set glpiPrinters
     *
     * @param boolean $glpiPrinters
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiPrinters($glpiPrinters)
    {
        $this->glpiPrinters = $glpiPrinters;

        return $this;
    }

    /**
     * Get glpiPrinters
     *
     * @return boolean 
     */
    public function getGlpiPrinters()
    {
        return $this->glpiPrinters;
    }

    /**
     * Set glpiPrintertypes
     *
     * @param boolean $glpiPrintertypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiPrintertypes($glpiPrintertypes)
    {
        $this->glpiPrintertypes = $glpiPrintertypes;

        return $this;
    }

    /**
     * Get glpiPrintertypes
     *
     * @return boolean 
     */
    public function getGlpiPrintertypes()
    {
        return $this->glpiPrintertypes;
    }

    /**
     * Set glpiProblems
     *
     * @param boolean $glpiProblems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiProblems($glpiProblems)
    {
        $this->glpiProblems = $glpiProblems;

        return $this;
    }

    /**
     * Get glpiProblems
     *
     * @return boolean 
     */
    public function getGlpiProblems()
    {
        return $this->glpiProblems;
    }

    /**
     * Set glpiProblemsTickets
     *
     * @param boolean $glpiProblemsTickets
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiProblemsTickets($glpiProblemsTickets)
    {
        $this->glpiProblemsTickets = $glpiProblemsTickets;

        return $this;
    }

    /**
     * Get glpiProblemsTickets
     *
     * @return boolean 
     */
    public function getGlpiProblemsTickets()
    {
        return $this->glpiProblemsTickets;
    }

    /**
     * Set glpiProblemsUsers
     *
     * @param boolean $glpiProblemsUsers
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiProblemsUsers($glpiProblemsUsers)
    {
        $this->glpiProblemsUsers = $glpiProblemsUsers;

        return $this;
    }

    /**
     * Get glpiProblemsUsers
     *
     * @return boolean 
     */
    public function getGlpiProblemsUsers()
    {
        return $this->glpiProblemsUsers;
    }

    /**
     * Set glpiProblemtasks
     *
     * @param boolean $glpiProblemtasks
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiProblemtasks($glpiProblemtasks)
    {
        $this->glpiProblemtasks = $glpiProblemtasks;

        return $this;
    }

    /**
     * Get glpiProblemtasks
     *
     * @return boolean 
     */
    public function getGlpiProblemtasks()
    {
        return $this->glpiProblemtasks;
    }

    /**
     * Set glpiProfiles
     *
     * @param boolean $glpiProfiles
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiProfiles($glpiProfiles)
    {
        $this->glpiProfiles = $glpiProfiles;

        return $this;
    }

    /**
     * Get glpiProfiles
     *
     * @return boolean 
     */
    public function getGlpiProfiles()
    {
        return $this->glpiProfiles;
    }

    /**
     * Set glpiProfilesReminders
     *
     * @param boolean $glpiProfilesReminders
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiProfilesReminders($glpiProfilesReminders)
    {
        $this->glpiProfilesReminders = $glpiProfilesReminders;

        return $this;
    }

    /**
     * Get glpiProfilesReminders
     *
     * @return boolean 
     */
    public function getGlpiProfilesReminders()
    {
        return $this->glpiProfilesReminders;
    }

    /**
     * Set glpiProfilesUsers
     *
     * @param boolean $glpiProfilesUsers
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiProfilesUsers($glpiProfilesUsers)
    {
        $this->glpiProfilesUsers = $glpiProfilesUsers;

        return $this;
    }

    /**
     * Get glpiProfilesUsers
     *
     * @return boolean 
     */
    public function getGlpiProfilesUsers()
    {
        return $this->glpiProfilesUsers;
    }

    /**
     * Set glpiRegistrykeys
     *
     * @param boolean $glpiRegistrykeys
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiRegistrykeys($glpiRegistrykeys)
    {
        $this->glpiRegistrykeys = $glpiRegistrykeys;

        return $this;
    }

    /**
     * Get glpiRegistrykeys
     *
     * @return boolean 
     */
    public function getGlpiRegistrykeys()
    {
        return $this->glpiRegistrykeys;
    }

    /**
     * Set glpiReminders
     *
     * @param boolean $glpiReminders
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiReminders($glpiReminders)
    {
        $this->glpiReminders = $glpiReminders;

        return $this;
    }

    /**
     * Get glpiReminders
     *
     * @return boolean 
     */
    public function getGlpiReminders()
    {
        return $this->glpiReminders;
    }

    /**
     * Set glpiRemindersUsers
     *
     * @param boolean $glpiRemindersUsers
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiRemindersUsers($glpiRemindersUsers)
    {
        $this->glpiRemindersUsers = $glpiRemindersUsers;

        return $this;
    }

    /**
     * Get glpiRemindersUsers
     *
     * @return boolean 
     */
    public function getGlpiRemindersUsers()
    {
        return $this->glpiRemindersUsers;
    }

    /**
     * Set glpiReservationitems
     *
     * @param boolean $glpiReservationitems
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiReservationitems($glpiReservationitems)
    {
        $this->glpiReservationitems = $glpiReservationitems;

        return $this;
    }

    /**
     * Get glpiReservationitems
     *
     * @return boolean 
     */
    public function getGlpiReservationitems()
    {
        return $this->glpiReservationitems;
    }

    /**
     * Set glpiReservations
     *
     * @param boolean $glpiReservations
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiReservations($glpiReservations)
    {
        $this->glpiReservations = $glpiReservations;

        return $this;
    }

    /**
     * Get glpiReservations
     *
     * @return boolean 
     */
    public function getGlpiReservations()
    {
        return $this->glpiReservations;
    }

    /**
     * Set glpiSoftwarecategories
     *
     * @param boolean $glpiSoftwarecategories
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiSoftwarecategories($glpiSoftwarecategories)
    {
        $this->glpiSoftwarecategories = $glpiSoftwarecategories;

        return $this;
    }

    /**
     * Get glpiSoftwarecategories
     *
     * @return boolean 
     */
    public function getGlpiSoftwarecategories()
    {
        return $this->glpiSoftwarecategories;
    }

    /**
     * Set glpiSoftwarelicenses
     *
     * @param boolean $glpiSoftwarelicenses
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiSoftwarelicenses($glpiSoftwarelicenses)
    {
        $this->glpiSoftwarelicenses = $glpiSoftwarelicenses;

        return $this;
    }

    /**
     * Get glpiSoftwarelicenses
     *
     * @return boolean 
     */
    public function getGlpiSoftwarelicenses()
    {
        return $this->glpiSoftwarelicenses;
    }

    /**
     * Set glpiSoftwarelicensetypes
     *
     * @param boolean $glpiSoftwarelicensetypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiSoftwarelicensetypes($glpiSoftwarelicensetypes)
    {
        $this->glpiSoftwarelicensetypes = $glpiSoftwarelicensetypes;

        return $this;
    }

    /**
     * Get glpiSoftwarelicensetypes
     *
     * @return boolean 
     */
    public function getGlpiSoftwarelicensetypes()
    {
        return $this->glpiSoftwarelicensetypes;
    }

    /**
     * Set glpiSoftwares
     *
     * @param boolean $glpiSoftwares
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiSoftwares($glpiSoftwares)
    {
        $this->glpiSoftwares = $glpiSoftwares;

        return $this;
    }

    /**
     * Get glpiSoftwares
     *
     * @return boolean 
     */
    public function getGlpiSoftwares()
    {
        return $this->glpiSoftwares;
    }

    /**
     * Set glpiSoftwareversions
     *
     * @param boolean $glpiSoftwareversions
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiSoftwareversions($glpiSoftwareversions)
    {
        $this->glpiSoftwareversions = $glpiSoftwareversions;

        return $this;
    }

    /**
     * Get glpiSoftwareversions
     *
     * @return boolean 
     */
    public function getGlpiSoftwareversions()
    {
        return $this->glpiSoftwareversions;
    }

    /**
     * Set glpiSolutiontypes
     *
     * @param boolean $glpiSolutiontypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiSolutiontypes($glpiSolutiontypes)
    {
        $this->glpiSolutiontypes = $glpiSolutiontypes;

        return $this;
    }

    /**
     * Get glpiSolutiontypes
     *
     * @return boolean 
     */
    public function getGlpiSolutiontypes()
    {
        return $this->glpiSolutiontypes;
    }

    /**
     * Set glpiStates
     *
     * @param boolean $glpiStates
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiStates($glpiStates)
    {
        $this->glpiStates = $glpiStates;

        return $this;
    }

    /**
     * Get glpiStates
     *
     * @return boolean 
     */
    public function getGlpiStates()
    {
        return $this->glpiStates;
    }

    /**
     * Set glpiSuppliers
     *
     * @param boolean $glpiSuppliers
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiSuppliers($glpiSuppliers)
    {
        $this->glpiSuppliers = $glpiSuppliers;

        return $this;
    }

    /**
     * Get glpiSuppliers
     *
     * @return boolean 
     */
    public function getGlpiSuppliers()
    {
        return $this->glpiSuppliers;
    }

    /**
     * Set glpiSuppliertypes
     *
     * @param boolean $glpiSuppliertypes
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiSuppliertypes($glpiSuppliertypes)
    {
        $this->glpiSuppliertypes = $glpiSuppliertypes;

        return $this;
    }

    /**
     * Get glpiSuppliertypes
     *
     * @return boolean 
     */
    public function getGlpiSuppliertypes()
    {
        return $this->glpiSuppliertypes;
    }

    /**
     * Set glpiTaskcategories
     *
     * @param boolean $glpiTaskcategories
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiTaskcategories($glpiTaskcategories)
    {
        $this->glpiTaskcategories = $glpiTaskcategories;

        return $this;
    }

    /**
     * Get glpiTaskcategories
     *
     * @return boolean 
     */
    public function getGlpiTaskcategories()
    {
        return $this->glpiTaskcategories;
    }

    /**
     * Set glpiTicketfollowups
     *
     * @param boolean $glpiTicketfollowups
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiTicketfollowups($glpiTicketfollowups)
    {
        $this->glpiTicketfollowups = $glpiTicketfollowups;

        return $this;
    }

    /**
     * Get glpiTicketfollowups
     *
     * @return boolean 
     */
    public function getGlpiTicketfollowups()
    {
        return $this->glpiTicketfollowups;
    }

    /**
     * Set glpiTicketrecurrents
     *
     * @param boolean $glpiTicketrecurrents
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiTicketrecurrents($glpiTicketrecurrents)
    {
        $this->glpiTicketrecurrents = $glpiTicketrecurrents;

        return $this;
    }

    /**
     * Get glpiTicketrecurrents
     *
     * @return boolean 
     */
    public function getGlpiTicketrecurrents()
    {
        return $this->glpiTicketrecurrents;
    }

    /**
     * Set glpiTickets
     *
     * @param boolean $glpiTickets
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiTickets($glpiTickets)
    {
        $this->glpiTickets = $glpiTickets;

        return $this;
    }

    /**
     * Get glpiTickets
     *
     * @return boolean 
     */
    public function getGlpiTickets()
    {
        return $this->glpiTickets;
    }

    /**
     * Set glpiTicketsatisfactions
     *
     * @param boolean $glpiTicketsatisfactions
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiTicketsatisfactions($glpiTicketsatisfactions)
    {
        $this->glpiTicketsatisfactions = $glpiTicketsatisfactions;

        return $this;
    }

    /**
     * Get glpiTicketsatisfactions
     *
     * @return boolean 
     */
    public function getGlpiTicketsatisfactions()
    {
        return $this->glpiTicketsatisfactions;
    }

    /**
     * Set glpiTicketsTickets
     *
     * @param boolean $glpiTicketsTickets
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiTicketsTickets($glpiTicketsTickets)
    {
        $this->glpiTicketsTickets = $glpiTicketsTickets;

        return $this;
    }

    /**
     * Get glpiTicketsTickets
     *
     * @return boolean 
     */
    public function getGlpiTicketsTickets()
    {
        return $this->glpiTicketsTickets;
    }

    /**
     * Set glpiTicketsUsers
     *
     * @param boolean $glpiTicketsUsers
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiTicketsUsers($glpiTicketsUsers)
    {
        $this->glpiTicketsUsers = $glpiTicketsUsers;

        return $this;
    }

    /**
     * Get glpiTicketsUsers
     *
     * @return boolean 
     */
    public function getGlpiTicketsUsers()
    {
        return $this->glpiTicketsUsers;
    }

    /**
     * Set glpiTickettasks
     *
     * @param boolean $glpiTickettasks
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiTickettasks($glpiTickettasks)
    {
        $this->glpiTickettasks = $glpiTickettasks;

        return $this;
    }

    /**
     * Get glpiTickettasks
     *
     * @return boolean 
     */
    public function getGlpiTickettasks()
    {
        return $this->glpiTickettasks;
    }

    /**
     * Set glpiTicketvalidations
     *
     * @param boolean $glpiTicketvalidations
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiTicketvalidations($glpiTicketvalidations)
    {
        $this->glpiTicketvalidations = $glpiTicketvalidations;

        return $this;
    }

    /**
     * Get glpiTicketvalidations
     *
     * @return boolean 
     */
    public function getGlpiTicketvalidations()
    {
        return $this->glpiTicketvalidations;
    }

    /**
     * Set glpiUsercategories
     *
     * @param boolean $glpiUsercategories
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiUsercategories($glpiUsercategories)
    {
        $this->glpiUsercategories = $glpiUsercategories;

        return $this;
    }

    /**
     * Get glpiUsercategories
     *
     * @return boolean 
     */
    public function getGlpiUsercategories()
    {
        return $this->glpiUsercategories;
    }

    /**
     * Set glpiUseremails
     *
     * @param boolean $glpiUseremails
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiUseremails($glpiUseremails)
    {
        $this->glpiUseremails = $glpiUseremails;

        return $this;
    }

    /**
     * Get glpiUseremails
     *
     * @return boolean 
     */
    public function getGlpiUseremails()
    {
        return $this->glpiUseremails;
    }

    /**
     * Set glpiUsers
     *
     * @param boolean $glpiUsers
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiUsers($glpiUsers)
    {
        $this->glpiUsers = $glpiUsers;

        return $this;
    }

    /**
     * Get glpiUsers
     *
     * @return boolean 
     */
    public function getGlpiUsers()
    {
        return $this->glpiUsers;
    }

    /**
     * Set glpiUsertitles
     *
     * @param boolean $glpiUsertitles
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiUsertitles($glpiUsertitles)
    {
        $this->glpiUsertitles = $glpiUsertitles;

        return $this;
    }

    /**
     * Get glpiUsertitles
     *
     * @return boolean 
     */
    public function getGlpiUsertitles()
    {
        return $this->glpiUsertitles;
    }

    /**
     * Set glpiVlans
     *
     * @param boolean $glpiVlans
     * 
     * @return GlpiPluginDumpentityModels
     */
    public function setGlpiVlans($glpiVlans)
    {
        $this->glpiVlans = $glpiVlans;

        return $this;
    }

    /**
     * Get glpiVlans
     *
     * @return boolean 
     */
    public function getGlpiVlans()
    {
        return $this->glpiVlans;
    }
}