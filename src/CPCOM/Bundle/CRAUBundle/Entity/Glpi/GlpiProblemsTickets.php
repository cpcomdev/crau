<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiProblemsTickets
 *
 * @ORM\Table(name="glpi_problems_tickets")
 * @ORM\Entity
 */
class GlpiProblemsTickets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="problems_id", type="integer", nullable=false)
     */
    private $problemsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickets_id", type="integer", nullable=false)
     */
    private $ticketsId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set problemsId
     *
     * @param integer $problemsId
     * 
     * @return GlpiProblemsTickets
     */
    public function setProblemsId($problemsId)
    {
        $this->problemsId = $problemsId;

        return $this;
    }

    /**
     * Get problemsId
     *
     * @return integer 
     */
    public function getProblemsId()
    {
        return $this->problemsId;
    }

    /**
     * Set ticketsId
     *
     * @param integer $ticketsId
     * 
     * @return GlpiProblemsTickets
     */
    public function setTicketsId($ticketsId)
    {
        $this->ticketsId = $ticketsId;

        return $this;
    }

    /**
     * Get ticketsId
     *
     * @return integer 
     */
    public function getTicketsId()
    {
        return $this->ticketsId;
    }
}