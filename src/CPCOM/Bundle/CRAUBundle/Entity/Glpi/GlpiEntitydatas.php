<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiEntitydatas
 *
 * @ORM\Table(name="glpi_entitydatas")
 * @ORM\Entity
 */
class GlpiEntitydatas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=255, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="town", type="string", length=255, nullable=true)
     */
    private $town;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="phonenumber", type="string", length=255, nullable=true)
     */
    private $phonenumber;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_email", type="string", length=255, nullable=true)
     */
    private $adminEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_email_name", type="string", length=255, nullable=true)
     */
    private $adminEmailName;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_reply", type="string", length=255, nullable=true)
     */
    private $adminReply;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_reply_name", type="string", length=255, nullable=true)
     */
    private $adminReplyName;

    /**
     * @var string
     *
     * @ORM\Column(name="notification_subject_tag", type="string", length=255, nullable=true)
     */
    private $notificationSubjectTag;

    /**
     * @var string
     *
     * @ORM\Column(name="notepad", type="text", nullable=true)
     */
    private $notepad;

    /**
     * @var string
     *
     * @ORM\Column(name="ldap_dn", type="string", length=255, nullable=true)
     */
    private $ldapDn;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255, nullable=true)
     */
    private $tag;

    /**
     * @var integer
     *
     * @ORM\Column(name="authldaps_id", type="integer", nullable=false)
     */
    private $authldapsId;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_domain", type="string", length=255, nullable=true)
     */
    private $mailDomain;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_ldapfilter", type="text", nullable=true)
     */
    private $entityLdapfilter;

    /**
     * @var string
     *
     * @ORM\Column(name="mailing_signature", type="text", nullable=true)
     */
    private $mailingSignature;

    /**
     * @var integer
     *
     * @ORM\Column(name="cartridges_alert_repeat", type="integer", nullable=false)
     */
    private $cartridgesAlertRepeat;

    /**
     * @var integer
     *
     * @ORM\Column(name="consumables_alert_repeat", type="integer", nullable=false)
     */
    private $consumablesAlertRepeat;

    /**
     * @var integer
     *
     * @ORM\Column(name="use_licenses_alert", type="integer", nullable=false)
     */
    private $useLicensesAlert;

    /**
     * @var integer
     *
     * @ORM\Column(name="use_contracts_alert", type="integer", nullable=false)
     */
    private $useContractsAlert;

    /**
     * @var integer
     *
     * @ORM\Column(name="use_infocoms_alert", type="integer", nullable=false)
     */
    private $useInfocomsAlert;

    /**
     * @var integer
     *
     * @ORM\Column(name="use_reservations_alert", type="integer", nullable=false)
     */
    private $useReservationsAlert;

    /**
     * @var integer
     *
     * @ORM\Column(name="autoclose_delay", type="integer", nullable=false)
     */
    private $autocloseDelay;

    /**
     * @var integer
     *
     * @ORM\Column(name="notclosed_delay", type="integer", nullable=false)
     */
    private $notclosedDelay;

    /**
     * @var integer
     *
     * @ORM\Column(name="calendars_id", type="integer", nullable=false)
     */
    private $calendarsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="auto_assign_mode", type="integer", nullable=false)
     */
    private $autoAssignMode;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickettype", type="integer", nullable=false)
     */
    private $tickettype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="max_closedate", type="datetime", nullable=true)
     */
    private $maxClosedate;

    /**
     * @var integer
     *
     * @ORM\Column(name="inquest_config", type="integer", nullable=false)
     */
    private $inquestConfig;

    /**
     * @var integer
     *
     * @ORM\Column(name="inquest_rate", type="integer", nullable=false)
     */
    private $inquestRate;

    /**
     * @var integer
     *
     * @ORM\Column(name="inquest_delay", type="integer", nullable=false)
     */
    private $inquestDelay;

    /**
     * @var string
     *
     * @ORM\Column(name="inquest_URL", type="string", length=255, nullable=true)
     */
    private $inquestUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="autofill_warranty_date", type="string", length=255, nullable=true)
     */
    private $autofillWarrantyDate;

    /**
     * @var string
     *
     * @ORM\Column(name="autofill_use_date", type="string", length=255, nullable=true)
     */
    private $autofillUseDate;

    /**
     * @var string
     *
     * @ORM\Column(name="autofill_buy_date", type="string", length=255, nullable=true)
     */
    private $autofillBuyDate;

    /**
     * @var string
     *
     * @ORM\Column(name="autofill_delivery_date", type="string", length=255, nullable=true)
     */
    private $autofillDeliveryDate;

    /**
     * @var string
     *
     * @ORM\Column(name="autofill_order_date", type="string", length=255, nullable=true)
     */
    private $autofillOrderDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickettemplates_id", type="integer", nullable=false)
     */
    private $tickettemplatesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id_software", type="integer", nullable=false)
     */
    private $entitiesIdSoftware;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_contract_alert", type="integer", nullable=false)
     */
    private $defaultContractAlert;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_infocom_alert", type="integer", nullable=false)
     */
    private $defaultInfocomAlert;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_alarm_threshold", type="integer", nullable=false)
     */
    private $defaultAlarmThreshold;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiEntitydatas
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set address
     *
     * @param string $address
     * 
     * @return GlpiEntitydatas
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * 
     * @return GlpiEntitydatas
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set town
     *
     * @param string $town
     * 
     * @return GlpiEntitydatas
     */
    public function setTown($town)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return string 
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set state
     *
     * @param string $state
     * 
     * @return GlpiEntitydatas
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set country
     *
     * @param string $country
     * 
     * @return GlpiEntitydatas
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set website
     *
     * @param string $website
     * 
     * @return GlpiEntitydatas
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set phonenumber
     *
     * @param string $phonenumber
     * 
     * @return GlpiEntitydatas
     */
    public function setPhonenumber($phonenumber)
    {
        $this->phonenumber = $phonenumber;

        return $this;
    }

    /**
     * Get phonenumber
     *
     * @return string 
     */
    public function getPhonenumber()
    {
        return $this->phonenumber;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * 
     * @return GlpiEntitydatas
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     * 
     * @return GlpiEntitydatas
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set adminEmail
     *
     * @param string $adminEmail
     * 
     * @return GlpiEntitydatas
     */
    public function setAdminEmail($adminEmail)
    {
        $this->adminEmail = $adminEmail;

        return $this;
    }

    /**
     * Get adminEmail
     *
     * @return string 
     */
    public function getAdminEmail()
    {
        return $this->adminEmail;
    }

    /**
     * Set adminEmailName
     *
     * @param string $adminEmailName
     * 
     * @return GlpiEntitydatas
     */
    public function setAdminEmailName($adminEmailName)
    {
        $this->adminEmailName = $adminEmailName;

        return $this;
    }

    /**
     * Get adminEmailName
     *
     * @return string 
     */
    public function getAdminEmailName()
    {
        return $this->adminEmailName;
    }

    /**
     * Set adminReply
     *
     * @param string $adminReply
     * 
     * @return GlpiEntitydatas
     */
    public function setAdminReply($adminReply)
    {
        $this->adminReply = $adminReply;

        return $this;
    }

    /**
     * Get adminReply
     *
     * @return string 
     */
    public function getAdminReply()
    {
        return $this->adminReply;
    }

    /**
     * Set adminReplyName
     *
     * @param string $adminReplyName
     * 
     * @return GlpiEntitydatas
     */
    public function setAdminReplyName($adminReplyName)
    {
        $this->adminReplyName = $adminReplyName;

        return $this;
    }

    /**
     * Get adminReplyName
     *
     * @return string 
     */
    public function getAdminReplyName()
    {
        return $this->adminReplyName;
    }

    /**
     * Set notificationSubjectTag
     *
     * @param string $notificationSubjectTag
     * 
     * @return GlpiEntitydatas
     */
    public function setNotificationSubjectTag($notificationSubjectTag)
    {
        $this->notificationSubjectTag = $notificationSubjectTag;

        return $this;
    }

    /**
     * Get notificationSubjectTag
     *
     * @return string 
     */
    public function getNotificationSubjectTag()
    {
        return $this->notificationSubjectTag;
    }

    /**
     * Set notepad
     *
     * @param string $notepad
     * 
     * @return GlpiEntitydatas
     */
    public function setNotepad($notepad)
    {
        $this->notepad = $notepad;

        return $this;
    }

    /**
     * Get notepad
     *
     * @return string 
     */
    public function getNotepad()
    {
        return $this->notepad;
    }

    /**
     * Set ldapDn
     *
     * @param string $ldapDn
     * 
     * @return GlpiEntitydatas
     */
    public function setLdapDn($ldapDn)
    {
        $this->ldapDn = $ldapDn;

        return $this;
    }

    /**
     * Get ldapDn
     *
     * @return string 
     */
    public function getLdapDn()
    {
        return $this->ldapDn;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * 
     * @return GlpiEntitydatas
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set authldapsId
     *
     * @param integer $authldapsId
     * 
     * @return GlpiEntitydatas
     */
    public function setAuthldapsId($authldapsId)
    {
        $this->authldapsId = $authldapsId;

        return $this;
    }

    /**
     * Get authldapsId
     *
     * @return integer 
     */
    public function getAuthldapsId()
    {
        return $this->authldapsId;
    }

    /**
     * Set mailDomain
     *
     * @param string $mailDomain
     * 
     * @return GlpiEntitydatas
     */
    public function setMailDomain($mailDomain)
    {
        $this->mailDomain = $mailDomain;

        return $this;
    }

    /**
     * Get mailDomain
     *
     * @return string 
     */
    public function getMailDomain()
    {
        return $this->mailDomain;
    }

    /**
     * Set entityLdapfilter
     *
     * @param string $entityLdapfilter
     * 
     * @return GlpiEntitydatas
     */
    public function setEntityLdapfilter($entityLdapfilter)
    {
        $this->entityLdapfilter = $entityLdapfilter;

        return $this;
    }

    /**
     * Get entityLdapfilter
     *
     * @return string 
     */
    public function getEntityLdapfilter()
    {
        return $this->entityLdapfilter;
    }

    /**
     * Set mailingSignature
     *
     * @param string $mailingSignature
     * 
     * @return GlpiEntitydatas
     */
    public function setMailingSignature($mailingSignature)
    {
        $this->mailingSignature = $mailingSignature;

        return $this;
    }

    /**
     * Get mailingSignature
     *
     * @return string 
     */
    public function getMailingSignature()
    {
        return $this->mailingSignature;
    }

    /**
     * Set cartridgesAlertRepeat
     *
     * @param integer $cartridgesAlertRepeat
     * 
     * @return GlpiEntitydatas
     */
    public function setCartridgesAlertRepeat($cartridgesAlertRepeat)
    {
        $this->cartridgesAlertRepeat = $cartridgesAlertRepeat;

        return $this;
    }

    /**
     * Get cartridgesAlertRepeat
     *
     * @return integer 
     */
    public function getCartridgesAlertRepeat()
    {
        return $this->cartridgesAlertRepeat;
    }

    /**
     * Set consumablesAlertRepeat
     *
     * @param integer $consumablesAlertRepeat
     * 
     * @return GlpiEntitydatas
     */
    public function setConsumablesAlertRepeat($consumablesAlertRepeat)
    {
        $this->consumablesAlertRepeat = $consumablesAlertRepeat;

        return $this;
    }

    /**
     * Get consumablesAlertRepeat
     *
     * @return integer 
     */
    public function getConsumablesAlertRepeat()
    {
        return $this->consumablesAlertRepeat;
    }

    /**
     * Set useLicensesAlert
     *
     * @param integer $useLicensesAlert
     * 
     * @return GlpiEntitydatas
     */
    public function setUseLicensesAlert($useLicensesAlert)
    {
        $this->useLicensesAlert = $useLicensesAlert;

        return $this;
    }

    /**
     * Get useLicensesAlert
     *
     * @return integer 
     */
    public function getUseLicensesAlert()
    {
        return $this->useLicensesAlert;
    }

    /**
     * Set useContractsAlert
     *
     * @param integer $useContractsAlert
     * 
     * @return GlpiEntitydatas
     */
    public function setUseContractsAlert($useContractsAlert)
    {
        $this->useContractsAlert = $useContractsAlert;

        return $this;
    }

    /**
     * Get useContractsAlert
     *
     * @return integer 
     */
    public function getUseContractsAlert()
    {
        return $this->useContractsAlert;
    }

    /**
     * Set useInfocomsAlert
     *
     * @param integer $useInfocomsAlert
     * 
     * @return GlpiEntitydatas
     */
    public function setUseInfocomsAlert($useInfocomsAlert)
    {
        $this->useInfocomsAlert = $useInfocomsAlert;

        return $this;
    }

    /**
     * Get useInfocomsAlert
     *
     * @return integer 
     */
    public function getUseInfocomsAlert()
    {
        return $this->useInfocomsAlert;
    }

    /**
     * Set useReservationsAlert
     *
     * @param integer $useReservationsAlert
     * 
     * @return GlpiEntitydatas
     */
    public function setUseReservationsAlert($useReservationsAlert)
    {
        $this->useReservationsAlert = $useReservationsAlert;

        return $this;
    }

    /**
     * Get useReservationsAlert
     *
     * @return integer 
     */
    public function getUseReservationsAlert()
    {
        return $this->useReservationsAlert;
    }

    /**
     * Set autocloseDelay
     *
     * @param integer $autocloseDelay
     * 
     * @return GlpiEntitydatas
     */
    public function setAutocloseDelay($autocloseDelay)
    {
        $this->autocloseDelay = $autocloseDelay;

        return $this;
    }

    /**
     * Get autocloseDelay
     *
     * @return integer 
     */
    public function getAutocloseDelay()
    {
        return $this->autocloseDelay;
    }

    /**
     * Set notclosedDelay
     *
     * @param integer $notclosedDelay
     * 
     * @return GlpiEntitydatas
     */
    public function setNotclosedDelay($notclosedDelay)
    {
        $this->notclosedDelay = $notclosedDelay;

        return $this;
    }

    /**
     * Get notclosedDelay
     *
     * @return integer 
     */
    public function getNotclosedDelay()
    {
        return $this->notclosedDelay;
    }

    /**
     * Set calendarsId
     *
     * @param integer $calendarsId
     * 
     * @return GlpiEntitydatas
     */
    public function setCalendarsId($calendarsId)
    {
        $this->calendarsId = $calendarsId;

        return $this;
    }

    /**
     * Get calendarsId
     *
     * @return integer 
     */
    public function getCalendarsId()
    {
        return $this->calendarsId;
    }

    /**
     * Set autoAssignMode
     *
     * @param integer $autoAssignMode
     * 
     * @return GlpiEntitydatas
     */
    public function setAutoAssignMode($autoAssignMode)
    {
        $this->autoAssignMode = $autoAssignMode;

        return $this;
    }

    /**
     * Get autoAssignMode
     *
     * @return integer 
     */
    public function getAutoAssignMode()
    {
        return $this->autoAssignMode;
    }

    /**
     * Set tickettype
     *
     * @param integer $tickettype
     * 
     * @return GlpiEntitydatas
     */
    public function setTickettype($tickettype)
    {
        $this->tickettype = $tickettype;

        return $this;
    }

    /**
     * Get tickettype
     *
     * @return integer 
     */
    public function getTickettype()
    {
        return $this->tickettype;
    }

    /**
     * Set maxClosedate
     *
     * @param \DateTime $maxClosedate
     * 
     * @return GlpiEntitydatas
     */
    public function setMaxClosedate($maxClosedate)
    {
        $this->maxClosedate = $maxClosedate;

        return $this;
    }

    /**
     * Get maxClosedate
     *
     * @return \DateTime 
     */
    public function getMaxClosedate()
    {
        return $this->maxClosedate;
    }

    /**
     * Set inquestConfig
     *
     * @param integer $inquestConfig
     * 
     * @return GlpiEntitydatas
     */
    public function setInquestConfig($inquestConfig)
    {
        $this->inquestConfig = $inquestConfig;

        return $this;
    }

    /**
     * Get inquestConfig
     *
     * @return integer 
     */
    public function getInquestConfig()
    {
        return $this->inquestConfig;
    }

    /**
     * Set inquestRate
     *
     * @param integer $inquestRate
     * 
     * @return GlpiEntitydatas
     */
    public function setInquestRate($inquestRate)
    {
        $this->inquestRate = $inquestRate;

        return $this;
    }

    /**
     * Get inquestRate
     *
     * @return integer 
     */
    public function getInquestRate()
    {
        return $this->inquestRate;
    }

    /**
     * Set inquestDelay
     *
     * @param integer $inquestDelay
     * 
     * @return GlpiEntitydatas
     */
    public function setInquestDelay($inquestDelay)
    {
        $this->inquestDelay = $inquestDelay;

        return $this;
    }

    /**
     * Get inquestDelay
     *
     * @return integer 
     */
    public function getInquestDelay()
    {
        return $this->inquestDelay;
    }

    /**
     * Set inquestUrl
     *
     * @param string $inquestUrl
     * 
     * @return GlpiEntitydatas
     */
    public function setInquestUrl($inquestUrl)
    {
        $this->inquestUrl = $inquestUrl;

        return $this;
    }

    /**
     * Get inquestUrl
     *
     * @return string 
     */
    public function getInquestUrl()
    {
        return $this->inquestUrl;
    }

    /**
     * Set autofillWarrantyDate
     *
     * @param string $autofillWarrantyDate
     * 
     * @return GlpiEntitydatas
     */
    public function setAutofillWarrantyDate($autofillWarrantyDate)
    {
        $this->autofillWarrantyDate = $autofillWarrantyDate;

        return $this;
    }

    /**
     * Get autofillWarrantyDate
     *
     * @return string 
     */
    public function getAutofillWarrantyDate()
    {
        return $this->autofillWarrantyDate;
    }

    /**
     * Set autofillUseDate
     *
     * @param string $autofillUseDate
     * 
     * @return GlpiEntitydatas
     */
    public function setAutofillUseDate($autofillUseDate)
    {
        $this->autofillUseDate = $autofillUseDate;

        return $this;
    }

    /**
     * Get autofillUseDate
     *
     * @return string 
     */
    public function getAutofillUseDate()
    {
        return $this->autofillUseDate;
    }

    /**
     * Set autofillBuyDate
     *
     * @param string $autofillBuyDate
     * 
     * @return GlpiEntitydatas
     */
    public function setAutofillBuyDate($autofillBuyDate)
    {
        $this->autofillBuyDate = $autofillBuyDate;

        return $this;
    }

    /**
     * Get autofillBuyDate
     *
     * @return string 
     */
    public function getAutofillBuyDate()
    {
        return $this->autofillBuyDate;
    }

    /**
     * Set autofillDeliveryDate
     *
     * @param string $autofillDeliveryDate
     * 
     * @return GlpiEntitydatas
     */
    public function setAutofillDeliveryDate($autofillDeliveryDate)
    {
        $this->autofillDeliveryDate = $autofillDeliveryDate;

        return $this;
    }

    /**
     * Get autofillDeliveryDate
     *
     * @return string 
     */
    public function getAutofillDeliveryDate()
    {
        return $this->autofillDeliveryDate;
    }

    /**
     * Set autofillOrderDate
     *
     * @param string $autofillOrderDate
     * 
     * @return GlpiEntitydatas
     */
    public function setAutofillOrderDate($autofillOrderDate)
    {
        $this->autofillOrderDate = $autofillOrderDate;

        return $this;
    }

    /**
     * Get autofillOrderDate
     *
     * @return string 
     */
    public function getAutofillOrderDate()
    {
        return $this->autofillOrderDate;
    }

    /**
     * Set tickettemplatesId
     *
     * @param integer $tickettemplatesId
     * 
     * @return GlpiEntitydatas
     */
    public function setTickettemplatesId($tickettemplatesId)
    {
        $this->tickettemplatesId = $tickettemplatesId;

        return $this;
    }

    /**
     * Get tickettemplatesId
     *
     * @return integer 
     */
    public function getTickettemplatesId()
    {
        return $this->tickettemplatesId;
    }

    /**
     * Set entitiesIdSoftware
     *
     * @param integer $entitiesIdSoftware
     * 
     * @return GlpiEntitydatas
     */
    public function setEntitiesIdSoftware($entitiesIdSoftware)
    {
        $this->entitiesIdSoftware = $entitiesIdSoftware;

        return $this;
    }

    /**
     * Get entitiesIdSoftware
     *
     * @return integer 
     */
    public function getEntitiesIdSoftware()
    {
        return $this->entitiesIdSoftware;
    }

    /**
     * Set defaultContractAlert
     *
     * @param integer $defaultContractAlert
     * 
     * @return GlpiEntitydatas
     */
    public function setDefaultContractAlert($defaultContractAlert)
    {
        $this->defaultContractAlert = $defaultContractAlert;

        return $this;
    }

    /**
     * Get defaultContractAlert
     *
     * @return integer 
     */
    public function getDefaultContractAlert()
    {
        return $this->defaultContractAlert;
    }

    /**
     * Set defaultInfocomAlert
     *
     * @param integer $defaultInfocomAlert
     * 
     * @return GlpiEntitydatas
     */
    public function setDefaultInfocomAlert($defaultInfocomAlert)
    {
        $this->defaultInfocomAlert = $defaultInfocomAlert;

        return $this;
    }

    /**
     * Get defaultInfocomAlert
     *
     * @return integer 
     */
    public function getDefaultInfocomAlert()
    {
        return $this->defaultInfocomAlert;
    }

    /**
     * Set defaultAlarmThreshold
     *
     * @param integer $defaultAlarmThreshold
     * 
     * @return GlpiEntitydatas
     */
    public function setDefaultAlarmThreshold($defaultAlarmThreshold)
    {
        $this->defaultAlarmThreshold = $defaultAlarmThreshold;

        return $this;
    }

    /**
     * Get defaultAlarmThreshold
     *
     * @return integer 
     */
    public function getDefaultAlarmThreshold()
    {
        return $this->defaultAlarmThreshold;
    }
}