<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginDatainjectionModels
 *
 * @ORM\Table(name="glpi_plugin_datainjection_models")
 * @ORM\Entity
 */
class GlpiPluginDatainjectionModels
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=false)
     */
    private $dateMod;

    /**
     * @var string
     *
     * @ORM\Column(name="filetype", type="string", length=255, nullable=false)
     */
    private $filetype;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255, nullable=false)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="behavior_add", type="boolean", nullable=false)
     */
    private $behaviorAdd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="behavior_update", type="boolean", nullable=false)
     */
    private $behaviorUpdate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="can_add_dropdown", type="boolean", nullable=false)
     */
    private $canAddDropdown;

    /**
     * @var integer
     *
     * @ORM\Column(name="can_overwrite_if_not_empty", type="integer", nullable=false)
     */
    private $canOverwriteIfNotEmpty;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_private", type="boolean", nullable=false)
     */
    private $isPrivate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recursive", type="boolean", nullable=false)
     */
    private $isRecursive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="perform_network_connection", type="boolean", nullable=false)
     */
    private $performNetworkConnection;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var string
     *
     * @ORM\Column(name="date_format", type="string", length=11, nullable=false)
     */
    private $dateFormat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="float_format", type="boolean", nullable=false)
     */
    private $floatFormat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="port_unicity", type="boolean", nullable=false)
     */
    private $portUnicity;

    /**
     * @var integer
     *
     * @ORM\Column(name="step", type="integer", nullable=false)
     */
    private $step;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set dateMod
     *
     * @param \DateTime $dateMod
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setDateMod($dateMod)
    {
        $this->dateMod = $dateMod;

        return $this;
    }

    /**
     * Get dateMod
     *
     * @return \DateTime 
     */
    public function getDateMod()
    {
        return $this->dateMod;
    }

    /**
     * Set filetype
     *
     * @param string $filetype
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setFiletype($filetype)
    {
        $this->filetype = $filetype;

        return $this;
    }

    /**
     * Get filetype
     *
     * @return string 
     */
    public function getFiletype()
    {
        return $this->filetype;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string 
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set behaviorAdd
     *
     * @param boolean $behaviorAdd
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setBehaviorAdd($behaviorAdd)
    {
        $this->behaviorAdd = $behaviorAdd;

        return $this;
    }

    /**
     * Get behaviorAdd
     *
     * @return boolean 
     */
    public function getBehaviorAdd()
    {
        return $this->behaviorAdd;
    }

    /**
     * Set behaviorUpdate
     *
     * @param boolean $behaviorUpdate
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setBehaviorUpdate($behaviorUpdate)
    {
        $this->behaviorUpdate = $behaviorUpdate;

        return $this;
    }

    /**
     * Get behaviorUpdate
     *
     * @return boolean 
     */
    public function getBehaviorUpdate()
    {
        return $this->behaviorUpdate;
    }

    /**
     * Set canAddDropdown
     *
     * @param boolean $canAddDropdown
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setCanAddDropdown($canAddDropdown)
    {
        $this->canAddDropdown = $canAddDropdown;

        return $this;
    }

    /**
     * Get canAddDropdown
     *
     * @return boolean 
     */
    public function getCanAddDropdown()
    {
        return $this->canAddDropdown;
    }

    /**
     * Set canOverwriteIfNotEmpty
     *
     * @param integer $canOverwriteIfNotEmpty
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setCanOverwriteIfNotEmpty($canOverwriteIfNotEmpty)
    {
        $this->canOverwriteIfNotEmpty = $canOverwriteIfNotEmpty;

        return $this;
    }

    /**
     * Get canOverwriteIfNotEmpty
     *
     * @return integer 
     */
    public function getCanOverwriteIfNotEmpty()
    {
        return $this->canOverwriteIfNotEmpty;
    }

    /**
     * Set isPrivate
     *
     * @param boolean $isPrivate
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setIsPrivate($isPrivate)
    {
        $this->isPrivate = $isPrivate;

        return $this;
    }

    /**
     * Get isPrivate
     *
     * @return boolean 
     */
    public function getIsPrivate()
    {
        return $this->isPrivate;
    }

    /**
     * Set isRecursive
     *
     * @param boolean $isRecursive
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setIsRecursive($isRecursive)
    {
        $this->isRecursive = $isRecursive;

        return $this;
    }

    /**
     * Get isRecursive
     *
     * @return boolean 
     */
    public function getIsRecursive()
    {
        return $this->isRecursive;
    }

    /**
     * Set performNetworkConnection
     *
     * @param boolean $performNetworkConnection
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setPerformNetworkConnection($performNetworkConnection)
    {
        $this->performNetworkConnection = $performNetworkConnection;

        return $this;
    }

    /**
     * Get performNetworkConnection
     *
     * @return boolean 
     */
    public function getPerformNetworkConnection()
    {
        return $this->performNetworkConnection;
    }

    /**
     * Set usersId
     *
     * @param integer $usersId
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setUsersId($usersId)
    {
        $this->usersId = $usersId;

        return $this;
    }

    /**
     * Get usersId
     *
     * @return integer 
     */
    public function getUsersId()
    {
        return $this->usersId;
    }

    /**
     * Set dateFormat
     *
     * @param string $dateFormat
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setDateFormat($dateFormat)
    {
        $this->dateFormat = $dateFormat;

        return $this;
    }

    /**
     * Get dateFormat
     *
     * @return string 
     */
    public function getDateFormat()
    {
        return $this->dateFormat;
    }

    /**
     * Set floatFormat
     *
     * @param boolean $floatFormat
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setFloatFormat($floatFormat)
    {
        $this->floatFormat = $floatFormat;

        return $this;
    }

    /**
     * Get floatFormat
     *
     * @return boolean 
     */
    public function getFloatFormat()
    {
        return $this->floatFormat;
    }

    /**
     * Set portUnicity
     *
     * @param boolean $portUnicity
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setPortUnicity($portUnicity)
    {
        $this->portUnicity = $portUnicity;

        return $this;
    }

    /**
     * Get portUnicity
     *
     * @return boolean 
     */
    public function getPortUnicity()
    {
        return $this->portUnicity;
    }

    /**
     * Set step
     *
     * @param integer $step
     * 
     * @return GlpiPluginDatainjectionModels
     */
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get step
     *
     * @return integer 
     */
    public function getStep()
    {
        return $this->step;
    }
}