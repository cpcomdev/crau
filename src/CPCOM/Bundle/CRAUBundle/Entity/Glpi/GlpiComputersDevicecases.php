<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicecases
 *
 * @ORM\Table(name="glpi_computers_devicecases")
 * @ORM\Entity
 */
class GlpiComputersDevicecases
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicecases_id", type="integer", nullable=false)
     */
    private $devicecasesId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiComputersDevicecases
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set devicecasesId
     *
     * @param integer $devicecasesId
     * 
     * @return GlpiComputersDevicecases
     */
    public function setDevicecasesId($devicecasesId)
    {
        $this->devicecasesId = $devicecasesId;

        return $this;
    }

    /**
     * Get devicecasesId
     *
     * @return integer 
     */
    public function getDevicecasesId()
    {
        return $this->devicecasesId;
    }
}