<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicepcis
 *
 * @ORM\Table(name="glpi_computers_devicepcis")
 * @ORM\Entity
 */
class GlpiComputersDevicepcis
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicepcis_id", type="integer", nullable=false)
     */
    private $devicepcisId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiComputersDevicepcis
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set devicepcisId
     *
     * @param integer $devicepcisId
     * 
     * @return GlpiComputersDevicepcis
     */
    public function setDevicepcisId($devicepcisId)
    {
        $this->devicepcisId = $devicepcisId;

        return $this;
    }

    /**
     * Get devicepcisId
     *
     * @return integer 
     */
    public function getDevicepcisId()
    {
        return $this->devicepcisId;
    }
}