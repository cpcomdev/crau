<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiOcsservers
 *
 * @ORM\Table(name="glpi_ocsservers")
 * @ORM\Entity
 */
class GlpiOcsservers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_db_user", type="string", length=255, nullable=true)
     */
    private $ocsDbUser;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_db_passwd", type="string", length=255, nullable=true)
     */
    private $ocsDbPasswd;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_db_host", type="string", length=255, nullable=true)
     */
    private $ocsDbHost;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_db_name", type="string", length=255, nullable=true)
     */
    private $ocsDbName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ocs_db_utf8", type="boolean", nullable=false)
     */
    private $ocsDbUtf8;

    /**
     * @var integer
     *
     * @ORM\Column(name="checksum", type="integer", nullable=false)
     */
    private $checksum;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_periph", type="boolean", nullable=false)
     */
    private $importPeriph;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_monitor", type="boolean", nullable=false)
     */
    private $importMonitor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_software", type="boolean", nullable=false)
     */
    private $importSoftware;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_printer", type="boolean", nullable=false)
     */
    private $importPrinter;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_name", type="boolean", nullable=false)
     */
    private $importGeneralName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_os", type="boolean", nullable=false)
     */
    private $importGeneralOs;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_serial", type="boolean", nullable=false)
     */
    private $importGeneralSerial;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_model", type="boolean", nullable=false)
     */
    private $importGeneralModel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_manufacturer", type="boolean", nullable=false)
     */
    private $importGeneralManufacturer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_type", type="boolean", nullable=false)
     */
    private $importGeneralType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_domain", type="boolean", nullable=false)
     */
    private $importGeneralDomain;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_contact", type="boolean", nullable=false)
     */
    private $importGeneralContact;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_comment", type="boolean", nullable=false)
     */
    private $importGeneralComment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_processor", type="boolean", nullable=false)
     */
    private $importDeviceProcessor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_memory", type="boolean", nullable=false)
     */
    private $importDeviceMemory;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_hdd", type="boolean", nullable=false)
     */
    private $importDeviceHdd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_iface", type="boolean", nullable=false)
     */
    private $importDeviceIface;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_gfxcard", type="boolean", nullable=false)
     */
    private $importDeviceGfxcard;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_sound", type="boolean", nullable=false)
     */
    private $importDeviceSound;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_drive", type="boolean", nullable=false)
     */
    private $importDeviceDrive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_port", type="boolean", nullable=false)
     */
    private $importDevicePort;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_device_modem", type="boolean", nullable=false)
     */
    private $importDeviceModem;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_registry", type="boolean", nullable=false)
     */
    private $importRegistry;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_os_serial", type="boolean", nullable=false)
     */
    private $importOsSerial;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_ip", type="boolean", nullable=false)
     */
    private $importIp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_disk", type="boolean", nullable=false)
     */
    private $importDisk;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_monitor_comment", type="boolean", nullable=false)
     */
    private $importMonitorComment;

    /**
     * @var integer
     *
     * @ORM\Column(name="states_id_default", type="integer", nullable=false)
     */
    private $statesIdDefault;

    /**
     * @var string
     *
     * @ORM\Column(name="tag_limit", type="string", length=255, nullable=true)
     */
    private $tagLimit;

    /**
     * @var string
     *
     * @ORM\Column(name="tag_exclude", type="string", length=255, nullable=true)
     */
    private $tagExclude;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_soft_dict", type="boolean", nullable=false)
     */
    private $useSoftDict;

    /**
     * @var integer
     *
     * @ORM\Column(name="cron_sync_number", type="integer", nullable=true)
     */
    private $cronSyncNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="deconnection_behavior", type="string", length=255, nullable=true)
     */
    private $deconnectionBehavior;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_url", type="string", length=255, nullable=true)
     */
    private $ocsUrl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var string
     *
     * @ORM\Column(name="deleted_behavior", type="string", length=255, nullable=false)
     */
    private $deletedBehavior;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_vms", type="boolean", nullable=false)
     */
    private $importVms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_general_uuid", type="boolean", nullable=false)
     */
    private $importGeneralUuid;

    /**
     * @var string
     *
     * @ORM\Column(name="ocs_version", type="string", length=255, nullable=true)
     */
    private $ocsVersion;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiOcsservers
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ocsDbUser
     *
     * @param string $ocsDbUser
     * 
     * @return GlpiOcsservers
     */
    public function setOcsDbUser($ocsDbUser)
    {
        $this->ocsDbUser = $ocsDbUser;

        return $this;
    }

    /**
     * Get ocsDbUser
     *
     * @return string 
     */
    public function getOcsDbUser()
    {
        return $this->ocsDbUser;
    }

    /**
     * Set ocsDbPasswd
     *
     * @param string $ocsDbPasswd
     * 
     * @return GlpiOcsservers
     */
    public function setOcsDbPasswd($ocsDbPasswd)
    {
        $this->ocsDbPasswd = $ocsDbPasswd;

        return $this;
    }

    /**
     * Get ocsDbPasswd
     *
     * @return string 
     */
    public function getOcsDbPasswd()
    {
        return $this->ocsDbPasswd;
    }

    /**
     * Set ocsDbHost
     *
     * @param string $ocsDbHost
     * 
     * @return GlpiOcsservers
     */
    public function setOcsDbHost($ocsDbHost)
    {
        $this->ocsDbHost = $ocsDbHost;

        return $this;
    }

    /**
     * Get ocsDbHost
     *
     * @return string 
     */
    public function getOcsDbHost()
    {
        return $this->ocsDbHost;
    }

    /**
     * Set ocsDbName
     *
     * @param string $ocsDbName
     * 
     * @return GlpiOcsservers
     */
    public function setOcsDbName($ocsDbName)
    {
        $this->ocsDbName = $ocsDbName;

        return $this;
    }

    /**
     * Get ocsDbName
     *
     * @return string 
     */
    public function getOcsDbName()
    {
        return $this->ocsDbName;
    }

    /**
     * Set ocsDbUtf8
     *
     * @param boolean $ocsDbUtf8
     * 
     * @return GlpiOcsservers
     */
    public function setOcsDbUtf8($ocsDbUtf8)
    {
        $this->ocsDbUtf8 = $ocsDbUtf8;

        return $this;
    }

    /**
     * Get ocsDbUtf8
     *
     * @return boolean 
     */
    public function getOcsDbUtf8()
    {
        return $this->ocsDbUtf8;
    }

    /**
     * Set checksum
     *
     * @param integer $checksum
     * 
     * @return GlpiOcsservers
     */
    public function setChecksum($checksum)
    {
        $this->checksum = $checksum;

        return $this;
    }

    /**
     * Get checksum
     *
     * @return integer 
     */
    public function getChecksum()
    {
        return $this->checksum;
    }

    /**
     * Set importPeriph
     *
     * @param boolean $importPeriph
     * 
     * @return GlpiOcsservers
     */
    public function setImportPeriph($importPeriph)
    {
        $this->importPeriph = $importPeriph;

        return $this;
    }

    /**
     * Get importPeriph
     *
     * @return boolean 
     */
    public function getImportPeriph()
    {
        return $this->importPeriph;
    }

    /**
     * Set importMonitor
     *
     * @param boolean $importMonitor
     * 
     * @return GlpiOcsservers
     */
    public function setImportMonitor($importMonitor)
    {
        $this->importMonitor = $importMonitor;

        return $this;
    }

    /**
     * Get importMonitor
     *
     * @return boolean 
     */
    public function getImportMonitor()
    {
        return $this->importMonitor;
    }

    /**
     * Set importSoftware
     *
     * @param boolean $importSoftware
     * 
     * @return GlpiOcsservers
     */
    public function setImportSoftware($importSoftware)
    {
        $this->importSoftware = $importSoftware;

        return $this;
    }

    /**
     * Get importSoftware
     *
     * @return boolean 
     */
    public function getImportSoftware()
    {
        return $this->importSoftware;
    }

    /**
     * Set importPrinter
     *
     * @param boolean $importPrinter
     * 
     * @return GlpiOcsservers
     */
    public function setImportPrinter($importPrinter)
    {
        $this->importPrinter = $importPrinter;

        return $this;
    }

    /**
     * Get importPrinter
     *
     * @return boolean 
     */
    public function getImportPrinter()
    {
        return $this->importPrinter;
    }

    /**
     * Set importGeneralName
     *
     * @param boolean $importGeneralName
     * 
     * @return GlpiOcsservers
     */
    public function setImportGeneralName($importGeneralName)
    {
        $this->importGeneralName = $importGeneralName;

        return $this;
    }

    /**
     * Get importGeneralName
     *
     * @return boolean 
     */
    public function getImportGeneralName()
    {
        return $this->importGeneralName;
    }

    /**
     * Set importGeneralOs
     *
     * @param boolean $importGeneralOs
     * 
     * @return GlpiOcsservers
     */
    public function setImportGeneralOs($importGeneralOs)
    {
        $this->importGeneralOs = $importGeneralOs;

        return $this;
    }

    /**
     * Get importGeneralOs
     *
     * @return boolean 
     */
    public function getImportGeneralOs()
    {
        return $this->importGeneralOs;
    }

    /**
     * Set importGeneralSerial
     *
     * @param boolean $importGeneralSerial
     * 
     * @return GlpiOcsservers
     */
    public function setImportGeneralSerial($importGeneralSerial)
    {
        $this->importGeneralSerial = $importGeneralSerial;

        return $this;
    }

    /**
     * Get importGeneralSerial
     *
     * @return boolean 
     */
    public function getImportGeneralSerial()
    {
        return $this->importGeneralSerial;
    }

    /**
     * Set importGeneralModel
     *
     * @param boolean $importGeneralModel
     * 
     * @return GlpiOcsservers
     */
    public function setImportGeneralModel($importGeneralModel)
    {
        $this->importGeneralModel = $importGeneralModel;

        return $this;
    }

    /**
     * Get importGeneralModel
     *
     * @return boolean 
     */
    public function getImportGeneralModel()
    {
        return $this->importGeneralModel;
    }

    /**
     * Set importGeneralManufacturer
     *
     * @param boolean $importGeneralManufacturer
     * 
     * @return GlpiOcsservers
     */
    public function setImportGeneralManufacturer($importGeneralManufacturer)
    {
        $this->importGeneralManufacturer = $importGeneralManufacturer;

        return $this;
    }

    /**
     * Get importGeneralManufacturer
     *
     * @return boolean 
     */
    public function getImportGeneralManufacturer()
    {
        return $this->importGeneralManufacturer;
    }

    /**
     * Set importGeneralType
     *
     * @param boolean $importGeneralType
     * 
     * @return GlpiOcsservers
     */
    public function setImportGeneralType($importGeneralType)
    {
        $this->importGeneralType = $importGeneralType;

        return $this;
    }

    /**
     * Get importGeneralType
     *
     * @return boolean 
     */
    public function getImportGeneralType()
    {
        return $this->importGeneralType;
    }

    /**
     * Set importGeneralDomain
     *
     * @param boolean $importGeneralDomain
     * 
     * @return GlpiOcsservers
     */
    public function setImportGeneralDomain($importGeneralDomain)
    {
        $this->importGeneralDomain = $importGeneralDomain;

        return $this;
    }

    /**
     * Get importGeneralDomain
     *
     * @return boolean 
     */
    public function getImportGeneralDomain()
    {
        return $this->importGeneralDomain;
    }

    /**
     * Set importGeneralContact
     *
     * @param boolean $importGeneralContact
     * 
     * @return GlpiOcsservers
     */
    public function setImportGeneralContact($importGeneralContact)
    {
        $this->importGeneralContact = $importGeneralContact;

        return $this;
    }

    /**
     * Get importGeneralContact
     *
     * @return boolean 
     */
    public function getImportGeneralContact()
    {
        return $this->importGeneralContact;
    }

    /**
     * Set importGeneralComment
     *
     * @param boolean $importGeneralComment
     * 
     * @return GlpiOcsservers
     */
    public function setImportGeneralComment($importGeneralComment)
    {
        $this->importGeneralComment = $importGeneralComment;

        return $this;
    }

    /**
     * Get importGeneralComment
     *
     * @return boolean 
     */
    public function getImportGeneralComment()
    {
        return $this->importGeneralComment;
    }

    /**
     * Set importDeviceProcessor
     *
     * @param boolean $importDeviceProcessor
     * 
     * @return GlpiOcsservers
     */
    public function setImportDeviceProcessor($importDeviceProcessor)
    {
        $this->importDeviceProcessor = $importDeviceProcessor;

        return $this;
    }

    /**
     * Get importDeviceProcessor
     *
     * @return boolean 
     */
    public function getImportDeviceProcessor()
    {
        return $this->importDeviceProcessor;
    }

    /**
     * Set importDeviceMemory
     *
     * @param boolean $importDeviceMemory
     * 
     * @return GlpiOcsservers
     */
    public function setImportDeviceMemory($importDeviceMemory)
    {
        $this->importDeviceMemory = $importDeviceMemory;

        return $this;
    }

    /**
     * Get importDeviceMemory
     *
     * @return boolean 
     */
    public function getImportDeviceMemory()
    {
        return $this->importDeviceMemory;
    }

    /**
     * Set importDeviceHdd
     *
     * @param boolean $importDeviceHdd
     * 
     * @return GlpiOcsservers
     */
    public function setImportDeviceHdd($importDeviceHdd)
    {
        $this->importDeviceHdd = $importDeviceHdd;

        return $this;
    }

    /**
     * Get importDeviceHdd
     *
     * @return boolean 
     */
    public function getImportDeviceHdd()
    {
        return $this->importDeviceHdd;
    }

    /**
     * Set importDeviceIface
     *
     * @param boolean $importDeviceIface
     * 
     * @return GlpiOcsservers
     */
    public function setImportDeviceIface($importDeviceIface)
    {
        $this->importDeviceIface = $importDeviceIface;

        return $this;
    }

    /**
     * Get importDeviceIface
     *
     * @return boolean 
     */
    public function getImportDeviceIface()
    {
        return $this->importDeviceIface;
    }

    /**
     * Set importDeviceGfxcard
     *
     * @param boolean $importDeviceGfxcard
     * 
     * @return GlpiOcsservers
     */
    public function setImportDeviceGfxcard($importDeviceGfxcard)
    {
        $this->importDeviceGfxcard = $importDeviceGfxcard;

        return $this;
    }

    /**
     * Get importDeviceGfxcard
     *
     * @return boolean 
     */
    public function getImportDeviceGfxcard()
    {
        return $this->importDeviceGfxcard;
    }

    /**
     * Set importDeviceSound
     *
     * @param boolean $importDeviceSound
     * 
     * @return GlpiOcsservers
     */
    public function setImportDeviceSound($importDeviceSound)
    {
        $this->importDeviceSound = $importDeviceSound;

        return $this;
    }

    /**
     * Get importDeviceSound
     *
     * @return boolean 
     */
    public function getImportDeviceSound()
    {
        return $this->importDeviceSound;
    }

    /**
     * Set importDeviceDrive
     *
     * @param boolean $importDeviceDrive
     * 
     * @return GlpiOcsservers
     */
    public function setImportDeviceDrive($importDeviceDrive)
    {
        $this->importDeviceDrive = $importDeviceDrive;

        return $this;
    }

    /**
     * Get importDeviceDrive
     *
     * @return boolean 
     */
    public function getImportDeviceDrive()
    {
        return $this->importDeviceDrive;
    }

    /**
     * Set importDevicePort
     *
     * @param boolean $importDevicePort
     * 
     * @return GlpiOcsservers
     */
    public function setImportDevicePort($importDevicePort)
    {
        $this->importDevicePort = $importDevicePort;

        return $this;
    }

    /**
     * Get importDevicePort
     *
     * @return boolean 
     */
    public function getImportDevicePort()
    {
        return $this->importDevicePort;
    }

    /**
     * Set importDeviceModem
     *
     * @param boolean $importDeviceModem
     * 
     * @return GlpiOcsservers
     */
    public function setImportDeviceModem($importDeviceModem)
    {
        $this->importDeviceModem = $importDeviceModem;

        return $this;
    }

    /**
     * Get importDeviceModem
     *
     * @return boolean 
     */
    public function getImportDeviceModem()
    {
        return $this->importDeviceModem;
    }

    /**
     * Set importRegistry
     *
     * @param boolean $importRegistry
     * 
     * @return GlpiOcsservers
     */
    public function setImportRegistry($importRegistry)
    {
        $this->importRegistry = $importRegistry;

        return $this;
    }

    /**
     * Get importRegistry
     *
     * @return boolean 
     */
    public function getImportRegistry()
    {
        return $this->importRegistry;
    }

    /**
     * Set importOsSerial
     *
     * @param boolean $importOsSerial
     * 
     * @return GlpiOcsservers
     */
    public function setImportOsSerial($importOsSerial)
    {
        $this->importOsSerial = $importOsSerial;

        return $this;
    }

    /**
     * Get importOsSerial
     *
     * @return boolean 
     */
    public function getImportOsSerial()
    {
        return $this->importOsSerial;
    }

    /**
     * Set importIp
     *
     * @param boolean $importIp
     * 
     * @return GlpiOcsservers
     */
    public function setImportIp($importIp)
    {
        $this->importIp = $importIp;

        return $this;
    }

    /**
     * Get importIp
     *
     * @return boolean 
     */
    public function getImportIp()
    {
        return $this->importIp;
    }

    /**
     * Set importDisk
     *
     * @param boolean $importDisk
     * 
     * @return GlpiOcsservers
     */
    public function setImportDisk($importDisk)
    {
        $this->importDisk = $importDisk;

        return $this;
    }

    /**
     * Get importDisk
     *
     * @return boolean 
     */
    public function getImportDisk()
    {
        return $this->importDisk;
    }

    /**
     * Set importMonitorComment
     *
     * @param boolean $importMonitorComment
     * 
     * @return GlpiOcsservers
     */
    public function setImportMonitorComment($importMonitorComment)
    {
        $this->importMonitorComment = $importMonitorComment;

        return $this;
    }

    /**
     * Get importMonitorComment
     *
     * @return boolean 
     */
    public function getImportMonitorComment()
    {
        return $this->importMonitorComment;
    }

    /**
     * Set statesIdDefault
     *
     * @param integer $statesIdDefault
     * 
     * @return GlpiOcsservers
     */
    public function setStatesIdDefault($statesIdDefault)
    {
        $this->statesIdDefault = $statesIdDefault;

        return $this;
    }

    /**
     * Get statesIdDefault
     *
     * @return integer 
     */
    public function getStatesIdDefault()
    {
        return $this->statesIdDefault;
    }

    /**
     * Set tagLimit
     *
     * @param string $tagLimit
     * 
     * @return GlpiOcsservers
     */
    public function setTagLimit($tagLimit)
    {
        $this->tagLimit = $tagLimit;

        return $this;
    }

    /**
     * Get tagLimit
     *
     * @return string 
     */
    public function getTagLimit()
    {
        return $this->tagLimit;
    }

    /**
     * Set tagExclude
     *
     * @param string $tagExclude
     * 
     * @return GlpiOcsservers
     */
    public function setTagExclude($tagExclude)
    {
        $this->tagExclude = $tagExclude;

        return $this;
    }

    /**
     * Get tagExclude
     *
     * @return string 
     */
    public function getTagExclude()
    {
        return $this->tagExclude;
    }

    /**
     * Set useSoftDict
     *
     * @param boolean $useSoftDict
     * 
     * @return GlpiOcsservers
     */
    public function setUseSoftDict($useSoftDict)
    {
        $this->useSoftDict = $useSoftDict;

        return $this;
    }

    /**
     * Get useSoftDict
     *
     * @return boolean 
     */
    public function getUseSoftDict()
    {
        return $this->useSoftDict;
    }

    /**
     * Set cronSyncNumber
     *
     * @param integer $cronSyncNumber
     * 
     * @return GlpiOcsservers
     */
    public function setCronSyncNumber($cronSyncNumber)
    {
        $this->cronSyncNumber = $cronSyncNumber;

        return $this;
    }

    /**
     * Get cronSyncNumber
     *
     * @return integer 
     */
    public function getCronSyncNumber()
    {
        return $this->cronSyncNumber;
    }

    /**
     * Set deconnectionBehavior
     *
     * @param string $deconnectionBehavior
     * 
     * @return GlpiOcsservers
     */
    public function setDeconnectionBehavior($deconnectionBehavior)
    {
        $this->deconnectionBehavior = $deconnectionBehavior;

        return $this;
    }

    /**
     * Get deconnectionBehavior
     *
     * @return string 
     */
    public function getDeconnectionBehavior()
    {
        return $this->deconnectionBehavior;
    }

    /**
     * Set ocsUrl
     *
     * @param string $ocsUrl
     * 
     * @return GlpiOcsservers
     */
    public function setOcsUrl($ocsUrl)
    {
        $this->ocsUrl = $ocsUrl;

        return $this;
    }

    /**
     * Get ocsUrl
     *
     * @return string 
     */
    public function getOcsUrl()
    {
        return $this->ocsUrl;
    }

    /**
     * Set dateMod
     *
     * @param \DateTime $dateMod
     * 
     * @return GlpiOcsservers
     */
    public function setDateMod($dateMod)
    {
        $this->dateMod = $dateMod;

        return $this;
    }

    /**
     * Get dateMod
     *
     * @return \DateTime 
     */
    public function getDateMod()
    {
        return $this->dateMod;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiOcsservers
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * 
     * @return GlpiOcsservers
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set deletedBehavior
     *
     * @param string $deletedBehavior
     * 
     * @return GlpiOcsservers
     */
    public function setDeletedBehavior($deletedBehavior)
    {
        $this->deletedBehavior = $deletedBehavior;

        return $this;
    }

    /**
     * Get deletedBehavior
     *
     * @return string 
     */
    public function getDeletedBehavior()
    {
        return $this->deletedBehavior;
    }

    /**
     * Set importVms
     *
     * @param boolean $importVms
     * 
     * @return GlpiOcsservers
     */
    public function setImportVms($importVms)
    {
        $this->importVms = $importVms;

        return $this;
    }

    /**
     * Get importVms
     *
     * @return boolean 
     */
    public function getImportVms()
    {
        return $this->importVms;
    }

    /**
     * Set importGeneralUuid
     *
     * @param boolean $importGeneralUuid
     * 
     * @return GlpiOcsservers
     */
    public function setImportGeneralUuid($importGeneralUuid)
    {
        $this->importGeneralUuid = $importGeneralUuid;

        return $this;
    }

    /**
     * Get importGeneralUuid
     *
     * @return boolean 
     */
    public function getImportGeneralUuid()
    {
        return $this->importGeneralUuid;
    }

    /**
     * Set ocsVersion
     *
     * @param string $ocsVersion
     * 
     * @return GlpiOcsservers
     */
    public function setOcsVersion($ocsVersion)
    {
        $this->ocsVersion = $ocsVersion;

        return $this;
    }

    /**
     * Get ocsVersion
     *
     * @return string 
     */
    public function getOcsVersion()
    {
        return $this->ocsVersion;
    }
}