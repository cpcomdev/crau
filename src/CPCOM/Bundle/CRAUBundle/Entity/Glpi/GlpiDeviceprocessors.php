<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiDeviceprocessors
 *
 * @ORM\Table(name="glpi_deviceprocessors")
 * @ORM\Entity
 */
class GlpiDeviceprocessors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var integer
     *
     * @ORM\Column(name="frequence", type="integer", nullable=false)
     */
    private $frequence;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="specif_default", type="integer", nullable=false)
     */
    private $specifDefault;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     * 
     * @return GlpiDeviceprocessors
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string 
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set frequence
     *
     * @param integer $frequence
     * 
     * @return GlpiDeviceprocessors
     */
    public function setFrequence($frequence)
    {
        $this->frequence = $frequence;

        return $this;
    }

    /**
     * Get frequence
     *
     * @return integer 
     */
    public function getFrequence()
    {
        return $this->frequence;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiDeviceprocessors
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set manufacturersId
     *
     * @param integer $manufacturersId
     * 
     * @return GlpiDeviceprocessors
     */
    public function setManufacturersId($manufacturersId)
    {
        $this->manufacturersId = $manufacturersId;

        return $this;
    }

    /**
     * Get manufacturersId
     *
     * @return integer 
     */
    public function getManufacturersId()
    {
        return $this->manufacturersId;
    }

    /**
     * Set specifDefault
     *
     * @param integer $specifDefault
     * 
     * @return GlpiDeviceprocessors
     */
    public function setSpecifDefault($specifDefault)
    {
        $this->specifDefault = $specifDefault;

        return $this;
    }

    /**
     * Get specifDefault
     *
     * @return integer 
     */
    public function getSpecifDefault()
    {
        return $this->specifDefault;
    }
}