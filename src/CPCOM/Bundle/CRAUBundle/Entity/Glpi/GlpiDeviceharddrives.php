<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiDeviceharddrives
 *
 * @ORM\Table(name="glpi_deviceharddrives")
 * @ORM\Entity
 */
class GlpiDeviceharddrives
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="rpm", type="string", length=255, nullable=true)
     */
    private $rpm;

    /**
     * @var integer
     *
     * @ORM\Column(name="interfacetypes_id", type="integer", nullable=false)
     */
    private $interfacetypesId;

    /**
     * @var string
     *
     * @ORM\Column(name="cache", type="string", length=255, nullable=true)
     */
    private $cache;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="specif_default", type="integer", nullable=false)
     */
    private $specifDefault;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     * 
     * @return GlpiDeviceharddrives
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string 
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set rpm
     *
     * @param string $rpm
     * 
     * @return GlpiDeviceharddrives
     */
    public function setRpm($rpm)
    {
        $this->rpm = $rpm;

        return $this;
    }

    /**
     * Get rpm
     *
     * @return string 
     */
    public function getRpm()
    {
        return $this->rpm;
    }

    /**
     * Set interfacetypesId
     *
     * @param integer $interfacetypesId
     * 
     * @return GlpiDeviceharddrives
     */
    public function setInterfacetypesId($interfacetypesId)
    {
        $this->interfacetypesId = $interfacetypesId;

        return $this;
    }

    /**
     * Get interfacetypesId
     *
     * @return integer 
     */
    public function getInterfacetypesId()
    {
        return $this->interfacetypesId;
    }

    /**
     * Set cache
     *
     * @param string $cache
     * 
     * @return GlpiDeviceharddrives
     */
    public function setCache($cache)
    {
        $this->cache = $cache;

        return $this;
    }

    /**
     * Get cache
     *
     * @return string 
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiDeviceharddrives
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set manufacturersId
     *
     * @param integer $manufacturersId
     * 
     * @return GlpiDeviceharddrives
     */
    public function setManufacturersId($manufacturersId)
    {
        $this->manufacturersId = $manufacturersId;

        return $this;
    }

    /**
     * Get manufacturersId
     *
     * @return integer 
     */
    public function getManufacturersId()
    {
        return $this->manufacturersId;
    }

    /**
     * Set specifDefault
     *
     * @param integer $specifDefault
     * 
     * @return GlpiDeviceharddrives
     */
    public function setSpecifDefault($specifDefault)
    {
        $this->specifDefault = $specifDefault;

        return $this;
    }

    /**
     * Get specifDefault
     *
     * @return integer 
     */
    public function getSpecifDefault()
    {
        return $this->specifDefault;
    }
}