<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiNetworkportsVlans
 *
 * @ORM\Table(name="glpi_networkports_vlans")
 * @ORM\Entity
 */
class GlpiNetworkportsVlans
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="networkports_id", type="integer", nullable=false)
     */
    private $networkportsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="vlans_id", type="integer", nullable=false)
     */
    private $vlansId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set networkportsId
     *
     * @param integer $networkportsId
     * 
     * @return GlpiNetworkportsVlans
     */
    public function setNetworkportsId($networkportsId)
    {
        $this->networkportsId = $networkportsId;

        return $this;
    }

    /**
     * Get networkportsId
     *
     * @return integer 
     */
    public function getNetworkportsId()
    {
        return $this->networkportsId;
    }

    /**
     * Set vlansId
     *
     * @param integer $vlansId
     * 
     * @return GlpiNetworkportsVlans
     */
    public function setVlansId($vlansId)
    {
        $this->vlansId = $vlansId;

        return $this;
    }

    /**
     * Get vlansId
     *
     * @return integer 
     */
    public function getVlansId()
    {
        return $this->vlansId;
    }
}