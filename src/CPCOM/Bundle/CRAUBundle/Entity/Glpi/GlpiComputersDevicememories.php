<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiComputersDevicememories
 *
 * @ORM\Table(name="glpi_computers_devicememories")
 * @ORM\Entity
 */
class GlpiComputersDevicememories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="computers_id", type="integer", nullable=false)
     */
    private $computersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="devicememories_id", type="integer", nullable=false)
     */
    private $devicememoriesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="specificity", type="integer", nullable=false)
     */
    private $specificity;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computersId
     *
     * @param integer $computersId
     * 
     * @return GlpiComputersDevicememories
     */
    public function setComputersId($computersId)
    {
        $this->computersId = $computersId;

        return $this;
    }

    /**
     * Get computersId
     *
     * @return integer 
     */
    public function getComputersId()
    {
        return $this->computersId;
    }

    /**
     * Set devicememoriesId
     *
     * @param integer $devicememoriesId
     * 
     * @return GlpiComputersDevicememories
     */
    public function setDevicememoriesId($devicememoriesId)
    {
        $this->devicememoriesId = $devicememoriesId;

        return $this;
    }

    /**
     * Get devicememoriesId
     *
     * @return integer 
     */
    public function getDevicememoriesId()
    {
        return $this->devicememoriesId;
    }

    /**
     * Set specificity
     *
     * @param integer $specificity
     * 
     * @return GlpiComputersDevicememories
     */
    public function setSpecificity($specificity)
    {
        $this->specificity = $specificity;

        return $this;
    }

    /**
     * Get specificity
     *
     * @return integer 
     */
    public function getSpecificity()
    {
        return $this->specificity;
    }
}