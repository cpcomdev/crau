<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiDevicecontrols
 *
 * @ORM\Table(name="glpi_devicecontrols")
 * @ORM\Entity
 */
class GlpiDevicecontrols
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_raid", type="boolean", nullable=false)
     */
    private $isRaid;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="interfacetypes_id", type="integer", nullable=false)
     */
    private $interfacetypesId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     * 
     * @return GlpiDevicecontrols
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string 
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set isRaid
     *
     * @param boolean $isRaid
     * 
     * @return GlpiDevicecontrols
     */
    public function setIsRaid($isRaid)
    {
        $this->isRaid = $isRaid;

        return $this;
    }

    /**
     * Get isRaid
     *
     * @return boolean 
     */
    public function getIsRaid()
    {
        return $this->isRaid;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiDevicecontrols
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set manufacturersId
     *
     * @param integer $manufacturersId
     * 
     * @return GlpiDevicecontrols
     */
    public function setManufacturersId($manufacturersId)
    {
        $this->manufacturersId = $manufacturersId;

        return $this;
    }

    /**
     * Get manufacturersId
     *
     * @return integer 
     */
    public function getManufacturersId()
    {
        return $this->manufacturersId;
    }

    /**
     * Set interfacetypesId
     *
     * @param integer $interfacetypesId
     * 
     * @return GlpiDevicecontrols
     */
    public function setInterfacetypesId($interfacetypesId)
    {
        $this->interfacetypesId = $interfacetypesId;

        return $this;
    }

    /**
     * Get interfacetypesId
     *
     * @return integer 
     */
    public function getInterfacetypesId()
    {
        return $this->interfacetypesId;
    }
}