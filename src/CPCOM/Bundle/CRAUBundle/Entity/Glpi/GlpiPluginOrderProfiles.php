<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPluginOrderProfiles
 *
 * @ORM\Table(name="glpi_plugin_order_profiles")
 * @ORM\Entity
 */
class GlpiPluginOrderProfiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="profiles_id", type="integer", nullable=false)
     */
    private $profilesId;

    /**
     * @var string
     *
     * @ORM\Column(name="order", type="string", length=1, nullable=true)
     */
    private $order;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=1, nullable=true)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="validation", type="string", length=1, nullable=true)
     */
    private $validation;

    /**
     * @var string
     *
     * @ORM\Column(name="cancel", type="string", length=1, nullable=true)
     */
    private $cancel;

    /**
     * @var string
     *
     * @ORM\Column(name="undo_validation", type="string", length=1, nullable=true)
     */
    private $undoValidation;

    /**
     * @var string
     *
     * @ORM\Column(name="bill", type="string", length=1, nullable=true)
     */
    private $bill;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery", type="string", length=1, nullable=true)
     */
    private $delivery;

    /**
     * @var string
     *
     * @ORM\Column(name="generate_order_odt", type="string", length=1, nullable=true)
     */
    private $generateOrderOdt;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profilesId
     *
     * @param integer $profilesId
     * 
     * @return GlpiPluginOrderProfiles
     */
    public function setProfilesId($profilesId)
    {
        $this->profilesId = $profilesId;

        return $this;
    }

    /**
     * Get profilesId
     *
     * @return integer 
     */
    public function getProfilesId()
    {
        return $this->profilesId;
    }

    /**
     * Set order
     *
     * @param string $order
     * 
     * @return GlpiPluginOrderProfiles
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return string 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * 
     * @return GlpiPluginOrderProfiles
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set validation
     *
     * @param string $validation
     * 
     * @return GlpiPluginOrderProfiles
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * Get validation
     *
     * @return string 
     */
    public function getValidation()
    {
        return $this->validation;
    }

    /**
     * Set cancel
     *
     * @param string $cancel
     * 
     * @return GlpiPluginOrderProfiles
     */
    public function setCancel($cancel)
    {
        $this->cancel = $cancel;

        return $this;
    }

    /**
     * Get cancel
     *
     * @return string 
     */
    public function getCancel()
    {
        return $this->cancel;
    }

    /**
     * Set undoValidation
     *
     * @param string $undoValidation
     * 
     * @return GlpiPluginOrderProfiles
     */
    public function setUndoValidation($undoValidation)
    {
        $this->undoValidation = $undoValidation;

        return $this;
    }

    /**
     * Get undoValidation
     *
     * @return string 
     */
    public function getUndoValidation()
    {
        return $this->undoValidation;
    }

    /**
     * Set bill
     *
     * @param string $bill
     * 
     * @return GlpiPluginOrderProfiles
     */
    public function setBill($bill)
    {
        $this->bill = $bill;

        return $this;
    }

    /**
     * Get bill
     *
     * @return string 
     */
    public function getBill()
    {
        return $this->bill;
    }

    /**
     * Set delivery
     *
     * @param string $delivery
     * 
     * @return GlpiPluginOrderProfiles
     */
    public function setDelivery($delivery)
    {
        $this->delivery = $delivery;

        return $this;
    }

    /**
     * Get delivery
     *
     * @return string 
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * Set generateOrderOdt
     *
     * @param string $generateOrderOdt
     * 
     * @return GlpiPluginOrderProfiles
     */
    public function setGenerateOrderOdt($generateOrderOdt)
    {
        $this->generateOrderOdt = $generateOrderOdt;

        return $this;
    }

    /**
     * Get generateOrderOdt
     *
     * @return string 
     */
    public function getGenerateOrderOdt()
    {
        return $this->generateOrderOdt;
    }
}