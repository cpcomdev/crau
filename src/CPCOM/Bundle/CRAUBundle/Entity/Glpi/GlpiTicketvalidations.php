<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiTicketvalidations
 *
 * @ORM\Table(name="glpi_ticketvalidations")
 * @ORM\Entity
 */
class GlpiTicketvalidations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tickets_id", type="integer", nullable=false)
     */
    private $ticketsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id_validate", type="integer", nullable=false)
     */
    private $usersIdValidate;

    /**
     * @var string
     *
     * @ORM\Column(name="comment_submission", type="text", nullable=true)
     */
    private $commentSubmission;

    /**
     * @var string
     *
     * @ORM\Column(name="comment_validation", type="text", nullable=true)
     */
    private $commentValidation;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="submission_date", type="datetime", nullable=true)
     */
    private $submissionDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="validation_date", type="datetime", nullable=true)
     */
    private $validationDate;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiTicketvalidations
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set usersId
     *
     * @param integer $usersId
     * 
     * @return GlpiTicketvalidations
     */
    public function setUsersId($usersId)
    {
        $this->usersId = $usersId;

        return $this;
    }

    /**
     * Get usersId
     *
     * @return integer 
     */
    public function getUsersId()
    {
        return $this->usersId;
    }

    /**
     * Set ticketsId
     *
     * @param integer $ticketsId
     * 
     * @return GlpiTicketvalidations
     */
    public function setTicketsId($ticketsId)
    {
        $this->ticketsId = $ticketsId;

        return $this;
    }

    /**
     * Get ticketsId
     *
     * @return integer 
     */
    public function getTicketsId()
    {
        return $this->ticketsId;
    }

    /**
     * Set usersIdValidate
     *
     * @param integer $usersIdValidate
     * 
     * @return GlpiTicketvalidations
     */
    public function setUsersIdValidate($usersIdValidate)
    {
        $this->usersIdValidate = $usersIdValidate;

        return $this;
    }

    /**
     * Get usersIdValidate
     *
     * @return integer 
     */
    public function getUsersIdValidate()
    {
        return $this->usersIdValidate;
    }

    /**
     * Set commentSubmission
     *
     * @param string $commentSubmission
     * 
     * @return GlpiTicketvalidations
     */
    public function setCommentSubmission($commentSubmission)
    {
        $this->commentSubmission = $commentSubmission;

        return $this;
    }

    /**
     * Get commentSubmission
     *
     * @return string 
     */
    public function getCommentSubmission()
    {
        return $this->commentSubmission;
    }

    /**
     * Set commentValidation
     *
     * @param string $commentValidation
     * 
     * @return GlpiTicketvalidations
     */
    public function setCommentValidation($commentValidation)
    {
        $this->commentValidation = $commentValidation;

        return $this;
    }

    /**
     * Get commentValidation
     *
     * @return string 
     */
    public function getCommentValidation()
    {
        return $this->commentValidation;
    }

    /**
     * Set status
     *
     * @param string $status
     * 
     * @return GlpiTicketvalidations
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set submissionDate
     *
     * @param \DateTime $submissionDate
     * 
     * @return GlpiTicketvalidations
     */
    public function setSubmissionDate($submissionDate)
    {
        $this->submissionDate = $submissionDate;

        return $this;
    }

    /**
     * Get submissionDate
     *
     * @return \DateTime 
     */
    public function getSubmissionDate()
    {
        return $this->submissionDate;
    }

    /**
     * Set validationDate
     *
     * @param \DateTime $validationDate
     * 
     * @return GlpiTicketvalidations
     */
    public function setValidationDate($validationDate)
    {
        $this->validationDate = $validationDate;

        return $this;
    }

    /**
     * Get validationDate
     *
     * @return \DateTime 
     */
    public function getValidationDate()
    {
        return $this->validationDate;
    }
}