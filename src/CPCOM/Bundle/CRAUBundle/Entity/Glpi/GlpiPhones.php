<?php

namespace CPCOM\Bundle\CRAUBundle\Entity\Glpi;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlpiPhones
 *
 * @ORM\Table(name="glpi_phones")
 * @ORM\Entity
 */
class GlpiPhones
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entities_id", type="integer", nullable=false)
     */
    private $entitiesId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_mod", type="datetime", nullable=true)
     */
    private $dateMod;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable=true)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_num", type="string", length=255, nullable=true)
     */
    private $contactNum;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id_tech", type="integer", nullable=false)
     */
    private $usersIdTech;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id_tech", type="integer", nullable=false)
     */
    private $groupsIdTech;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=255, nullable=true)
     */
    private $serial;

    /**
     * @var string
     *
     * @ORM\Column(name="otherserial", type="string", length=255, nullable=true)
     */
    private $otherserial;

    /**
     * @var string
     *
     * @ORM\Column(name="firmware", type="string", length=255, nullable=true)
     */
    private $firmware;

    /**
     * @var integer
     *
     * @ORM\Column(name="locations_id", type="integer", nullable=false)
     */
    private $locationsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="phonetypes_id", type="integer", nullable=false)
     */
    private $phonetypesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="phonemodels_id", type="integer", nullable=false)
     */
    private $phonemodelsId;

    /**
     * @var string
     *
     * @ORM\Column(name="brand", type="string", length=255, nullable=true)
     */
    private $brand;

    /**
     * @var integer
     *
     * @ORM\Column(name="phonepowersupplies_id", type="integer", nullable=false)
     */
    private $phonepowersuppliesId;

    /**
     * @var string
     *
     * @ORM\Column(name="number_line", type="string", length=255, nullable=true)
     */
    private $numberLine;

    /**
     * @var boolean
     *
     * @ORM\Column(name="have_headset", type="boolean", nullable=false)
     */
    private $haveHeadset;

    /**
     * @var boolean
     *
     * @ORM\Column(name="have_hp", type="boolean", nullable=false)
     */
    private $haveHp;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=false)
     */
    private $manufacturersId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_global", type="boolean", nullable=false)
     */
    private $isGlobal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_template", type="boolean", nullable=false)
     */
    private $isTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="template_name", type="string", length=255, nullable=true)
     */
    private $templateName;

    /**
     * @var string
     *
     * @ORM\Column(name="notepad", type="text", nullable=true)
     */
    private $notepad;

    /**
     * @var integer
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="groups_id", type="integer", nullable=false)
     */
    private $groupsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="states_id", type="integer", nullable=false)
     */
    private $statesId;

    /**
     * @var float
     *
     * @ORM\Column(name="ticket_tco", type="decimal", nullable=true)
     */
    private $ticketTco;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entitiesId
     *
     * @param integer $entitiesId
     * 
     * @return GlpiPhones
     */
    public function setEntitiesId($entitiesId)
    {
        $this->entitiesId = $entitiesId;

        return $this;
    }

    /**
     * Get entitiesId
     *
     * @return integer 
     */
    public function getEntitiesId()
    {
        return $this->entitiesId;
    }

    /**
     * Set name
     *
     * @param string $name
     * 
     * @return GlpiPhones
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateMod
     *
     * @param \DateTime $dateMod
     * 
     * @return GlpiPhones
     */
    public function setDateMod($dateMod)
    {
        $this->dateMod = $dateMod;

        return $this;
    }

    /**
     * Get dateMod
     *
     * @return \DateTime 
     */
    public function getDateMod()
    {
        return $this->dateMod;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * 
     * @return GlpiPhones
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set contactNum
     *
     * @param string $contactNum
     * 
     * @return GlpiPhones
     */
    public function setContactNum($contactNum)
    {
        $this->contactNum = $contactNum;

        return $this;
    }

    /**
     * Get contactNum
     *
     * @return string 
     */
    public function getContactNum()
    {
        return $this->contactNum;
    }

    /**
     * Set usersIdTech
     *
     * @param integer $usersIdTech
     * 
     * @return GlpiPhones
     */
    public function setUsersIdTech($usersIdTech)
    {
        $this->usersIdTech = $usersIdTech;

        return $this;
    }

    /**
     * Get usersIdTech
     *
     * @return integer 
     */
    public function getUsersIdTech()
    {
        return $this->usersIdTech;
    }

    /**
     * Set groupsIdTech
     *
     * @param integer $groupsIdTech
     * 
     * @return GlpiPhones
     */
    public function setGroupsIdTech($groupsIdTech)
    {
        $this->groupsIdTech = $groupsIdTech;

        return $this;
    }

    /**
     * Get groupsIdTech
     *
     * @return integer 
     */
    public function getGroupsIdTech()
    {
        return $this->groupsIdTech;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * 
     * @return GlpiPhones
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set serial
     *
     * @param string $serial
     * 
     * @return GlpiPhones
     */
    public function setSerial($serial)
    {
        $this->serial = $serial;

        return $this;
    }

    /**
     * Get serial
     *
     * @return string 
     */
    public function getSerial()
    {
        return $this->serial;
    }

    /**
     * Set otherserial
     *
     * @param string $otherserial
     * 
     * @return GlpiPhones
     */
    public function setOtherserial($otherserial)
    {
        $this->otherserial = $otherserial;

        return $this;
    }

    /**
     * Get otherserial
     *
     * @return string 
     */
    public function getOtherserial()
    {
        return $this->otherserial;
    }

    /**
     * Set firmware
     *
     * @param string $firmware
     * 
     * @return GlpiPhones
     */
    public function setFirmware($firmware)
    {
        $this->firmware = $firmware;

        return $this;
    }

    /**
     * Get firmware
     *
     * @return string 
     */
    public function getFirmware()
    {
        return $this->firmware;
    }

    /**
     * Set locationsId
     *
     * @param integer $locationsId
     * 
     * @return GlpiPhones
     */
    public function setLocationsId($locationsId)
    {
        $this->locationsId = $locationsId;

        return $this;
    }

    /**
     * Get locationsId
     *
     * @return integer 
     */
    public function getLocationsId()
    {
        return $this->locationsId;
    }

    /**
     * Set phonetypesId
     *
     * @param integer $phonetypesId
     * 
     * @return GlpiPhones
     */
    public function setPhonetypesId($phonetypesId)
    {
        $this->phonetypesId = $phonetypesId;

        return $this;
    }

    /**
     * Get phonetypesId
     *
     * @return integer 
     */
    public function getPhonetypesId()
    {
        return $this->phonetypesId;
    }

    /**
     * Set phonemodelsId
     *
     * @param integer $phonemodelsId
     * 
     * @return GlpiPhones
     */
    public function setPhonemodelsId($phonemodelsId)
    {
        $this->phonemodelsId = $phonemodelsId;

        return $this;
    }

    /**
     * Get phonemodelsId
     *
     * @return integer 
     */
    public function getPhonemodelsId()
    {
        return $this->phonemodelsId;
    }

    /**
     * Set brand
     *
     * @param string $brand
     * 
     * @return GlpiPhones
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return string 
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set phonepowersuppliesId
     *
     * @param integer $phonepowersuppliesId
     * 
     * @return GlpiPhones
     */
    public function setPhonepowersuppliesId($phonepowersuppliesId)
    {
        $this->phonepowersuppliesId = $phonepowersuppliesId;

        return $this;
    }

    /**
     * Get phonepowersuppliesId
     *
     * @return integer 
     */
    public function getPhonepowersuppliesId()
    {
        return $this->phonepowersuppliesId;
    }

    /**
     * Set numberLine
     *
     * @param string $numberLine
     * 
     * @return GlpiPhones
     */
    public function setNumberLine($numberLine)
    {
        $this->numberLine = $numberLine;

        return $this;
    }

    /**
     * Get numberLine
     *
     * @return string 
     */
    public function getNumberLine()
    {
        return $this->numberLine;
    }

    /**
     * Set haveHeadset
     *
     * @param boolean $haveHeadset
     * 
     * @return GlpiPhones
     */
    public function setHaveHeadset($haveHeadset)
    {
        $this->haveHeadset = $haveHeadset;

        return $this;
    }

    /**
     * Get haveHeadset
     *
     * @return boolean 
     */
    public function getHaveHeadset()
    {
        return $this->haveHeadset;
    }

    /**
     * Set haveHp
     *
     * @param boolean $haveHp
     * 
     * @return GlpiPhones
     */
    public function setHaveHp($haveHp)
    {
        $this->haveHp = $haveHp;

        return $this;
    }

    /**
     * Get haveHp
     *
     * @return boolean 
     */
    public function getHaveHp()
    {
        return $this->haveHp;
    }

    /**
     * Set manufacturersId
     *
     * @param integer $manufacturersId
     * 
     * @return GlpiPhones
     */
    public function setManufacturersId($manufacturersId)
    {
        $this->manufacturersId = $manufacturersId;

        return $this;
    }

    /**
     * Get manufacturersId
     *
     * @return integer 
     */
    public function getManufacturersId()
    {
        return $this->manufacturersId;
    }

    /**
     * Set isGlobal
     *
     * @param boolean $isGlobal
     * 
     * @return GlpiPhones
     */
    public function setIsGlobal($isGlobal)
    {
        $this->isGlobal = $isGlobal;

        return $this;
    }

    /**
     * Get isGlobal
     *
     * @return boolean 
     */
    public function getIsGlobal()
    {
        return $this->isGlobal;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * 
     * @return GlpiPhones
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set isTemplate
     *
     * @param boolean $isTemplate
     * 
     * @return GlpiPhones
     */
    public function setIsTemplate($isTemplate)
    {
        $this->isTemplate = $isTemplate;

        return $this;
    }

    /**
     * Get isTemplate
     *
     * @return boolean 
     */
    public function getIsTemplate()
    {
        return $this->isTemplate;
    }

    /**
     * Set templateName
     *
     * @param string $templateName
     * 
     * @return GlpiPhones
     */
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;

        return $this;
    }

    /**
     * Get templateName
     *
     * @return string 
     */
    public function getTemplateName()
    {
        return $this->templateName;
    }

    /**
     * Set notepad
     *
     * @param string $notepad
     * 
     * @return GlpiPhones
     */
    public function setNotepad($notepad)
    {
        $this->notepad = $notepad;

        return $this;
    }

    /**
     * Get notepad
     *
     * @return string 
     */
    public function getNotepad()
    {
        return $this->notepad;
    }

    /**
     * Set usersId
     *
     * @param integer $usersId
     * 
     * @return GlpiPhones
     */
    public function setUsersId($usersId)
    {
        $this->usersId = $usersId;

        return $this;
    }

    /**
     * Get usersId
     *
     * @return integer 
     */
    public function getUsersId()
    {
        return $this->usersId;
    }

    /**
     * Set groupsId
     *
     * @param integer $groupsId
     * 
     * @return GlpiPhones
     */
    public function setGroupsId($groupsId)
    {
        $this->groupsId = $groupsId;

        return $this;
    }

    /**
     * Get groupsId
     *
     * @return integer 
     */
    public function getGroupsId()
    {
        return $this->groupsId;
    }

    /**
     * Set statesId
     *
     * @param integer $statesId
     * 
     * @return GlpiPhones
     */
    public function setStatesId($statesId)
    {
        $this->statesId = $statesId;

        return $this;
    }

    /**
     * Get statesId
     *
     * @return integer 
     */
    public function getStatesId()
    {
        return $this->statesId;
    }

    /**
     * Set ticketTco
     *
     * @param float $ticketTco
     * 
     * @return GlpiPhones
     */
    public function setTicketTco($ticketTco)
    {
        $this->ticketTco = $ticketTco;

        return $this;
    }

    /**
     * Get ticketTco
     *
     * @return float 
     */
    public function getTicketTco()
    {
        return $this->ticketTco;
    }
}